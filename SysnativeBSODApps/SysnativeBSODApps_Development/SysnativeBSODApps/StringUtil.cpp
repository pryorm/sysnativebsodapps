#include "StringUtil.h"
#include <algorithm>

bool StringUtil::contains(const std::string& str, const std::string& substr)
{
    return str.find(substr) != std::string::npos;
}

bool StringUtil::startsWith(const std::string& str, const std::string& substr)
{
    return str.find(substr) == 0;
}

bool StringUtil::endsWith(const std::string& str, const std::string& substr)
{
    return str.rfind(substr) == str.length() - substr.length();
}

std::string StringUtil::replace(const std::string& str, const std::string& oldSubstr, const std::string& newSubstr)
{
    std::string result = str;
    size_t pos = 0;
    while ((pos = result.find(oldSubstr, pos)) != std::string::npos)
    {
        result.replace(pos, oldSubstr.length(), newSubstr);
        pos += newSubstr.length();
    }
    return result;
}

std::string StringUtil::toLower(const std::string& str)
{
    std::string result = str;
    std::transform(result.begin(), result.end(), result.begin(), ::tolower);
    return result;
}

std::string StringUtil::toUpper(const std::string& str)
{
    std::string result = str;
    std::transform(result.begin(), result.end(), result.begin(), ::toupper);
    return result;
}

std::string StringUtil::trim(const std::string& str, const char c)
{
    // Trim all occurrences of the character
    size_t start = str.find_first_not_of(c);
    size_t end = str.find_last_not_of(c);
    return str.substr(start, end - start + 1);
}

std::string StringUtil::trim(const std::string& str)
{
    // Trim all whitespace characters
    size_t start = str.find_first_not_of(" \t\n\r\f\v");
    size_t end = str.find_last_not_of(" \t\n\r\f\v");
    return str.substr(start, end - start + 1);
}

std::vector<std::string> StringUtil::split(const std::string& str, const char c)
{
    std::vector<std::string> result;
    size_t start = 0;
    size_t end = str.find(c);
    while (end != std::string::npos)
    {
        result.push_back(str.substr(start, end - start));
        start = end + 1;
        end = str.find(c, start);
    }
    result.push_back(str.substr(start, end));
    return result;
}

std::vector<std::string> StringUtil::split(const std::string& str, const std::string& substr)
{
    std::vector<std::string> result;
    size_t start = 0;
    size_t end = str.find(substr);
    while (end != std::string::npos)
    {
        result.push_back(str.substr(start, end - start));
        start = end + substr.length();
        end = str.find(substr, start);
    }
    result.push_back(str.substr(start, end));
    return result;
}

bool StringUtil::unsignedCompare(const std::string& str1, const std::string& str2)
{
    if (str1.size() != str2.size())
    {
        return false;
    }
    for (std::string::const_iterator c1 = str1.begin(), c2 = str2.begin(); c1 != str1.end(); ++c1, ++c2)
    {
        // Convert both cases to lowercase prior to comparing
        if (tolower(*c1) != tolower(*c2))
        {
            return false;
        }
    }
    return true;
}