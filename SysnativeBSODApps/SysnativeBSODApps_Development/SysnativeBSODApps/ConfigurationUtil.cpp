#include "ConfigurationUtil.h"

#include "LogUtil.h"
#include "SystemStandardConversionUtil.h"

#include <algorithm>
#include <ctime>
#include <iostream>

std::string ConfigurationUtil::parmsDirectory = "";
std::string ConfigurationUtil::outputDirectory = "";
std::string ConfigurationUtil::tempDirectory = "";
std::string ConfigurationUtil::userProfilePath = "";
std::string ConfigurationUtil::timeDirectory = "";

std::string ConfigurationUtil::getParmsDirectory()
{
	if (parmsDirectory.empty())
	{
		parmsDirectory = getUserProfilePath() + "\\SysnativeBSODApps\\parms\\";
	}
	return parmsDirectory;
}

std::string ConfigurationUtil::getUserProfilePath()
{
	if (userProfilePath.empty())
	{
		char* userProfile = nullptr;
		size_t len = 120;
		errno_t err = _dupenv_s(&userProfile, &len, "USERPROFILE");

		if (userProfile)
		{
			userProfilePath = userProfile;
            free(userProfile);
		}
		else
		{
			LogUtil::logErrorAsync("Failed to retrieve USERPROFILE variable.");
		}
	}
	return userProfilePath;
}

std::string ConfigurationUtil::currentDateTime()
{
    time_t     timeNow = time(0);

    struct tm ts;
    char szBuffer[80] = "DD_MM_YY_HH_MM_SS";

    errno_t err = localtime_s(&ts, &timeNow);

    if (err)
    {
        std::cerr << "Failed to get the current time." << std::endl;
    }
    else
    {
        // Format the time
        strftime(szBuffer, sizeof(szBuffer), "%Y_%b_%d_%X%p", &ts);
    }
    return szBuffer;
}

std::string ConfigurationUtil::getTimeDirectory()
{
    if (timeDirectory.empty())
    {
        std::string timeNow = currentDateTime();
        replace_if(timeNow.begin(), timeNow.end(), ::ispunct, '_');
        timeDirectory = timeNow.substr(0, timeNow.end() - timeNow.begin() - 5);
    }
    return timeDirectory;
}

void ConfigurationUtil::setTempDirectory(const std::string& tempDir)
{
    tempDirectory = tempDir;
}

std::string ConfigurationUtil::getTempDirectory()
{
    return tempDirectory;
}

void ConfigurationUtil::setOutputDirectory(const std::string& outputDir)
{
    outputDirectory = outputDir;
}

std::string ConfigurationUtil::getOutputDirectory()
{
    return outputDirectory;
}

std::string ConfigurationUtil::getLogDirectory()
{
    return getUserProfilePath() + "\\SysnativeBSODApps\\Logs\\";
}

std::string ConfigurationUtil::getCurrentDirectory()
{
    return SystemStandardConversionUtil::convertSystemStringToStdString(System::IO::Directory::GetCurrentDirectory());
}