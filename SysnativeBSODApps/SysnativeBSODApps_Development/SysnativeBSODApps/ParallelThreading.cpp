#include "ParallelThreading.h"
#include <string>
#include <fstream>
#include <Windows.h>

#include "debug.h"


//#include <vld.h>

using namespace SysnativeBSODApps;

System::Void ParallelThreading::ParallelThreading_Load(System::Object^  sender, System::EventArgs^  e)
{  
  if(onTop)
  {
    this->TopMost = true;
    UpdateZOrder();
    BringToFront();
  }
  else
  {
    this->TopMost = false;
  }
  numericUpDown1->Value = defaultThreads;
  numericUpDown2->Value = userThreads;
}

System::Void ParallelThreading::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
  defaultThreads = int(numericUpDown1->Value);
  userThreads = int(numericUpDown2->Value);
  Close();
}

System::Void ParallelThreading::button2_Click(System::Object^  sender, System::EventArgs^  e){
  Close();
}

System::Void ParallelThreading::numericUpDown1_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
  SYSTEM_INFO sysinfo;
  GetSystemInfo(&sysinfo);
  int numCPU = sysinfo.dwNumberOfProcessors;  // number of threads
  if(numericUpDown1->Value > (int)((double)numCPU * 1.5))
  {
    numericUpDown1->Value = (int)((double)numCPU * 1.5);
    label3->Text = L"Threads exceed optimum value.\r\nMax threads set to " + numericUpDown1->Value.ToString();
  }
  else
  {
    label3->Text = L"";
  }
}

System::Void ParallelThreading::numericUpDown2_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
  SYSTEM_INFO sysinfo;
  GetSystemInfo(&sysinfo);
  int numCPU = sysinfo.dwNumberOfProcessors;  // number of threads
  if(numericUpDown2->Value > (int)((double)numCPU * 1.5))
  {
    numericUpDown2->Value = (int)((double)numCPU * 1.5);
    label3->Text = L"Threads exceed optimum value.\r\nMax threads set to " + numericUpDown1->Value.ToString();
  }
  else
  {
    label3->Text = L"";
  }
}