#pragma once

#ifdef _DEBUG

  #include <cstdlib>

  #include <vld.h>

  #define _CRTDBG_MAP_ALLOC

  #ifndef DEBUG_NEW

    #define DEBUG_NEW

    #define _CRTDBG_MAP_ALLOC
    // #define _CRTDBG_MAP_ALLOC_NEW 

    #ifndef DBG_NEW
        #define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
    #endif

  #endif

#endif