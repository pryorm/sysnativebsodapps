#include "Windows_8_1_and_10.h"
#include "CenterWindow.h"
#include "SystemStandardConversionUtil.h"
#include <fstream>
#include <vector>
#include <sstream>

using namespace SysnativeBSODApps;
using namespace System::Drawing;

// Compare two std::strings regardless of case
bool Windows_8_1_and_10::unSignedCompare(std::string str1, std::string str2)
{
  if (str1.size() != str2.size()) {
    return false;
  }
  for (std::string::const_iterator c1 = str1.begin(), c2 = str2.begin(); c1 != str1.end(); ++c1, ++c2) {
    // Convert both cases to lowercase prior to comparing
    if (tolower(*c1) != tolower(*c2)) {
      return false;
    }
  }
  return true;
}

System::Void Windows_8_1_and_10::load81oldDriverAfter()
{
  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);

  // read in the 81oldDriverAfter file
  std::ifstream infile;
  std::string inChar;
  System::String^ str2;
  std::string inPath = tempDir + "\\81oldDriverAfter.txt";
  infile.open(inPath);
  System::String^ str3;
  System::String^ str4;
  int oldDriverAfterCount = 0;
  if(infile.good()){      
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
        std::string oldDriverAfter = inChar;
        str4 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str3 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str2 = gcnew System::String(oldDriverAfter.c_str());
        oldDriverAfterCount++;
      }
    }
    infile.close();
  }
  else{  
    std::string oldDriverAfter = "2013";
    str2 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "27";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Aug";
    str4 = gcnew System::String(oldDriverAfter.c_str());
    infile.close();
  }
  if(str2 == str3 && str2 == str4){
    std::string oldDriverAfter = "31";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Dec";
    str4 = gcnew System::String(oldDriverAfter.c_str());
  }
  if(str2 == str3){
    std::string oldDriverAfter = "1";
    str3 = gcnew System::String(oldDriverAfter.c_str());
  }
  textBox18->Text = str4;
  textBox19->Text = str3;
  textBox20->Text = str2;

  inPath = tempDir + "\\10oldDriverAfter.txt";
  infile.open(inPath);
  oldDriverAfterCount = 0;
  if(infile.good()){      
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
        std::string oldDriverAfter = inChar;
        str4 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str3 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str2 = gcnew System::String(oldDriverAfter.c_str());
        oldDriverAfterCount++;
      }
    }
    infile.close();
  }
  else{  
    std::string oldDriverAfter = "2015";
    str2 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "1";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Jul";
    str4 = gcnew System::String(oldDriverAfter.c_str());
    infile.close();
  }
  if(str2 == str3 && str2 == str4){
    std::string oldDriverAfter = "31";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Dec";
    str4 = gcnew System::String(oldDriverAfter.c_str());
  }
  if(str2 == str3){
    std::string oldDriverAfter = "1";
    str3 = gcnew System::String(oldDriverAfter.c_str());
  }
  textBox21->Text = str4;
  textBox22->Text = str3;
  textBox23->Text = str2;
}

System::Void Windows_8_1_and_10::loadExcludedDrivers()
{
  System::String^ str2;
  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);

  richTextBox6->Lines = m_SADriverNames;
  richTextBox7->Lines = m_SADriverDates;

  // read in the EightOneED.txt file
  std::string inPath = tempDir + "\\EightOneED.txt";
  std::ifstream infile;
  infile.open(inPath);
  richTextBox11->Clear();
  std::vector<std::string> EightOneED;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      EightOneED.push_back(inCh);
    }
  }
  infile.close();
  while(int(EightOneED.size()) < m_SADriverNames->Length){
    EightOneED.push_back("");
  }
  for(int i = 0; i < int(m_SADriverNames->Length); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << EightOneED[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << EightOneED[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << EightOneED[i];
    }
    EightOneED[i] = sstemp.str();
    if(i < int(m_SADriverNames->Length) - 1){
      EightOneED[i] = EightOneED[i] + "\n";
    }
    str2 = gcnew System::String(EightOneED[i].c_str());
    richTextBox10->AppendText(str2);
  }

  // read in the TenED.txt file
  inPath = tempDir + "\\TenED.txt";
  infile.open(inPath);
  richTextBox11->Clear();
  std::vector<std::string> TenED;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      TenED.push_back(inCh);
    }
  }
  infile.close();
  while(int(TenED.size()) < m_SADriverNames->Length){
    TenED.push_back("");
  }
  for(int i = 0; i < int(m_SADriverNames->Length); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << TenED[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << TenED[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << TenED[i];
    }
    TenED[i] = sstemp.str();
    if(i < int(m_SADriverNames->Length) - 1){
      TenED[i] = TenED[i] + "\n";
    }
    str2 = gcnew System::String(TenED[i].c_str());
    richTextBox11->AppendText(str2);
  }
}

System::Void Windows_8_1_and_10::Windows_8_1_and_10_Load(System::Object^  sender, System::EventArgs^  e)
{
  m_nSavedED = 0;
  // Using CDPI example class
  CDPI g_metrics;
  int dpiX = g_metrics.GetDPIX();

  richTextBox6->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox7->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox10->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox11->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));

  sP1Sep92002ToolStripMenuItem_Click(sender, e);
  feb42008ToolStripMenuItem_Click(sender, e);
  checkBox11->Checked = true;

  load81oldDriverAfter();

  loadExcludedDrivers();
}

System::Void Windows_8_1_and_10::Windows_8_1_and_10_Resize(System::Object^  sender, System::EventArgs^  e)
{
  // Using CDPI example class
  CDPI g_metrics;
  int dpiX = g_metrics.GetDPIX();
  tabControl1->Height = this->Height - 110 - int(double(100)*double(dpiX-96)/double(64));
  tabControl1->Width = this->Width - int(double(20)*double(dpiX)/double(96));
  richTextBox6->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox7->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox10->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox11->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
}

System::Void Windows_8_1_and_10::betaJun12013ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Jun";
  textBox19->Text = L"1";
  textBox20->Text = L"2013";
}

System::Void Windows_8_1_and_10::sP1Sep92002ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Aug";
  textBox19->Text = L"27";
  textBox20->Text = L"2013";
}

System::Void Windows_8_1_and_10::pubOct172013ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Oct";
  textBox19->Text = L"17";
  textBox20->Text = L"2013";
}

System::Void Windows_8_1_and_10::rTMNov82007ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Oct";
  textBox22->Text = L"1";
  textBox23->Text = L"2014";
}

System::Void Windows_8_1_and_10::feb42008ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Jun";
  textBox22->Text = L"";
  textBox23->Text = L"2015";
}

System::Void Windows_8_1_and_10::pubJuly292015ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Jul";
  textBox22->Text = L"29";
  textBox23->Text = L"2015";
}

System::Void Windows_8_1_and_10::nov12015ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Nov";
  textBox22->Text = L"";
  textBox23->Text = L"2015";
}

System::Void Windows_8_1_and_10::sep12016ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Sep";
  textBox22->Text = L"";
  textBox23->Text = L"2016";
}

System::Void Windows_8_1_and_10::apr12017ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Apr";
  textBox22->Text = L"";
  textBox23->Text = L"2017";
}

System::Void Windows_8_1_and_10::write81OldDriverAfter()
{  
  using namespace System::Runtime::InteropServices;
  System::String^ s;
  const char* chars;
  

  s = textBox20->Text;
  chars = 
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    
  s = textBox19->Text;
  chars = 
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os2 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    
  s = textBox18->Text;
  chars = 
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os3 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);

  std::string options = tempDir + "\\81oldDriverAfter.txt";
  std::ofstream outfile;
  outfile.open(options);
  outfile << os3 << " " << os2 << " " << os;
  outfile.close();
}

System::Void Windows_8_1_and_10::write10OldDriverAfter()
{  
  using namespace System::Runtime::InteropServices;
  System::String^ s;
  const char* chars;
  

  s = textBox23->Text;
  chars = 
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    
  s = textBox22->Text;
  chars = 
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os2 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    
  s = textBox21->Text;
  chars = 
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os3 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);

  std::string options = tempDir + "\\10oldDriverAfter.txt";
  std::ofstream outfile;
  outfile.open(options);
  outfile << os3 << " " << os2 << " " << os;
  outfile.close();
}

System::Void Windows_8_1_and_10::writeExcludedDrivers()
{
  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
  richTextBox10->SaveFile(gcnew System::String(tempDir.c_str()) + "\\EightOneED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  richTextBox11->SaveFile(gcnew System::String(tempDir.c_str()) + "\\TenED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  std::ifstream infile(tempDir + "\\EightOneED.txt");
  std::vector<std::string> ED81;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      ED81.push_back(inCh);
    }
  }
  infile.close();
  std::ofstream outfile(tempDir + "\\EightOneED.txt");
  for(size_t i = 0; i < ED81.size(); i++)
  {
    size_t endChar = ED81[i].size();
    for(size_t j = 0; j < endChar; j++)
    {
      if(ED81[i].substr(0,1) != "x")
      {
        ED81[i].erase(0,1);
      }
    }
    if(i > 0)
    {
      outfile << std::endl;
    }
    outfile << ED81[i];
  }
  outfile.close();

  infile.open(tempDir + "\\TenED.txt");
  std::vector<std::string> ED10;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      ED10.push_back(inCh);
    }
  }
  infile.close();
  outfile.open(tempDir + "\\TenED.txt");
  for(size_t i = 0; i < ED10.size(); i++)
  {
    size_t endChar = ED10[i].size();
    for(size_t j = 0; j < endChar; j++)
    {      
      if(ED10[i].substr(0,1) != "x")
      {
        ED10[i].erase(0,1);
      }
    }
    if(i > 0)
    {
      outfile << std::endl;
    }
    outfile << ED10[i];
  }
  outfile.close();
  
  std::ifstream excludedNamesFile(tempDir + "\\excludedNames.txt");
  std::ifstream excludedDatesFile(tempDir + "\\excludedDates.txt");
  std::string tempPath = tempDir + "\\analyzedDmpOf.txt";
  std::ofstream outProg;
  outProg.open(tempPath);
  outProg << "Saving Excluded Drivers list, Please Wait...";
  std::cout << "Saving Excluded Drivers list, Please Wait..." << std::endl;
  outProg.close();

  std::string excludedNamesFilePath;
  excludedNamesFilePath = tempDir + "\\excludedDrivers.txt";
  outfile.open(excludedNamesFilePath);

  std::vector<std::string> excludedNames;
  std::vector<std::string> excludedDates;
  int num = 0;
  while(!excludedNamesFile.eof() && excludedNamesFile.good()){
    std::string inString;
    std::string newString;
    getline(excludedNamesFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) > 0){
      excludedNames.push_back(newString);
    }
    getline(excludedDatesFile,inString);
    find = 0;
    find2 = 0;
    find3 = 0;
    intFound = 0;
    brokenList = 0;
    std::stringstream ssnum3;
    std::stringstream ssErase2;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase2 << inString.c_str()[i];
        }
      }
      else{
        ssErase2 << inString.c_str()[i];
      }
    }
    inString = ssErase2.str();
    brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum3 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum3.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) > 0){
      excludedDates.push_back(newString);
    }
  }
    
  while(excludedDates.size() < excludedNames.size()){
    excludedDates.push_back("2008");
  }
  for(int i = 0; i < int(excludedNames.size()); i++){
    outfile << excludedNames[i] << " " << excludedDates[i];
    if(i < int(excludedNames.size()) - 1){
      outfile << std::endl;
    }
  }
  outfile.close();
  m_SADriverNames = gcnew array<System::String^>(excludedNames.size());
  m_SADriverDates = gcnew array<System::String^>(excludedDates.size());
  for(size_t i=0; i < excludedNames.size(); i++)
  {
    m_SADriverNames[i] = gcnew System::String(excludedNames[i].c_str());
  }
  for(size_t i=0; i < excludedDates.size(); i++)
  {
    m_SADriverDates[i] = gcnew System::String(excludedDates[i].c_str());
  }
}

System::Void Windows_8_1_and_10::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
  m_nSavedED = 1;
  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);

  write81OldDriverAfter();
  write10OldDriverAfter();  
  richTextBox6->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\excludedNames.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);
    richTextBox7->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\excludedDates.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  writeExcludedDrivers();
  Close();
}

System::Void Windows_8_1_and_10::checkBox11_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox11->Checked){
    checkBox12->Checked = false;
    numericUpDown1->Enabled = false;
    checkBox21->Checked = false;
    textBox24->ReadOnly = false;
    textBox25->ReadOnly = false;
    checkBox13->Enabled = true;
    checkBox14->Enabled = true;
  }
}

System::Void Windows_8_1_and_10::numericUpDown1_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
  checkBox12_CheckedChanged(sender, e);
}

System::Void Windows_8_1_and_10::checkBox12_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox12->Checked){
    textBox24->ReadOnly = false;
    textBox25->ReadOnly = false;
    checkBox13->Enabled = true;
    checkBox14->Enabled = true;
    checkBox11->Checked = false;
    numericUpDown1->Enabled = true;
    int num = 0;
    System::String^ str2;
	std::vector<std::string> ED1;
    std::ifstream EDFile;

    std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
    richTextBox6->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown1->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox24->Text = str2;
      }
    }
  }
  EDFile.close();

    richTextBox7->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    ED1.clear();
    num = 0;
    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown1->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox25->Text = str2;
      }
    }
  }
  EDFile.close();

    richTextBox10->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    ED1.clear();
    num = 0;
    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(newString.length() > 0 && num == int(numericUpDown1->Value)){
        checkBox13->Checked = true;
      }
      else if(num == int(numericUpDown1->Value)){
        checkBox13->Checked = false;
      }
    }
  }
  EDFile.close();

    richTextBox11->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    ED1.clear();
    num = 0;
    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(newString.length() > 0 && num == int(numericUpDown1->Value)){
        checkBox14->Checked = true;
      }
      else if(num == int(numericUpDown1->Value)){
        checkBox14->Checked = false;
      }
    }
  }
  EDFile.close();        

  numericUpDown1->Maximum = int(ED1.size());
  numericUpDown1->Minimum = 1;
  }
  else if(!checkBox12->Checked){
    numericUpDown1->Enabled = false;
    checkBox21->Checked = false;
  }
  if(checkBox21->Checked){
    checkBox12->Checked = true;
    checkBox11->Checked = false;
    numericUpDown1->Visible = true;
    textBox24->ReadOnly = true;
    textBox25->ReadOnly = true;
    checkBox13->Enabled = false;
    checkBox14->Enabled = false;
    checkBox13->Checked = false;
    checkBox14->Checked = false;
  }
}

System::Void Windows_8_1_and_10::checkBox21_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{       
  if(checkBox21->Checked){
    checkBox12->Checked = true;
    checkBox21->Checked = true;
    int num = 0;
    System::String^ str2;
	std::vector<std::string> ED1;
    std::ifstream EDFile;

    std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
    richTextBox6->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown1->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox24->Text = str2;
      }
    }
  }
  EDFile.close();
    checkBox11->Checked = false;
    numericUpDown1->Visible = true;
    textBox24->ReadOnly = true;
    textBox25->ReadOnly = true;
    checkBox13->Enabled = false;
    checkBox14->Enabled = false;
    // textBox24->Clear();
    // textBox25->Clear();
    checkBox13->Checked = false;
    checkBox14->Checked = false;
  }       
  else if(!checkBox21->Checked){
    textBox24->ReadOnly = false;
    textBox25->ReadOnly = false;
    checkBox12_CheckedChanged(sender, e);
    checkBox13->Enabled = true;
    checkBox14->Enabled = true;
  }
}

System::Void Windows_8_1_and_10::cleanUpOldOSEDs()
{
  for(int i=0; i<m_SAXPED->Length; i++)
  {
    std::string tempString = SystemStandardConversionUtil::convertSystemStringToStdString(m_SAXPED[i]);
    size_t lngth = tempString.size();
    for(size_t j=0; j<lngth; j++)
    {
      if(tempString.substr(0,1) != "x")
      {
        tempString.erase(0,1);
      }
    }
    m_SAXPED[i] = gcnew System::String(tempString.c_str());
  }

  for(int i=0; i<m_SAVistaED->Length; i++)
  {
    std::string tempString = SystemStandardConversionUtil::convertSystemStringToStdString(m_SAVistaED[i]);
    size_t lngth = tempString.size();
    for(size_t j=0; j<lngth; j++)
    {
      if(tempString.substr(0,1) != "x")
      {
        tempString.erase(0,1);
      }
    }
    m_SAVistaED[i] = gcnew System::String(tempString.c_str());
  }

  for(int i=0; i<m_SASevenED->Length; i++)
  {
    std::string tempString = SystemStandardConversionUtil::convertSystemStringToStdString(m_SASevenED[i]);
    size_t lngth = tempString.size();
    for(size_t j=0; j<lngth; j++)
    {
      if(tempString.substr(0,1) != "x")
      {
        tempString.erase(0,1);
      }
    }
    m_SASevenED[i] = gcnew System::String(tempString.c_str());
  }

  for(int i=0; i<m_SAEightED->Length; i++)
  {
    std::string tempString = SystemStandardConversionUtil::convertSystemStringToStdString(m_SAEightED[i]);
    size_t lngth = tempString.size();
    for(size_t j=0; j<lngth; j++)
    {
      if(tempString.substr(0,1) != "x")
      {
        tempString.erase(0,1);
      }
    }
    m_SAEightED[i] = gcnew System::String(tempString.c_str());
  }
}

System::Void Windows_8_1_and_10::deleteOldOSEDs(int num)
{
  array<System::String^>^ tempString = gcnew array<System::String^>( m_SAXPED->Length - 1);
  for(int i=0; i < m_SAXPED->Length; i++)
  {
    if(i < num)
    {
      if(i < 9)
      {
        tempString[i] = "{" + (i+1).ToString() + "}    " + m_SAXPED[i];
      }
      if(i >= 9 && i < 99)
      {
        tempString[i] = "{" + (i+1).ToString() + "}   " + m_SAXPED[i];
      }
      if(i >= 99 && i < 999)
      {
        tempString[i] = "{" + (i+1).ToString() + "}  " + m_SAXPED[i];
      }
    }
    if(i > num)
    {
      if(i-1 < 9)
      {
        tempString[i-1] = "{" + (i).ToString() + "}    " + m_SAXPED[i];
      }
      if(i-1 >= 9 && i-1 < 99)
      {
        tempString[i-1] = "{" + (i).ToString() + "}   " + m_SAXPED[i];
      }
      if(i-1 >= 99 && i-1 < 999)
      {
        tempString[i-1] = "{" + (i).ToString() + "}  " + m_SAXPED[i];
      }
    }
  }
  m_SAXPED = tempString;

  tempString = gcnew array<System::String^>( m_SAVistaED->Length - 1);
  for(int i=0; i < m_SAVistaED->Length; i++)
  {
    if(i < num)
    {
      if(i < 9)
      {
        tempString[i] = "{" + (i+1).ToString() + "}    " + m_SAVistaED[i];
      }
      if(i >= 9 && i < 99)
      {
        tempString[i] = "{" + (i+1).ToString() + "}   " + m_SAVistaED[i];
      }
      if(i >= 99 && i < 999)
      {
        tempString[i] = "{" + (i+1).ToString() + "}  " + m_SAVistaED[i];
      }
    }
    if(i > num)
    {
      if(i-1 < 9)
      {
        tempString[i-1] = "{" + (i).ToString() + "}    " + m_SAVistaED[i];
      }
      if(i-1 >= 9 && i-1 < 99)
      {
        tempString[i-1] = "{" + (i).ToString() + "}   " + m_SAVistaED[i];
      }
      if(i-1 >= 99 && i-1 < 999)
      {
        tempString[i-1] = "{" + (i).ToString() + "}  " + m_SAVistaED[i];
      }
    }
  }
  m_SAVistaED = tempString;
  
  tempString = gcnew array<System::String^>( m_SASevenED->Length - 1);
  for(int i=0; i < m_SASevenED->Length; i++)
  {
    if(i < num)
    {
      if(i < 9)
      {
        tempString[i] = "{" + (i+1).ToString() + "}    " + m_SASevenED[i];
      }
      if(i >= 9 && i < 99)
      {
        tempString[i] = "{" + (i+1).ToString() + "}   " + m_SASevenED[i];
      }
      if(i >= 99 && i < 999)
      {
        tempString[i] = "{" + (i+1).ToString() + "}  " + m_SASevenED[i];
      }
    }
    if(i > num)
    {
      if(i-1 < 9)
      {
        tempString[i-1] = "{" + (i).ToString() + "}    " + m_SASevenED[i];
      }
      if(i-1 >= 9 && i-1 < 99)
      {
        tempString[i-1] = "{" + (i).ToString() + "}   " + m_SASevenED[i];
      }
      if(i-1 >= 99 && i-1 < 999)
      {
        tempString[i-1] = "{" + (i).ToString() + "}  " + m_SASevenED[i];
      }
    }
  }
  m_SASevenED = tempString;
  
  tempString = gcnew array<System::String^>( m_SAEightED->Length - 1);
  for(int i=0; i < m_SAEightED->Length; i++)
  {
    if(i < num)
    {
      if(i < 9)
      {
        tempString[i] = "{" + (i+1).ToString() + "}    " + m_SAEightED[i];
      }
      if(i >= 9 && i < 99)
      {
        tempString[i] = "{" + (i+1).ToString() + "}   " + m_SAEightED[i];
      }
      if(i >= 99 && i < 999)
      {
        tempString[i] = "{" + (i+1).ToString() + "}  " + m_SAEightED[i];
      }
    }
    if(i > num)
    {
      if(i-1 < 9)
      {
        tempString[i-1] = "{" + (i).ToString() + "}    " + m_SAEightED[i];
      }
      if(i-1 >= 9 && i-1 < 99)
      {
        tempString[i-1] = "{" + (i).ToString() + "}   " + m_SAEightED[i];
      }
      if(i-1 >= 99 && i-1 < 999)
      {
        tempString[i-1] = "{" + (i).ToString() + "}  " + m_SAEightED[i];
      }
    }
  }
  m_SAEightED = tempString;
}

System::Void Windows_8_1_and_10::button5_Click(System::Object^  sender, System::EventArgs^  e)
{
  int num = 0;
  System::String^ str2;
  std::vector<std::string> ED1;
  std::vector<std::string> ED2;
  std::vector<std::string> ED3;
  std::vector<std::string> ED4;
  std::ifstream EDFile;
  int boxClear = 0;
  int osClear = 0;
       
  if(checkBox21->Checked){
    checkBox12->Checked = true;
    checkBox11->Checked = false;
    numericUpDown1->Visible = true;
    textBox24->ReadOnly = true;
    textBox25->ReadOnly = true;
    textBox24->Clear();
    textBox25->Clear();
    checkBox13->Checked = false;
    checkBox14->Checked = false;
  }
  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
  richTextBox6->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if((int(newString.length()) > 0) || (checkBox12->Checked && int(numericUpDown1->Value) == num && int(newString.length()) >= 0)){
      System::String ^ s = textBox24->Text;
      const char* chars = 
      (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
      std::string os = chars;
      System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(os.length() > 3){
          ED1.push_back(os);
        }
        else{
          boxClear++;
        }

      }
      else{        
        ED1.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    System::String ^ s = textBox24->Text;
    const char* chars = 
    (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
    std::string os = chars;
    System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    if(os.length() > 3){
      ED1.push_back(os);
    }
    else{
      boxClear++;
    }
  }
  EDFile.close();

  richTextBox7->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  num = 0;
  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if((int(newString.length()) > 0) || (checkBox12->Checked && int(numericUpDown1->Value) == num && int(newString.length()) >= 0)){
      System::String ^ s = textBox25->Text;
      const char* chars = 
      (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
      std::string os = chars;
      System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(os.length() > 3){
          ED2.push_back(os);
        }
        else{
          boxClear++;
        }

      }
      else{        
        ED2.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    System::String ^ s = textBox25->Text;
    const char* chars = 
    (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
    std::string os = chars;
	System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    if(os.length() > 3){
      ED2.push_back(os);
    }
    else{
      boxClear++;
    }
  }
  EDFile.close();

  richTextBox10->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  num = 0;
  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(checkBox13->Checked){
          ED3.push_back("x");
          osClear++;
        }
        else if(!boxClear){
          ED3.push_back("");
        }

      }
      else{        
        ED3.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    if(checkBox13->Checked){
      ED3.push_back("x");
      osClear++;
    }
    else{
      ED3.push_back("");
    }
  }
  EDFile.close();

  richTextBox11->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  num = 0;
  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' || 
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' || 
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' || 
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(checkBox14->Checked){
          ED4.push_back("x");
          osClear++;
        }
        else if(!boxClear){
          ED4.push_back("");
        }
      }
      else{        
        ED4.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    if(checkBox14->Checked){
      ED4.push_back("x");
      osClear++;
    }
    else{
      ED4.push_back("");
    }
  }
  EDFile.close();

    
  if(boxClear == 2 && !osClear && checkBox12->Checked){
    TopMost = true;
    if(System::Windows::Forms::MessageBox::Show("Are you sure you want to delete the line chosen?\nThis cannot be undone!",L"Clear Excluded Driver", System::Windows::Forms::MessageBoxButtons::YesNo, System::Windows::Forms::MessageBoxIcon::Question,
		System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly) == System::Windows::Forms::DialogResult::Yes){
        boxClear = 0;
        osClear = 1;
        cleanUpOldOSEDs();
        deleteOldOSEDs(int(numericUpDown1->Value)-1);
    }
    else
    {
      boxClear = 23;
    }
    TopMost = false;
  }

    
  if(!boxClear && osClear){
    richTextBox6->Clear();
    for(int i = 0; i < int(ED1.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED1[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED1[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED1[i];
      }
      ED1[i] = sstemp.str();
      if(i < int(ED1.size()) - 1){
        ED1[i] = ED1[i] + "\n";
      }
      str2 = gcnew System::String(ED1[i].c_str());
      richTextBox6->AppendText(str2);
    }
    richTextBox7->Clear();
    for(int i = 0; i < int(ED2.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED2[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED2[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED2[i];
      }
      ED2[i] = sstemp.str();
      if(i < int(ED2.size()) - 1){
        ED2[i] = ED2[i] + "\n";
      }
      str2 = gcnew System::String(ED2[i].c_str());
      richTextBox7->AppendText(str2);
    }
    richTextBox10->Clear();
    for(int i = 0; i < int(ED3.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED3[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED3[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED3[i];
      }
      ED3[i] = sstemp.str();
      if(i < int(ED3.size()) - 1){
        ED3[i] = ED3[i] + "\n";
      }
      str2 = gcnew System::String(ED3[i].c_str());
      richTextBox10->AppendText(str2);
    }
    richTextBox11->Clear();
    for(int i = 0; i < int(ED4.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED4[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED4[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED4[i];
      }
      ED4[i] = sstemp.str();
      if(i < int(ED4.size()) - 1){
        ED4[i] = ED4[i] + "\n";
      }
      str2 = gcnew System::String(ED4[i].c_str());
      richTextBox11->AppendText(str2);
    }
  }
  else if(boxClear && boxClear != 23){
    TopMost = true;
	System::Windows::Forms::MessageBox::Show(L"A key input was left blank.\nPlease fill in all text boxes",L"Blank Input", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
    TopMost = false;
  }
  /*else if(!osClear){
    TopMost = true;
    MessageBox::Show(L"No Windows Version was selected for this driver.\nPlease select a Windows OS version",L"Missing OS Version",MessageBoxButtons::OK, MessageBoxIcon::Error, MessageBoxDefaultButton::Button1, MessageBoxOptions::DefaultDesktopOnly);
    TopMost = false;
  }*/  
  checkBox21_CheckedChanged(sender, e);
}