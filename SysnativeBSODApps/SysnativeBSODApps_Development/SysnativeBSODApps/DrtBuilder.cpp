#include "DrtBuilder.h"

#include "ConfigurationUtil.h"
#include "LogUtil.h"
#include "StringUtil.h"
extern "C" {
#include "sqlite3.h"
}

#include <fstream>
#include <iterator>
#include <regex>
#include <string>
#include <sstream>

DrtBuilder::DrtBuilder()
{
    buildDriverNameProviderMap();
}

bool DrtBuilder::parseDriverInfo(std::string htmlFile)
{
	bool foundDriverInfo = false;
	bool isMicrosoftDriver = false;
	std::regex trRegex("<tr role=\"row\">([\\s\\S]*?)</tr>");
	std::regex tdRegex("<td class=\"drivers-index-name\"><a href=\"(.*?)\">(.*?)</a></td>");
	std::regex tdDescRegex("<td class=\"drivers-index-description\"><div class=\"bbWrapper\">(.*?)</div></td>");
	std::regex tdUrlRegex("<td class=\"drivers-index-url\"><div class=\"bbWrapper\">([\\s\\S]*?)</div></td>");

	std::ifstream file(htmlFile);
	std::string html((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

	std::smatch tbodyMatch, trMatch, tdMatch, tdDescMatch, tdUrlMatch;
	std::string driverName, driverDesc, driverUrl;

	while (std::regex_search(html, trMatch, trRegex))
	{
		std::string trStr = trMatch[1].str();
		html = trMatch.suffix().str();

		if (std::regex_search(trStr, tdMatch, tdRegex))
		{
			driverName = tdMatch[2].str();
			driverUrlMap[driverName] = tdMatch[1].str();
		}

		if (std::regex_search(trStr, tdDescMatch, tdDescRegex))
		{
			driverDesc = tdDescMatch[1].str();
		}

		if (std::regex_search(trStr, tdUrlMatch, tdUrlRegex)) {
			std::string urlStr = tdUrlMatch[1].str(); // Adjust to capture all URLs in the cell
			std::regex aTagRegex("<a href=\"(.*?)\">(.*?)</a>");
			std::smatch aTagMatch;

			std::string collectedUrls; // Temporary string to collect URLs
			bool append = false;
			while (std::regex_search(urlStr, aTagMatch, aTagRegex)) {
				std::string urlVal = aTagMatch[2].str();
				if (append)
				{
					collectedUrls += " , ";
				}
				collectedUrls += urlVal;
				append = true;
				urlStr = aTagMatch.suffix().str();
			}
			if (collectedUrls.empty())
			{
				collectedUrls = urlStr;
			}
			driverUrl = collectedUrls; // Assign collected URLs to driverUrl
		}

		if (!driverName.empty())
		{
            isMicrosoftDriver = StringUtil::contains(driverNameProviderMap[StringUtil::toLower(StringUtil::split(driverName, '.')[0])], "Microsoft");
			foundDriverInfo = true;
            if (StringUtil::toLower(driverUrl) == "windows update")
            {
                isMicrosoftDriver = true;
            }
			driverInfo.push_back(driverName + "}https://www.sysnative.com" + driverUrlMap[driverName] + "}" + driverDesc + "}" + driverUrl + "}" + std::to_string(isMicrosoftDriver));
		}
	}

	return foundDriverInfo;
}

void DrtBuilder::writeDriverInfoToFile(std::string filename)const
{
	std::ofstream file(filename);
	for (std::string info : driverInfo)
	{
		file << info << '{';
	}
	file << std::endl;
	file.close();
}

std::vector<std::string> DrtBuilder::getDriverInfo()const
{
	return driverInfo;
}

int DrtBuilder::getMaxPages(std::string htmlFile)const
{
	std::regex liRegex("<li class=\"pageNav-page pageNav-page--skip pageNav-page--skipEnd\">([\\s\\S]*?)</li>");
	std::regex maxPagesRegex(" min=\"1\" max=\"([\\s\\S]*?)\"");

	std::ifstream file(htmlFile);
	std::string html((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

	std::smatch liMatch, maxMatch;
	int maxPages = 0;

	if (std::regex_search(html, liMatch, liRegex)) {
		std::string liContent = liMatch[1].str();

		if (std::regex_search(liContent, maxMatch, maxPagesRegex)) {
			maxPages = std::stoi(maxMatch[1].str());
		}
	}

	return maxPages;
}

void DrtBuilder::buildDriverNameProviderMap()
{
    using namespace std::string_literals;
    sqlite3 *db;
    sqlite3_stmt *stmt;
    int rc = sqlite3_open((ConfigurationUtil::getUserProfilePath() + "\\SysnativeBSODApps\\driverStatistics\\drivers.db"s).c_str(), &db);
    if (rc)
    {
        sqlite3_close(db);
        std::ostringstream osErr;
        osErr << "Unable to open database: " << sqlite3_errmsg(db);
        LogUtil::logErrorAsync(osErr.str());
        return;
    }

    const char* sql = "SELECT Driver, OriginalFileName, ClassName, ProviderName, Date, Version FROM Drivers;";
    rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

    if (rc != SQLITE_OK) {
        sqlite3_close(db);
        std::ostringstream osErr;
        osErr << "Failed to retrieve data: " << sqlite3_errmsg(db);
        LogUtil::logErrorAsync(osErr.str());
        return;
    }

    while (sqlite3_step(stmt) == SQLITE_ROW)
    {
        std::string driverName = std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)));
        std::string provider = std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 3)));
        // Trim extension from driver name
        driverName = StringUtil::trim(StringUtil::toLower(StringUtil::split(driverName, ".")[0]), '\"');
        driverNameProviderMap[driverName] = StringUtil::trim(provider, '\"');
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
}