#pragma once

#include <string>
#include <vector>

ref class FileSystemUtil
{
public:
    // Move a file from one location to another
    // @param sourceFile: the file to move
    // @param destFile: the location to move the file to
    // @return: true if the move was successful, false otherwise
    static bool moveFile(System::String^ sourceFile, System::String^ destFile);

    // Move a file from one location to another
    // @param sourceFile: the file to move
    // @param destFile: the location to move the file to
    // @return: true if the move was successful, false otherwise
    static bool moveFile(const std::string& sourceFile, const std::string& destFile);

    // Copy file from one location to another
    // @param sourceFile: the file to copy
    // @param destFile: the location to copy the file to
    // @param overwrite: true to overwrite the destination file if it exists, false otherwise
    // @return: true if the copy was successful, false otherwise
    static bool copyFile(System::String^ sourceFile, System::String^ destFile, bool overwrite);

    // Copy file from one location to another
    // @param sourceFile: the file to copy
    // @param destFile: the location to copy the file to
    // @param overwrite: true to overwrite the destination file if it exists, false otherwise
    // @return: true if the copy was successful, false otherwise
    static bool copyFile(const std::string& sourceFile, const std::string& destFile, bool overwrite);

    // Copy directory and all subdirectories and files inside the directory
    // @param sourceDir: the directory to copy
    // @param destDir: the directory to copy sourceDir to
    // @return: true if the copy was successful, false otherwise
    static bool copyDirectoryAndContents(System::String^ sourceDir, System::String^ destDir, bool overwrite);

    // Copy directory and all subdirectories and files inside the directory
    // @param sourceDir: the directory to copy
    // @param destDir: the directory to copy sourceDir to
    // @return: true if the copy was successful, false otherwise
    static bool copyDirectoryAndContents(const std::string& sourceDir, const std::string& destDir, bool overwrite);

    // Remove directory and all subdirectories and files inside the directory
    // @param dirPath: the directory to remove
    static void removeDirectoryAndContents(System::String^ dirPath);

    // Remove directory and all subdirectories and files inside the directory
    // @param dirPath: the directory to remove
    static void removeDirectoryAndContents(const std::string& dirPath);

    // Remove empty directory
    // @param dirPath: the directory to remove
    static void removeEmptyDirectory(System::String^ dirPath);

    // Remove empty directory
    // @param dirPath: the directory to remove
    static void removeEmptyDirectory(const std::string& dirPath);

    // Backup file if it exists using its last write time as part of the backup file name
    // @param sourceFile: the file to backup
    // @return: true if the backup was successful, false otherwise
    static bool backupFile(System::String^ sourceFile);

    // Create a directory and backup the file if a file exists with the same name as the directory being created
    // @param topDirectory: the directory to create the subdirectory in
    // @param subDirectory: the subdirectory to create
    // @return: true if the directory was created, false otherwise
    static bool createDirectoryWithBackup(System::String^ topDirectory, System::String^ subDirectory);

    // Create a directory and backup the file if a file exists with the same name as the directory being created
    // @param topDirectory: the directory to create the subdirectory in
    // @param subDirectory: the subdirectory to create
    // @return: true if the directory was created, false otherwise
    static bool createDirectoryWithBackup(const std::string& topDirectory, const std::string& subDirectory);

    // Move a directory, all subdirectories, and all files inside the directory/subdirectories to a new location
    // and do so without recursion
    // @param sourceDir: the directory to move
    // @param destDir: the directory to move sourceDir to
    // @return: true if the move was successful, false otherwise
    static bool moveDirectoryAndContents(System::String^ sourceDir, System::String^ destDir);

    // Backup existing files in a directory that are being replaced by new files if the new files are newer
    // @param sourceDir: the directory containing the new files
    // @param destDir: the directory containing the existing files
    // @param files: the files to check for changes
    static void transferFilesWithBackupAndReplace(System::String^ sourceDir, System::String^ destDir);

    // Move a directory, all subdirectories, and all files inside the directory/subdirectories to a new location
    // and do so with recursion, copying files that exist and have changed since the last copy after backing up
    // the original file by changing its name to filename-%%YYYYMMDDHHMMSS%%.ext
    // @param sourceDir: the directory to move
    // @param destDir: the directory to move sourceDir to
    // @return: true if the move was successful, false otherwise
    static bool transferDirectoryAndContentsWithBackup(System::String^ sourceDir, System::String^ destDir, bool removeSource);

    // Move a directory, all subdirectories, and all files inside the directory/subdirectories to a new location
    // and do so with recursion, copying files that exist and have changed since the last copy after backing up
    // the original file by changing its name to filename-%%YYYYMMDDHHMMSS%%.ext
    // @param sourceDir: the directory to move
    // @param destDir: the directory to move sourceDir to
    // @return: true if the move was successful, false otherwise
    static bool transferDirectoryAndContentsWithBackup(const std::string& sourceDir, const std::string& destDir, bool removeSource);

    // Get the files in a directory
    // @param sourceDir: the directory to get the files from
    // @return: an array of file paths in the directory
    static cli::array<System::String^>^ getFiles(System::String^ sourceDir);

    // Get the files in a directory
    // @param sourceDir: the directory to get the files from
    // @return: a vector of file paths in the directory
    static std::vector<std::string> getFiles(const std::string& sourceDir);

    // Get the filename from a file path
    // @param filePath: the file path to get the filename from
    // @return: the filename
    static System::String^ getFileName(System::String^ filePath);

    // Get the filename from a file path
    // @param filePath: the file path to get the filename from
    // @return: the filename
    static std::string getFileName(const std::string& filePath);

    // Create a symbolic link to a directory
    // @param linkPath: the path to the symbolic link
    // @param targetPath: the path to the target directory
    static void createLink(System::String^ linkPath, System::String^ targetPath);

    // Remove an empty directory or symbolic link
    // @param linkPath: the path to the directory or symbolic link to remove
    static void removeEmptyDirectoryOrLink(System::String^ linkPath);

    // Modify the permissions and attributes of a directory
    // @param dirPath: the path to the directory to modify
    static void modifyPermissionsAndAttributes(System::String^ dirPath, System::String^ group, System::Security::AccessControl::FileSystemRights rights, System::Security::AccessControl::AccessControlType accessType, System::IO::FileAttributes attributes);

    // Open file
    // @param filePath: the path to the file to open
    static void openFile(System::String^ filePath);

    // Open file
    // @param filePath: the path to the file to open
    static void openFile(const std::string& filePath);

    // Get current action
    // @return: the current action
    static System::String^ getCurrentAction();

private:
    static void runCommand(System::String^ command, System::String^ arguments);

    static System::String^ currentAction;

};