#include "LoadSaveDZDN.h"

#include "SystemStandardConversionUtil.h"

#include <vector>
#include <fstream>

#include "debug.h"


//#include <vld.h>

// Constructor
LoadSaveDZDN::LoadSaveDZDN()
{
}

LoadSaveDZDN::~LoadSaveDZDN()
{
}

// Load the default.dzdn file information
std::vector<std::string> LoadSaveDZDN::LoadDefaultDZDN()
{  
  // Read user profile and save it to the userprofile string variable
  std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
  std::ifstream infile(userProfile + "\\SysnativeBSODApps\\forumSettings\\Default.dzdn");
  std::vector<std::string> inStringV;
  while(infile.good() && !infile.eof())
  {
    std::string inString;
    getline(infile,inString);
    if(inString == "## ### end of standard settings")
    {
      return inStringV;
    }
    inStringV.push_back(inString);
  }
  return inStringV;
}