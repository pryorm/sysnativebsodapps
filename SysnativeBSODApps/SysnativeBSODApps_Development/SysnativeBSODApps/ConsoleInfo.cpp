#include <iostream>
#include <fstream>
#include <windows.h>
#include <string>
#include "ConsoleInfo.hpp"

#include "debug.h"

ConsoleInfo::ConsoleInfo(){}
ConsoleInfo::~ConsoleInfo(){}
std::string ConsoleInfo::getConsoleInfo(LPWSTR Command) 
{ 
  if(1){
  std::string result = ""; 
  
  HANDLE hChildStdoutRd, hChildStdoutWr, hChildStdoutRdDup, hSaveStdout; 
  SECURITY_ATTRIBUTES saAttr; 
  BOOL fSuccess; 

  // Set the bInheritHandle flag so pipe handles are inherited. 
  saAttr.nLength = sizeof(SECURITY_ATTRIBUTES); 
  saAttr.bInheritHandle = TRUE; 
  saAttr.lpSecurityDescriptor = NULL; 

  // Save the handle to the current STDOUT. 
  hSaveStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
  
  // Create a pipe for the child process's STDOUT. 
  if (! CreatePipe(&hChildStdoutRd, &hChildStdoutWr, &saAttr, 0)){
    SetStdHandle(STD_OUTPUT_HANDLE, hSaveStdout);
    return "Broken"; 
  }

  // Set a write handle to the pipe to be STDOUT.
  if (! SetStdHandle(STD_OUTPUT_HANDLE, hChildStdoutWr)) {
    SetStdHandle(STD_OUTPUT_HANDLE, hSaveStdout);
    return "Broken"; 
  }
  
  // Create noninheritable read handle and close the inheritable read handle. 
  fSuccess = DuplicateHandle(GetCurrentProcess(), hChildStdoutRd, 
      GetCurrentProcess(), &hChildStdoutRdDup , 0, 
      FALSE, 
      DUPLICATE_SAME_ACCESS); 
  if( !fSuccess ){
    SetStdHandle(STD_OUTPUT_HANDLE, hSaveStdout);
    CloseHandle(hChildStdoutRd);
    return "Broken";  
  }
  else{
    CloseHandle(hChildStdoutRd); 
  }

  // Now create the child process. 
  PROCESS_INFORMATION piProcInfo; 
  STARTUPINFO siStartInfo; 

  // Set up members of the PROCESS_INFORMATION structure. 
    ZeroMemory( &piProcInfo, sizeof(PROCESS_INFORMATION) ); 
  
  // Set up members of the STARTUPINFO structure. 
  ZeroMemory( &siStartInfo, sizeof(STARTUPINFO) ); 
  siStartInfo.cb = sizeof(STARTUPINFO); 

  // Create the child process. 
  fSuccess = CreateProcess(NULL,
	  Command,                      // command line 
	  NULL,                         // process security attributes 
	  NULL,                         // primary thread security attributes 
	  TRUE,                         // handles are inherited 
	  CREATE_NO_WINDOW,             // creation flags 
	  NULL,                         // use parent's environment 
	  NULL,                         // use parent's current directory 
	  &siStartInfo,                 // STARTUPINFO pointer 
	  &piProcInfo                   // receives PROCESS_INFORMATION
  );
  
  if (! fSuccess){
    SetStdHandle(STD_OUTPUT_HANDLE, hSaveStdout);
    return "Broken"; 
  }
  else
  {
      // Wait until child process exits.
	  WaitForSingleObject(piProcInfo.hProcess, INFINITE);
	  CloseHandle(piProcInfo.hProcess);  // Close the process handle
	  CloseHandle(piProcInfo.hThread);  // Close the thread handle
  }
  
  // After process creation, restore the saved STDIN and STDOUT. 
    if (! SetStdHandle(STD_OUTPUT_HANDLE, hSaveStdout)) {
    SetStdHandle(STD_OUTPUT_HANDLE, hSaveStdout);
    return "Broken"; 
    }
  
  // Read from pipe that is the standard output for child process. 
  DWORD dwRead; 
  CHAR chBuf[4096]; 
  HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 

  // Close the write end of the pipe before reading from the 
  // read end of the pipe. 
    if (!CloseHandle(hChildStdoutWr)) {
    SetStdHandle(STD_OUTPUT_HANDLE, hSaveStdout);
    return "Broken"; 
    }
  
  // Read output from the child process, and write to parent's STDOUT. 
  for (;;) 
  { 
    if( !ReadFile(hChildStdoutRdDup, chBuf, 4096, &dwRead, NULL) || dwRead == 0) 
     break; 
    result += std::string(chBuf,dwRead);
  } 
  
  return result; 
  }
  else{
    return "Broken";
  }
} 