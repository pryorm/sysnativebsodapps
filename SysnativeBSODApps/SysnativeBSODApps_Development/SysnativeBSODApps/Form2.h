#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
//#include <vld.h>
#include <Windows.h>

#include "debug.h"

#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for Form2
  /// </summary>
  public ref class Form2 : public System::Windows::Forms::Form
  {
  public:
    Form2(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~Form2()
    {
      if (components)
      {
        delete components;
      }
    }
  private: System::Windows::Forms::Label^  label1;
  protected: 
  private: System::Windows::Forms::LinkLabel^  linkLabel1;
  private: System::Windows::Forms::Button^  button1;


  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form2::typeid));
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->linkLabel1 = (gcnew System::Windows::Forms::LinkLabel());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->SuspendLayout();
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(0, 0);
      this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(313, 26);
      this->label1->TabIndex = 0;
      this->label1->Text = L"There may be an update available for the Sysnative BSOD Apps.\r\nPlease check for u" 
        L"pdates at the forums homepage:";
      this->label1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
      // 
      // linkLabel1
      // 
      this->linkLabel1->AutoSize = true;
      this->linkLabel1->Location = System::Drawing::Point(46, 28);
      this->linkLabel1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->linkLabel1->Name = L"linkLabel1";
      this->linkLabel1->Size = System::Drawing::Size(220, 13);
      this->linkLabel1->TabIndex = 1;
      this->linkLabel1->TabStop = true;
      this->linkLabel1->Text = L"https://www.sysnative.com/forums/";
      this->linkLabel1->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Form2::goToWebpage);
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(124, 54);
      this->button1->Margin = System::Windows::Forms::Padding(2);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(56, 19);
      this->button1->TabIndex = 2;
      this->button1->Text = L"OK";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &Form2::button1_Click);
      // 
      // Form2
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(314, 75);
      this->Controls->Add(this->button1);
      this->Controls->Add(this->linkLabel1);
      this->Controls->Add(this->label1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Margin = System::Windows::Forms::Padding(2);
      this->Name = L"Form2";
      this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
      this->Text = L"Update Available";
      this->TopMost = true;
      this->Load += gcnew System::EventHandler(this, &Form2::Form2_Load);
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void goToWebpage(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e);
private: System::Void Form2_Load(System::Object^  sender, System::EventArgs^  e);
};
}
