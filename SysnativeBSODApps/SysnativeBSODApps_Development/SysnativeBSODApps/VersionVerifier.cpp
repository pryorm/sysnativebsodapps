#include "VersionVerifier.h"

#include "Form2.h"

#include <iostream>

namespace SysnativeBSODApps
{

    System::String^ VersionVerifier::getVersionUrl()
    {
        return "https://drive.google.com/uc?export=view&id=18BSF-kjKdVrbYyAJcTNX3J6coqyRQ63F";
    }

    System::Threading::Tasks::Task^ VersionVerifier::checkForUpdateAsync()
    {
        return System::Threading::Tasks::Task::Factory->StartNew(gcnew System::Action(&VersionVerifier::fetchAndCompareVersion));
    }

    void VersionVerifier::fetchAndCompareVersion()
    {
        try
        {
            System::Net::WebClient^ client = gcnew System::Net::WebClient();
            System::String^ latestVersion = client->DownloadString(getVersionUrl())->Trim();

            if (isNewVersionAvailable(latestVersion))
            {
                notifyUser(latestVersion);
            }
        }
        catch (System::Net::WebException^)
        {
            notifyNetworkError();
        }
    }

    bool VersionVerifier::isNewVersionAvailable(System::String^ latestVersion)
    {
        System::String^ currentVersion = getCurrentVersion();
        return System::String::Compare(currentVersion, latestVersion) < 0;
    }

    void VersionVerifier::notifyUser(System::String^ latestVersion)
    {
        // Invoke the UI thread to show the dialog
        System::Windows::Forms::Form^ mainForm = System::Windows::Forms::Application::OpenForms[0];
        if (mainForm->InvokeRequired)
        {
            mainForm->Invoke(gcnew System::Action<System::String^>(VersionVerifier::showUpdateDialog), latestVersion);
        }
        else
        {
            showUpdateDialog(latestVersion);
        }
    }

    void VersionVerifier::notifyNetworkError()
    {
        // Invoke the UI thread to show the network error dialog
        System::Windows::Forms::Form^ mainForm = System::Windows::Forms::Application::OpenForms[0];
        if (mainForm->InvokeRequired)
        {
            mainForm->Invoke(gcnew System::Action(VersionVerifier::showNetworkErrorDialog));
        }
        else
        {
            showNetworkErrorDialog();
        }
    }

    void VersionVerifier::showUpdateDialog(System::String^ latestVersion)
    {
        // Display a dialog to the user
        Form2 ^ form2 = gcnew Form2();
        form2->ShowDialog();
    }

    void VersionVerifier::showNetworkErrorDialog()
    {
        // Display a network error dialog to the user
        System::Windows::Forms::MessageBox::Show("Unable to check for updates. Please check your internet connection.", "Network Error", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error);
    }

    System::String^ VersionVerifier::getCurrentVersion()
    {
        // Retrieve the current version of the application
        return "3.2.0";
    }


}// end namespace SysnativeBSODApps