#include <string>
#include <vector>
#include "FindPaths.h"
#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for KDPaths
  /// </summary>
  public ref class KDPaths : public System::Windows::Forms::Form
  {
  public:
    KDPaths(void)
    {
      m_bLogging = false;
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

    void EnableLogging()
    {
      m_bLogging = true;
    }

    void SetKdPaths(cli::array<int>^ nAKdPaths, cli::array<System::String^>^ sAKdPaths)
    {
      if(sAKdPaths != nullptr && nAKdPaths != nullptr)
      {
        m_SAKdPaths = gcnew cli::array<System::String^>(sAKdPaths->Length);
        m_NAKdPaths = gcnew cli::array<int>(nAKdPaths->Length);
        sAKdPaths->CopyTo(m_SAKdPaths, 0);
        nAKdPaths->CopyTo(m_NAKdPaths, 0);
      }
      else
      {
        m_SAKdPaths = nullptr;
        m_NAKdPaths = nullptr;
      }
    }

	cli::array<System::String^>^ GetKdPaths()
    {
      return m_SAKdPaths;
    }

	cli::array<int>^ GetKdActive()
    {
      return m_NAKdPaths;
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~KDPaths()
    {
      if (components)
      {
        delete components;
      }
    }
  private: System::Windows::Forms::Label^  label12;
  protected: 
  private: System::Windows::Forms::ToolStripMenuItem^  autoFindKdexePathToolStripMenuItem;
  private: System::Windows::Forms::Panel^  panel1;
  private: System::Windows::Forms::Button^  button2;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::Label^  label1;
  private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
  private: System::Windows::Forms::Button^  button10;
  private: System::Windows::Forms::ComboBox^  comboBox1;
  private: System::Windows::Forms::Button^  button8;
  private: System::Windows::Forms::MenuStrip^  menuStrip1;

  protected: 

  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;
    FindPaths^ m_fp;
	cli::array<System::String^>^ m_SAKdPaths;
	cli::array<int>^ m_NAKdPaths;
    bool m_bLogging;
    bool m_bFinished;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(KDPaths::typeid));
      this->label12 = (gcnew System::Windows::Forms::Label());
      this->autoFindKdexePathToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->panel1 = (gcnew System::Windows::Forms::Panel());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
      this->button10 = (gcnew System::Windows::Forms::Button());
      this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
      this->button8 = (gcnew System::Windows::Forms::Button());
      this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
      this->menuStrip1->SuspendLayout();
      this->SuspendLayout();
      // 
      // label12
      // 
      this->label12->AutoSize = true;
      this->label12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label12->ForeColor = System::Drawing::SystemColors::GrayText;
      this->label12->Location = System::Drawing::Point(11, 135);
      this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label12->Name = L"label12";
      this->label12->Size = System::Drawing::Size(127, 13);
      this->label12->TabIndex = 57;
      this->label12->Text = L" � 2024 Mike Pryor";
      // 
      // autoFindKdexePathToolStripMenuItem
      // 
      this->autoFindKdexePathToolStripMenuItem->Name = L"autoFindKdexePathToolStripMenuItem";
      this->autoFindKdexePathToolStripMenuItem->Size = System::Drawing::Size(134, 20);
      this->autoFindKdexePathToolStripMenuItem->Text = L"Auto Find kd.exe Path";
      this->autoFindKdexePathToolStripMenuItem->Click += gcnew System::EventHandler(this, &KDPaths::autoFindKdexePathToolStripMenuItem_Click);
      // 
      // panel1
      // 
      this->panel1->BackColor = System::Drawing::SystemColors::MenuText;
      this->panel1->Location = System::Drawing::Point(5, 84);
      this->panel1->Name = L"panel1";
      this->panel1->Size = System::Drawing::Size(670, 1);
      this->panel1->TabIndex = 56;
      // 
      // button2
      // 
      this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->button2->Location = System::Drawing::Point(563, 98);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(104, 32);
      this->button2->TabIndex = 55;
      this->button2->Text = L"Cancel";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &KDPaths::button2_Click);
      // 
      // button1
      // 
      this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->button1->Location = System::Drawing::Point(453, 98);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(104, 32);
      this->button1->TabIndex = 54;
      this->button1->Text = L"OK";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &KDPaths::button1_Click);
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label1->Location = System::Drawing::Point(199, 5);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(92, 20);
      this->label1->TabIndex = 53;
      this->label1->Text = L"kd.exe Path";
      // 
      // button10
      // 
      this->button10->Location = System::Drawing::Point(576, 35);
      this->button10->Name = L"button10";
      this->button10->Size = System::Drawing::Size(91, 23);
      this->button10->TabIndex = 52;
      this->button10->Text = L"Clear List";
      this->button10->UseVisualStyleBackColor = true;
      this->button10->Click += gcnew System::EventHandler(this, &KDPaths::button10_Click);
      // 
      // comboBox1
      // 
      this->comboBox1->DropDownWidth = 1024;
      this->comboBox1->FormattingEnabled = true;
      this->comboBox1->Location = System::Drawing::Point(12, 35);
      this->comboBox1->Name = L"comboBox1";
      this->comboBox1->Size = System::Drawing::Size(507, 21);
      this->comboBox1->TabIndex = 51;
      // 
      // button8
      // 
      this->button8->Location = System::Drawing::Point(525, 35);
      this->button8->Name = L"button8";
      this->button8->Size = System::Drawing::Size(40, 23);
      this->button8->TabIndex = 50;
      this->button8->Text = L"...";
      this->button8->UseVisualStyleBackColor = true;
      this->button8->Click += gcnew System::EventHandler(this, &KDPaths::button8_Click);
      // 
      // menuStrip1
      // 
      this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->autoFindKdexePathToolStripMenuItem});
      this->menuStrip1->Location = System::Drawing::Point(0, 0);
      this->menuStrip1->Name = L"menuStrip1";
      this->menuStrip1->Size = System::Drawing::Size(680, 24);
      this->menuStrip1->TabIndex = 58;
      this->menuStrip1->Text = L"menuStrip1";
      //
      // backgroundWorker1
      //
      this->backgroundWorker1->WorkerReportsProgress = true;
      this->backgroundWorker1->WorkerSupportsCancellation = true;
      this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &KDPaths::backgroundWorker1_DoWork);
      // 
      // KDPaths
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(680, 156);
      this->Controls->Add(this->label12);
      this->Controls->Add(this->panel1);
      this->Controls->Add(this->button2);
      this->Controls->Add(this->button1);
      this->Controls->Add(this->label1);
      this->Controls->Add(this->button10);
      this->Controls->Add(this->comboBox1);
      this->Controls->Add(this->button8);
      this->Controls->Add(this->menuStrip1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Name = L"KDPaths";
      this->Text = L"KDPaths";
      this->Load += gcnew System::EventHandler(this, &KDPaths::KDPaths_Load);
      this->menuStrip1->ResumeLayout(false);
      this->menuStrip1->PerformLayout();
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
private: System::Void KDPaths_Load(System::Object^  sender, System::EventArgs^  e);
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void autoFindKdexePathToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
private: System::Void KDPaths_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e);
  };
}
