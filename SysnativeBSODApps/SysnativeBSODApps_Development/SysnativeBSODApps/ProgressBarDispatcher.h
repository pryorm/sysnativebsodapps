#pragma once

#include <atomic>

namespace SysnativeBSODApps
{

class ProgressBarDispatcher
{
public:
    ProgressBarDispatcher(const ProgressBarDispatcher&) = delete;
    ProgressBarDispatcher& operator= (const ProgressBarDispatcher&) = delete;
    static ProgressBarDispatcher& getInstance();
    void openProgressBar();
private:
    ProgressBarDispatcher();
    ~ProgressBarDispatcher();
    std::atomic<bool> mProgressActive;
};
}// end namespace SysnativeBSODApps