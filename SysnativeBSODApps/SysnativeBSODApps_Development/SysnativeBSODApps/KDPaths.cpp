#include "KDPaths.h"

#include "SystemStandardConversionUtil.h"

#include <string>
#include <fstream>
#include <sstream>
#include <Windows.h>

using namespace SysnativeBSODApps;

// Load kdPaths.dzdn to get previously used kd.exe paths
System::Void KDPaths::KDPaths_Load(System::Object^  sender, System::EventArgs^  e)
{ 
  TopMost = true;
  Hide();
  UpdateZOrder();
  BringToFront();
  SetForegroundWindow(static_cast<HWND>(this->Handle.ToPointer()));
  Show();
  TopMost = false;
  std::string dbugDir = "";
  if(m_SAKdPaths == nullptr || m_NAKdPaths == nullptr)
  {
    return;
  }
  else
  {
    for(int i = 0; i < m_SAKdPaths->Length; ++i)
    {
      if(m_NAKdPaths[i] == 1)
      {
        dbugDir = SystemStandardConversionUtil::convertSystemStringToStdString(m_SAKdPaths[i]);
      }
    }
  }
  std::ifstream inDbugDir(dbugDir);

  m_bFinished = false;
  System::String^ selectedKd;

  for(int i=0; i<m_SAKdPaths->Length; i++)
  {
    System::String^ str2;
    if(m_SAKdPaths[i]->Length == 0)
    {
        break;
    }
    str2 = L"\"" + m_SAKdPaths[i] + L"\"";
    int seenThis = 0;
    if(str2->Length > 0)
    {
        for(int j=0; j<comboBox1->Items->Count; j++){
          if(str2 == comboBox1->Items[j]->ToString()){
            seenThis++;
          }
        }
        if(!seenThis){
          if(m_NAKdPaths[i] == 1)
          {
            selectedKd = str2;
          }
          comboBox1->Items->Add(str2);
        }
    }
  }
  comboBox1->SelectedIndex = comboBox1->FindStringExact(selectedKd);
  if (comboBox1->SelectedIndex < 0 && comboBox1->Items->Count > 0)
  {
    comboBox1->SelectedIndex = comboBox1->Items->Count - 1;
  }
}

// Browse for kd.exe and add to comboBox
System::Void KDPaths::button8_Click(System::Object^ sender, System::EventArgs^ e)
{
  bool seenThis = false;
  for(int i=0; i<comboBox1->Items->Count; i++)
  {
    if(comboBox1->Items[i]->ToString() == comboBox1->Text)
    {
      seenThis = true;
    }
  }
  if(!seenThis && comboBox1->Text->Length > 5)
  {
    comboBox1->Items->Add(comboBox1->Text);
    comboBox1->SelectedIndex = comboBox1->Items->Count - 1;
  }
  System::IO::Stream^ myStream;
  System::Windows::Forms::OpenFileDialog^ openFileDialog1 = gcnew System::Windows::Forms::OpenFileDialog;
  openFileDialog1->InitialDirectory = "C:\\Program Files (x86)\\";
  openFileDialog1->Filter = "All files (*.*)|*.*|exe files (kd.exe)|kd.exe";
  openFileDialog1->FilterIndex = 2;
  openFileDialog1->RestoreDirectory = true;
  if ( openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK )
  {
    if ( (myStream = openFileDialog1->OpenFile()) != nullptr )
    {
      using namespace System::Runtime::InteropServices;
      System::String^ s = openFileDialog1->FileName;
      const char* chars = 
      (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
	  std::string os = chars;
      os = "\"" + os + "\"";
      s = gcnew System::String(os.c_str());
      Marshal::FreeHGlobal(System::IntPtr((void*)chars));
      myStream->Close();
      int seenThis = 0;
      for(int i=0; i<comboBox1->Items->Count; i++){
        if(s == comboBox1->Items[i]->ToString()){
          seenThis++;
        }
      }
      if(os.length() > 0 && !seenThis){        
        comboBox1->Items->Add(s);
        comboBox1->SelectedIndex = comboBox1->FindStringExact(s);
      }
    }
  }
}

// Clear kd.exe comboBox
System::Void KDPaths::button10_Click(System::Object^ sender, System::EventArgs^ e)
{
  comboBox1->Items->Clear();
  comboBox1->Text = L"";
} 

// Cancel
System::Void KDPaths::button2_Click(System::Object^  sender, System::EventArgs^  e)
{
  comboBox1->Items->Clear();
  comboBox1->Text = L"";
  m_bFinished = true;
  Close();
}

// OK / Save
System::Void KDPaths::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
  bool seenThis = false;
  for(int i=0; i<comboBox1->Items->Count; i++)
  {
    if(comboBox1->Items[i]->ToString() == comboBox1->Text)
    {
      seenThis = true;
    }
  }
  if(!seenThis && comboBox1->Text->Length > 5)
  {
        if (!comboBox1->Text->Replace("\"", "")->ToLower()->Contains("kd.exe"))
        {    
          System::String^ str2 = L"No valid kd.exe path found ...\nMake sure the path is fully qualified: e.g.\n"
            + "C:\\Program Files (x86)\\Windows Kits\\10\\Debuggers\\x64\\kd.exe\n\n" + 
            "Please add a valid kd.exe path or use the Auto-Finding Tool for help ...";
          if(System::Windows::Forms::MessageBox::Show(str2,L"Retry?", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error,
			  System::Windows::Forms::MessageBoxDefaultButton::Button1) == System::Windows::Forms::DialogResult::OK)
          {
          }
          return;
        }
        if (!System::IO::File::Exists(comboBox1->Text->Replace("\"", "")))
        {    
          System::String^ str2 = L"No valid kd.exe path found ...\nMake sure the file exists:\n\n" + 
            "Please add a valid kd.exe path or use the Auto-Finding Tool for help ...";
          if(System::Windows::Forms::MessageBox::Show(str2,L"Retry?", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error,
			  System::Windows::Forms::MessageBoxDefaultButton::Button1) == System::Windows::Forms::DialogResult::OK)
          {
          }
          return;
        }
    comboBox1->Items->Add(comboBox1->Text);
    comboBox1->SelectedIndex = comboBox1->Items->Count - 1;
  }
  if (comboBox1->SelectedIndex < 0 && comboBox1->Items->Count > 0)
  {
    comboBox1->SelectedIndex = comboBox1->Items->Count - 1;
  }
  int selectedIndex = comboBox1->SelectedIndex;
  m_SAKdPaths = gcnew array<System::String^>(comboBox1->Items->Count);
  m_NAKdPaths = gcnew array<int>(comboBox1->Items->Count);
  for(int i=0; i<comboBox1->Items->Count; i++)
  {
    std::string s = SystemStandardConversionUtil::convertSystemStringToStdString(comboBox1->Items[i]->ToString());
	std::stringstream ss;
    for(int j = 0; j < int(s.length()); j++)
    {
      if(s.c_str()[j] != '\"')
      {
        ss << s.c_str()[j];
      }
    }
    s = ss.str();
    m_SAKdPaths[i] = gcnew System::String(s.c_str());
    
    int tf = 0;
    m_NAKdPaths[i] = 0;
    if(i == selectedIndex)
    {
      tf = 1;
      m_NAKdPaths[i] = 1;
    }
  }
  m_bFinished = true;
  Close();
}

System::Void KDPaths::autoFindKdexePathToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  m_fp = gcnew FindPaths();
  m_fp->m_bFinished = false;
  backgroundWorker1->RunWorkerAsync();
  m_fp->ShowDialog();
}
System::Void KDPaths::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
  while(!m_fp->m_bFinished)
  {
    Sleep(105);
  }
  if(m_fp->m_SAKdPaths != nullptr && m_fp->m_NAKdPaths != nullptr)
  {
    m_SAKdPaths = gcnew array<System::String^>(m_fp->m_SAKdPaths->Length);
    m_NAKdPaths = gcnew array<int>(m_fp->m_NAKdPaths->Length);
    m_fp->m_SAKdPaths->CopyTo(m_SAKdPaths, 0);
    m_fp->m_NAKdPaths->CopyTo(m_NAKdPaths, 0);
  }
  else
  {
    return;
  }
  System::String^ selectedKd;
  comboBox1->Items->Clear();
  comboBox1->Text = L"";
  for(int i=0; i<m_SAKdPaths->Length; i++)
  {
    System::String^ str2;
    str2 = L"\"" + m_SAKdPaths[i] + L"\"";
    int seenThis = 0;
    if(str2->Length > 0)
    {
      for(int j=0; j<comboBox1->Items->Count; j++){
        if(str2 == comboBox1->Items[j]->ToString()){
          seenThis++;
        }
      }
      if(!seenThis){
        if(m_NAKdPaths[i] == 1)
        {
          selectedKd = str2;
        }
        comboBox1->Items->Add(str2);
      }
    }
  }
  comboBox1->SelectedIndex = comboBox1->FindStringExact(selectedKd);
  // this->Text = selectedKd + L" " + m_NAKdPaths[comboBox1->SelectedIndex]->ToString();
}

System::Void KDPaths::KDPaths_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e)
{
  m_bFinished = true;
  button2_Click(sender, e);
}