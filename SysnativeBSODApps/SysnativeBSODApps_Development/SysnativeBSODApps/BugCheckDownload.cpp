#include "BugCheckDownload.h"
#include "ConfigurationUtil.h"
#include "StringUtil.h"
#include "FileSystemUtil.h"
#include <UrlMon.h>      //For URL Download function
#include <fstream>
#include <sstream>
#include <vector>
#include <tchar.h>
#include "wininet.h" // for clearing URL cache DeleteUrlCacheEntry

#include "debug.h"

#pragma comment(lib, "wininet.lib") // for clearing URL cache DeleteUrlCacheEntry
#pragma comment(lib, "urlmon.lib")

BugCheckDownload::BugCheckDownload(){
    tempString_s = ConfigurationUtil::getTempDirectory();
}

BugCheckDownload::~BugCheckDownload(){
}

void BugCheckDownload::downloadBugCheckInfo(){
  std::string tempDir = tempString_s;
  std::string userProfile = ConfigurationUtil::getUserProfilePath();

  std::string tempPath = tempDir + "\\bugCheckInfo.html";
  TCHAR *Lparam=new TCHAR[tempPath.size()+1];
  Lparam[tempPath.size()]=0;
  //As much as we'd love to, we can't use memcpy() because
  //sizeof(TCHAR)==sizeof(char) may not be true:
  copy(tempPath.begin(),tempPath.end(),Lparam);
  DeleteUrlCacheEntry (_T("https://learn.microsoft.com/en-us/windows-hardware/drivers/debugger/bug-check-code-reference2")); 
  HRESULT hr = URLDownloadToFile ( NULL, _T("https://learn.microsoft.com/en-us/windows-hardware/drivers/debugger/bug-check-code-reference2"), _T(param), 0, NULL );
  if(int(hr))
  {
    FileSystemUtil::copyFile(userProfile + "\\SysnativeBSODApps\\download\\bugCheckInfo.html.bak", tempDir + "\\bugCheckInfo.html", true);
  }
  else
  {
    FileSystemUtil::copyFile(tempDir + "\\bugCheckInfo.html", userProfile + "\\SysnativeBSODApps\\download\\bugCheckInfo.html.bak", true);
  }
  delete[] Lparam;
}

void BugCheckDownload::parseBugCheckInfo(){
    std::string tempDir = tempString_s;
    std::string userProfile = ConfigurationUtil::getUserProfilePath();
    std::ofstream outNewDebug;

    std::ifstream infile(tempDir + "\\bugCheckInfo.html");
    std::string line;
    bool startBugCheckCodes = false;
    // Read through the file to find <h2 id="bug-check-codes">Bug Check Codes</h2>
    while (std::getline(infile, line))
    {
        if (StringUtil::contains(line, "<h2 id=\"bug-check-codes\">Bug Check Codes</h2>"))
        {
            startBugCheckCodes = true;
        }
        // Get the href link and add it to the bugCheckLink vector and the bugcheck code to the bugCheckCode vector
        //   bugcheck codes are in the hyperlink text, e.g. <td><a href="bug-check-0x1--apc-index-mismatch"> has a bugcheck code of 0x1
        if (startBugCheckCodes && StringUtil::contains(line, "<td><a href=\""))
        {
            std::string baseUrlString = "https://learn.microsoft.com/en-us/windows-hardware/drivers/debugger/";
            bugCheckLink.push_back(baseUrlString + StringUtil::trim(StringUtil::split(StringUtil::split(line, "<td><a href=\"")[1], "\" data-linktype=\"relative-path\">")[0]));
            bugCheckCode.push_back(StringUtil::split(StringUtil::split(line, "<td><a href=\"bug-check-")[1], "--")[0]);
        }
    }
    infile.close();
}