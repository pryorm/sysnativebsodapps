#include "KDPaths.h"
#include "ParallelThreading.h"
#include "SetDirectorySysnativeResults.h"

#include "debug.h"

#pragma once

namespace SysnativeBSODApps {
class ProgressBarDispatcher;

public ref class ManagedProgressBarDispatcherWrapper
{
public:
    ManagedProgressBarDispatcherWrapper();
    void openProgressBar();

private:
    ProgressBarDispatcher* mDispatcher;
};

  /// <summary>
  /// Summary for Form1
  /// </summary>
  public ref class Form1 : public System::Windows::Forms::Form
  {
  public:
  private: System::Windows::Forms::ToolStripMenuItem^  changeThreadsToolStripMenuItem;
  private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
  private: System::Windows::Forms::ToolStripMenuItem^  sysnativeResultsDIrectoryToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  setDirectoryToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  loggingToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  closeAppsWhenFinishedToolStripMenuItem;
  private: System::Windows::Forms::Button^  button11;
  private: System::Windows::Forms::Button^  button12;
  private: System::Windows::Forms::ToolStripMenuItem^  windowsToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  kdexePathsToolStripMenuItem;
  public:
    Form1();
    void setLoggingChecked(const bool loggingChecked);
    bool getLoggingChecked();
    cli::array<System::String^>^ getFileList();
    bool getParallelChecked();
    int getDispatcherParallelThreads();
    int getDispatcherUserThreads();
    bool getSysnativeResultsChecked();
    System::String^ getSysnativeResultsBuilderSysnativeResults();
    cli::array<System::String^>^ getSavedKdPaths();
    cli::array<int>^ getActiveKdPaths();

protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~Form1();
  private: System::Windows::Forms::TabControl^  tabControl1;
  protected:
    System::String^ mUsername;
    System::String^ mOutputDir;
    System::String^ mTimeDir;
    bool mFullGui;
    bool mCloseWhenFinished;
    bool mTopMost;
    ManagedProgressBarDispatcherWrapper^ mProgressBarDispatcher;

  private: System::Windows::Forms::TabPage^  tabPage2;
  private: System::Windows::Forms::RichTextBox^  richTextBox1;
  private: System::Windows::Forms::Label^  label13;
  private: System::Windows::Forms::TabPage^  tabPage5;
  private: System::Windows::Forms::RichTextBox^  richTextBox2;
  private: System::Windows::Forms::Label^  label14;
  private: System::Windows::Forms::TabPage^  tabPage6;
  private: System::Windows::Forms::RichTextBox^  richTextBox3;
  private: System::Windows::Forms::Label^  label15;
  private: System::Windows::Forms::TabPage^  tabPage7;
  private: System::Windows::Forms::RichTextBox^  richTextBox4;
  private: System::Windows::Forms::Label^  label16;
  private: System::Windows::Forms::TabPage^  tabPage8;
  private: System::Windows::Forms::TextBox^  textBox13;
  private: System::Windows::Forms::TextBox^  textBox12;
  private: System::Windows::Forms::Label^  label18;
  private: System::Windows::Forms::Label^  label17;
  private: System::Windows::Forms::TabPage^  tabPage9;
  private: System::Windows::Forms::RichTextBox^  richTextBox5;
  private: System::Windows::Forms::MenuStrip^  menuStrip1;
  private: System::Windows::Forms::ToolStripMenuItem^  pressF1ForHelpToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  loadToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  saveAsToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  loadPreviousToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  saveAndRunToolStripMenuItem1;
  private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  quickSaveToolStripMenuItem;
  private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
  private: System::Windows::Forms::TabPage^  tabPage10;
  private: System::Windows::Forms::RichTextBox^  richTextBox7;
  private: System::Windows::Forms::RichTextBox^  richTextBox6;
  private: System::Windows::Forms::Label^  label24;
  private: System::Windows::Forms::Label^  label23;
  private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
  private: System::Windows::Forms::TabPage^  tabPage11;
  private: System::Windows::Forms::Label^  label25;
  private: System::Windows::Forms::RichTextBox^  richTextBox8;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::TabPage^  tabPage12;
  private: System::Windows::Forms::CheckBox^  checkBox8;
  private: System::Windows::Forms::Label^  label26;
  private: System::Windows::Forms::RichTextBox^  richTextBox9;
  private: System::Windows::Forms::CheckBox^  checkBox9;
  private: System::Windows::Forms::TextBox^  textBox4;
  private: System::Windows::Forms::Label^  label27;
  private: System::Windows::Forms::TextBox^  textBox11;
  private: System::Windows::Forms::Label^  label28;
  private: System::Windows::Forms::Label^  label29;
  private: System::Windows::Forms::Button^  button2;
  private: System::Windows::Forms::Button^  button3;
  private: System::Windows::Forms::CheckBox^  checkBox10;
  private: System::Windows::Forms::CheckBox^  checkBox24;
  private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  alwaysOnTopToolStripMenuItem;
  private: System::Windows::Forms::Label^  label45;
  private: System::Windows::Forms::RichTextBox^  richTextBox13;
  private: System::Windows::Forms::Label^  label44;
  private: System::Windows::Forms::RichTextBox^  richTextBox12;
  private: System::Windows::Forms::Label^  label43;
  private: System::Windows::Forms::RichTextBox^  richTextBox11;
  private: System::Windows::Forms::Label^  label42;
  private: System::Windows::Forms::RichTextBox^  richTextBox10;
  private: System::Windows::Forms::Button^  button4;
  private: System::Windows::Forms::Button^  button5;
  private: System::Windows::Forms::CheckBox^  checkBox16;
  private: System::Windows::Forms::CheckBox^  checkBox15;
  private: System::Windows::Forms::CheckBox^  checkBox14;
  private: System::Windows::Forms::CheckBox^  checkBox13;
  private: System::Windows::Forms::TextBox^  textBox25;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
  private: System::Windows::Forms::CheckBox^  checkBox12;
  private: System::Windows::Forms::CheckBox^  checkBox11;
  private: System::Windows::Forms::TextBox^  textBox24;
  private: System::Windows::Forms::Button^  button6;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown2;
  private: System::Windows::Forms::CheckBox^  checkBox17;
  private: System::Windows::Forms::CheckBox^  checkBox18;
  private: System::Windows::Forms::TextBox^  textBox26;
  private: System::Windows::Forms::Button^  button7;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown3;
  private: System::Windows::Forms::CheckBox^  checkBox19;
  private: System::Windows::Forms::CheckBox^  checkBox20;
  private: System::Windows::Forms::TextBox^  textBox27;
  private: System::Windows::Forms::CheckBox^  checkBox21;
  private: System::Windows::Forms::CheckBox^  checkBox22;
  private: System::Windows::Forms::CheckBox^  checkBox23;
  private: System::Windows::Forms::Label^  label46;
  private: System::Windows::Forms::TabPage^  tabPage13;
  private: System::Windows::Forms::TabPage^  tabPage1;
  private: System::Windows::Forms::CheckBox^  checkBox7;
  private: System::Windows::Forms::CheckBox^  checkBox6;
  private: System::Windows::Forms::CheckBox^  checkBox5;
  private: System::Windows::Forms::CheckBox^  checkBox4;
  private: System::Windows::Forms::CheckBox^  checkBox3;
  private: System::Windows::Forms::CheckBox^  checkBox2;
  private: System::Windows::Forms::CheckBox^  checkBox1;
  private: System::Windows::Forms::TextBox^  textBox10;
  private: System::Windows::Forms::Label^  label22;
  //private: System::Windows::Forms::TabControl^  tabControl2;
  private: System::Windows::Forms::TabPage^  tabPage3;
  private: System::Windows::Forms::TabPage^  tabPage4;
  private: System::Windows::Forms::Label^  label9;
  private: System::Windows::Forms::TextBox^  textBox3;
  private: System::Windows::Forms::Label^  label7;
  private: System::Windows::Forms::Label^  label3;
  private: System::Windows::Forms::CheckedListBox^  checkedListBox3;
  private: System::Windows::Forms::Label^  label2;
  private: System::Windows::Forms::Label^  label1;
  private: System::Windows::Forms::CheckedListBox^  checkedListBox2;
  private: System::Windows::Forms::CheckedListBox^  checkedListBox1;
  private: System::Windows::Forms::Label^  label34;
  private: System::Windows::Forms::Label^  label35;
  private: System::Windows::Forms::Label^  label36;
  private: System::Windows::Forms::TextBox^  textBox18;
  private: System::Windows::Forms::TextBox^  textBox19;
  private: System::Windows::Forms::TextBox^  textBox20;
  private: System::Windows::Forms::Label^  label37;
  private: System::Windows::Forms::Label^  label38;
  private: System::Windows::Forms::Label^  label39;
  private: System::Windows::Forms::Label^  label40;
  private: System::Windows::Forms::TextBox^  textBox21;
  private: System::Windows::Forms::TextBox^  textBox22;
  private: System::Windows::Forms::TextBox^  textBox23;
  private: System::Windows::Forms::Label^  label41;
  private: System::Windows::Forms::Label^  label30;
  private: System::Windows::Forms::Label^  label31;
  private: System::Windows::Forms::Label^  label32;
  private: System::Windows::Forms::TextBox^  textBox15;
  private: System::Windows::Forms::TextBox^  textBox16;
  private: System::Windows::Forms::TextBox^  textBox17;
  private: System::Windows::Forms::Label^  label33;
  private: System::Windows::Forms::MenuStrip^  menuStrip2;
  private: System::Windows::Forms::ToolStripMenuItem^  xPDatesToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMAugToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP1Sep92002ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP2Aug252004ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP2bAug2006ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP2cAug102007ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP3Apr212008RTMToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  vistaDatesToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMNov82007ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  feb42008ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP2Apr282009RTMToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  datesToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMJul222009ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP1Feb112011ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  datesToolStripMenuItem1;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMAugust12012ToolStripMenuItem;
  private: System::Windows::Forms::Label^  label21;
  private: System::Windows::Forms::Label^  label20;
  private: System::Windows::Forms::Label^  label19;
  private: System::Windows::Forms::TextBox^  textBox9;
  private: System::Windows::Forms::TextBox^  textBox8;
  private: System::Windows::Forms::TextBox^  textBox14;
  private: System::Windows::Forms::Label^  label8;
  private: System::Windows::Forms::TextBox^  textBox5;
  private: System::Windows::Forms::Label^  label10;
  private: System::Windows::Forms::TextBox^  textBox6;
  private: System::Windows::Forms::Label^  label11;
  private: System::Windows::Forms::TextBox^  textBox2;
  private: System::Windows::Forms::Label^  label6;
  private: System::Windows::Forms::TextBox^  textBox1;
  private: System::Windows::Forms::Label^  label5;
  private: System::Windows::Forms::Label^  label4;
  private: System::Windows::Forms::Label^  label12;
  private: System::Windows::Forms::TextBox^  textBox28;
  private: System::Windows::Forms::Label^  label47;
  private: System::Windows::Forms::TextBox^  textBox29;
  private: System::Windows::Forms::Label^  label48;
  private: System::Windows::Forms::Label^  label49;
  //private: System::Windows::Forms::Button^  button8;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown4;
  private: System::Windows::Forms::Label^  label50;
  private: System::Windows::Forms::Button^  button9;
  //private: System::Windows::Forms::ComboBox^  comboBox1;
  //private: System::Windows::Forms::Button^  button10;
  private: System::Windows::Forms::ToolStripMenuItem^  useParallelThreadingToolStripMenuItem;
  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container^ mComponents;
    int m_nSavedED;
    KDPaths^ mKdPaths;
    cli::array<System::String^>^ mSavedKdPaths;
    cli::array<System::String^>^ mDriverNames;
    cli::array<System::String^>^ mDriverDates;
    cli::array<int>^ mActiveKdPaths;
    System::String^ mForumName;
    FindPaths^ mFindPaths;
    ParallelThreading^ mDispatcher;
    SetDirectorySysnativeResults^ mSysnativeResultsBuilder;
    bool mParallelChecked;
    int mDefaultThreads;
    int mUserThreads;
    bool mSysnativeResultsChecked;
    bool mLoggingChecked;
    cli::array<System::String^>^ mKdPathsUnknown;
    cli::array<System::String^>^ mFileList;
    System::String^ mSysnativeResults;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent();
#pragma endregion
  public:  System::Void centerForm1();
  private: System::Void openForumFile(std::string os);
  private: System::Void open(std::string os);
  private: System::Void Help(System::Object^  sender, System::EventArgs^  e);
    bool unSignedCompare(std::string str1, std::string str2);
  private: System::Void loadPreviousSettings2(System::Object^  sender, System::EventArgs^  e);
  private: System::Void loadPreviousSettings(System::Object^  sender, System::EventArgs^  e);
  private: System::Void saveAndRunTheApps(System::Object^ sender, System::EventArgs^ e);
  private: System::Void saveAndRunTheApps2(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox1Change(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox2Change(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox3Change(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox4Change(System::Object^  sender, System::EventArgs^  e);
  private: System::Void runTheApps(System::Object^  sender, System::EventArgs^  e);
  private: System::Void runTheApps2(System::Object^  sender, System::EventArgs^  e);
  private: System::Void Open(System::Object^  sender, System::EventArgs^  e);
  private: System::Void saveAs(System::Object^  sender, System::EventArgs^  e);
  private: System::Void saveBackup(std::string os);
  private: System::Void saveOnly(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox5_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox6_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox7_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkList1Change(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkList2Change(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkList3Change(System::Object^  sender, System::EventArgs^  e);
  private: System::Void resize(System::Object^  sender, System::EventArgs^  e);
  private: System::Void XPRTM(System::Object^  sender, System::EventArgs^  e);
  private: System::Void XPSP1(System::Object^  sender, System::EventArgs^  e);
  private: System::Void XPSP2(System::Object^  sender, System::EventArgs^  e);
  private: System::Void XPSP2b(System::Object^  sender, System::EventArgs^  e);
  private: System::Void XPSP2c(System::Object^  sender, System::EventArgs^  e);
  private: System::Void XPSP3(System::Object^  sender, System::EventArgs^  e);
  private: System::Void VistaRTM(System::Object^  sender, System::EventArgs^  e);
  private: System::Void VistaSP1(System::Object^  sender, System::EventArgs^  e);
  private: System::Void VistaSP2(System::Object^  sender, System::EventArgs^  e);
  private: System::Void SevenRTM(System::Object^  sender, System::EventArgs^  e);
  private: System::Void SevenSP1(System::Object^  sender, System::EventArgs^  e);
  private: System::Void EightRTM(System::Object^  sender, System::EventArgs^  e);
  private: System::Void loadForm1(System::Object^  sender, System::EventArgs^  e);
  private: System::Void loadTabControl1(System::Object^  sender, System::EventArgs^  e);
  private: System::Void onChecked(System::Object^  sender, System::EventArgs^  e);
  private: System::Void Exit(System::Object^  sender, System::EventArgs^  e);
  private: System::Void clearDriverStatistics(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox8_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox9_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox11_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox12_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox21_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox18_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox17_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox22_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
  private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox20_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox19_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void checkBox23_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
  //private: System::Void browse(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e);
  //private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
  //private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void useParallelThreadingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
  private: System::Void sysnativeResultsDIrectoryToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
  private: void setupFirstScreen();
  private: void deleteNewOSes(int num);
  private: void checkForKD(System::ComponentModel::BackgroundWorker^ worker);
  private: System::Void setDirectoryToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void loggingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void onChecked2(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button11_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void kdexePathsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void refillExcludedDrivers();
  private: bool CheckDebugFile();
  private:
    System::Void UpdateFirstScreenUI();
    System::Void UpdateWorker1UI();
    System::Void hideControlsForKdSearch();
    System::Void showControlsAfterKdSearch();
    System::Void SafeInvoke(Control^ control, System::Action^ action);
  };
}

