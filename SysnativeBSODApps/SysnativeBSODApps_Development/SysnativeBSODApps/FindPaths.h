#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for FindPaths
  /// </summary>
  public ref class FindPaths : public System::Windows::Forms::Form
  {
  private:
    int foundPaths;
    int highestPercentageReached;
    int maxVal;
    int closeForm;
  public:
    bool m_bFinished;
	cli::array<System::String^>^ m_SAKdPaths;
	cli::array<int>^ m_NAKdPaths;
    static int onTop = 0;
    FindPaths(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~FindPaths()
    {
      if (components)
      {
        delete components;
      }
    }

  protected: 
    System::String^ mDirectoryToSearch;
    cli::array<System::String^>^ mKdFiles;
  private: System::Windows::Forms::ListBox^  listBox1;
  private: System::Windows::Forms::TextBox^  textBox1;
  private: System::Windows::Forms::ComboBox^  comboBox1;
  private: System::Windows::Forms::Label^  label2;
  private: System::Windows::Forms::Label^  label1;


  private: System::Windows::Forms::Button^  button4;
  private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
  private: System::Windows::Forms::Label^  label3;
  private: System::Windows::Forms::Button^  button10;
  private: System::Windows::Forms::ComboBox^  comboBox2;
  private: System::Windows::Forms::Label^  label4;
  private: System::Windows::Forms::Panel^  panel1;
  private: System::Windows::Forms::Button^  button3;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::Button^  button2;
  private: System::Windows::Forms::ProgressBar^  progressBar1;


  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(FindPaths::typeid));
      this->listBox1 = (gcnew System::Windows::Forms::ListBox());
      this->textBox1 = (gcnew System::Windows::Forms::TextBox());
      this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->button4 = (gcnew System::Windows::Forms::Button());
      this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
      this->label3 = (gcnew System::Windows::Forms::Label());
      this->button10 = (gcnew System::Windows::Forms::Button());
      this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
      this->label4 = (gcnew System::Windows::Forms::Label());
      this->panel1 = (gcnew System::Windows::Forms::Panel());
      this->button3 = (gcnew System::Windows::Forms::Button());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
      this->SuspendLayout();
      // 
      // listBox1
      // 
      this->listBox1->FormattingEnabled = true;
      this->listBox1->Location = System::Drawing::Point(147, 12);
      this->listBox1->Name = L"listBox1";
      this->listBox1->Size = System::Drawing::Size(479, 147);
      this->listBox1->TabIndex = 6;
      this->listBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &FindPaths::listBox1_SelectedIndexChanged);
      // 
      // textBox1
      // 
      this->textBox1->Location = System::Drawing::Point(3, 44);
      this->textBox1->Name = L"textBox1";
      this->textBox1->ReadOnly = true;
      this->textBox1->Size = System::Drawing::Size(120, 20);
      this->textBox1->TabIndex = 9;
      // 
      // comboBox1
      // 
      this->comboBox1->FormattingEnabled = true;
      this->comboBox1->Location = System::Drawing::Point(3, 132);
      this->comboBox1->Name = L"comboBox1";
      this->comboBox1->Size = System::Drawing::Size(120, 21);
      this->comboBox1->TabIndex = 8;
      this->comboBox1->Text = L"C:\\";
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(3, 100);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(45, 13);
      this->label2->TabIndex = 7;
      this->label2->Text = L"Look in:";
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(3, 20);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(139, 13);
      this->label1->TabIndex = 11;
      this->label1->Text = L"Search for files that contain:";
      // 
      // button4
      // 
      this->button4->Location = System::Drawing::Point(427, 366);
      this->button4->Name = L"button4";
      this->button4->Size = System::Drawing::Size(102, 25);
      this->button4->TabIndex = 13;
      this->button4->Text = L"OK";
      this->button4->UseVisualStyleBackColor = true;
      this->button4->Click += gcnew System::EventHandler(this, &FindPaths::button4_Click);
      // 
      // backgroundWorker1
      // 
      this->backgroundWorker1->WorkerReportsProgress = true;
      this->backgroundWorker1->WorkerSupportsCancellation = true;
      this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &FindPaths::backgroundWorker1_DoWork);
      this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &FindPaths::backgroundWorker1_ProgressChanged);
      this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &FindPaths::backgroundWorker1_RunWorkerCompleted);
      // 
      // label3
      // 
      this->label3->AutoSize = true;
      this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label3->Location = System::Drawing::Point(110, 278);
      this->label3->Name = L"label3";
      this->label3->Size = System::Drawing::Size(92, 20);
      this->label3->TabIndex = 17;
      this->label3->Text = L"kd.exe Path";
      // 
      // button10
      // 
      this->button10->Location = System::Drawing::Point(535, 302);
      this->button10->Name = L"button10";
      this->button10->Size = System::Drawing::Size(91, 25);
      this->button10->TabIndex = 16;
      this->button10->Text = L"Clear List";
      this->button10->UseVisualStyleBackColor = true;
      this->button10->Click += gcnew System::EventHandler(this, &FindPaths::button10_Click);
      // 
      // comboBox2
      // 
      this->comboBox2->DropDownWidth = 1024;
      this->comboBox2->FormattingEnabled = true;
      this->comboBox2->Location = System::Drawing::Point(11, 305);
      this->comboBox2->Name = L"comboBox2";
      this->comboBox2->Size = System::Drawing::Size(507, 21);
      this->comboBox2->TabIndex = 15;
      // 
      // label4
      // 
      this->label4->AutoSize = true;
      this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label4->ForeColor = System::Drawing::Color::Blue;
      this->label4->Location = System::Drawing::Point(35, 253);
      this->label4->Name = L"label4";
      this->label4->Size = System::Drawing::Size(460, 16);
      this->label4->TabIndex = 18;
      this->label4->Text = L"Select Files from the List above to add them to the kd.exe path list";
      // 
      // panel1
      // 
      this->panel1->BackColor = System::Drawing::SystemColors::MenuText;
      this->panel1->Location = System::Drawing::Point(3, 241);
      this->panel1->Name = L"panel1";
      this->panel1->Size = System::Drawing::Size(640, 2);
      this->panel1->TabIndex = 19;
      // 
      // button3
      // 
      this->button3->Location = System::Drawing::Point(228, 203);
      this->button3->Name = L"button3";
      this->button3->Size = System::Drawing::Size(134, 25);
      this->button3->TabIndex = 21;
      this->button3->Text = L"Stop Search";
      this->button3->UseVisualStyleBackColor = true;
      this->button3->Click += gcnew System::EventHandler(this, &FindPaths::button3_Click);
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(147, 203);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(75, 25);
      this->button1->TabIndex = 20;
      this->button1->Text = L"&Search";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &FindPaths::button1_Click);
      // 
      // button2
      // 
      this->button2->Location = System::Drawing::Point(535, 366);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(102, 25);
      this->button2->TabIndex = 22;
      this->button2->Text = L"Cancel";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &FindPaths::button2_Click);
      // 
      // progressBar1
      // 
      this->progressBar1->Location = System::Drawing::Point(147, 171);
      this->progressBar1->Name = L"progressBar1";
      this->progressBar1->Size = System::Drawing::Size(479, 23);
      this->progressBar1->TabIndex = 23;
      // 
      // FindPaths
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(644, 404);
      this->Controls->Add(this->progressBar1);
      this->Controls->Add(this->button2);
      this->Controls->Add(this->button3);
      this->Controls->Add(this->button1);
      this->Controls->Add(this->panel1);
      this->Controls->Add(this->label4);
      this->Controls->Add(this->label3);
      this->Controls->Add(this->button10);
      this->Controls->Add(this->comboBox2);
      this->Controls->Add(this->button4);
      this->Controls->Add(this->listBox1);
      this->Controls->Add(this->textBox1);
      this->Controls->Add(this->comboBox1);
      this->Controls->Add(this->label2);
      this->Controls->Add(this->label1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Name = L"FindPaths";
      this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
      this->Text = L"Auto-Finding Tool";
      this->TopMost = true;
      this->Load += gcnew System::EventHandler(this, &FindPaths::FindPaths_Load);
      this->Shown += gcnew System::EventHandler(this, &FindPaths::FindPaths_Shown);
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
  private: System::Void FindPaths_Load(System::Object^  sender, System::EventArgs^  e);
  private: System::Void FindPaths_Shown(System::Object^  sender, System::EventArgs^  e);
  private: void DirSearch(System::String^ sDir, System::ComponentModel::BackgroundWorker^ worker);
  private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);  
  private: System::Void listBox1_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e);
  private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
  private: System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e);
  private: System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
  private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
  System::Void updateListBox1();
  System::Void SafeInvoke(Control^ control, System::Action^ action);
};
}
