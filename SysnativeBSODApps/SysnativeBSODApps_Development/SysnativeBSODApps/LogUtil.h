#pragma once

#include <string>

ref class LogUtil
{
public:
    static System::Threading::Tasks::Task^ logErrorAsync(System::String^ logMsg);
    static void logErrorAsync(const std::string& logMsg);
private:
    static void logError();
    static System::String^ mLogMsg;
};