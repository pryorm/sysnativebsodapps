#include "Progress.h"

#include "debug.h"

namespace SysnativeBSODApps
{
Progress::Progress()
: mErrorsProcessed(0)
{
    InitializeComponent();
    // Add the KeyDown event handler for the form
    this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Progress::Form_KeyDown);

    // Make sure the form can receive key events
    this->KeyPreview = true;
}

Progress::~Progress()
{
    if (mComponents)
    {
        delete mComponents;
    }
}

#pragma region Windows Form Designer generated code
// Boilerplate-heavy code
void Progress::InitializeComponent()
{
    System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(Progress::typeid));
    this->mProgressDisplay = (gcnew System::Windows::Forms::ProgressBar());
    this->mStatusDisplay = (gcnew System::Windows::Forms::Label());
    this->mBackgroundProgressUpdater = (gcnew System::ComponentModel::BackgroundWorker());
    this->mBackgroundKdOutputBuilder = (gcnew System::ComponentModel::BackgroundWorker());
    this->mDefaultKdDisplay = (gcnew System::Windows::Forms::TextBox());
    this->mShowDefaultKd = (gcnew System::Windows::Forms::CheckBox());
    this->mShowUserKd = (gcnew System::Windows::Forms::CheckBox());
    this->mCopyrightDisplay = (gcnew System::Windows::Forms::Label());
    this->mErrorDisplay = (gcnew System::Windows::Forms::TextBox());
    this->mUserKdDisplay = (gcnew System::Windows::Forms::TextBox());
    this->mNotifyDisplay = (gcnew System::Windows::Forms::Label());
    this->mMenu = (gcnew System::Windows::Forms::MenuStrip());
    this->mOptions = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->mAlwaysOnTop = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->mMenu->SuspendLayout();
    this->SuspendLayout();
    // 
    // mProgressDisplay
    // 
    this->mProgressDisplay->Location = System::Drawing::Point(32, 84);
    this->mProgressDisplay->Margin = System::Windows::Forms::Padding(4);
    this->mProgressDisplay->Name = L"mProgressDisplay";
    this->mProgressDisplay->Size = System::Drawing::Size(805, 28);
    this->mProgressDisplay->TabIndex = 0;
    // 
    // mStatusDisplay
    // 
    this->mStatusDisplay->AutoSize = true;
    this->mStatusDisplay->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->mStatusDisplay->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
    this->mStatusDisplay->Location = System::Drawing::Point(27, 55);
    this->mStatusDisplay->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
    this->mStatusDisplay->Name = L"mStatusDisplay";
    this->mStatusDisplay->Size = System::Drawing::Size(379, 25);
    this->mStatusDisplay->TabIndex = 1;
    this->mStatusDisplay->Text = L"Sysnative BSOD Processing Progress";
    this->mStatusDisplay->TextAlign = System::Drawing::ContentAlignment::TopCenter;
    // 
    // mBackgroundProgressUpdater
    // 
    this->mBackgroundProgressUpdater->WorkerReportsProgress = true;
    this->mBackgroundProgressUpdater->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Progress::updateProgress);
    this->mBackgroundProgressUpdater->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &Progress::progressChanged);
    // 
    // mBackgroundKdOutputBuilder
    // 
    this->mBackgroundKdOutputBuilder->WorkerReportsProgress = true;
    this->mBackgroundKdOutputBuilder->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Progress::buildKdOutput);
    // 
    // mDefaultKdDisplay
    // 
    this->mDefaultKdDisplay->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->mDefaultKdDisplay->Location = System::Drawing::Point(32, 331);
    this->mDefaultKdDisplay->Margin = System::Windows::Forms::Padding(4);
    this->mDefaultKdDisplay->Multiline = true;
    this->mDefaultKdDisplay->Name = L"mDefaultKdDisplay";
    this->mDefaultKdDisplay->ReadOnly = true;
    this->mDefaultKdDisplay->ScrollBars = System::Windows::Forms::ScrollBars::Both;
    this->mDefaultKdDisplay->Size = System::Drawing::Size(1207, 498);
    this->mDefaultKdDisplay->TabIndex = 3;
    this->mDefaultKdDisplay->Visible = false;
    this->mDefaultKdDisplay->WordWrap = false;
    // 
    // mShowDefaultKd
    // 
    this->mShowDefaultKd->AutoSize = true;
    this->mShowDefaultKd->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->mShowDefaultKd->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
    this->mShowDefaultKd->Location = System::Drawing::Point(32, 294);
    this->mShowDefaultKd->Margin = System::Windows::Forms::Padding(4);
    this->mShowDefaultKd->Name = L"mShowDefaultKd";
    this->mShowDefaultKd->Size = System::Drawing::Size(181, 29);
    this->mShowDefaultKd->TabIndex = 4;
    this->mShowDefaultKd->Text = L"View kd Output";
    this->mShowDefaultKd->UseVisualStyleBackColor = true;
    this->mShowDefaultKd->CheckedChanged += gcnew System::EventHandler(this, &Progress::showDefaultKdCheckedChanged);
    // 
    // mShowUserKd
    // 
    this->mShowUserKd->AutoSize = true;
    this->mShowUserKd->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->mShowUserKd->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
    this->mShowUserKd->Location = System::Drawing::Point(232, 294);
    this->mShowUserKd->Margin = System::Windows::Forms::Padding(4);
    this->mShowUserKd->Name = L"mShowUserKd";
    this->mShowUserKd->Size = System::Drawing::Size(232, 29);
    this->mShowUserKd->TabIndex = 5;
    this->mShowUserKd->Text = L"View User kd Output";
    this->mShowUserKd->UseVisualStyleBackColor = true;
    this->mShowUserKd->CheckedChanged += gcnew System::EventHandler(this, &Progress::showUserKdCheckedChanged);
    // 
    // mCopyrightDisplay
    // 
    this->mCopyrightDisplay->AutoSize = true;
    this->mCopyrightDisplay->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->mCopyrightDisplay->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
    this->mCopyrightDisplay->Location = System::Drawing::Point(28, 21);
    this->mCopyrightDisplay->Name = L"mCopyrightDisplay";
    this->mCopyrightDisplay->Size = System::Drawing::Size(160, 17);
    this->mCopyrightDisplay->TabIndex = 30;
    this->mCopyrightDisplay->Text = L" � 2024 Mike Pryor";
    // 
    // mErrorDisplay
    // 
    this->mErrorDisplay->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->mErrorDisplay->ForeColor = System::Drawing::Color::Red;
    this->mErrorDisplay->Location = System::Drawing::Point(31, 119);
    this->mErrorDisplay->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
    this->mErrorDisplay->Multiline = true;
    this->mErrorDisplay->Name = L"mErrorDisplay";
    this->mErrorDisplay->ScrollBars = System::Windows::Forms::ScrollBars::Both;
    this->mErrorDisplay->Size = System::Drawing::Size(807, 168);
    this->mErrorDisplay->TabIndex = 32;
    this->mErrorDisplay->WordWrap = false;
    // 
    // mUserKdDisplay
    // 
    this->mUserKdDisplay->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->mUserKdDisplay->Location = System::Drawing::Point(32, 331);
    this->mUserKdDisplay->Margin = System::Windows::Forms::Padding(4);
    this->mUserKdDisplay->Multiline = true;
    this->mUserKdDisplay->Name = L"mUserKdDisplay";
    this->mUserKdDisplay->ReadOnly = true;
    this->mUserKdDisplay->ScrollBars = System::Windows::Forms::ScrollBars::Both;
    this->mUserKdDisplay->Size = System::Drawing::Size(1207, 498);
    this->mUserKdDisplay->TabIndex = 35;
    this->mUserKdDisplay->Visible = false;
    this->mUserKdDisplay->WordWrap = false;
    // 
    // mNotifyDisplay
    // 
    this->mNotifyDisplay->AutoSize = true;
    this->mNotifyDisplay->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->mNotifyDisplay->ForeColor = System::Drawing::Color::Black;
    this->mNotifyDisplay->Location = System::Drawing::Point(487, 294);
    this->mNotifyDisplay->Name = L"mNotifyDisplay";
    this->mNotifyDisplay->Size = System::Drawing::Size(317, 25);
    this->mNotifyDisplay->TabIndex = 36;
    this->mNotifyDisplay->Text = L"(These may take time to load...)";
    // 
    // mMenu
    // 
    this->mMenu->Anchor = System::Windows::Forms::AnchorStyles::Top;
    this->mMenu->Dock = System::Windows::Forms::DockStyle::None;
    this->mMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->mOptions });
    this->mMenu->Location = System::Drawing::Point(241, 1);
    this->mMenu->Name = L"mMenu";
    this->mMenu->Size = System::Drawing::Size(173, 28);
    this->mMenu->TabIndex = 37;
    this->mMenu->Text = L"mMenu";
    // 
    // mOptions
    // 
    this->mOptions->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->mAlwaysOnTop });
    this->mOptions->Name = L"mOptions";
    this->mOptions->Size = System::Drawing::Size(73, 24);
    this->mOptions->Text = L"Options";
    // 
    // mAlwaysOnTop
    // 
    this->mAlwaysOnTop->Name = L"mAlwaysOnTop";
    this->mAlwaysOnTop->Size = System::Drawing::Size(175, 24);
    this->mAlwaysOnTop->Text = L"Always on Top";
    this->mAlwaysOnTop->Click += gcnew System::EventHandler(this, &Progress::alwaysOnTopClick);
    // 
    // Progress
    // 
    this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->AutoValidate = System::Windows::Forms::AutoValidate::EnablePreventFocusChange;
    this->BackColor = System::Drawing::SystemColors::AppWorkspace;
    this->ClientSize = System::Drawing::Size(865, 336);
    this->Controls->Add(this->mNotifyDisplay);
    this->Controls->Add(this->mUserKdDisplay);
    this->Controls->Add(this->mErrorDisplay);
    this->Controls->Add(this->mCopyrightDisplay);
    this->Controls->Add(this->mShowUserKd);
    this->Controls->Add(this->mShowDefaultKd);
    this->Controls->Add(this->mDefaultKdDisplay);
    this->Controls->Add(this->mStatusDisplay);
    this->Controls->Add(this->mProgressDisplay);
    this->Controls->Add(this->mMenu);
    this->DoubleBuffered = true;
    this->ForeColor = System::Drawing::Color::Red;
    this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
    this->MainMenuStrip = this->mMenu;
    this->Margin = System::Windows::Forms::Padding(4);
    this->Name = L"Progress";
    this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
    this->Text = L"Sysnative BSOD Processing Progress";
    this->TransparencyKey = System::Drawing::Color::Gray;
    this->Load += gcnew System::EventHandler(this, &Progress::loadProgress);
    this->Resize += gcnew System::EventHandler(this, &Progress::progressResize);
    this->mMenu->ResumeLayout(false);
    this->mMenu->PerformLayout();
    this->ResumeLayout(false);
    this->PerformLayout();

}
#pragma endregion

System::Void Progress::safeInvoke(Control^ const control, System::Action^ const action)
{
    if (control != nullptr && !control->IsDisposed && control->InvokeRequired)
    {
        try
        {
            control->Invoke(action);
        }
        catch (System::ObjectDisposedException ^ excep)
        {
            LogUtil::logErrorAsync(excep->Message);
        }
    }
    else if (control != nullptr && !control->IsDisposed)
    {
        action();
    }
}

System::Void Progress::loadProgress(System::Object^ sender, System::EventArgs^ e)
{
    mAlwaysOnTop->Checked = false;
    std::string tempDir = ConfigurationUtil::getTempDirectory();
    std::string tempPath;
    tempPath = tempDir + "\\progressRunning.txt";
    std::ofstream outfile;
    outfile.open(tempPath);
    outfile << "" << std::endl;
    outfile.close();

    TopMost = true;
    Hide();
    UpdateZOrder();
    BringToFront();
    SetForegroundWindow(static_cast<HWND>(this->Handle.ToPointer()));
    Show();
    TopMost = false;
    // Using CDPI example class
    CDPI g_metrics;
    int dpiX = g_metrics.GetDPIX();
    mDefaultKdDisplay->Visible = false;
    this->Height = mShowDefaultKd->Location.Y + mShowDefaultKd->Height + int(double(50) * double(dpiX) / double(96));
    this->Width = mProgressDisplay->Width + int(double(60) * double(dpiX) / double(96));
    CenterWindow* myWindow;
    myWindow = new CenterWindow;
    myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
    delete myWindow;
    mBackgroundProgressUpdater->RunWorkerAsync();
    mBackgroundKdOutputBuilder->RunWorkerAsync();
    TopMost = true;
    Hide();
    UpdateZOrder();
    BringToFront();
    SetForegroundWindow(static_cast<HWND>(this->Handle.ToPointer()));
    Show();
    TopMost = false;
}

System::Void Progress::initializeProgressBar()
{
    updateZOrder();
    BringToFront();
    mProgressDisplay->Maximum = 100;
    mProgressDisplay->Value = 0;
    mProgressMax = 100;

    mDefaultKdDisplay->Clear();
    mUserKdDisplay->Clear();
    mErrorDisplay->Clear();
}

System::Void Progress::updateProgressBar()
{
    mProgressVal = mProgressDisplay->Value;
    if (mStatus != nullptr)
    {
        mStatusDisplay->Text = mStatus;
    }
    if (mHeader != nullptr && mHeader->Length > 0)
    {
        appendTextBox(mErrorDisplay, mHeader);
        mHeader = "";
    }
    mCheckBox1Checked = mShowDefaultKd->Checked;
}

System::Void Progress::updateWindow()
{
    // Using CDPI example class
    CDPI g_metrics;
    int dpiX = g_metrics.GetDPIX();
    this->Height = mShowDefaultKd->Location.Y + mShowDefaultKd->Height + int(double(50) * double(dpiX) / double(96));
    this->Width = mProgressDisplay->Width + int(double(60) * double(dpiX) / double(96));
    CenterWindow myWindow;
    myWindow.MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
}

System::Void Progress::updateZOrder()
{
    TopMost = true;
    Hide();
    UpdateZOrder();
    BringToFront();
    SetForegroundWindow(static_cast<HWND>(this->Handle.ToPointer()));
    Show();
    TopMost = false;
}

}// end namespace SysnativeBSODApps