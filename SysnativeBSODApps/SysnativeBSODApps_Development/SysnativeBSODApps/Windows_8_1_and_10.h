#include <string>

#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for Windows_8_1_and_10
  /// </summary>
  public ref class Windows_8_1_and_10 : public System::Windows::Forms::Form
  {
  public:
  int m_nSavedED;
  System::String^ tempStringS;
  cli::array<System::String^>^ m_SADriverNames;
  cli::array<System::String^>^ m_SADriverDates;
  cli::array<System::String^>^ m_SAXPED;
  cli::array<System::String^>^ m_SAVistaED;
  cli::array<System::String^>^ m_SASevenED;
  cli::array<System::String^>^ m_SAEightED;

    Windows_8_1_and_10(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~Windows_8_1_and_10()
    {
      if (components)
      {
        delete components;
      }
    }
  private: System::Windows::Forms::TabControl^  tabControl1;
  protected: 
  private: System::Windows::Forms::TabPage^  tabPage1;
  private: System::Windows::Forms::TabPage^  tabPage2;
  private: System::Windows::Forms::Label^  label34;
  private: System::Windows::Forms::Label^  label35;
  private: System::Windows::Forms::Label^  label36;
  private: System::Windows::Forms::TextBox^  textBox18;
  private: System::Windows::Forms::TextBox^  textBox19;
  private: System::Windows::Forms::TextBox^  textBox20;
  private: System::Windows::Forms::Label^  label37;
  private: System::Windows::Forms::Label^  label38;
  private: System::Windows::Forms::Label^  label39;
  private: System::Windows::Forms::Label^  label40;
  private: System::Windows::Forms::TextBox^  textBox21;
  private: System::Windows::Forms::TextBox^  textBox22;
  private: System::Windows::Forms::TextBox^  textBox23;
  private: System::Windows::Forms::Label^  label41;
  private: System::Windows::Forms::MenuStrip^  menuStrip2;
  private: System::Windows::Forms::ToolStripMenuItem^  xPDatesToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  betaJun12013ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  vistaDatesToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMNov82007ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  feb42008ToolStripMenuItem;
  private: System::Windows::Forms::CheckBox^  checkBox21;
  private: System::Windows::Forms::Button^  button5;
  private: System::Windows::Forms::CheckBox^  checkBox14;
  private: System::Windows::Forms::CheckBox^  checkBox13;
  private: System::Windows::Forms::TextBox^  textBox25;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
  private: System::Windows::Forms::CheckBox^  checkBox12;
  private: System::Windows::Forms::CheckBox^  checkBox11;
  private: System::Windows::Forms::TextBox^  textBox24;
  private: System::Windows::Forms::Label^  label43;
  private: System::Windows::Forms::RichTextBox^  richTextBox11;
  private: System::Windows::Forms::Label^  label42;
  private: System::Windows::Forms::RichTextBox^  richTextBox10;
  private: System::Windows::Forms::Label^  label24;
  private: System::Windows::Forms::Label^  label23;
  private: System::Windows::Forms::RichTextBox^  richTextBox7;
  private: System::Windows::Forms::RichTextBox^  richTextBox6;
  private: System::Windows::Forms::ToolStripMenuItem^  sP1Sep92002ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  pubOct172013ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  pubJuly292015ToolStripMenuItem;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::Button^  button2;
  private: System::Windows::Forms::ToolStripMenuItem^  nov12015ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sep2016ToolStripMenuItem; 
  private: System::Windows::Forms::ToolStripMenuItem^  apr2017ToolStripMenuItem;


















  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Windows_8_1_and_10::typeid));
      this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
      this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->label34 = (gcnew System::Windows::Forms::Label());
      this->label35 = (gcnew System::Windows::Forms::Label());
      this->label36 = (gcnew System::Windows::Forms::Label());
      this->textBox18 = (gcnew System::Windows::Forms::TextBox());
      this->textBox19 = (gcnew System::Windows::Forms::TextBox());
      this->textBox20 = (gcnew System::Windows::Forms::TextBox());
      this->label37 = (gcnew System::Windows::Forms::Label());
      this->label38 = (gcnew System::Windows::Forms::Label());
      this->label39 = (gcnew System::Windows::Forms::Label());
      this->label40 = (gcnew System::Windows::Forms::Label());
      this->textBox21 = (gcnew System::Windows::Forms::TextBox());
      this->textBox22 = (gcnew System::Windows::Forms::TextBox());
      this->textBox23 = (gcnew System::Windows::Forms::TextBox());
      this->label41 = (gcnew System::Windows::Forms::Label());
      this->menuStrip2 = (gcnew System::Windows::Forms::MenuStrip());
      this->xPDatesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->betaJun12013ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sP1Sep92002ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->pubOct172013ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->vistaDatesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->rTMNov82007ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->feb42008ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->pubJuly292015ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->nov12015ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sep2016ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->apr2017ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->checkBox21 = (gcnew System::Windows::Forms::CheckBox());
      this->button5 = (gcnew System::Windows::Forms::Button());
      this->checkBox14 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox13 = (gcnew System::Windows::Forms::CheckBox());
      this->textBox25 = (gcnew System::Windows::Forms::TextBox());
      this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
      this->checkBox12 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox11 = (gcnew System::Windows::Forms::CheckBox());
      this->textBox24 = (gcnew System::Windows::Forms::TextBox());
      this->label43 = (gcnew System::Windows::Forms::Label());
      this->richTextBox11 = (gcnew System::Windows::Forms::RichTextBox());
      this->label42 = (gcnew System::Windows::Forms::Label());
      this->richTextBox10 = (gcnew System::Windows::Forms::RichTextBox());
      this->label24 = (gcnew System::Windows::Forms::Label());
      this->label23 = (gcnew System::Windows::Forms::Label());
      this->richTextBox7 = (gcnew System::Windows::Forms::RichTextBox());
      this->richTextBox6 = (gcnew System::Windows::Forms::RichTextBox());
      this->tabControl1->SuspendLayout();
      this->tabPage1->SuspendLayout();
      this->menuStrip2->SuspendLayout();
      this->tabPage2->SuspendLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->BeginInit();
      this->SuspendLayout();
      // 
      // tabControl1
      // 
      this->tabControl1->Controls->Add(this->tabPage1);
      this->tabControl1->Controls->Add(this->tabPage2);
      this->tabControl1->Location = System::Drawing::Point(17, 15);
      this->tabControl1->Margin = System::Windows::Forms::Padding(4);
      this->tabControl1->Name = L"tabControl1";
      this->tabControl1->SelectedIndex = 0;
      this->tabControl1->Size = System::Drawing::Size(953, 630);
      this->tabControl1->TabIndex = 0;
      // 
      // tabPage1
      // 
      this->tabPage1->Controls->Add(this->button1);
      this->tabPage1->Controls->Add(this->label34);
      this->tabPage1->Controls->Add(this->label35);
      this->tabPage1->Controls->Add(this->label36);
      this->tabPage1->Controls->Add(this->textBox18);
      this->tabPage1->Controls->Add(this->textBox19);
      this->tabPage1->Controls->Add(this->textBox20);
      this->tabPage1->Controls->Add(this->label37);
      this->tabPage1->Controls->Add(this->label38);
      this->tabPage1->Controls->Add(this->label39);
      this->tabPage1->Controls->Add(this->label40);
      this->tabPage1->Controls->Add(this->textBox21);
      this->tabPage1->Controls->Add(this->textBox22);
      this->tabPage1->Controls->Add(this->textBox23);
      this->tabPage1->Controls->Add(this->label41);
      this->tabPage1->Controls->Add(this->menuStrip2);
      this->tabPage1->Location = System::Drawing::Point(4, 25);
      this->tabPage1->Margin = System::Windows::Forms::Padding(4);
      this->tabPage1->Name = L"tabPage1";
      this->tabPage1->Padding = System::Windows::Forms::Padding(4);
      this->tabPage1->Size = System::Drawing::Size(945, 601);
      this->tabPage1->TabIndex = 0;
      this->tabPage1->Text = L"Old Driver After";
      this->tabPage1->UseVisualStyleBackColor = true;
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(541, 44);
      this->button1->Margin = System::Windows::Forms::Padding(4);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(140, 28);
      this->button1->TabIndex = 173;
      this->button1->Text = L"Save and Close";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::button1_Click);
      // 
      // label34
      // 
      this->label34->AutoSize = true;
      this->label34->Location = System::Drawing::Point(381, 84);
      this->label34->Name = L"label34";
      this->label34->Size = System::Drawing::Size(38, 17);
      this->label34->TabIndex = 172;
      this->label34->Text = L"Year";
      // 
      // label35
      // 
      this->label35->AutoSize = true;
      this->label35->Location = System::Drawing::Point(344, 84);
      this->label35->Name = L"label35";
      this->label35->Size = System::Drawing::Size(33, 17);
      this->label35->TabIndex = 171;
      this->label35->Text = L"Day";
      // 
      // label36
      // 
      this->label36->AutoSize = true;
      this->label36->Location = System::Drawing::Point(288, 84);
      this->label36->Name = L"label36";
      this->label36->Size = System::Drawing::Size(47, 17);
      this->label36->TabIndex = 170;
      this->label36->Text = L"Month";
      // 
      // textBox18
      // 
      this->textBox18->Location = System::Drawing::Point(291, 102);
      this->textBox18->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBox18->Name = L"textBox18";
      this->textBox18->Size = System::Drawing::Size(48, 22);
      this->textBox18->TabIndex = 169;
      // 
      // textBox19
      // 
      this->textBox19->Location = System::Drawing::Point(348, 102);
      this->textBox19->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBox19->Name = L"textBox19";
      this->textBox19->Size = System::Drawing::Size(33, 22);
      this->textBox19->TabIndex = 168;
      // 
      // textBox20
      // 
      this->textBox20->Location = System::Drawing::Point(387, 102);
      this->textBox20->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBox20->Name = L"textBox20";
      this->textBox20->Size = System::Drawing::Size(56, 22);
      this->textBox20->TabIndex = 167;
      // 
      // label37
      // 
      this->label37->AutoSize = true;
      this->label37->BackColor = System::Drawing::SystemColors::Window;
      this->label37->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label37->Location = System::Drawing::Point(16, 106);
      this->label37->Name = L"label37";
      this->label37->Size = System::Drawing::Size(223, 17);
      this->label37->TabIndex = 166;
      this->label37->Text = L"Windows 8.1 Old Driver After:";
      // 
      // label38
      // 
      this->label38->AutoSize = true;
      this->label38->Location = System::Drawing::Point(381, 133);
      this->label38->Name = L"label38";
      this->label38->Size = System::Drawing::Size(38, 17);
      this->label38->TabIndex = 165;
      this->label38->Text = L"Year";
      // 
      // label39
      // 
      this->label39->AutoSize = true;
      this->label39->Location = System::Drawing::Point(344, 133);
      this->label39->Name = L"label39";
      this->label39->Size = System::Drawing::Size(33, 17);
      this->label39->TabIndex = 164;
      this->label39->Text = L"Day";
      // 
      // label40
      // 
      this->label40->AutoSize = true;
      this->label40->Location = System::Drawing::Point(288, 133);
      this->label40->Name = L"label40";
      this->label40->Size = System::Drawing::Size(47, 17);
      this->label40->TabIndex = 163;
      this->label40->Text = L"Month";
      // 
      // textBox21
      // 
      this->textBox21->Location = System::Drawing::Point(291, 151);
      this->textBox21->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBox21->Name = L"textBox21";
      this->textBox21->Size = System::Drawing::Size(48, 22);
      this->textBox21->TabIndex = 162;
      // 
      // textBox22
      // 
      this->textBox22->Location = System::Drawing::Point(348, 151);
      this->textBox22->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBox22->Name = L"textBox22";
      this->textBox22->Size = System::Drawing::Size(33, 22);
      this->textBox22->TabIndex = 161;
      // 
      // textBox23
      // 
      this->textBox23->Location = System::Drawing::Point(387, 151);
      this->textBox23->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBox23->Name = L"textBox23";
      this->textBox23->Size = System::Drawing::Size(56, 22);
      this->textBox23->TabIndex = 160;
      // 
      // label41
      // 
      this->label41->AutoSize = true;
      this->label41->BackColor = System::Drawing::SystemColors::Window;
      this->label41->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label41->Location = System::Drawing::Point(16, 154);
      this->label41->Name = L"label41";
      this->label41->Size = System::Drawing::Size(218, 17);
      this->label41->TabIndex = 159;
      this->label41->Text = L"Windows 10 Old Driver After:";
      // 
      // menuStrip2
      // 
      this->menuStrip2->Dock = System::Windows::Forms::DockStyle::None;
      this->menuStrip2->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->xPDatesToolStripMenuItem, 
        this->vistaDatesToolStripMenuItem});
      this->menuStrip2->Location = System::Drawing::Point(32, 44);
      this->menuStrip2->Name = L"menuStrip2";
      this->menuStrip2->Padding = System::Windows::Forms::Padding(5, 2, 0, 2);
      this->menuStrip2->Size = System::Drawing::Size(260, 28);
      this->menuStrip2->TabIndex = 140;
      this->menuStrip2->Text = L"menuStrip2";
      // 
      // xPDatesToolStripMenuItem
      // 
      this->xPDatesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->betaJun12013ToolStripMenuItem, 
        this->sP1Sep92002ToolStripMenuItem, this->pubOct172013ToolStripMenuItem});
      this->xPDatesToolStripMenuItem->Name = L"xPDatesToolStripMenuItem";
      this->xPDatesToolStripMenuItem->Size = System::Drawing::Size(82, 24);
      this->xPDatesToolStripMenuItem->Text = L"8.1 Dates";
      // 
      // betaJun12013ToolStripMenuItem
      // 
      this->betaJun12013ToolStripMenuItem->Name = L"betaJun12013ToolStripMenuItem";
      this->betaJun12013ToolStripMenuItem->Size = System::Drawing::Size(203, 24);
      this->betaJun12013ToolStripMenuItem->Text = L"Beta: Jun. 1, 2013";
      this->betaJun12013ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::betaJun12013ToolStripMenuItem_Click);
      // 
      // sP1Sep92002ToolStripMenuItem
      // 
      this->sP1Sep92002ToolStripMenuItem->Name = L"sP1Sep92002ToolStripMenuItem";
      this->sP1Sep92002ToolStripMenuItem->Size = System::Drawing::Size(203, 24);
      this->sP1Sep92002ToolStripMenuItem->Text = L"RTM: Aug. 27, 2013";
      this->sP1Sep92002ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::sP1Sep92002ToolStripMenuItem_Click);
      // 
      // pubOct172013ToolStripMenuItem
      // 
      this->pubOct172013ToolStripMenuItem->Name = L"pubOct172013ToolStripMenuItem";
      this->pubOct172013ToolStripMenuItem->Size = System::Drawing::Size(203, 24);
      this->pubOct172013ToolStripMenuItem->Text = L"Pub: Oct. 17, 2013";
      this->pubOct172013ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::pubOct172013ToolStripMenuItem_Click);
      // 
      // vistaDatesToolStripMenuItem
      // 
      this->vistaDatesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {this->rTMNov82007ToolStripMenuItem, 
        this->feb42008ToolStripMenuItem, this->pubJuly292015ToolStripMenuItem, this->nov12015ToolStripMenuItem, this->sep2016ToolStripMenuItem, 
        this->apr2017ToolStripMenuItem});
      this->vistaDatesToolStripMenuItem->Name = L"vistaDatesToolStripMenuItem";
      this->vistaDatesToolStripMenuItem->Size = System::Drawing::Size(79, 24);
      this->vistaDatesToolStripMenuItem->Text = L"10 Dates";
      // 
      // rTMNov82007ToolStripMenuItem
      // 
      this->rTMNov82007ToolStripMenuItem->Name = L"rTMNov82007ToolStripMenuItem";
      this->rTMNov82007ToolStripMenuItem->Size = System::Drawing::Size(192, 24);
      this->rTMNov82007ToolStripMenuItem->Text = L"Beta: Oct. 1, 2014";
      this->rTMNov82007ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::rTMNov82007ToolStripMenuItem_Click);
      // 
      // feb42008ToolStripMenuItem
      // 
      this->feb42008ToolStripMenuItem->Name = L"feb42008ToolStripMenuItem";
      this->feb42008ToolStripMenuItem->Size = System::Drawing::Size(192, 24);
      this->feb42008ToolStripMenuItem->Text = L"RTM: Jun. 2015";
      this->feb42008ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::feb42008ToolStripMenuItem_Click);
      // 
      // pubJuly292015ToolStripMenuItem
      // 
      this->pubJuly292015ToolStripMenuItem->Name = L"pubJuly292015ToolStripMenuItem";
      this->pubJuly292015ToolStripMenuItem->Size = System::Drawing::Size(192, 24);
      this->pubJuly292015ToolStripMenuItem->Text = L"Pub: Jul. 29, 2015";
      this->pubJuly292015ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::pubJuly292015ToolStripMenuItem_Click);
      // 
      // nov12015ToolStripMenuItem
      // 
      this->nov12015ToolStripMenuItem->Name = L"nov12015ToolStripMenuItem";
      this->nov12015ToolStripMenuItem->Size = System::Drawing::Size(192, 24);
      this->nov12015ToolStripMenuItem->Text = L"1511: Nov. 2015";
      this->nov12015ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::nov12015ToolStripMenuItem_Click);
      // 
      // sep2016ToolStripMenuItem
      // 
      this->sep2016ToolStripMenuItem->Name = L"sep2016ToolStripMenuItem";
      this->sep2016ToolStripMenuItem->Size = System::Drawing::Size(192, 24);
      this->sep2016ToolStripMenuItem->Text = L"1607: Sep. 2016";
      this->sep2016ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::sep12016ToolStripMenuItem_Click);
      // 
      // apr2017ToolStripMenuItem
      // 
      this->apr2017ToolStripMenuItem->Name = L"apr2017ToolStripMenuItem";
      this->apr2017ToolStripMenuItem->Size = System::Drawing::Size(192, 24);
      this->apr2017ToolStripMenuItem->Text = L"1703: Apr. 2017";
      this->apr2017ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::apr12017ToolStripMenuItem_Click);
      // 
      // tabPage2
      // 
      this->tabPage2->Controls->Add(this->button2);
      this->tabPage2->Controls->Add(this->checkBox21);
      this->tabPage2->Controls->Add(this->button5);
      this->tabPage2->Controls->Add(this->checkBox14);
      this->tabPage2->Controls->Add(this->checkBox13);
      this->tabPage2->Controls->Add(this->textBox25);
      this->tabPage2->Controls->Add(this->numericUpDown1);
      this->tabPage2->Controls->Add(this->checkBox12);
      this->tabPage2->Controls->Add(this->checkBox11);
      this->tabPage2->Controls->Add(this->textBox24);
      this->tabPage2->Controls->Add(this->label43);
      this->tabPage2->Controls->Add(this->richTextBox11);
      this->tabPage2->Controls->Add(this->label42);
      this->tabPage2->Controls->Add(this->richTextBox10);
      this->tabPage2->Controls->Add(this->label24);
      this->tabPage2->Controls->Add(this->label23);
      this->tabPage2->Controls->Add(this->richTextBox7);
      this->tabPage2->Controls->Add(this->richTextBox6);
      this->tabPage2->Location = System::Drawing::Point(4, 25);
      this->tabPage2->Margin = System::Windows::Forms::Padding(4);
      this->tabPage2->Name = L"tabPage2";
      this->tabPage2->Padding = System::Windows::Forms::Padding(4);
      this->tabPage2->Size = System::Drawing::Size(945, 601);
      this->tabPage2->TabIndex = 1;
      this->tabPage2->Text = L"Excluded Drivers";
      this->tabPage2->UseVisualStyleBackColor = true;
      // 
      // button2
      // 
      this->button2->Location = System::Drawing::Point(741, 14);
      this->button2->Margin = System::Windows::Forms::Padding(4);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(140, 28);
      this->button2->TabIndex = 174;
      this->button2->Text = L"Save and Close";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::button1_Click);
      // 
      // checkBox21
      // 
      this->checkBox21->AutoSize = true;
      this->checkBox21->Location = System::Drawing::Point(147, 18);
      this->checkBox21->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->checkBox21->Name = L"checkBox21";
      this->checkBox21->Size = System::Drawing::Size(71, 21);
      this->checkBox21->TabIndex = 103;
      this->checkBox21->Text = L"Delete";
      this->checkBox21->UseVisualStyleBackColor = true;
      this->checkBox21->CheckedChanged += gcnew System::EventHandler(this, &Windows_8_1_and_10::checkBox21_CheckedChanged);
      // 
      // button5
      // 
      this->button5->Location = System::Drawing::Point(416, 16);
      this->button5->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->button5->Name = L"button5";
      this->button5->Size = System::Drawing::Size(100, 27);
      this->button5->TabIndex = 102;
      this->button5->Text = L"Proceed";
      this->button5->UseVisualStyleBackColor = true;
      this->button5->Click += gcnew System::EventHandler(this, &Windows_8_1_and_10::button5_Click);
      // 
      // checkBox14
      // 
      this->checkBox14->AutoSize = true;
      this->checkBox14->Location = System::Drawing::Point(860, 70);
      this->checkBox14->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->checkBox14->Name = L"checkBox14";
      this->checkBox14->Size = System::Drawing::Size(18, 17);
      this->checkBox14->TabIndex = 99;
      this->checkBox14->UseVisualStyleBackColor = true;
      // 
      // checkBox13
      // 
      this->checkBox13->AutoSize = true;
      this->checkBox13->Location = System::Drawing::Point(745, 70);
      this->checkBox13->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->checkBox13->Name = L"checkBox13";
      this->checkBox13->Size = System::Drawing::Size(18, 17);
      this->checkBox13->TabIndex = 98;
      this->checkBox13->UseVisualStyleBackColor = true;
      // 
      // textBox25
      // 
      this->textBox25->Location = System::Drawing::Point(507, 66);
      this->textBox25->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->textBox25->Name = L"textBox25";
      this->textBox25->Size = System::Drawing::Size(196, 22);
      this->textBox25->TabIndex = 97;
      // 
      // numericUpDown1
      // 
      this->numericUpDown1->Enabled = false;
      this->numericUpDown1->Location = System::Drawing::Point(243, 16);
      this->numericUpDown1->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->numericUpDown1->Name = L"numericUpDown1";
      this->numericUpDown1->Size = System::Drawing::Size(160, 22);
      this->numericUpDown1->TabIndex = 96;
      this->numericUpDown1->ValueChanged += gcnew System::EventHandler(this, &Windows_8_1_and_10::numericUpDown1_ValueChanged);
      // 
      // checkBox12
      // 
      this->checkBox12->AutoSize = true;
      this->checkBox12->Location = System::Drawing::Point(83, 18);
      this->checkBox12->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->checkBox12->Name = L"checkBox12";
      this->checkBox12->Size = System::Drawing::Size(54, 21);
      this->checkBox12->TabIndex = 95;
      this->checkBox12->Text = L"Edit";
      this->checkBox12->UseVisualStyleBackColor = true;
      this->checkBox12->CheckedChanged += gcnew System::EventHandler(this, &Windows_8_1_and_10::checkBox12_CheckedChanged);
      // 
      // checkBox11
      // 
      this->checkBox11->AutoSize = true;
      this->checkBox11->Location = System::Drawing::Point(19, 18);
      this->checkBox11->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->checkBox11->Name = L"checkBox11";
      this->checkBox11->Size = System::Drawing::Size(55, 21);
      this->checkBox11->TabIndex = 94;
      this->checkBox11->Text = L"Add";
      this->checkBox11->UseVisualStyleBackColor = true;
      this->checkBox11->CheckedChanged += gcnew System::EventHandler(this, &Windows_8_1_and_10::checkBox11_CheckedChanged);
      // 
      // textBox24
      // 
      this->textBox24->Location = System::Drawing::Point(19, 66);
      this->textBox24->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
      this->textBox24->Name = L"textBox24";
      this->textBox24->Size = System::Drawing::Size(480, 22);
      this->textBox24->TabIndex = 93;
      // 
      // label43
      // 
      this->label43->AutoSize = true;
      this->label43->Location = System::Drawing::Point(856, 50);
      this->label43->Name = L"label43";
      this->label43->Size = System::Drawing::Size(24, 17);
      this->label43->TabIndex = 88;
      this->label43->Text = L"10";
      // 
      // richTextBox11
      // 
      this->richTextBox11->Location = System::Drawing::Point(815, 98);
      this->richTextBox11->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->richTextBox11->Name = L"richTextBox11";
      this->richTextBox11->ReadOnly = true;
      this->richTextBox11->Size = System::Drawing::Size(103, 530);
      this->richTextBox11->TabIndex = 87;
      this->richTextBox11->Text = L"";
      this->richTextBox11->WordWrap = false;
      // 
      // label42
      // 
      this->label42->AutoSize = true;
      this->label42->Location = System::Drawing::Point(741, 50);
      this->label42->Name = L"label42";
      this->label42->Size = System::Drawing::Size(28, 17);
      this->label42->TabIndex = 86;
      this->label42->Text = L"8.1";
      // 
      // richTextBox10
      // 
      this->richTextBox10->Location = System::Drawing::Point(708, 98);
      this->richTextBox10->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->richTextBox10->Name = L"richTextBox10";
      this->richTextBox10->ReadOnly = true;
      this->richTextBox10->Size = System::Drawing::Size(103, 530);
      this->richTextBox10->TabIndex = 85;
      this->richTextBox10->Text = L"";
      this->richTextBox10->WordWrap = false;
      // 
      // label24
      // 
      this->label24->AutoSize = true;
      this->label24->Location = System::Drawing::Point(559, 50);
      this->label24->Name = L"label24";
      this->label24->Size = System::Drawing::Size(80, 17);
      this->label24->TabIndex = 84;
      this->label24->Text = L"Driver Date";
      // 
      // label23
      // 
      this->label23->AutoSize = true;
      this->label23->Location = System::Drawing::Point(187, 46);
      this->label23->Name = L"label23";
      this->label23->Size = System::Drawing::Size(87, 17);
      this->label23->TabIndex = 83;
      this->label23->Text = L"Driver Name";
      // 
      // richTextBox7
      // 
      this->richTextBox7->Location = System::Drawing::Point(507, 98);
      this->richTextBox7->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->richTextBox7->Name = L"richTextBox7";
      this->richTextBox7->ReadOnly = true;
      this->richTextBox7->Size = System::Drawing::Size(196, 530);
      this->richTextBox7->TabIndex = 82;
      this->richTextBox7->Text = L"May 13 2009\nMay 18 2009\nJuly 13 2009";
      this->richTextBox7->WordWrap = false;
      // 
      // richTextBox6
      // 
      this->richTextBox6->Location = System::Drawing::Point(19, 98);
      this->richTextBox6->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->richTextBox6->Name = L"richTextBox6";
      this->richTextBox6->ReadOnly = true;
      this->richTextBox6->Size = System::Drawing::Size(480, 530);
      this->richTextBox6->TabIndex = 81;
      this->richTextBox6->Text = L"ASACPI.sys\nGEARAspiWDM.sys\nintelppm.sys";
      this->richTextBox6->WordWrap = false;
      // 
      // Windows_8_1_and_10
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(975, 655);
      this->Controls->Add(this->tabControl1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Margin = System::Windows::Forms::Padding(4);
      this->Name = L"Windows_8_1_and_10";
      this->Text = L"Windows_8_1_and_10";
      this->Load += gcnew System::EventHandler(this, &Windows_8_1_and_10::Windows_8_1_and_10_Load);
      this->Resize += gcnew System::EventHandler(this, &Windows_8_1_and_10::Windows_8_1_and_10_Resize);
      this->tabControl1->ResumeLayout(false);
      this->tabPage1->ResumeLayout(false);
      this->tabPage1->PerformLayout();
      this->menuStrip2->ResumeLayout(false);
      this->menuStrip2->PerformLayout();
      this->tabPage2->ResumeLayout(false);
      this->tabPage2->PerformLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->EndInit();
      this->ResumeLayout(false);

    }
#pragma endregion
private: bool unSignedCompare(std::string str1, std::string str2);
private: System::Void load81oldDriverAfter();
private: System::Void loadExcludedDrivers();
private: System::Void Windows_8_1_and_10_Load(System::Object^  sender, System::EventArgs^  e);
private: System::Void Windows_8_1_and_10_Resize(System::Object^  sender, System::EventArgs^  e);
private: System::Void betaJun12013ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void sP1Sep92002ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void pubOct172013ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void rTMNov82007ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void feb42008ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void pubJuly292015ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void nov12015ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void sep12016ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void apr12017ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void write81OldDriverAfter();
private: System::Void write10OldDriverAfter();
private: System::Void writeExcludedDrivers();
private: System::Void cleanUpOldOSEDs();
private: System::Void deleteOldOSEDs(int num);
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void checkBox11_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
private: System::Void checkBox12_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
private: System::Void checkBox21_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
private: System::Void numericUpDown1_ValueChanged(System::Object^  sender, System::EventArgs^  e);
};
}
