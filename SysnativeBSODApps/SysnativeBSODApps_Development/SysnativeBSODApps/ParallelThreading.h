

#include "debug.h"

//#include <vld.h>

#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for ParallelThreading
  /// </summary>
  public ref class ParallelThreading : public System::Windows::Forms::Form
  {
  public:
    static int onTop = 0;
    static int defaultThreads;
    static int userThreads;
  private: System::Windows::Forms::Label^  label3;
  public: 
    static array<System::String^>^ defaultdzdnS;
    ParallelThreading(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~ParallelThreading()
    {
      if (components)
      {
        delete components;
      }
    }
  private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown2;
  private: System::Windows::Forms::Label^  label1;
  private: System::Windows::Forms::Label^  label2;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::Button^  button2;
  private: System::Windows::Forms::Panel^  panel1;
  protected: 

  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ParallelThreading::typeid));
      this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
      this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->panel1 = (gcnew System::Windows::Forms::Panel());
      this->label3 = (gcnew System::Windows::Forms::Label());
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->BeginInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown2))->BeginInit();
      this->SuspendLayout();
      // 
      // numericUpDown1
      // 
      this->numericUpDown1->Location = System::Drawing::Point(200, 12);
      this->numericUpDown1->Name = L"numericUpDown1";
      this->numericUpDown1->Size = System::Drawing::Size(120, 20);
      this->numericUpDown1->TabIndex = 0;
      this->numericUpDown1->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {6, 0, 0, 0});
      this->numericUpDown1->ValueChanged += gcnew System::EventHandler(this, &ParallelThreading::numericUpDown1_ValueChanged);
      // 
      // numericUpDown2
      // 
      this->numericUpDown2->Location = System::Drawing::Point(200, 67);
      this->numericUpDown2->Name = L"numericUpDown2";
      this->numericUpDown2->Size = System::Drawing::Size(120, 20);
      this->numericUpDown2->TabIndex = 1;
      this->numericUpDown2->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {6, 0, 0, 0});
      this->numericUpDown2->ValueChanged += gcnew System::EventHandler(this, &ParallelThreading::numericUpDown2_ValueChanged);
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(12, 14);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(165, 13);
      this->label1->TabIndex = 2;
      this->label1->Text = L"Number of Threads for Default kd";
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(12, 69);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(153, 13);
      this->label2->TabIndex = 3;
      this->label2->Text = L"Number of Threads for User kd";
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(149, 148);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(75, 23);
      this->button1->TabIndex = 4;
      this->button1->Text = L"OK";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &ParallelThreading::button1_Click);
      // 
      // button2
      // 
      this->button2->Location = System::Drawing::Point(245, 148);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(75, 23);
      this->button2->TabIndex = 5;
      this->button2->Text = L"Cancel";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &ParallelThreading::button2_Click);
      // 
      // panel1
      // 
      this->panel1->BackColor = System::Drawing::SystemColors::ActiveCaptionText;
      this->panel1->Location = System::Drawing::Point(0, 140);
      this->panel1->Name = L"panel1";
      this->panel1->Size = System::Drawing::Size(330, 2);
      this->panel1->TabIndex = 6;
      // 
      // label3
      // 
      this->label3->AutoSize = true;
      this->label3->ForeColor = System::Drawing::Color::Red;
      this->label3->Location = System::Drawing::Point(99, 94);
      this->label3->Name = L"label3";
      this->label3->Size = System::Drawing::Size(0, 13);
      this->label3->TabIndex = 7;
      // 
      // ParallelThreading
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(331, 183);
      this->Controls->Add(this->label3);
      this->Controls->Add(this->panel1);
      this->Controls->Add(this->button2);
      this->Controls->Add(this->button1);
      this->Controls->Add(this->label2);
      this->Controls->Add(this->label1);
      this->Controls->Add(this->numericUpDown2);
      this->Controls->Add(this->numericUpDown1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Name = L"ParallelThreading";
      this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
      this->Text = L"ParallelThreading";
      this->Load += gcnew System::EventHandler(this, &ParallelThreading::ParallelThreading_Load);
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->EndInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown2))->EndInit();
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
  private: System::Void ParallelThreading_Load(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void numericUpDown1_ValueChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void numericUpDown2_ValueChanged(System::Object^  sender, System::EventArgs^  e);
};
}
