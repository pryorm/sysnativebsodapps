﻿#include <stdlib.h>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include "ConfigurationUtil.h"
#include "ConsoleInfo.hpp"
#include "FileSystemUtil.h"
#include "LogUtil.h"
#include "SystemStandardConversionUtil.h"
#include <Windows.h>
#include "CenterWindow.h"
#include "outputOrder.h"

#include "debug.h"


#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for Form3
  /// </summary>
  public ref class Form3 : public System::Windows::Forms::Form
  {
  public:
    outputOrder^ oo;
    array<System::String^>^ fileListS;
    System::String^ timeS;
    static int firstScreeni = 1;
    static int cancelClicked = 0;
  public: System::Windows::Forms::Label^  label4;
  private: System::Windows::Forms::ToolStripMenuItem^  hTMLToolStripMenuItem;
  private: static int outputOrder_n = 0;
  private: static int reportProgressBar_n = 0;
  private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
  private: System::Windows::Forms::ListBox^  listBox1;
  private: System::Windows::Forms::Label^  label2;


  public: 
    System::String^ tempStringS;
    Form3(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~Form3()
    {
      if (components)
      {
        delete components;
      }
    }

  protected: 
    System::String^ mStatus;

  private: System::Windows::Forms::Label^  label12;


  private: System::Windows::Forms::Button^  button1;

  private: System::Windows::Forms::Button^  button4;
  private: System::Windows::Forms::Button^  button2;

  private: System::Windows::Forms::ProgressBar^  progressBar1;
  private: System::Windows::Forms::WebBrowser^  webBrowser1;

  private: System::Windows::Forms::Label^  label1;



  private: System::Windows::Forms::Label^  label3;
  private: System::Windows::Forms::MenuStrip^  menuStrip1;
  private: System::Windows::Forms::ToolStripMenuItem^  viewToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  windowsToolStripMenuItem;

  private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  parseBBCodeToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
  private: System::Windows::Forms::ToolStripMenuItem^  txtsToolStripMenuItem;

   private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form3::typeid));
      this->label12 = (gcnew System::Windows::Forms::Label());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->button4 = (gcnew System::Windows::Forms::Button());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
      this->webBrowser1 = (gcnew System::Windows::Forms::WebBrowser());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->label3 = (gcnew System::Windows::Forms::Label());
      this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
      this->viewToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->windowsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->txtsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->hTMLToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->parseBBCodeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
      this->listBox1 = (gcnew System::Windows::Forms::ListBox());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->label4 = (gcnew System::Windows::Forms::Label());
      this->menuStrip1->SuspendLayout();
      this->SuspendLayout();
      // 
      // label12
      // 
      this->label12->AutoSize = true;
      this->label12->Location = System::Drawing::Point(1, 121);
      this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label12->Name = L"label12";
      this->label12->Size = System::Drawing::Size(107, 13);
      this->label12->TabIndex = 29;
      this->label12->Text = L" © 2024 Mike Pryor";
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(4, 61);
      this->button1->Margin = System::Windows::Forms::Padding(2);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(118, 19);
      this->button1->TabIndex = 31;
      this->button1->TabStop = false;
      this->button1->Text = L"View HTML";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &Form3::button1_Click);
      // 
      // button4
      // 
      this->button4->Location = System::Drawing::Point(88, 84);
      this->button4->Margin = System::Windows::Forms::Padding(2);
      this->button4->Name = L"button4";
      this->button4->Size = System::Drawing::Size(73, 19);
      this->button4->TabIndex = 34;
      this->button4->TabStop = false;
      this->button4->Text = L"Cancel";
      this->button4->UseVisualStyleBackColor = true;
      this->button4->Click += gcnew System::EventHandler(this, &Form3::button4_Click);
      // 
      // button2
      // 
      this->button2->Location = System::Drawing::Point(127, 61);
      this->button2->Margin = System::Windows::Forms::Padding(2);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(118, 19);
      this->button2->TabIndex = 32;
      this->button2->TabStop = false;
      this->button2->Text = L"View .txts";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &Form3::button2_Click);
      // 
      // progressBar1
      // 
      this->progressBar1->Location = System::Drawing::Point(4, 81);
      this->progressBar1->Name = L"progressBar1";
      this->progressBar1->Size = System::Drawing::Size(241, 23);
      this->progressBar1->TabIndex = 36;
      // 
      // webBrowser1
      // 
      this->webBrowser1->Location = System::Drawing::Point(250, 70);
      this->webBrowser1->MinimumSize = System::Drawing::Size(20, 20);
      this->webBrowser1->Name = L"webBrowser1";
      this->webBrowser1->Size = System::Drawing::Size(720, 441);
      this->webBrowser1->TabIndex = 37;
      this->webBrowser1->ScriptErrorsSuppressed = true;
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(251, 54);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(60, 13);
      this->label1->TabIndex = 39;
      this->label1->Text = L"HTML Title";
      // 
      // label3
      // 
      this->label3->AutoSize = true;
      this->label3->Location = System::Drawing::Point(1, 64);
      this->label3->Name = L"label3";
      this->label3->Size = System::Drawing::Size(72, 13);
      this->label3->TabIndex = 43;
      this->label3->Text = L"Progress Text";
      // 
      // menuStrip1
      // 
      this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->viewToolStripMenuItem, 
        this->optionsToolStripMenuItem});
      this->menuStrip1->Location = System::Drawing::Point(0, 0);
      this->menuStrip1->Name = L"menuStrip1";
      this->menuStrip1->Size = System::Drawing::Size(1349, 24);
      this->menuStrip1->TabIndex = 44;
      this->menuStrip1->Text = L"menuStrip1";
      // 
      // viewToolStripMenuItem
      // 
      this->viewToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->windowsToolStripMenuItem, 
        this->txtsToolStripMenuItem, this->hTMLToolStripMenuItem});
      this->viewToolStripMenuItem->Name = L"viewToolStripMenuItem";
      this->viewToolStripMenuItem->Size = System::Drawing::Size(44, 20);
      this->viewToolStripMenuItem->Text = L"View";
      // 
      // windowsToolStripMenuItem
      // 
      this->windowsToolStripMenuItem->Name = L"windowsToolStripMenuItem";
      this->windowsToolStripMenuItem->Size = System::Drawing::Size(189, 22);
      this->windowsToolStripMenuItem->Text = L"Change Output Order";
      this->windowsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form3::windowsToolStripMenuItem_Click);
      // 
      // txtsToolStripMenuItem
      // 
      this->txtsToolStripMenuItem->Name = L"txtsToolStripMenuItem";
      this->txtsToolStripMenuItem->Size = System::Drawing::Size(189, 22);
      this->txtsToolStripMenuItem->Text = L".txts";
      this->txtsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form3::txtsToolStripMenuItem_Click);
      // 
      // hTMLToolStripMenuItem
      // 
      this->hTMLToolStripMenuItem->Name = L"hTMLToolStripMenuItem";
      this->hTMLToolStripMenuItem->Size = System::Drawing::Size(189, 22);
      this->hTMLToolStripMenuItem->Text = L"HTML";
      this->hTMLToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form3::hTMLToolStripMenuItem_Click);
      // 
      // optionsToolStripMenuItem
      // 
      this->optionsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->parseBBCodeToolStripMenuItem, 
        this->toolStripMenuItem1});
      this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
      this->optionsToolStripMenuItem->Size = System::Drawing::Size(61, 20);
      this->optionsToolStripMenuItem->Text = L"Options";
      // 
      // parseBBCodeToolStripMenuItem
      // 
      this->parseBBCodeToolStripMenuItem->CheckOnClick = true;
      this->parseBBCodeToolStripMenuItem->Name = L"parseBBCodeToolStripMenuItem";
      this->parseBBCodeToolStripMenuItem->Size = System::Drawing::Size(152, 22);
      this->parseBBCodeToolStripMenuItem->Text = L"Parse BBCode";
      this->parseBBCodeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form3::parseBBCodeToolStripMenuItem_Click);
      // 
      // toolStripMenuItem1
      // 
      this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
      this->toolStripMenuItem1->Size = System::Drawing::Size(152, 22);
      // 
      // backgroundWorker1
      // 
      this->backgroundWorker1->WorkerReportsProgress = true;
      this->backgroundWorker1->WorkerSupportsCancellation = true;
      this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Form3::backgroundWorker1_DoWork);
      this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &Form3::backgroundWorker1_ProgressChanged);
      this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &Form3::backgroundWorker1_RunWorkerCompleted);
      // 
      // listBox1
      // 
      this->listBox1->FormattingEnabled = true;
      this->listBox1->Location = System::Drawing::Point(976, 70);
      this->listBox1->Name = L"listBox1";
      this->listBox1->Size = System::Drawing::Size(256, 446);
      this->listBox1->TabIndex = 38;
      this->listBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &Form3::listBox1_SelectedIndexChanged);
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(973, 51);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(63, 13);
      this->label2->TabIndex = 40;
      this->label2->Text = L"Output Files";
      // 
      // label4
      // 
      this->label4->AutoSize = true;
      this->label4->Location = System::Drawing::Point(1, 34);
      this->label4->Name = L"label4";
      this->label4->Size = System::Drawing::Size(76, 13);
      this->label4->TabIndex = 45;
      this->label4->Text = L"timeToAnalyze";
      // 
      // Form3
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(1349, 520);
      this->Controls->Add(this->label4);
      this->Controls->Add(this->label3);
      this->Controls->Add(this->label2);
      this->Controls->Add(this->label1);
      this->Controls->Add(this->listBox1);
      this->Controls->Add(this->webBrowser1);
      this->Controls->Add(this->button4);
      this->Controls->Add(this->button2);
      this->Controls->Add(this->label12);
      this->Controls->Add(this->progressBar1);
      this->Controls->Add(this->menuStrip1);
      this->Controls->Add(this->button1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->MainMenuStrip = this->menuStrip1;
      this->Margin = System::Windows::Forms::Padding(2);
      this->Name = L"Form3";
      this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
      this->Text = L"Output";
      this->Load += gcnew System::EventHandler(this, &Form3::form3Shown);
      this->ResizeEnd += gcnew System::EventHandler(this, &Form3::resize);
      this->Resize += gcnew System::EventHandler(this, &Form3::resize);
      this->menuStrip1->ResumeLayout(false);
      this->menuStrip1->PerformLayout();
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion

// Compare two std::strings regardless of case
bool unSignedCompare(std::string str1, std::string str2){
  if (str1.size() != str2.size()) {
    return false;
  }
  for (std::string::const_iterator c1 = str1.begin(), c2 = str2.begin(); c1 != str1.end(); ++c1, ++c2) {
    // Convert both cases to lowercase prior to comparing
    if (tolower(*c1) != tolower(*c2)) {
      return false;
    }
  }
  return true;
}

private: void bbCodeToHTML(std::string inPath, std::string thisAddress)
  {
    std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
    std::string userProfile = ConfigurationUtil::getUserProfilePath();
    std::ifstream infile;
    
    int htmlBBCode;
    std::string fullPathString = userProfile + "\\SysnativeBSODApps\\dmpOptions\\htmlBBCode.txt";
    std::ifstream inHtml(fullPathString);
    if(inHtml.good()){
      inHtml >> htmlBBCode;
    }
    else{
      htmlBBCode = 0;
    }
    if(htmlBBCode){
      parseBBCodeToolStripMenuItem->Checked = false;
    }
    else{
      parseBBCodeToolStripMenuItem->Checked = true;
    }
    inHtml.close();
    infile.open(inPath);
    if(infile.good()){
      std::ofstream outPercentDone;
      outPercentDone.open(tempDir + "\\percentDone.txt");
      outPercentDone << 452312;
      outPercentDone.close();

      std::ofstream outProg;
      std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
      std::string tempPath;
      tempPath = tempDir + "\\analyzedDmpOf.txt";
      outProg.open(tempPath);
      outProg << "Creating template.html...";
      std::cout << "Creating template.html..." << std::endl;
      outProg.close();
      std::ofstream outfile;
      outfile.open(thisAddress);
      std::vector<int> listType;
      while(!infile.eof()){
        std::string inCh;
        std::string inChTemp;
        getline(infile,inCh);  

        size_t find;
        size_t find2;
        int startInt;
        int endInt;
        int lengthInt;
        int startInt2;
        int endInt2;
        int lengthInt2;
        std::string bbCode;
        int findString;
        int findInt;
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,5),"font=")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        std::vector<int> findSpaces;
        for (int i = 0; i < int(inCh.length()); i++){
          findSpaces.push_back(23);
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          for (int i = 0; i < int(inCh.length()); i++){
            if(inCh.c_str()[i]==' '){
              std::stringstream sstemp;
              if(i >= int(find) && i <= int(find2)){
                findSpaces[i] = 55;
              }
              else if(findSpaces[i] != 55){
                findSpaces[i] = 0;
              }
            }
          }
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,5),"font=")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }    
        std::stringstream ssspace;
        for (int i = 0; i < int(inCh.length()); i++){
          if(inCh.c_str()[i]==' ' && findSpaces[i] == 0){
            ssspace << "&nbsp;";
          }
          else{
            ssspace << inCh.c_str()[i];
          }
        }
        inCh = ssspace.str();
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,6),"color=")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<font color=";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 7;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,6),"color=")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,5),"color")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</font";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 7;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,5),"color")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,4),"url=")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<a href=";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 5;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,4),"url=")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,3),"url")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</a";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 5;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,3),"url")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,2),"b]")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<b";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 2;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,2),"b]")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,2),"b]")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</b";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 3;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,2),"b]")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,2),"u]")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<u";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 2;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,2),"u]")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,2),"u]")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</u";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 3;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,2),"u]")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,2),"i]")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<i";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 2;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,2),"i]")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,2),"i]")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</i";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 3;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,2),"i]")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,5),"font=")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<font face=\"";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 6;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + "\">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,5),"font=")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,4),"font")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</font";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 6;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,4),"font")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,5),"size=")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<font size=";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 6;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,5),"size=")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,4),"size")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</font";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 6;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,4),"size")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,6),"list=1")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<ol style=\"list-style-type: decimal\"";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 7;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,6),"list=1")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,6),"list=a")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<ol style=\"list-style-type: lower-alpha\"";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 7;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,6),"list=a")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,4),"list")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<ul";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 5;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,4),"list")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }

        bbCode = "[*]";
        find = inCh.find(bbCode);
        while(find != std::string::npos && !htmlBBCode){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<li";
          lengthInt = bbCode.length() - 1;
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + lengthInt;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          find = inCh.find(bbCode.c_str(),find2+1,lengthInt+1);
        }
        
        findInt = 0;
        findString = 0;
        int sizeString = inCh.length();
        std::vector<int> listInt;
        for(int i63 = 0; i63 < int(inCh.length()); i63++){
          listInt.push_back(0);
        }
        while(findInt < int(inCh.length())){
          bbCode = "<ul";
          find = inCh.find(bbCode.c_str(),findInt,3);
          if(find != std::string::npos){
            listInt[int(find)] = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        findInt = 0;
        while(findInt < int(inCh.length())){
          bbCode = "<ol";
          find = inCh.find(bbCode.c_str(),findInt,3);
          if(find != std::string::npos){
            listInt[int(find)] = 2;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,4),"list")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          listInt[int(find)] = 3;
          lengthInt = bbCode.length();
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,4),"list")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        for(int i63 = 0; i63 < int(listInt.size()); i63++){
          if(listInt[i63] == 1){
            listType.push_back(1);
          }
          if(listInt[i63] == 2){
            listType.push_back(2);
          }
          if(listInt[i63] == 3){
            listType.push_back(3);
          }
        }        
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,4),"list")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          int listStore = 0;
          int recentStore = 0;
          for(int i63 = 0; i63 < int(listType.size()); i63++){
            if(listType[i63] != 3 && listType[i63] != 0){
              listStore = listType[i63];
              recentStore = i63;
            }
            else if(listType[i63] == 3 && listStore == 2){
              inChTemp = inChTemp + "</ol";
              listType[recentStore] = 0;
              listType[i63] = 0;
              break;
            }
            else if(listType[i63] == 3 && listStore == 1){
              inChTemp = inChTemp + "</ul";
              listType[recentStore] = 0;
              listType[i63] = 0;
              break;
            }
          }
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 6;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,4),"list")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,6),"indent")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<blockquote";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 7;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,6),"indent")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,6),"indent")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</blockquote";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 8;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,6),"indent")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[";
          find = inCh.find(bbCode.c_str(),findInt,1);
          if(unSignedCompare(inCh.substr(int(find)+1,4),"code")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = "";
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "<div class=\"smallfont\" style=\"margin-bottom:2px\">Code:</div>";
          inChTemp = inChTemp + "\n" + "<pre class=\"alt2\" dir=\"ltr\" style=\"";
          inChTemp = inChTemp + "\n" + "margin: 0px;";
          inChTemp = inChTemp + "\n" + "padding: 8px;";
          inChTemp = inChTemp + "\n" + "border: 1px inset;";
          inChTemp = inChTemp + "\n" + "width: 640px;";
          inChTemp = inChTemp + "\n" + "height: 498px;";
          inChTemp = inChTemp + "\n" + "text-align: left;";
          inChTemp = inChTemp + "\n" + "overflow: auto\"";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 5;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+1,4),"code")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        
        findInt = 0;
        findString = 0;
        while(findString == 0 && findInt < int(inCh.length())){
          bbCode = "[/";
          find = inCh.find(bbCode.c_str(),findInt,2);
          if(unSignedCompare(inCh.substr(int(find)+2,4),"code")){
            findString = 1;
            findInt = int(find) + 1;
          }
          else{
            findInt++;
          }
        }
        while(find != std::string::npos && !htmlBBCode && findString){
          inChTemp = inCh.substr(0,int(find));
          inChTemp = inChTemp + "</pre";
          lengthInt = bbCode.length();
          find2 = inCh.find("]",find+1,1);
          startInt = int(find) + 6;
          endInt = int(find2) - startInt;
          inChTemp = inChTemp + inCh.substr(startInt,endInt); 
          inChTemp = inChTemp + ">";
          startInt2 = int(find2) + 1;
          lengthInt2 = inCh.length();
          endInt2 = lengthInt2 - startInt2;
          inChTemp = inChTemp + inCh.substr(startInt2, endInt2);
          inCh = inChTemp;
          findString = 0;
          findInt = int(find) + 1;
          while(findString == 0 && findInt < int(inCh.length())){
            bbCode = "[/";
            find = inCh.find(bbCode.c_str(),findInt,lengthInt);
            if(unSignedCompare(inCh.substr(int(find)+2,4),"code")){
              findString = 1;
              findInt = int(find) + 1;
            }
            else{
              findInt++;
            }
          }
        }
        outfile << inCh << std::endl << "<br>";
      }
      infile.close();
      outfile.close();
    }
    else{
      infile.close();
    }
  }

private: void reportProgressBar()
  {
      reportProgressBar_n = 1;
      // Using CDPI example class
      CDPI g_metrics;
      int dpiX = g_metrics.GetDPIX();
      parseBBCodeToolStripMenuItem->Visible = false;
      optionsToolStripMenuItem->Visible = false;
      txtsToolStripMenuItem->Visible = false;
      hTMLToolStripMenuItem->Visible = false;
      label1->Visible = false;
      label2->Visible = false;
      webBrowser1->Visible = false;
      listBox1->Visible = false;
      webBrowser1->Location = System::Drawing::Point(4, 31);
      int x = webBrowser1->Location.X + webBrowser1->Width + 10;
      int y = 31;
      listBox1->Location = System::Drawing::Point(x,y);
      button1->Visible = false;
      button2->Visible = false;
      
      button4->Visible = true;
      // Progress output
      label3->Visible = true;
      label3->Location = System::Drawing::Point(4,31);
      progressBar1->Visible = true;
      button4->Location = System::Drawing::Point(button4->Location.X,progressBar1->Location.Y + progressBar1->Height + int(double(10) * double(dpiX)/double(96)));
      y = button4->Location.Y + button4->Height + int(double(65)*double(dpiX)/double(96));
      x = button1->Width + button2->Width + 30;
      this->MaximumSize = System::Drawing::Size(x,y);
      this->MinimumSize = System::Drawing::Size(x,y);
      this->Height = x;
      this->Width = y;
      x = 0;
      y = this->Height - label12->Height - int(double(45) * double(dpiX)/double(96));
      this->label12->Location = System::Drawing::Point(x, y);
      CenterWindow *myWindow;
      myWindow = new CenterWindow;
      myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
      delete myWindow;
      
      // Start the asynchronous operation.
      backgroundWorker1->RunWorkerAsync();
  }
System::Void initializeHtml()
{
    progressBar1->Maximum = 100;
}

System::Void updateHtmlStatus()
{
    label3->Text = mStatus;
}

System::Void SafeInvoke(Control^ control, System::Action^ action)
{
    if (control != nullptr && !control->IsDisposed && control->InvokeRequired)
    {
        try
        {
            control->Invoke(action);
        }
        catch (System::ObjectDisposedException ^ excep)
        {
            LogUtil::logErrorAsync(excep->Message);
        }
    }
    else if (control != nullptr && !control->IsDisposed)
    {
        action();
    }
}

private: System::Void makeHTML(){
    std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
    std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
    std::ifstream inTimeDir(tempDir + "\\outTimeDir.txt");
    std::string timeDir;
    getline(inTimeDir,timeDir);
    inTimeDir.close();
    std::string inPath;
    std::ofstream outfile;
    std::string thisAddress;
    FileSystemUtil::createDirectoryWithBackup(timeDir, "html");

    std::vector<std::string> fileList;
    for(int i=0; i<fileListS->Length; i++)
    {
      if(backgroundWorker1->CancellationPending)
      {
        break;
      }
      fileList.push_back(SystemStandardConversionUtil::convertSystemStringToStdString(fileListS[i]));
    }

    this->SafeInvoke(this, gcnew System::Action(this, &Form3::initializeHtml));
    for(int i=0; i<int(fileList.size()); i++)
    {
      if(backgroundWorker1->CancellationPending)
      {
        break;
      }
      mStatus = "Converting " + fileListS[i] + ".txt to " + fileListS[i] + ".html";
      this->SafeInvoke(this, gcnew System::Action(this, &Form3::updateHtmlStatus));
      int percentComplete = int(double(i)/double(fileList.size()-1)*100.0);
      backgroundWorker1->ReportProgress( percentComplete );
      inPath = timeDir + "\\" + fileList[i] + ".txt";
      if(fileList[i].find("user") != std::string::npos)
      {
        inPath = timeDir + "\\userCommands\\" + fileList[i] + ".txt";
      }
      thisAddress = timeDir + "\\html\\" + fileList[i] + ".html";
      if(fileList[i] == "_97-debug" || fileList[i] == "_98-debug" || 
        fileList[i] == "_88-debug" || fileList[i] == "template" ||  
        fileList[i] == "importantInfo" || fileList[i] == "3rdPartyDriversName" ||
        fileList[i] == "3rdPartyDriversDate")
      {
        bbCodeToHTML(inPath, thisAddress);
      }
      else
      {
        spacesToHTML(inPath,thisAddress);
      }
    }
  }
  private: void spacesToHTML(std::string inPath, std::string thisAddress)
  {
    std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
    std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
    std::ifstream infile;
    std::ofstream outfile;
    infile.open(inPath);
    
    int htmlBBCode;
    std::string fullPathString = userProfile + "\\SysnativeBSODApps\\dmpOptions\\htmlBBCode.txt";
    std::ifstream inHtml(fullPathString);
    if(inHtml.good()){
      inHtml >> htmlBBCode;
    }
    else{
      htmlBBCode = 0;
    }
    if(htmlBBCode){
      parseBBCodeToolStripMenuItem->Checked = false;
    }
    else{
      parseBBCodeToolStripMenuItem->Checked = true;
    }
    inHtml.close();
    if(infile.good()){
      std::ofstream outPercentDone;
      outPercentDone.open(tempDir + "\\percentDone.txt");
      outPercentDone << 452312;
      outPercentDone.close();

      std::ofstream outProg;
      std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
      std::string tempPath;
      tempPath = tempDir + "\\analyzedDmpOf.txt";
      outProg.open(tempPath);
      outProg << "Creating _95-debug.html...";
      std::cout << "Creating _95-debug.html..." << std::endl;
      outProg.close();
      outfile.open(thisAddress);  
      while(!infile.eof()){
        std::string inCh;
        getline(infile,inCh);        
        for (int i = 0; i < int(inCh.length()); i++){
          if(inCh.c_str()[i]==' '){
            std::stringstream sstemp;
            sstemp << inCh.substr(0,i) << "&nbsp;" << inCh.substr(i+1,inCh.length()-i-1);
            inCh = sstemp.str();
          }
        }
        outfile << inCh << std::endl << "<br>";
      }
      infile.close();
      outfile.close();
    }
    else{
      infile.close();
    }
  }
private: System::Void resize(System::Object^  sender, System::EventArgs^  e) {
    // Using CDPI example class
    CDPI g_metrics;
    int dpiX = g_metrics.GetDPIX();
    webBrowser1->Width = this->Width - listBox1->Width - 40;
    webBrowser1->Height = this->Height - int(double(120) * double(dpiX)/double(96));
    int x = webBrowser1->Location.X + webBrowser1->Width + 10;
    int y = webBrowser1->Location.Y;
    if(!outputOrder_n)
    {
      listBox1->Location = System::Drawing::Point(x,y);
    }
    x = webBrowser1->Location.X + int(float(webBrowser1->Width - label1->Width)/2.0F);
    y = 31;
    label1->Location = System::Drawing::Point(x,y);
    x = listBox1->Location.X + int(float(listBox1->Width - label2->Width)/2.0F);
    y = 31;
    label2->Location = System::Drawing::Point(x,y);
    x = 0;
    y = this->Height - label12->Height - int(double(45) * double(dpiX)/double(96));
    this->label12->Location = System::Drawing::Point(x, y);
     }
public: System::Void centerForm3()
    {
      firstScreeni = 1;
      // Using CDPI example class
      CDPI g_metrics;
      int dpiX = g_metrics.GetDPIX();
      parseBBCodeToolStripMenuItem->Visible = false;
      optionsToolStripMenuItem->Visible = false;
      txtsToolStripMenuItem->Visible = false;
      hTMLToolStripMenuItem->Visible = false;
      label1->Visible = false;
      label2->Visible = false;
      webBrowser1->Visible = false;
      listBox1->Visible = false;
      webBrowser1->Location = System::Drawing::Point(4, 31);
      int x = webBrowser1->Location.X + webBrowser1->Width + 10;
      int y = 31;
      listBox1->Location = System::Drawing::Point(x,y);
      
      button1->Visible = true;
      button2->Visible = true;
      button4->Visible = true;
      // Progress output
      label3->Visible = false;
      progressBar1->Visible = false;
      this->Height = button4->Location.Y + button4->Height + int(double(65)*double(dpiX)/double(96));
      this->Width = button1->Width + button2->Width + 30;
      this->MaximumSize = System::Drawing::Size(this->Width,this->Height);
      this->MinimumSize = System::Drawing::Size(this->Width,this->Height);
      x = 0;
      y = this->Height - label12->Height - int(double(45) * double(dpiX)/double(96));
      this->label12->Location = System::Drawing::Point(x, y);
      CenterWindow *myWindow;
      myWindow = new CenterWindow;
      myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
      delete myWindow;
   }
private: System::Void form3Shown(System::Object^  sender, System::EventArgs^  e) {    
      // start output window
      std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
      std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
      std::ofstream inFirstScreen(tempDir + "\\firstScreen.txt");
      inFirstScreen << "1";
      inFirstScreen.close();
      TopMost = true;
      Hide();
      UpdateZOrder();
      BringToFront();
      SetForegroundWindow(static_cast<HWND>(this->Handle.ToPointer()));
      loadFiles();
      Show();
      TopMost = false;
    int innFinished = 0;
    std::ifstream inFinished(userProfile + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
    if(inFinished.good())
    {
    inFinished >> innFinished;
    }
    if(innFinished)
    {
      button4_Click(sender, e);
    }
    inFinished.close();
  }
// View HTML
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
   { 
     firstScreeni = 0;
     if(oo->okClicked)
     {
       fileListS = oo->fileListS;     
       listBox1->Items->Clear();   
       for(int j=0; j<fileListS->Length; j++)
       {
         listBox1->Items->Add(fileListS[j]);
       }
     }
     label4->Visible = false;
     viewToolStripMenuItem->Visible = false;
     reportProgressBar();
   }
// View .txts
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
    if(oo->okClicked)
    {
      fileListS = oo->fileListS;  
      listBox1->Items->Clear();      
      for(int j=0; j<fileListS->Length; j++)
      {
        listBox1->Items->Add(fileListS[j]);
      }
    }
    label4->Visible = false;
    std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
    std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
    std::string ODTimeDir;
    std::ifstream timeDir(tempDir + "\\outTimeDir.txt");
    getline(timeDir, ODTimeDir);
    timeDir.close();

    for(int i=fileListS->Length - 1; i>=0; i--)
    {
      if(SystemStandardConversionUtil::convertSystemStringToStdString(fileListS[i]).find("user") != std::string::npos)
      {
        std::string str;
        str = ODTimeDir + "\\userCommands\\" + SystemStandardConversionUtil::convertSystemStringToStdString(fileListS[i]) + ".txt";
        FileSystemUtil::openFile(str);
      }
      else
      {
        std::string str;
        str = ODTimeDir + "\\" + SystemStandardConversionUtil::convertSystemStringToStdString(fileListS[i]) + ".txt";
        FileSystemUtil::openFile(str);
      }
      Sleep(105);
    }
    if(firstScreeni){
      remove(std::string(tempDir + "\\firstScreen.txt").c_str());
      Close();
    }
    else{
    }
     }
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e)
  {
    cancelClicked = 1;
    firstScreeni = 0;
    label4->Visible = false;
    backgroundWorker1->CancelAsync();
    if(!reportProgressBar_n)
    {
      Close();
    }
    else
    {
      viewToolStripMenuItem->Visible = true;
      label4->Visible = true;
      centerForm3();
      form3Shown(sender, e);
    }
  }
public: void loadFiles()
        {
          std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
          std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
          array<System::String^>^ outputOrderS;
          System::Array::Resize(outputOrderS,7);
          outputOrderS[0] = "stack";
          outputOrderS[1] = "_97-debug";
          outputOrderS[2] = "_98-debug";
          outputOrderS[3] = "template";
          outputOrderS[4] = "_88-debug";
          outputOrderS[5] = "_99-debug";
          outputOrderS[6] = "_95-debug";
          int i1 = 0;
          while(i1 < fileListS->Length)
          {
            System::Array::Resize(outputOrderS,i1 + 1);
            outputOrderS[i1] = fileListS[i1];
            i1++;
          }
          std::string ODTimeDir;
          std::ifstream timeDir(tempDir + "\\outTimeDir.txt");
          getline(timeDir, ODTimeDir);
          timeDir.close();
          std::vector<std::string> fileList;
          fileList = FileSystemUtil::getFiles(ODTimeDir);
          int index = 0;
          for(int i=0; i<int(fileList.size()); i++)
          {            
            size_t find1 = fileList[i].find("\\");
            int j1 = 0;
            while(find1 != std::string::npos)
            {
              j1 = int(find1);
              find1 = fileList[i].find("\\",j1+1,1);
            }
            find1 = fileList[i].find(".txt");
            int j2 = 0;
            while(find1 != std::string::npos)
            {
              j2 = int(find1);
              find1 = fileList[i].find(".txt",j2+1,1);
            }
            fileList[i] = fileList[i].substr(j1+1,j2-j1-1);
            System::Array::Resize(fileListS,index+1);
            fileListS[index] = gcnew System::String(fileList[i].c_str());
            index++;
          }
          fileList.clear();
          fileList = FileSystemUtil::getFiles(ODTimeDir + "\\userCommands");
          for(int i=0; i<int(fileList.size()); i++)
          {            
            size_t find1 = fileList[i].find("\\");
            int j1 = 0;
            while(find1 != std::string::npos)
            {
              j1 = int(find1);
              find1 = fileList[i].find("\\",j1+1,1);
            }
            find1 = fileList[i].find(".txt");
            int j2 = 0;
            while(find1 != std::string::npos)
            {
              j2 = int(find1);
              find1 = fileList[i].find(".txt",j2+1,1);
            }
            fileList[i] = fileList[i].substr(j1+1,j2-j1-1);
            System::Array::Resize(fileListS,index+1);
            fileListS[index] = gcnew System::String(fileList[i].c_str());
            index++;
          }
          for(int i=0; i<outputOrderS->Length; i++)
          {
            for(int j=0; j<fileListS->Length; j++)
            {
              if(outputOrderS[i] == fileListS[j])
              {
                int i2;
                if(i > fileListS->Length - 1)
                {
                  i2 = fileListS->Length - 1;
                }
                else
                {
                  i2 = i;
                }
                System::String^ tempS = fileListS[j];
                fileListS[j] = fileListS[i2];
                fileListS[i2] = tempS;
              }
            }
          }
          listBox1->Items->Clear();
          for(int j=0; j<fileListS->Length; j++)
          {
            listBox1->Items->Add(fileListS[j]);
          }
        }
// Whether or not to parse BBCode
private: System::Void parseBBCodeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
  {    
    firstScreeni = 0;
    if(oo->okClicked)
    {
      fileListS = oo->fileListS; 
      listBox1->Items->Clear();
      for(int j=0; j<fileListS->Length; j++)
      {
        listBox1->Items->Add(fileListS[j]);
      }
    }
    label4->Visible = false;
    std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
    std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));

    std::string fullPathString = userProfile + "\\SysnativeBSODApps\\dmpOptions\\htmlBBCode.txt";
    std::ofstream outfile;
    outfile.open(fullPathString);
    if(parseBBCodeToolStripMenuItem->Checked){
      outfile << 0;
    }
    else{
      outfile << 1;
    }
    outfile.close();
    reportProgressBar();
  }
// change output order
private: System::Void windowsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
    {
     firstScreeni = 0;
     label4->Visible = true;
     centerForm3();
     form3Shown(sender, e);
     oo = gcnew outputOrder();
     oo->okClicked = 0;
     oo->cancelClicked = 0;
     oo->fileListS = fileListS;
     oo->ShowDialog();
    }
private: System::Void txtsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
           firstScreeni = 0;
           label4->Visible = false;
           outputOrder_n = 0;
           button2_Click(sender,e);
         }
private: System::Void listBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
           if(!outputOrder_n)
           {
             std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
             std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
             std::ifstream inTimeDir(tempDir + "\\outTimeDir.txt");
             std::string timeDir;
             getline(inTimeDir,timeDir);
             inTimeDir.close();
             System::String^ timeDirS = gcnew System::String(timeDir.c_str());
             label1->Text = listBox1->SelectedItem->ToString();
             this->webBrowser1->Navigate( timeDirS + "\\html\\" + listBox1->SelectedItem->ToString() + ".html");
             int x = webBrowser1->Location.X + int(float(webBrowser1->Width - label1->Width)/2.0F);
             int y = 31;
             label1->Location = System::Drawing::Point(x,y);
           }
         }
private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
           makeHTML();
         }
private: System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e) 
  {
    this->progressBar1->Value = e->ProgressPercentage;
  }
private: System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) 
  {
    reportProgressBar_n = 0;
    if(!cancelClicked)
    {
      std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
      std::string userProfile = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
      outputOrder_n = 0;
      this->MaximumSize = System::Drawing::Size(0,0);
      this->MinimumSize = System::Drawing::Size(0,0);
      // Using CDPI example class
      CDPI g_metrics;
      int dpiX = g_metrics.GetDPIX();      
      button1->Visible = false;
      button2->Visible = false;
      button4->Visible = false;
      label3->Visible = false;
      progressBar1->Visible = false;
      hTMLToolStripMenuItem->Visible = true;
      hTMLToolStripMenuItem->Visible = false;
      viewToolStripMenuItem->Visible = true;
      
      parseBBCodeToolStripMenuItem->Visible = true;
      optionsToolStripMenuItem->Visible = true;
      label1->Visible = true;
      label2->Visible = true;
      listBox1->SelectedIndex = 0;
      label1->Text = listBox1->SelectedItem->ToString();
      std::ifstream inTimeDir(tempDir + "\\outTimeDir.txt");
      std::string timeDir;
      getline(inTimeDir,timeDir);
      inTimeDir.close();
      System::String^ timeDirS = gcnew System::String(timeDir.c_str());
      this->webBrowser1->Navigate( timeDirS + "\\html\\" + listBox1->SelectedItem->ToString() + ".html");
      webBrowser1->Visible = true;
      windowsToolStripMenuItem->Visible = true;      
      txtsToolStripMenuItem->Visible = true;
      listBox1->Visible = true;
      webBrowser1->Location = System::Drawing::Point(webBrowser1->Location.X, webBrowser1->Location.Y + label1->Height + int(double(10)*double(dpiX)/double(96)));
      int x = webBrowser1->Location.X + int(float(webBrowser1->Width - label1->Width)/2.0F);
      int y = 31;
      label1->Location = System::Drawing::Point(x,y);
      x = listBox1->Location.X + int(float(listBox1->Width - label2->Width)/2.0F);
      y = 31;
      label2->Location = System::Drawing::Point(x,y);
      x = webBrowser1->Location.X + webBrowser1->Width + 10;
      y = webBrowser1->Location.Y;
      listBox1->Location = System::Drawing::Point(x,y);
      // Progress output
      this->Height = 600;
      this->Width = 720 + listBox1->Width;
      x = 0;
      y = this->Height - label12->Height - int(double(45) * double(dpiX)/double(96));
      this->label12->Location = System::Drawing::Point(x, y);
      CenterWindow *myWindow;
      myWindow = new CenterWindow;
      myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
      delete myWindow;
    }
    cancelClicked = 0;
  }
private: System::Void hTMLToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
           firstScreeni = 0;
           label4->Visible = false;
           button1_Click(sender, e);
         }
};
}
