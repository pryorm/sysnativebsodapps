#include "SysnativeFileSystemManager.h"

#include "ConfigurationUtil.h"
#include "FileSystemUtil.h"
#include "LogUtil.h"
#include "SystemStandardConversionUtil.h"

#include "debug.h"

#include <fstream>

std::vector<std::string> SysnativeFileSystemManager::dmpCheck(std::string tempDir)
{
  using namespace System;
  String^ myPath = IO::Directory::GetCurrentDirectory();
  array<String^>^ files = IO::Directory::GetFiles(myPath);
  std::vector<std::string> foundDmps;
  for (int j=0; j < files->Length; j++)
  {
    if((files[j]->ToLower()->Contains("dmp")) || (files[j]->ToLower()->Contains("txt"))
                                           || (files[j]->ToLower()->Contains("dump")))
    {
      using namespace System::IO;
      using namespace System::Text;
      StreamReader^ fileText = nullptr;
      try
      {
        fileText = gcnew StreamReader(files[j]);
      }
      catch (System::Exception^ excep)
      {
          LogUtil::logErrorAsync(excep->Message);
      }
      String^ line;
      // Read lines from the file until the end of the file is reached.
      if(fileText)
      {
        line = fileText->ReadLine();
        delete (IDisposable^)fileText;
      }
      if (!line)
      {
        std::string dmpName;
        dmpName = SystemStandardConversionUtil::convertSystemStringToStdString(System::IO::Path::GetFileName(files[j]));
		std::ofstream outError;
        outError.open(tempDir + "\\outError.txt", std::ios::app);
        outError << dmpName << " may be incomplete; there is a size mismatch  " << std::endl;
        outError.close();
        try
        {
            System::IO::File::Delete(files[j]);
        }
        catch (System::Exception ^ excep)
        {
            LogUtil::logErrorAsync(excep->Message);
        }
        emptyFiles.push_back(dmpName);
        continue;
      }
      else if(line->Contains("PAGEDU") || line->Contains("MDMP"))
      {
        std::string dmpName;
        dmpName = SystemStandardConversionUtil::convertSystemStringToStdString(System::IO::Path::GetFileName(files[j]));
        foundDmps.push_back(dmpName);
      }
    }
  }
  dmpsExistVal = static_cast<int>(foundDmps.size());
  return foundDmps;
}

const std::vector<std::string>& SysnativeFileSystemManager::getEmptyFiles()const
{
    return emptyFiles;
}

void SysnativeFileSystemManager::setUpDirectoryStructure(int form)
{
    std::string tempDir = ConfigurationUtil::getTempDirectory();
    std::string userProfile = ConfigurationUtil::getUserProfilePath();

    std::ifstream inOutputDmpsSubDir(tempDir + "\\outputDmpsSubDir.txt");
    getline(inOutputDmpsSubDir, mTimeDir);
    inOutputDmpsSubDir.close();

    System::String^ outputDmpsDirPath = gcnew System::String(outputDmpsDir.c_str());
    System::String^ timeDirPath = gcnew System::String(mTimeDir.c_str());
    System::String^ userProfilePath = gcnew System::String(userProfile.c_str());

    // If the outputDmps directory does not exist,
    //    create the directory

    System::String^ myPath = System::IO::Directory::GetCurrentDirectory();
    FileSystemUtil::createDirectoryWithBackup(myPath, outputDmpsDirPath);

    // Create an output subdirectory based on the date/time
    if (!form) {
        FileSystemUtil::createDirectoryWithBackup(outputDmpsDirPath, timeDirPath);
    }

    FileSystemUtil::createDirectoryWithBackup(userProfilePath, "SysnativeBSODApps");
    FileSystemUtil::createDirectoryWithBackup(userProfilePath + "\\SysnativeBSODApps", "backup");
    FileSystemUtil::createDirectoryWithBackup(userProfilePath + "\\SysnativeBSODApps", "dmpOptions");
    FileSystemUtil::createDirectoryWithBackup(userProfilePath + "\\SysnativeBSODApps", "download");
    FileSystemUtil::createDirectoryWithBackup(userProfilePath + "\\SysnativeBSODApps", "forumSettings");
    FileSystemUtil::createDirectoryWithBackup(userProfilePath + "\\SysnativeBSODApps", "parms");
    FileSystemUtil::createDirectoryWithBackup(userProfilePath + "\\SysnativeBSODApps", "driverStatistics");
    parms2 = userProfile + "\\SysnativeBSODApps\\parms";
    mTimeDir = SystemStandardConversionUtil::convertSystemStringToStdString(myPath) + "\\" + outputDmpsDir + "\\" + mTimeDir;
}

void SysnativeFileSystemManager::getCurrentSubdirectories()
{
    std::string tempDir = ConfigurationUtil::getTempDirectory();
    std::ifstream inOutputDmpsDir(tempDir + "\\outputDmpsDir.txt");
    getline(inOutputDmpsDir, outputDmpsDir);
    inOutputDmpsDir.close();
}

void SysnativeFileSystemManager::deleteTemporaryDirectory()
{
}

void SysnativeFileSystemManager::deleteDmpOptionsDirectory()
{
    System::IO::Directory::Delete(SystemStandardConversionUtil::convertStdStringToSystemString(mTimeDir));
}

const std::string& SysnativeFileSystemManager::getParms()const
{
    return parms2;
}

const std::string& SysnativeFileSystemManager::getTimeDir()const
{
    return mTimeDir;
}

int SysnativeFileSystemManager::dmpsExist()const
{
    return dmpsExistVal;
}

void SysnativeFileSystemManager::setTimeDir(const std::string& timeDir)
{
    mTimeDir = timeDir;
}