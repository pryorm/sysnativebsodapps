#include "Form2.h"

#include "ConfigurationUtil.h"

#include "debug.h"

using namespace SysnativeBSODApps;


System::Void Form2::button1_Click(System::Object^  sender, System::EventArgs^  e) 
{
  Form2::Close();
}

System::Void Form2::button2_Click(System::Object^  sender, System::EventArgs^  e) {
  Form2::Close();
}

System::Void Form2::goToWebpage(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e) {
  this->TopMost = false;
  System::Diagnostics::Process::Start("https://www.sysnative.com/forums");
}

System::Void Form2::Form2_Load(System::Object^  sender, System::EventArgs^  e) {
  TopMost = true;
  Hide();
  UpdateZOrder();
  BringToFront();
  SetForegroundWindow(static_cast<HWND>(this->Handle.ToPointer()));
  Show();
  TopMost = false;

  // Read user profile and save it to the userprofile string variable
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  int inTopi = 1;
  std::ifstream inTop(userProfile + "\\SysnativeBSODApps\\dmpOptions\\alwaysOnTop.txt");
  if(inTop.good())
  {
    inTop >> inTopi;
  }
  inTop.close();
  if(inTopi)
  {
    TopMost = true;
  }
}