﻿

#include "debug.h"

//#include <vld.h>

#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for outputOrder
  /// </summary>
  public ref class outputOrder : public System::Windows::Forms::Form
  {
  public:    
	cli::array<System::String^>^ fileListS;
    static int okClicked = 0;
    static int cancelClicked = 0;
    outputOrder(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~outputOrder()
    {
      if (components)
      {
        delete components;
      }
    }
  private: System::Windows::Forms::Button^  button6;
  protected: 
  private: System::Windows::Forms::Button^  button5;
  private: System::Windows::Forms::Label^  label2;
  public: System::Windows::Forms::ListBox^  listBox1;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::Button^  button2;
  private: System::Windows::Forms::Panel^  panel1;

  protected: 




  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(outputOrder::typeid));
      this->button6 = (gcnew System::Windows::Forms::Button());
      this->button5 = (gcnew System::Windows::Forms::Button());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->listBox1 = (gcnew System::Windows::Forms::ListBox());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->panel1 = (gcnew System::Windows::Forms::Panel());
      this->SuspendLayout();
      // 
      // button6
      // 
      this->button6->Location = System::Drawing::Point(274, 159);
      this->button6->Name = L"button6";
      this->button6->Size = System::Drawing::Size(75, 23);
      this->button6->TabIndex = 46;
      this->button6->Text = L"↓";
      this->button6->UseVisualStyleBackColor = true;
      this->button6->Click += gcnew System::EventHandler(this, &outputOrder::button6_Click);
      // 
      // button5
      // 
      this->button5->Location = System::Drawing::Point(274, 130);
      this->button5->Name = L"button5";
      this->button5->Size = System::Drawing::Size(75, 23);
      this->button5->TabIndex = 45;
      this->button5->Text = L"↑";
      this->button5->UseVisualStyleBackColor = true;
      this->button5->Click += gcnew System::EventHandler(this, &outputOrder::button5_Click);
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(9, 13);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(63, 13);
      this->label2->TabIndex = 44;
      this->label2->Text = L"Output Files";
      // 
      // listBox1
      // 
      this->listBox1->FormattingEnabled = true;
      this->listBox1->Location = System::Drawing::Point(12, 32);
      this->listBox1->Name = L"listBox1";
      this->listBox1->Size = System::Drawing::Size(256, 251);
      this->listBox1->TabIndex = 43;
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(283, 343);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(75, 23);
      this->button1->TabIndex = 48;
      this->button1->Text = L"Cancel";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &outputOrder::button1_Click);
      // 
      // button2
      // 
      this->button2->Location = System::Drawing::Point(193, 343);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(75, 23);
      this->button2->TabIndex = 47;
      this->button2->Text = L"OK";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &outputOrder::button2_Click);
      // 
      // panel1
      // 
      this->panel1->BackColor = System::Drawing::SystemColors::ActiveCaptionText;
      this->panel1->Location = System::Drawing::Point(4, 331);
      this->panel1->Name = L"panel1";
      this->panel1->Size = System::Drawing::Size(360, 2);
      this->panel1->TabIndex = 49;
      // 
      // outputOrder
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(370, 474);
      this->Controls->Add(this->panel1);
      this->Controls->Add(this->button1);
      this->Controls->Add(this->button2);
      this->Controls->Add(this->button6);
      this->Controls->Add(this->button5);
      this->Controls->Add(this->label2);
      this->Controls->Add(this->listBox1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Name = L"outputOrder";
      this->Text = L"Output Order";
      this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &outputOrder::outputOrder_FormClosed);
      this->Load += gcnew System::EventHandler(this, &outputOrder::outputOrder_Load);
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
    // OK button
  private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
    // Cancel button
  private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
    // Up button
  private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e);
    // Down button
  private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void outputOrder_Load(System::Object^  sender, System::EventArgs^  e);
  private: System::Void outputOrder_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e);
};
}
