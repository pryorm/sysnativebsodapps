#ifndef BUGCHECKDOWNLOAD_H
#define BUGCHECKDOWNLOAD_H
#include <string>
#include <vector>

class BugCheckDownload{
public:
  std::string tempString_s;
  BugCheckDownload();
  virtual ~BugCheckDownload();
  void downloadBugCheckInfo();
  void parseBugCheckInfo();
  std::vector<std::string> bugCheckCode;
  std::vector<std::string> bugCheckCauses;
  std::vector<std::string> bugCheckLink;
};


#endif