#pragma once
#include <unordered_map>

class DrtBuilder
{
public:
    DrtBuilder();
	bool parseDriverInfo(std::string htmlFile);
	void writeDriverInfoToFile(std::string filename)const;
	std::vector<std::string> getDriverInfo()const;
	int getMaxPages(std::string htmlFile)const;
private:
    void buildDriverNameProviderMap();

	std::vector<std::string> driverInfo;
	std::unordered_map<std::string, std::string> driverUrlMap;
    std::unordered_map<std::string, std::string> driverNameProviderMap;
};