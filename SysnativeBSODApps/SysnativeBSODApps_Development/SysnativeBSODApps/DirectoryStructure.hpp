#ifndef DIRECTORYSTRUCTURE_HPP
#define DIRECTORYSTRUCTURE_HPP
//-----------------------------------------------------------------------------
//  File:         DirectoryStructure c++ class
//  Description:  Generates the directory structure for the output files
//  Project:      dmpAnalysis
//  Author:       Mikael Pryor
//  Date:         July  28, 2012
//  Changes:      July  29, 2012
//  Changes Made: Added functions for obtaining the current driver structure
//                and for generating the directories needed for output files.
//                Also obtained the user profile directory.
//    07/29/2012: Added SysnativeBSODApps directory listings
//                Added the deleteTempDirectory() function.
//    11/10/2024: Deprecated!!
//-----------------------------------------------------------------------------

#include <string>

class DirectoryStructure {
public:
protected:

private:
};

#endif