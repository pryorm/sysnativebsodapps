#pragma once

#include "CenterWindow.h"
#include "ConfigurationUtil.h"
#include "LogUtil.h"
#include "SystemStandardConversionUtil.h"

#include <ctime>
#include <Windows.h>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>

#include "debug.h"

namespace SysnativeBSODApps {

/// <summary>
/// Creates a progress bar and tracks progress of a given task
/// </summary>
public ref class Progress : public System::Windows::Forms::Form
{
public:
    Progress();

protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~Progress();

    int mProgressVal;
    int mProgressMax;
    System::String^ mStatus;
    System::String^ mHeader;
    bool mCheckBox1Checked;
    System::String^ mKdDefaultOutput;
    System::String^ mKdUserOutput;
    int mKdFilesProcessed;
    int mUserFilesProcessed;
    int mErrorsProcessed;
    System::String^ mLastSearchTerm;
private:
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void);
    System::Void safeInvoke(Control^ const control, System::Action^ const action);
    System::Void loadProgress(System::Object^ sender, System::EventArgs^ e);
    System::Void initializeProgressBar();
    System::Void updateProgressBar();
    System::Void updateWindow();
    System::Void updateZOrder();

    System::Windows::Forms::ProgressBar^  mProgressDisplay;
    System::Windows::Forms::Label^  mStatusDisplay;
    System::ComponentModel::BackgroundWorker^ mBackgroundProgressUpdater;
    System::ComponentModel::BackgroundWorker^ mBackgroundKdOutputBuilder;
    System::Windows::Forms::TextBox^  mDefaultKdDisplay;
    System::Windows::Forms::CheckBox^  mShowDefaultKd;
    System::Windows::Forms::CheckBox^  mShowUserKd;
    System::Windows::Forms::Label^  mCopyrightDisplay;
    System::Windows::Forms::TextBox^  mErrorDisplay;
    System::Windows::Forms::TextBox^  mUserKdDisplay;
    System::Windows::Forms::Label^  mNotifyDisplay;
    System::Windows::Forms::MenuStrip^  mMenu;
    System::Windows::Forms::ToolStripMenuItem^  mOptions;
    System::Windows::Forms::ToolStripMenuItem^  mAlwaysOnTop;
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container^ mComponents;

  private: System::Void hideControls()
  {
      mShowDefaultKd->Visible = false;
      mShowUserKd->Visible = false;
      mNotifyDisplay->Visible = false;
      mDefaultKdDisplay->Visible = false;
      mUserKdDisplay->Visible = false;
  }

  private: System::Void showCheckboxesAndLabel()
  {
      mShowDefaultKd->Visible = true;
      mShowUserKd->Visible = true;
      mNotifyDisplay->Visible = true;
  }

  System::Void uncheckCheckBoxes()
  {
      mShowDefaultKd->Checked = false;
      mShowUserKd->Checked = false;
  }

  private: System::Void updateProgress(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
         int countBad = 0;

         clock_t t01, t02;
         t01 = clock();
         t02 = clock();

         int countErrorLines = 0;
         this->safeInvoke(this, gcnew System::Action(this, &Progress::initializeProgressBar));
         while(mProgressVal < mProgressMax){
          Sleep(465);
          int userOpened = 0;
          int kdOpened = 0;
          this->Text = L"Sysnative BSOD Processing Progress";
          std::string tempDir = ConfigurationUtil::getTempDirectory();
          std::string tempPath;
          tempPath = tempDir + "\\percentDone.txt";
          std::ifstream infile(tempPath);
          int percentDone = mProgressVal;
          if(infile.good()){
            infile >> percentDone;
            countBad = 0;
                    }
          else{
            countBad++;
            //this->Text = countBad.ToString();
            if(countBad > 100){
              percentDone = 100;
            }
          }
          infile.close();

          double newValue = 0.0;
          if(percentDone == 339155){
            Close();
          }
          if(percentDone == 452312){
              this->safeInvoke(this, gcnew System::Action(this, &Progress::updateWindow));
          }
          clock_t t03, t04;
          t03 = clock();
          t04 = clock();
          if(percentDone == 339155){
            Close();
          }
          this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
          while(percentDone == 452312){
            Sleep(465);
            this->safeInvoke(this, gcnew System::Action(this, &Progress::hideControls));
            std::string tempDir = ConfigurationUtil::getTempDirectory();
            std::string tempPath;
            tempPath = tempDir + "\\analyzedDmpOf.txt";
            infile.open(tempPath);
            std::string analyzedString = "";
            if(infile.good()){
              getline(infile, analyzedString);
            }
            infile.close();
            mStatus = gcnew System::String(analyzedString.c_str());
            this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
            
            t04 = clock();
            float diff01 = ((float)t04 - (float)t03) / 1000.0F;
            if(diff01 > 0.5){
              t03 = clock();
              t04 = clock();
              newValue += 1.0;
            }
            if(newValue > 90){
              newValue = 0.0;
            }  
            mBackgroundProgressUpdater->ReportProgress(int(newValue));                     

              tempPath = tempDir + "\\outError.txt";
              infile.open(tempPath);
              std::string header1 = "";
              countErrorLines = 0;
              std::string inCh = "";
              while (getline(infile, inCh))
              {
                  countErrorLines++;
                  if (countErrorLines > mErrorsProcessed)
                  {
                      header1 = header1 + inCh;
                      if (!inCh.empty()) {
                          header1 = header1 + "\n\r\n\r";
                      }
                      ++mErrorsProcessed;
                  }
              }

            t02 = clock();
            diff01 = ((float)t02 - (float)t01) / 1000.0F;
            if(!header1.empty()){
              mHeader = gcnew System::String(header1.c_str());
              this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
              Sleep(15); // Provide time for safeInvoke
            }
            infile.close();  
            if(diff01 > 2.0){
              t01 = clock();
              t02 = clock();
              this->safeInvoke(this, gcnew System::Action(this, &Progress::Refresh));
            }
            Sleep(465);
              tempPath = tempDir + "\\percentDone.txt";
              infile.open(tempPath);
            if(infile.good()){
              infile >> percentDone;
              countBad = 0;
            }
            else{
              countBad++;
              //this->Text = countBad.ToString();
              if(countBad > 100){
                percentDone = 100;
              }
            }
            infile.close();
          }  
          int percentDoneStore = percentDone;
          if(percentDoneStore == 339154){
            this->safeInvoke(this, gcnew System::Action(this, &Progress::Hide));
          }
          if(percentDoneStore == 339155){
            this->safeInvoke(this, gcnew System::Action(this, &Progress::Close));
          }
          while(percentDone == 339154){
            Sleep(465);
            std::string tempDir = ConfigurationUtil::getTempDirectory();
            std::string tempPath;
            tempPath = tempDir + "\\percentDone.txt";
            infile.open(tempPath);          
            if(infile.good()){
              infile >> percentDone;
              countBad = 0;
            }
            else{
              countBad++;
              //this->Text = countBad.ToString();
              if(countBad > 100){
                percentDone = 100;
              }
            }
            infile.close();
            
            t02 = clock();
            float diff01 = ((float)t02 - (float)t01) / 1000.0F;
            if(diff01 > 2.0){
              t01 = clock();
              t02 = clock();
              this->safeInvoke(this, gcnew System::Action(this, &Progress::Refresh));
            }
            Sleep(465);
          }
          if(percentDoneStore == 339155){
            Close();
          }
          if(percentDoneStore == 339154){
              this->safeInvoke(this, gcnew System::Action(this, &Progress::updateZOrder));
          }

          this->safeInvoke(this, gcnew System::Action(this, &Progress::showCheckboxesAndLabel));

          tempPath = tempDir + "\\analyzedDmpOf.txt";
          infile.open(tempPath);
          std::string analyzedString = "";
          if(infile.good()){
            getline(infile, analyzedString);
          }
          infile.close();
          mStatus = gcnew System::String(analyzedString.c_str());
          this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
          if(percentDone > 100){
            percentDone = 0;
          }
          mBackgroundProgressUpdater->ReportProgress(percentDone);             

          std::string inPath = tempDir + "\\outError.txt";
          infile.open(inPath);
          std::string header1 = "";
          countErrorLines = 0;
          std::string inCh = "";
          while(getline(infile, inCh))
          {
              countErrorLines++;
              if (countErrorLines > mErrorsProcessed)
              {
                  header1 = header1 + inCh;
                  if (!inCh.empty()) {
                      header1 = header1 + "\n\r\n\r";
                  }
                  ++mErrorsProcessed;
              }
          }
          t02 = clock();
          float diff01 = ((float)t02 - (float)t01) / 1000.0F;
          if(!header1.empty()){
            mHeader = gcnew System::String(header1.c_str());
            this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
            Sleep(15); // Provide time for safeInvoke
          }
          infile.close();  
          if(diff01 > 2.0){
            t01 = clock();
            t02 = clock();
            this->safeInvoke(this, gcnew System::Action(this, &Progress::Refresh));
          }

          this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
          
          mNotifyDisplay->Text = L"(These may take time to load...)";

          if((!kdOpened && !userOpened) || (!mShowDefaultKd->Checked && !mShowUserKd->Checked)){
            t02 = clock();
            float diff01 = ((float)t02 - (float)t01) / 1000.0F;
            if(diff01 > 2.0){
              t01 = clock();
              t02 = clock();
              this->safeInvoke(this, gcnew System::Action(this, &Progress::Refresh));
            }
            Sleep(465);
          }
          else if(mShowDefaultKd->Checked && kdOpened){
            while(mShowDefaultKd->Checked){
              Sleep(465);
              infile.open(tempDir + "\\analyzedDmpOf.txt");
              std::string analyzedString = "";
              if(infile.good()){
                getline(infile, analyzedString);
              }
              infile.close();
              mStatus = gcnew System::String(analyzedString.c_str());
              this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
                
              infile.open(tempDir + "\\percentDone.txt");            
              if(infile.good()){
                infile >> percentDone;
                countBad = 0;
              }
              else{
                countBad++;
                //this->Text = countBad.ToString();
                if(countBad > 100){
                  percentDone = 100;
                }
              }
              infile.close();

              if(percentDone > 100){
                this->safeInvoke(this, gcnew System::Action(this, &Progress::uncheckCheckBoxes));
                percentDone = 0;
                break;
              }

              mBackgroundProgressUpdater->ReportProgress(percentDone);             

              std::string inPath = tempDir + "\\outError.txt";
              infile.open(inPath);
              std::string header1 = "";
              countErrorLines = 0;
              std::string inCh = "";
              while (getline(infile, inCh))
              {
                  countErrorLines++;
                  if (countErrorLines > mErrorsProcessed)
                  {
                      header1 = header1 + inCh;
                      if (!inCh.empty()) {
                          header1 = header1 + "\n\r\n\r";
                      }
                      ++mErrorsProcessed;
                  }
              }

              t02 = clock();
              diff01 = ((float)t02 - (float)t01) / 1000.0F;
              if(!header1.empty()){
                mHeader = gcnew System::String(header1.c_str());
                this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
                Sleep(15); // Provide time for safeInvoke
              }
              infile.close();  
              if(diff01 > 2.0){
                t01 = clock();
                t02 = clock();
                this->safeInvoke(this, gcnew System::Action(this, &Progress::Refresh));
              }
            }
          }
          else if(mShowUserKd->Checked && userOpened){
            while(mShowUserKd->Checked){
              Sleep(465);
              infile.open(tempDir + "\\analyzedDmpOf.txt");
              std::string analyzedString = "";
              if(infile.good()){
                getline(infile, analyzedString);
              }
              infile.close();
              mStatus = gcnew System::String(analyzedString.c_str());
              this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
                
              infile.open(tempDir + "\\percentDone.txt");            
              if(infile.good()){
                infile >> percentDone;
                countBad = 0;
              }
              else{
                countBad++;
                //this->Text = countBad.ToString();
                if(countBad > 100){
                  percentDone = 100;
                }
              }
              infile.close();

              if(percentDone > 100){
                this->safeInvoke(this, gcnew System::Action(this, &Progress::uncheckCheckBoxes));
                percentDone = 0;
                break;
              }

              mBackgroundProgressUpdater->ReportProgress(percentDone);             

              std::string inPath = tempDir + "\\outError.txt";
              infile.open(inPath);
              std::string header1 = "";
              countErrorLines = 0;
              std::string inCh = "";
              while (getline(infile, inCh))
              {
                  countErrorLines++;
                  if (countErrorLines > mErrorsProcessed)
                  {
                      header1 = header1 + inCh;
                      if (!inCh.empty()) {
                          header1 = header1 + "\n\r\n\r";
                      }
                      ++mErrorsProcessed;
                  }
              }

              t02 = clock();
              diff01 = ((float)t02 - (float)t01) / 1000.0F;
              if(!header1.empty()){
                mHeader = gcnew System::String(header1.c_str());
                this->safeInvoke(this, gcnew System::Action(this, &Progress::updateProgressBar));
                Sleep(15); // Provide time for safeInvoke
              }
              infile.close();  
              if(diff01 > 2.0){
                t01 = clock();
                t02 = clock();
                Refresh();
              }
            }
          }
         }
        std::string tempDir = ConfigurationUtil::getTempDirectory();
        std::ofstream outExitFile(tempDir + "\\outExitFile.txt");
        outExitFile << 1;
        outExitFile.close();
        std::string tempPath = tempDir + "\\progressRunning.txt";
        remove(tempPath.c_str());
        this->safeInvoke(this, gcnew System::Action(this, &Progress::Close));
       }
  private: System::Void progressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e) {
        // Change the value of the ProgressBar to the BackgroundWorker progress.
        mProgressDisplay->Value = e->ProgressPercentage;
       }
         
    System::Void appendTextBox(System::Windows::Forms::TextBox^ textBox, System::String^ text)
    {
        if (textBox->Visible)
        {
            // Save current caret position
            int caretPos = textBox->SelectionStart;

            // Disable painting of the text box
            SendMessage((HWND)textBox->Handle.ToPointer(), WM_SETREDRAW, FALSE, 0);

            // Append text
            textBox->AppendText(text);

            // Restore caret position
            textBox->SelectionStart = caretPos;

            // Re-enable painting of the text box
            SendMessage((HWND)textBox->Handle.ToPointer(), WM_SETREDRAW, TRUE, 0);

            // Force the text box to repaint
            textBox->Invalidate();
            textBox->Update();
        }
        else
        {
            // Append text
            textBox->AppendText(text);
        }
    }

    System::Void updateKdOutput()
    {
        appendTextBox(mDefaultKdDisplay, mKdDefaultOutput);
        appendTextBox(mUserKdDisplay, mKdUserOutput);
    }

    void searchTextBox(System::Windows::Forms::TextBox^ textBox, System::String^ searchTerm, bool findNext)
    {
        // Check if searchTerm is valid
        if (System::String::IsNullOrEmpty(searchTerm))
        {
            return;
        }

        int startPos;
        if (findNext)
        {
            startPos = textBox->Text->IndexOf(searchTerm, textBox->SelectionStart + textBox->SelectionLength, System::StringComparison::InvariantCultureIgnoreCase);
        }
        else
        {
            startPos = textBox->Text->LastIndexOf(searchTerm, textBox->SelectionStart - 1, System::StringComparison::InvariantCultureIgnoreCase);
        }

        if (startPos >= 0)
        {
            // Highlight the found text
            textBox->SelectionStart = startPos;
            textBox->SelectionLength = searchTerm->Length;
            textBox->ScrollToCaret();
        }
        else
        {
            // Optionally, provide feedback if the search term is not found
            System::Windows::Forms::MessageBox::Show("Search term not found.");
        }
    }

    void Form_KeyDown(System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e)
    {
        if (e->Control && e->KeyCode == System::Windows::Forms::Keys::F)
        {
            // CTRL + F detected, open search dialog or trigger search function
            mLastSearchTerm = PromptForSearchTerm();
            searchTextBoxes(mLastSearchTerm, true);
        }
        if (e->KeyCode == System::Windows::Forms::Keys::F3)
        {
            // F3 detected, find next occurrence
            searchTextBoxes(mLastSearchTerm, true);
        }
        if (e->Shift && e->KeyCode == System::Windows::Forms::Keys::F3)
        {
            // Shift + F3 detected, find previous occurrence
            searchTextBoxes(mLastSearchTerm, false);
        }
    }

    void inputForm_KeyDown(System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e)
    {
        if (e->KeyCode == System::Windows::Forms::Keys::Escape)
        {
            (safe_cast<System::Windows::Forms::Form^>(sender))->Close();
        }
    }

    System::String^ PromptForSearchTerm()
    {
        if (!mDefaultKdDisplay->Visible && !mUserKdDisplay->Visible)
        {
            return "";
        }

        // Create a simple input box
        System::Windows::Forms::Form^ inputForm = gcnew System::Windows::Forms::Form();
        inputForm->Text = "Search";
        inputForm->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
        inputForm->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
        inputForm->MaximizeBox = false;
        inputForm->MinimizeBox = false;
        inputForm->ShowIcon = false;
        inputForm->TopMost = true;
        inputForm->KeyPreview = true; // Enable key events for the form

        // Position the form on the same screen as the progress window
        inputForm->Location = System::Drawing::Point(this->Location.X + 20, this->Location.Y + 20);

        // Handle the ESC key to close the dialog
        inputForm->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Progress::inputForm_KeyDown);

        // Create the TextBox for input
        System::Windows::Forms::TextBox^ inputBox = gcnew System::Windows::Forms::TextBox();
        inputBox->Location = System::Drawing::Point(10, 10);
        inputBox->Width = 180;
        inputForm->Controls->Add(inputBox);

        // Create a Find Next button
        System::Windows::Forms::Button^ findNextButton = gcnew System::Windows::Forms::Button();
        findNextButton->Text = "Find Next";
        findNextButton->Location = System::Drawing::Point(10, 40);
        findNextButton->Size = System::Drawing::Size(80, 25);
        findNextButton->Click += gcnew System::EventHandler(this, &Progress::findNextButton_Click);
        inputForm->Controls->Add(findNextButton);

        // Create a Find Prev button
        System::Windows::Forms::Button^ findPrevButton = gcnew System::Windows::Forms::Button();
        findPrevButton->Text = "Find Prev";
        findPrevButton->Location = System::Drawing::Point(100, 40);
        findPrevButton->Size = System::Drawing::Size(80, 25);
        findPrevButton->Click += gcnew System::EventHandler(this, &Progress::findPrevButton_Click);
        inputForm->Controls->Add(findPrevButton);

        // Tie the Enter key to the Find Next button
        inputForm->AcceptButton = findNextButton;

        // Automatically adjust the form size based on its content
        inputForm->ClientSize = System::Drawing::Size(
            inputBox->Width + 20,
            findNextButton->Height + findPrevButton->Height + inputBox->Height + 30
        );

        // Show the dialog without blocking interaction with the underlying form
        inputForm->Show(this);
        return "";
    }

    void searchTextBoxes(System::String^ searchTerm, bool findNext)
    {
        if (mDefaultKdDisplay->Visible)
        {
            searchTextBox(mDefaultKdDisplay, searchTerm, findNext);
        }
        if (mUserKdDisplay->Visible)
        {
            searchTextBox(mUserKdDisplay, searchTerm, findNext);
        }
    }

    void findNextButton_Click(System::Object^ sender, System::EventArgs^ e)
    {
        System::Windows::Forms::Form^ inputForm = safe_cast<System::Windows::Forms::Button^>(sender)->FindForm();
        System::Windows::Forms::TextBox^ inputBox = safe_cast<System::Windows::Forms::TextBox^>(inputForm->Controls[0]);
        mLastSearchTerm = inputBox->Text;
        searchTextBoxes(mLastSearchTerm, true);
    }

    void findPrevButton_Click(System::Object^ sender, System::EventArgs^ e)
    {
        System::Windows::Forms::Form^ inputForm = safe_cast<System::Windows::Forms::Button^>(sender)->FindForm();
        System::Windows::Forms::TextBox^ inputBox = safe_cast<System::Windows::Forms::TextBox^>(inputForm->Controls[0]);
        mLastSearchTerm = inputBox->Text;
        searchTextBoxes(mLastSearchTerm, false);
    }

private:
    ref struct FilesLines
    {
    public:
        System::String^ lines;
        int filesRead;
    };

    FilesLines^ readFiles(const std::string& fileContainingList, int startIndex)
    {
        FilesLines^ filesLines = gcnew FilesLines();

        int maxDmp = -1;
        std::vector<std::string> kdPath1;
        std::ifstream maxDmpFile(fileContainingList);
        std::string line = "";
        while (std::getline(maxDmpFile, line))
        {
            if (!line.empty())
            {
                kdPath1.push_back(line);
            }
            maxDmp = int(kdPath1.size());
        }
        maxDmpFile.close();
        std::string kdOutput = "";
        while (startIndex < maxDmp)
        {
            std::ifstream kdFile(kdPath1[startIndex]);
            line = "";
            while (std::getline(kdFile, line))
            {
                kdOutput += line + "\n\r\n\r";
            }
            ++startIndex;
        }
        filesLines->filesRead = startIndex;
        filesLines->lines = SystemStandardConversionUtil::convertStdStringToSystemString(kdOutput);

        return filesLines;
    }

    private: System::Void buildKdOutput(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
    {
        mProgressVal = 0;
        mProgressMax = 100;
        mKdFilesProcessed = 0;
        mUserFilesProcessed = 0;
        while (!mBackgroundKdOutputBuilder->CancellationPending && mProgressVal < mProgressMax)
        {
            auto filePath = ConfigurationUtil::getTempDirectory() + "\\dmps\\maxkdDmp.txt";
            FilesLines^ kdDefaultLines = readFiles(filePath, mKdFilesProcessed);
            filePath = ConfigurationUtil::getTempDirectory() + "\\dmps\\maxUserDmp.txt";
            FilesLines^ kdUserLines = readFiles(filePath, mUserFilesProcessed);
            mKdDefaultOutput = kdDefaultLines->lines;
            mKdFilesProcessed = kdDefaultLines->filesRead;
            mKdUserOutput = kdUserLines->lines;
            mUserFilesProcessed = kdUserLines->filesRead;
            safeInvoke(this, gcnew System::Action(this, &Progress::updateKdOutput));

            Sleep(150);
        }
    }

  public: void hideShow(){
    Hide();
    Show();
  }
private: System::Void showDefaultKdCheckedChanged(System::Object^  sender, System::EventArgs^  e) {
       if(mShowDefaultKd->Checked){
         CDPI g_metrics;
         int dpiX = g_metrics.GetDPIX();
         mShowUserKd->Checked = false;
         mDefaultKdDisplay->Visible = true;
         mUserKdDisplay->Visible = false;  
         this->Height = mShowDefaultKd->Location.Y + mShowDefaultKd->Height + int(double(50)*double(dpiX)/double(96)) + 300;
         this->Width = mProgressDisplay->Width + int(double(60)*double(dpiX)/double(96)) + 300;
         CenterWindow *myWindow;
         myWindow = new CenterWindow;
         myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
         delete myWindow;
       }
       else if(!mShowUserKd->Checked){
         // Using CDPI example class
         CDPI g_metrics;
         int dpiX = g_metrics.GetDPIX();  
         mDefaultKdDisplay->Visible = false;        
         this->Height = mShowDefaultKd->Location.Y + mShowDefaultKd->Height + int(double(50)*double(dpiX)/double(96));
         this->Width = mProgressDisplay->Width + int(double(60)*double(dpiX)/double(96));
         CenterWindow *myWindow;
         myWindow = new CenterWindow;
         myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
         delete myWindow;
       }
     }
private: System::Void showUserKdCheckedChanged(System::Object^  sender, System::EventArgs^  e) {
       if(mShowUserKd->Checked){
         CDPI g_metrics;
         int dpiX = g_metrics.GetDPIX();
         mShowDefaultKd->Checked = false;
         mUserKdDisplay->Visible = true;
         mDefaultKdDisplay->Visible = false;          
         this->Height = mShowDefaultKd->Location.Y + mShowDefaultKd->Height + int(double(50)*double(dpiX)/double(96)) + 300;
         this->Width = mProgressDisplay->Width + int(double(60)*double(dpiX)/double(96)) + 300;
         CenterWindow *myWindow;
         myWindow = new CenterWindow;
         myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
         delete myWindow;
       }
       else if(!mShowDefaultKd->Checked){
         // Using CDPI example class
         CDPI g_metrics;
         int dpiX = g_metrics.GetDPIX();  
         mUserKdDisplay->Visible = false;        
         this->Height = mShowDefaultKd->Location.Y + mShowDefaultKd->Height + int(double(50)*double(dpiX)/double(96));
         this->Width = mProgressDisplay->Width + int(double(60)*double(dpiX)/double(96));
         CenterWindow *myWindow;
         myWindow = new CenterWindow;
         myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
         delete myWindow;
       }
     }
private: System::Void progressResize(System::Object^  sender, System::EventArgs^  e) {
       // Using CDPI example class
       CDPI g_metrics;
       int dpiX = g_metrics.GetDPIX();  
       mDefaultKdDisplay->Width = this->Width - 38 - int(double(30)*double(dpiX)/double(96));
       mDefaultKdDisplay->Height =  int(double(this->Height - 318)*double(96)/double(dpiX));
       mUserKdDisplay->Width = this->Width - 38 - int(double(30)*double(dpiX)/double(96));
       mUserKdDisplay->Height = int(double(this->Height - 318)*double(96)/double(dpiX));
     }
private: System::Void textBox3TextChanged(System::Object^  sender, System::EventArgs^  e) {
       mShowDefaultKd->Checked = false;
     }
private: System::Void textBox4TextChanged(System::Object^  sender, System::EventArgs^  e) {
       mShowUserKd->Checked = false;
     }
private: System::Void alwaysOnTopClick(System::Object^  sender, System::EventArgs^  e) {
      if(mAlwaysOnTop->Checked){
        mAlwaysOnTop->Checked = false;
        this->TopMost = false;
      }
      else if(!mAlwaysOnTop->Checked){
        mAlwaysOnTop->Checked = true;
        this->TopMost = true;
      }
     }
};
}
