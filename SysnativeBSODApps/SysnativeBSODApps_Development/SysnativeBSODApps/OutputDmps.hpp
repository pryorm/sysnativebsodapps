#ifndef OUTPUTDMPS_HPP
#define OUTPUTDMPS_HPP
//-----------------------------------------------------------------------------
//  File:         OutputDmps c++ class
//  Description:  Gathers info and formatting and outputs the .dmps analysis
//  Project:      dmpAnalysis
//  Author:       Mikael Pryor
//  Date:         July  28, 2012
//  Changes:      July  29, 2012
//  Changes Made: Created the constructor and destructor and obtain/setup the 
//                directory structure.
//    07/29/2012: Added getDmpOptions() and ODSymbolsString
//          Added the deleteTemporaryDirectory() function 
//          Added the cleanupOperations() function    
//    07/30/2012: Added input file storage varibles
//    07/31/2012: Added output file capability and options
//-----------------------------------------------------------------------------
#include "DirectoryStructure.hpp"
#include "DmpOptions.hpp"
#include "InputData.hpp"
#include "SysnativeFileSystemManager.h"
#include <string>

class OutputDmps {
public:
  // Contructor, Destructor, and Functions
  OutputDmps(std::string tempString_s);
  virtual ~OutputDmps();
  void setUpDirectoryStructure();
  void getDmpOptions();
  void cleanUpOperations();
  void outputAnalysisFiles();
  void setOutputTimeDir(const std::string& timeDir);
  const std::string& getOutputTimeDir()const;

  //Variables  
  std::string userName;
  std::string originatingPost;
  std::vector<std::string> description;
  DmpOptions *options;
  InputData *input;
  int console;
  std::string tempDir;
  std::string tempPath;
  std::string userProfile;
  bool loggingChecked;
protected:

private:
  // Private Functions
  void getInputData();
  void outputCSV();
  void outputDmps();
  void outputImportantInfo();
  void outputStack();
  void outputSMBIOS();
  void outputFullDrivers();
  void outputMissingFromDRT();
  void outputThirdPartyDriversName();
  void outputThirdPartyDriversDate();
  void outputUnloadedDrivers();
  void outputTemplate();
  void outputNinetyNine();
  void outputNinetyEight();
  void outputNinetySeven();
  void inputNinetyFive();
  void outputNinetyFive();
  void outputEightyEight();
  void getEightyEight();
  int thirdPartyCheck(std::string test, std::string userprofile, std::vector<std::string> database, std::vector<int> databaseSystem, int strSize);
  int thirdPartyCheck(std::string test, std::string userprofile, std::vector<std::string> database, int strSize);
  bool unSignedCompare(std::string str1, std::string str2);
  void outputLog(std::string logStr);
  void OutputTimeBetween(float diff01, std::string str);

  // Private variables
  std::string ODOutputDmpsDir;
  std::string ODUserProfile;
  std::string ODParms;
  std::string ODTimeDir;
  std::string ODSymbolsString;
  int    ODSymbolsInt;
  std::string ODDriverOutSite;
  std::string ODDriverInSite;
  int    ODDmpsExist;
  int ODLoadingDumpFile;
  int ODKernelVersion;
  int ODBuiltBy;
  int ODDebugSessionTime;
  int ODSystemUptime;
  int ODBugCheck;
  int ODUnableToVerify;
  int ODSymbolsNotLoaded;
  int ODProbablyCausedBy;
  int ODDefaultBucketID;
  int ODBugcheckStr;
  int ODBugCheckAnalysis;
  int ODProcessName;
  int ODFailureBucketID;
  int ODBugcheckCode;
  int ODArguments;
  int ODCPUID;
  int ODMaxSpeed;
  int ODCurrentSpeed;
  int ODBIOSVersion;
  int ODBIOSReleaseDate;
  int ODSystemManufacturer;
  int ODSystemProductName;
  int ODOutputDmps;
  int ODOutputFullDrivers;
  int ODOutputThirdPartyDriversName;
  int ODOutputThirdPartyDriversDate;
  int ODOutputImportantInfo;
  int ODOutputStack;
  int ODOutputMissingFromDRT;
  int ODOutputUnloadedDrivers;
  int ODOutputSMBIOS;
  int ODOutputTemplate;
  int ODOutputNinetyNine;
  int ODOutputNinetyEight;
  int ODOutputNinetySeven;
  int ODOutputNinetyFive;
  int ODOutputEightyEight;
  int ODOpenOnExit;
  int ODUrlQuestion;
  int ODTurnOffBBCode;
  int ODNoBBCodeCodeBoxes;
  std::vector<std::string> ODEightyEightDate;
  std::vector<std::string> ODEightyEightName;
  std::vector<std::string> ODEightyEightDateTime;
  std::vector<std::string> ODEightyEightNameTime;
  bool m_bLogStarted;
  bool m_bTimeStarted;
  SysnativeFileSystemManager fileSystemManager;
};

#endif