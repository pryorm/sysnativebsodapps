#include "FileSystemUtil.h"

#include "LogUtil.h"
#include "SystemStandardConversionUtil.h"

#include <Windows.h>

bool FileSystemUtil::moveFile(System::String^ sourceFile, System::String^ destFile)
{
    currentAction = "Moving file: " + sourceFile + " to " + destFile;
    // Check if the source file exists
    try
    {
        if (!System::IO::File::Exists(sourceFile))
        {
            currentAction = "";
            return false;
        }

        if (System::IO::File::Exists(destFile))
        {
            System::IO::File::Delete(destFile);
        }

        // Move the file to the destination
        System::IO::File::Move(sourceFile, destFile);
    }
    catch (System::Exception ^ e)
    {
        LogUtil::logErrorAsync(e->Message);
    }

    currentAction = "";
    return true;
}

bool FileSystemUtil::moveFile(const std::string& sourceFile, const std::string& destFile)
{
    return moveFile(SystemStandardConversionUtil::convertStdStringToSystemString(sourceFile), SystemStandardConversionUtil::convertStdStringToSystemString(destFile));
}

bool FileSystemUtil::copyFile(System::String^ sourceFile, System::String^ destFile, bool overwrite)
{
    currentAction = "Copying file: " + sourceFile + " to " + destFile;
    // Check if the source file exists
    if (!System::IO::File::Exists(sourceFile))
    {
        currentAction = "";
        return false;
    }

    try
    {
        // Create the destination directory if it does not already exist
        if (!System::IO::Directory::Exists(System::IO::Path::GetDirectoryName(destFile)))
        {
            System::IO::Directory::CreateDirectory(System::IO::Path::GetDirectoryName(destFile));
        }

        // Copy the file to the destination
        System::IO::File::Copy(sourceFile, destFile, overwrite);
    }
    catch(System::Exception^ e)
    {
        LogUtil::logErrorAsync(e->Message);
        currentAction = "";
        return false;
    }

    currentAction = "";
    return true;
}

bool FileSystemUtil::copyFile(const std::string& sourceFile, const std::string& destFile, bool overwrite)
{
    return copyFile(SystemStandardConversionUtil::convertStdStringToSystemString(sourceFile), SystemStandardConversionUtil::convertStdStringToSystemString(destFile), overwrite);
}

bool FileSystemUtil::copyDirectoryAndContents(System::String^ sourceDir, System::String^ destDir, bool overwrite)
{
    currentAction = "Copying directory: " + sourceDir + " to " + destDir;
    // Check if the source directory exists
    if (!System::IO::Directory::Exists(sourceDir))
    {
        currentAction = "";
        return false;
    }

    // Copy the directory and all subdirectories and files inside the directory
    auto files = getFiles(sourceDir);

    for (int i = 0; i < files->Length; ++i)
    {
        copyFile(files[i], destDir + "//" + System::IO::Path::GetFileName(files[i]), overwrite);
    }

    currentAction = "";
    return true;
}

bool FileSystemUtil::copyDirectoryAndContents(const std::string& sourceDir, const std::string& destDir, bool overwrite)
{
    return copyDirectoryAndContents(SystemStandardConversionUtil::convertStdStringToSystemString(sourceDir), SystemStandardConversionUtil::convertStdStringToSystemString(destDir), overwrite);
}

void FileSystemUtil::removeDirectoryAndContents(System::String^ dirPath)
{
    currentAction = "Removing directory: " + dirPath;
    // Check if the directory exists
    if (!System::IO::Directory::Exists(dirPath))
    {
        currentAction = "";
        return;
    }

    // Delete the directory and all subdirectories and files inside the directory
    System::IO::Directory::Delete(dirPath, true);
    currentAction = "";
}

void FileSystemUtil::removeDirectoryAndContents(const std::string& dirPath)
{
    removeDirectoryAndContents(SystemStandardConversionUtil::convertStdStringToSystemString(dirPath));
}

void FileSystemUtil::removeEmptyDirectory(System::String^ dirPath)
{
    currentAction = "Removing directory: " + dirPath;
    // Check if the directory exists
    if (!System::IO::Directory::Exists(dirPath))
    {
        currentAction = "";
        return;
    }

    if (System::IO::Directory::GetFileSystemEntries(dirPath)->Length > 0)
    {
        currentAction = "";
        return;
    }

    try
    {
        // Delete the directory if it is empty
        System::IO::Directory::Delete(dirPath);
    }
    catch (System::Exception ^ excep)
    {
        LogUtil::logErrorAsync(excep->Message);
    }
    currentAction = "";
}

void FileSystemUtil::removeEmptyDirectory(const std::string& dirPath)
{
    removeEmptyDirectory(SystemStandardConversionUtil::convertStdStringToSystemString(dirPath));
}

bool FileSystemUtil::backupFile(System::String^ sourceFile)
{
    currentAction = "Backing up file: " + sourceFile;
    // Check if the source file exists
    if (!System::IO::File::Exists(sourceFile))
    {
        currentAction = "";
        return false;
    }

    // Backup the existing file
    System::String^ fileNameWithoutExtension = System::IO::Path::GetFileNameWithoutExtension(sourceFile);
    System::DateTime lastWriteTime = System::IO::File::GetLastWriteTime(sourceFile);
    System::IO::File::Move(sourceFile, fileNameWithoutExtension + "-" + lastWriteTime.ToString("yyyyMMddHHmmss") + System::IO::Path::GetExtension(sourceFile));

    currentAction = "";
    return true;
}

bool FileSystemUtil::createDirectoryWithBackup(System::String^ topDirectory, System::String^ subDirectory)
{
    currentAction = "Creating directory: " + subDirectory + " in " + topDirectory;
    // Check if the top directory exists
    if (!System::IO::Directory::Exists(topDirectory))
    {
        currentAction = "";
        return false;
    }

    System::String^ newDirectoryPath = topDirectory + "\\" + subDirectory;

    // Check if the subdirectory exists
    if (System::IO::File::Exists(newDirectoryPath))
    {
        // Backup the existing file
        if (!backupFile(newDirectoryPath))
        {
            currentAction = "";
            return false;
        }
    }

    // Create the subdirectory if it does not already exist
    if (!System::IO::Directory::Exists(newDirectoryPath))
    {
        System::IO::Directory::CreateDirectory(newDirectoryPath);
    }

    currentAction = "";
    return true;
}

bool FileSystemUtil::createDirectoryWithBackup(const std::string& topDirectory, const std::string& subDirectory)
{
    return createDirectoryWithBackup(SystemStandardConversionUtil::convertStdStringToSystemString(topDirectory), SystemStandardConversionUtil::convertStdStringToSystemString(subDirectory));
}

bool FileSystemUtil::moveDirectoryAndContents(System::String^ sourceDir, System::String^ destDir)
{
    currentAction = "Moving directory: " + sourceDir + " to " + destDir;
    // Check if the source directory exists and the destination directory does not exist
    if (!System::IO::Directory::Exists(sourceDir) || System::IO::Directory::Exists(destDir))
    {
        currentAction = "";
        return false;
    }

    // Check if the destination exists but is a file
    if (System::IO::File::Exists(destDir))
    {
        // Backup the existing file
        if (!backupFile(destDir))
        {
            currentAction = "";
            return false;
        }
    }

    // Move the source directory to the destination directory
    try
    {
        System::IO::Directory::Move(sourceDir, destDir);
    }
    catch(System::Exception^ e)
    {
        LogUtil::logErrorAsync(e->Message);
        currentAction = "";
        return false;
    }

    currentAction = "";
    return true;
}

void FileSystemUtil::transferFilesWithBackupAndReplace(System::String^ sourceDir, System::String^ destDir)
{
    currentAction = "Transferring files from: " + sourceDir + " to " + destDir;
    // Get the files in the source directory
    cli::array<System::String^>^ files = System::IO::Directory::GetFiles(sourceDir, "*", System::IO::SearchOption::AllDirectories);

    // Check if files have changed and backup the existing files if they have to fileName-%%YYYYMMDDHHMMSS%%.ext
    for(int i = 0; i < files->Length; i++)
    {
        // Get the file name
        System::String^ fileName = destDir + "\\" + System::IO::Path::GetFileName(files[i]);

        // Check if the file exists in the destination directory
        if (System::IO::File::Exists(fileName))
        {
            // Get the last write time of the source file
            System::DateTime sourceFileLastWriteTime = System::IO::File::GetLastWriteTime(files[i]);

            // Get the last write time of the destination file
            System::DateTime destFileLastWriteTime = System::IO::File::GetLastWriteTime(fileName);

            // Check if the source file is newer than the destination file
            if (sourceFileLastWriteTime > destFileLastWriteTime)
            {
                // Backup the existing file
                if (!backupFile(fileName))
                {
                    LogUtil::logErrorAsync("Failed to backup file: " + fileName);
                }
            }
        }
        else
        {
            moveFile(files[i], destDir + "\\" + fileName);
        }
    }
    currentAction = "";
}

bool FileSystemUtil::transferDirectoryAndContentsWithBackup(System::String^ sourceDir, System::String^ destDir, bool removeSource)
{
    currentAction = "Transferring directory: " + sourceDir + " to " + destDir;
    // Check if the source directory exists
    if (!System::IO::Directory::Exists(sourceDir))
    {
        // The source directory does not exist
        LogUtil::logErrorAsync("The source directory \"" + sourceDir +"\" does not exist");
        currentAction = "";
        return false;
    }

    // Move the source directory to the destination directory if the destination directory does not exist
    if (!System::IO::Directory::Exists(destDir) && moveDirectoryAndContents(sourceDir, destDir))
    {
        currentAction = "";
        return true;
    }

    try
    {
        transferFilesWithBackupAndReplace(sourceDir, destDir);
    }
    catch(System::Exception^ e)
    {
        LogUtil::logErrorAsync(e->Message);
        currentAction = "";
        return false;
    }

    // Get the subdirectories in the source directory
    cli::array<System::String^>^ subDirs = System::IO::Directory::GetDirectories(sourceDir, "*", System::IO::SearchOption::AllDirectories);

    for(int i = 0; i < subDirs->Length; i++)
    {
        // Transfer the subdirectory and its contents, backing up previous files that have changed
        if (!transferDirectoryAndContentsWithBackup(subDirs[i], destDir + "\\" + subDirs[i]->Substring(sourceDir->Length + 1), true))
        {
            currentAction = "";
            return false;
        }
    }
    if (removeSource)
    {
        System::IO::Directory::Delete(sourceDir, true);
    }
    currentAction = "";
    return true;
}

bool FileSystemUtil::transferDirectoryAndContentsWithBackup(const std::string& sourceDir, const std::string& destDir, bool removeSource)
{
    return transferDirectoryAndContentsWithBackup(SystemStandardConversionUtil::convertStdStringToSystemString(sourceDir), SystemStandardConversionUtil::convertStdStringToSystemString(destDir), removeSource);
}

cli::array <System::String^>^ FileSystemUtil::getFiles(System::String^ sourceDir)
{
    try
    {
        if (System::IO::Directory::Exists(sourceDir))
        {
            return System::IO::Directory::GetFiles(sourceDir);
        }
    }
    catch (System::Exception ^ e)
    {
        LogUtil::logErrorAsync(e->Message);
        currentAction = "";
    }
    return gcnew cli::array<System::String^>(0);
}

std::vector<std::string> FileSystemUtil::getFiles(const std::string& sourceDir)
{
    std::vector<std::string> files;
    cli::array<System::String^>^ fileArray = getFiles(SystemStandardConversionUtil::convertStdStringToSystemString(sourceDir));
    for(int i = 0; i < fileArray->Length; i++)
    {
        files.push_back(SystemStandardConversionUtil::convertSystemStringToStdString(fileArray[i]));
    }
    return files;
}

System::String^ FileSystemUtil::getFileName(System::String^ filePath)
{
    return System::IO::Path::GetFileName(filePath);
}

std::string FileSystemUtil::getFileName(const std::string& filePath)
{
    return SystemStandardConversionUtil::convertSystemStringToStdString(getFileName(SystemStandardConversionUtil::convertStdStringToSystemString(filePath)));
}

void FileSystemUtil::createLink(System::String^ linkPath, System::String^ targetPath)
{
    currentAction = "Creating link: " + linkPath + " to " + targetPath;
    if (System::Environment::OSVersion->Version->Major >= 6) // Check if Windows version is Vista or later
    {
        
        if (!CreateSymbolicLink(SystemStandardConversionUtil::convertSystemStringToStdWstring(linkPath).c_str(), SystemStandardConversionUtil::convertSystemStringToStdWstring(targetPath).c_str(), SYMBOLIC_LINK_FLAG_DIRECTORY))
        {
            currentAction = "";
            throw gcnew System::ComponentModel::Win32Exception(System::Runtime::InteropServices::Marshal::GetLastWin32Error());
        }
    }
    else
    {
        // Fallback method for Windows XP using linkd command
        runCommand("cmd.exe", "/C linkd \"" + linkPath + "\" \"" + targetPath + "\"");
    }
    currentAction = "";
}

void FileSystemUtil::removeEmptyDirectoryOrLink(System::String^ linkPath)
{
    currentAction = "Removing directory or link: " + linkPath;
    if (System::Environment::OSVersion->Version->Major >= 6) // Check if Windows version is Vista or later
    {
        System::IO::Directory::Delete(linkPath);
    }
    else
    {
        // Fallback method for Windows XP using rmdir command
        runCommand("cmd.exe", "/C rmdir \"" + linkPath + "\"");
    }
    currentAction = "";
}

void FileSystemUtil::modifyPermissionsAndAttributes(System::String^ dirPath, System::String^ group, System::Security::AccessControl::FileSystemRights rights, System::Security::AccessControl::AccessControlType accessType, System::IO::FileAttributes attributes)
{
    currentAction = "Modifying permissions and attributes for directory: " + dirPath;
    System::Security::AccessControl::DirectorySecurity^ dirSecurity = System::IO::Directory::GetAccessControl(dirPath);
    System::Security::AccessControl::FileSystemAccessRule^ accessRule = gcnew System::Security::AccessControl::FileSystemAccessRule(group, rights, accessType);
    dirSecurity->AddAccessRule(accessRule);
    System::IO::Directory::SetAccessControl(dirPath, dirSecurity);
    // Set the directory attributes to hidden and system
    System::IO::DirectoryInfo^ dirInfo = gcnew System::IO::DirectoryInfo(dirPath);
    dirInfo->Attributes = dirInfo->Attributes | attributes;
    currentAction = "";
}

void FileSystemUtil::openFile(System::String^ filePath)
{
    currentAction = "Opening file: " + filePath;
    System::Diagnostics::Process::Start(filePath);
    currentAction = "";
}

void FileSystemUtil::openFile(const std::string& str)
{
    openFile(SystemStandardConversionUtil::convertStdStringToSystemString(str));
}

System::String^ FileSystemUtil::getCurrentAction()
{
    return currentAction;
}

void FileSystemUtil::runCommand(System::String^ command, System::String^ arguments)
{
    currentAction = "Running command: " + command + " " + arguments;
    System::Diagnostics::Process^ process = gcnew System::Diagnostics::Process();
    process->StartInfo->FileName = command;
    process->StartInfo->Arguments = arguments;
    process->StartInfo->UseShellExecute = false;
    process->StartInfo->CreateNoWindow = true;
    process->Start();
    process->WaitForExit();
    currentAction = "";
}