#include "Portable.h"

#include "ConfigurationUtil.h"
#include "ConsoleInfo.hpp"
#include "FileSystemUtil.h"
#include "SystemStandardConversionUtil.h"
#include <string>
#include <fstream>
#include <iostream>

#include "debug.h"


//#include <vld.h>

using namespace SysnativeBSODApps;

//Load window
System::Void Portable::Portable_Load(System::Object^  sender, System::EventArgs^  e)
{
  array<System::String^>^ p_logicalDrives = System::IO::Directory::GetLogicalDrives();
  int numDrives = p_logicalDrives->Length;

  for (int i=0; i< numDrives; i++)
  {
    comboBox1->Items->Add(p_logicalDrives[i]);
  }
  this->Width = label1->Location.X + label1->Width + 40;
}

// Drive list
System::Void Portable::comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
}

System::Void Portable::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
  label1->Text = L"Backing up old files. Please wait...";
  System::ComponentModel::BackgroundWorker^ worker = dynamic_cast<System::ComponentModel::BackgroundWorker^>(sender);
  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP"));

  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  // create a junction to the flash drive settings using a batch file
  FileSystemUtil::transferDirectoryAndContentsWithBackup(userProfile + "\\SysnativeBSODApps", tempDir + "\\SysnativeBSODApps_zdnbak", true);
  remove(std::string(tempDir + "\\SysnativeBSODApps_zdnbak\\license.txt").c_str());
  remove(std::string(tempDir + "\\SysnativeBSODApps_zdnbak\\SysnativeBSODApps.exe").c_str());
  remove(std::string(tempDir + "\\SysnativeBSODApps_zdnbak\\SysnativeBSODApps.chm").c_str());
  remove(std::string(tempDir + "\\SysnativeBSODApps_zdnbak\\dmpOptions\\installDir.txt").c_str());
  label1->Text = L"Creating link, please wait...";
  Refresh();
  std::string path = SystemStandardConversionUtil::convertSystemStringToStdString(comboBox1->Text);
  if(path[path.length()-1] == '\\')
  {
    path = path.substr(0,path.length()-1);
  }
  FileSystemUtil::createDirectoryWithBackup(path, "SysnativeBSODApps");
  FileSystemUtil::createDirectoryWithBackup(path + "\\SysnativeBSODApps","dmpOptions");
  std::ofstream outfile(path + "\\SysnativeBSODApps\\dmpOptions\\installDir.txt");
  outfile << path;
  outfile.close();
  std::string batch = "";
  batch = batch + "attrib -h -s \"" + userProfile + "\\SysnativeBSODApps\"";
  batch = batch + "\nattrib /l -h -s \"" + userProfile + "\\SysnativeBSODApps\"";
  batch = batch + "\nrmdir \"" + userProfile + "\\SysnativeBSODApps\"";
  batch = batch + "\ndel /q \"" + userProfile + "\\SysnativeBSODApps\"";
  if(userProfile != path && userProfile + "\\" != path)
  {
    batch = batch + "\nrmdir /s /q \"" + userProfile + "\\SysnativeBSODApps\"";
    batch = batch + "\nlinkd \"" + userProfile + "\\SysnativeBSODApps\" \"" + path + "\\SysnativeBSODApps\"";
    batch = batch + "\nmklink /j \"" + userProfile + "\\SysnativeBSODApps\" \"" + path + "\\SysnativeBSODApps\"";
    batch = batch + "\nattrib /l +h +s \"" + userProfile + "\\SysnativeBSODApps\"";
    batch = batch + "\nicacls \"" + path + "\\SysnativeBSODApps\" /t /c /grant Users:F";
    batch = batch + "\ncacls \"" + path + "\\SysnativeBSODApps\" /e /t /c /g Users:F";
  }
  outfile.open(tempDir + "\\hidden.bat");
  outfile << batch;
  outfile.close();
  // run the batch file behind the scenes
  std::string vbs = "Set WshShell = CreateObject(\"WScript.Shell\")";
  vbs = vbs + "\nWshShell.Run chr(34) & \"" + tempDir + "\\hidden.bat\" & Chr(34), 0";
  vbs = vbs + "\nSet WshShell = Nothing";
  outfile.open(tempDir + "\\hiddenbat.vbs");
  outfile << vbs;
  outfile.close();
  std::string myCommand = "cmd.exe /c " + tempDir + "\\hiddenbat.vbs";
  std::string test;
  // run the batch file with the console command option
  ConsoleInfo *console;
  console = new ConsoleInfo();
  // create a wstring for the input to the console object
  std::wstring temp;
  // convert the command string to wstring format
  temp.assign(myCommand.begin(),myCommand.end());
  // run the command and save the output to a string in memory
  test = console->getConsoleInfo(const_cast<LPWSTR>(temp.c_str()));
  // delete the console object now that it is not needed (cleans up the memory)
  delete console;
  Refresh();
  Sleep(2010);
  FileSystemUtil::copyDirectoryAndContents(tempDir + "\\SysnativeBSODApps_zdnbak", userProfile + "\\SysnativeBSODApps", true);
  Refresh();
}
System::Void Portable::backgroundWorker2_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
  while(System::String::IsNullOrEmpty(FileSystemUtil::getCurrentAction()))
  {
    Sleep(105);
  }
  while(!System::String::IsNullOrEmpty(FileSystemUtil::getCurrentAction()))
  {
    label1->Text = FileSystemUtil::getCurrentAction();
  }
}

System::Void Portable::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
  Sleep(495);
  Close();
}

System::Void Portable::backgroundWorker2_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{
  Sleep(495);
}

// OK Button
System::Void Portable::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
  button1->Enabled = false;
  backgroundWorker1->RunWorkerAsync();
  backgroundWorker2->RunWorkerAsync();
}

// Cancel Button
System::Void Portable::button2_Click(System::Object^  sender, System::EventArgs^  e)
{
  Close();
}