#ifndef CONSOLEINFO_HPP
#define CONSOLEINFO_HPP
//-----------------------------------------------------------------------------
//  File:         ConsoleInfo c++ class
//  Description:  Gathers info from command line
//  Project:      dmpAnalysis
//  Author:       Mikael Pryor
//  Date:         July  30, 2012
//  Changes:      July  30, 2012
//  Changes Made: Created the constructor and destructor and pasted the function.                           
//-----------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <windows.h>

class ConsoleInfo {
public:
  // Contructor, Destructor, and Functions
  ConsoleInfo();
  virtual ~ConsoleInfo();
  std::string getConsoleInfo(LPWSTR Command);
protected:

private:
};

#endif