#pragma once

#include <string>

class SystemStandardConversionUtil
{
    public:
        static std::string convertSystemStringToStdString(System::String^ systemString);
        static std::wstring convertSystemStringToStdWstring(System::String^ systemString);
        static System::String^ convertStdStringToSystemString(std::string stdString);
};