#include "SystemStandardConversionUtil.h"

std::string SystemStandardConversionUtil::convertSystemStringToStdString(System::String^ systemString)
{
    using namespace System;
    using namespace Runtime::InteropServices;
    const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(systemString)).ToPointer();
    return chars;
}

std::wstring SystemStandardConversionUtil::convertSystemStringToStdWstring(System::String^ systemString)
{
    using namespace System;
    using namespace Runtime::InteropServices;
    const wchar_t* chars = (const wchar_t*)(Marshal::StringToHGlobalUni(systemString)).ToPointer();
    return chars;
}

System::String^ SystemStandardConversionUtil::convertStdStringToSystemString(std::string stdString)
{
    using namespace System;
    using namespace Runtime::InteropServices;
    return Marshal::PtrToStringAnsi((IntPtr)(char*)stdString.c_str());
}