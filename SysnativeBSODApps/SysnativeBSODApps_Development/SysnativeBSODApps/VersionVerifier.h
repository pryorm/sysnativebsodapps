#pragma once

namespace SysnativeBSODApps
{
public ref class VersionVerifier
{
public:
    static System::Threading::Tasks::Task^ checkForUpdateAsync();

private:
    static System::String^ getVersionUrl();
    static void fetchAndCompareVersion();
    static bool isNewVersionAvailable(System::String^ latestVersion);
    static void notifyUser(System::String^ latestVersion);
    static void notifyNetworkError();
    static void showUpdateDialog(System::String^ latestVersion);
    static void showNetworkErrorDialog();
    static System::String^ getCurrentVersion();
};
}// end namespace SysnativeBSODApps