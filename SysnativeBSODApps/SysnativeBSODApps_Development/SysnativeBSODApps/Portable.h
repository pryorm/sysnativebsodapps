#include "debug.h"

#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for Portable
  /// </summary>
  public ref class Portable : public System::Windows::Forms::Form
  {
  public:
    Portable(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~Portable()
    {
      if (components)
      {
        delete components;
      }
    }
  private: System::Windows::Forms::Label^  label1;
  protected: 
  private: System::Windows::Forms::ComboBox^  comboBox1;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::Button^  button2;
  private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
  private: System::ComponentModel::BackgroundWorker^  backgroundWorker2;

  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Portable::typeid));
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
      this->backgroundWorker2 = (gcnew System::ComponentModel::BackgroundWorker());
      this->SuspendLayout();
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(0, 21);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(340, 39);
      this->label1->TabIndex = 0;
      this->label1->Text = L"Are you running the apps on another system via a flash / thumb drive\? \r\n\r\nIf so, " 
        L"select the drive from the dropdown list and click OK.";
      this->label1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
      // 
      // comboBox1
      // 
      this->comboBox1->FormattingEnabled = true;
      this->comboBox1->Location = System::Drawing::Point(3, 77);
      this->comboBox1->Name = L"comboBox1";
      this->comboBox1->Size = System::Drawing::Size(121, 21);
      this->comboBox1->TabIndex = 1;
      this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &Portable::comboBox1_SelectedIndexChanged);
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(176, 147);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(75, 23);
      this->button1->TabIndex = 2;
      this->button1->Text = L"OK";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &Portable::button1_Click);
      // 
      // button2
      // 
      this->button2->Location = System::Drawing::Point(257, 147);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(75, 23);
      this->button2->TabIndex = 3;
      this->button2->Text = L"Cancel";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &Portable::button2_Click);
      // 
      // backgroundWorker1
      // 
      this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Portable::backgroundWorker1_DoWork);
      this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &Portable::backgroundWorker1_RunWorkerCompleted);
      // 
      // backgroundWorker2
      // 
      this->backgroundWorker2->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Portable::backgroundWorker2_DoWork);
      this->backgroundWorker2->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &Portable::backgroundWorker2_RunWorkerCompleted);
      // 
      // Portable
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(344, 182);
      this->Controls->Add(this->button2);
      this->Controls->Add(this->button1);
      this->Controls->Add(this->comboBox1);
      this->Controls->Add(this->label1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Name = L"Portable";
      this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
      this->Text = L"Portable";
      this->Load += gcnew System::EventHandler(this, &Portable::Portable_Load);
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
  private: System::Void Portable_Load(System::Object^  sender, System::EventArgs^  e);
  private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
  private: System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
  private: System::Void backgroundWorker2_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e);
  private: System::Void backgroundWorker2_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e);
};
}
