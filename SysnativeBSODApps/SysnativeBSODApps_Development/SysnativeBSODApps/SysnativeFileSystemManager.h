#ifndef SYSNATIVEFILESYSTEMMANAGER_H
#define SYSNATIVEFILESYSTEMMANAGER_H
#include <vector>
#include <string>

class SysnativeFileSystemManager{
public:
  std::vector<std::string> dmpCheck(std::string tempDir);
  const std::vector<std::string>& getEmptyFiles()const;
  // Functions
  void setUpDirectoryStructure(int form);
  void getCurrentSubdirectories();
  void deleteTemporaryDirectory();
  void deleteDmpOptionsDirectory();
  const std::string& getParms()const;
  const std::string& getTimeDir()const;
  int dmpsExist()const;
  void setTimeDir(const std::string& timeDir);

private:
    std::vector<std::string> emptyFiles;
    int sysnativeBSODApps;      // For determining whether SysnativeBSODApps exists
    int sysnativeBSODAppsSub1;  // For determining whether subdirectories exist
    int sysnativeBSODAppsSub2;  // For determining whether subdirectories exist
    int sysnativeBSODAppsSub3;  // For determining whether subdirectories exist
    int sysnativeBSODAppsSub4;  // For determining whether subdirectories exist
    int sysnativeBSODAppsSub5;  // For determining whether subdirectories exist
    std::string outputDmpsDir;

    // Variables
    std::string mTimeDir;
    std::string parms2;
    int parmsExist;          // For determining if the _jcgriff2_ parms directory exists
    int dmpsExistVal;
};
#endif