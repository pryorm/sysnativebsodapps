#include "ProgressBarDispatcher.h"

#include "ConfigurationUtil.h"
#include "Progress.h"

#include <fstream>

namespace SysnativeBSODApps
{

ProgressBarDispatcher::ProgressBarDispatcher()
: mProgressActive(false)
{
}

ProgressBarDispatcher::~ProgressBarDispatcher()
{
}

#pragma unmanaged
ProgressBarDispatcher& ProgressBarDispatcher::getInstance()
{
    static ProgressBarDispatcher progressBarDispatcherInstance;
    return progressBarDispatcherInstance;
}

#pragma managed
void ProgressBarDispatcher::openProgressBar()
{
    std::string tempDir = ConfigurationUtil::getTempDirectory();
    std::ofstream outExitFile(tempDir + "\\outExitFile.txt");
    outExitFile << 0;
    outExitFile.close();
    outExitFile.open(tempDir + "\\inExitFile.txt");
    outExitFile << 0;
    outExitFile.close();

    if (mProgressActive.load())
    {
        return;
    }
    mProgressActive.store(true);
    SysnativeBSODApps::Progress^ progressWindow;
    progressWindow = gcnew SysnativeBSODApps::Progress();
    System::Windows::Forms::Application::Run(progressWindow);
    mProgressActive.store(false);

    int inExit;
    std::ifstream inExitFile(tempDir + "\\outExitFile.txt");
    inExitFile >> inExit;
    inExitFile.close();
    if (!inExit)
    {
        outExitFile.open(tempDir + "\\inExitFile.txt");
        outExitFile << 1;
        outExitFile.close();
    }
    outExitFile.close();
}

}// end namespace SysnativeBSODApps