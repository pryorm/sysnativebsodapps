// SysnativeBSODApps.cpp : main project file.
#include "CenterWindow.h"
#include "ConfigurationUtil.h"
#include "FileSystemUtil.h"
#include "Form1.h"
#include "Form2.h"
#include "Form3.h"
#include "LimitSingleInstance.h"
#include "OutputDmps.hpp"
#include "Progress.h"
#include "Portable.h"
#include "SystemStandardConversionUtil.h"
#include "VersionVerifier.h"

#include <ctime>

#include "debug.h"

#include <iostream>
#include <tchar.h>
#include <string>
#include <conio.h>
#include <fstream>
#include <UrlMon.h>
#include <cstdio>
#include "wininet.h" // for clearing URL cache DeleteUrlCacheEntry
#pragma comment(lib, "wininet.lib") // for clearing URL cache DeleteUrlCacheEntry
#include <Windows.h>

bool m_bTimeStarted = false;

void OutputTimeBetween(float diff01, std::string str, bool loggingChecked, std::string tempDir)
{
  if(loggingChecked)
  {
    std::string IDUserProfile = ConfigurationUtil::getUserProfilePath();
    std::ofstream outTime;
    std::string timePath2 = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRuns.txt";
    std::string timePath3 = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\outputOptions.txt";
    std::ofstream outTime2;
    std::ofstream outTime3;

    if(!m_bTimeStarted)
    {
      m_bTimeStarted = true;
      outTime.open(tempDir + "\\outputDmps\\timeBetweenRuns.txt", std::ios::app);
      outTime2.open(timePath2, std::ios::app);
      outTime3.open(timePath3, std::ios::app);
    }
    else
    {
      outTime.open(tempDir + "\\outputDmps\\timeBetweenRuns.txt", std::ios::app);
      outTime2.open(timePath2, std::ios::app);
      outTime3.open(timePath3, std::ios::app);
    }

    outTime << diff01 << str << std::endl;
    outTime2 << diff01 << str << std::endl;
    outTime3 << "# " << diff01 << str << std::endl;
    outTime.close();
    outTime2.close();
    outTime3.close();
  }
}

using namespace SysnativeBSODApps;

#ifdef _DEBUG
#pragma comment(linker, "/ENTRY:mainCRTStartup")
#endif

[System::STAThreadAttribute]

int main(array<System::String ^> ^args)
{
    CLimitSingleInstance limitSingleInstance(L"SysnativeBSODAppsMutex");

#ifdef _DEBUG
  //Turn on debugging for memory leaks. This is automatically turned off when the build is Release.
  #define new DBG_NEW

  HANDLE hLogFile;

  //Open a file for output.
  hLogFile = CreateFile (L"Memory Leaks.zdn", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
  _CrtSetDbgFlag (_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
  _CrtSetReportMode (_CRT_WARN, _CRTDBG_MODE_FILE);
  _CrtSetReportFile (_CRT_WARN, hLogFile);
  _CrtSetReportMode (_CRT_ERROR, _CRTDBG_MODE_FILE);
  _CrtSetReportFile (_CRT_ERROR, hLogFile);
  _CrtSetReportMode (_CRT_ASSERT, _CRTDBG_MODE_FILE);
  _CrtSetReportFile (_CRT_ASSERT, hLogFile);
  // _CrtSetBreakAlloc(100936884);
#define VLD_FORCE_ENABLE
#define VLD_VERBOSE_REPORT
#endif

  // for saving and loading settings
  array<System::String^>^ fileListS;
  int parallelChecked;
  int defaultThreads;
  int userThreads;
  int sysnativeResultsChecked;
  System::String^ sysnativeResultsS;
  array<System::String^>^ kdPaths;
  array<int>^ kdActive;
  bool loggingChecked = false;
  // Set up application GUI styles
  System::Windows::Forms::Application::EnableVisualStyles();
  System::Windows::Forms::Application::SetCompatibleTextRenderingDefault(false);
  remove("outDebug.txt");
  std::ofstream outNewDebug;
  // outNewDebug.open("C:\\Users\\Public\\Documents\\debug.txt");
  // outNewDebug << "";
  // outNewDebug.close();

  std::vector<std::string> dmpsList;
  std::string copy1, copy2;
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  ConfigurationUtil::setTempDirectory(SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP")));
  std::string tempDir = ConfigurationUtil::getTempDirectory();

  copy1 = userProfile + "\\SysnativeBSODApps\\license.txt";
  remove(std::string(userProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRuns.txt").c_str());
  FileSystemUtil::copyFile(copy1, ConfigurationUtil::getCurrentDirectory() + "\\mylicense.txt", true);
  time_t randt1, randt2;
  randt1 = clock();
  randt2 = clock();
  while(float(randt2) - float(randt1) < 50)
  {
    std::stringstream sstemp;
    randt2 = clock();
    sstemp << rand();
    dmpsList.push_back(sstemp.str());
  }
  dmpsList[0] = dmpsList[dmpsList.size()-1];
  std::ifstream inLicense(ConfigurationUtil::getCurrentDirectory() + "\\mylicense.txt");
  std::string licenseInt = "0";
  if(inLicense.good()){
    inLicense >> licenseInt;
  }
  std::string setupLicenseInt = "028374615983726";
  inLicense.close();
  remove((ConfigurationUtil::getCurrentDirectory() + "\\mylicense.txt").c_str());
  if (licenseInt != setupLicenseInt)
  {
    Portable^ port;
    port = gcnew Portable();    
	System::Windows::Forms::Application::Run(port);
  }
  FileSystemUtil::copyFile(copy1, ConfigurationUtil::getCurrentDirectory() + "\\mylicense.txt", true);
  inLicense.open(ConfigurationUtil::getCurrentDirectory() + "\\mylicense.txt");
  licenseInt = "0";
  if(inLicense.good()){
    inLicense >> licenseInt;
  }
  inLicense.close();
  remove((ConfigurationUtil::getCurrentDirectory() + "\\mylicense.txt").c_str());
  if (licenseInt != setupLicenseInt)
  {
    System::Windows::Forms::MessageBox::Show(L"You must install the apps with the Setup.exe file and accept the license agreement.",L"BSOD Processing Apps License Violation", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
    return 0;
  }
    
  remove(std::string(tempDir + "\\hidden.bat").c_str());
  remove(std::string(tempDir + "\\hiddenbat.vbs").c_str());
  
  FileSystemUtil::createDirectoryWithBackup(tempDir, "SysnativeBSODApps");
  if(dmpsList.size() > 0)
  {
    FileSystemUtil::createDirectoryWithBackup(tempDir + "\\SysnativeBSODApps", dmpsList[0]);
    tempDir = tempDir + "\\SysnativeBSODApps" + "\\" + dmpsList[0];
  }
  else
  {
    dmpsList.push_back("noDmps.dmp");
    FileSystemUtil::createDirectoryWithBackup(tempDir + "\\SysnativeBSODApps", dmpsList[0]);
    tempDir = tempDir + "\\SysnativeBSODApps" + "\\" + dmpsList[0];
  }
  ConfigurationUtil::setTempDirectory(tempDir);
  FileSystemUtil::removeDirectoryAndContents(tempDir + "\\kdc");
  FileSystemUtil::createDirectoryWithBackup(tempDir, "outputDmps");
  std::string tempPath = tempDir + "\\progressRunning.txt";
  remove(tempPath.c_str());

  tempPath = tempDir + "\\outExitFile.txt";
  
  std::ofstream outExitFile(tempPath);
  outExitFile << 0;
  outExitFile.close();
  tempPath = tempDir + "\\inExitFile.txt";
  outExitFile.open(tempPath);
  outExitFile << 0;
  outExitFile.close();
  
  std::string tempTime = tempDir + "\\outputDmps\\timeBetweenRuns.txt";
  std::ofstream outTime;
  outTime.open(tempTime);
  outTime << "";
  outTime.close();
  clock_t t01, t02;
  outTime.open(tempTime, std::ios::app);
  t01 = clock();
  t02 = clock();
  float diff01 = ((float)t02 - (float)t01) / 1000.0F;
  // Clean up the previous outputOptions.txt file output
  std::string path = userProfile + "\\SysnativeBSODApps\\dmpOptions\\outputOptions.txt";
  std::ifstream inTime(path);
  std::vector<std::string> outputOptions;
  while(inTime.good() && !inTime.eof()){
    std::string inString;
    getline(inTime, inString);
    if(inString.find(" seconds to EnableVisualStyles()") != std::string::npos){
      break;
    }
    else{
      outputOptions.push_back(inString);
    }
  }
  inTime.close();
  if(outputOptions.size() > 0){
    std::ofstream outTimeOptions(path);
    for(int i=0; i<int(outputOptions.size()); i++){
      outTimeOptions << outputOptions[i] << std::endl;
    }
    outTimeOptions.close();
  }
  OutputTimeBetween(diff01, " seconds to EnableVisualStyles()", loggingChecked, tempDir);

  t01 = clock();
  t02 = clock();
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to SetCompatibleTextRenderingDefault(false)", loggingChecked, tempDir);
  
  t01 = clock();
  
  // Check for update
  VersionVerifier::checkForUpdateAsync();

  t02 = clock();
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to get today's date", loggingChecked, tempDir);
  
  t01 = clock();

  tempPath = tempDir + "\\keepGoingDmps.txt";
  remove(tempPath.c_str());
  std::ifstream inKeepGoingDmps(tempPath);

  // Creates an output object to create the output files from the .dmps analysis
  OutputDmps *output;
  // measure the time between runs
  clock_t t1, t2;

  bool keepGoingBool = inKeepGoingDmps.good();

  int countKeep = 0;
  while(!keepGoingBool){
    output = new OutputDmps(tempDir);
    output->loggingChecked = loggingChecked;
    tempPath = tempDir + "\\outExitFile.txt";
    std::ofstream outExitFile(tempPath);
    outExitFile << 0;
    outExitFile.close();
    tempPath = tempDir + "\\inExitFile.txt";
    outExitFile.open(tempPath);
    outExitFile << 0;
    outExitFile.close();
    std::ofstream outError2;
    tempPath = tempDir + "\\outError.txt";
    outError2.open(tempPath);
    outError2 << "";
    outError2.close();
    // Create the main window and run it
    tempPath = tempDir + "\\percentDone.txt";
    std::ofstream outPercentDone(tempPath);
    outPercentDone << 100;
    outPercentDone.close();
    Form1^ myForm = nullptr;
    Form3^ myForm3 = nullptr;
    myForm = gcnew Form1();
    myForm->centerForm1();
    myForm->setLoggingChecked(loggingChecked);
	System::Windows::Forms::Application::Run(myForm);
    loggingChecked = myForm->getLoggingChecked();
    fileListS = myForm->getFileList();
    parallelChecked = myForm->getParallelChecked();
    defaultThreads = myForm->getDispatcherParallelThreads();
    userThreads = myForm->getDispatcherUserThreads();
    sysnativeResultsChecked = static_cast<int>(myForm->getSysnativeResultsChecked());
    sysnativeResultsS = myForm->getSysnativeResultsBuilderSysnativeResults();
    kdPaths = myForm->getSavedKdPaths();
    kdActive = myForm->getActiveKdPaths();
    inKeepGoingDmps.close();
    tempPath = tempDir + "\\viewPreviousHTML.txt";
    std::ifstream inPreviousHTML(tempPath);
    int innFinished = 0;
    std::ifstream inFinished(userProfile + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
    if(inFinished.good())
    {
      inFinished >> innFinished;
    }
    inFinished.close();
    std::ofstream outFinished(userProfile + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
    outFinished << 0;
    outFinished.close();
    while(inPreviousHTML.good())
    {  
        inPreviousHTML.close();  
        copy1 = userProfile + "\\SysnativeBSODApps\\dmpOptions\\outTimeDir.txt";
        copy2 = tempDir + "\\outTimeDir.txt";
        FileSystemUtil::copyFile(copy1, copy2, true);
        if(myForm3 != nullptr)
        {
          delete myForm3;
          myForm3 = nullptr;
        }
        myForm3 = gcnew Form3();      
        myForm3->timeS = "";
        myForm3->label4->Text = "Viewing Previous Output";
        myForm3->tempStringS = gcnew System::String(tempDir.c_str());
        myForm3->centerForm3();
        myForm3->fileListS = fileListS;
        myForm3->loadFiles();
		System::Windows::Forms::Application::Run(myForm3);
        fileListS = myForm3->fileListS;
        tempPath = tempDir + "\\viewPreviousHTML.txt";
        remove(tempPath.c_str());
        outFinished.open(userProfile + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
        outFinished << innFinished;
        outFinished.close();
        delete myForm;
        myForm = nullptr;
        myForm = gcnew Form1();
        myForm->centerForm1();
        myForm->setLoggingChecked(loggingChecked);
        System::Windows::Forms::Application::Run(myForm);
        loggingChecked = myForm->getLoggingChecked();
        fileListS = myForm->getFileList();
        parallelChecked = myForm->getParallelChecked();
        defaultThreads = myForm->getDispatcherParallelThreads();
        userThreads = myForm->getDispatcherUserThreads();
        sysnativeResultsChecked = static_cast<int>(myForm->getSysnativeResultsChecked());
        sysnativeResultsS = myForm->getSysnativeResultsBuilderSysnativeResults();
        kdPaths = myForm->getSavedKdPaths();
        kdActive = myForm->getActiveKdPaths();
        inPreviousHTML.open(tempPath);
        inFinished.open(userProfile + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
        if(inFinished.good())
        {
          inFinished >> innFinished;
        }
        inFinished.close();
        outFinished.open(userProfile + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
        outFinished << 0;
        outFinished.close();
    }  
    outFinished.open(userProfile + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
    outFinished << innFinished;
    outFinished.close();
    inPreviousHTML.close();  
    output->loggingChecked = loggingChecked;
    
    tempPath = tempDir + "\\outGUI.txt";
    std::ifstream inGUI(tempPath);
    int inGUIExit = 0;
    if(inGUI.good()){
      inGUI >> inGUIExit;
      inGUI.close();
      remove(tempPath.c_str());
    }
    else{
      inGUIExit = 1;
      inGUI.close();
    }
    if(inGUIExit){
      outTime.close();
      // Move the currentDir.txt file to tmp so it is deleted during cleanup
      copy1 = "currentDir.txt";
      copy2 = tempDir + "\\currentDir.txt";
      FileSystemUtil::copyFile(copy1, copy2, true);
      remove(copy1.c_str());
  
      std::ofstream outfile(userProfile + "\\SysnativeBSODApps\\forumSettings\\default.dzdn");
      outfile << "## ###outputOrder_list" << std::endl;
      for(int i=0; i<fileListS->Length; i++)
      {
        outfile << SystemStandardConversionUtil::convertSystemStringToStdString(fileListS[i]);
        if(i < fileListS->Length-1)
        {
          outfile << std::endl;
        }
      }
      outfile << std::endl << "## ###parallelThreading_nu" << std::endl;
      outfile << parallelChecked << std::endl;
      outfile << defaultThreads << std::endl;
      outfile << userThreads;     
      outfile << std::endl << "## ###sysnativeResults_cb" << std::endl;
      outfile << sysnativeResultsChecked;
      outfile << std::endl << "## ###sysnativeResults_s" << std::endl;
      outfile << SystemStandardConversionUtil::convertSystemStringToStdString(sysnativeResultsS);
      if(kdPaths != nullptr)
      {
        outfile << std::endl << "## ###kdPaths" << std::endl;
        for(int i=0; i<kdPaths->Length; i++)
        {
          outfile << SystemStandardConversionUtil::convertSystemStringToStdString(kdPaths[i]);
          if(i < kdPaths->Length-1)
          {
            outfile << std::endl;
          }
        }
        outfile << std::endl << "## ###kdActive" << std::endl;
        for(int i=0; i<kdActive->Length; i++)
        {
          outfile << kdActive[i];
          if(i < kdActive->Length-1)
          {
            outfile << std::endl;
          }
        }
      }
      outfile.close();
      delete output;
      return 0;
    }
    t02 = clock();
    diff01 = ((float)t02 - (float)t01) / 1000.0F;
    OutputTimeBetween(diff01, " seconds to Run(gcnew Form1())", loggingChecked, tempDir);

    int fullGUI = 0;
    tempPath = tempDir + "\\fullGUI.txt";
    std::ifstream infile;
    infile.open(tempPath);
    if(infile.good()){
      infile >> fullGUI;
    }
    infile.close();

    tempPath = tempDir + "\\percentDone.txt";
    outPercentDone.open(tempPath);
    outPercentDone << 452312;
    outPercentDone.close();
    // The current (initial) time.
    t1 = clock();
    t01 = clock();
    output->console = 0;
    t02 = clock();
    diff01 = ((float)t02 - (float)t01) / 1000.0F;
    OutputTimeBetween(diff01, " seconds to create output object", loggingChecked, tempDir);

    t01 = clock();
    // Set up the directories for the output files
    //stringstream ssDebug;
    //ssDebug << "Setting up directories... \r";
    //outputLog(ssDebug.str());
    tempPath = tempDir + "\\analyzedDmpOf.txt";
    std::ofstream outProg(tempPath);
    outProg << "Setting up directories...";
    outProg.close();
    output->setUpDirectoryStructure();
    t02 = clock();
    diff01 = ((float)t02 - (float)t01) / 1000.0F;
    OutputTimeBetween(diff01, " seconds to setUpDirectoryStructure()", loggingChecked, tempDir);
    tempPath = tempDir + "\\percentDone.txt";
    outPercentDone.open(tempPath);
    outPercentDone << 99;
    outPercentDone.close();
    t01 = clock();
    
    tempPath = tempDir + "\\percentDone.txt";
    outPercentDone.open(tempPath);
    outPercentDone << 452312;
    outPercentDone.close();
    // Get options for the output files
	std::cout << "Getting user options... \r";
    tempPath = tempDir + "\\analyzedDmpOf.txt";
    outProg.open(tempPath);
    outProg << "Getting user options...";
    outProg.close();

    output->getDmpOptions();
    std::string dbugDir = "";
    if(kdPaths != nullptr)
    {
      for(int i=0; i<kdPaths->Length; i++)
      {
        if(kdActive[i] == 1)
        {
          dbugDir = "\"" + SystemStandardConversionUtil::convertSystemStringToStdString(kdPaths[i]) + "\"";
        }
      }
    }
    output->options->dbugDir = dbugDir;
    t02 = clock();
    diff01 = ((float)t02 - (float)t01) / 1000.0F;
    OutputTimeBetween(diff01, " seconds to getDmpOptions()", loggingChecked, tempDir);

    outTime.close();
    outPercentDone << 99;
    outPercentDone.close();
    t01 = clock();
    
    tempPath = tempDir + "\\percentDone.txt";
    outPercentDone.open(tempPath);
    outPercentDone << 452312;
    outPercentDone.close();
    // save to default.dzdn for InputData object
    std::ofstream outfile(userProfile + "\\SysnativeBSODApps\\forumSettings\\default.dzdn");
    outfile << "## ###outputOrder_list" << std::endl;
    for(int i=0; i<fileListS->Length; i++)
    {
      outfile << SystemStandardConversionUtil::convertSystemStringToStdString(fileListS[i]);
      if(i < fileListS->Length-1)
      {
        outfile << std::endl;
      }
    }
    outfile << std::endl << "## ###parallelThreading_nu" << std::endl;
    outfile << parallelChecked << std::endl;
    outfile << defaultThreads << std::endl;
    outfile << userThreads;  
    outfile << std::endl << "## ###sysnativeResults_cb" << std::endl;
    outfile << sysnativeResultsChecked;
    outfile << std::endl << "## ###sysnativeResults_s" << std::endl;
    outfile << SystemStandardConversionUtil::convertSystemStringToStdString(sysnativeResultsS);
    if(kdPaths != nullptr)
    {
      outfile << std::endl << "## ###kdPaths" << std::endl;
      for(int i=0; i<kdPaths->Length; i++)
      {
        outfile << SystemStandardConversionUtil::convertSystemStringToStdString(kdPaths[i]);
        if(i < kdPaths->Length-1)
        {
          outfile << std::endl;
        }
      }
      outfile << std::endl << "## ###kdActive" << std::endl;
      for(int i=0; i<kdActive->Length; i++)
      {
        outfile << kdActive[i];
        if(i < kdActive->Length-1)
        {
          outfile << std::endl;
        }
      }
    }
    outfile.close();
    
    // Get input data and store it in memory and then output it to files  
    output->outputAnalysisFiles();
    t02 = clock();
    diff01 = ((float)t02 - (float)t01) / 1000.0F;
    outTime.open(tempDir + "\\outputDmps\\timeBetweenRuns.txt", std::ios::app);
    OutputTimeBetween(diff01, " seconds to outputAnalysisFiles()", loggingChecked, tempDir);
    outTime.close();
    std::ifstream inKeepGoingDmps(tempDir + "\\keepGoingDmps.txt");
    keepGoingBool = inKeepGoingDmps.good();
    if(!keepGoingBool)
    {
      std::string tempDir1 = tempDir;
      if(countKeep == 0)
      {
        tempDir = tempDir + SystemStandardConversionUtil::convertSystemStringToStdString(countKeep.ToString());
        ConfigurationUtil::setTempDirectory(tempDir);
      }
      else
      {
        tempDir = tempDir.substr(0,tempDir.length()-1) + SystemStandardConversionUtil::convertSystemStringToStdString(countKeep.ToString());
        ConfigurationUtil::setTempDirectory(tempDir);
      }
      countKeep++;
      FileSystemUtil::copyDirectoryAndContents(tempDir1, tempDir, true);
      FileSystemUtil::removeDirectoryAndContents(tempDir + "\\dmps");
      FileSystemUtil::removeDirectoryAndContents(tempDir + "\\kdc");
      delete output;
    }
  }
  inKeepGoingDmps.close();
  remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());

  t01 = clock();
  // Cleanup the directory once output files are finished
  output->cleanUpOperations();
  t02 = clock();
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  outTime.open(tempDir + "\\outputDmps\\timeBetweenRuns.txt", std::ios::app);
  OutputTimeBetween(diff01, " seconds to perform cleanUpOperations()", loggingChecked, tempDir);
  
  // The current (final) time
  t2 = clock();
  // Difference between the final and initial time
  float diff = ((float)t2 - (float)t1) / 1000000.0F;
  outTime << diff * 1000 << " seconds to run the apps" << std::endl;
  // diff was in milliseconds, so multiply by 1000 to get seconds
  outTime.close();
  std::ofstream outTimeDir(tempDir + "\\outTimeDir.txt");
  System::String^ myPath = System::IO::Directory::GetCurrentDirectory();
  std::ofstream outFullPath(tempDir + "\\fullPath.txt");
  outFullPath << SystemStandardConversionUtil::convertSystemStringToStdString(myPath);
  outFullPath.close();
  std::ifstream fullPath(tempDir + "\\fullPath.txt");
  std::string fullPathString;
  getline(fullPath,fullPathString);
  fullPath.close();
  output->setOutputTimeDir(output->getOutputTimeDir());
  outTimeDir << output->getOutputTimeDir();
  outTimeDir.close();
  FileSystemUtil::copyFile(tempDir + "\\outputDmps\\timeBetweenRuns.txt", userProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRunsTemp.txt", true);
  path = tempDir + "\\outTimeDir.txt";
  std::ifstream inTimeDir(path);
  std::string timeDir;
  getline(inTimeDir,timeDir);
  inTimeDir.close();
  FileSystemUtil::copyDirectoryAndContents(userProfile + "\\SysnativeBSODApps\\dmpOptions",timeDir + "\\SysnativeBSODApps\\dmpOptions", true);
  std::stringstream errorss;
  std::string errorString;
  errorss << "time to run = ";
  errorss << diff * 1000;
  errorss << " seconds";
  errorString = errorss.str();
  // convert the command std::string to String format
  System::String^ str2 = gcnew System::String(errorString.c_str());
  std::string inHtmlPath;
  inHtmlPath = userProfile + "\\SysnativeBSODApps\\dmpOptions\\inHtml.txt";
  std::ifstream inHtml(inHtmlPath);
  int inHtmlInt;
  output->input->showMessageBoxes();
  if(inHtml.good()){
    inHtml >> inHtmlInt;
    inHtmlInt = 1;
  }
  else{
    inHtmlInt = 1;
  }
  if(inHtmlInt && output->input->inHtml){
    outTimeDir.open(tempDir + "\\outTimeDir.txt");
    System::String^ myPath = System::IO::Directory::GetCurrentDirectory();
    std::ofstream outFullPath(tempDir + "\\fullPath.txt");
    outFullPath << SystemStandardConversionUtil::convertSystemStringToStdString(myPath);
    outFullPath.close();
    fullPath.open(tempDir + "\\fullPath.txt");
    fullPathString = "";
    getline(fullPath,fullPathString);
    fullPath.close();
    outTimeDir << output->getOutputTimeDir();
    outTimeDir.close();
    Sleep(105);
  Form3^ myForm3;
  myForm3 = gcnew Form3(); 
  myForm3->timeS = str2; 
  myForm3->label4->Text = str2;
  myForm3->tempStringS = gcnew System::String(tempDir.c_str());
  myForm3->centerForm3();
  myForm3->fileListS = fileListS; 
  myForm3->loadFiles();
  System::Windows::Forms::Application::Run(myForm3);
  fileListS = myForm3->fileListS;
    FileSystemUtil::moveFile(tempDir + "\\outTimeDir.txt", userProfile + "\\SysnativeBSODApps\\dmpOptions\\outTimeDir.txt");
    std::ofstream outPercentDone;
    outPercentDone.open(tempDir + "\\percentDone.txt");
    outPercentDone << 100;
    outPercentDone.close();
  }
  delete output;
  // Move the currentDir.txt file to tmp so it is deleted during cleanup
  copy1 = "currentDir.txt";
  copy2 = tempDir + "\\currentDir.txt";
  FileSystemUtil::copyFile(copy1, copy2, true);
  remove(copy1.c_str());
  // Copy the output to %userprofile%\SysnativeResults
  std::string DSOutputDmpsDir;
  std::ifstream inOutputDmpsDir(tempDir + "\\outputDmpsDir.txt");
  getline(inOutputDmpsDir, DSOutputDmpsDir);
  inOutputDmpsDir.close();
  myPath = System::IO::Directory::GetCurrentDirectory();
  std::string currentDir = SystemStandardConversionUtil::convertSystemStringToStdString(myPath);
  if(sysnativeResultsChecked)
  {
    FileSystemUtil::copyDirectoryAndContents(currentDir + "\\" + DSOutputDmpsDir, SystemStandardConversionUtil::convertSystemStringToStdString(sysnativeResultsS), false);
  }
  
  std::ofstream outfile(userProfile + "\\SysnativeBSODApps\\forumSettings\\default.dzdn");
  outfile << "## ###outputOrder_list" << std::endl;
  for(int i=0; i<fileListS->Length; i++)
  {
    outfile << SystemStandardConversionUtil::convertSystemStringToStdString(fileListS[i]);
    if(i < fileListS->Length-1)
    {
      outfile << std::endl;
    }
  }
  outfile << std::endl << "## ###parallelThreading_nu" << std::endl;
  outfile << parallelChecked << std::endl;
  outfile << defaultThreads << std::endl;
  outfile << userThreads;   
  outfile << std::endl << "## ###sysnativeResults_cb" << std::endl;
  outfile << sysnativeResultsChecked;
  outfile << std::endl << "## ###sysnativeResults_s" << std::endl;
  outfile << SystemStandardConversionUtil::convertSystemStringToStdString(sysnativeResultsS);
  if(kdPaths != nullptr)
  {
    outfile << std::endl << "## ###kdPaths" << std::endl;
    for(int i=0; i<kdPaths->Length; i++)
    {
      outfile << SystemStandardConversionUtil::convertSystemStringToStdString(kdPaths[i]);
      if(i < kdPaths->Length-1)
      {
        outfile << std::endl;
      }
    }
    outfile << std::endl << "## ###kdActive" << std::endl;
    for(int i=0; i<kdActive->Length; i++)
    {
      outfile << kdActive[i];
      if(i < kdActive->Length-1)
      {
        outfile << std::endl;
      }
    }
  }
  outfile.close();

#ifdef _DEBUG

  _CrtDumpMemoryLeaks();

#endif

  return 0;
}
