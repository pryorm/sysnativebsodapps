#include "outputOrder.h"

#include "debug.h"


//#include <vld.h>

using namespace SysnativeBSODApps;

    // OK button
System::Void outputOrder::button2_Click(System::Object^  sender, System::EventArgs^  e)
{
  okClicked = 1;
  Close();
}
    // Cancel button
System::Void outputOrder::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
  cancelClicked = 1;
  Close();
}

// Up Button
System::Void outputOrder::button5_Click(System::Object^  sender, System::EventArgs^  e) 
{
  if(listBox1->SelectedItem)
  {
    for(int i=1; i<fileListS->Length; i++)
    {
      if(listBox1->SelectedItem->ToString() == fileListS[i])
      {
        System::String^ tempS = fileListS[i-1];
        fileListS[i-1] = fileListS[i];
        fileListS[i] = tempS;
        listBox1->Items[i] = fileListS[i];
        listBox1->Items[i-1] = fileListS[i-1];
        listBox1->SelectedIndex = i-1;
      }
    }
  }
}

// Down Button
System::Void outputOrder::button6_Click(System::Object^  sender, System::EventArgs^  e) 
{
  if(listBox1->SelectedItem)
  {
    for(int i=fileListS->Length-2; i>=0; i--)
    {
      if(listBox1->SelectedItem->ToString() == fileListS[i])
      {
        System::String^ tempS = fileListS[i+1];
        fileListS[i+1] = fileListS[i];
        fileListS[i] = tempS;
        listBox1->Items[i] = fileListS[i];
        listBox1->Items[i+1] = fileListS[i+1];
        listBox1->SelectedIndex = i+1;
      }
    }
  }
}

// Load file list
System::Void outputOrder::outputOrder_Load(System::Object^  sender, System::EventArgs^  e)
{           
  for(int j=0; j<fileListS->Length; j++)
  {
    listBox1->Items->Add(fileListS[j]);
  }
}

// Check if the user wants to save file list order
System::Void outputOrder::outputOrder_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e)
{
  if(!okClicked && !cancelClicked)
  {
    if(System::Windows::Forms::MessageBox::Show("Do you want to save the output order?",L"Closed", System::Windows::Forms::MessageBoxButtons::YesNo, System::Windows::Forms::MessageBoxIcon::Question,
		System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly) == System::Windows::Forms::DialogResult::Yes)
    {
      okClicked = 1;
    }
  }
}