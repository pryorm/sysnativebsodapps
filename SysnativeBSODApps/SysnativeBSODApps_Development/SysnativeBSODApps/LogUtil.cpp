#include "LogUtil.h"

#include "ConfigurationUtil.h"
#include "FileSystemUtil.h"
#include "SystemStandardConversionUtil.h"

#include <fstream>
#include <iostream>

void LogUtil::logError()
{
    std::string logMsgStr = SystemStandardConversionUtil::convertSystemStringToStdString(mLogMsg);
    std::string logDir = ConfigurationUtil::getLogDirectory();
    std::string logFile = logDir + "\\SysnativeBSODApps.log";
    FileSystemUtil::createDirectoryWithBackup(ConfigurationUtil::getUserProfilePath() + "\\SysnativeBSODApps", "Logs");

    std::ofstream logStream(logFile, std::ios::app);
    if (logStream.is_open())
    {
        logStream << logMsgStr << std::endl;
        logStream.close();
    }
    else
    {
        std::cerr << "Failed to open log file: " << logFile << std::endl;
    }
}

// Async logging of error messages
System::Threading::Tasks::Task^ LogUtil::logErrorAsync(System::String^ logMsg)
{
    mLogMsg = logMsg;
    return System::Threading::Tasks::Task::Factory->StartNew(gcnew System::Action(LogUtil::logError));
}

void LogUtil::logErrorAsync(const std::string& logMsg)
{
    System::String^ systemLogMsg = SystemStandardConversionUtil::convertStdStringToSystemString(logMsg);
    logErrorAsync(systemLogMsg);
}