#include "DmpOptions.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>  // for replace_if

#include "debug.h"


//#include <vld.h>

// The constructor uses the user profile path to obtain
//    the options for the output files from SysnativeBSODApps
DmpOptions::DmpOptions(std::string userProfile, std::string parms){
  DOUserProfile = userProfile;
  DOParms = parms;
  getSymbols();
  getDriverList();
  getOpenOnExit();
  getUrlQuestion();
  getNonParsed();
  getAllFilesDmps();
  getAddSpaces();
  getOldDriverAfter();
  getASACPI();
  getProblemDrivers();
  getkdCommands();
  getParms();
}
DmpOptions::~DmpOptions(){
}

void DmpOptions::getSymbols(){
  // Read lines from the dmpOptions symbols.txt file
  std::string inputCh;
  DODmpOptions = DOUserProfile + "\\SysnativeBSODApps\\dmpOptions";
  std::string options = DODmpOptions + "\\symbols.txt";
  std::ifstream infileOptions(options);
  symbolsString = "";
  std::string symbolsString2 = "";
  int symbolsLocal = -1; // MIP TODO: This is dangerous and needs to be addressed
  // Determine whether the symbols.txt file exists
  if(infileOptions.good()){
    // If it exists, obtain the symbols path
    size_t foundPound;
    // Get only the symbols path and ignore any commented lines
    int countOptions = 0;
    while(!infileOptions.eof()){
      getline(infileOptions,inputCh);
      foundPound = inputCh.find("#");
      while(foundPound != std::string::npos){
        getline(infileOptions,inputCh);
        foundPound = inputCh.find("#");  
      }
      countOptions++;
      if (countOptions == 1){
        symbolsString = inputCh;
      }
      if (countOptions == 2){
        symbolsString2 = inputCh;
      }
      if (countOptions == 3){
        std::stringstream sstemp;
        sstemp << inputCh;
        sstemp >> symbolsInt;
      }
      if (countOptions == 4){
        std::stringstream sstemp;
        sstemp << inputCh;
        sstemp >> symbolsLocal;
      }
    }
    infileOptions.close();
    if(!symbolsLocal){
      symbolsString = symbolsString2;
      symbolsInt = 1;
    }
  }
  else{
    // if it does not exist, use online symbols and create symbols.txt
    symbolsString = "c:\\symbols";
    symbolsString2 = "srv*c:\\symbols*https://msdl.microsoft.com/download/symbols";
    infileOptions.close();
    std::ofstream outfile(options);
    outfile << symbolsString << std::endl;
    outfile << symbolsString2 << std::endl;
    outfile << "2" << std::endl;
    outfile << "1";
    symbolsInt = 2;
    symbolsLocal = 1;
    outfile.close();
    symbolsString = symbolsString;
  }
}

void DmpOptions::getDriverList(){
  // Read lines from the dmpOptions driverInOut.txt file
  std::string inputCh;
  std::string options = DODmpOptions + "\\driverInOut.txt";
  std::ifstream infileOptions(options);
  // Determine whether the driverInOut.txt file exists
  if(infileOptions.good()){
    // If it exists, obtain the formatting of the driver lists
    size_t foundPound;
    // Get only the formatting information and ignore commented lines
    int countOptions = 0;
    while(!infileOptions.eof()){
      getline(infileOptions,inputCh);
      foundPound = inputCh.find("#");
      while(foundPound != std::string::npos){
        getline(infileOptions,inputCh);
        foundPound = inputCh.find("#");  
      }
      countOptions++;
      if (countOptions < 2){
        driverInSite = inputCh;
      }
      else if(countOptions < 3){
        driverOutSite = inputCh;
      }
    }
    infileOptions.close();
  }
  else{
    // if driverInOut.txt does not exist, create the formatting for the drivers by default
    driverInSite = "https://www.sysnative.com/drivers/driver.php?id=";
    driverOutSite = "https://www.sysnative.com/drivers/driver.php?id=[B][COLOR=BLUE]<driver Name>[/COLOR][/B]";
    infileOptions.close();
    std::ofstream outfile(options);
    outfile << driverInSite << std::endl;
    outfile << driverOutSite;
    outfile.close();
  }
}

void DmpOptions::getTemplate(){  
  std::string inChar;
  std::string options = DODmpOptions + "\\template.txt";
  std::ifstream infileOptions(options);
  templateExists = 0;
  // Determine whether the template.txt file exists
  if(infileOptions.good()){
    templateExists = 1;
    // if it exists, read through the file and get template formatting
    while(!infileOptions.eof()){
      getline(infileOptions,inChar);
      size_t found;
      found = inChar.find("# ");
      if((found != std::string::npos) || (inChar.length() > 0 && inChar[0] == '#')){
      }
      else{
        // save the template formatting to a vector std::string
        fullTemplate.push_back(inChar);
      }
    }
  }
  infileOptions.close();
}

void DmpOptions::getOutputOptions(){
  std::string inChar;
  std::string temp = DODmpOptions + "\\outputOptions.txt";
  std::ifstream infile(temp);
  // Determine whether the outputOptions.txt file exists
  if(infile.good()){
    loadingDumpFile = 0;
    kernelVersion = 0;
    missingServicePack = 0;
    builtBy = 0;
    debugSessionTime = 0;
    systemUptime = 0;
    bugCheck = 0;
    unableToVerify = 0;
    symbolsNotLoaded = 0;
    probablyCausedBy = 0;
    defaultBucketID = 0;
    verifier = 0;
    bugcheckStr = 0;
    bugCheckAnalysis = 0;
    processName = 0;
    failureBucketID = 0;
    bugcheckCode = 0;
    errorCode = 0;
    diskHardwareError = 0;
    arguments = 0;
    CPUID = 0;
    maxSpeed = 0;
    currentSpeed = 0;
    overclock = 0;
    BIOSVersion = 0;
    BIOSReleaseDate = 0;
    systemManufacturer = 0;
    systemProductName = 0;
    outputDmps = 0;
    outputFullDrivers = 0;
    outputThirdPartyDriversName = 0;
    outputThirdPartyDriversDate = 0;
    outputImportantInfo = 0;
    outputStack = 0;
    outputMissingFromDRT = 0;
    outputUnloadedDrivers = 0;
    outputSMBIOS = 0;
    outputTemplate = 0;
    outputNinetyNine = 0;
    outputNinetyEight = 0;
    outputNinetySeven = 0;
    outputNinetyFive = 0;
    outputEightyEight = 0;
    // get the output options for the .dmp file analysis information
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        size_t found;        
        found = inChar.find("outputDmps");
        if(found != std::string::npos){
          outputDmps = 1;
        }        
        found = inChar.find("outputFullDrivers");
        if(found != std::string::npos){
          outputFullDrivers = 1;
        }        
        found = inChar.find("outputThirdPartyDriversName");
        if(found != std::string::npos){
          outputThirdPartyDriversName = 1;
        }        
        found = inChar.find("outputThirdPartyDriversDate");
        if(found != std::string::npos){
          outputThirdPartyDriversDate = 1;
        }        
        found = inChar.find("outputImportantInfo");
        if(found != std::string::npos){
          outputImportantInfo = 1;
        }        
        found = inChar.find("outputStack");
        if(found != std::string::npos){
          outputStack = 1;
        }        
        found = inChar.find("outputMissingFromDRT");
        if(found != std::string::npos){
          outputMissingFromDRT = 1;
        }        
        found = inChar.find("outputUnloadedDrivers");
        if(found != std::string::npos){
          outputUnloadedDrivers = 1;
        }        
        found = inChar.find("outputSMBIOS");
        if(found != std::string::npos){
          outputSMBIOS= 1;
        }        
        found = inChar.find("outputTemplate");
        if(found != std::string::npos){
          outputTemplate = 1;
        }      
        found = inChar.find("outputNinetyNine");
        if(found != std::string::npos){
          outputNinetyNine = 1;
        }      
        found = inChar.find("outputNinetyEight");
        if(found != std::string::npos){
          outputNinetyEight = 1;
        }      
        found = inChar.find("outputNinetySeven");
        if(found != std::string::npos){
          outputNinetySeven = 1;
        }    
        found = inChar.find("outputNinetyFive");
        if(found != std::string::npos){
          outputNinetyFive = 1;
        }    
        found = inChar.find("outputEightyEight");
        if(found != std::string::npos){
          outputEightyEight = 1;
        }  
        found = inChar.find("loadingDumpFile");
        if(found != std::string::npos){
          loadingDumpFile = 1;
        }
        found = inChar.find("kernelVersion");
        if(found != std::string::npos){
          kernelVersion = 1;
        }
        found = inChar.find("missingServicePack");
        if(found != std::string::npos){
          missingServicePack = 1;
        }
        found = inChar.find("builtBy");
        if(found != std::string::npos){
          builtBy = 1;
        }
        found = inChar.find("debugSessionTime");
        if(found != std::string::npos){
          debugSessionTime = 1;
        }
        found = inChar.find("systemUptime");
        if(found != std::string::npos){
          systemUptime = 1;
        }
        found = inChar.find("bugCheck");
        if(found != std::string::npos){
          bugCheck = 1;
        }
        found = inChar.find("unableToVerify");
        if(found != std::string::npos){
          unableToVerify = 1;
        }
        found = inChar.find("symbolsNotLoaded");
        if(found != std::string::npos){
          symbolsNotLoaded = 1;
        }
        found = inChar.find("probablyCausedBy");
        if(found != std::string::npos){
          probablyCausedBy = 1;
        }
        found = inChar.find("defaultBucketID");
        if(found != std::string::npos){
          defaultBucketID = 1;
        }
        found = inChar.find("verifier");
        if(found != std::string::npos){
          verifier = 1;
        }
        found = inChar.find("bugcheckStr");
        if(found != std::string::npos){
          bugcheckStr = 1;
        }
        found = inChar.find("bugCheckAnalysis");
        if(found != std::string::npos){
          bugCheckAnalysis = 1;
        }
        found = inChar.find("processName");
        if(found != std::string::npos){
          processName = 1;
        }
        found = inChar.find("failureBucketID");
        if(found != std::string::npos){
          failureBucketID = 1;
        }
        found = inChar.find("bugcheckCode");
        if(found != std::string::npos){
          bugcheckCode = 1;
        }
        found = inChar.find("errorCode");
        if(found != std::string::npos){
          errorCode = 1;
        }
        found = inChar.find("diskHardwareError");
        if(found != std::string::npos){
          diskHardwareError = 1;
        }
        found = inChar.find("arguments");
        if(found != std::string::npos){
          arguments = 1;
        }
        found = inChar.find("CPUID");
        if(found != std::string::npos){
          CPUID = 1;
        }
        found = inChar.find("maxSpeed");
        if(found != std::string::npos){
          maxSpeed = 1;
        }
        found = inChar.find("currentSpeed");
        if(found != std::string::npos){
          currentSpeed = 1;
        }
        found = inChar.find("overclock");
        if(found != std::string::npos){
          overclock = 1;
        }
        found = inChar.find("BIOSVersion");
        if(found != std::string::npos){
          BIOSVersion = 1;
        }
        found = inChar.find("BIOSReleaseDate");
        if(found != std::string::npos){
          BIOSReleaseDate = 1;
        }
        found = inChar.find("systemManufacturer");
        if(found != std::string::npos){
          systemManufacturer = 1;
        }
        found = inChar.find("systemProductName");
        if(found != std::string::npos){
          systemProductName = 1;
        }
      }
    }
    infile.close();
  }
  else{  
    // the default for all options is one if the file does not exist
    loadingDumpFile = 1;
    kernelVersion = 1;
    missingServicePack = 1;
    builtBy = 1;
    debugSessionTime = 1;
    systemUptime = 1;
    bugCheck = 1;
    unableToVerify = 1;
    symbolsNotLoaded = 1;
    probablyCausedBy = 1;
    defaultBucketID = 1;
    verifier = 1;
    bugcheckStr = 1;
    bugCheckAnalysis = 1;
    processName = 1;
    failureBucketID = 1;
    bugcheckCode = 1;
    errorCode = 1;
    diskHardwareError = 1;
    arguments = 1;
    CPUID = 1;
    maxSpeed = 1;
    currentSpeed = 1;
    overclock = 1;
    BIOSVersion = 1;
    BIOSReleaseDate = 1;
    systemManufacturer = 1;
    systemProductName = 1;
    outputDmps = 0;
    outputFullDrivers = 0;
    outputThirdPartyDriversName = 0;
    outputThirdPartyDriversDate = 0;
    outputImportantInfo = 0;
    outputStack = 0;
    outputMissingFromDRT = 0;
    outputUnloadedDrivers = 0;
    outputSMBIOS = 0;
    outputTemplate = 0;
    outputNinetyNine = 1;
    outputNinetyEight = 1;
    outputNinetySeven = 1;
    outputNinetyFive = 1;
    outputEightyEight = 1;
    
    infile.close();
    std::ofstream outfile(temp);
    outfile << "outputNinetyNine" << std::endl;
    outfile << "outputNinetyEight" << std::endl;
    outfile << "outputNinetySeven" << std::endl;
    outfile << "outputNinetyFive" << std::endl;
    outfile << "outputEightyEight" << std::endl;
    outfile << "loadingDumpFile" << std::endl;
    outfile << "kernelVersion" << std::endl;
    outfile << "missingServicePack" << std::endl;
    outfile << "builtBy" << std::endl;
    outfile << "debugSessionTime" << std::endl;
    outfile << "systemUptime" << std::endl;
    outfile << "bugCheck" << std::endl;
    outfile << "unableToVerify" << std::endl;
    outfile << "symbolsNotLoaded" << std::endl;
    outfile << "probablyCausedBy" << std::endl;
    outfile << "defaultBucketID" << std::endl;
    outfile << "verifier" << std::endl;
    outfile << "bugcheckStr" << std::endl;
    outfile << "bugCheckAnalysis" << std::endl;
    outfile << "processName" << std::endl;
    outfile << "failureBucketID" << std::endl;
    outfile << "bugcheckCode" << std::endl;
    outfile << "errorCode" << std::endl;
    outfile << "diskHardwareError" << std::endl;
    outfile << "arguments" << std::endl;
    outfile << "CPUID" << std::endl;
    outfile << "maxSpeed" << std::endl;
    outfile << "currentSpeed" << std::endl;
    outfile << "BIOSVersion" << std::endl;
    outfile << "BIOSReleaseDate" << std::endl;
    outfile << "systemManufacturer" << std::endl;
    outfile << "systemProductName" << std::endl;
    outfile << "overclock" << std::endl;
    outfile.close();
  }
}

// determine whether the user wants all files to open on exit
void DmpOptions::getOpenOnExit(){  
  std::string inChar;
  std::string temp = DODmpOptions + "\\openOnExit.txt";
  std::ifstream infile(temp);
  // Determine whether the openOnExit.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> openOnExit;
      }
    }
  }
  else{
    // if the file does not exist, open all files on exit as default action
    openOnExit = 1;
  }
  infile.close();  
  
  temp = DODmpOptions + "\\inHtml.txt";
  infile.open(temp);
  // Determine whether the openOnExit.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inHtml;
      }
    }
  }
  else{
    // if the file does not exist, open all files on exit as default action
    inHtml = 1;
  }
  infile.close();
  openOnExit = 0;
  inHtml = 1;
}

// determine whether the user wants to participate in DRT updates
void DmpOptions::getUrlQuestion(){  
  std::string inChar;
  std::string temp = DODmpOptions + "\\urlQuestion.txt";
  std::ifstream infile(temp);
  // Determine whether the urlQuestion.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> urlQuestion;
      }
    }
  }
  else{
    // if the file does not exist, ask for URL info as default action
    urlQuestion = 1;
  }
  urlQuestion = 0;
  infile.close();
}

// determine whether the user wants BBCode in importantInfo.txt
void DmpOptions::getNonParsed(){  
  std::string inChar;
  std::string temp = DODmpOptions + "\\noBBCode.txt";
  std::ifstream infile(temp);
  // Determine whether the noBBCode.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> nonParsed;
      }
    }
  }
  else{
    // if the file does not exist, do not write BBCode as default action
    nonParsed = 1;
  }
  infile.close();

  temp = DODmpOptions + "\\turnOffBBCode.txt";
  infile.open(temp);
  // Determine whether the turnOffBBCode.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> turnOffBBCode;
      }
    }
  }
  else{
    // if the file does not exist, write BBCode as default action
    turnOffBBCode = 0;
  }
  infile.close();

  temp = DODmpOptions + "\\noBBCodeCodeBoxes.txt";
  infile.open(temp);
  // Determine whether the turnOffBBCode.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> noBBCodeCodeBoxes;
      }
    }
  }
  else{
    // if the file does not exist, write BBCode as default action
    noBBCodeCodeBoxes = 0;
  }
  infile.close();

  temp = DODmpOptions + "\\oldDriversRed.txt";
  infile.open(temp);
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> oldDriversRed;
      }
    }
  }
  else{
    // if the file does not exist, turn on red as default action
    oldDriversRed = 1;
  }
  infile.close();
}

void DmpOptions::getAllFilesDmps(){
  std::string temp;
  std::string inChar;
  temp = DODmpOptions + "\\allFilesDmps.txt";
  std::ifstream infile(temp);
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> allFilesDmps;
      }
    }
  }
  else{
    // if the file does not exist, check all files as default action
    allFilesDmps = 1;
  }
  infile.close();
}

// determine whether the user wants spaces between items in the importantInfo.txt file
void DmpOptions::getAddSpaces(){  
  std::string inChar;
  std::string temp = DODmpOptions + "\\addSpaces.txt";
  std::ifstream infile(temp);
  // Determine whether the addSpaces.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> addSpaces;
      }
    }
  }
  else{
    // if the file does not exist, add spaces as default action
    addSpaces = 1;
  }
  infile.close();
}

// determine the year that any year prior to will have red highlighting for the driver list
void DmpOptions::getOldDriverAfter(){  
  std::string inChar;
  std::string temp = DODmpOptions + "\\XPoldDriverAfter.txt";
  std::ifstream infile(temp);
  // Determine whether the oldDriverAfter.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
        oldDriverAfter = inChar;
        if (oldDriverAfter.find("Jan") != std::string::npos || oldDriverAfter.find("jan") != std::string::npos || oldDriverAfter == "1"){
          month = 1;
        }
        if (oldDriverAfter.find("Feb") != std::string::npos || oldDriverAfter.find("feb") != std::string::npos || oldDriverAfter == "2"){
          month = 2;
        }
        if (oldDriverAfter.find("Mar") != std::string::npos || oldDriverAfter.find("mar") != std::string::npos || oldDriverAfter == "3"){
          month = 3;
        }
        if (oldDriverAfter.find("Apr") != std::string::npos || oldDriverAfter.find("apr") != std::string::npos || oldDriverAfter == "4"){
          month = 4;
        }
        if (oldDriverAfter.find("May") != std::string::npos || oldDriverAfter.find("may") != std::string::npos || oldDriverAfter == "5"){
          month = 5;
        }
        if (oldDriverAfter.find("Jun") != std::string::npos || oldDriverAfter.find("jun") != std::string::npos || oldDriverAfter == "6"){
          month = 6;
        }
        if (oldDriverAfter.find("Jul") != std::string::npos || oldDriverAfter.find("jul") != std::string::npos || oldDriverAfter == "7"){
          month = 7;
        }
        if (oldDriverAfter.find("Aug") != std::string::npos || oldDriverAfter.find("aug") != std::string::npos || oldDriverAfter == "8"){
          month = 8;
        }
        if (oldDriverAfter.find("Sep") != std::string::npos || oldDriverAfter.find("sep") != std::string::npos || oldDriverAfter == "9"){
          month = 9;
        }
        if (oldDriverAfter.find("Oct") != std::string::npos || oldDriverAfter.find("oct") != std::string::npos || oldDriverAfter == "10"){
          month = 10;
        }
        if (oldDriverAfter.find("Nov") != std::string::npos || oldDriverAfter.find("nov") != std::string::npos || oldDriverAfter == "11"){
          month = 11;
        }
        if (oldDriverAfter.find("Dec") != std::string::npos || oldDriverAfter.find("dec") != std::string::npos || oldDriverAfter == "12"){
          month = 12;
        }
        sstemp >> inChar;
        std::stringstream sstempDay;
        sstempDay << inChar;
        sstempDay >> day;
        oldDriverAfter = oldDriverAfter + " " + inChar;
        sstemp >> inChar;
        std::stringstream sstempYear;
        sstempYear << inChar;
        sstempYear >> year;
        XPmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
        oldDriverAfter = oldDriverAfter + " " + inChar;
      }
    }
  }
  else{
    // if the file does not exist, make Aug. 25, 2004 the default
    oldDriverAfter = "Aug 25 2004";
    month = 8;
    day = 25;
    year = 2004;
    XPmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  std::stringstream sstemp;
  std::string str2;
  std::string str3;
  std::string str4;
  sstemp << oldDriverAfter;
  sstemp >> str2;
  sstemp >> str3;
  sstemp >> str4;
  if(str2 == str3 && str2 == str4){
    oldDriverAfter = "Dec 31 " + str2;
    month = 12;
    day = 31;
    XPmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  if(str3 == str4){
    std::string oldDriverAfter = str2 + " 1" + str4;
    day = 1;
    XPmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  infile.close();
  
  temp = DODmpOptions + "\\VistaoldDriverAfter.txt";
  infile.open(temp);
  // Determine whether the oldDriverAfter.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        sstemp.clear();
        sstemp.str("");
        sstemp.clear();
        sstemp << inChar;
        sstemp >> inChar;
        oldDriverAfter = inChar;
        if (oldDriverAfter.find("Jan") != std::string::npos || oldDriverAfter.find("jan") != std::string::npos || oldDriverAfter == "1"){
          month = 1;
        }
        if (oldDriverAfter.find("Feb") != std::string::npos || oldDriverAfter.find("feb") != std::string::npos || oldDriverAfter == "2"){
          month = 2;
        }
        if (oldDriverAfter.find("Mar") != std::string::npos || oldDriverAfter.find("mar") != std::string::npos || oldDriverAfter == "3"){
          month = 3;
        }
        if (oldDriverAfter.find("Apr") != std::string::npos || oldDriverAfter.find("apr") != std::string::npos || oldDriverAfter == "4"){
          month = 4;
        }
        if (oldDriverAfter.find("May") != std::string::npos || oldDriverAfter.find("may") != std::string::npos || oldDriverAfter == "5"){
          month = 5;
        }
        if (oldDriverAfter.find("Jun") != std::string::npos || oldDriverAfter.find("jun") != std::string::npos || oldDriverAfter == "6"){
          month = 6;
        }
        if (oldDriverAfter.find("Jul") != std::string::npos || oldDriverAfter.find("jul") != std::string::npos || oldDriverAfter == "7"){
          month = 7;
        }
        if (oldDriverAfter.find("Aug") != std::string::npos || oldDriverAfter.find("aug") != std::string::npos || oldDriverAfter == "8"){
          month = 8;
        }
        if (oldDriverAfter.find("Sep") != std::string::npos || oldDriverAfter.find("sep") != std::string::npos || oldDriverAfter == "9"){
          month = 9;
        }
        if (oldDriverAfter.find("Oct") != std::string::npos || oldDriverAfter.find("oct") != std::string::npos || oldDriverAfter == "10"){
          month = 10;
        }
        if (oldDriverAfter.find("Nov") != std::string::npos || oldDriverAfter.find("nov") != std::string::npos || oldDriverAfter == "11"){
          month = 11;
        }
        if (oldDriverAfter.find("Dec") != std::string::npos || oldDriverAfter.find("dec") != std::string::npos || oldDriverAfter == "12"){
          month = 12;
        }
        sstemp >> inChar;
        std::stringstream sstempDay;
        sstempDay << inChar;
        sstempDay >> day;
        oldDriverAfter = oldDriverAfter + " " + inChar;
        sstemp >> inChar;
        std::stringstream sstempYear;
        sstempYear << inChar;
        sstempYear >> year;
        VistamyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
        oldDriverAfter = oldDriverAfter + " " + inChar;
      }
    }
  }
  else{
    // if the file does not exist, make Apr. 28, 2009 the default
    oldDriverAfter = "Apr 28 2009";
    month = 4;
    day = 28;
    year = 2009;
    VistamyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  sstemp.clear();
  sstemp.str("");
  sstemp.clear();
  str2;
  str3;
  str4;
  sstemp << oldDriverAfter;
  sstemp >> str2;
  sstemp >> str3;
  sstemp >> str4;
  if(str2 == str3 && str2 == str4){
    oldDriverAfter = "Dec 31 " + str2;
    month = 12;
    day = 31;
    VistamyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  if(str3 == str4){
    std::string oldDriverAfter = str2 + " 1" + str4;
    day = 1;
    VistamyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  infile.close();
  
  temp = DODmpOptions + "\\7oldDriverAfter.txt";
  infile.open(temp);
  // Determine whether the oldDriverAfter.txt file exists
  int SevenOldDriver = 0;
  if(infile.good()){
    SevenOldDriver = 1;
  }
  else{
    infile.close();
    temp = DODmpOptions + "\\oldDriverAfter.txt";
    infile.open(temp);
    if(infile.good()){
      SevenOldDriver = 1;
    }
  }
  if(SevenOldDriver){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        sstemp.clear();
        sstemp.str("");
        sstemp.clear();
        sstemp << inChar;
        sstemp >> inChar;
        oldDriverAfter = inChar;
        if (oldDriverAfter.find("Jan") != std::string::npos || oldDriverAfter.find("jan") != std::string::npos || oldDriverAfter == "1"){
          month = 1;
        }
        if (oldDriverAfter.find("Feb") != std::string::npos || oldDriverAfter.find("feb") != std::string::npos || oldDriverAfter == "2"){
          month = 2;
        }
        if (oldDriverAfter.find("Mar") != std::string::npos || oldDriverAfter.find("mar") != std::string::npos || oldDriverAfter == "3"){
          month = 3;
        }
        if (oldDriverAfter.find("Apr") != std::string::npos || oldDriverAfter.find("apr") != std::string::npos || oldDriverAfter == "4"){
          month = 4;
        }
        if (oldDriverAfter.find("May") != std::string::npos || oldDriverAfter.find("may") != std::string::npos || oldDriverAfter == "5"){
          month = 5;
        }
        if (oldDriverAfter.find("Jun") != std::string::npos || oldDriverAfter.find("jun") != std::string::npos || oldDriverAfter == "6"){
          month = 6;
        }
        if (oldDriverAfter.find("Jul") != std::string::npos || oldDriverAfter.find("jul") != std::string::npos || oldDriverAfter == "7"){
          month = 7;
        }
        if (oldDriverAfter.find("Aug") != std::string::npos || oldDriverAfter.find("aug") != std::string::npos || oldDriverAfter == "8"){
          month = 8;
        }
        if (oldDriverAfter.find("Sep") != std::string::npos || oldDriverAfter.find("sep") != std::string::npos || oldDriverAfter == "9"){
          month = 9;
        }
        if (oldDriverAfter.find("Oct") != std::string::npos || oldDriverAfter.find("oct") != std::string::npos || oldDriverAfter == "10"){
          month = 10;
        }
        if (oldDriverAfter.find("Nov") != std::string::npos || oldDriverAfter.find("nov") != std::string::npos || oldDriverAfter == "11"){
          month = 11;
        }
        if (oldDriverAfter.find("Dec") != std::string::npos || oldDriverAfter.find("dec") != std::string::npos || oldDriverAfter == "12"){
          month = 12;
        }
        sstemp >> inChar;
        std::stringstream sstempDay;
        sstempDay << inChar;
        sstempDay >> day;
        oldDriverAfter = oldDriverAfter + " " + inChar;
        sstemp >> inChar;
        std::stringstream sstempYear;
        sstempYear << inChar;
        sstempYear >> year;
        SevenmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
        oldDriverAfter = oldDriverAfter + " " + inChar;
      }
    }
  }
  else{
    // if the file does not exist, make Jul 12, 2009 the default
    oldDriverAfter = "Jul 12 2009";
    month = 7;
    day = 12;
    year = 2009;
    SevenmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  sstemp.clear();
  sstemp.str("");
  sstemp.clear();
  str2;
  str3;
  str4;
  sstemp << oldDriverAfter;
  sstemp >> str2;
  sstemp >> str3;
  sstemp >> str4;
  if(str2 == str3 && str2 == str4){
    oldDriverAfter = "Dec 31 " + str2;
    month = 12;
    day = 31;
    SevenmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  if(str3 == str4){
    std::string oldDriverAfter = str2 + " 1" + str4;
    day = 1;
    SevenmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  infile.close();
  
  temp = DODmpOptions + "\\8oldDriverAfter.txt";
  infile.open(temp);
  // Determine whether the oldDriverAfter.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        sstemp.clear();
        sstemp.str("");
        sstemp.clear();
        sstemp << inChar;
        sstemp >> inChar;
        oldDriverAfter = inChar;
        if (oldDriverAfter.find("Jan") != std::string::npos || oldDriverAfter.find("jan") != std::string::npos || oldDriverAfter == "1"){
          month = 1;
        }
        if (oldDriverAfter.find("Feb") != std::string::npos || oldDriverAfter.find("feb") != std::string::npos || oldDriverAfter == "2"){
          month = 2;
        }
        if (oldDriverAfter.find("Mar") != std::string::npos || oldDriverAfter.find("mar") != std::string::npos || oldDriverAfter == "3"){
          month = 3;
        }
        if (oldDriverAfter.find("Apr") != std::string::npos || oldDriverAfter.find("apr") != std::string::npos || oldDriverAfter == "4"){
          month = 4;
        }
        if (oldDriverAfter.find("May") != std::string::npos || oldDriverAfter.find("may") != std::string::npos || oldDriverAfter == "5"){
          month = 5;
        }
        if (oldDriverAfter.find("Jun") != std::string::npos || oldDriverAfter.find("jun") != std::string::npos || oldDriverAfter == "6"){
          month = 6;
        }
        if (oldDriverAfter.find("Jul") != std::string::npos || oldDriverAfter.find("jul") != std::string::npos || oldDriverAfter == "7"){
          month = 7;
        }
        if (oldDriverAfter.find("Aug") != std::string::npos || oldDriverAfter.find("aug") != std::string::npos || oldDriverAfter == "8"){
          month = 8;
        }
        if (oldDriverAfter.find("Sep") != std::string::npos || oldDriverAfter.find("sep") != std::string::npos || oldDriverAfter == "9"){
          month = 9;
        }
        if (oldDriverAfter.find("Oct") != std::string::npos || oldDriverAfter.find("oct") != std::string::npos || oldDriverAfter == "10"){
          month = 10;
        }
        if (oldDriverAfter.find("Nov") != std::string::npos || oldDriverAfter.find("nov") != std::string::npos || oldDriverAfter == "11"){
          month = 11;
        }
        if (oldDriverAfter.find("Dec") != std::string::npos || oldDriverAfter.find("dec") != std::string::npos || oldDriverAfter == "12"){
          month = 12;
        }
        sstemp >> inChar;
        std::stringstream sstempDay;
        sstempDay << inChar;
        sstempDay >> day;
        oldDriverAfter = oldDriverAfter + " " + inChar;
        sstemp >> inChar;
        std::stringstream sstempYear;
        sstempYear << inChar;
        sstempYear >> year;
        EightmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
        oldDriverAfter = oldDriverAfter + " " + inChar;
      }
    }
  }
  else{
    // if the file does not exist, make 2012 the default
    oldDriverAfter = "Jan 1 2012";
    month = 1;
    day = 1;
    year = 2012;
    EightmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  sstemp.clear();
  sstemp.str("");
  sstemp.clear();
  str2;
  str3;
  str4;
  sstemp << oldDriverAfter;
  sstemp >> str2;
  sstemp >> str3;
  sstemp >> str4;
  if(str2 == str3 && str2 == str4){
    oldDriverAfter = "Dec 31 " + str2;
    month = 12;
    day = 31;
    EightmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  if(str3 == str4){
    std::string oldDriverAfter = str2 + " 1" + str4;
    day = 1;
    EightmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  infile.close();
  
  temp = DODmpOptions + "\\81oldDriverAfter.txt";
  infile.open(temp);
  // Determine whether the oldDriverAfter.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        sstemp.clear();
        sstemp.str("");
        sstemp.clear();
        sstemp << inChar;
        sstemp >> inChar;
        oldDriverAfter = inChar;
        if (oldDriverAfter.find("Jan") != std::string::npos || oldDriverAfter.find("jan") != std::string::npos || oldDriverAfter == "1"){
          month = 1;
        }
        if (oldDriverAfter.find("Feb") != std::string::npos || oldDriverAfter.find("feb") != std::string::npos || oldDriverAfter == "2"){
          month = 2;
        }
        if (oldDriverAfter.find("Mar") != std::string::npos || oldDriverAfter.find("mar") != std::string::npos || oldDriverAfter == "3"){
          month = 3;
        }
        if (oldDriverAfter.find("Apr") != std::string::npos || oldDriverAfter.find("apr") != std::string::npos || oldDriverAfter == "4"){
          month = 4;
        }
        if (oldDriverAfter.find("May") != std::string::npos || oldDriverAfter.find("may") != std::string::npos || oldDriverAfter == "5"){
          month = 5;
        }
        if (oldDriverAfter.find("Jun") != std::string::npos || oldDriverAfter.find("jun") != std::string::npos || oldDriverAfter == "6"){
          month = 6;
        }
        if (oldDriverAfter.find("Jul") != std::string::npos || oldDriverAfter.find("jul") != std::string::npos || oldDriverAfter == "7"){
          month = 7;
        }
        if (oldDriverAfter.find("Aug") != std::string::npos || oldDriverAfter.find("aug") != std::string::npos || oldDriverAfter == "8"){
          month = 8;
        }
        if (oldDriverAfter.find("Sep") != std::string::npos || oldDriverAfter.find("sep") != std::string::npos || oldDriverAfter == "9"){
          month = 9;
        }
        if (oldDriverAfter.find("Oct") != std::string::npos || oldDriverAfter.find("oct") != std::string::npos || oldDriverAfter == "10"){
          month = 10;
        }
        if (oldDriverAfter.find("Nov") != std::string::npos || oldDriverAfter.find("nov") != std::string::npos || oldDriverAfter == "11"){
          month = 11;
        }
        if (oldDriverAfter.find("Dec") != std::string::npos || oldDriverAfter.find("dec") != std::string::npos || oldDriverAfter == "12"){
          month = 12;
        }
        sstemp >> inChar;
        std::stringstream sstempDay;
        sstempDay << inChar;
        sstempDay >> day;
        oldDriverAfter = oldDriverAfter + " " + inChar;
        sstemp >> inChar;
        std::stringstream sstempYear;
        sstempYear << inChar;
        sstempYear >> year;
        EightOnemyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
        oldDriverAfter = oldDriverAfter + " " + inChar;
      }
    }
  }
  else{
    // if the file does not exist, make Aug. 27, 2013 the default
    oldDriverAfter = "Aug 27 2013";
    month = 8;
    day = 27;
    year = 2013;
    EightOnemyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  sstemp.clear();
  sstemp.str("");
  sstemp.clear();
  str2;
  str3;
  str4;
  sstemp << oldDriverAfter;
  sstemp >> str2;
  sstemp >> str3;
  sstemp >> str4;
  if(str2 == str3 && str2 == str4){
    oldDriverAfter = "Dec 31 " + str2;
    month = 12;
    day = 31;
    EightOnemyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  if(str3 == str4){
    std::string oldDriverAfter = str2 + " 1" + str4;
    day = 1;
    EightOnemyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  infile.close();
  
  temp = DODmpOptions + "\\10oldDriverAfter.txt";
  infile.open(temp);
  // Determine whether the oldDriverAfter.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        sstemp.clear();
        sstemp.str("");
        sstemp.clear();
        sstemp << inChar;
        sstemp >> inChar;
        oldDriverAfter = inChar;
        if (oldDriverAfter.find("Jan") != std::string::npos || oldDriverAfter.find("jan") != std::string::npos || oldDriverAfter == "1"){
          month = 1;
        }
        if (oldDriverAfter.find("Feb") != std::string::npos || oldDriverAfter.find("feb") != std::string::npos || oldDriverAfter == "2"){
          month = 2;
        }
        if (oldDriverAfter.find("Mar") != std::string::npos || oldDriverAfter.find("mar") != std::string::npos || oldDriverAfter == "3"){
          month = 3;
        }
        if (oldDriverAfter.find("Apr") != std::string::npos || oldDriverAfter.find("apr") != std::string::npos || oldDriverAfter == "4"){
          month = 4;
        }
        if (oldDriverAfter.find("May") != std::string::npos || oldDriverAfter.find("may") != std::string::npos || oldDriverAfter == "5"){
          month = 5;
        }
        if (oldDriverAfter.find("Jun") != std::string::npos || oldDriverAfter.find("jun") != std::string::npos || oldDriverAfter == "6"){
          month = 6;
        }
        if (oldDriverAfter.find("Jul") != std::string::npos || oldDriverAfter.find("jul") != std::string::npos || oldDriverAfter == "7"){
          month = 7;
        }
        if (oldDriverAfter.find("Aug") != std::string::npos || oldDriverAfter.find("aug") != std::string::npos || oldDriverAfter == "8"){
          month = 8;
        }
        if (oldDriverAfter.find("Sep") != std::string::npos || oldDriverAfter.find("sep") != std::string::npos || oldDriverAfter == "9"){
          month = 9;
        }
        if (oldDriverAfter.find("Oct") != std::string::npos || oldDriverAfter.find("oct") != std::string::npos || oldDriverAfter == "10"){
          month = 10;
        }
        if (oldDriverAfter.find("Nov") != std::string::npos || oldDriverAfter.find("nov") != std::string::npos || oldDriverAfter == "11"){
          month = 11;
        }
        if (oldDriverAfter.find("Dec") != std::string::npos || oldDriverAfter.find("dec") != std::string::npos || oldDriverAfter == "12"){
          month = 12;
        }
        sstemp >> inChar;
        std::stringstream sstempDay;
        sstempDay << inChar;
        sstempDay >> day;
        oldDriverAfter = oldDriverAfter + " " + inChar;
        sstemp >> inChar;
        std::stringstream sstempYear;
        sstempYear << inChar;
        sstempYear >> year;
        TenmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
        oldDriverAfter = oldDriverAfter + " " + inChar;
      }
    }
  }
  else{
    // if the file does not exist, make June, 2015 the default
    oldDriverAfter = "Jun 1 2015";
    month = 6;
    day = 1;
    year = 2015;
    TenmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  sstemp.clear();
  sstemp.str("");
  sstemp.clear();
  str2;
  str3;
  str4;
  sstemp << oldDriverAfter;
  sstemp >> str2;
  sstemp >> str3;
  sstemp >> str4;
  if(str2 == str3 && str2 == str4){
    oldDriverAfter = "Dec 31 " + str2;
    month = 12;
    day = 31;
    TenmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  if(str3 == str4){
    std::string oldDriverAfter = str2 + " 1" + str4;
    day = 1;
    TenmyTimeStamp = double(year) * 365.25 + double(month - 1) * 30 + double(day);
  }
  infile.close();
}

// determine the year that any year prior to will have red highlighting for the excluded driver list
void DmpOptions::getASACPI(){  
  std::string inChar;
  std::string temp = DODmpOptions + "\\excludedDrivers.txt";
  std::ifstream infile(temp);
  // Determine whether the excludedDrivers.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream inCharTemp;
        for(int i = 0; i < int(inChar.length()); i++){
          if(inChar.c_str()[i] != ','){
            inCharTemp << inChar.c_str()[i];
          }
        }
        inChar = inCharTemp.str();
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
        excludedNames.push_back(inChar);
        sstemp >> inChar;
        ASACPIsys = inChar;
        if (ASACPIsys.find("Jan") != std::string::npos || ASACPIsys.find("jan") != std::string::npos || ASACPIsys == "1"){
          monthA = 1;
        }
        if (ASACPIsys.find("Feb") != std::string::npos || ASACPIsys.find("feb") != std::string::npos || ASACPIsys == "2"){
          monthA = 2;
        }
        if (ASACPIsys.find("Mar") != std::string::npos || ASACPIsys.find("mar") != std::string::npos || ASACPIsys == "3"){
          monthA = 3;
        }
        if (ASACPIsys.find("Apr") != std::string::npos || ASACPIsys.find("apr") != std::string::npos || ASACPIsys == "4"){
          monthA = 4;
        }
        if (ASACPIsys.find("May") != std::string::npos || ASACPIsys.find("may") != std::string::npos || ASACPIsys == "5"){
          monthA = 5;
        }
        if (ASACPIsys.find("Jun") != std::string::npos || ASACPIsys.find("jun") != std::string::npos || ASACPIsys == "6"){
          monthA = 6;
        }
        if (ASACPIsys.find("Jul") != std::string::npos || ASACPIsys.find("jul") != std::string::npos || ASACPIsys == "7"){
          monthA = 7;
        }
        if (ASACPIsys.find("Aug") != std::string::npos || ASACPIsys.find("aug") != std::string::npos || ASACPIsys == "8"){
          monthA = 8;
        }
        if (ASACPIsys.find("Sep") != std::string::npos || ASACPIsys.find("sep") != std::string::npos || ASACPIsys == "9"){
          monthA = 9;
        }
        if (ASACPIsys.find("Oct") != std::string::npos || ASACPIsys.find("oct") != std::string::npos || ASACPIsys == "10"){
          monthA = 10;
        }
        if (ASACPIsys.find("Nov") != std::string::npos || ASACPIsys.find("nov") != std::string::npos || ASACPIsys == "11"){
          monthA = 11;
        }
        if (ASACPIsys.find("Dec") != std::string::npos || ASACPIsys.find("dec") != std::string::npos || ASACPIsys == "12"){
          monthA = 12;
        }
        sstemp >> inChar;
        std::stringstream sstempDay;
        sstempDay << inChar;
        sstempDay >> dayA;
        ASACPIsys = ASACPIsys + " " + inChar;
        sstemp >> inChar;
        std::stringstream sstempYear;
        sstempYear << inChar;
        sstempYear >> yearA;
        myTimeStampA = double(yearA) * 365.25 + double(monthA - 1) * 30 + double(dayA - 1);
        ASACPIsys = ASACPIsys + " " + inChar;
        std::stringstream sstemp2;
        std::string str2;
        std::string str3;
        std::string str4;
        sstemp2 << ASACPIsys;
        sstemp2 >> str2;
        sstemp2 >> str3;
        sstemp2 >> str4;
        if(str2 == str3 && str2 == str4){
          ASACPIsys = "Dec 31 " + str2;
          monthA = 12;
          dayA = 31;
          myTimeStampA = double(yearA) * 365.25 + double(monthA - 1) * 30 + double(dayA);
        }
        if(str3 == str4){
          std::string ASACPIsys = str2 + " 1" + str4;
          dayA = 1;
          myTimeStampA = double(yearA) * 365.25 + double(monthA - 1) * 30 + double(dayA);
        }
        myTimeStampV.push_back(myTimeStampA);
      }
    }
  }
  else{
    // if the file does not exist, make 2008 the default
    ASACPIsys = "Dec 31 2008";
    monthA = 12;
    dayA = 31;
    yearA = 2008;
    myTimeStampA = double(yearA) * 365.25 + double(monthA - 1) * 30 + double(dayA);
    excludedNames.push_back(" ");
    myTimeStampV.push_back(0.0);
  }
  infile.close();
  
  temp = DODmpOptions + "\\XPED.txt";
  infile.open(temp);
  // Determine whether the excludedDrivers.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      int XPEDTemp = 0;
      for(int i=0; i<int(inChar.length()); i++){
        if(inChar.c_str()[i] != ' '){
          XPEDTemp++;
        }
      }
      if(XPEDTemp){
        XPED.push_back(1);
      }
      else{
        XPED.push_back(0);
      }
    }
  }
  infile.close();
  
  temp = DODmpOptions + "\\VistaED.txt";
  infile.open(temp);
  // Determine whether the excludedDrivers.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      int VistaEDTemp = 0;
      for(int i=0; i<int(inChar.length()); i++){
        if(inChar.c_str()[i] != ' '){
          VistaEDTemp++;
        }
      }
      if(VistaEDTemp){
        VistaED.push_back(1);
      }
      else{
        VistaED.push_back(0);
      }
    }
  }
  infile.close();
  
  temp = DODmpOptions + "\\SevenED.txt";
  infile.open(temp);
  // Determine whether the excludedDrivers.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      int SevenEDTemp = 0;
      for(int i=0; i<int(inChar.length()); i++){
        if(inChar.c_str()[i] != ' '){
          SevenEDTemp++;
        }
      }
      if(SevenEDTemp){
        SevenED.push_back(1);
      }
      else{
        SevenED.push_back(0);
      }
      // cout << "SevenED = " << SevenED[int(SevenED.size())-1] << std::endl;
    }
  }
  infile.close();
  
  temp = DODmpOptions + "\\EightED.txt";
  infile.open(temp);
  // Determine whether the excludedDrivers.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      int EightEDTemp = 0;
      for(int i=0; i<int(inChar.length()); i++){
        if(inChar.c_str()[i] != ' '){
          EightEDTemp++;
        }
      }
      if(EightEDTemp){
        EightED.push_back(1);
      }
      else{
        EightED.push_back(0);
      }
    }
  }
  infile.close();
  
  temp = DODmpOptions + "\\EightOneED.txt";
  infile.open(temp);
  // Determine whether the excludedDrivers.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      int EightOneEDTemp = 0;
      for(int i=0; i<int(inChar.length()); i++){
        if(inChar.c_str()[i] != ' '){
          EightOneEDTemp++;
        }
      }
      if(EightOneEDTemp){
        EightOneED.push_back(1);
      }
      else{
        EightOneED.push_back(0);
      }
      // cout << "EightOneED = " << EightOneED[int(EightOneED.size())-1] << std::endl;
    }
  }
  infile.close();
  
  temp = DODmpOptions + "\\TenED.txt";
  infile.open(temp);
  // Determine whether the excludedDrivers.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      int TenEDTemp = 0;
      for(int i=0; i<int(inChar.length()); i++){
        if(inChar.c_str()[i] != ' '){
          TenEDTemp++;
        }
      }
      if(TenEDTemp){
        TenED.push_back(1);
      }
      else{
        TenED.push_back(0);
      }
      // cout << "TenED = " << TenED[int(TenED.size())-1] << std::endl;
    }
  }
  infile.close();
}

// determine which drivers should have statistics maintained for them
void DmpOptions::getProblemDrivers(){  
  std::string inChar;
  std::string temp = DODmpOptions + "\\problemDrivers.txt";
  std::ifstream infile(temp);
  // Determine whether the oldDriverAfter.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream inCharTemp;
        for(int i = 0; i < int(inChar.length()); i++){
          if(inChar.c_str()[i] != ','){
            inCharTemp << inChar.c_str()[i];
          }
        }
        inChar = inCharTemp.str();
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
        problemNames.push_back(inChar);
      }
    }
  }
  else{
    // if the file does not exist, store a blank line
    problemNames.push_back(" ");
  }
  infile.close();
}

// determine which user kd commands will be implemented
void DmpOptions::getkdCommands(){
  std::string inChar;
  std::string temp = DODmpOptions + "\\onlyUser.txt";
  std::ifstream infile(temp);
  // Determine whether the onlyUser.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> onlyUser;
      }
    }
  }
  else{
    // if the file does not exist, use default kd commands
    onlyUser = 0;
  }
  infile.close();  
  
  inChar;
  temp = DODmpOptions + "\\userFirst.txt";
  infile.open(temp);
  // Determine whether the userFirst.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> userFirst;
      }
    }
  }
  else{
    // if the file does not exist, use default kd commands
    userFirst = 0;
  }
  infile.close();  

  temp = DODmpOptions + "\\kdCommands.txt";
  infile.open(temp);
  // Determine whether the oldDriverAfter.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        kdCommands.push_back(inChar);
      }
    }
  }
  else{
    // if the file does not exist, store a blank line
    kdCommands.push_back("");
  }
  infile.close();
}

// Gets parms
void DmpOptions::getParms(){
  // read in the $spacer1 file
  std::string inPath = DOParms + "$spacer1";
  std::string inChar;
  std::ifstream infile(inPath.c_str());
  inChar = "ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``";
  if(infile.good())
  {
    getline(infile,inChar);
  }
  infile.close();
  spacer1 = inChar;

  // read in the $_code_box_begin file
  inPath = DOParms + "$_code_box_begin";
  infile.open(inPath.c_str());
  inChar = "[CODE]";
  if(infile.good())
  {
    getline(infile,inChar);
  }
  infile.close();
  codeBoxBegin = inChar;
  
  // read in the $_code_box_end file
  inPath = DOParms + "$_code_box_end";
  infile.open(inPath.c_str());
  inChar = "[/CODE]";
  if(infile.good())
  {
    getline(infile,inChar);
  }
  infile.close();
  codeBoxEnd = inChar;

  //// read in the $_dbugdir file
  //inPath = DOParms + "$_dbugdir";
  //infile.open(inPath.c_str());
 // inChar = "";
 // if(infile.good())
 // {
  //  getline(infile,inChar);
 // }
  //infile.close();
  //stringstream sstemp;
  //for(int i = 0; i < int(inChar.length()); i++){
  //  if(inChar.c_str()[i] != '\"'){
  //    sstemp << inChar.c_str()[i];
  //  }
  //}
  //dbugDir = sstemp.str();

  if(!dbugDir.empty() && dbugDir[0] != '\"' && dbugDir[dbugDir.length()-1] != '\"'){
    dbugDir = "\"" + dbugDir + "\"";
  }

  // read in the $_del1 file
  inPath = DOParms + "$_del1";
  infile.open(inPath.c_str());
  if(infile.good())
  {
    getline(infile,inChar);
  }
  infile.close();
  del1 = inChar;

  // read in the $_driver_list_hex_timestamp file
  inPath = DOParms + "$_driver_list_hex_timestamp";
  infile.open(inPath.c_str());
  if(infile.good())
  {
    getline(infile,inChar);
  }
  infile.close();
  driverListHexTimestamp = inChar;

  // read in the $_driver_update_header1 file
  inPath = DOParms + "$_driver_update_header1";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    driverUpdateHeader1.push_back(inChar);
  }
  infile.close();

  // read in the $_dumpfilecount file
  inPath = DOParms + "$_dumpfilecount";
  infile.open(inPath.c_str());
  if(infile.good()){
    getline(infile,inChar);
  }
  else{
    inChar = "0";
  }
  infile.close();
  dumpFileCount = inChar;

  // read in the $_dvrref_table_98 file
  inPath = DOParms + "$_dvrref_table_98";
  infile.open(inPath.c_str());
  if(infile.good())
  {
    getline(infile,inChar);
  }
  infile.close();
  dvrrefTable98 = inChar;

  // read in the $_dvrref_updatetime file
  inPath = DOParms + "$_dvrref_updatetime";
  infile.open(inPath.c_str());
  if(infile.good())
  {
    getline(infile,inChar);
  }
  infile.close();
  dvrrefUpdatetime = inChar;

  // read in the $_footer1 file
  inPath = DOParms + "$_footer1";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    footer1.push_back(inChar);
  }
  infile.close();

  // read in the $_header1 file
  inPath = DOParms + "$_header1";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    header1.push_back(inChar);
  }
  infile.close();

  // read in the $_kd_display file
  inPath = DOParms + "$_kd_display";
  infile.open(inPath.c_str());
  inChar = "";
  if(infile.good()){
    getline(infile,inChar);
  }
  infile.close();
  kdDisplay = inChar;


  // read in the $_kernel_dir1 file
  inPath = DOParms + "$_kernel_dir1";
  infile.open(inPath.c_str());
  inChar = "";
  if(infile.good()){
    getline(infile,inChar);
  }
  infile.close();
  kernelDir1 = inChar;

  // read in the $_OS_ver1 file
  inPath = DOParms + "$_OS_ver1";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    OSVer1.push_back(inChar);
  }
  infile.close();

  // read in the $_parm2 file
  inPath = DOParms + "$_parm2";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    parm2.push_back(inChar);
  }
  infile.close();

  // read in the $_parms_dir1 file
  inPath = DOParms + "$_parms_dir1";
  infile.open(inPath.c_str());
  inChar = "";
  if(infile.good()){
    getline(infile,inChar);
  }
  infile.close();
  parmsDir1 = inChar;

  // read in the $_sig1 file
  inPath = DOParms + "$_sig1";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    sig1.push_back(inChar);
  }
  infile.close();

  // read in the $_symbols file
  inPath = DOParms + "$_symbols";
  infile.open(inPath.c_str());
  inChar = "";
  if(infile.good()){
    getline(infile,inChar);
  }
  infile.close();
  symbols = inChar;

  // read in the $_sys_uptime file
  inPath = DOParms + "$_sys_uptime";
  infile.open(inPath.c_str());
  inChar = "";
  if(infile.good()){
    getline(infile,inChar);
  }
  infile.close();
  sysUptime = inChar;

  // read in the $_years file
  inPath = DOParms + "$_years";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    years.push_back(inChar);
  }
  infile.close();

  // read in the $_years_Vista file
  inPath = DOParms + "$_years_Vista";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    yearsVista.push_back(inChar);
  }
  infile.close();

  // read in the $_years_Windows7 file
  inPath = DOParms + "$_years_Windows7";
  infile.open(inPath.c_str());
  while(!infile.eof() && infile.good()){
    getline(infile,inChar);
    yearsWindows7.push_back(inChar);
  }
  infile.close();

  // read in the $un95 file
  inPath = DOParms + "un95";
  infile.open(inPath.c_str());
  inChar = "";
  if(infile.good()){
    getline(infile,inChar);
  }
  infile.close();
  un95 = inChar;
}

void DmpOptions::getOSParms(){
  // read in the $_Microsoft_OS_Drivers file
  std::string inPath = DOParms + "$_Microsoft_OS_Drivers";
  std::ifstream infile;
  infile.open(inPath.c_str());
  std::string inChar;
  while(infile.good() && !infile.eof()){
    getline(infile,inChar);
    // MicrosoftOSDrivers.push_back(inChar);
  }
  infile.close();
  MicrosoftOSDrivers.push_back("thisVariableIsDeprecated");
}
