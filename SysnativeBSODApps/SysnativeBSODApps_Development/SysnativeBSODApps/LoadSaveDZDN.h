#ifndef LOADSAVEDZDN_H
#define LOADSAVEDZDN_H
#include <string>
#include <vector>
class LoadSaveDZDN
{
public:
  LoadSaveDZDN();
  virtual ~LoadSaveDZDN();
  std::vector<std::string> LoadDefaultDZDN();
  std::vector<std::string> LoadDZDN();
  void SaveDZDN(std::vector<std::string> settings);
  void ConvertZDNToDZDN();
private:
};

#endif