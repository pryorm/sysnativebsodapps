#ifndef DMPOPTIONS_HPP
#define DMPOPTIONS_HPP
//-----------------------------------------------------------------------------
//  File:         DmpOptions c++ class
//  Description:  Obtain options for running .dmps and output formatting
//  Project:      dmpAnalysis
//  Author:       Mikael Pryor
//  Date:         July  28, 2012
//  Changes:      July  29, 2012
//  Changes Made: Created the constructor and destructor.
//    07/29/2012: Added getSymbols function and symbolsString    
//    07/31/2012: Added options for output files and importantInfo.txt
//                Added options for opening all files on exit
//                Added options for symbols to run locally and get online symbols
//                   prior to using the local symbols
//                Added options for template formatting
//                Fixed problem with symbols.txt having first two lines
//                   read when only the first line was intended to be read
//-----------------------------------------------------------------------------
#include <string>
#include <vector>

class DmpOptions {
public:
  // Constructor, Destructor, and Functions
  DmpOptions(std::string userProfile, std::string parms);
  virtual ~DmpOptions();
  void getDmpOptions();
  void getTemplate();
  void getOutputOptions();
  void getOpenOnExit();
  void getUrlQuestion();
  void getNonParsed();
  void getAllFilesDmps();
  void getParms();
  void getOSParms();
  void getAddSpaces();
  void getOldDriverAfter();
  void getASACPI();
  void getProblemDrivers();
  void getkdCommands();

  // Variables
  std::string symbolsString;
  int symbolsInt;
  std::vector<std::string> fullTemplate;
  std::string driverInSite;
  std::string driverOutSite;
  int templateExists;
  int loadingDumpFile;
  int kernelVersion;
  int missingServicePack;
  int builtBy;
  int debugSessionTime;
  int systemUptime;
  int bugCheck;
  int unableToVerify;
  int symbolsNotLoaded;
  int probablyCausedBy;
  int defaultBucketID;
  int verifier;
  int bugcheckStr;
  int bugCheckAnalysis;
  int processName;
  int failureBucketID;
  int bugcheckCode;
  int errorCode;
  int diskHardwareError;
  int arguments;
  int CPUID;
  int maxSpeed;
  int currentSpeed;
  int overclock;
  int BIOSVersion;
  int BIOSReleaseDate;
  int systemManufacturer;
  int systemProductName;
  int outputDmps;
  int outputFullDrivers;
  int outputThirdPartyDriversName;
  int outputThirdPartyDriversDate;
  int outputImportantInfo;
  int outputStack;
  int outputMissingFromDRT;
  int outputUnloadedDrivers;
  int outputSMBIOS;
  int outputTemplate;
  int outputNinetyNine;
  int outputNinetyEight;
  int outputEightyEight;
  int outputNinetySeven;
  int outputNinetyFive;
  int openOnExit;
  int inHtml;
  int urlQuestion;
  int nonParsed;
  int addSpaces;
  int turnOffBBCode;
  int noBBCodeCodeBoxes;
  int oldDriversRed;
  int allFilesDmps;
  std::string oldDriverAfter;
  int month;
  int day;
  int year;
  double XPmyTimeStamp;
  double VistamyTimeStamp;
  double SevenmyTimeStamp;
  double EightmyTimeStamp;
  double EightOnemyTimeStamp;
  double TenmyTimeStamp;
  double myTimeStamp;
  std::string ASACPIsys;
  int monthA;
  int dayA;
  int yearA;
  double myTimeStampA;
  std::vector<double> myTimeStampV;
  std::vector<std::string> excludedNames;
  std::vector<std::string> problemNames;
  std::vector<int> XPED;
  std::vector<int> VistaED;
  std::vector<int> SevenED;
  std::vector<int> EightED;
  std::vector<int> EightOneED;
  std::vector<int> TenED;
  std::string spacer1;
  // Parms
  std::string codeBoxBegin;
  std::string codeBoxEnd;
  std::string dbugDir;
  std::string del1;
  std::string driverListHexTimestamp;  
  std::vector<std::string> driverUpdateHeader1;
  std::string dumpFileCount;
  std::string dvrrefTable98;
  std::string dvrrefUpdatetime;
  std::vector<std::string> footer1;
  std::vector<std::string> header1;
  std::string kdDisplay;
  std::string kernelDir1;
  std::vector<std::string> MicrosoftOSDrivers;  // deprecated
  std::vector<std::string> OSVer1;
  std::vector<std::string> parm2;
  std::string parmsDir1;
  std::vector<std::string> sig1;
  std::string symbols;            // deprecated
  std::string sysUptime;          // deprecated
  std::vector<std::string> years;
  std::vector<std::string> yearsVista;
  std::vector<std::string> yearsWindows7;
  std::string un95;
  std::vector<std::string> kdCommands;
  int onlyUser;
  int userFirst;
protected:

private:
  // Private Functions
  void getSymbols();
  void getDriverList();

  // Private Variables
  std::string DOUserProfile;
  std::string DOParms;
  std::string DODmpOptions;
};

#endif
