#pragma once

#include <string>
#include <vector>

class StringUtil
{
    public:
        static bool contains(const std::string& str, const std::string& substr);

        static bool startsWith(const std::string& str, const std::string& substr);

        static bool endsWith(const std::string& str, const std::string& substr);

        static std::string replace(const std::string& str, const std::string& oldSubstr, const std::string& newSubstr);

        static std::string toLower(const std::string& str);

        static std::string toUpper(const std::string& str);

        static std::string trim(const std::string& str, const char c);

        static std::string trim(const std::string& str);

        static std::vector<std::string> split(const std::string& str, const char c);

        static std::vector<std::string> split(const std::string& str, const std::string& substr);

        static bool unsignedCompare(const std::string& str1, const std::string& str2);
};