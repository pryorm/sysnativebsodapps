#include "FindPaths.h"

#include "LogUtil.h"
#include "SystemStandardConversionUtil.h"

#include <Windows.h> // for HWND
#include <string>
#include <fstream>
#include <sstream>
#include <time.h>

#include "debug.h"


//#include <vld.h>

using namespace SysnativeBSODApps;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;

System::Void FindPaths::FindPaths_Load(System::Object^  sender, System::EventArgs^  e)
{
  closeForm = 0;
  if(onTop)
  {
    this->TopMost = true;
  }
  else
  {
    this->TopMost = false;
  }
  progressBar1->Visible = false;
  // Enlist all the network drives that are available on the computer.
  comboBox1->Items->Clear();
  comboBox1->Text = L"C:\\";
  cli::array<String^>^ p_logicalDrives = IO::Directory::GetLogicalDrives();
  int numDrives = p_logicalDrives->Length;

  for (int i=0; i< numDrives; i++)
  {
    comboBox1->Items->Add(p_logicalDrives[i]);
  }
  textBox1->Text = L"kd.exe";
  foundPaths = 0;
  button1->Enabled = true;
  button3->Enabled = false;
  button4->Enabled = true;

  // Reset the variable for percentage tracking.
  highestPercentageReached = 0;
  maxVal = 0;
}

System::Void FindPaths::FindPaths_Shown(System::Object^  sender, System::EventArgs^  e)
{
  this->TopMost = false;
}

System::Void FindPaths::updateListBox1()
{
    if (listBox1 != nullptr && listBox1->Items != nullptr && mKdFiles != nullptr && mKdFiles->Length > 0)
    {
        listBox1->Items->AddRange(mKdFiles);
        mKdFiles = gcnew cli::array<String^>(0);
    }
}

System::Void FindPaths::SafeInvoke(Control^ control, System::Action^ action)
{
    if (control != nullptr && !control->IsDisposed && control->InvokeRequired)
    {
        try
        {
            control->Invoke(action);
        }
        catch (System::ObjectDisposedException ^ excep)
        {
            LogUtil::logErrorAsync(excep->Message);
        }
    }
    else if (control != nullptr && !control->IsDisposed)
    {
        action();
    }
}

void FindPaths::DirSearch(String^ sDir, BackgroundWorker^ worker)
{
  try
  {
    // Find the subfolders in the folder that is passed in.
	cli::array<String^>^ d = IO::Directory::GetDirectories(sDir);
    int numDirs = d->Length;
    // Find all the files in the subfolder.
	cli::array<String^>^ f = IO::Directory::GetFiles(sDir,textBox1->Text,IO::SearchOption::TopDirectoryOnly);
    int numFiles = f->Length;

    for (int j=0; j < numFiles; j++)
    {
      if ( worker->CancellationPending )
      {
        return;
      }
      Array::Resize(mKdFiles, mKdFiles->Length + 1);
      mKdFiles[mKdFiles->Length - 1] = f[j];
    }
       
    for (int i=0; i < numDirs; i++)
    {
      maxVal++;
      int maxVal2;
      if(maxVal < 2000)
      {
        maxVal2 = maxVal + 4000;
      }
      else{
        maxVal2 = maxVal + 4000;
      }
      // Report progress as a percentage of the total task. 
      int percentComplete = (int)((float)maxVal / (float)(maxVal2) * 100);
      if ( percentComplete > highestPercentageReached )
      {
        highestPercentageReached = percentComplete;
        worker->ReportProgress( percentComplete );
      }
      if ( worker->CancellationPending )
      {
        return;
      }
      DirSearch(d[i], worker);
    }
    this->SafeInvoke(this, gcnew System::Action(this, &FindPaths::updateListBox1));
  }
  catch (System::Exception^ e)
  {
    System::Console::WriteLine(e->Message);
  }
}

System::Void FindPaths::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
  progressBar1->Visible = true;
  button1->Enabled = false;
  button3->Enabled = true;
  // Clear the list box.
  listBox1->Items->Clear();
  // You do not have the option to change the values of the files to be searched
  // until the search is completed. Therefore, the following value is false.
  textBox1->Enabled = false;
  comboBox1->Enabled = false;

  button1->Text = L"Searching...";
  this->Cursor = Cursors::WaitCursor;
  Application::DoEvents();

  // Start the asynchronous operation.
  mDirectoryToSearch = comboBox1->Text;
  backgroundWorker1->RunWorkerAsync();
  button1->Text = L"Search";
  this->Cursor = Cursors::Default;

  // After the search is completed, the search criteria is enabled 
  //and you can add other search criteria to search the files recursively.
  textBox1->Enabled = true;
  comboBox1->Enabled = true;
}

System::Void FindPaths::listBox1_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
{
  bool seenThis = false;
  for(int i=0; i<comboBox2->Items->Count; i++)
  {
    if(comboBox2->Items[i]->ToString() == comboBox2->Text)
    {
      seenThis = true;
    }
  }
  if(!seenThis && comboBox2->Text->Length > 5)
  {
    comboBox2->Items->Add(comboBox2->Text);
    comboBox2->SelectedIndex = comboBox2->Items->Count - 1;
  }
  for(int i=0; i<listBox1->Items->Count; i++)
  {
    if(listBox1->GetSelected(i) && listBox1->Items[i]->ToString() != L"")
    {
      comboBox2->Items->Add("\"" + listBox1->Items[i]->ToString() + "\"");
      comboBox2->SelectedIndex = comboBox2->FindStringExact("\"" + listBox1->Items[i]->ToString() + "\"");
      foundPaths++;
    }
  }
}

System::Void FindPaths::backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
{
  closeForm = 0;
  // Get the BackgroundWorker that raised this event.
  BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
  // Call the recursive search method.
  mKdFiles = gcnew cli::array<String^>(0);
  DirSearch(mDirectoryToSearch, worker);
  worker->ReportProgress( 99 );
  time_t t1, t2;
  t1 = clock();
  t2 = clock();
  while(float(t2) - float(t1) < 500)
  {
    if(worker->CancellationPending)
    {
      return;
    }
    t2 = clock();
    this->SafeInvoke(this, gcnew System::Action(this, &FindPaths::Refresh));
  }
}

System::Void FindPaths::backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
{
  this->progressBar1->Value = e->ProgressPercentage;
}

System::Void FindPaths::backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
{  
  // First, handle the case where an exception was thrown. 
  if ( e->Error != nullptr )
  {
      MessageBox::Show( e->Error->Message );
  }
  else 
  if ( e->Cancelled )
  {
      // Next, handle the case where the user cancelled  
      // the operation. 
      // Note that due to a race condition in  
      // the DoWork event handler, the Cancelled 
      // flag may not have been set, even though 
      // CancelAsync was called.
  }
  else
  {
      // Finally, handle the case where the operation  
      // succeeded.
  }
  button1->Enabled = true;
  button3->Enabled = false;
  progressBar1->Visible = false;

  // Reset the variable for percentage tracking.
  highestPercentageReached = 0;
  maxVal = 0;

  if(closeForm)
  {
    Close();
  }

}
// OK / Save
System::Void FindPaths::button4_Click(System::Object^  sender, System::EventArgs^  e)
{
  bool seenThis = false;
  for(int i=0; i<comboBox2->Items->Count; i++)
  {
    if(comboBox2->Items[i]->ToString() == comboBox2->Text)
    {
      seenThis = true;
    }
  }
  if(!seenThis && comboBox2->Text->Length > 5)
  {
    comboBox2->Items->Add(comboBox2->Text);
    comboBox2->SelectedIndex = comboBox2->Items->Count - 1;
  }
  button3_Click(sender, e);
  int selectedIndex = comboBox2->SelectedIndex;
  m_SAKdPaths = gcnew cli::array<String^>(0);
  m_NAKdPaths = gcnew cli::array<int>(0);
  for(int i=0; i<comboBox2->Items->Count; i++)
  {
    std::string s = SystemStandardConversionUtil::convertSystemStringToStdString(comboBox2->Items[i]->ToString());
	std::stringstream ss;
    for(int j = 0; j < int(s.length()); j++)
    {
      if(s.c_str()[j] != '\"')
      {
        ss << s.c_str()[j];
      }
    }
    s = ss.str();
    Array::Resize(m_SAKdPaths, m_SAKdPaths->Length + 1);
    Array::Resize(m_NAKdPaths, m_NAKdPaths->Length + 1);
    m_SAKdPaths[i] = gcnew String(s.c_str());
    m_NAKdPaths[i] = 0;
    int tf = 0;
    if(i == selectedIndex)
    {
      m_NAKdPaths[i] = 1;
      tf = 1;
    }
    //outfile << "\"" << s << "\"" << endl;
  }
  //outfile.close();
  if(comboBox2->Items->Count == 0)
  {
    String^ str2 = L"Make sure to click on a path to add it to the kd.exe path list.\n\n" + 
      "No paths were added for the kd.exe path. Are you sure you want to close the auto-finding tool?";
    if(MessageBox::Show(str2,L"Close Auto-Finding Tool?",MessageBoxButtons::YesNo, MessageBoxIcon::Question, 
        MessageBoxDefaultButton::Button1) == System::Windows::Forms::DialogResult::Yes)
    {
      closeForm = 1;
      if(!backgroundWorker1->IsBusy)
      {
        Close();
      }
    }
  }
  else
  {
    closeForm = 1;
    if(!backgroundWorker1->IsBusy)
    {
      Close();
    }
  }
  m_bFinished = true;
}

System::Void FindPaths::button3_Click(System::Object^  sender, System::EventArgs^  e)
{
  // Cancel the asynchronous operation. 
  this->backgroundWorker1->CancelAsync();

  button3->Enabled = false;
  button4->Enabled = true;
}

System::Void FindPaths::button10_Click(System::Object^  sender, System::EventArgs^  e)
{
  comboBox2->Items->Clear();
  comboBox2->Text = L"";
}

System::Void FindPaths::button2_Click(System::Object^  sender, System::EventArgs^  e)
{
  closeForm = 1;
  button3_Click(sender, e);
  while(backgroundWorker1->IsBusy)
  {
    Sleep(105);
  }
  m_bFinished = true;
  Close();
}