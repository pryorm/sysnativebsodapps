#include "SetDirectorySysnativeResults.h"

#include "ConfigurationUtil.h"
#include "FileSystemUtil.h"
#include "SystemStandardConversionUtil.h"
#include <string>

#include "debug.h"


//#include <vld.h>

using namespace SysnativeBSODApps;

// Set the current path to the saved path
void SetDirectorySysnativeResults::currentPath(System::String^ curPath)
{
  textBox1->Text = curPath;
  sysnativeResultsS = curPath;
}

// Click Browse to find a path
System::Void SetDirectorySysnativeResults::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
  std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP"));
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  folderBrowserDialog1->ShowDialog();
  textBox1->Text = folderBrowserDialog1->SelectedPath + "\\SysnativeResults";
}

// click OK to save path and exit
System::Void SetDirectorySysnativeResults::button2_Click(System::Object^  sender, System::EventArgs^  e)
{
  std::string path = SystemStandardConversionUtil::convertSystemStringToStdString(textBox1->Text);
  size_t find1 = path.find("\\");
  int j1 = 0;
  while(find1 != std::string::npos)
  {
    j1 = int(find1);
    find1 = path.find("\\",j1+1,1);
  }
  std::string dirName = path.substr(j1+1,path.length()-j1-1);
  FileSystemUtil::createDirectoryWithBackup(folderBrowserDialog1->SelectedPath, gcnew System::String(dirName.c_str()));
  sysnativeResultsS = textBox1->Text;
  Close();
}

// click Cancel to exit
System::Void SetDirectorySysnativeResults::button3_Click(System::Object^  sender, System::EventArgs^  e)
{
  Close();
}