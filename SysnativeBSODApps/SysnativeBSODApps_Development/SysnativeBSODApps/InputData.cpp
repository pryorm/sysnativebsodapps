#include "InputData.hpp"

#include "ConfigurationUtil.h"
#include "ConsoleInfo.hpp"
#include "DirectoryStructure.hpp"
#include "DmpList.h"
#include "DrtBuilder.h"
#include "FileSystemUtil.h"
#include "LogUtil.h"
#include "StringUtil.h"
#include "SystemStandardConversionUtil.h"

#ifdef _MSC_VER
#include <vcruntime.h>
#endif
#include <msclr/marshal.h>
#include <msclr/marshal_cppstd.h>

#include <string>
//#include <vld.h>
#include <stdlib.h>
#include <tchar.h>      //For _T character
#include <UrlMon.h>      //For URL Download function
#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <conio.h>
#include <time.h>
#include "wininet.h" // for clearing URL cache DeleteUrlCacheEntry
#include "progress.h"

#include "debug.h"
#include <thread>

#pragma comment(lib, "wininet.lib") // for clearing URL cache DeleteUrlCacheEntry

int countKD = 0;
int countUser = 0;
std::vector<std::string> alreadySeenV;
std::vector<std::string> corruptedSeenV;
std::vector<PROCESS_INFORMATION> piV;

// Global Critical section
volatile int myLock = 0;

namespace
{
	// Declare the critical section
	CRITICAL_SECTION cs;
	void downloadAndParse(DrtBuilder& drtBuilder, int page, std::string tempDir) {
		std::string tempPath = tempDir + "\\dump" + std::to_string(page) + ".html";
		WCHAR* Lparam = new WCHAR[tempPath.size() + 1];
		Lparam[tempPath.size()] = 0;
		std::wstring wTempPath(tempPath.begin(), tempPath.end());
		wcsncpy_s(Lparam, tempPath.size() + 1, wTempPath.c_str(), tempPath.size());
		std::wstring wHttpUrl = L"https://www.sysnative.com/forums/drivers/page-" + std::to_wstring(page);
		DeleteUrlCacheEntryW(wHttpUrl.c_str());
		HRESULT hr = URLDownloadToFileW(NULL, wHttpUrl.c_str(), Lparam, 0, NULL);
		if (SUCCEEDED(hr)) {
			EnterCriticalSection(&cs);
			drtBuilder.parseDriverInfo(tempPath);
			LeaveCriticalSection(&cs);
		}
		delete[] Lparam;
	}

	// Initialize the critical section
	void InitCriticalSection() {
		InitializeCriticalSection(&cs);
	}

	// Delete the critical section
	void DelCriticalSection() {
		DeleteCriticalSection(&cs);
	}
}

public ref class defaultKD
{
public:
   static int i;
   static int IDSymbolsInt;
   static System::String^ IDDbugDirS;
   static System::String^ commandS;
   static System::String^ tempStringS;
   static System::String^ IDSymbolsStringS;
   static System::String^ dmpList2S;
   static System::String^ outPathS;
   static void DoWork(){

     std::string IDDbugDir = SystemStandardConversionUtil::convertSystemStringToStdString(IDDbugDirS);
     std::string command = SystemStandardConversionUtil::convertSystemStringToStdString(commandS);
     std::string dmpList2 = SystemStandardConversionUtil::convertSystemStringToStdString(dmpList2S);
     std::string IDSymbolsString = SystemStandardConversionUtil::convertSystemStringToStdString(IDSymbolsStringS);
     std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
     std::string outPath = SystemStandardConversionUtil::convertSystemStringToStdString(outPathS);
     
     std::string kd = IDDbugDir + " -c \"" + command + "\" -y \"" + 
       IDSymbolsString + "\"" + " -z " + "\"" + dmpList2 + "\" -logo " + outPath;
     
     STARTUPINFO si;
     PROCESS_INFORMATION pi;
     ZeroMemory( &si, sizeof(si) );
     si.cb = sizeof(si);
     ZeroMemory( &pi, sizeof(pi) );
     
	 std::wstring temp;
     // convert the command std::string to std::wstring format
     temp.assign(kd.begin(),kd.end());

     // Start the child process.   

     if( !CreateProcess( NULL,   // No module name (use command line)
         const_cast<LPWSTR>(temp.c_str()),        // Command line
         NULL,                                    // Process handle not inheritable
         NULL,                                    // Thread handle not inheritable
         FALSE,                                   // Set handle inheritance to FALSE
         CREATE_NO_WINDOW,                        // No creation flags
         NULL,                                    // Use parent's environment block
         NULL,                                    // Use parent's starting directory 
         &si,                                     // Pointer to STARTUPINFO structure
         &pi )                                    // Pointer to PROCESS_INFORMATION structure
     )
     {
     }
     piV.push_back(pi);

     myLock = 0;
     
     // Wait until child process exits.
     WaitForSingleObject( pi.hProcess, INFINITE );

     // Close process and thread handles. 
     CloseHandle( pi.hProcess );
     CloseHandle( pi.hThread );

	 std::ifstream infile(outPath);
     while(infile.good() && !infile.eof())
     {
       std::string inString;
       getline(infile,inString);       
       size_t foundCorrupt1;
       size_t foundCorrupt2;
       foundCorrupt1 = inString.find("module list might be corrupt");
       foundCorrupt2 = inString.find("corrupt data");
        std::string IDSymbolsString3 = IDSymbolsString;
        std::transform(IDSymbolsString3.begin(), IDSymbolsString3.end(), 
          IDSymbolsString3.begin(), ::tolower);
       if(IDSymbolsInt == 2 && (inString.find("WARNING: Unable to verify") != std::string::npos ||
         inString.find("Symbol file could not be found") != std::string::npos ||
         inString.find("Kernel symbols are WRONG") != std::string::npos))
       {
         int alreadySeen = 0;
         for(size_t i = 0; i < alreadySeenV.size(); i++)
         {
           if(inString == alreadySeenV[i] && !(inString.find("Kernel symbols are WRONG") != std::string::npos))
           {
             alreadySeen++;
           }
         }
         if(!alreadySeen && !(IDSymbolsString3.find("srv*") != std::string::npos))
         {
           infile.close();
           alreadySeenV.push_back(inString);
           std::string IDSymbolsString2 = "srv*" + IDSymbolsString + "*https://msdl.microsoft.com/download/symbols";
           kd = IDDbugDir + " -c \"" + command + "\" -y \"" + 
             IDSymbolsString2 + "\"" + " -z " + "\"" + dmpList2 + "\" -logo " + outPath;
           // create a std::wstring for the input to the console object
		   std::wstring temp;
           // convert the command std::string to std::wstring format
           temp.assign(kd.begin(),kd.end());

           // Start the child process.   

           if( !CreateProcess( NULL,   // No module name (use command line)
               const_cast<LPWSTR>(temp.c_str()),        // Command line
               NULL,                                    // Process handle not inheritable
               NULL,                                    // Thread handle not inheritable
               FALSE,                                   // Set handle inheritance to FALSE
               CREATE_NO_WINDOW,                        // No creation flags
               NULL,                                    // Use parent's environment block
               NULL,                                    // Use parent's starting directory 
               &si,                                     // Pointer to STARTUPINFO structure
               &pi )                                    // Pointer to PROCESS_INFORMATION structure
           )
           {
           }
           piV.push_back(pi);
     
           // Wait until child process exits.
           WaitForSingleObject( pi.hProcess, INFINITE );

           // Close process and thread handles. 
           CloseHandle( pi.hProcess );
           CloseHandle( pi.hThread );
         
           std::ofstream outError;
           outError.open(tempDir + "\\outError.txt", std::ios::app);
           outError << "Using online symbols...     " << std::endl;
           outError.close();
         }
       }
       else if(foundCorrupt1 != std::string::npos || foundCorrupt2 != std::string::npos)
       {
         int seenThis = 0;
         for(size_t i = 0; i<corruptedSeenV.size(); i++)
         {
           if(corruptedSeenV[i] == dmpList2)
           {
             seenThis++;
           }
         }
         if(!seenThis)
         {
           corruptedSeenV.push_back(dmpList2);           
           std::ofstream outError;
           outError.open(tempDir + "\\outError.txt", std::ios::app);
           outError << dmpList2 << " may be corrupted..." << std::endl;
           outError.close();
         }
       }
       else if(!(IDDbugDir.find("http") != std::string::npos) && 
         inString.find("Kernel symbols are WRONG") != std::string::npos && 
         !(IDSymbolsString3.find("srv*") != std::string::npos))
       {
         infile.close();
         std::string IDSymbolsString2 = "srv*" + IDSymbolsString + "*https://msdl.microsoft.com/download/symbols";
         kd = IDDbugDir + " -c \"" + command + "\" -y \"" + 
           IDSymbolsString2 + "\"" + " -z " + "\"" + dmpList2 + "\" -logo " + outPath;
         // create a std::wstring for the input to the console object
         std::wstring temp;
         // convert the command std::string to std::wstring format
         temp.assign(kd.begin(),kd.end());         

         // Start the child process.   

         if( !CreateProcess( NULL,   // No module name (use command line)
             const_cast<LPWSTR>(temp.c_str()),        // Command line
             NULL,                                    // Process handle not inheritable
             NULL,                                    // Thread handle not inheritable
             FALSE,                                   // Set handle inheritance to FALSE
             CREATE_NO_WINDOW,                        // No creation flags
             NULL,                                    // Use parent's environment block
             NULL,                                    // Use parent's starting directory 
             &si,                                     // Pointer to STARTUPINFO structure
             &pi )                                    // Pointer to PROCESS_INFORMATION structure
         )
         {
         }
         piV.push_back(pi);
     
         // Wait until child process exits.
         WaitForSingleObject( pi.hProcess, INFINITE );

         // Close process and thread handles. 
         CloseHandle( pi.hProcess );
         CloseHandle( pi.hThread );
         
         std::ofstream outError;
         outError.open(tempDir + "\\outError.txt", std::ios::app);
         outError << "Using online symbols...     " << std::endl;
         outError.close();
       }

     }
     infile.close();
     if(outPath.find("\\dmps\\") != std::string::npos)
     {
       countKD++;
       std::ofstream outfile;
       outfile.open(tempDir + "\\dmps\\maxkdDmp.txt", std::ios::app);
       outfile << outPath << std::endl;
       outfile.close();
     }
     if(outPath.find("\\kdc\\") != std::string::npos)
     {
       countUser++;
       std::ofstream outfile;
       outfile.open(tempDir + "\\dmps\\maxUserDmp.txt", std::ios::app);
       outfile << outPath << std::endl;
       outfile.close();
     }

   }
};

// InputData constructor obtains dmpOptions info via its input.
// The constructor also creates a structure and compare object for directory structure information and
//    driver name comparisons with the DRT database.
InputData::InputData(std::string symbolsString, std::string symbolsString2, int symbolsInt, std::string driverInSite, 
           std::string driverOutSite, std::string userProfile, int dmpsExist, int inLoadingDumpFile, 
           int inKernelVersion, int inBuiltBy, int inDebugSessionTime, 
           int inSystemUptime, int inBugCheck, 
           int inUnableToVerify, int inSymbolsNotLoaded, int inProbablyCausedBy, 
           int inDefaultBucketID, int inBugcheckStr, int inBugCheckAnalysis, 
           int inProcessName, int inFailureBucketID, int inBugcheckCode, 
           int inArguments, int inCPUID, int inMaxSpeed, int inCurrentSpeed, 
           int inBIOSVersion, int inBIOSReleaseDate, int inSystemManufacturer, 
           int inSystemProductName, std::string inDbugDir, int inAddSpaces,
           int inTurnOffBBCode, int inErrorCode, int inDiskHardwareError, int inVerifier,
           int inOpenOnExit, int inNoBBCodeCodeBoxes, int inAllFilesDmps,
           int inInHtml, int inOverclock, int inNonParsed, std::vector<std::string> inkdCommands,
           int inOnlyUser, int inMissingServicePack, std::string tempString_s){
  IDSymbolsString = symbolsString;
  IDSymbolsString2 = symbolsString2;
  IDSymbolsInt = symbolsInt;
  IDDriverInSite = driverInSite;
  IDDriverOutSite = driverOutSite;
  IDUserProfile = userProfile;
  IDDmpsExist = dmpsExist;
  compare = new StringComparison(tempString_s);
  IDLoadingDumpFile = inLoadingDumpFile;
  IDKernelVersion = inKernelVersion;
  IDMissingServicePack = inMissingServicePack;
  IDBuiltBy = inBuiltBy;
  IDDebugSessionTime = inDebugSessionTime;
  IDSystemUptime = inSystemUptime;
  IDBugCheck = inBugCheck;
  IDUnableToVerify = inUnableToVerify;
  IDSymbolsNotLoaded = inSymbolsNotLoaded;
  IDProbablyCausedBy = inProbablyCausedBy;
  IDDefaultBucketID = inDefaultBucketID;
  IDVerifier = inVerifier;
  IDBugcheckStr = inBugcheckStr;
  IDBugCheckAnalysis = inBugCheckAnalysis;
  IDProcessName = inProcessName;
  IDFailureBucketID = inFailureBucketID;
  IDBugcheckCode = inBugcheckCode;
  IDErrorCode = inErrorCode;
  IDDiskHardwareError = inDiskHardwareError;
  IDArguments = inArguments;
  IDCPUID = inCPUID;
  IDMaxSpeed = inMaxSpeed;
  IDCurrentSpeed = inCurrentSpeed;
  IDOverclock = inOverclock;
  IDBIOSVersion = inBIOSVersion;
  IDBIOSReleaseDate = inBIOSReleaseDate;
  IDSystemManufacturer = inSystemManufacturer;
  IDSystemProductName = inSystemProductName;
  IDDbugDir = inDbugDir;
  IDAddSpaces = inAddSpaces;
  IDTurnOffBBCode = inTurnOffBBCode;
  IDNoBBCodeCodeBoxes = inNoBBCodeCodeBoxes;
  IDNonParsed = inNonParsed;
  IDAllFilesDmps = inAllFilesDmps;
  IDForDebugging = 1;
  IDMemory = 1;
  openOnExit = inOpenOnExit;
  inHtml = inInHtml;
  cleanUp = 1;
  for(int i = 0; i < int(inkdCommands.size()); i++)
  {
    if(inkdCommands[i].length() > 0)
    {
      IDkdCommands.push_back(inkdCommands[i]);
    }
  }
  IDOnlyUser = inOnlyUser;

  OSVersion = -1;
  tempDir = tempString_s;
  userProfile = ConfigurationUtil::getUserProfilePath();
  m_bLogStarted = false;
  m_bTimeStarted = false;
}
InputData::~InputData(){
  delete compare;
}

// Deletes the temporary directory
void InputData::deleteTemporaryDirectory(){
  fileSystemManager.deleteTemporaryDirectory();
}

// Cleans up after the output files are generated
void InputData::cleanUpOperations(std::string timeDir){
  // The first cleanup operation is moving .dmp files into the
  //    output directory if there are .dmps to move
  if(IDDmpsExist && cleanUp){
    FileSystemUtil::createDirectoryWithBackup(timeDir, "dmps");
    for(int i = 0; i < int(dmpList2.size()); i++){
      FileSystemUtil::moveFile(dmpList2[i], timeDir + "\\dmps\\" + dmpList2[i]);
    }
    for(int i = 0; i < int(dmpList.size()); i++){
      FileSystemUtil::createDirectoryWithBackup(tempDir, "oldDmps");
      FileSystemUtil::moveFile(dmpList[i], tempDir + "\\oldDmps\\" + dmpList[i]);
    }
  }
  // The second cleanup operation is deleting the tmp directory
  //deleteTemporaryDirectory();
}

void InputData::showMessageBoxes(){  
  std::string errorString;
  // create a std::wstring for the input to the console object
  std::wstring temp;
  if(corruptedDmps.size() > 0){
    errorString = "The following .dmps were probably corrupted:";
    errorString = errorString + "\r\n\r\n";
    for(int i = 0; i < int(corruptedDmps.size()); i++){
      if(i > 0 && corruptedDmps[i] == corruptedDmps[i-1]){
      }
      else{
        errorString = errorString + corruptedDmps[i];
        errorString = errorString + "\r\n";
      }
    }
    // convert the command std::string to std::wstring format
    temp.assign(errorString.begin(),errorString.end());
    MessageBox(HWND_DESKTOP,const_cast<LPWSTR>(temp.c_str()),L"Corrupted .dmp Files",MB_OK | MB_ICONERROR | MB_SETFOREGROUND | MB_SYSTEMMODAL);
  }
  if(sizeMismatch.size() > 0){
    errorString = "The following .dmps were probably incomplete. There was a size mismatch:";
    errorString = errorString + "\r\n\r\n";
    for(int i = 0; i < int(sizeMismatch.size()); i++){
      errorString = errorString + sizeMismatch[i];
      errorString = errorString + "\r\n";
    }
    // convert the command std::string to std::wstring format
    temp.assign(errorString.begin(),errorString.end());
    MessageBox(HWND_DESKTOP,const_cast<LPWSTR>(temp.c_str()),L"Size Mismatch",MB_OK | MB_ICONERROR | MB_SETFOREGROUND | MB_SYSTEMMODAL);
  }
}

int InputData::downloadDumpFile(){
  // Download the dump.html file containing the DRT info
  std::string tempPath = tempDir + "\\dump.html";
  DrtBuilder drtBuilder;
  std::vector<std::thread> threads;
  int page = 1;
  int lastSize = -1;

  InitCriticalSection();
  downloadAndParse(drtBuilder, page, tempDir);
  int maxPages = drtBuilder.getMaxPages(tempDir + "\\dump" + std::to_string(page) + ".html");
  ++page;
  while (page <= maxPages)
  {
	  threads.emplace_back(downloadAndParse, std::ref(drtBuilder), page, tempDir);
	  ++page;
  }

  for (auto& thread : threads)
  {
	  if (thread.joinable())
	  {
		  thread.join();
	  }
  }

  drtBuilder.writeDriverInfoToFile(tempDir + "\\dump.txt");
  DelCriticalSection();
  std::ifstream infile;
  infile.open(tempDir + "\\dump.txt");
  std::string stringIn;
  while(!infile.eof() && infile.good()){
    infile >> stringIn;
    size_t found;
    found = stringIn.find("</html>");
    if(found != std::string::npos){
      infile.close();
      //return 2345;
    }
  }
  infile.close();
  int hrCombined = 0;
  std::ofstream outProg;
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Getting bugcheck codes to provide BugCheck Link";
  outProg.close();
  bcd.downloadBugCheckInfo();
  bcd.parseBugCheckInfo();
  return int(hrCombined);
}

// Parse the dump.txt file and create a usable driver list
void InputData::cleanUpDriverList(){
  // First, open the dump.txt file for accessing the DRT info
  std::string dlString = IDUserProfile + "/SysnativeBSODApps/download/dump.txt";
  std::ifstream inuser(dlString);
  // Second, create a file to output the info in an easier to read format
  std::string outString = IDUserProfile + "/SysnativeBSODApps/download/driverList.txt";
  std::ofstream cleaner(outString);
  // Second, create a file to output the descriptions in an easier to read format
  outString = IDUserProfile + "/SysnativeBSODApps/download/descriptions.txt";
  std::ofstream describe(outString);
  int twoCount;
  int countBrack = 0;
  while(!inuser.eof() && inuser.good()){
    char inCh;
    // read the file character by character
    inuser.get(inCh);
    // Each database variable in dump .txt is separated by a }
    if (inCh == '}' && countBrack < 2){
      countBrack++;
    }
    // New drivers may begin with a { separator
    if (countBrack > 3){
      countBrack = 0;
    }
    else{
      // the information before the first bracket contains the driver information
      if(countBrack < 1){
        cleaner << inCh;
        if(inCh != '}')
        {
          describe << inCh;
        }
        else
        {
          describe << " ";
        }
        twoCount = 0;
      }
      // get the description for the driver
      if(countBrack == 2){
        if (twoCount == 0){
          describe << "<";
          if(inCh == '}')
          {
            inuser >> inCh;
          }
        }
        if (inCh == '}'){
          char inCh2;
          char inCh3;
          inuser >> inCh2;
          while(inCh2 == '}')
          {
            inuser >> inCh2;
          }
          inuser >> inCh3;
          if(inCh3 == '{' || inCh3 == '\n' || inCh3 == '\r')
          {
            // the information after the third/fourth bracket contains the driver type integer
            cleaner << " " << inCh2;
            countBrack = 0;
            cleaner << std::endl;            
            describe << " ";
                describe << ">" << std::endl;
          }
          else
          {
            if (inCh == '}')
            {
              describe << " ";
            }
            else
            {
              describe << inCh;
            }
            if (inCh2 == '}')
            {
              describe << " ";
            }
            else
            {
              describe << inCh2;
            }
            if (inCh3 == '}')
            {
              describe << " ";
            }
            else
            {
              describe << inCh3;
            }
          }
        }
        else{          
          if (inCh == '}')
          {
            describe << " ";
          }
          else
          {
            describe << inCh;
          }
        }
        twoCount++;
      }
    }
  }
  cleaner.close();
  describe.close();
  inuser.close();
}

void InputData::getDriverList(int i){
  std::string databaseTemp[1] = {" "};
  int databaseSystemTemp[1] = {0};
  std::string descriptionTemp[1] = {" "};
  for(int j=0; j<1; j++){
    database.push_back(databaseTemp[j]);
    database2.push_back(databaseTemp[j]);
    databaseSystem.push_back(databaseSystemTemp[j]);
    description.push_back(descriptionTemp[j]);
  }
}

// Get the driver list and store it in memory
void InputData::getDriverList(){
  // std::ofstream outNewDebug;
    // outNewDebug.open("C:\\Users\\Public\\Documents\\debug.txt", std::ios::app);
  std::string inString = IDUserProfile + "/SysnativeBSODApps/download/driverList.txt";
  std::ifstream infile(inString);
  inString = IDUserProfile + "/SysnativeBSODApps/download/descriptions.txt";
  std::ifstream describe(inString);
  std::string databaseTemp;
  int databaseSystemTemp;
  int whileCount = 0;
  // store each driver and driver type in a database
  while (!infile.eof() && infile.good()){
    std::string tempString;
    infile >> tempString;
    // make sure the driver name is long enough to keep
    if(tempString.length() > 1){
      // Check if the full driver name was added
      databaseTemp = tempString;
      while(tempString.length() > 1)
      {// Convert spaces to underscores
        infile >> tempString;
        if(tempString.length() > 1){
          databaseTemp += "_" + tempString;
        }
      }
      std::stringstream ssInt;
      ssInt << tempString;
      // stores the driver in the database std::vector std::string
      ssInt >> databaseSystemTemp;
      if(databaseSystemTemp == 1){
        MicrosoftOSDrivers.push_back(databaseTemp);
      }        
      database.push_back(databaseTemp);
      // stores the driver type in the databaseSystem std::vector int
      databaseSystem.push_back(databaseSystemTemp);
      //stringstream outNewDebug;
      // outNewDebug << databaseTemp << " " << databaseSystemTemp;
      //outputLog(outNewDebug.str());
    }
    // if the drivername is not long enough to keep, read in the driver type 
    //    as a temporary variable and do not store it
    else{
      infile >> databaseSystemTemp;
      //stringstream outNewDebug;
      // outNewDebug << "should never see me " << databaseTemp << " " << databaseSystemTemp;
      //outputLog(outNewDebug.str());
    }
    ++whileCount;
    if(infile.eof() || !infile.good())
    {
      bool endOfFile = infile.eof();
      bool badFile = infile.good();
      int count = whileCount;
    }
  }
  infile.close();

  // count left triangle bracket when it appears
  int countTriangle1 = 0;
  int countTriangle2 = 0;
  int i = 0;
  int j = 0;
  // std::stringstream for holding driver name info
  std::stringstream ssdatabase;
  // std::stringstream for holding description info
  std::stringstream ssdescribe;
  while(!describe.eof() && describe.good()){
    char inch;
    // read character by character
    describe.get(inch);
    if(inch == '<'){
      countTriangle1 = 1;
    }
    if(inch == '>'){
      describe.get(inch);
      if(inch == '\n' || inch == '\r')
      {
        countTriangle2 = 1;
      }
    }
    // Getting driver name one character at a time
    if(!countTriangle1){
      ssdatabase << inch;
      i++;
    }
    // Getting driver description one character at a time
    if(countTriangle1 && !countTriangle2){
      if(i > 2 && inch != '<'){
        ssdescribe << inch;
        j++;
      }
    }
    // Storing driver names and descriptions in a database
    if(countTriangle1 && countTriangle2){
      if(i > 2){
        database2.push_back(ssdatabase.str());
        description.push_back(ssdescribe.str());
        //stringstream outNewDebug;
        // outNewDebug << "DB: " << databaseSystem[database2.size()-1] << " - " << database[database2.size()-1] << " - " << ssdescribe.str() << std::endl;
        // outNewDebug << databaseSystem[database2.size()-1] << " - " << ssdatabase.str() << " - " << ssdescribe.str();
        //outputLog(outNewDebug.str());        
        //outputLog(ssdatabase.str());
      }
      countTriangle1 = 0;
      countTriangle2 = 0;
      i = 0;
      j = 0;
      ssdatabase.str("");
      ssdescribe.str("");
    }
  }

  while(description.size() < database.size()){
    description.push_back(" ");
    database2.push_back(" ");
  }
  std::stringstream outfile;
  outfile << "Size of database = " << database.size() << std::endl;
  outfile << "Size of description = " << description.size() << std::endl;
  if (!database.empty())
  {
	  outfile << "database[" << int(database.size()) - 1 << "] = " << database[int(database.size()) - 1] << std::endl;
  }
  if (!description.empty())
  {
	  outfile << "description[" << int(description.size()) - 1 << "] = " << description[int(description.size()) - 1] << std::endl;
  }
  outfile << "Finished getting description std::vector" << std::endl;
  outputLog(outfile.str());
  describe.close();
  for(int i=0; i<int(database2.size()); i++){
    if(description[i].find("]BSOD") != std::string::npos ||
      description[i].find(" BSOD") != std::string::npos ||
      description[i].find("w/BSOD") != std::string::npos){
        SevenBSODDriver.push_back(database2[i]);
        EightBSODDriver.push_back(database2[i]);
    }
  }
}

void InputData::getDmpsList(){
  clock_t t01, t02;
  float diff01;
  std::ofstream outError2;
  outError2.open(tempDir + "\\outError.txt");
  outError2 << "";
  outError2.close();
  countKD = 0;
  countUser = 0;
  std::ofstream outPercentDone;
  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  outPercentDone << 99;
  outPercentDone.close();

  std::vector<std::string> dmpsFound;
  dmpsFound = fileSystemManager.dmpCheck(tempDir);
  if(dmpsFound.size() > 0)
  {    
      IDDmpsExist++;
  }
  else if(fileSystemManager.getEmptyFiles().size() > 0)
  {
    std::string errorString = "The following .dmps were empty: ";
    for(size_t i=0; i<fileSystemManager.getEmptyFiles().size(); i++)
    {
      errorString = errorString + "\n" + fileSystemManager.getEmptyFiles()[i];
    }
    std::wstring temp;
    temp.assign(errorString.begin(), errorString.end());

    if(MessageBox(HWND_DESKTOP,const_cast<LPWSTR>(temp.c_str()),
      L"Empty .dmps",MB_OK | MB_ICONERROR | MB_SETFOREGROUND | MB_SYSTEMMODAL) == IDOK)
    {
      openOnExit = 0;
      cleanUp = 0;
      inHtml = 0;
      tempPath = tempDir + "\\percentDone.txt";
      outPercentDone.open(tempPath);
      outPercentDone << 100;
      outPercentDone.close();
      remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
      if(oldDmpList.size() > 0){        
        for(int i = 0; i < int(oldDmpList.size()); i++){
          FileSystemUtil::createDirectoryWithBackup(tempDir, "oldDmps");
          FileSystemUtil::moveFile(oldDmpList[i],tempDir + "oldDmps\\" + oldDmpList[i]);
        }
      }
      return;
    }
  }

  // list all files in the current directory
  //    and save the list to a temporary file
  std::vector<std::string> fileList;
  System::String^ myPath = System::IO::Directory::GetCurrentDirectory();
  std::string currentDir = SystemStandardConversionUtil::convertSystemStringToStdString(myPath);
  fileList = FileSystemUtil::getFiles(currentDir);
  // Open that temporary file to read
  //    what files are in the current directory
  std::ifstream infile;
  // Store the dmp entries
  std::string inputCh0;
  std::string inputString0;
  // Store each file to determine whether it
  //    is a .dmp file

  t01 = clock();

  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  outPercentDone << 452312;
  outPercentDone.close();

  std::ofstream outProg;
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Getting list of old .dmp files";
  outProg.close();
  //stringstream ssDebug;
  //ssDebug << "Determining which files are .dmp files, please wait..." << "\r";
  //outputLog(ssDebug.str());
  std::ifstream inNewDmps(tempDir + "\\newDmpsOnly.txt");
  int newDmpsOnly;
  inNewDmps >> newDmpsOnly;
  inNewDmps.close();
  newDmpsOnly = 0;
  std::string path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
  std::ifstream inOldDmps(path);
  while(inOldDmps.good() && !inOldDmps.eof()){
    std::string inString;
    getline(inOldDmps,inString);
    if(inString.length() > 0){
      oldDmpList.push_back(inString);
    }
  }
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to get list of old .dmp files");
  inOldDmps.close();
  if(oldDmpList.size() > 10000){
    remove(path.c_str());
    oldDmpList.clear();
  }
  
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Getting timestamps for .dmps to analyze";
  outProg.close();
  int fl = 0;
  // put files in chronological order from newest to oldest
  std::vector<double> dayd;
  t01 = clock();
  while(fl < int(fileList.size()))
  {
    System::String^ str = gcnew System::String(fileList[fl].c_str());
    System::DateTime dt = System::IO::File::GetLastWriteTime( str );
    System::TimeSpan ts = System::DateTime::Now.Subtract(dt);
    dayd.push_back(double(dt.Year)*365.25 + double(dt.Month)*365.25/12.0 + 
      dt.Day + double(dt.Hour)/24.0 + double(dt.Minute)/24.0/60.0 + double(dt.Second)/24.0/60.0/60.0);
    fl++;
  }
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to get timestamps for .dmps to analyze");
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Sorting .dmps by timestamp, newest to oldest";
  outProg.close();
  fl = 0;
  t01 = clock();
  while(fl < int(fileList.size()))
  {
    int fl2 = fl + 1;
    while(fl2 < int(fileList.size()))
    {
      if(dayd[fl2] > dayd[fl])
      {
        double tempd = dayd[fl];
        dayd[fl] = dayd[fl2];
        dayd[fl2] = tempd;
        std::string temps = fileList[fl];
        fileList[fl] = fileList[fl2];
        fileList[fl2] = temps;
      }
      fl2++;
    }
    fl++;
  }
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to sort .dmps based on timestamps");
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Getting .dmp file names (extracting from full paths)";
  outProg.close();
  fl = 0;
  std::vector<int> sizei;
  t01 = clock();
  while(fl < int(fileList.size()))
  {   
    double thisSize;
    std::ifstream ifile(fileList[fl]);
    ifile.seekg(0, std::ios_base::end);//seek to end
    //now get current position as length of file
    thisSize = double(ifile.tellg()) / 1024.0;
    ifile.close();
    sizei.push_back(int(ceil(thisSize)+0.1));
    size_t find1 = fileList[fl].find("\\");
    int j1 = 0;
    while(find1 != std::string::npos)
    {
      j1 = int(find1);
      find1 = fileList[fl].find("\\",j1+1,1);
    }
    fileList[fl] = fileList[fl].substr(j1+1,fileList[fl].length()-j1-1);
    fl++;
  }
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to extract file names from full paths");
  fl = 0;
  int dmpsFoundi;
  while(fl < int(fileList.size()))
  {
    Sleep(105);
    t01 = clock();
    tempPath = tempDir + "\\analyzedDmpOf.txt";
    outProg.open(tempPath);
    outProg << "Checking " << fileList[fl] << " for .dmp file header";
    outProg.close();
    std::stringstream ssLog;
    ssLog << "check " << fileList[fl] << " for .dmp file header";
    int inExit = 0;

    std::ifstream inExitFile(tempDir + "\\inExitFile.txt");
    inExitFile >> inExit;
    inExitFile.close();

    if(inExit){
      openOnExit = 0;
      cleanUp = 0;
      inHtml = 0;
      tempPath = tempDir + "\\percentDone.txt";
      outPercentDone.open(tempPath);
      outPercentDone << 100;
      outPercentDone.close();
      remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
      return;
    }
    // read the current line in the temporary
    //    file listing current directory
    inputCh0 = fileList[fl];
    fl++;
      
    inputString0 = inputCh0;
    for(int i1=0; i1 < int(inputString0.size()); i1++){
      inputString0[i1] = tolower(inputString0[i1]);
    }
    int didItFindADmp = 0;
    // create a variable to determine if the
    //    directory item is a .dmp file
    size_t found;
    // Does the item contain .dmp in its name
    found = inputString0.find(".dmp");
    // If so, determine the number of .dmps
    if (found != std::string::npos && !IDAllFilesDmps){
      int seenThis = 0;
      if(newDmpsOnly){
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
      }
      dmpsFoundi = 0;
      for(int df=0; df<int(dmpsFound.size()); df++)
      {
        if(inputCh0==dmpsFound[df])
        {
          dmpsFoundi++;
        }
      }
      if(!seenThis && dmpsFoundi){
        dmpList.push_back(inputCh0);
        std::string path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
        std::ofstream outOldDmps;
        outOldDmps.open(path, std::ios::app);        
        int seenThis = 0;
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
        if(!seenThis){
          outOldDmps << inputString0 << std::endl;
        }
        outOldDmps.close();
      }
      didItFindADmp++;
    }
    // Does the item contain .kdmp in its name
    found = inputString0.find(".kdmp");
    // If so, determine the number of .dmps
    if (found != std::string::npos && !IDAllFilesDmps){
      int seenThis = 0;
      if(newDmpsOnly){
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
      }
      dmpsFoundi = 0;
      for(int df=0; df<int(dmpsFound.size()); df++)
      {
        if(inputCh0==dmpsFound[df])
        {
          dmpsFoundi++;
        }
      }
      if(!seenThis && dmpsFoundi){
        dmpList.push_back(inputCh0);
        std::string path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
        std::ofstream outOldDmps;
        outOldDmps.open(path, std::ios::app);        
        int seenThis = 0;
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
        if(!seenThis){
          outOldDmps << inputString0 << std::endl;
        }
        outOldDmps.close();
      }
      didItFindADmp++;
      IDDmpsExist++;
    }
    // Does the item contain .hdmp in its name
    found = inputString0.find(".hdmp");
    // If so, determine the number of .dmps
    if (found != std::string::npos && !IDAllFilesDmps){
      int seenThis = 0;
      if(newDmpsOnly){
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
      }
      dmpsFoundi = 0;
      for(int df=0; df<int(dmpsFound.size()); df++)
      {
        if(inputCh0==dmpsFound[df])
        {
          dmpsFoundi++;
        }
      }
      if(!seenThis && dmpsFoundi){
        dmpList.push_back(inputCh0);
        std::string path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
        std::ofstream outOldDmps;
        outOldDmps.open(path, std::ios::app);        
        int seenThis = 0;
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
        if(!seenThis){
          outOldDmps << inputString0 << std::endl;
        }
        outOldDmps.close();
      }
      didItFindADmp++;
      IDDmpsExist++;
    }
    // Does the item contain .mdmp in its name
    found = inputString0.find(".mdmp");
    // If so, determine the number of .dmps
    if (found != std::string::npos && !IDAllFilesDmps){
      int seenThis = 0;
      if(newDmpsOnly){
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
      }
      dmpsFoundi = 0;
      for(int df=0; df<int(dmpsFound.size()); df++)
      {
        if(inputCh0==dmpsFound[df])
        {
          dmpsFoundi++;
        }
      }
      if(!seenThis && dmpsFoundi){
        dmpList.push_back(inputCh0);
        std::string path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
        std::ofstream outOldDmps;
        outOldDmps.open(path, std::ios::app);        
        int seenThis = 0;
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
        if(!seenThis){
          outOldDmps << inputString0 << std::endl;
        }
        outOldDmps.close();
      }
      didItFindADmp++;
      IDDmpsExist++;
    }
    // Does the item contain .dump in its name
    found = inputString0.find(".dump");
    // If so, determine the number of .dmps
    if (found != std::string::npos && !IDAllFilesDmps){
      int seenThis = 0;
      if(newDmpsOnly){
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
      }
      dmpsFoundi = 0;
      for(int df=0; df<int(dmpsFound.size()); df++)
      {
        if(inputCh0==dmpsFound[df])
        {
          dmpsFoundi++;
        }
      }
      if(!seenThis && dmpsFoundi){
        dmpList.push_back(inputCh0);
        std::string path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
        std::ofstream outOldDmps;
        outOldDmps.open(path, std::ios::app);        
        int seenThis = 0;
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
        if(!seenThis){
          outOldDmps << inputString0 << std::endl;
        }
        outOldDmps.close();
      }
      didItFindADmp++;
      IDDmpsExist++;
    }
    // Does the item contain .txt in its name
    found = inputString0.find(".txt");
    // If so, determine the number of .dmps
    if (found != std::string::npos && !IDAllFilesDmps){
      int seenThis = 0;
      if(newDmpsOnly){
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
      }
      dmpsFoundi = 0;
      for(int df=0; df<int(dmpsFound.size()); df++)
      {
        if(inputCh0==dmpsFound[df])
        {
          dmpsFoundi++;
        }
      }
      if(!seenThis && dmpsFoundi){
        dmpList.push_back(inputCh0);
        std::string path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
        std::ofstream outOldDmps;
        outOldDmps.open(path, std::ios::app);        
        int seenThis = 0;
        for(int i=0; i<int(oldDmpList.size()); i++){
          if(inputString0 == oldDmpList[i]){
            seenThis++;
          }
        }
        if(!seenThis){
          outOldDmps << inputString0 << std::endl;
        }
        outOldDmps.close();
      }
      didItFindADmp++;
      IDDmpsExist++;
    }
    if(!didItFindADmp || IDAllFilesDmps){
      std::string kd = "cd.exe /c \"" + IDDbugDir + " -c \"q\" -z \"" + inputString0 + "\"" + "\"" + " > " + tempDir + "\\inKD.txt 2>&1";
      std::string kd2 = "\"" + IDDbugDir + " -c \"q\" -z \"" + inputString0 + "\"" + "\"" + " > " + tempDir + "\\inKD.txt 2>&1";
      std::string test;
      int countBadSymbols = 0;
        
      // Does the item contain .txt in its name
      found = inputString0.find(".txt");
      if (found != std::string::npos)
      {
        test = "................................................................";
      }
      
      // Does the item contain .dump in its name
      found = inputString0.find(".dump");
      if (1)
      {
        test = "................................................................";
      }
      
      if(test.find("................................................................") != std::string::npos){
        int seenThis = 0;
        if(newDmpsOnly){
          for(int i=0; i<int(oldDmpList.size()); i++){
            if(inputString0 == oldDmpList[i]){
              seenThis++;
            }
          }
        }
        dmpsFoundi = 0;
        for(int df=0; df<int(dmpsFound.size()); df++)
        {
          // std::stringstream outNewDebug;
          // outNewDebug << inputCh0 << " == " << dmpsFound[df] 
          //             << " " << bool(inputCh0 == dmpsFound[df]) << std::endl;
          // outputLog(outNewDebug.str());
          if(inputCh0==dmpsFound[df])
          {
            dmpsFoundi++;
          }
        }
        if(!seenThis && dmpsFoundi){
          dmpList.push_back(inputCh0);
          std::string path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
          std::ofstream outOldDmps;
          outOldDmps.open(path, std::ios::app);
          outOldDmps << inputString0 << std::endl;
          outOldDmps.close();
        }
        test = "";
        IDDmpsExist++;
      }
    }
    t02 = clock();  
    diff01 = ((float)t02 - (float)t01) / 1000.0F;
    OutputTimeBetween(diff01, " seconds to " + ssLog.str());
  }
  std::ofstream outfile;
  int tooSmall = 0;
  int wrongName = 0;
  int comma = 0;
  fl = 0;
  while(fl < int(fileList.size()))
  {
    Sleep(105);
    t01 = clock();
    tempPath = tempDir + "\\analyzedDmpOf.txt";
    outProg.open(tempPath);
    outProg << "Checking " << fileList[fl] << "'s file size";
    outProg.close();
    std::stringstream ssLog;
    ssLog << "check " << fileList[fl] << " for file size";
    inputCh0 = fileList[fl];
    int thisSize = sizei[fl];
    fl++;
    if(thisSize > 1){
      if(inputCh0.find(".exe") != std::string::npos || inputCh0.find(".EXE") != std::string::npos){
        comma++;
      }
    }
    for(int i = 0; i < int(dmpList.size()); i++){
      if(inputCh0.find(dmpList[i]) != std::string::npos){
        if(thisSize > 1){
        }
        else{      
          std::ofstream outError;
          outError.open(tempDir + "\\outError.txt", std::ios::app);
          outError << dmpList[i] << " may be incomplete; there is a size mismatch  " << std::endl;
          outError.close();
        }
      }
    }
    if(comma > 0 && dmpList.size() == 0){
      wrongName = 1;
    }
    if(inputCh0.find(".txt") != std::string::npos && dmpList.size() == 0){
      wrongName = 1;
    }
    if(inputCh0.find(".dump") != std::string::npos && dmpList.size() == 0){
      wrongName = 1;
    }
    t02 = clock();  
    diff01 = ((float)t02 - (float)t01) / 1000.0F;
    OutputTimeBetween(diff01, " seconds to " + ssLog.str());
  }
  if(wrongName){
    std::string errorString;
      
    std::ofstream outError;
    outError.open(tempDir + "\\outError.txt", std::ios::app);
    outError << "The .dmp extension may have been changed. Double check extensions." << std::endl;
    outError.close();

    if(MessageBox(HWND_DESKTOP,L"Missing .dmps\nPlease add .dmps to the directory and run again...",
      L"Missing .dmps",MB_OK | MB_ICONERROR | MB_SETFOREGROUND | MB_SYSTEMMODAL) == IDOK)
    {
      openOnExit = 0;
      cleanUp = 0;
      inHtml = 0;
      tempPath = tempDir + "\\percentDone.txt";
      outPercentDone.open(tempPath);
      outPercentDone << 100;
      outPercentDone.close();
      remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
      if(oldDmpList.size() > 0){        
        for(int i = 0; i < int(oldDmpList.size()); i++){
          FileSystemUtil::createDirectoryWithBackup(tempDir, "oldDmps");
          FileSystemUtil::moveFile(oldDmpList[i], tempDir + "\\oldDmps\\" + oldDmpList[i]);
        }
      }
      return;
    }
    else{
      openOnExit = 0;
      cleanUp = 0;
      inHtml = 0;
      tempPath = tempDir + "\\percentDone.txt";
      outPercentDone.open(tempPath);
      outPercentDone << 100;
      outPercentDone.close();
      remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
      if(oldDmpList.size() > 0){
        for(int i = 0; i < int(oldDmpList.size()); i++){
          FileSystemUtil::createDirectoryWithBackup(tempDir,"oldDmps");
          FileSystemUtil::moveFile(oldDmpList[i], tempDir + "\\oldDmps\\" + oldDmpList[i]);
        }
      }
      return;
    }
  }
  infile.close();
  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  outPercentDone << 99;
  outPercentDone.close();  
  std::ofstream outKeepGoingDmps(tempDir + "\\keepGoingDmps.txt");
  outKeepGoingDmps << "1" << std::endl;
  outKeepGoingDmps.close();

  std::string pathHere = SystemStandardConversionUtil::convertSystemStringToStdString(System::IO::Directory::GetCurrentDirectory());
  std::ofstream outPath(tempDir + "\\dmpList.txt");
  std::ofstream outPath2(tempDir + "\\dmpList2.txt");

  t01 = clock();
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Adding .dmp files list to file";
  outProg.close();
  for(int i=0; i<int(dmpList.size()); i++){
    std::string path;
    path = pathHere + "\\" + dmpList[i];
    outPath << path << std::endl;
    outPath2 << dmpList[i] << std::endl;
  }
  outPath.close();
  outPath2.close();
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to add .dmp files list to file");

  path = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
  outPath.open(path);
  t01 = clock();
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Adding .dmp files list to old .dmps list";
  outProg.close();
  for(int i=0; i<int(oldDmpList.size()); i++){
    outPath << oldDmpList[i] << std::endl;
  }
  outPath.close();
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to add .dmp files list to old .dmps list");
  
  t01 = clock();
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Removing list files from %TEMP%";
  outProg.close();
  remove(std::string(tempDir + "\\dmpListGUI.txt").c_str());
  remove(std::string(tempDir + "\\dmpListGUI2.txt").c_str());
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to remove list files from %TEMP%");

  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  outPercentDone << 339154;
  outPercentDone.close();

  // Enabling Windows XP visual effects before any controls are created
  System::Windows::Forms::Application::EnableVisualStyles();
  // SysnativeBSODApps::Application::SetCompatibleTextRenderingDefault(false); 

  // Create the main window and run it
  SysnativeBSODApps::DmpList^ myDmpList;
  myDmpList = gcnew SysnativeBSODApps::DmpList();
  myDmpList->tempStringS = gcnew System::String(tempDir.c_str());
  myDmpList->centerDmpList();
  myDmpList->ShowDialog();
    
  t01 = clock();
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Reading .dmp file list into memory for new .dmps";
  outProg.close();
  infile.open(tempDir + "\\dmpList3.txt");
  while(infile.good() && !infile.eof()){
    std::string inString;
    getline(infile, inString);
    if(inString.length() > 0){
      dmpList2.push_back(inString);
    }
  }
  infile.close();
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to read .dmp file list into memory for new .dmps");
  infile.open(tempDir + "\\dmpListGUI.txt");  
  t01 = clock();
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Checking if new .dmps exist (New .dmps Only?)";
  outProg.close();
  if(!infile.good() || dmpList2.size() == 0){
    infile.close();
    openOnExit = 0;
    cleanUp = 0;
    inHtml = 0;  
    tempPath = tempDir + "\\percentDone.txt";
    outPercentDone.open(tempPath);
    outPercentDone << 100;
    outPercentDone.close();  
    remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
    if(dmpList2.size() == 0 && infile.good()){
      std::string errorString;
      errorString = "There are currently no new .dmps to analyze.\r\n";
      std::wstring temp;
      // convert the command std::string to std::wstring format
      temp.assign(errorString.begin(),errorString.end());
    }
    return;
  }
  infile.close();
  t02 = clock();  
  diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to check if new .dmps exist (New .dmps Only?)");
  remove(std::string(tempDir + "\\dmpListGUI.txt").c_str());
}

void InputData::runDmps()
{
  std::ofstream outfile(tempDir + "\\dmps\\maxkdDmp.txt");
  outfile << "";
  outfile.close();
  if(IDOnlyUser)
  {
    return;
  }
  int defaultThreads;
  int userThreads;
  std::ifstream infile(IDUserProfile + "\\SysnativeBSODApps\\forumSettings\\default.dzdn");
  int ptnu = 0;
  if(!infile.good())
  {
    defaultThreads = 1;
    userThreads = 1;
  }
  while(infile.good() && !infile.eof())
  {
    std::string inString;
    getline(infile,inString);
    if(inString.find("## ###parallelThreading_nu") != std::string::npos)
    {
      int cb, nu1, nu2;
      infile >> cb;
      infile >> nu1;
      infile >> nu2;
      defaultThreads = nu1;
      userThreads = nu2;
      if(cb == 0)
      {
        defaultThreads = 1;
        userThreads = 1;
      }
    }
  }
  infile.close();

  defaultKD^ threadWork;
  System::Threading::Thread^ newThread;
  
  for(int i=0; i<int(dmpList2.size()); i++)
  {   
    threadWork = gcnew defaultKD();
    threadWork->i = i;
    threadWork->IDSymbolsInt = IDSymbolsInt;
    threadWork->IDDbugDirS = gcnew System::String(IDDbugDir.c_str());
    threadWork->commandS = "!analyze -v; !sysinfo cpuspeed; !sysinfo SMBIOS; lmtsmn; q";
    threadWork->tempStringS = gcnew System::String(tempDir.c_str());
    threadWork->IDSymbolsStringS = gcnew System::String(IDSymbolsString.c_str());
    threadWork->dmpList2S = gcnew System::String(dmpList2[i].c_str());
    threadWork->outPathS = gcnew System::String(std::string(tempDir + "\\dmps\\outkdOutput" + std::to_string(i + 1) + ".txt").c_str());

    if(defaultThreads > 1)
    {
      time_t t1,t2;
      t1 = clock();
      t2 = clock();
      float diff = float(t2) - float(t1);
      while(myLock && diff < 5000)
      {
        t2 = clock();
        diff = float(t2) - float(t1);
        Sleep(105);
      }
      myLock = 1;
      newThread = gcnew System::Threading::Thread( gcnew System::Threading::ThreadStart( &threadWork->DoWork ) );
      newThread->Start();
      t1 = clock();
      t2 = clock();
      diff = float(t2) - float(t1);
      while(myLock && diff < 5000)
      {
        t2 = clock();
        diff = float(t2) - float(t1);
        Sleep(105);
      }
    }
    else
    {
      int inExit = 0;

      std::ifstream inExitFile(tempDir + "\\inExitFile.txt");
      inExitFile >> inExit;
      inExitFile.close();

      if(inExit){
        for(size_t pii=0; pii < piV.size(); pii++)
        {
          // Terminate process
          TerminateProcess(piV[pii].hProcess,1);
          CloseHandle( piV[pii].hProcess );
          CloseHandle( piV[pii].hThread );
        }
        countKD = 0;
        countUser = 0;
        openOnExit = 0;
        cleanUp = 0;
        inHtml = 0;
        tempPath = tempDir + "\\percentDone.txt";
        std::ofstream outPercentDone;
        outPercentDone.open(tempPath);
        outPercentDone << 100;
        outPercentDone.close();
        remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
        return;
      }
      if(IDkdCommands.size() > 0)
      {
        tempPath = tempDir + "\\percentDone.txt";
        std::ofstream outPercentDone;
        outPercentDone.open(tempPath);
        outPercentDone << int(double(countKD + countUser) / double(dmpList2.size() + dmpList2.size() * IDkdCommands.size()) * 100.0);
        outPercentDone.close();
        std::ofstream outProg;      
        tempPath = tempDir + "\\analyzedDmpOf.txt";
        outProg.open(tempPath);
        outProg << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
        //stringstream ssDebug;
        //ssDebug << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
        //outputLog(ssDebug.str());
        outProg.close();
      }
      else
      {
        tempPath = tempDir + "\\percentDone.txt";
        std::ofstream outPercentDone;
        outPercentDone.open(tempPath);
        outPercentDone << int(double(countKD) / double(dmpList2.size()) * 100.0);
        outPercentDone.close();
        std::ofstream outProg;      
        tempPath = tempDir + "\\analyzedDmpOf.txt";
        outProg.open(tempPath);
        outProg << "Running kd " << countKD << " of " << dmpList2.size();
        //stringstream ssDebug;
        //ssDebug << "Running kd " << countKD << " of " << dmpList2.size();
        //outputLog(ssDebug.str());
        outProg.close();
      }
      threadWork->DoWork();
      if(loggingChecked)
      {
        FileSystemUtil::createDirectoryWithBackup(IDUserProfile + "\\SysnativeBSODApps", "Logs");
        FileSystemUtil::copyFile(IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRuns.txt", IDUserProfile + "\\SysnativeBSODApps\\Logs\\timeBetweenRuns.txt", true);
        FileSystemUtil::copyDirectoryAndContents(tempDir, IDUserProfile + "\\SysnativeBSODApps\\Logs", true);
      }
    }
    if(IDkdCommands.size() > 0)
    {
      tempPath = tempDir + "\\percentDone.txt";
      std::ofstream outPercentDone;
      outPercentDone.open(tempPath);
      outPercentDone << int(double(countKD + countUser) / double(dmpList2.size() + dmpList2.size() * IDkdCommands.size()) * 100.0);
      outPercentDone.close();
      std::ofstream outProg;      
      tempPath = tempDir + "\\analyzedDmpOf.txt";
      outProg.open(tempPath);
      outProg << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
      //stringstream ssDebug;
      //ssDebug << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
      //outputLog(ssDebug.str());
      outProg.close();
    }
    else
    {
      tempPath = tempDir + "\\percentDone.txt";
      std::ofstream outPercentDone;
      outPercentDone.open(tempPath);
      outPercentDone << int(double(countKD) / double(dmpList2.size()) * 100.0);
      outPercentDone.close();
      std::ofstream outProg;      
      tempPath = tempDir + "\\analyzedDmpOf.txt";
      outProg.open(tempPath);
      outProg << "Running kd " << countKD << " of " << dmpList2.size();
      //stringstream ssDebug;
      //ssDebug << "Running kd " << countKD << " of " << dmpList2.size();
      //outputLog(ssDebug.str());
      outProg.close();
    }
    if(int(dmpList2.size()) >= defaultThreads)
    {
      if((i - countKD >= defaultThreads - 1) || defaultThreads == 1)
      {
        while(i - countKD >= defaultThreads - 1)
        {
          Sleep(105);
          int inExit = 0;

          std::ifstream inExitFile(tempDir + "\\inExitFile.txt");
          inExitFile >> inExit;
          inExitFile.close();

          if(inExit){
            newThread->Abort();
            for(size_t pii=0; pii < piV.size(); pii++)
            {
              // Terminate process
              TerminateProcess(piV[pii].hProcess,1);
              CloseHandle( piV[pii].hProcess );
              CloseHandle( piV[pii].hThread );
            }
            countKD = 0;
            countUser = 0;
            openOnExit = 0;
            cleanUp = 0;
            inHtml = 0;
            tempPath = tempDir + "\\percentDone.txt";
            std::ofstream outPercentDone;
            outPercentDone.open(tempPath);
            outPercentDone << 100;
            outPercentDone.close();
            remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
            return;
          }
          if(loggingChecked)
          {
            FileSystemUtil::createDirectoryWithBackup(IDUserProfile + "\\SysnativeBSODApps", "Logs");
            FileSystemUtil::copyFile(IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRuns.txt", IDUserProfile + "\\SysnativeBSODApps\\Logs\\timeBetweenRuns.txt", true);
            FileSystemUtil::copyDirectoryAndContents(tempDir, IDUserProfile + "\\SysnativeBSODApps\\Logs", true);
          }
        }
      }
    }
  }
}

void InputData::runUserDmps(){
  FileSystemUtil::createDirectoryWithBackup(tempDir,"kdc");
  std::ofstream outfile(tempDir + "\\dmps\\maxUserDmp.txt");
  outfile << "";
  outfile.close();
  int defaultThreads;
  int userThreads;
  std::ifstream infile(IDUserProfile + "\\SysnativeBSODApps\\forumSettings\\Default.dzdn");
  int ptnu = 0;
  if(!infile.good())
  {
    defaultThreads = 1;
    userThreads = 1;
  }
  while(infile.good() && !infile.eof())
  {
    std::string inString;
    getline(infile,inString);
    if(inString.find("## ###parallelThreading_nu") != std::string::npos)
    {
      int cb, nu1, nu2;
      infile >> cb;
      infile >> nu1;
      infile >> nu2;
      defaultThreads = nu1;
      userThreads = nu2;
      if(cb == 0)
      {
        defaultThreads = 1;
        userThreads = 1;
      }
    }
  }
  infile.close();
  
  defaultKD^ threadWork;
  System::Threading::Thread^ newThread;
  if(IDOnlyUser && countKD < int(dmpList2.size()))
  {
    countKD = int(dmpList2.size());
  }
  
  for(int i=0; i<int(dmpList2.size()); i++)
  {  
    for(int i1=0; i1<int(IDkdCommands.size()); i1++)
    {
      threadWork = gcnew defaultKD();
      threadWork->i = i;
      threadWork->IDSymbolsInt = IDSymbolsInt;
      threadWork->IDDbugDirS = gcnew System::String(IDDbugDir.c_str());
      threadWork->commandS = gcnew System::String(std::string(IDkdCommands[i1] + "; q").c_str());
      threadWork->tempStringS = gcnew System::String(tempDir.c_str());
      threadWork->IDSymbolsStringS = gcnew System::String(IDSymbolsString.c_str());
      threadWork->dmpList2S = gcnew System::String(dmpList2[i].c_str());
      threadWork->outPathS = gcnew System::String(std::string(tempDir + "\\kdc\\outkdOutput" + std::to_string(i + 1) + "." + 
         std::to_string(i1 + 1) + ".txt").c_str());
      if(userThreads > 1)
      {
        time_t t1,t2;
        t1 = clock();
        t2 = clock();
        float diff = float(t2) - float(t1);
        while(myLock && diff < 5000)
        {
          t2 = clock();
          diff = float(t2) - float(t1);
          Sleep(105);
        }
        myLock = 1;
        newThread = gcnew System::Threading::Thread( gcnew System::Threading::ThreadStart( &threadWork->DoWork ) );
        newThread->Start();
        t1 = clock();
        t2 = clock();
        diff = float(t2) - float(t1);
        while(myLock && diff < 5000)
        {
          t2 = clock();
          diff = float(t2) - float(t1);
          Sleep(105);
        }
      }
      else
      {
        int inExit = 0;

        std::ifstream inExitFile(tempDir + "\\inExitFile.txt");
        inExitFile >> inExit;
        inExitFile.close();

        if(inExit){
          for(size_t pii=0; pii < piV.size(); pii++)
          {
            // Terminate process
            TerminateProcess(piV[pii].hProcess,1);
            CloseHandle( piV[pii].hProcess );
            CloseHandle( piV[pii].hThread );
          }
          countKD = 0;
          countUser = 0;
          openOnExit = 0;
          cleanUp = 0;
          inHtml = 0;
          tempPath = tempDir + "\\percentDone.txt";
          std::ofstream outPercentDone;
          outPercentDone.open(tempPath);
          outPercentDone << 100;
          outPercentDone.close();
          remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
          return;
        }
        if(!IDOnlyUser)
        {
          tempPath = tempDir + "\\percentDone.txt";
          std::ofstream outPercentDone;
          outPercentDone.open(tempPath);
          outPercentDone << int(double(countKD + countUser) / double(dmpList2.size() + dmpList2.size() * IDkdCommands.size()) * 100.0);
          outPercentDone.close();
          std::ofstream outProg;      
          tempPath = tempDir + "\\analyzedDmpOf.txt";
          outProg.open(tempPath);
          outProg << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
          //stringstream ssDebug;
          //ssDebug << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
          //outputLog(ssDebug.str());
          outProg.close();
        }
        else
        {  
          tempPath = tempDir + "\\percentDone.txt";
          std::ofstream outPercentDone;
          outPercentDone.open(tempPath);
          outPercentDone << int(double(countUser) / double(dmpList2.size() * IDkdCommands.size()) * 100.0);
          outPercentDone.close();
          std::ofstream outProg;      
          tempPath = tempDir + "\\analyzedDmpOf.txt";
          outProg.open(tempPath);
          outProg << "Running user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
          //stringstream ssDebug;
          //ssDebug << "Running user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
          //outputLog(ssDebug.str());
          outProg.close();
        }
        threadWork->DoWork();
        if(loggingChecked)
        {
          FileSystemUtil::createDirectoryWithBackup(IDUserProfile + "\\SysnativeBSODApps", "Logs");
          FileSystemUtil::copyFile(IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRuns.txt", IDUserProfile + "\\SysnativeBSODApps\\Logs\\timeBetweenRuns.txt", true);
          FileSystemUtil::copyDirectoryAndContents(tempDir, IDUserProfile + "\\SysnativeBSODApps\\Logs", true);
        }
      }
      if(!IDOnlyUser)
      {
        tempPath = tempDir + "\\percentDone.txt";
        std::ofstream outPercentDone;
        outPercentDone.open(tempPath);
        outPercentDone << int(double(countKD + countUser) / double(dmpList2.size() + dmpList2.size() * IDkdCommands.size()) * 100.0);
        outPercentDone.close();
        std::ofstream outProg;      
        tempPath = tempDir + "\\analyzedDmpOf.txt";
        outProg.open(tempPath);
        outProg << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
        //stringstream ssDebug;
        //ssDebug << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
        //outputLog(ssDebug.str());
        outProg.close();
      }
      else
      {  
        tempPath = tempDir + "\\percentDone.txt";
        std::ofstream outPercentDone;
        outPercentDone.open(tempPath);
        outPercentDone << int(double(countUser) / double(dmpList2.size() * IDkdCommands.size()) * 100.0);
        outPercentDone.close();
        std::ofstream outProg;      
        tempPath = tempDir + "\\analyzedDmpOf.txt";
        outProg.open(tempPath);
        outProg << "Running user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
        //stringstream ssDebug;
        //ssDebug << "Running user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
        //outputLog(ssDebug.str());
        outProg.close();
      }
      if(int(dmpList2.size() * IDkdCommands.size()) >= userThreads)
      {
        if((i * int(IDkdCommands.size()) + i1 + int(dmpList2.size()) - countKD - countUser >= userThreads -1)
          || userThreads == 1)
        {
          while(i * int(IDkdCommands.size()) + i1 + int(dmpList2.size()) - countKD - countUser >= userThreads - 1)
          {
            Sleep(105);
            int inExit = 0;

            std::ifstream inExitFile(tempDir + "\\inExitFile.txt");
            inExitFile >> inExit;
            inExitFile.close();

            if(inExit){
              newThread->Abort();
              for(size_t pii=0; pii < piV.size(); pii++)
              {
                // Terminate process
                TerminateProcess(piV[pii].hProcess,1);
                CloseHandle( piV[pii].hProcess );
                CloseHandle( piV[pii].hThread );
              }
              countKD = 0;
              countUser = 0;
              openOnExit = 0;
              cleanUp = 0;
              inHtml = 0;
              tempPath = tempDir + "\\percentDone.txt";
              std::ofstream outPercentDone;
              outPercentDone.open(tempPath);
              outPercentDone << 100;
              outPercentDone.close();
              remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
              return;
            }
            if(loggingChecked)
            {
              FileSystemUtil::createDirectoryWithBackup(IDUserProfile + "\\SysnativeBSODApps", "Logs");
              FileSystemUtil::copyFile(IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRuns.txt", IDUserProfile + "\\SysnativeBSODApps\\Logs\\timeBetweenRuns.txt", true);
              FileSystemUtil::copyDirectoryAndContents(tempDir, IDUserProfile + "\\SysnativeBSODApps\\Logs", true);
            }
          }
        }
      }
    }
  }
}

void InputData::getImportantInfo()
{  
  int thisCountKD = 0;
  int thisCountUser = 0;
  if(IDOnlyUser && countKD < int(dmpList2.size()))
  {
    countKD = int(dmpList2.size());
  }
  tempPath = tempDir + "\\percentDone.txt";
  std::ofstream outPercentDone;
  outPercentDone.open(tempPath);
  std::ofstream outProg;      
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  if(IDkdCommands.size() > 0 && !IDOnlyUser)
  {
    int currentProg = int(double(countKD + countUser-1) / double(dmpList2.size() + dmpList2.size() * IDkdCommands.size()) * 100.0);
    if(currentProg < 0)
    {
      currentProg = 0;
    }
    outPercentDone << currentProg;
    outProg << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
    //stringstream ssDebug;
    //ssDebug << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
    //outputLog(ssDebug.str());
  }
  else if(IDkdCommands.size() == 0)
  {
    int currentProg = int(double(countKD-1) / double(dmpList2.size()) * 100.0);
    if(currentProg < 0)
    {
      currentProg = 0;
    }
    outPercentDone << currentProg;
    outProg << "Running kd " << countKD << " of " << dmpList2.size();
    //stringstream ssDebug;
    //ssDebug << "Running kd " << countKD << " of " << dmpList2.size();
    //outputLog(ssDebug.str());
  }
  else
  {
    int currentProg = int(double(countUser-1) / double(dmpList2.size() * IDkdCommands.size()) * 100.0);
    if(currentProg < 0)
    {
      currentProg = 0;
    }
    outPercentDone << currentProg;
    outProg << "Running user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
    //stringstream ssDebug;
    //ssDebug << "Running user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size() << std::endl;
    //outputLog(ssDebug.str());
  }
  outProg.close();
  outPercentDone.close();
  while(countKD < int(dmpList2.size()) || countUser < int(dmpList2.size() * IDkdCommands.size()))
  {
    Sleep(105);
    int inExit = 0;

    std::ifstream inExitFile(tempDir + "\\inExitFile.txt");
    inExitFile >> inExit;
    inExitFile.close();

    if(inExit){
      for(size_t pii=0; pii < piV.size(); pii++)
      {
        // Terminate process
        TerminateProcess(piV[pii].hProcess,1);
        CloseHandle( piV[pii].hProcess );
        CloseHandle( piV[pii].hThread );
      }
      countKD = 0;
      countUser = 0;
      openOnExit = 0;
      cleanUp = 0;
      inHtml = 0;
      tempPath = tempDir + "\\percentDone.txt";
      std::ofstream outPercentDone;
      outPercentDone.open(tempPath);
      outPercentDone << 100;
      outPercentDone.close();
      remove(std::string(tempDir + "\\keepGoingDmps.txt").c_str());
      return;
    }
    if(loggingChecked)
    {
      FileSystemUtil::createDirectoryWithBackup(IDUserProfile + "\\SysnativeBSODApps", "Logs");
      FileSystemUtil::copyFile(IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRuns.txt", IDUserProfile + "\\SysnativeBSODApps\\Logs\\timeBetweenRuns.txt", true);
      FileSystemUtil::copyDirectoryAndContents(tempDir, IDUserProfile + "\\SysnativeBSODApps\\Logs", true);
    }
  }
  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  if(IDkdCommands.size() > 0 && !IDOnlyUser)
  {
    int currentProg = int(double(countKD + countUser-1) / double(dmpList2.size() + dmpList2.size() * IDkdCommands.size()) * 100.0);
    if(currentProg < 0)
    {
      currentProg = 0;
    }
    outPercentDone << currentProg;
    outProg << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
    //stringstream ssDebug;
    //ssDebug  << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
    //outputLog(ssDebug.str());
    //ssDebug.str("");
    //ssDebug << "Running kd " << countKD << " of " << dmpList2.size() << " and " << "user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size() << std::endl;
    //outputLog(ssDebug.str());
  }
  else if(IDkdCommands.size() == 0)
  {
    int currentProg = int(double(countKD-1) / double(dmpList2.size()) * 100.0);
    if(currentProg < 0)
    {
      currentProg = 0;
    }
    outPercentDone << currentProg;
    outProg << "Running kd " << countKD << " of " << dmpList2.size();
    //stringstream ssDebug;
    //ssDebug <<  "Running kd " << countKD << " of " << dmpList2.size() << std::endl;
    //outputLog(ssDebug.str());
  }
  else
  {
    int currentProg = int(double(countUser-1) / double(dmpList2.size() * IDkdCommands.size()) * 100.0);
    if(currentProg < 0)
    {
      currentProg = 0;
    }
    outPercentDone << currentProg;
    outProg << "Running user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size();
    //stringstream ssDebug;
    //ssDebug << "Running user kd " << countUser << " of " << dmpList2.size() * IDkdCommands.size() << std::endl;
    //outputLog(ssDebug.str());
  }
  outProg.close();
  outPercentDone.close();
  Sleep(210);
  countKD = 0;
  countUser = 0;
  std::vector<std::string> defaultOutput;
  std::vector<int> defaultIndex;
  std::vector<int> defaultIndex2;
  defaultIndex.push_back(0);
  for(int i=0; i<int(dmpList2.size()); i++)
  {
    using namespace std::string_literals;
    std::string inPath = tempDir + "\\dmps\\outkdOutput"s + std::to_string(i + 1) + ".txt"s;
    std::ifstream infile3(inPath);
    while(!infile3.eof() && infile3.good())
    {
      std::string str;
      // read the std::stringstream variable line by line
      getline(infile3,str);
      // store the .dmp analysis from the kd command in a std::vector std::string
      defaultOutput.push_back(str);
      // get the debug time for when each .dmp was generated on the system
      //    that the .dmps were generated on
      size_t foundKeyword = str.find("Debug session time");
      if (foundKeyword != std::string::npos)
      {
        std::string temp = str.substr(20,str.end() - str.begin());
        // store the time of each .dmp for separating each .dmp in the output files
        timeDmp.push_back(temp);
      }
    }
    infile3.close();
    if(defaultIndex.size() > timeDmp.size() && defaultOutput.size() > 0)
    {
      timeDmp.push_back("Sun Jan  1 00:00:00.000 1969 (UTC - 0:00)");
    }
    if(i<int(dmpList2.size()-1))
    {
      defaultIndex.push_back(int(defaultOutput.size()));
    }
    defaultIndex2.push_back(int(defaultOutput.size()));
  }

  for(int i=0; i<int(timeDmp.size()); i++)
  {
    for(int j=i+1; j<int(timeDmp.size()); j++)
    {
      double timeStampDmpi;
      double timeStampDmpj;
      std::stringstream sstempi;
      std::stringstream sstempj;
      for(int i2=0; i2<int(timeDmp[i].length()); i2++)
      {
        if(timeDmp[i][i2] == ':' || timeDmp[i][i2] == '.')
        {
          sstempi << ' ';
        }
        else
        {
          sstempi << timeDmp[i][i2];
        }
      }
      for(int i2=0; i2<int(timeDmp[j].length()); i2++)
      {
        if(timeDmp[j][i2] == ':' || timeDmp[j][i2] == '.')
        {
          sstempj << ' ';
        }
        else
        {
          sstempj << timeDmp[j][i2];
        }
      }
      std::string day;
      std::string month;
      int monthn;
      int dayn;
      int hourn;
      int minn;
      int secn;
      int miln;
      int yearn;
      sstempi >> day >> month >> dayn >> hourn >> minn >> secn >> miln >> yearn;
      monthn = 0;
      monthn = (month == "Jan") ?  1 : monthn;
      monthn = (month == "Feb") ?  2 : monthn;
      monthn = (month == "Mar") ?  3 : monthn;
      monthn = (month == "Apr") ?  4 : monthn;
      monthn = (month == "May") ?  5 : monthn;
      monthn = (month == "Jun") ?  6 : monthn;
      monthn = (month == "Jul") ?  7 : monthn;
      monthn = (month == "Aug") ?  8 : monthn;
      monthn = (month == "Sep") ?  9 : monthn;
      monthn = (month == "Oct") ? 10 : monthn;
      monthn = (month == "Nov") ? 11 : monthn;
      monthn = (month == "Dec") ? 12 : monthn;
      timeStampDmpi = double(yearn) * 365.25 + double(monthn) * 365.25/12.0 + double(dayn) + double(hourn) * 1.0/24.0
        + double(minn)*1.0/24.0/60.0 + double(secn)*1.0/24.0/60.0/60.0 + double(miln)*1.0/24.0/60.0/60.0/1000.0;
      sstempj >> day >> month >> dayn >> hourn >> minn >> secn >> miln >> yearn;
      monthn = 0;
      monthn = (month == "Jan") ?  1 : monthn;
      monthn = (month == "Feb") ?  2 : monthn;
      monthn = (month == "Mar") ?  3 : monthn;
      monthn = (month == "Apr") ?  4 : monthn;
      monthn = (month == "May") ?  5 : monthn;
      monthn = (month == "Jun") ?  6 : monthn;
      monthn = (month == "Jul") ?  7 : monthn;
      monthn = (month == "Aug") ?  8 : monthn;
      monthn = (month == "Sep") ?  9 : monthn;
      monthn = (month == "Oct") ? 10 : monthn;
      monthn = (month == "Nov") ? 11 : monthn;
      monthn = (month == "Dec") ? 12 : monthn;
      timeStampDmpj = double(yearn) * 365.25 + double(monthn) * 365.25/12.0 + double(dayn) + double(hourn) * 1.0/24.0
        + double(minn)*1.0/24.0/60.0 + double(secn)*1.0/24.0/60.0/60.0 + double(miln)*1.0/24.0/60.0/60.0/1000.0;
      if(timeStampDmpj > timeStampDmpi)
      {
        std::string tempTimeDmp = timeDmp[j];
        timeDmp[j] = timeDmp[i];
        timeDmp[i] = tempTimeDmp;
        int tempDefaultIndex = defaultIndex[j];
        defaultIndex[j] = defaultIndex[i];
        defaultIndex[i] = tempDefaultIndex;
        int tempDefaultIndex2 = defaultIndex2[j];
        defaultIndex2[j] = defaultIndex2[i];
        defaultIndex2[i] = tempDefaultIndex2;
      }
    }
  }
  for(int i=0; i<int(defaultIndex2.size()); i++)
  {
    int i10 = defaultIndex[i];
    int i11 = defaultIndex2[i];
    for(int i12=i10; i12<i11; i12++)
    {
      analyzedDmps.push_back(defaultOutput[i12]);
    }
  }


  
  for(int i1=0; i1<int(IDkdCommands.size()); i1++)
  {
    std::vector<std::string> userOutput;
    std::vector<int> userIndex;
    std::vector<int> userIndex2;
    std::vector<std::string> timeDmpUser;
    std::vector<std::string> analyzedDmpsUser;
    userIndex.push_back(0);

    for(int i=0; i<int(dmpList2.size()); i++)
    {
      using namespace std::string_literals;
      std::string inPath = tempDir + "\\kdc\\outkdOutput"s + std::to_string(i + 1) + "."s +
          std::to_string(i1 + 1) + ".txt"s;
      std::ifstream infile3;
      infile3.open(inPath);
      while(!infile3.eof() && infile3.good())
      {
        std::string str;
        // read the std::stringstream variable line by line
        getline(infile3,str);
        // store the .dmp analysis from the kd command in a std::vector std::string
        userOutput.push_back(str);
        // get the debug time for when each .dmp was generated on the system
        //    that the .dmps were generated on
        size_t foundKeyword = str.find("Debug session time");
        if (foundKeyword != std::string::npos && i1 == 0 && IDOnlyUser)
        {
          std::string temp = str.substr(20,str.end() - str.begin());
          // store the time of each .dmp for separating each .dmp in the output files
          timeDmp.push_back(temp);
          timeDmpUser.push_back(temp);
        }
        else if(foundKeyword != std::string::npos)
        {
          std::string temp = str.substr(20,str.end() - str.begin());
          // store the time of each .dmp for separating each .dmp in the output files
          timeDmpUser.push_back(temp);
        }
      }
      infile3.close();
      remove(inPath.c_str());
      
      if(userIndex.size() > timeDmpUser.size() && userOutput.size() > 0)
      {
        timeDmpUser.push_back("Sun Jan  1 00:00:00.000 1969 (UTC - 0:00)");
      }
      if(i<int(dmpList2.size()-1))
      {
        userIndex.push_back(int(userOutput.size()));
      }
      userIndex2.push_back(int(userOutput.size()));
    }

    for(int i=0; i<int(timeDmpUser.size()); i++)
    {
      for(int j=i+1; j<int(timeDmpUser.size()); j++)
      {
        double timeStampDmpi;
        double timeStampDmpj;
        std::stringstream sstempi;
        std::stringstream sstempj;
        for(int i2=0; i2<int(timeDmpUser[i].length()); i2++)
        {
          if(timeDmpUser[i][i2] == ':' || timeDmpUser[i][i2] == '.')
          {
            sstempi << ' ';
          }
          else
          {
            sstempi << timeDmpUser[i][i2];
          }
        }
        for(int i2=0; i2<int(timeDmpUser[j].length()); i2++)
        {
          if(timeDmpUser[j][i2] == ':' || timeDmpUser[j][i2] == '.')
          {
            sstempj << ' ';
          }
          else
          {
            sstempj << timeDmpUser[j][i2];
          }
        }
        std::string day;
        std::string month;
        int monthn;
        int dayn;
        int hourn;
        int minn;
        int secn;
        int miln;
        int yearn;
        sstempi >> day >> month >> dayn >> hourn >> minn >> secn >> miln >> yearn;
        monthn = 0;
        monthn = (month == "Jan") ?  1 : monthn;
        monthn = (month == "Feb") ?  2 : monthn;
        monthn = (month == "Mar") ?  3 : monthn;
        monthn = (month == "Apr") ?  4 : monthn;
        monthn = (month == "May") ?  5 : monthn;
        monthn = (month == "Jun") ?  6 : monthn;
        monthn = (month == "Jul") ?  7 : monthn;
        monthn = (month == "Aug") ?  8 : monthn;
        monthn = (month == "Sep") ?  9 : monthn;
        monthn = (month == "Oct") ? 10 : monthn;
        monthn = (month == "Nov") ? 11 : monthn;
        monthn = (month == "Dec") ? 12 : monthn;
        timeStampDmpi = double(yearn) * 365.25 + double(monthn) * 365.25/12.0 + double(dayn) + double(hourn) * 1.0/24.0
          + double(minn)*1.0/24.0/60.0 + double(secn)*1.0/24.0/60.0/60.0 + double(miln)*1.0/24.0/60.0/60.0/1000.0;
        sstempj >> day >> month >> dayn >> hourn >> minn >> secn >> miln >> yearn;
        monthn = 0;
        monthn = (month == "Jan") ?  1 : monthn;
        monthn = (month == "Feb") ?  2 : monthn;
        monthn = (month == "Mar") ?  3 : monthn;
        monthn = (month == "Apr") ?  4 : monthn;
        monthn = (month == "May") ?  5 : monthn;
        monthn = (month == "Jun") ?  6 : monthn;
        monthn = (month == "Jul") ?  7 : monthn;
        monthn = (month == "Aug") ?  8 : monthn;
        monthn = (month == "Sep") ?  9 : monthn;
        monthn = (month == "Oct") ? 10 : monthn;
        monthn = (month == "Nov") ? 11 : monthn;
        monthn = (month == "Dec") ? 12 : monthn;
        timeStampDmpj = double(yearn) * 365.25 + double(monthn) * 365.25/12.0 + double(dayn) + double(hourn) * 1.0/24.0
          + double(minn)*1.0/24.0/60.0 + double(secn)*1.0/24.0/60.0/60.0 + double(miln)*1.0/24.0/60.0/60.0/1000.0;
        if(timeStampDmpj > timeStampDmpi)
        {
          std::string tempTimeDmpUser = timeDmpUser[j];
          timeDmpUser[j] = timeDmpUser[i];
          timeDmpUser[i] = tempTimeDmpUser;
          int tempUserIndex = userIndex[j];
          userIndex[j] = userIndex[i];
          userIndex[i] = tempUserIndex;
          int tempUserIndex2 = userIndex2[j];
          userIndex2[j] = userIndex2[i];
          userIndex2[i] = tempUserIndex2;
        }
      }
    }
    for(int i=0; i<int(userIndex2.size()); i++)
    {
      int i10 = userIndex[i];
      int i11 = userIndex2[i];
      for(int i12=i10; i12<i11; i12++)
      {
        analyzedDmpsUser.push_back(userOutput[i12]);
      }
      analyzedDmpsUser.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
    }
    std::ofstream outkdCommand;
    std::string strUserkd;
    std::stringstream ssi1;
    ssi1 << i1+1;
    strUserkd = tempDir + "\\kdc\\user" + ssi1.str() + ".txt";
    outkdCommand.open(strUserkd, std::ios::app);
    int continueFlag = 0;
    for(int i=0; i<int(analyzedDmpsUser.size()); i++)
    {
      std::string str;
      // read the std::stringstream variable line by line
      str = analyzedDmpsUser[i];
      outkdCommand << str << std::endl;
      if(IDOnlyUser)
      {
        // store the .dmp analysis from the kd command in a std::vector std::string
        if(str.find("quit") != std::string::npos && i1 < int(IDkdCommands.size()) - 1)
        {
          continueFlag = 0;
        }
        else if(str.find("kd>") != std::string::npos && i1 > 0)
        {
          continueFlag = 1;
        }
        else if(i1 == 0)
        {
          continueFlag = 1;
        }
        if(continueFlag)
        {
          analyzedDmps.push_back(str);
        }
      }
    }
    outkdCommand.close();
  }

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Parsing selected lines from kd output for output files...";
  outProg.close();
  //stringstream ssDebug;
  //ssDebug << "Parsing selected lines from kd output for output files..." << std::endl;
  //outputLog(ssDebug.str());

  // counter to keep track of which .dmp file is being output
  int i = 0;
  // counter to determine whether the current portion of the .dmp info
  //    contains stack information
  IDStack = 0;
  std::stringstream outfile;
  outfile << "getImportantInfo" << std::endl;
  for(int j = 0; j < int(analyzedDmps.size()); j++){
    size_t foundKeyword;
    foundKeyword = analyzedDmps[j].find("quit:");
    outfile << analyzedDmps[j] << " " << j << " of " << int(analyzedDmps.size()) << std::endl;
    if(foundKeyword != std::string::npos || j == 0){
      if(i > 0){
        dmpsOutput.push_back(analyzedDmps[j]);
        fillInNinetyEight(i);
        // if this is not the first .dmp file, add a separator to the end of the previous .dmp file
        importantInfo.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
        importantInfoNon.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
        dmpsOutput.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
        analyzedStack.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
        analyzedBIOS.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
      }
      // make sure the timeDmp std::vector does not cause a memory leak
      if (i < int(timeDmp.size())){
        // add a separator to indicate which .dmp is being analyzed and information added to the output
        importantInfo.push_back("**************************" + timeDmp[i] + "**************************");
        importantInfoNon.push_back("**************************" + timeDmp[i] + "**************************");
        dmpsOutput.push_back("**************************" + timeDmp[i] + "**************************");
        analyzedStack.push_back("**************************" + timeDmp[i] + "**************************");
        analyzedBIOS.push_back("**************************" + timeDmp[i] + "**************************");
        std::string temp = "Debug session time: " + timeDmp[i];
        debugSessionTime.push_back(temp);
        ninetyEight.push_back(temp);
      }
      if(i == 0){
        dmpsOutput.push_back(analyzedDmps[j]);
      }
      i++;
    }
    else{      
      // add the kd output line to the dmps.txt dmpsOutput std::vector std::string
      dmpsOutput.push_back(analyzedDmps[j]);
    }
    // search for the std::string "Loading Dump File" and add the line to the importantInfo std::vector std::string
    //    if it is among the output items asked for by the user
    foundKeyword = analyzedDmps[j].find("Loading Dump File");
    if(foundKeyword != std::string::npos){
      // check whether this item is among those asked for in the output by the user
      if(IDLoadingDumpFile){
        importantInfo.push_back(analyzedDmps[j]);
        importantInfoNon.push_back(analyzedDmps[j]);
        ninetyEight.push_back(analyzedDmps[j]);
        if(IDAddSpaces){
          importantInfo.push_back(" ");
          importantInfoNon.push_back(" ");
        }
        loadingDumpFile.push_back(analyzedDmps[j]);
      }
    }
    // go through importantInfo and add those lines that correspond with output items
    //    asked for by the user
    getImportantLines(j);
    // add stack items to the stack.txt analyzedStack std::vector std::string
    getStack(j);
    // add SMBIOS items to the SMBIOS.txt analyzedBIOS std::vector std::string
    
    getSMBIOS(j);
  }
  // Fill in the last .dmp info for the _98-dbug.txt file
  if(i > 0){
    fillInNinetyEight(i);
  }
  outputLog(outfile.str());
//  outfile2.close();
}

// check for user specified items to store from the output
void InputData::getImportantLines(int j){
  std::stringstream outDebug;
  // outDebug << "kernelVersionString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDKernelVersion){
    kernelVersionString(j);
  }
  // outDebug << "builtByString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDBuiltBy){
    builtByString(j);
  }
  // outDebug << "IDSystemUptime(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDSystemUptime){
    systemUptimeString(j);
  }
  // outDebug << "bugCheckString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDBugCheck){
    bugCheckString(j);
  }
  // outDebug << "unableToVerifyString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDUnableToVerify){
    unableToVerifyString(j);
  }
  // outDebug << "symbolsNotLoadedString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDSymbolsNotLoaded){
    symbolsNotLoadedString(j);
  }
  // outDebug << "probablyCausedByString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDProbablyCausedBy){
    probablyCausedByString(j);
  }
  // outDebug << "bugcheckStrString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDBugcheckStr){
    bugcheckStrString(j);
  }
  // outDebug << "defaultBucketIDString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDDefaultBucketID){
    defaultBucketIDString(j);
  }
  // outDebug << "verifierStringOne(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDVerifier){
    verifierStringOne(j);
    if(!IDDefaultBucketID){
      // outDebug << "verifierStringTwo(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
      verifierStringTwo(j);
    }
  }
  // outDebug << "bugCheckAnalysisString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDBugCheckAnalysis){
    bugCheckAnalysisString(j);
  }
  // outDebug << "processNameString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDProcessName){
    processNameString(j);
  }
  // outDebug << "failureBucketIDString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDFailureBucketID){
    failureBucketIDString(j);
  }
  // outDebug << "errorCodeString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDErrorCode){
    errorCodeString(j);
  }
  // outDebug << "diskHardwareErrorString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDDiskHardwareError){
    diskHardwareErrorString(j);
  }
  // outDebug << "argumentsString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDArguments){
    argumentsString(j);
  }
  // outDebug << "CPUIDString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDCPUID){
    CPUIDString(j);
  }
  // outDebug << "maxSpeedString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDMaxSpeed){
    maxSpeedString(j);
  }
  // outDebug << "currentSpeedString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDCurrentSpeed){
    currentSpeedString(j);
  }
  // outDebug << "BIOSVersionString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDBIOSVersion){
    BIOSVersionString(j);
  }
  // outDebug << "BIOSReleaseDateString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDBIOSReleaseDate){
    BIOSReleaseDateString(j);
  }
  // outDebug << "systemManufacturerString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDSystemManufacturer){
    systemManufacturerString(j);
  }
  // outDebug << "systemProductNameString(" << j << " of " << analyzedDmps.size() << ")" << std::endl;
  if(IDSystemProductName){
    systemProductNameString(j);
  }
  // outputLog(outDebug.str());
}

// search for the std::string "Windows 7 Kernel Version" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::kernelVersionString(int j){
  std::string temp;
  std::string temp2;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("Kernel Version");
  // make sure the version 7600 or 7601 is easily found to check for SP1
  if (foundKeyword != std::string::npos){
    m_bProbablyCausedBy = false;
    m_sSymbolName = "";
    m_sUnableToVerify = "";
    m_sSymbolsNotLoaded = "";
    std::stringstream ssBuild;
    ssBuild << analyzedDmps[j].substr(int(foundKeyword) + 15,4);
    ssBuild >> buildNumber;
    ssBuild.clear();
    std::stringstream ssLog;
    ssLog.str("");
    ssLog << "Build number : " << buildNumber;
    outputLog(ssLog.str());

    if(buildNumber > 1000 && buildNumber <= 2599)
    {
      ssBuild << analyzedDmps[j].substr(int(foundKeyword) + 15,5);
      ssBuild >> buildNumber;
      ssLog.str("");
      ssLog << "Build number : " << buildNumber;
      outputLog(ssLog.str());
      temp = analyzedDmps[j].substr(0,24) + " [B]"
          + analyzedDmps[j].substr(25,6) + "[/B]" 
          + analyzedDmps[j].substr(31,analyzedDmps[j].end() 
                              - analyzedDmps[j].begin());
    }
    else
    {
      temp = analyzedDmps[j].substr(0,24) + " [B]"
          + analyzedDmps[j].substr(25,5) + "[/B]" 
          + analyzedDmps[j].substr(30,analyzedDmps[j].end() 
                              - analyzedDmps[j].begin());
    }
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    kernelVersion.push_back(analyzedDmps[j]);

    if(buildNumber == 2600){
      int SP;
      size_t foundKeyword2;
      foundKeyword2 = analyzedDmps[j].find("Service Pack 1");
      if (foundKeyword2 != std::string::npos){
        SP = 1;
      }
      foundKeyword2 = analyzedDmps[j].find("Service Pack 2");
      if (foundKeyword2 != std::string::npos){
        SP = 2;
      }
      foundKeyword2 = analyzedDmps[j].find("Service Pack 3");
      if (foundKeyword2 != std::string::npos){
        SP = 3;
      }
      if(SP == 1){
        if(!IDTurnOffBBCode && !IDNoBBCodeCodeBoxes){
          temp = "[B][COLOR=RED]Missing Windows XP Service Pack 2 and 3[/COLOR][/B]";
          temp2 = "Missing Windows XP Service Pack 2 and 3";
        }
        else{
          temp2 = "Missing Windows XP Service Pack 2 and 3";
        }
      }
      else if(SP == 2){
        if(!IDTurnOffBBCode && !IDNoBBCodeCodeBoxes){
          temp = "[B][COLOR=RED]Missing Windows XP Service Pack 3[/COLOR][/B]";
          temp2 = "Missing Windows XP Service Pack 3";
        }
        else{
          temp2 = "Missing Windows XP Service Pack 3";
        }
      }
      else if(SP == 3){
      }
      else{
        if(!IDTurnOffBBCode && !IDNoBBCodeCodeBoxes){
          temp = "[B][COLOR=RED]Missing Windows XP Service Pack 1, 2, and 3[/COLOR][/B]";
          temp2 = "Missing Windows XP Service Pack 1, 2, and 3";
        }
        else{
          temp2 = "Missing Windows XP Service Pack 1, 2, and 3";
        }
      }
      importantInfo.push_back(temp);
      importantInfoNon.push_back(temp2);
      ninetyEight.push_back(temp);
      missingServicePack.push_back(temp);
      OSVersion = 0;
      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
    }

    if(buildNumber == 6000){
      if(!IDTurnOffBBCode && !IDNoBBCodeCodeBoxes){
        temp = "[B][COLOR=RED]Missing Windows Vista Service Pack 1 and 2[/COLOR][/B]";
        temp2 = "Missing Windows Vista Service Pack 1 and 2";
      }
      else{
        temp2 = "Missing Windows Vista Service Pack 1 and 2";
      }
      importantInfo.push_back(temp);
      importantInfoNon.push_back(temp2);
      ninetyEight.push_back(temp);
      missingServicePack.push_back(temp);
      OSVersion = 1;
      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
    }

    if(buildNumber == 6001){
      if(!IDTurnOffBBCode && !IDNoBBCodeCodeBoxes){
        temp = "[B][COLOR=RED]Missing Windows Vista Service Pack 2[/COLOR][/B]";
        temp2 = "Missing Windows Vista Service Pack 2";
      }
      else{
        temp2 = "Missing Windows Vista Service Pack 2";
      }
      importantInfo.push_back(temp);
      importantInfoNon.push_back(temp2);
      ninetyEight.push_back(temp);
      missingServicePack.push_back(temp);
      OSVersion = 1;
      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
    }
    
    if(buildNumber == 6002){
      OSVersion = 1;
    }

    if(buildNumber == 7600){
      if(!IDTurnOffBBCode && !IDNoBBCodeCodeBoxes){
        temp = "[B][COLOR=RED]Missing Windows 7 Service Pack 1[/COLOR][/B]";
        temp2 = "Missing Windows 7 Service Pack 1";
      }
      else{
        temp2 = "Missing Windows 7 Service Pack 1";
      }
      importantInfo.push_back(temp);
      importantInfoNon.push_back(temp2);
      ninetyEight.push_back(temp);
      missingServicePack.push_back(temp);
      OSVersion = 2;
      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
    }

    if(buildNumber == 7601){
      OSVersion = 2;
    }

    if(buildNumber >= 8400 && buildNumber <= 9363){
      OSVersion = 3;
    }

    if(buildNumber > 9363 && buildNumber <= 9800)
    {
      OSVersion = 4;
    }

    if(buildNumber > 9801 && buildNumber <= 100000)
    {
      OSVersion = 5;
    }
    ssLog.str("");
    ssLog << "OSVersion : " << OSVersion;
    outputLog(ssLog.str());
  }
}

// search for the std::string "Built By" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::builtByString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("Built by");
  // make sure the version 7600 or 7601 is easily found to check for SP1
  if (foundKeyword != std::string::npos){
    if(buildNumber > 9999)
    {
      temp = analyzedDmps[j].substr(0,9) + " [B]"
          + analyzedDmps[j].substr(10,5) + "[/B]" 
          + analyzedDmps[j].substr(15,analyzedDmps[j].end() 
                              - analyzedDmps[j].begin());
    }
    else
    {
      temp = analyzedDmps[j].substr(0,9) + " [B]"
          + analyzedDmps[j].substr(10,4) + "[/B]" 
          + analyzedDmps[j].substr(14,analyzedDmps[j].end() 
                              - analyzedDmps[j].begin());
    }
    importantInfo.push_back(temp);
    importantInfoNon.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    builtBy.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "System Uptime" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::systemUptimeString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("System Uptime");
  // Make sure the uptime is in bold so it is easy to see
  if (foundKeyword != std::string::npos){
    temp = analyzedDmps[j].substr(0,14) + "[B]"
          + analyzedDmps[j].substr(15,analyzedDmps[j].end() 
                              - analyzedDmps[j].begin()) + "[/B]";
    importantInfo.push_back(temp);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    systemUptime.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "BugCheck" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::bugCheckString(int j){
  size_t foundKeyword;
  std::string temp;
  foundKeyword = analyzedDmps[j].find("BugCheck ");
  // highlight the BugCheck and its arguments (if there are any)
  if (foundKeyword != std::string::npos && IDStack == 0){
    bugCheck.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "Unable to verify" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::unableToVerifyString(int j){
  std::string temp;
  size_t foundKeyword;
  // Unable to verify lines may indicate a driver in the stack causing the crash
  foundKeyword = analyzedDmps[j].find("Unable to verify");
  if (foundKeyword != std::string::npos){
    temp = analyzedDmps[j];
    importantInfo.push_back(temp);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    if (m_sUnableToVerify == "")
    {
      unableToVerify.push_back(analyzedDmps[j]);
      m_sUnableToVerify = analyzedDmps[j];
    }
    else
    {
      int idx = unableToVerify.size() - 1;
      unableToVerify[idx] += "\n" + analyzedDmps[j];
      m_sUnableToVerify += "\n" + analyzedDmps[j];
    }
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "symbols could not be loaded" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::symbolsNotLoadedString(int j){
  std::string temp;
  size_t foundKeyword;
  // symbols could not be loaded lines may indicate a driver in the stack causing the crash
  foundKeyword = analyzedDmps[j].find("symbols could not be loaded");
  if (foundKeyword != std::string::npos){
    temp = analyzedDmps[j];
    importantInfo.push_back(temp);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    if (m_sSymbolsNotLoaded == "")
    {
      symbolsNotLoaded.push_back(temp);
      m_sSymbolsNotLoaded = temp;
    }
    else
    {
      int idx = symbolsNotLoaded.size() - 1;
      symbolsNotLoaded[idx] += "\n" + temp;
      m_sSymbolsNotLoaded += "\n" + temp;
    }
    ninetyEight.push_back(temp);
  }
}

// search for the std::string "Probably caused by" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::probablyCausedByString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("Probably caused by");
  if (foundKeyword != std::string::npos && !m_bProbablyCausedBy){
    m_bProbablyCausedBy = true;
    probablyCausedBy.push_back(analyzedDmps[j]);
    if(probablyCausedBy.size()==probablyCausedBy.size()){
      int j1 = probablyCausedBy.size() - 1;
      temp = probablyCausedBy[j1].substr(0,20) + "[B]"
                                               + probablyCausedBy[j1].substr(21,probablyCausedBy[j1].end()
                                                                                               - probablyCausedBy[j1].begin())
            + "[/B]";
      importantInfo.push_back(temp);
      importantInfoNon.push_back(probablyCausedBy[j1]);
      ninetyEight.push_back(probablyCausedBy[j1]);
      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
    }
  }
  foundKeyword = analyzedDmps[j].find("SYMBOL_NAME");
  if (foundKeyword != std::string::npos && !m_bProbablyCausedBy)
  {
    m_sSymbolName = analyzedDmps[j].substr(14,analyzedDmps[j].end()
                        - analyzedDmps[j].begin());
    m_sSymbolName = " ( " + m_sSymbolName + " )";
  }
  foundKeyword = analyzedDmps[j].find("IMAGE_NAME");
  if (foundKeyword != std::string::npos && !m_bProbablyCausedBy){
    m_bProbablyCausedBy = true;
    if(probablyCausedBy.size()==probablyCausedBy.size()){
      temp = "Probably caused by: [B]"
            + analyzedDmps[j].substr(13,analyzedDmps[j].end()
                        - analyzedDmps[j].begin())
                        + m_sSymbolName 
            + "[/B]";
      importantInfo.push_back(temp);
      temp = "Probably caused by: "
            + analyzedDmps[j].substr(13,analyzedDmps[j].end()
                        - analyzedDmps[j].begin())
                        + m_sSymbolName;
      probablyCausedBy.push_back(temp);
      importantInfoNon.push_back(temp);
      ninetyEight.push_back(temp);
      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
    }
  }
}

// search for the std::string "BUGCHECK_STR" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::bugcheckStrString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("BUGCHECK_STR");
  if (foundKeyword != std::string::npos){
    temp = analyzedDmps[j];
    importantInfo.push_back(temp);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    bugcheckStr.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
    if(defaultBucketID.size()==bugcheckStr.size()){
      int j1 = defaultBucketID.size() - 1;
      // Check for Verifier enabled in this line, and if it is found, highlight it in red
      foundKeyword = defaultBucketID[j1].find("VERIFIER");
      if (foundKeyword != std::string::npos){
        temp = defaultBucketID[j1].substr(0,19) + "[B][COLOR=RED]"
              + defaultBucketID[j1].substr(20,defaultBucketID[j1].end() 
                          - defaultBucketID[j1].begin()) 
              + "[/COLOR][/B]";
        importantInfo.push_back(temp);
        importantInfoNon.push_back(defaultBucketID[j1]);
        if(IDAddSpaces){
          importantInfo.push_back(" ");
          importantInfoNon.push_back(" ");
        }
        ninetyEight.push_back(defaultBucketID[j1]);
      }
      else{
        importantInfo.push_back(defaultBucketID[j1]);
        importantInfoNon.push_back(defaultBucketID[j1]);
        if(IDAddSpaces){
          importantInfo.push_back(" ");
          importantInfoNon.push_back(" ");
        }
        ninetyEight.push_back(defaultBucketID[j1]);
      }
    }
  }
}

// search for the std::string "DEFAULT_BUCKET_ID" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::defaultBucketIDString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("DEFAULT_BUCKET_ID");
  if (foundKeyword != std::string::npos){
    defaultBucketID.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "verifier" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::verifierStringOne(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("VERIFIER_DETECTED");
  if (foundKeyword != std::string::npos){
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    verifierOne.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "verifier" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::verifierStringTwo(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("VERIFIER_ENABLED");
  if (foundKeyword != std::string::npos){
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    verifierTwo.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "*                        Bugcheck Analysis                                    *" 
//    and add the line to the importantInfo std::vector std::string if it is among the output items asked for by the user
void InputData::bugCheckAnalysisString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("*                        Bugcheck Analysis                                    *");
  if (foundKeyword != std::string::npos){
    foundKeyword = analyzedDmps[j+4].find("Use !analyze -v to get detailed debugging information.");
    if (foundKeyword != std::string::npos){
    }
    // highlight the BugCheck info line
    else{      
      std::string bugCheckLink1 = "";
      int s1 = int(analyzedDmps[j+4].find("("));
      int f1 = int(analyzedDmps[j+4].find(")"));
      int foundIt = 0;
      for (int i1 = 0; i1<int(bcd.bugCheckCode.size()); i1++) {
          if (StringUtil::contains(StringUtil::toLower(analyzedDmps[j + 4]), StringUtil::toLower(bcd.bugCheckCode[i1])))
          {
              bugCheckLink1 = bcd.bugCheckLink[i1];
              foundIt++;
              break;
          }
      }
      if (!foundIt)
      {
          for (int i1 = 0; i1<int(bcd.bugCheckCode.size()); i1++)
          {
              std::string hexStr = "0x" + analyzedDmps[j + 4].substr(s1 + 1, f1 - 1 - s1);
              if (StringUtil::contains(StringUtil::toLower(bcd.bugCheckLink[i1]), StringUtil::toLower(hexStr))) {
                  bugCheckLink1 = bcd.bugCheckLink[i1];
                  foundIt++;
                  break;
              }
          }
      }
      if (bugCheckLink1.length() < 3)
      {
          bugCheckLink1 = "https://learn.microsoft.com/en-us/windows-hardware/drivers/debugger/bug-check-code-reference2";
      }
      
      temp = "BugCheck Info: [url=" + bugCheckLink1 + "]" + analyzedDmps[j+4] + "[/url]";
      bugCheckAnalysis.push_back(analyzedDmps[j+4]);
      if(bugCheck.size()==bugCheckAnalysis.size()){
        int j1 = bugCheck.size() - 1;
        std::string temp2 = "BugCheck [B]" + bugCheck[j1].substr(9,bugCheck[j1].length()-9) + "[/B]";
        importantInfo.push_back(temp2);
        temp2 = "BugCheck " + bugCheck[j1].substr(9,bugCheck[j1].length()-9);
        importantInfoNon.push_back(temp2);
        ninetyEight.push_back(temp2);
      }
      importantInfoNon.push_back(temp);
      ninetyEight.push_back(temp);
      importantInfo.push_back(temp);

      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
      int startReading = 0;
      std::stringstream sstemp;
      for(int i = 0; i < int(analyzedDmps[j+4].length()); i++){
        if(analyzedDmps[j+4].c_str()[i] == ')'){
          startReading = 0;
        }
        if(startReading){
          sstemp << analyzedDmps[j+4].c_str()[i];
        }
        if(analyzedDmps[j+4].c_str()[i] == '('){
          startReading = 1;
        }
      }
      sstemp >> temp;
      temp[temp.length()-1] = toupper(temp[temp.length() - 1]);
      int sizeTemp = temp.length();
      for (int i = 0; i < int(8 - sizeTemp); i++){
        temp = "0" + temp;
      }
      if(IDBugcheckCode){
        temp = "Bugcheck code " + temp;
        bugcheckCode.push_back(temp);
        ninetyEight.push_back(temp);
      }
    }
  }
}

// search for the std::string "PROCESS_NAME" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::processNameString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("PROCESS_NAME");
  if (foundKeyword != std::string::npos){
    // highlight the process name
    temp = analyzedDmps[j];
    importantInfo.push_back(temp);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    processName.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "FAILURE_BUCKET_ID" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::failureBucketIDString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("FAILURE_BUCKET_ID");
  // highlight the failure ID (sometimes it includes the driver at fault)
  if (foundKeyword != std::string::npos){
    temp = analyzedDmps[j].substr(0,19) + "[B]"
          + analyzedDmps[j].substr(20,analyzedDmps[j].end() 
                              - analyzedDmps[j].begin()) 
          + "[/B]";
    importantInfo.push_back(temp);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    failureBucketID.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// Get the arguments of the BugCheck code
void InputData::errorCodeString(int j){
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("Error Code");
  if (foundKeyword != std::string::npos){    
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    errorCode.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// Get the arguments of the BugCheck code
void InputData::diskHardwareErrorString(int j){
  size_t foundKeyword;
  // Arguments are saved as Arg1, Arg2, Arg3, Arg4...
  foundKeyword = analyzedDmps[j].find("DISK_HARDWARE_ERROR");
  if (foundKeyword != std::string::npos){    
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    diskHardwareError.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// Get the arguments of the BugCheck code
void InputData::argumentsString(int j){
  size_t foundKeyword;
  // Arguments are saved as Arg1, Arg2, Arg3, Arg4...
  foundKeyword = analyzedDmps[j].find("Arg1:");
  if (foundKeyword != std::string::npos){
    std::string temp = "Arguments: \n";
    foundKeyword = analyzedDmps[j].find("Arg2:");
    int argsCount2 = 0;
    while(!(foundKeyword != std::string::npos)){
      if(argsCount2 == 0){
        temp = temp + analyzedDmps[j] + "\n";
      }
      else{
        temp = temp + analyzedDmps[j] + "\n";
      }
      j++;
      argsCount2++;
      foundKeyword = analyzedDmps[j].find("Arg2:");
    }
    int argsCount = 1;
    while(argsCount){
      foundKeyword = analyzedDmps[j].find("Arg2:");
      if (foundKeyword != std::string::npos){
        foundKeyword = analyzedDmps[j].find("Arg3:");
        argsCount2 = 0;
        while(!(foundKeyword != std::string::npos)){
          if(argsCount2 == 0){
            temp = temp + analyzedDmps[j] + "\n";
          }
          else{
            temp = temp + analyzedDmps[j] + "\n";
          }
          j++;
          argsCount2++;
          foundKeyword = analyzedDmps[j].find("Arg3:");
        }
      }
      foundKeyword = analyzedDmps[j].find("Arg3:");
      if (foundKeyword != std::string::npos){
        foundKeyword = analyzedDmps[j].find("Arg4:");
        argsCount2 = 0;
        while(!(foundKeyword != std::string::npos)){
          if(argsCount2 == 0){
            temp = temp + analyzedDmps[j] + "\n";
          }
          else{
            temp = temp + analyzedDmps[j] + "\n";
          }
          j++;
          argsCount2++;
          foundKeyword = analyzedDmps[j].find("Arg4:");
        }
      }
      foundKeyword = analyzedDmps[j].find("Arg4:");
      if (foundKeyword != std::string::npos){
        argsCount2 = 0;
        while(int(analyzedDmps[j].length()) > 1){
          if(argsCount2 == 0){
            temp = temp + analyzedDmps[j];
          }
          else{
            temp = temp + "\n" + analyzedDmps[j];
          }
          j++;
          argsCount2++;
        }
        argsCount = 0;
      }
      j++;
    }    
    arguments.push_back(temp);
    ninetyEight.push_back(temp);
    importantInfo.push_back(temp);
    importantInfoNon.push_back(temp);

  }
}

// search for the std::string "CPUID" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::CPUIDString(int j){        
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("CPUID:");
  if (foundKeyword != std::string::npos){
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    CPUID.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "MaxSpeed" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::maxSpeedString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("MaxSpeed:");
  if (foundKeyword != std::string::npos){
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    maxSpeed.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
  }
}

// search for the std::string "CurrentSpeed" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::currentSpeedString(int j){
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("CurrentSpeed:");
  if (foundKeyword != std::string::npos){
    temp = analyzedDmps[j].substr(0,13) + " [B]"
          + analyzedDmps[j].substr(14,analyzedDmps[j].end() 
                              - analyzedDmps[j].begin()) 
          + "[/B]";
    importantInfo.push_back(temp);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    currentSpeed.push_back(analyzedDmps[j]);
    ninetyEight.push_back(analyzedDmps[j]);
    double maxSpeedOver;
    double currentSpeedOver;
    std::stringstream sstemp;
    sstemp << maxSpeed[int(maxSpeed.size()) - 1].substr(14,4);
    sstemp >> maxSpeedOver;
    std::stringstream sstemp2;
    sstemp2 << currentSpeed[int(currentSpeed.size()) - 1].substr(14,4);
    sstemp2 >> currentSpeedOver;
    if(IDOverclock && currentSpeedOver - maxSpeedOver > 100.0){
      std::stringstream expectedFrequency;
      std::stringstream actualFrequency;
      std::stringstream overclockRatio;
      std::stringstream overclockRatio2;
      std::stringstream overclockRatio3;
      expectedFrequency << "Expected Frequency:   " << int(maxSpeedOver); 
      actualFrequency   << "Actual Frequency:     " << int(currentSpeedOver);
      overclockRatio    << "Overclock Ratio:      ";
      overclockRatio2   << "Overclock Ratio:      ";
      overclockRatio3   << "Overclock Ratio:      ";
      if(!IDTurnOffBBCode && !IDNoBBCodeCodeBoxes){
        overclockRatio << "[color=red]";
      }
      overclockRatio3 << "[color=red]";
      overclockRatio << currentSpeedOver/maxSpeedOver;
      overclockRatio2 << currentSpeedOver/maxSpeedOver;
      overclockRatio3 << currentSpeedOver/maxSpeedOver;
      overclockRatio3 << "[/color]";
      if(!IDTurnOffBBCode && !IDNoBBCodeCodeBoxes){
        overclockRatio << "[/color]";
      }
      importantInfo.push_back(" ");
      importantInfo.push_back("Processor may be overclocked!");
      importantInfo.push_back(expectedFrequency.str());
      importantInfo.push_back(actualFrequency.str());
      importantInfo.push_back(overclockRatio3.str());
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
      importantInfoNon.push_back("Processor may be overclocked!");
      importantInfoNon.push_back(expectedFrequency.str());
      importantInfoNon.push_back(actualFrequency.str());
      importantInfoNon.push_back(overclockRatio2.str());
      importantInfoNon.push_back(" ");
      ninetyEight.push_back(" ");
      ninetyEight.push_back("Processor may be overclocked!");
      ninetyEight.push_back(expectedFrequency.str());
      ninetyEight.push_back(actualFrequency.str());
      ninetyEight.push_back(overclockRatio.str());
      ninetyEight.push_back(" ");
      overclock.push_back(" ");
      overclock.push_back("Processor may be overclocked!");
      overclock.push_back(expectedFrequency.str());
      overclock.push_back(actualFrequency.str());
      overclock.push_back(overclockRatio.str());
      overclock.push_back(" ");
    }
  }
}

// search for the std::string "BIOS Release Date" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::BIOSReleaseDateString(int j){        
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("BIOS Release Date");
  if (foundKeyword != std::string::npos){
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    temp = "BiosReleaseDate = " + analyzedDmps[j].substr(32,10);
    biosReleaseDate.push_back(temp);
    ninetyEight.push_back(temp);
  }
}

// search for the std::string "BIOS Version" and add the line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::BIOSVersionString(int j){        
  std::string temp;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("BIOS Version");
  if (foundKeyword != std::string::npos){
    importantInfo.push_back(analyzedDmps[j]);
    importantInfoNon.push_back(analyzedDmps[j]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    temp = "BiosVersion = " + analyzedDmps[j].substr(32,analyzedDmps[j].length() - 32);
    biosVersion.push_back(temp);
    ninetyEight.push_back(temp);
  }
}

// search for the std::string "System Information (Type 1)" and add the Manufacturer line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::systemManufacturerString(int j){
  std::string temp, temp2;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("System Information (Type 1)");
  if (foundKeyword != std::string::npos){
    importantInfo.push_back(analyzedDmps[j+1]);
    importantInfoNon.push_back(analyzedDmps[j+1]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    temp = "SystemManufacturer = " + analyzedDmps[j+1].substr(32,analyzedDmps[j+1].length() - 32);
    systemManufacturer.push_back(temp);
    ninetyEight.push_back(temp);
  }
  if(j+9 >= int(analyzedDmps.size()))
  {
    return;
  }
  foundKeyword = analyzedDmps[j+9].find("BaseBoard Information (Type 2)");
  if (foundKeyword != std::string::npos){
    temp2 = "SystemManufacturer = " + analyzedDmps[j+10].substr(32,analyzedDmps[j+10].length() - 32);
    if(compare->unSignedCompare(temp,temp2))
    {
    }
    else
    {
      importantInfo.push_back("  Baseboard Manufacturer" + analyzedDmps[j+10].substr(24,analyzedDmps[j+10].length() - 24));
      importantInfoNon.push_back("  Baseboard Manufacturer" + analyzedDmps[j+10].substr(24,analyzedDmps[j+10].length() - 24));
      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
      temp = "BaseBoardManufacturer = " + analyzedDmps[j+10].substr(32,analyzedDmps[j+10].length() - 32);
      systemManufacturer.push_back(temp);
      ninetyEight.push_back(temp);
    }
  }
}

// search for the std::string "System Information (Type 1)" and add the Product Name line to the importantInfo std::vector std::string
//    if it is among the output items asked for by the user
void InputData::systemProductNameString(int j){
  std::string temp, temp2;
  size_t foundKeyword;
  foundKeyword = analyzedDmps[j].find("System Information (Type 1)");
  if (foundKeyword != std::string::npos){
    importantInfo.push_back(analyzedDmps[j+2]);
    importantInfoNon.push_back(analyzedDmps[j+2]);
    if(IDAddSpaces){
      importantInfo.push_back(" ");
      importantInfoNon.push_back(" ");
    }
    temp = "SystemProductName = " + analyzedDmps[j+2].substr(32,analyzedDmps[j+2].length() - 32);
    systemProductName.push_back(temp);
    ninetyEight.push_back(temp);
  }
  if(j+9 >= int(analyzedDmps.size()))
  {
    return;
  }
  foundKeyword = analyzedDmps[j+9].find("BaseBoard Information (Type 2)");
  if (foundKeyword != std::string::npos){
    temp2 = "SystemProductName = " + analyzedDmps[j+11].substr(32,analyzedDmps[j+11].length() - 32);
    if(compare->unSignedCompare(temp,temp2))
    {
    }
    else
    {
      importantInfo.push_back("  Baseboard Product" + analyzedDmps[j+11].substr(19,analyzedDmps[j+11].length() - 19));
      importantInfoNon.push_back("  Baseboard Product" + analyzedDmps[j+11].substr(19,analyzedDmps[j+11].length() - 19));
      if(IDAddSpaces){
        importantInfo.push_back(" ");
        importantInfoNon.push_back(" ");
      }
      temp = "BaseBoardProduct = " + analyzedDmps[j+11].substr(32,analyzedDmps[j+11].length() - 32);
      systemProductName.push_back(temp);
      ninetyEight.push_back(temp);
    }
  }
}

std::vector<std::string> InputData::Split(std::string str, char c)
{
  std::vector<std::string> strs;
  std::stringstream sstream;
  for(int i = 0; i < (int)str.size(); ++i)
  {
    if (str[i] == c || i == (int)str.size() - 1)
    {
      if(i == (int)str.size() - 1 && str[i] != c)
      {
        sstream << str[i];
      }
      strs.push_back(sstream.str());
      sstream.str("");
    }
    else
    {
      sstream << str[i];
    }
  }
  if (strs.size() == 0)
  {
    strs.push_back("");
  }
  return strs;
}

std::string InputData::RemoveTrailingZeros(std::string str)
{
  bool allZeros = true;
  std::string newStr = "";
  for(int i = 0; i < (int)str.size(); ++i)
  {
    if(str[i] != '0' || !allZeros)
    {// Skip preceding zeros and check if all argument characters are zeros
      newStr += str[i];
      allZeros = false;
    }
  }
  if (allZeros)
  {
    return "0";
  }
  return newStr;
}

std::string InputData::GetBugCheckFromAnalysis(std::string bugCheckAnalysisStr, std::string argsStr)
{
  int s1 = int(bugCheckAnalysisStr.find("("));
  int f1 = int(bugCheckAnalysisStr.find(")"));
  std::string bugCheckNumberStr = bugCheckAnalysisStr.substr(s1+1,f1-1-s1);
  std::transform(bugCheckNumberStr.begin(), bugCheckNumberStr.end(), 
    bugCheckNumberStr.begin(), ::toupper);
  std::vector<std::string> args;
  
  std::vector<std::string> argsStrSplit = Split(argsStr, '\n');
  for(int i = 0; i < (int)argsStrSplit.size(); ++i)
  {
    int idx = args.size() + 1;
    std::stringstream ssFind;
    ssFind << "Arg" << idx;
    if(argsStrSplit[i].find(ssFind.str()) != std::string::npos)
    {
      args.push_back(Split(argsStrSplit[i], ' ')[1]);
      idx = args.size() - 1;
      args[idx] = Split(args[idx], ',')[0];
      args[idx] = RemoveTrailingZeros(args[idx]);
      if(idx < 3)
      {
        args[idx] += ", ";
      }
    }
  }
  std::string bugCheckStr = "BugCheck " + bugCheckNumberStr + ", {" + args[0] + args[1] + args[2] + args[3] + "}";
  return bugCheckStr;
}

// Fill in info for the _98-dbug.txt file
void InputData::fillInNinetyEight(int i){
  if(IDLoadingDumpFile && int(loadingDumpFile.size()) < i){
    loadingDumpFile.push_back("Loading Dump File... NOT FOUND");
  }
  if(IDKernelVersion && int(kernelVersion.size()) < i){
    kernelVersion.push_back("Kernel Version NOT FOUND");
  }
  if(IDBuiltBy && int(builtBy.size()) < i){
    builtBy.push_back("Built by: NOT FOUND");
  }
  if(IDDebugSessionTime && int(debugSessionTime.size()) < i){
    debugSessionTime.push_back("Debug session time: NOT FOUND");
  }
  if(IDSystemUptime && int(systemUptime.size()) < i){
    systemUptime.push_back("System Uptime: NOT FOUND");
  }
  if(IDBugCheck && int(bugCheck.size()) < i){
    if((int)bugCheckAnalysis.size() >= i && (int)arguments.size() >= i)
    {      
      bugCheck.push_back(GetBugCheckFromAnalysis(bugCheckAnalysis[bugCheckAnalysis.size() -1], arguments[arguments.size() - 1]));
    }
    else
    {
      bugCheck.push_back("Bugcheck NOT FOUND");
    }
  }
  if(IDUnableToVerify && int(unableToVerify.size()) < i){
    unableToVerify.push_back("empty");
  }
  if(IDSymbolsNotLoaded && int(symbolsNotLoaded.size()) < i){
    symbolsNotLoaded.push_back("empty");
  }
  if(IDProbablyCausedBy && int(probablyCausedBy.size()) < i){
    probablyCausedBy.push_back("Probably caused by : NOT FOUND");
  }
  if(IDDefaultBucketID && int(defaultBucketID.size()) < i){
    defaultBucketID.push_back("DEFAULT_BUCKET_ID: NOT FOUND");
  }
  if(IDVerifier && int(verifierOne.size()) < i){
    verifierOne.push_back(" ");
  }
  if(IDVerifier && int(verifierTwo.size()) < i){
    verifierTwo.push_back(" ");
  }
  if(IDBugcheckStr && int(bugcheckStr.size()) < i){
    bugcheckStr.push_back("BUGCHECK_STR: NOT FOUND");
  }
  if(IDProcessName && int(processName.size()) < i){
    processName.push_back("PROCESS_NAME: NOT FOUND");
  }
  if(IDFailureBucketID && int(failureBucketID.size()) < i){
    failureBucketID.push_back("FAILURE_BUCKET_ID: NOT FOUND");
  }
  if(IDBugcheckCode && int(bugcheckCode.size()) < i){
    bugcheckCode.push_back("Bugcheck Code NOT FOUND");
  }
  if(IDBugCheckAnalysis && int(bugCheckAnalysis.size()) < i){
    bugCheckAnalysis.push_back("Bugcheck Analysis NOT FOUND");
  }
  if(IDErrorCode && int(errorCode.size()) < i){
    errorCode.push_back(" ");
  }
  if(IDDiskHardwareError && int(diskHardwareError.size()) < i){
    diskHardwareError.push_back(" ");
  }
  if(IDArguments && int(arguments.size()) < i){
    arguments.push_back("Arguments NOT FOUND");
  }
  if(IDBIOSVersion && int(biosVersion.size()) < i){
    biosVersion.push_back("BiosVersion = NOT FOUND");
  }
  if(IDBIOSReleaseDate && int(biosReleaseDate.size()) < i){
    biosReleaseDate.push_back("BiosReleaseDate = NOT FOUND");
  }
  if(IDSystemManufacturer && int(systemManufacturer.size()) < i){
    systemManufacturer.push_back("SystemManufacturer = NOT FOUND");
  }
  if(IDSystemProductName && int(systemProductName.size()) < i){
    systemProductName.push_back("SystemProductName = NOT FOUND");
  }
  if(IDCPUID && int(CPUID.size()) < i){
    CPUID.push_back("CPUID: NOT FOUND");
  }
  if(IDMaxSpeed && int(maxSpeed.size()) < i){
    maxSpeed.push_back("MaxSpeed: NOT FOUND");
  }
  if(IDCurrentSpeed && int(currentSpeed.size()) < i){
    currentSpeed.push_back("CurrentSpeed: NOT FOUND");
  }
  ninetyEight.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
}

// Get the stack trace output
void InputData::getStack(int j){
  size_t foundKeyword;
  // stack trace information starts after "STACK_TEXT" output
  foundKeyword = analyzedDmps[j].find("STACK_TEXT");
  if (foundKeyword != std::string::npos){
    std::ofstream outfile2;
    IDStack = 1;
  }
  // the stack trace can end with the STACK_COMMAND line
  foundKeyword = analyzedDmps[j].find("STACK_COMMAND");
  if (foundKeyword != std::string::npos){
    IDStack = 0;
  }
  // the stack trace can end with the SYMBOL_STACK_INDEX line
  foundKeyword = analyzedDmps[j].find("SYMBOL_STACK_INDEX");
  if (foundKeyword != std::string::npos){
    IDStack = 0;
  }  
  // the stack trace can end with the FOLLOWUP_IP line      
  foundKeyword = analyzedDmps[j].find("FOLLOWUP_IP");
  if (foundKeyword != std::string::npos){
    IDStack = 0;
  }  
  // the stack trace can end with the WARNING: Frame IP line      
  foundKeyword = analyzedDmps[j].find("WARNING: Frame IP");
  if (foundKeyword != std::string::npos){
    IDStack = 0;
  }
  // output to the stack.txt analyzedStack std::vector std::string
  if (IDStack == 1){
    analyzedStack.push_back(analyzedDmps[j]);
  }
}

// Get the SMBIOS output
void InputData::getSMBIOS(int j){
  size_t foundKeyword;
  // The SMBIOS output starts with the SMBIOS Data Tables line
  foundKeyword = analyzedDmps[j].find("SMBIOS Data Tables");
  if (foundKeyword != std::string::npos){
    IDSMBIOS = 1;
  }
  // The SMBIOS information ends when the module list starts
  foundKeyword = analyzedDmps[j].find("module name");
  if (foundKeyword != std::string::npos){
    IDSMBIOS = 0;
  }
  // output to SMBIOS.txt via the analyzedBIOS std::vector std::string
  if (IDSMBIOS == 1){
    analyzedBIOS.push_back(analyzedDmps[j]);
  }
}

void InputData::getDriverInfo(){
  std::stringstream outfile;
  outfile << "getDriverInfo" << std::endl;
  // get the maximum driver name length including its extension
  IDMaxDriver = 0;
  // get the maximum driver name length
  IDMaxName = 0;
  // determine whether the loaded modules list has started
  int countMod = 0;
  // determine whether the unloaded modules list has started
  int unloaded = 0;
  // keep track of which .dmp the driver list came from
  int l = -1;
  int l2 = 0;
  int l3 = 0;
  int dmpNum = 0;
  bool reproducibleBuildFileHash;
  for(int i = 0; i < int(analyzedDmps.size()); i++){
    reproducibleBuildFileHash = false;
    size_t foundKeyword;
    // Does the item contain module name in its name
    foundKeyword = analyzedDmps[i].find("module name");

    size_t foundDmp;
    foundDmp = analyzedDmps[i].find("Loading Dump File");
    if (foundDmp != std::string::npos){
      dmpNum++;
    }
    size_t foundDebugTime;
    foundDebugTime = analyzedDmps[i].find("Debug session time");
    if(foundDebugTime != std::string::npos)
    {
      l++;
    }
    size_t reproducibleHash = analyzedDmps[i].find("This is a reproducible build file hash");
    if(reproducibleHash != std::string::npos)
    {
      reproducibleBuildFileHash = true;
    }
    bool invalidTime = false;
    size_t invalidString = analyzedDmps[i].find("Invalid");
    if(invalidString != std::string::npos)
    {
      invalidTime = true;
    }
    // If so, next line start reading driver info
    if (foundKeyword != std::string::npos){
      countMod++;
      i++;
      reproducibleHash = analyzedDmps[i].find("This is a reproducible build file hash");
      if(reproducibleHash != std::string::npos)
      {
        reproducibleBuildFileHash = true;
      }
      invalidString = analyzedDmps[i].find("Invalid");
      if(invalidString != std::string::npos)
      {
        invalidTime = true;
      }
      // if a previous driver list ended from a previous .dmp, 
      //    separate the current list from the previous list
      if(l > l2){
        l2++;
        IDMemStart.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
        IDMemEnd.push_back(" ");
        IDName.push_back(" ");
        IDDriver.push_back(" ");
        pDriver.push_back(" ");
        IDDmpNum.push_back(dmpNum-1);
        pDmpNum.push_back(dmpNum-1);
        IDDayname.push_back(" ");
        IDMonths.push_back(" ");
        pMonths.push_back(" ");
        IDDay.push_back(0);
        IDTime.push_back(" ");
        pTime.push_back(" ");
        IDYear.push_back(0);
        pYear.push_back(0);
        IDTimestamp.push_back(" ");
        unloadedDrivers.push_back("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
        pDmpTime.push_back(l);
      }
      if(l == l3)
      {
        l3++;
        // Add a header to each driver list indicating which .dmp it came from
        std::string temp = "**************************" + timeDmp[l] + "**************************";
        IDMemStart.push_back(temp);
        IDMemEnd.push_back(" ");
        IDName.push_back(" ");
        IDDriver.push_back(" ");
        pDriver.push_back(" ");      
        IDDmpNum.push_back(dmpNum-1);  
        pDmpNum.push_back(dmpNum-1);
        IDDayname.push_back(" ");
        IDMonths.push_back(" ");
        pMonths.push_back(" ");
        IDDay.push_back(0);
        IDTime.push_back(" ");
        pTime.push_back(" ");
        IDYear.push_back(0);
        pYear.push_back(0);
        IDTimestamp.push_back(" ");
        unloadedDrivers.push_back(temp);
        pDmpTime.push_back(l);
      }
    }
    // The next section checks for drivers that are not listed properly
    // create a std::stringstream variable for easier input and output
    std::stringstream sstemp;
    // copy the drivers list line into the std::stringstream variable
    sstemp << analyzedDmps[i];
    if(countMod > 0){
      std::string temp;
      // get the starting memory std::string
      sstemp >> temp;
      size_t found2;
      // make sure the starting memory is a normal memory std::string
      found2 = temp.find("__");
      if (found2 != std::string::npos){
        // if the starting memory std::string is corrupted, do not proceed with getting driver information
        //    from this line
        countMod = 0;
      }
      // if the loaded modules list has ended, stop getting driver information for this .dmp file
      if(temp == "Unloaded" || temp == "unloaded" 
      || temp == "\n" || temp == "\r"
      || temp == " " || temp == "Mini" 
      || temp == "mini" || temp == "Missing"
      || analyzedDmps[i].find("quit") != std::string::npos 
      || temp.length() < 5){
        countMod = 0;
        unloaded++;
      }
      // if the starting memory std::string makes sense, store it in the IDMemStart std::vector std::string
      if(countMod > 0){
        IDMemStart.push_back(temp);
      }
    }
    // check for the end of the unloaded module list
    if(unloaded > 0){
      size_t found2;
      // the last line of unloaded modules should be the quit command for kd.exe
      found2 = analyzedDmps[i].find("quit");
      if (found2 != std::string::npos){
        unloaded = 0;
      }
      // if the unloaded modules list has not ended, contine writing it to 
      //    unloadedDrivers.txt via the unloadedDrivers std::vector std::string
      unloadedDrivers.push_back(analyzedDmps[i]);
    }
    // if the loaded modules list is being read
    if(countMod > 0){
      std::string temp;
      int tempint;
      // read the ending memory for the current loaded module
      sstemp >> temp;
      IDMemEnd.push_back(temp);
      size_t found2;
      // if the ending memory is corrupted, do nothing?
      //    ...not really sure this segment even does anything
      found2 = temp.find("__");
      if (found2 != std::string::npos){
        countMod = 0;
      }
      // read the driver name for the current loaded module
      sstemp >> temp;
      IDName.push_back(temp);
      found2 = temp.find("__");
      // if the name is corrupted, mark it as Unknown
      if (found2 != std::string::npos){
        IDName[IDName.size()-1] = "Unknown";
      }
      // if the name is not corrupted or unknown
      if(IDName[IDName.size()-1] != "Unknown" && IDName[IDName.size()-1] != "unknown")
      {
        // read the full driver name and extension for the current loaded module
        sstemp >> temp;
        // This is failing because name = vpnva64_6 and the full name is vpnva64-6.sys
        // if(temp.find(IDName[int(IDName.size())-1]) != std::string::npos)
        if(1)
        {
          IDDriver.push_back(temp);
          pDriver.push_back(temp);
          IDDmpNum.push_back(dmpNum-1);
          pDmpNum.push_back(dmpNum-1);
          pDmpTime.push_back(l);
          // make sure the driver name is not separated by an _ that 
          //    becomes a line return when read
          if(temp.end() - temp.begin() > 3){
            sstemp >> temp;
            IDDayname.push_back(temp);
          }
          else{
            // if the driver name is separated by an _ and breaks lines,
            //    reformat it correctly to prevent the app from crashing
            std::string tmpString;
            sstemp >> tmpString;
            IDDriver[IDDriver.size()-1] = IDDriver[IDDriver.size()-1] + "_" + tmpString;
            sstemp >> temp;
            IDDayname.push_back(temp);
          }
        }
        else{
          // std::ofstream outNewDebug;
          // outNewDebug.open("C:\\Users\\Public\\Documents\\debug.txt", std::ios::app);
          // outNewDebug << IDName[IDName.size()-1] << std::endl;
          // outNewDebug << bool(IDName[IDName.size()-1] != "Unknown") << std::endl;
          // outNewDebug << bool(IDName[IDName.size()-1] != "unknown") << std::endl;
          // outNewDebug.close();
          std::string temp2;
          temp2 = "Unknown";
          // treat the driver information as corrupted if the name is corrupted
          IDDriver.push_back(temp2);      
          pDriver.push_back(temp2);
          IDDmpNum.push_back(dmpNum-1);
          pDmpNum.push_back(dmpNum-1);
          pDmpTime.push_back(l);
          if(temp == "Sun" || temp == "Mon" || temp == "Tue" || temp == "Wed" || temp == "Thu" || 
            temp == "Fri" || temp == "Sat"){
            IDDayname.push_back(temp);
          }
          else{
            sstemp >> temp;
            if(temp == "Sun" || temp == "Mon" || temp == "Tue" || temp == "Wed" || temp == "Thu" || 
              temp == "Fri" || temp == "Sat"){
              IDDayname.push_back(temp);
            }
            else{
              IDDayname.push_back("Unavailable");
            }
          }
        }
      }
      else{
        // treat the driver information as corrupted if the name is corrupted
        IDDriver.push_back("Unknown");      
        pDriver.push_back("Unknown");
        IDDmpNum.push_back(dmpNum-1);
        pDmpNum.push_back(dmpNum-1);
        pDmpTime.push_back(l);
        IDDayname.push_back("Unavailable");
      }
      if(IDDayname[IDDayname.size()-1] != "Unavailable" && IDDayname[IDDayname.size()-1] != "unavailable"
        && !invalidTime){
        // if the driver info is not corrupted,
        // read the month name for the current loaded module
        sstemp >> temp;
        if (reproducibleBuildFileHash)
        {
          temp = "(This is a reproducible build file hash, not a timestamp)";
        }
        IDMonths.push_back(temp);
        pMonths.push_back(temp);
        if (!reproducibleBuildFileHash)
        {
          // read the day created for the current loaded module
          sstemp >> tempint;
          IDDay.push_back(tempint);
          // read the time created for the current loaded module
          sstemp >> temp;
          IDTime.push_back(temp);
          pTime.push_back(temp);
          // read the year created for the current loaded module
          sstemp >> tempint;
          IDYear.push_back(tempint);
          pYear.push_back(tempint);
          // read the timestamp for the current loaded module
          sstemp >> temp;
          IDTimestamp.push_back(temp);
        }
        else
        {
          IDDay.push_back(-1);
          IDTime.push_back(" ");
          pTime.push_back(" ");
          IDYear.push_back(-1);
          pYear.push_back(-1);
          IDTimestamp.push_back("(00000000)");
        }
      }
      else{
        // if the driver information is corrupted, output corrupted driver info
        IDDayname[IDDayname.size()-1] = "Unknown";
        IDMonths.push_back("   ");
        pMonths.push_back("   ");
        IDDay.push_back(0);
        IDTime.push_back("00:00:00");
        pTime.push_back("00:00:00");
        IDYear.push_back(1969);
        pYear.push_back(1969);
        IDTimestamp.push_back("(00000000)");
      }
      // obtain the maximum driver name length
      if(int(IDName[IDName.size()-1].length()) > IDMaxName){
        IDMaxName = int(IDName[IDName.size()-1].length());
      }
      // obtain the maximum driver name and extension length
      if(int(IDDriver[IDDriver.size()-1].length()) > IDMaxDriver){
        IDMaxDriver = int(IDDriver[IDDriver.size()-1].length());
      }      
      if(int(IDMemStart.size())-1 >= 0 && int(IDMemEnd.size())-1 >= 0 && int(IDName.size())-1 >= 0 && 
         int(IDDriver.size())-1 >= 0 && int(IDDayname.size())-1 >= 0 && int(IDMonths.size())-1 >= 0 && 
         int(IDDay.size())-1>= 0 && int(IDTime.size())-1 >= 0 && int(IDYear.size())-1 >= 0 && int(IDTimestamp.size())-1 >= 0){
        outfile << IDMemStart[IDMemStart.size()-1] << " ";
        outfile << IDMemEnd[IDMemEnd.size()-1] << " ";
        outfile << IDName[IDName.size()-1] << " ";
        outfile << IDDriver[IDDriver.size()-1] << " ";
        outfile << IDDayname[IDDayname.size()-1] << " ";
        outfile << IDMonths[IDMonths.size()-1] << " ";
        outfile << IDDay[IDDay.size()-1] << " ";
        outfile << IDTime[IDTime.size()-1] << " ";
        outfile << IDYear[IDYear.size()-1] << " ";
        outfile << IDTimestamp[IDTimestamp.size()-1] << " " ;
        outfile << (reproducibleBuildFileHash ? "True " : "False ") << i << std::endl;
      }
    }
  }
  outfile << IDMemStart.size() << " " << IDMemEnd.size() << " " << IDName.size() << " " << IDDriver.size() << " " << 
         IDDayname.size() << " " << IDMonths.size() << " " << IDDay.size() << " " << IDTime.size() << " " << IDYear.size() << " " << IDTimestamp.size() << std::endl;
  outputLog(outfile.str());
}

// output the full loaded driver list for each .dmp with proper formatting
//    to make it easier to read each category
void InputData::outputFullDriverList(){
  std::stringstream outfile;
  outfile << "outputFullDriverList" << std::endl;
  for(int i = 0; i < int(IDDriver.size()); i++){
    // create a std::stringstream variable for easier input from std::vector std::strings
    std::stringstream sstemp;
    // input the driver information
    sstemp << IDMemStart[i] << " " << IDMemEnd[i] << "   " << IDName[i];
    // make sure spaces are added to line up the columns of each std::vector std::string
    for(int j=0; j < IDMaxName - int(IDName[i].length()); j++){
      sstemp << " ";
    }
    // add an extra space between the driver name and driver name with extension
    sstemp << " " << IDDriver[i];
    // make sure spaces are added to line up the columns of each std::vector std::string
    for(int j=0; j < IDMaxDriver - int(IDDriver[i].length()); j++){
      sstemp << " ";
    }
    // add spaces betweeen day and date/time
    sstemp << "   " << " " << IDDayname[i]
          << " " << IDMonths[i] << " ";
    if (IDDay[i] > 0)
    {
      // make sure the single digit days line up with the double digit days
      if (IDDay[i] < 10){
        sstemp << " ";
        sstemp << IDDay[i];
      }
      else{
        sstemp << IDDay[i];
      }
      // output the time, year, and timestamp with space between
      sstemp << " " << IDTime[i] << " " << IDYear[i] << " " 
            << IDTimestamp[i];  
    }
    // save the driver information line to the fullDriverList std::vector std::string
    //    for output to drivers.txt
    fullDriverList.push_back(sstemp.str());
    outfile << sstemp.str() << std::endl;
  }
  outputLog(outfile.str());
}

// check whether the driver is a third party driver or a system driver
std::vector<int> InputData::thirdPartyCheck(std::string test, std::string userprofile, std::vector<std::string> database1, std::vector<int> databaseSystem1, int strSize){
  return compare->thirdPartyCheck(test, userprofile, database1, databaseSystem1, strSize);
}
  
// check whether the driver has already been discovered with the same timestamp from a previous .dmp run
int InputData::driverNewCheck(std::string test1, std::string test2, std::vector<std::string> driverData, std::vector<std::string> timestampData, int countDmps, int strSize){
  return compare->driverNewCheck(test1, test2, driverData, timestampData, countDmps, strSize);
}

// 
void InputData::outputThirdPartyNames(){
  std::stringstream outfile;
  outfile << "outputThirdPartyNames" << std::endl;
  // store drivers that have already been found
  std::vector<std::string> driverStore;
  // also store those driver timestamps to check if the driver has been updated
  std::vector<std::string> timestampStore;
  // keep track of whether a new .dmp is being analyzed
  //    this prevent separators from being added for .dmps that do not have new
  //    driver information
  
  thirdi = 0;
  int countDmps = 0;
  thirdMaxLength = 0;
  clock_t t01, t02;
  t01 = clock();
  for(int i=0; i < int(IDDriver.size()); i++){
    size_t found1;
    found1 = IDMemStart[i].find("**************************");
    // if the first line of this set of drivers is found
    if(found1 != std::string::npos){
      // keep track of where the set of drivers begins
      i1.push_back(thirdi + 1);
      thirdBegin.push_back(IDMemStart[i]);
      thirdEnd.push_back(" ");
      thirdDriver.push_back(" ");
      thirdDayname.push_back(" ");
      thirdMonths.push_back(" ");
      thirdTime.push_back(" ");
      thirdTimestamp.push_back(" ");
      thirdDay.push_back(0);
      thirdYear.push_back(0);
      thirdThird.push_back(0);
      thirdDRT.push_back(0);
      thirdIsItNew.push_back(0);
      thirdIsItNew2.push_back(0);
      outfile << IDMemStart[i] << std::endl;
      thirdi++;
    }
    // if the last line of this set of drivers is found
    size_t found2;
    found2 = IDMemStart[i].find("ииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии``");
    if(found2 != std::string::npos){
      // keep track of where the set of drivers ends
      i2.push_back(thirdi);      
      thirdEnd.push_back(IDMemStart[i]);
      thirdBegin.push_back(" ");
      thirdDriver.push_back(" ");
      thirdDayname.push_back(" ");
      thirdMonths.push_back(" ");
      thirdTime.push_back(" ");
      thirdTimestamp.push_back(" ");
      thirdDay.push_back(0);
      thirdYear.push_back(0);
      thirdThird.push_back(0);
      thirdDRT.push_back(0);
      thirdIsItNew.push_back(0);
      thirdIsItNew2.push_back(0);
      outfile << IDMemStart[i] << std::endl;
      thirdi++;
      countDmps++;
    }

    // check for new drivers and add the new drivers to the already found list
    if(found1 != std::string::npos || found2 != std::string::npos){ // || i == int(IDDriver.size()) - 1){
    }
    else{
      int isItNew = driverNewCheck(IDDriver[i], IDTimestamp[i], driverStore, timestampStore, countDmps, driverStore.size());
      if(IDDriver[i].length() > 2 && isItNew){  
          std::vector<int> thirdCheck;
          if(IDDriver[i] != "Unknown" && IDDriver[i] != "unknown"){
            thirdCheck = thirdPartyCheck(IDDriver[i], IDUserProfile, database, databaseSystem, database.size());
          }
          else{
            thirdCheck.push_back(0);
            thirdCheck.push_back(0);
          }
          driverStore.push_back(IDDriver[i]);
          timestampStore.push_back(IDTimestamp[i]);  
          thirdDriver.push_back(IDDriver[i]);
          thirdDayname.push_back(IDDayname[i]);
          thirdMonths.push_back(IDMonths[i]);
          thirdTime.push_back(IDTime[i]);
          thirdTimestamp.push_back(IDTimestamp[i]);
          thirdDay.push_back(IDDay[i]);
          thirdYear.push_back(IDYear[i]);
          thirdThird.push_back(thirdCheck[0]);
          thirdDRT.push_back(thirdCheck[1]);
          thirdBegin.push_back(" ");
          thirdEnd.push_back(" ");
          thirdIsItNew.push_back(isItNew);
          thirdIsItNew2.push_back(isItNew);
          outfile << isItNew << " ";
          outfile << IDDriver[i] << " ";
          outfile << IDDayname[i] << " ";
          outfile << IDMonths[i] << " ";
          outfile << IDDay[i] << " ";
          outfile << IDTime[i] << " ";
          outfile << IDYear[i] << " ";
          outfile << IDTimestamp[i] << std::endl;
          thirdi++;
      }
      // get the maximum driver name length
      // determine if its name is longer than the currently stored maximum name length
      if(int(IDDriver[i].length()) > thirdMaxLength){
        thirdMaxLength = int(IDDriver[i].length());
      }
    }
    if(i == int(IDDriver.size()) - 1){
      // keep track of the very last set of drivers and where it ends
      i2.push_back(thirdi);
      thirdBegin.push_back(" ");
      thirdEnd.push_back(" ");
      thirdDriver.push_back(" ");
      thirdDayname.push_back(" ");
      thirdMonths.push_back(" ");
      thirdTime.push_back(" ");
      thirdTimestamp.push_back(" ");
      thirdDay.push_back(0);
      thirdYear.push_back(0);
      thirdThird.push_back(0);
      thirdDRT.push_back(0);
      thirdIsItNew.push_back(0);
      thirdIsItNew2.push_back(0);
      outfile << " " << std::endl;
      countDmps++;
    }
  }
  t02 = clock();
  float diff01 = ((float)t02 - (float)t01) / 1000.0F;
  OutputTimeBetween(diff01, " seconds to get third party info and DRT info");
  int beginDmp1 = 0;
  int endDmp1 = 0;
  int beginDmp2 = 0;
  int endDmp2 = 0;
  t01 = clock();
  for(int k = 0; k < int(i1.size()); k++){
    for(int i=i1[k]; i < i2[k]; i++){
      std::string temp;
      if(i == i1[k]){
        if(k > 0){
          endDmp1 = 1;
          endDmp2 = 1;
        }
        beginDmp1 = 1;
        beginDmp2 = 1;
      }
      // check for drivers missing from the DRT, make sure the drivers have not already been found, and make sure
      //    the driver name is long enough to actually be a driver
      if(thirdDriver[i].length() > 1
        && thirdDRT[i]){
          std::stringstream sstemp;
          // add the driver name to the missing from DRT list
          sstemp << thirdDriver[i];
          // make sure driver ane driver link columns line up
          for(int j=0; j < thirdMaxLength - int(thirdDriver[i].length()); j++){
            sstemp << " ";
          }
          // also add a possible link for driver information about the missing driver
          sstemp << "    https://www.google.com/search?q=" << thirdDriver[i];  
          // if it is the beginning of a new .dmp, add separators
          if(beginDmp1){
            if(endDmp1){
              missingDRT.push_back(thirdEnd[i2[k-1]]);
              outfile << thirdEnd[i2[k-1]] << std::endl;
              endDmp1 = 0;
            }
            temp = thirdBegin[i1[k]-1];
            missingDRT.push_back(temp);
            outfile << i1[k]-1 << " " << temp << std::endl;
            beginDmp1 = 0;
          }
          // then add the driver name and link
          missingDRT.push_back(sstemp.str());
          outfile << sstemp.str() << std::endl;
      }
      // check for 3rd party drivers, whether the driver has already been found in a previous .dmp,
      //    and whether the driver name is long enough to be a true driver
      if(thirdDay[i] > 0
        && thirdThird[i]){
          // create driver list for in the code box
          std::stringstream sstemp;
          // make sure all the driver information is formatted so columns line up
          sstemp << thirdDriver[i];
          for(int j=0; j < thirdMaxLength - int(thirdDriver[i].length()); j++){
            sstemp << " ";
          }
          sstemp << "   " << thirdDayname[i]
                << " " << thirdMonths[i] << " ";
          if(thirdDay[i] < 10){
            sstemp << " " << thirdDay[i] << " " 
                  << thirdTime[i] << " " << thirdYear[i] << " "
                  << thirdTimestamp[i];
          }
          else{
            sstemp << thirdDay[i] << " " 
                  << thirdTime[i] << " " << thirdYear[i] << " "
                  << thirdTimestamp[i];
          }
          // add separators to the beginning of new driver lists
          if(beginDmp2){
            if(endDmp2){
              nameCode.push_back(thirdEnd[i2[k-1]]);
              nameIsItNew.push_back(thirdIsItNew[i2[k-1]]);
              outfile << thirdEnd[i2[k-1]] << std::endl;
              endDmp2 = 0;
            }
            temp = thirdBegin[i1[k]-1];
            nameCode.push_back(temp);
            nameIsItNew.push_back(thirdIsItNew[i1[k]-1]);
            outfile << i1[k]-1 << " " << temp << std::endl;
            beginDmp2 = 0;
          }
          nameCode.push_back(sstemp.str());
          nameIsItNew.push_back(thirdIsItNew[i]);
          outfile << thirdIsItNew[i] << " " << sstemp.str() << std::endl;

          // create driver links only if they appear on the DRT
          if(thirdDRT[i]){
            std::string tempString = "";
            if(!IDTurnOffBBCode){
              tempString = "[color=#777777][color=#4b0082]";
            }
            tempString = tempString + thirdDriver[i];
            if(!IDTurnOffBBCode){
              tempString = tempString + "[/color]" + 
                           " - this driver hasn't been added to the DRT as of this run. Please search Google/Bing for the driver if additional information is needed." + 
                           "[/color]";
            }
            nameLinks.push_back(tempString);
            outfile << tempString << std::endl;
          }
          else{
            // use the driver link formatting from driverInOut.txt
            std::stringstream sstemp2;
            if(!IDTurnOffBBCode){
              sstemp2 << "[url=";
            }
            sstemp2 << IDDriverInSite << thirdDriver[i];
            if(!IDTurnOffBBCode){
              sstemp2 << "]";
            }
            int countDriverName = 0;
            int countDriverName2 = 0;
            for(int j=0; j < int(IDDriverOutSite.size()); j++){
              if (IDDriverOutSite.c_str()[j] == '<' || j == int(IDDriverOutSite.size()) - 1){
                if(!countDriverName2){
                  countDriverName++;
                  if(j == int(IDDriverOutSite.size()) - 1){
                    sstemp2 << IDDriverOutSite.c_str()[j];
                  }
                }
              }
              if (countDriverName == 1){
                if(!IDTurnOffBBCode){
                  sstemp2 << thirdDriver[i];
                }
                countDriverName++;
                countDriverName2++;
              }
              if (countDriverName == 0 && !IDTurnOffBBCode){
                sstemp2 << IDDriverOutSite.c_str()[j];
              }
              if (IDDriverOutSite.c_str()[j] == '>'){
                countDriverName = 0;
              }
            }
            if(!IDTurnOffBBCode){
              sstemp2 << "[/url]";
            }
            nameLinks.push_back(sstemp2.str());
            outfile << sstemp2.str() << std::endl;
          }
      }
    }
  }
  t02 = clock();
  diff01 = ((float)t02 - (float)t01) / 1000.0F;  
  OutputTimeBetween(diff01, " seconds to store 3rd party info and DRT info");
  outputLog(outfile.str());
}



void InputData::outputThirdPartyDates(){
  // sort the drivers first by their timestamps
  outputLog("outputThirdPartyDates()");
  outputLog("here");
  sortThirdPartyDates();
  outputLog("Sorted");
  int countDmps = 0;
  // store drivers that have already been found
  std::vector<std::string> driverStore;
  // store their timestamps to determine if the driver has been updated
  std::vector<std::string> timestampStore;
  // keep track of which 3rd party driver lists need separators
  int beginDmp1 = 0;
  int endDmp1 = 0;
  for(int k = 0; k < int(i1.size()); k++){
    for(int i=i1[k]; i < i2[k]; i++){
      std::string temp;
      if(i == i1[k]){
        if(k > 0){
          endDmp1 = 1;
        }
        beginDmp1 = 1;
      }
      // check for 3rd party drivers
      if(thirdDay[i] > 0
        && thirdThird[i]){
          // create driver list for the code box and make sure it is formatted in columns for easier reading
          std::stringstream sstemp;
          sstemp << thirdDriver[i];
          for(int j=0; j < thirdMaxLength - int(thirdDriver[i].length()); j++){
            sstemp << " ";
          }
          sstemp << "   " << thirdDayname[i]
                << " " << thirdMonths[i] << " ";
          if(thirdDay[i] < 10){
            sstemp << " " << thirdDay[i] << " " 
                  << thirdTime[i] << " " << thirdYear[i] << " "
                  << thirdTimestamp[i];
          }
          else{
            sstemp << thirdDay[i] << " " 
                  << thirdTime[i] << " " << thirdYear[i] << " "
                  << thirdTimestamp[i];
          }
          if(beginDmp1){
            if(endDmp1){
              dateCode.push_back(thirdEnd[i2[k-1]]);
              dateIsItNew.push_back(thirdIsItNew2[i2[k-1]]);
              std::stringstream outfile;
              outfile << thirdEnd[i2[k-1]];
              outputLog(outfile.str());
              endDmp1 = 0;
            }
            temp = thirdBegin[i1[k]-1];
            dateCode.push_back(temp);
            dateIsItNew.push_back(thirdIsItNew2[i1[k]-1]);
            beginDmp1 = 0;
            std::stringstream outfile;
            outfile << thirdBegin[i1[k]-1];
            outputLog(outfile.str());
          }
          dateCode.push_back(sstemp.str());
          dateIsItNew.push_back(thirdIsItNew2[i]);
          outputLog(sstemp.str());

          // create driver links only if the driver appears on the DRT
          if(thirdDRT[i]){
            std::string tempString = "";
            if(!IDTurnOffBBCode){
              tempString = "[color=#777777][color=#4b0082]";
            }
            tempString = tempString + thirdDriver[i];
            if(!IDTurnOffBBCode){
              tempString = tempString + "[/color]" + 
                           " - this driver hasn't been added to the DRT as of this run. Please search Google/Bing for the driver if additional information is needed." + 
                           "[/color]";
            }
            dateLinks.push_back(tempString);
          }
          else{
            std::stringstream sstemp2;
            // format the driver links based on driverInOut.txt formatting
            if(!IDTurnOffBBCode){
              sstemp2 << "[url="; 
            }
            sstemp2 << IDDriverInSite << thirdDriver[i];
            if(!IDTurnOffBBCode){
              sstemp2 << "]"; 
            }
            int countDriverName = 0;
            int countDriverName2 = 0;
            for(int j=0; j < int(IDDriverOutSite.size()); j++){
              if (IDDriverOutSite.c_str()[j] == '<' || j == int(IDDriverOutSite.size()) - 1){
                if(!countDriverName2){
                  countDriverName++;
                  if(j == int(IDDriverOutSite.size()) - 1){
                    sstemp2 << IDDriverOutSite.c_str()[j];
                  }
                }
              }
              if (countDriverName == 1){
                if(!IDTurnOffBBCode){
                  sstemp2 << thirdDriver[i];
                }
                countDriverName++;
                countDriverName2++;
              }
              if (countDriverName == 0 && !IDTurnOffBBCode){
                sstemp2 << IDDriverOutSite.c_str()[j];
              }
              if (IDDriverOutSite.c_str()[j] == '>'){
                countDriverName = 0;
              }
            }
            if(!IDTurnOffBBCode){
              sstemp2 << "[/url]"; 
            }
            dateLinks.push_back(sstemp2.str());
            outputLog(sstemp2.str());
          }
      }
    }
  }
}

// sort the drivers by timestamp
void InputData::sortThirdPartyDates(){
  std::stringstream outfile;
  std::string driverT, daynameT, monthsT, timeT, timestampT;
  int dayT, yearT, thirdThirdT, thirdDRTT, thirdIsItNew2T;
  // rearrange the drivers within each set so they are in order by timestamp
  for(int k = 0; k < int(i1.size()); k++){
    outfile << "k = " << k << " of " << int(i1.size()) << std::endl;
    for(int i=i1[k]; i < i2[k]; i++){
      for(int j=i; j < i2[k]; j++){
        //outfile << "i = " << i << " of " << i2[k];
        driverT = thirdDriver[i];
        daynameT = thirdDayname[i];
        monthsT = thirdMonths[i];
        timeT = thirdTime[i];
        timestampT = thirdTimestamp[i];
        dayT = thirdDay[i];
        yearT = thirdYear[i];
        thirdThirdT = thirdThird[i];
        thirdDRTT = thirdDRT[i];
        thirdIsItNew2T = thirdIsItNew2[i];
        //outfile << "j = " << j << " of " << i2[k];
        if(thirdTimestamp[j].substr(1,thirdTimestamp[j].length() - 2)
          < thirdTimestamp[i].substr(1,thirdTimestamp[i].length() - 2)){
            thirdDriver[i] = thirdDriver[j];
            thirdDriver[j] = driverT;
            thirdDayname[i] = thirdDayname[j];
            thirdDayname[j] = daynameT;
            thirdMonths[i] = thirdMonths[j];
            thirdMonths[j] = monthsT;
            thirdTime[i] = thirdTime[j];
            thirdTime[j] = timeT;
            thirdTimestamp[i] = thirdTimestamp[j];
            thirdTimestamp[j] = timestampT;
            thirdDay[i] = thirdDay[j];
            thirdDay[j] = dayT;
            thirdYear[i] = thirdYear[j];
            thirdYear[j] = yearT;
            thirdThird[i] = thirdThird[j];
            thirdThird[j] = thirdThirdT;
            thirdDRT[i] = thirdDRT[j];
            thirdDRT[j] = thirdDRTT;
            thirdIsItNew2[i] = thirdIsItNew2[j];
            thirdIsItNew2[j] = thirdIsItNew2T;
        }
      }
    }
  }
  outputLog(outfile.str());
}

// get the full, third party by name, third party by date, and unloaded drivers lists
void InputData::getDriverLists(){
  clock_t t01, t02;

  t01 = clock();
  getDriverInfo();
  t02 = clock();
  float diff01 = ((float)t02 - (float)t01) / 1000.0F;  
  OutputTimeBetween(diff01, " seconds to getDriverInfo()");
  outputLog("Gathering the full driver list");
  t01 = clock();
  outputFullDriverList();
  t02 = clock();
  diff01 = ((float)t02 - (float)t01) / 1000.0F;  
  OutputTimeBetween(diff01, " seconds to outputFullDriverList()");
  outputLog("Gathering third party drivers by name");
  t01 = clock();
  outputThirdPartyNames();
  t02 = clock();
  diff01 = ((float)t02 - (float)t01) / 1000.0F;  
  OutputTimeBetween(diff01, " seconds to outputThirdPartyNames()");
  outputLog("Gathering thrid party drivers by date");
  t01 = clock();
  outputThirdPartyDates();
  t02 = clock();
  diff01 = ((float)t02 - (float)t01) / 1000.0F;    
  OutputTimeBetween(diff01, " seconds to outputThirdPartyDates()");
  outputLog("Finished gathering third party drivers");
}

void InputData::OutputTimeBetween(float diff01, std::string str)
{
  if(loggingChecked)
  {
    std::ofstream outTime;
    std::string timePath2 = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\timeBetweenRuns.txt";
    std::string timePath3 = IDUserProfile + "\\SysnativeBSODApps\\dmpOptions\\outputOptions.txt";
    std::ofstream outTime2;
    std::ofstream outTime3;

    if(!m_bTimeStarted)
    {
      m_bTimeStarted = true;
      outTime.open(tempDir + "\\outputDmps\\timeBetweenRuns.txt", std::ios::app);
      outTime2.open(timePath2, std::ios::app);
      outTime3.open(timePath3, std::ios::app);
    }
    else
    {
      outTime.open(tempDir + "\\outputDmps\\timeBetweenRuns.txt", std::ios::app);
      outTime2.open(timePath2, std::ios::app);
      outTime3.open(timePath3, std::ios::app);
    }

    outTime << diff01 << str << std::endl;
    outTime2 << diff01 << str << std::endl;
    outTime3 << "# " << diff01 << str << std::endl;
    outTime.close();
    outTime2.close();
    outTime3.close();
  }
}

// Output std::strings to a logfile
void InputData::outputLog(std::string logStr)
{
  if(loggingChecked)
  {
    std::ofstream outLog;
    std::string logPath = IDUserProfile + "\\SysnativeBSODApps\\Logs\\InputData.log";
    if(m_bLogStarted)
    {
      outLog.open(logPath, std::ios::app);
      outLog << logStr << std::endl;
      outLog.close();
    }
    else
    {
      using namespace std::string_literals;
      FileSystemUtil::createDirectoryWithBackup(IDUserProfile, "SysnativeBSODApps");
      FileSystemUtil::createDirectoryWithBackup(IDUserProfile + "\\SysnativeBSODApps"s, "Logs");
      m_bLogStarted = true;
      outLog.open(logPath);
      outLog << logStr << std::endl;
      outLog.close();
    }
  }
}