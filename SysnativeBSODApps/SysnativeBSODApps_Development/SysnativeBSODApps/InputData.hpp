#ifndef INPUTDATA_HPP
#define INPUTDATA_HPP
//-----------------------------------------------------------------------------
//  File:         InputData c++ class
//  Description:  Collects data from system, stores it in .txt files and memory
//  Project:      dmpAnalysis
//  Author:       Mikael Pryor
//  Date:         July  29, 2012
//  Changes:      July  29, 2012
//  Changes Made: Created constructor, destructor, cleanUpOperations(),
//                and deleteTemporaryDirectory()
//-----------------------------------------------------------------------------
#include "StringComparison.hpp"
#include "BugCheckDownload.h"
#include "SysnativeFileSystemManager.h"
#include <string>
#include <vector>

class InputData {
public:
  // Contructor, Destructor, and Functions
  InputData(std::string symbolsString, std::string symbolsString2, int symbolsInt, std::string driverInSite, 
        std::string driverOutSite, std::string userProfile, int dmpsExist, int inLoadingDumpFile, 
        int inKernelVersion, int inBuiltBy, int inDebugSessionTime, 
        int inSystemUptime, int inBugCheck, 
        int inUnableToVerify, int inSymbolsNotLoaded, int inProbablyCausedBy, 
        int inDefaultBucketID, int inBugcheckStr, int inBugCheckAnalysis, 
        int inProcessName, int inFailureBucketID, int inBugcheckCode, 
        int inArguments, int inCPUID, int inMaxSpeed, int inCurrentSpeed, 
        int inBIOSVersion, int inBIOSReleaseDate, int inSystemManufacturer, 
        int inSystemProductName, std::string inDbugDir, int inAddSpaces,
        int inTurnOffBBCode, int inErrorCode, int inDiskHardwareError, int inVerifier,
        int inOpenOnExit, int inNoBBCodeCodeBoxes, int inAllFilesDmps, 
        int inInHtml, int inOverclock, int inNonParsed, std::vector<std::string> inkdCommands,
        int inOnlyUser, int inMissingServicePack, std::string tempString_s);
  virtual ~InputData();
  void cleanUpOperations(std::string timeDir);
  void showMessageBoxes();
  int downloadDumpFile();
  void cleanUpDriverList();
  void getDriverList();
  void getDriverList(int i);
  void getDmpsList();
  void runDmps();
  void runUserDmps();
  void getImportantInfo();
  void getDriverLists();

  // Variables
  std::vector<std::string> database;
  std::vector<std::string> database2;
  std::vector<int> databaseSystem;
  std::vector<std::string> dmpList;
  std::vector<std::string> dmpList2;
  std::vector<std::string> oldDmpList;
  std::vector<std::string> analyzedDmps;
  std::vector<std::string> analyzedStack;    // output to stack.txt
  std::vector<std::string> analyzedBIOS;    // output to SMBIOS.txt
  std::vector<std::string> timeDmp;
  std::vector<std::string> dmpsOutput;      // output to dmps.txt
  std::vector<std::string> importantInfo;    // output to importantInfo.txt
  std::vector<std::string> importantInfoNon;  // output to importantInfo.txt nonparsed
  std::vector<std::string> ninetyEight;      // output the _98-dbug.txt
  std::vector<std::string> fullDriverList;    // output to drivers.txt
  std::vector<std::string> unloadedDrivers;    // output to unloadedDrivers.txt
  std::vector<std::string> nameCode;      // output wrapped in code to 3rdPartyDriversName.txt
  std::vector<int> nameIsItNew;      // 
  std::vector<std::string> nameLinks;      // output to 3rdPartyDriversName.txt
  std::vector<std::string> dateCode;      // output wrapped in code to 3rdPartyDriversDate.txt
  std::vector<int> dateIsItNew;      // 
  std::vector<std::string> dateLinks;      // output to 3rdPartyDriversDate.txt
  std::vector<std::string> missingDRT;      // output to missingFromDRT.txt
  std::vector<std::string> loadingDumpFile;     // output Loading Dump File for each .dmp
  std::vector<std::string> kernelVersion;    // output Kernel Version for each .dmp
  std::vector<std::string> missingServicePack;  // output Missing Service Pack info
  std::vector<std::string> builtBy;        // output Built by for each .dmp
  std::vector<std::string> debugSessionTime;  // output Debug session time for each .dmp
  std::vector<std::string> systemUptime;    // output System Uptime for each .dmp
  std::vector<std::string> bugCheck;      // output BugCheck for each .dmp
  std::vector<std::string> bugCheckAnalysis;  // output BugCheckAnalysis for each .dmp
  std::vector<std::string> unableToVerify;    // output Unable to verify timestamp for each .dmp
  std::vector<std::string> symbolsNotLoaded;  // output Symbols could not be loaded for each .dmp
  std::vector<std::string> probablyCausedBy;  // output Probably caused by for each .dmp
  std::vector<std::string> bugcheckStr;      // output BUGCHECK_STR for each .dmp
  std::vector<std::string> defaultBucketID;    // output DEFAULT_BUCKET_ID for each .dmp
  std::vector<std::string> verifierOne;    // output DEFAULT_BUCKET_ID for each .dmp
  std::vector<std::string> verifierTwo;    // output DEFAULT_BUCKET_ID for each .dmp
  std::vector<std::string> processName;      // output PROCESS_NAME for each .dmp
  std::vector<std::string> failureBucketID;    // output FAILURE_BUCKET_ID for each .dmp
  std::vector<std::string> bugcheckCode;    // output Bugcheck code for each .dmp
  std::vector<std::string> errorCode;    // output Error Code for each .dmp
  std::vector<std::string> diskHardwareError;  // output DISK_HARDWARE_ERROR for each .dmp
  std::vector<std::string> arguments;      // output Arguments for each .dmp
  std::vector<std::string> CPUID;        // output CPUID for each .dmp
  std::vector<std::string> biosVersion;      // BIOS Version for each .dmp
  std::vector<std::string> biosReleaseDate;    // BIOS Release Date for each .dmp
  std::vector<std::string> systemManufacturer;  // Manufacturer for each .dmp
  std::vector<std::string> systemProductName;  // Product Name for each .dmp
  std::vector<std::string> maxSpeed;      // MaxSpeed for each .dmp
  std::vector<std::string> currentSpeed;    // CurrentSpeed for each .dmp
  std::vector<std::string> overclock;      // output Overclock info for each .dmp
  std::vector<std::string> description;      // description of driver
  std::vector<int> i1,i2;          // std::vectors for storing the start and end of .dmps
  std::vector<std::string> thirdDriver;      // third party driver name
  std::vector<std::string> thirdDayname;    // third party day name
  std::vector<std::string> thirdMonths;
  std::vector<std::string> thirdTime;
  std::vector<std::string> thirdTimestamp;
  std::vector<std::string> MicrosoftOSDrivers;
  std::vector<int> thirdDay;
  std::vector<int> thirdYear;
  std::vector<int> thirdThird;
  std::vector<int> thirdDRT;
  int thirdi;
  std::vector<std::string> thirdBegin;
  std::vector<std::string> thirdEnd;
  std::vector<int> thirdIsItNew;
  std::vector<int> thirdIsItNew2;
  int thirdMaxLength;
  double percentDone;
  double percentDoneStore;
  int openOnExit;
  int inHtml;
  int cleanUp;
  std::vector<std::string> corruptedDmps;
  std::vector<std::string> sizeMismatch;
  std::vector<int> pYear, pDmpNum, pDmpTime;
  std::vector<std::string> pDriver, pMonths, pTime; 
  std::vector<std::string> SevenBSODDriver;
  std::vector<std::string> EightBSODDriver;
  int console;
  int buildNumber;
  int OSVersion;
  std::string tempDir;
  std::string tempPath;
  std::string userProfile;
  bool loggingChecked;
protected:

private:
  // Private Functions
  void deleteTemporaryDirectory();
  void getImportantLines(int j);
  void kernelVersionString(int j);
  void builtByString(int j);
  void systemUptimeString(int j);
  void bugCheckString(int j);
  void unableToVerifyString(int j);
  void symbolsNotLoadedString(int j);
  void probablyCausedByString(int j);
  void defaultBucketIDString(int j);
  void verifierStringOne(int j);
  void verifierStringTwo(int j);
  void bugcheckStrString(int j);
  void bugCheckAnalysisString(int j);
  void processNameString(int j);
  void failureBucketIDString(int j);
  void errorCodeString(int j);
  void diskHardwareErrorString(int j);
  void argumentsString(int j);
  void CPUIDString(int j);
  void maxSpeedString(int j);
  void currentSpeedString(int j);
  void BIOSVersionString(int j);
  void BIOSReleaseDateString(int j);
  void systemManufacturerString(int j);
  void systemProductNameString(int j);
  void getStack(int j);
  void getSMBIOS(int j);
  void getDriverInfo();
  void outputFullDriverList();
  void outputThirdPartyNames();
  void outputThirdPartyDates();
  void sortThirdPartyDates();
  std::vector<int> thirdPartyCheck(std::string test, std::string userprofile, std::vector<std::string> database1, std::vector<int> databaseSystem1, int strSize);
  int driverNewCheck(std::string test1, std::string test2, std::vector<std::string> driverData, std::vector<std::string> timestampData, int countDmps, int strSize);
  std::vector<std::string> Split(std::string str, char c);
  std::string RemoveTrailingZeros(std::string str);
  std::string GetBugCheckFromAnalysis(std::string bugCheckAnalysisStr, std::string argsStr);
  void fillInNinetyEight(int i);
  void outputLog(std::string logStr);
  void OutputTimeBetween(float diff01, std::string str);

  // Private Variables
  std::string IDSymbolsString;
  std::string IDSymbolsString2;
  int IDSymbolsInt;
  std::string IDDriverInSite;
  std::string IDDriverOutSite;
  std::string IDUserProfile;
  int IDDmpsExist;
  int IDStack;
  int IDSMBIOS;
  StringComparison *compare;
  std::vector<int> IDDay, IDYear, IDDmpNum;
  std::vector<std::string> IDMemStart, IDMemEnd, IDName, IDDriver, 
           IDDayname, IDMonths, IDTimestamp, IDTime; 
  int IDMaxName;
  int IDMaxDriver;
  int IDLoadingDumpFile;
  int IDKernelVersion;
  int IDMissingServicePack;
  int IDBuiltBy;
  int IDDebugSessionTime;
  int IDSystemUptime;
  int IDBugCheck;
  int IDUnableToVerify;
  int IDSymbolsNotLoaded;
  int IDProbablyCausedBy;
  int IDDefaultBucketID;
  int IDVerifier;
  int IDBugcheckStr;
  int IDBugCheckAnalysis;
  int IDProcessName;
  int IDFailureBucketID;
  int IDBugcheckCode;
  int IDErrorCode;
  int IDDiskHardwareError;
  int IDArguments;
  int IDCPUID;
  int IDMaxSpeed;
  int IDCurrentSpeed;
  int IDOverclock;
  int IDBIOSVersion;
  int IDBIOSReleaseDate;
  int IDSystemManufacturer;
  int IDSystemProductName;
  int IDAddSpaces;
  int IDTurnOffBBCode;
  int IDNoBBCodeCodeBoxes;
  int IDNonParsed;
  int IDAllFilesDmps;
  std::string IDDbugDir;
  int IDForDebugging;
  int IDMemory;
  std::vector<std::string> IDkdCommands;
  int IDOnlyUser;
  BugCheckDownload bcd;
  SysnativeFileSystemManager fileSystemManager;
  bool m_bLogStarted;
  bool m_bTimeStarted;
  bool m_bProbablyCausedBy;
  std::string m_sSymbolName;
  std::string m_sUnableToVerify;
  std::string m_sSymbolsNotLoaded;
};

#endif
