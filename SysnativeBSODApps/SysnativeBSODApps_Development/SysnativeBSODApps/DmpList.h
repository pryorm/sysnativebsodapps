#include "CenterWindow.h"
#include "ConfigurationUtil.h"
#include "LogUtil.h"
#include "SystemStandardConversionUtil.h"

#include <stdlib.h>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>
#include <iostream>
#include <Windows.h> // for Sleep

#include "debug.h"


#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for DmpList
  /// </summary>
  public ref class DmpList : public System::Windows::Forms::Form
  {
  public:
    System::String^ tempStringS;
    DmpList(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

    System::Void centerDmpList()
    {
        CenterWindow myWindow;
        myWindow.MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~DmpList()
    {
      if (components)
      {
        delete components;
      }
    }
  private: System::Windows::Forms::CheckBox^  checkBox1;
  protected: 
    System::String^ mStatus;
  private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::Label^  label4;
  private: System::Windows::Forms::Label^  label12;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
  private: System::Windows::Forms::Label^  label2;
  private: System::Windows::Forms::Label^  label1;
  private: System::Windows::Forms::CheckedListBox^  checkedListBox1;

  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(DmpList::typeid));
      this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
      this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->label4 = (gcnew System::Windows::Forms::Label());
      this->label12 = (gcnew System::Windows::Forms::Label());
      this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->checkedListBox1 = (gcnew System::Windows::Forms::CheckedListBox());
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->BeginInit();
      this->SuspendLayout();
      // 
      // checkBox1
      // 
      this->checkBox1->AutoSize = true;
      this->checkBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Underline)), 
        System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
      this->checkBox1->Location = System::Drawing::Point(53, 62);
      this->checkBox1->Name = L"checkBox1";
      this->checkBox1->Size = System::Drawing::Size(122, 17);
      this->checkBox1->TabIndex = 42;
      this->checkBox1->Text = L".dmps to Analyze";
      this->checkBox1->UseVisualStyleBackColor = true;
      this->checkBox1->Click += gcnew System::EventHandler(this, &DmpList::checkBox1_CheckedChanged);
      // 
      // backgroundWorker1
      // 
      this->backgroundWorker1->WorkerReportsProgress = true;
      this->backgroundWorker1->WorkerSupportsCancellation = true;
      this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &DmpList::backgroundWorker1_DoWork);
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(202, 58);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(75, 23);
      this->button1->TabIndex = 41;
      this->button1->Text = L"Continue";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &DmpList::button1_Click);
      // 
      // label4
      // 
      this->label4->AutoSize = true;
      this->label4->Location = System::Drawing::Point(50, 35);
      this->label4->Name = L"label4";
      this->label4->Size = System::Drawing::Size(0, 13);
      this->label4->TabIndex = 40;
      // 
      // label12
      // 
      this->label12->AutoSize = true;
      this->label12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label12->ForeColor = System::Drawing::SystemColors::GrayText;
      this->label12->Location = System::Drawing::Point(6, 7);
      this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label12->Name = L"label12";
      this->label12->Size = System::Drawing::Size(127, 13);
      this->label12->TabIndex = 39;
      this->label12->Text = L" � 2024 Mike Pryor";
      // 
      // numericUpDown1
      // 
      this->numericUpDown1->Location = System::Drawing::Point(173, 564);
      this->numericUpDown1->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {10000, 0, 0, 0});
      this->numericUpDown1->Name = L"numericUpDown1";
      this->numericUpDown1->Size = System::Drawing::Size(73, 20);
      this->numericUpDown1->TabIndex = 38;
      this->numericUpDown1->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {365, 0, 0, 0});
      this->numericUpDown1->ValueChanged += gcnew System::EventHandler(this, &DmpList::ageToExclude);
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(251, 566);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(31, 13);
      this->label2->TabIndex = 37;
      this->label2->Text = L"Days";
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(50, 566);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(121, 13);
      this->label1->TabIndex = 36;
      this->label1->Text = L"Age of Dmps to Exclude";
      // 
      // checkedListBox1
      // 
      this->checkedListBox1->CheckOnClick = true;
      this->checkedListBox1->FormattingEnabled = true;
      this->checkedListBox1->Location = System::Drawing::Point(50, 84);
      this->checkedListBox1->Name = L"checkedListBox1";
      this->checkedListBox1->Size = System::Drawing::Size(227, 469);
      this->checkedListBox1->TabIndex = 35;
      this->checkedListBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &DmpList::checkedListBox1_SelectedIndexChanged);
      // 
      // DmpList
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(343, 593);
      this->Controls->Add(this->checkBox1);
      this->Controls->Add(this->button1);
      this->Controls->Add(this->label4);
      this->Controls->Add(this->label12);
      this->Controls->Add(this->numericUpDown1);
      this->Controls->Add(this->label2);
      this->Controls->Add(this->label1);
      this->Controls->Add(this->checkedListBox1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Name = L"DmpList";
      this->Text = L"DmpList";
      this->Load += gcnew System::EventHandler(this, &DmpList::loadList);
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->EndInit();
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
  private: System::Void loadList(System::Object^  sender, System::EventArgs^  e) {
         TopMost = true;
         Hide();
         UpdateZOrder();
         BringToFront();
         SetForegroundWindow(static_cast<HWND>(this->Handle.ToPointer()));
         Show();
         TopMost = false;
         clock_t t01;
         t01 = clock();
		 std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
		 std::string userProfile = ConfigurationUtil::getUserProfilePath();
		 std::ofstream outTime(tempDir + "\\startList.txt");
         outTime << t01;
         outTime.close();
         backgroundWorker1->RunWorkerAsync();
		 std::ifstream inNewDmps(tempDir + "\\newDmpsOnly.txt");
         int newDmpsOnly;
         inNewDmps >> newDmpsOnly;
         inNewDmps.close();

		 std::string path = userProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";
		 std::ifstream inOldDmps(path);
		 std::vector<std::string> oldDmpList;
         while(inOldDmps.good() && !inOldDmps.eof()){
           std::string inString;
           getline(inOldDmps, inString);
           if(inString.length() > 0){
             oldDmpList.push_back(inString);
           }
         }
         inOldDmps.close();

		 std::ifstream infile2(tempDir + "\\dmpList2.txt");
         checkedListBox1->Items->Clear();
		 std::ofstream outfile;
         outfile.open(path, std::ios::app);
         int i1 = 0;
         while(infile2.good() && !infile2.eof()){
           std::string test;
           getline(infile2,test);
           if(test.length() > 0){
             System::String^ str = gcnew System::String(test.c_str());
             this->checkedListBox1->Items->Add( str, false );
           }
           int seenThis = 0;
           for(int i=0; i<int(oldDmpList.size()); i++){
             if(oldDmpList[i] == test){
               seenThis++;
             }
           }
           if(!seenThis && test.length() > 0){
            checkedListBox1->SetItemChecked(i1, true);
           }
           else if(!newDmpsOnly && test.length() > 0){
            checkedListBox1->SetItemChecked(i1, true);
           }
           if(test.length() > 0){
            i1++;
           }
         }
         infile2.close();
}
  private: System::Void loadList2(System::Object^  sender, System::EventArgs^  e) {
		 std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
		 std::string userProfile = ConfigurationUtil::getUserProfilePath();
		 std::ifstream infile(tempDir + "\\dmpList.txt");
		 std::ifstream infile2(tempDir + "\\dmpList2.txt");
         checkedListBox1->Items->Clear();
         while(infile2.good() && !infile2.eof()){
           std::string test;
           getline(infile2,test);
           if(test.length() > 0){
             System::String^ str = gcnew System::String(test.c_str());
             this->checkedListBox1->Items->Add( str, false );
           }
         }
         int i = 0;
         while(infile.good() && !infile.eof()){
		   std::string test;
           getline(infile,test);
           if(test.length() > 0){
             System::String^ str = gcnew System::String(test.c_str());
             System::DateTime dt = System::IO::File::GetLastWriteTime( str );
             System::TimeSpan ts = System::DateTime::Now.Subtract(dt);
             if(ts.Days < numericUpDown1->Value){
              checkedListBox1->SetItemChecked(i, true);
              // this->textBox1->AppendText( ts.ToString() );
             }
             i++;
             // this->textBox1->AppendText("\n");
           }
         }
         infile.close();
         infile2.close();
       }

  private: System::Void saveList(System::Object^  sender, System::EventArgs^  e) {
		 std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
		 std::string userProfile = ConfigurationUtil::getUserProfilePath();

		 std::string path = userProfile + "\\SysnativeBSODApps\\dmpOptions\\oldDmpsList.txt";

		 std::ifstream inOldDmps(path);
		 std::vector<std::string> oldDmpList;
         while(inOldDmps.good() && !inOldDmps.eof()){
           std::string inString;
           getline(inOldDmps, inString);
           if(inString.length() > 0){
             oldDmpList.push_back(inString);
           }
         }
         inOldDmps.close();

		 std::ofstream outfile;
		 std::ifstream infile2(tempDir + "\\dmpList2.txt");
         outfile.open(path, std::ios::app);
         int i1 = 0;
         while(infile2.good() && !infile2.eof()){
           std::string test;
           getline(infile2,test);
           int seenThis = 0;
           for(int i=0; i<int(oldDmpList.size()); i++){
             if(oldDmpList[i] == test){
               seenThis++;
             }
           }
           if(!seenThis && test.length() > 0){
             // cout << checkedListBox1->GetItemChecked(i1) << endl;
             if(checkedListBox1->GetItemChecked(i1)){
               outfile << test << std::endl;
             }
           }
           if(test.length() > 0){
            i1++;
           }
         }
         infile2.close();
         outfile.close();

		 std::ofstream outCheck(tempDir + "\\dmpCheck.txt");
         for(int i=0; i<checkedListBox1->Items->Count; i++){
           if(i != checkedListBox1->Items->Count - 1){
            outCheck << checkedListBox1->GetItemChecked(i) << std::endl;
           }
           else{
             outCheck << checkedListBox1->GetItemChecked(i);
           }
         }
         outCheck.close();
		 std::ifstream infile(tempDir + "\\dmpCheck.txt");
         infile2.open(tempDir + "\\dmpList2.txt");
		 std::vector<std::string> dmpList;
         while(infile2.good() && !infile2.eof()){
           std::string test;
           getline(infile2,test);
           if(test.length() > 0){
             dmpList.push_back(test);
           }
         }
         int i = 0;
         outfile.open(tempDir + "\\dmpList3.txt");
         while(infile.good() && !infile.eof()){
           int test;
           infile >> test;
           if(test){
             outfile << dmpList[i] << std::endl;
           }
           i++;
         }
         outfile.close();
         infile.close();
         infile2.close();
       }
private: System::Void ageToExclude(System::Object^  sender, System::EventArgs^  e) {
       backgroundWorker1->CancelAsync();
       if(numericUpDown1->Value < 1){
         numericUpDown1->Value = 1;
       }
       loadList2(sender, e);
     }

  private: System::Void SafeInvoke(Control^ control, System::Action^ action)
  {
      if (control != nullptr && !control->IsDisposed && control->InvokeRequired)
      {
          try
          {
              control->Invoke(action);
          }
          catch (System::ObjectDisposedException ^ excep)
          {
              LogUtil::logErrorAsync(excep->Message);
          }
      }
      else if (control != nullptr && !control->IsDisposed)
      {
          action();
      }
  }

  System::Void clearLabel4()
  {
      label4->Text = "";
  }

  System::Void updateLabel4Status()
  {
      label4->Text = mStatus;
  }
private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
	   std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
	   std::string dmpListTemp = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP")) + "\\SysnativeBSODApps\\";
       std::string userProfile = ConfigurationUtil::getUserProfilePath();
       clock_t t01, t02;
	   std::ifstream inTime(tempDir + "\\startList.txt");
       inTime >> t01;
       inTime.close();

       while(1){
         Sleep(495);
         if (backgroundWorker1->CancellationPending == true)
         {
             e->Cancel = true;
             this->SafeInvoke(this, gcnew System::Action(this, &DmpList::clearLabel4));
             break;
         }
         t02 = clock();
         float diff;
  
		 std::string DOParms = userProfile + "\\SysnativeBSODApps\\parms\\";
		 std::string DODmpOptions = dmpListTemp;

		 std::string path = DODmpOptions + "dmpListTimeout.txt";

         DODmpOptions = userProfile + "\\SysnativeBSODApps\\dmpOptions\\";
		 std::ifstream inDmpListTimeout(path);
         int dmpListTimeout;
         inDmpListTimeout >> dmpListTimeout;
         inDmpListTimeout.close();
         if(dmpListTimeout > 0){
           dmpListTimeout = dmpListTimeout + 1;
         }
         diff = (float)(dmpListTimeout) - ((float)t02 - (float)t01) / 1000.0F;
         mStatus = "Time 'Til Close: " + int(diff).ToString();
         this->SafeInvoke(this, gcnew System::Action(this, &DmpList::updateLabel4Status));
         if (diff <= 0.1){
           saveList(sender, e);
		   std::ofstream outfile(tempDir + "\\dmpListGUI.txt");
           outfile << "1";
           outfile.close();
           this->SafeInvoke(this, gcnew System::Action(this, &DmpList::Close));
           break;
         }
		 std::ifstream infile;
         infile.open(tempDir + "\\dmpListGUI2.txt");
         if(infile.good()){
           e->Cancel = true;
           this->SafeInvoke(this, gcnew System::Action(this, &DmpList::clearLabel4));
           break;
         }
         infile.close();
       }
     }
private: System::Void checkedListBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
       backgroundWorker1->CancelAsync();
       for(int i = 0; i < checkedListBox1->Items->Count; i++){
         if(checkedListBox1->GetItemChecked(i)){
          checkBox1->Checked = true;
          break;
        }
        else{
          checkBox1->Checked = false;
        }
       }
     }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	   std::string tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(tempStringS);
       std::string userProfile = ConfigurationUtil::getUserProfilePath();
       backgroundWorker1->CancelAsync();       
       saveList(sender, e);
	   std::ofstream outfile(tempDir + "\\dmpListGUI.txt");
       outfile << "1";
       outfile.close();
       Close();
     }
private: System::Void checkBox1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
       backgroundWorker1->CancelAsync();
       if(checkBox1->Checked){
         for(int i=0; i<checkedListBox1->Items->Count; i++){
           checkedListBox1->SetItemChecked(i, true);
         }
       }
       else{
         for(int i=0; i<checkedListBox1->Items->Count; i++){
           checkedListBox1->SetItemChecked(i, false);
         }
       }
     }
  };
}
