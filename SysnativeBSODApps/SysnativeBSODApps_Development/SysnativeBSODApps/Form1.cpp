#include "Form1.h"

#include "CenterWindow.h"
#include "ConfigurationUtil.h"
#include "FileSystemUtil.h"
#include "LimitSingleInstance.h"
#include "LoadSaveDZDN.h"
#include "LogUtil.h"
#include "ProgressBarDispatcher.h"
#include "SysnativeFileSystemManager.h"
#include "SystemStandardConversionUtil.h"
#include "Windows_8_1_and_10.h"

#include <algorithm>
#include <fstream>
#include <sstream>

using System::Threading::Thread;
using namespace System::Runtime::InteropServices;

namespace SysnativeBSODApps
{
ManagedProgressBarDispatcherWrapper::ManagedProgressBarDispatcherWrapper()
{
    mDispatcher = &ProgressBarDispatcher::getInstance();
}

void ManagedProgressBarDispatcherWrapper::openProgressBar()
{
    mDispatcher->openProgressBar();
}

Form1::Form1()
: mProgressBarDispatcher(gcnew ManagedProgressBarDispatcherWrapper())
, mLoggingChecked(false)
{
    InitializeComponent();
}

Form1::~Form1()
{
    if (mComponents)
    {
        delete mComponents;
    }
}

void Form1::setLoggingChecked(const bool loggingChecked)
{
    mLoggingChecked = loggingChecked;
}

bool Form1::getLoggingChecked()
{
    return mLoggingChecked;
}

cli::array<System::String^>^ Form1::getFileList()
{
    return mFileList;
}

bool Form1::getParallelChecked()
{
    return mParallelChecked;
}

int Form1::getDispatcherParallelThreads()
{
    return mDispatcher->defaultThreads;
}

int Form1::getDispatcherUserThreads()
{
    return mDispatcher->userThreads;
}

bool Form1::getSysnativeResultsChecked()
{
    return mSysnativeResultsChecked;
}

System::String^ Form1::getSysnativeResultsBuilderSysnativeResults()
{
    return mSysnativeResultsBuilder->sysnativeResultsS;
}

cli::array<System::String^>^ Form1::getSavedKdPaths()
{
    return mSavedKdPaths;
}

cli::array<int>^ Form1::getActiveKdPaths()
{
    return mActiveKdPaths;
}

// Boilerplate-heavy code
void Form1::InitializeComponent()
{
    System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
    this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
    this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
    this->textBox2 = (gcnew System::Windows::Forms::TextBox());
    this->label6 = (gcnew System::Windows::Forms::Label());
    this->textBox1 = (gcnew System::Windows::Forms::TextBox());
    this->label5 = (gcnew System::Windows::Forms::Label());
    this->label4 = (gcnew System::Windows::Forms::Label());
    this->checkBox7 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox6 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox5 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox4 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
    this->textBox10 = (gcnew System::Windows::Forms::TextBox());
    this->label22 = (gcnew System::Windows::Forms::Label());
    //this->tabControl2 = (gcnew System::Windows::Forms::TabControl());
    this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
    this->tabPage4 = (gcnew System::Windows::Forms::TabPage());
    //this->button10 = (gcnew System::Windows::Forms::Button());
    //this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
    //this->button8 = (gcnew System::Windows::Forms::Button());
    this->label9 = (gcnew System::Windows::Forms::Label());
    this->textBox3 = (gcnew System::Windows::Forms::TextBox());
    this->label7 = (gcnew System::Windows::Forms::Label());
    this->label3 = (gcnew System::Windows::Forms::Label());
    this->checkedListBox3 = (gcnew System::Windows::Forms::CheckedListBox());
    this->label2 = (gcnew System::Windows::Forms::Label());
    this->label1 = (gcnew System::Windows::Forms::Label());
    this->checkedListBox2 = (gcnew System::Windows::Forms::CheckedListBox());
    this->checkedListBox1 = (gcnew System::Windows::Forms::CheckedListBox());
    this->tabPage13 = (gcnew System::Windows::Forms::TabPage());
    this->button11 = (gcnew System::Windows::Forms::Button());
    this->label34 = (gcnew System::Windows::Forms::Label());
    this->label35 = (gcnew System::Windows::Forms::Label());
    this->label36 = (gcnew System::Windows::Forms::Label());
    this->textBox18 = (gcnew System::Windows::Forms::TextBox());
    this->textBox19 = (gcnew System::Windows::Forms::TextBox());
    this->textBox20 = (gcnew System::Windows::Forms::TextBox());
    this->label37 = (gcnew System::Windows::Forms::Label());
    this->label38 = (gcnew System::Windows::Forms::Label());
    this->label39 = (gcnew System::Windows::Forms::Label());
    this->label40 = (gcnew System::Windows::Forms::Label());
    this->textBox21 = (gcnew System::Windows::Forms::TextBox());
    this->textBox22 = (gcnew System::Windows::Forms::TextBox());
    this->textBox23 = (gcnew System::Windows::Forms::TextBox());
    this->label41 = (gcnew System::Windows::Forms::Label());
    this->label30 = (gcnew System::Windows::Forms::Label());
    this->label31 = (gcnew System::Windows::Forms::Label());
    this->label32 = (gcnew System::Windows::Forms::Label());
    this->textBox15 = (gcnew System::Windows::Forms::TextBox());
    this->textBox16 = (gcnew System::Windows::Forms::TextBox());
    this->textBox17 = (gcnew System::Windows::Forms::TextBox());
    this->label33 = (gcnew System::Windows::Forms::Label());
    this->menuStrip2 = (gcnew System::Windows::Forms::MenuStrip());
    this->xPDatesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->rTMAugToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->sP1Sep92002ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->sP2Aug252004ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->sP2bAug2006ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->sP2cAug102007ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->sP3Apr212008RTMToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->vistaDatesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->rTMNov82007ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->feb42008ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->sP2Apr282009RTMToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->datesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->rTMJul222009ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->sP1Feb112011ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->datesToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->rTMAugust12012ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->label21 = (gcnew System::Windows::Forms::Label());
    this->label20 = (gcnew System::Windows::Forms::Label());
    this->label19 = (gcnew System::Windows::Forms::Label());
    this->textBox9 = (gcnew System::Windows::Forms::TextBox());
    this->textBox8 = (gcnew System::Windows::Forms::TextBox());
    this->textBox14 = (gcnew System::Windows::Forms::TextBox());
    this->label8 = (gcnew System::Windows::Forms::Label());
    this->textBox5 = (gcnew System::Windows::Forms::TextBox());
    this->label10 = (gcnew System::Windows::Forms::Label());
    this->textBox6 = (gcnew System::Windows::Forms::TextBox());
    this->label11 = (gcnew System::Windows::Forms::Label());
    this->tabPage10 = (gcnew System::Windows::Forms::TabPage());
    this->button12 = (gcnew System::Windows::Forms::Button());
    this->checkBox21 = (gcnew System::Windows::Forms::CheckBox());
    this->button5 = (gcnew System::Windows::Forms::Button());
    this->checkBox16 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox15 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox14 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox13 = (gcnew System::Windows::Forms::CheckBox());
    this->textBox25 = (gcnew System::Windows::Forms::TextBox());
    this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
    this->checkBox12 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox11 = (gcnew System::Windows::Forms::CheckBox());
    this->textBox24 = (gcnew System::Windows::Forms::TextBox());
    this->label45 = (gcnew System::Windows::Forms::Label());
    this->richTextBox13 = (gcnew System::Windows::Forms::RichTextBox());
    this->label44 = (gcnew System::Windows::Forms::Label());
    this->richTextBox12 = (gcnew System::Windows::Forms::RichTextBox());
    this->label43 = (gcnew System::Windows::Forms::Label());
    this->richTextBox11 = (gcnew System::Windows::Forms::RichTextBox());
    this->label42 = (gcnew System::Windows::Forms::Label());
    this->richTextBox10 = (gcnew System::Windows::Forms::RichTextBox());
    this->label24 = (gcnew System::Windows::Forms::Label());
    this->label23 = (gcnew System::Windows::Forms::Label());
    this->richTextBox7 = (gcnew System::Windows::Forms::RichTextBox());
    this->richTextBox6 = (gcnew System::Windows::Forms::RichTextBox());
    this->tabPage12 = (gcnew System::Windows::Forms::TabPage());
    this->checkBox22 = (gcnew System::Windows::Forms::CheckBox());
    this->button6 = (gcnew System::Windows::Forms::Button());
    this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
    this->checkBox17 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox18 = (gcnew System::Windows::Forms::CheckBox());
    this->textBox26 = (gcnew System::Windows::Forms::TextBox());
    this->checkBox9 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox8 = (gcnew System::Windows::Forms::CheckBox());
    this->label26 = (gcnew System::Windows::Forms::Label());
    this->richTextBox9 = (gcnew System::Windows::Forms::RichTextBox());
    this->tabPage11 = (gcnew System::Windows::Forms::TabPage());
    this->checkBox23 = (gcnew System::Windows::Forms::CheckBox());
    this->button7 = (gcnew System::Windows::Forms::Button());
    this->numericUpDown3 = (gcnew System::Windows::Forms::NumericUpDown());
    this->checkBox19 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox20 = (gcnew System::Windows::Forms::CheckBox());
    this->textBox27 = (gcnew System::Windows::Forms::TextBox());
    this->button1 = (gcnew System::Windows::Forms::Button());
    this->label25 = (gcnew System::Windows::Forms::Label());
    this->richTextBox8 = (gcnew System::Windows::Forms::RichTextBox());
    this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
    this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
    this->label13 = (gcnew System::Windows::Forms::Label());
    this->tabPage5 = (gcnew System::Windows::Forms::TabPage());
    this->richTextBox2 = (gcnew System::Windows::Forms::RichTextBox());
    this->label14 = (gcnew System::Windows::Forms::Label());
    this->tabPage6 = (gcnew System::Windows::Forms::TabPage());
    this->richTextBox3 = (gcnew System::Windows::Forms::RichTextBox());
    this->label15 = (gcnew System::Windows::Forms::Label());
    this->tabPage7 = (gcnew System::Windows::Forms::TabPage());
    this->richTextBox4 = (gcnew System::Windows::Forms::RichTextBox());
    this->label16 = (gcnew System::Windows::Forms::Label());
    this->tabPage8 = (gcnew System::Windows::Forms::TabPage());
    this->textBox13 = (gcnew System::Windows::Forms::TextBox());
    this->textBox12 = (gcnew System::Windows::Forms::TextBox());
    this->label18 = (gcnew System::Windows::Forms::Label());
    this->label17 = (gcnew System::Windows::Forms::Label());
    this->tabPage9 = (gcnew System::Windows::Forms::TabPage());
    this->label46 = (gcnew System::Windows::Forms::Label());
    this->richTextBox5 = (gcnew System::Windows::Forms::RichTextBox());
    this->button9 = (gcnew System::Windows::Forms::Button());
    this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
    this->pressF1ForHelpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->loadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->saveAsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->loadPreviousToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->quickSaveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
    this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->alwaysOnTopToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->useParallelThreadingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->changeThreadsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->sysnativeResultsDIrectoryToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->setDirectoryToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->loggingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->closeAppsWhenFinishedToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->windowsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->kdexePathsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->saveAndRunToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
    this->textBox4 = (gcnew System::Windows::Forms::TextBox());
    this->label27 = (gcnew System::Windows::Forms::Label());
    this->textBox11 = (gcnew System::Windows::Forms::TextBox());
    this->label28 = (gcnew System::Windows::Forms::Label());
    this->label29 = (gcnew System::Windows::Forms::Label());
    this->button2 = (gcnew System::Windows::Forms::Button());
    this->button3 = (gcnew System::Windows::Forms::Button());
    this->checkBox10 = (gcnew System::Windows::Forms::CheckBox());
    this->checkBox24 = (gcnew System::Windows::Forms::CheckBox());
    this->button4 = (gcnew System::Windows::Forms::Button());
    this->label12 = (gcnew System::Windows::Forms::Label());
    this->textBox28 = (gcnew System::Windows::Forms::TextBox());
    this->label47 = (gcnew System::Windows::Forms::Label());
    this->textBox29 = (gcnew System::Windows::Forms::TextBox());
    this->label48 = (gcnew System::Windows::Forms::Label());
    this->label49 = (gcnew System::Windows::Forms::Label());
    this->numericUpDown4 = (gcnew System::Windows::Forms::NumericUpDown());
    this->label50 = (gcnew System::Windows::Forms::Label());
    this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
    this->tabControl1->SuspendLayout();
    this->tabPage1->SuspendLayout();
    //this->tabControl2->SuspendLayout();
    this->tabPage4->SuspendLayout();
    this->tabPage13->SuspendLayout();
    this->menuStrip2->SuspendLayout();
    this->tabPage10->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->BeginInit();
    this->tabPage12->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->BeginInit();
    this->tabPage11->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown3))->BeginInit();
    this->tabPage2->SuspendLayout();
    this->tabPage5->SuspendLayout();
    this->tabPage6->SuspendLayout();
    this->tabPage7->SuspendLayout();
    this->tabPage8->SuspendLayout();
    this->tabPage9->SuspendLayout();
    this->menuStrip1->SuspendLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown4))->BeginInit();
    this->SuspendLayout();
    //
    // tabControl1
    //
    this->tabControl1->Controls->Add(this->tabPage1);
    this->tabControl1->Controls->Add(this->tabPage13);
    this->tabControl1->Controls->Add(this->tabPage10);
    this->tabControl1->Controls->Add(this->tabPage12);
    this->tabControl1->Controls->Add(this->tabPage11);
    this->tabControl1->Controls->Add(this->tabPage2);
    this->tabControl1->Controls->Add(this->tabPage5);
    this->tabControl1->Controls->Add(this->tabPage6);
    this->tabControl1->Controls->Add(this->tabPage7);
    this->tabControl1->Controls->Add(this->tabPage8);
    this->tabControl1->Controls->Add(this->tabPage9);
    this->tabControl1->Enabled = false;
    this->tabControl1->Location = System::Drawing::Point(2, 24);
    this->tabControl1->Margin = System::Windows::Forms::Padding(2);
    this->tabControl1->Name = L"tabControl1";
    this->tabControl1->SelectedIndex = 0;
    this->tabControl1->Size = System::Drawing::Size(1024, 451);
    this->tabControl1->TabIndex = 1;
    this->tabControl1->Visible = false;
    //
    // tabPage1
    //
    this->tabPage1->Controls->Add(this->textBox2);
    this->tabPage1->Controls->Add(this->label6);
    this->tabPage1->Controls->Add(this->textBox1);
    this->tabPage1->Controls->Add(this->label5);
    this->tabPage1->Controls->Add(this->label4);
    this->tabPage1->Controls->Add(this->checkBox7);
    this->tabPage1->Controls->Add(this->checkBox6);
    this->tabPage1->Controls->Add(this->checkBox5);
    this->tabPage1->Controls->Add(this->checkBox4);
    this->tabPage1->Controls->Add(this->checkBox3);
    this->tabPage1->Controls->Add(this->checkBox2);
    this->tabPage1->Controls->Add(this->checkBox1);
    this->tabPage1->Controls->Add(this->textBox10);
    this->tabPage1->Controls->Add(this->label22);
    //this->tabPage1->Controls->Add(this->tabControl2);
    this->tabPage1->Controls->Add(this->label9);
    this->tabPage1->Controls->Add(this->textBox3);
    this->tabPage1->Controls->Add(this->label7);
    this->tabPage1->Controls->Add(this->label3);
    this->tabPage1->Controls->Add(this->checkedListBox3);
    this->tabPage1->Controls->Add(this->label2);
    this->tabPage1->Controls->Add(this->label1);
    this->tabPage1->Controls->Add(this->checkedListBox2);
    this->tabPage1->Controls->Add(this->checkedListBox1);
    this->tabPage1->Location = System::Drawing::Point(4, 22);
    this->tabPage1->Margin = System::Windows::Forms::Padding(2);
    this->tabPage1->Name = L"tabPage1";
    this->tabPage1->Padding = System::Windows::Forms::Padding(2);
    this->tabPage1->Size = System::Drawing::Size(1016, 425);
    this->tabPage1->TabIndex = 0;
    this->tabPage1->Text = L"Analysis Options";
    this->tabPage1->UseVisualStyleBackColor = true;
    //
    // textBox2
    //
    this->textBox2->Location = System::Drawing::Point(127, 397);
    this->textBox2->Margin = System::Windows::Forms::Padding(2);
    this->textBox2->Name = L"textBox2";
    this->textBox2->Size = System::Drawing::Size(182, 20);
    this->textBox2->TabIndex = 112;
    //
    // label6
    //
    this->label6->AutoSize = true;
    this->label6->BackColor = System::Drawing::SystemColors::Window;
    this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label6->Location = System::Drawing::Point(12, 402);
    this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label6->Name = L"label6";
    this->label6->Size = System::Drawing::Size(101, 13);
    this->label6->TabIndex = 111;
    this->label6->Text = L"Originating Post:";
    //
    // textBox1
    //
    this->textBox1->Location = System::Drawing::Point(127, 373);
    this->textBox1->Margin = System::Windows::Forms::Padding(2);
    this->textBox1->Name = L"textBox1";
    this->textBox1->Size = System::Drawing::Size(182, 20);
    this->textBox1->TabIndex = 110;
    //
    // label5
    //
    this->label5->AutoSize = true;
    this->label5->BackColor = System::Drawing::SystemColors::Window;
    this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label5->Location = System::Drawing::Point(12, 376);
    this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label5->Name = L"label5";
    this->label5->Size = System::Drawing::Size(67, 13);
    this->label5->TabIndex = 109;
    this->label5->Text = L"Username:";
    //
    // label4
    //
    this->label4->AutoSize = true;
    this->label4->BackColor = System::Drawing::SystemColors::Window;
    this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label4->ForeColor = System::Drawing::SystemColors::Desktop;
    this->label4->Location = System::Drawing::Point(41, 350);
    this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label4->Name = L"label4";
    this->label4->Size = System::Drawing::Size(267, 17);
    this->label4->TabIndex = 108;
    this->label4->Text = L"Driver Reference Table Update Info";
    //
    // checkBox7
    //
    this->checkBox7->AutoSize = true;
    this->checkBox7->Location = System::Drawing::Point(512, 7);
    this->checkBox7->Margin = System::Windows::Forms::Padding(2);
    this->checkBox7->Name = L"checkBox7";
    this->checkBox7->Size = System::Drawing::Size(15, 14);
    this->checkBox7->TabIndex = 80;
    this->checkBox7->UseVisualStyleBackColor = true;
    this->checkBox7->Click += gcnew System::EventHandler(this, &Form1::checkBox7_CheckedChanged);
    //
    // checkBox6
    //
    this->checkBox6->AutoSize = true;
    this->checkBox6->Location = System::Drawing::Point(176, 6);
    this->checkBox6->Margin = System::Windows::Forms::Padding(2);
    this->checkBox6->Name = L"checkBox6";
    this->checkBox6->Size = System::Drawing::Size(15, 14);
    this->checkBox6->TabIndex = 79;
    this->checkBox6->UseVisualStyleBackColor = true;
    this->checkBox6->Click += gcnew System::EventHandler(this, &Form1::checkBox6_CheckedChanged);
    //
    // checkBox5
    //
    this->checkBox5->AutoSize = true;
    this->checkBox5->Location = System::Drawing::Point(15, 7);
    this->checkBox5->Margin = System::Windows::Forms::Padding(2);
    this->checkBox5->Name = L"checkBox5";
    this->checkBox5->Size = System::Drawing::Size(15, 14);
    this->checkBox5->TabIndex = 29;
    this->checkBox5->UseVisualStyleBackColor = true;
    this->checkBox5->Click += gcnew System::EventHandler(this, &Form1::checkBox5_CheckedChanged);
    //
    // checkBox4
    //
    this->checkBox4->AutoSize = true;
    this->checkBox4->Location = System::Drawing::Point(658, 401);
    this->checkBox4->Margin = System::Windows::Forms::Padding(2);
    this->checkBox4->Name = L"checkBox4";
    this->checkBox4->Size = System::Drawing::Size(225, 17);
    this->checkBox4->TabIndex = 78;
    this->checkBox4->Text = L"Online only if symbols are WRONG (faster)";
    this->checkBox4->UseVisualStyleBackColor = true;
    this->checkBox4->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox4Change);
    //
    // checkBox3
    //
    this->checkBox3->AutoSize = true;
    this->checkBox3->Location = System::Drawing::Point(658, 381);
    this->checkBox3->Margin = System::Windows::Forms::Padding(2);
    this->checkBox3->Name = L"checkBox3";
    this->checkBox3->Size = System::Drawing::Size(231, 17);
    this->checkBox3->TabIndex = 77;
    this->checkBox3->Text = L"Online for module warnings (robust symbols)";
    this->checkBox3->UseVisualStyleBackColor = true;
    this->checkBox3->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox3Change);
    //
    // checkBox2
    //
    this->checkBox2->AutoSize = true;
    this->checkBox2->Location = System::Drawing::Point(718, 314);
    this->checkBox2->Margin = System::Windows::Forms::Padding(2);
    this->checkBox2->Name = L"checkBox2";
    this->checkBox2->Size = System::Drawing::Size(15, 14);
    this->checkBox2->TabIndex = 76;
    this->checkBox2->UseVisualStyleBackColor = true;
    this->checkBox2->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox2Change);
    //
    // checkBox1
    //
    this->checkBox1->AutoSize = true;
    this->checkBox1->Location = System::Drawing::Point(718, 289);
    this->checkBox1->Margin = System::Windows::Forms::Padding(2);
    this->checkBox1->Name = L"checkBox1";
    this->checkBox1->Size = System::Drawing::Size(15, 14);
    this->checkBox1->TabIndex = 75;
    this->checkBox1->UseVisualStyleBackColor = true;
    this->checkBox1->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox1Change);
    //
    // textBox10
    //
    this->textBox10->Location = System::Drawing::Point(621, 311);
    this->textBox10->Margin = System::Windows::Forms::Padding(2);
    this->textBox10->Name = L"textBox10";
    this->textBox10->Size = System::Drawing::Size(143, 20);
    this->textBox10->TabIndex = 74;
    //
    // label22
    //
    this->label22->AutoSize = true;
    this->label22->BackColor = System::Drawing::SystemColors::Window;
    this->label22->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label22->Location = System::Drawing::Point(507, 315);
    this->label22->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label22->Name = L"label22";
    this->label22->Size = System::Drawing::Size(97, 13);
    this->label22->TabIndex = 73;
    this->label22->Text = L"Online Symbols:";
    ////
    //// tabControl2
    ////
    //this->tabControl2->Controls->Add(this->tabPage3);
    //this->tabControl2->Controls->Add(this->tabPage4);
    //this->tabControl2->Location = System::Drawing::Point(14, 280);
    //this->tabControl2->Margin = System::Windows::Forms::Padding(2);
    //this->tabControl2->Name = L"tabControl2";
    //this->tabControl2->SelectedIndex = 0;
    //this->tabControl2->Size = System::Drawing::Size(459, 68);
    //this->tabControl2->TabIndex = 65;
    //
    // tabPage3
    //
    this->tabPage3->Location = System::Drawing::Point(4, 22);
    this->tabPage3->Margin = System::Windows::Forms::Padding(2);
    this->tabPage3->Name = L"tabPage3";
    this->tabPage3->Padding = System::Windows::Forms::Padding(2);
    this->tabPage3->Size = System::Drawing::Size(451, 42);
    this->tabPage3->TabIndex = 0;
    this->tabPage3->Text = L" ";
    this->tabPage3->UseVisualStyleBackColor = true;
    //
    // tabPage4
    //
    //this->tabPage4->Controls->Add(this->button10);
    //this->tabPage4->Controls->Add(this->comboBox1);
    //this->tabPage4->Controls->Add(this->button8);
    this->tabPage4->Location = System::Drawing::Point(4, 22);
    this->tabPage4->Margin = System::Windows::Forms::Padding(2);
    this->tabPage4->Name = L"tabPage4";
    this->tabPage4->Padding = System::Windows::Forms::Padding(2);
    this->tabPage4->Size = System::Drawing::Size(451, 42);
    this->tabPage4->TabIndex = 1;
    this->tabPage4->Text = L"kd.exe Path";
    this->tabPage4->UseVisualStyleBackColor = true;
    ////
    //// button10
    ////
    //this->button10->Location = System::Drawing::Point(355, 10);
    //this->button10->Name = L"button10";
    //this->button10->Size = System::Drawing::Size(91, 23);
    //this->button10->TabIndex = 3;
    //this->button10->Text = L"Clear List";
    //this->button10->UseVisualStyleBackColor = true;
    //this->button10->Click += gcnew System::EventHandler(this, &Form1::button10_Click);
    ////
    //// comboBox1
    ////
    //this->comboBox1->DropDownWidth = 500;
    //this->comboBox1->FormattingEnabled = true;
    //this->comboBox1->Location = System::Drawing::Point(7, 10);
    //this->comboBox1->Name = L"comboBox1";
    //this->comboBox1->Size = System::Drawing::Size(292, 21);
    //this->comboBox1->TabIndex = 2;
    ////
    //// button8
    ////
    //this->button8->Location = System::Drawing::Point(304, 10);
    //this->button8->Name = L"button8";
    //this->button8->Size = System::Drawing::Size(40, 23);
    //this->button8->TabIndex = 1;
    //this->button8->Text = L"...";
    //this->button8->UseVisualStyleBackColor = true;
    //this->button8->Click += gcnew System::EventHandler(this, &Form1::browse);
    //
    // label9
    //
    this->label9->AutoSize = true;
    this->label9->BackColor = System::Drawing::SystemColors::Window;
    this->label9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label9->Location = System::Drawing::Point(507, 392);
    this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label9->Name = L"label9";
    this->label9->Size = System::Drawing::Size(139, 13);
    this->label9->TabIndex = 59;
    this->label9->Text = L"Local Symbols Options:";
    //
    // textBox3
    //
    this->textBox3->Location = System::Drawing::Point(621, 288);
    this->textBox3->Margin = System::Windows::Forms::Padding(2);
    this->textBox3->Name = L"textBox3";
    this->textBox3->Size = System::Drawing::Size(143, 20);
    this->textBox3->TabIndex = 58;
    //
    // label7
    //
    this->label7->AutoSize = true;
    this->label7->BackColor = System::Drawing::SystemColors::Window;
    this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label7->Location = System::Drawing::Point(507, 292);
    this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label7->Name = L"label7";
    this->label7->Size = System::Drawing::Size(92, 13);
    this->label7->TabIndex = 57;
    this->label7->Text = L"Local Symbols:";
    //
    // label3
    //
    this->label3->AutoSize = true;
    this->label3->BackColor = System::Drawing::SystemColors::Window;
    this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label3->ForeColor = System::Drawing::SystemColors::Desktop;
    this->label3->Location = System::Drawing::Point(529, 6);
    this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label3->Name = L"label3";
    this->label3->Size = System::Drawing::Size(285, 17);
    this->label3->TabIndex = 51;
    this->label3->Text = L"Formatting and Miscellaneous Options";
    //
    // checkedListBox3
    //
    this->checkedListBox3->CheckOnClick = true;
    this->checkedListBox3->FormattingEnabled = true;
    this->checkedListBox3->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
        L"Add spaces between lines in importantInfo.txt",
            L"No BBCode in importantInfo.txt", L"Turn Off BBCode Parsing in HTML Viewers", L"Turn off all BBCode", L"Turn off BBCode in Code Boxes",
            L"Old drivers shown in red", L"Treat all files as possible .dmps (more thorough, but slower)"
    });
    this->checkedListBox3->Location = System::Drawing::Point(510, 22);
    this->checkedListBox3->Margin = System::Windows::Forms::Padding(2);
    this->checkedListBox3->Name = L"checkedListBox3";
    this->checkedListBox3->Size = System::Drawing::Size(342, 214);
    this->checkedListBox3->TabIndex = 50;
    this->checkedListBox3->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::checkList3Change);
    //
    // label2
    //
    this->label2->AutoSize = true;
    this->label2->BackColor = System::Drawing::SystemColors::Window;
    this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label2->ForeColor = System::Drawing::SystemColors::Desktop;
    this->label2->Location = System::Drawing::Point(199, 6);
    this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label2->Name = L"label2";
    this->label2->Size = System::Drawing::Size(118, 17);
    this->label2->TabIndex = 49;
    this->label2->Text = L"Output Options";
    //
    // label1
    //
    this->label1->AutoSize = true;
    this->label1->BackColor = System::Drawing::SystemColors::Window;
    this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label1->ForeColor = System::Drawing::SystemColors::Desktop;
    this->label1->Location = System::Drawing::Point(44, 6);
    this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label1->Name = L"label1";
    this->label1->Size = System::Drawing::Size(102, 17);
    this->label1->TabIndex = 48;
    this->label1->Text = L"Files to Save";
    //
    // checkedListBox2
    //
    this->checkedListBox2->CheckOnClick = true;
    this->checkedListBox2->FormattingEnabled = true;
    this->checkedListBox2->Items->AddRange(gcnew cli::array< System::Object^  >(28) {
        L"Loading Dump File", L"Kernel Version",
            L"Missing Service Pack", L"Built by:", L"Debug session time:", L"System Uptime:", L"BugCheck ", L"*** WARNING:", L"*** ERROR:  ",
            L"Probably caused by", L"DEFAULT_BUCKET_ID:", L"VERIFIER", L"BUGCHECK_STR:", L"Bugcheck Info", L"PROCESS_NAME:", L"FAILURE_BUCKET_ID:",
            L"Bugcheck code ", L"Error Code", L"DISK_HARDWARE_ERROR", L"Arguments ", L"CPUID", L"MaxSpeed:", L"CurrentSpeed:", L"BiosVersion =",
            L"BiosReleaseDate =", L"SystemManufacturer = ", L"SystemProductName = ", L"Overclock Ratio:"
    });
    this->checkedListBox2->Location = System::Drawing::Point(174, 22);
    this->checkedListBox2->Margin = System::Windows::Forms::Padding(2);
    this->checkedListBox2->Name = L"checkedListBox2";
    this->checkedListBox2->Size = System::Drawing::Size(156, 214);
    this->checkedListBox2->TabIndex = 47;
    this->checkedListBox2->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::checkList2Change);
    //
    // checkedListBox1
    //
    this->checkedListBox1->CheckOnClick = true;
    this->checkedListBox1->FormattingEnabled = true;
    this->checkedListBox1->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
        L"dmps.txt", L"drivers.txt", L"3rdPartyDriversName.txt",
            L"3rdPartyDriversDate.txt", L"importantInfo.txt", L"stack.txt", L"missingFromDRT.txt", L"unloadedDrivers.txt", L"SMBIOS.txt",
            L"template.txt", L"_99-debug.txt", L"_98-debug.txt", L"_97-debug.txt", L"_95-debug.txt", L"_88-debug.txt"
    });
    this->checkedListBox1->Location = System::Drawing::Point(14, 22);
    this->checkedListBox1->Margin = System::Windows::Forms::Padding(2);
    this->checkedListBox1->Name = L"checkedListBox1";
    this->checkedListBox1->Size = System::Drawing::Size(155, 214);
    this->checkedListBox1->TabIndex = 46;
    this->checkedListBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::checkList1Change);
    //
    // tabPage13
    //
    this->tabPage13->Controls->Add(this->button11);
    this->tabPage13->Controls->Add(this->label34);
    this->tabPage13->Controls->Add(this->label35);
    this->tabPage13->Controls->Add(this->label36);
    this->tabPage13->Controls->Add(this->textBox18);
    this->tabPage13->Controls->Add(this->textBox19);
    this->tabPage13->Controls->Add(this->textBox20);
    this->tabPage13->Controls->Add(this->label37);
    this->tabPage13->Controls->Add(this->label38);
    this->tabPage13->Controls->Add(this->label39);
    this->tabPage13->Controls->Add(this->label40);
    this->tabPage13->Controls->Add(this->textBox21);
    this->tabPage13->Controls->Add(this->textBox22);
    this->tabPage13->Controls->Add(this->textBox23);
    this->tabPage13->Controls->Add(this->label41);
    this->tabPage13->Controls->Add(this->label30);
    this->tabPage13->Controls->Add(this->label31);
    this->tabPage13->Controls->Add(this->label32);
    this->tabPage13->Controls->Add(this->textBox15);
    this->tabPage13->Controls->Add(this->textBox16);
    this->tabPage13->Controls->Add(this->textBox17);
    this->tabPage13->Controls->Add(this->label33);
    this->tabPage13->Controls->Add(this->menuStrip2);
    this->tabPage13->Controls->Add(this->label21);
    this->tabPage13->Controls->Add(this->label20);
    this->tabPage13->Controls->Add(this->label19);
    this->tabPage13->Controls->Add(this->textBox9);
    this->tabPage13->Controls->Add(this->textBox8);
    this->tabPage13->Controls->Add(this->textBox14);
    this->tabPage13->Controls->Add(this->label8);
    this->tabPage13->Controls->Add(this->textBox5);
    this->tabPage13->Controls->Add(this->label10);
    this->tabPage13->Controls->Add(this->textBox6);
    this->tabPage13->Controls->Add(this->label11);
    this->tabPage13->Location = System::Drawing::Point(4, 22);
    this->tabPage13->Name = L"tabPage13";
    this->tabPage13->Padding = System::Windows::Forms::Padding(3);
    this->tabPage13->Size = System::Drawing::Size(1016, 425);
    this->tabPage13->TabIndex = 10;
    this->tabPage13->Text = L"Old Driver After";
    this->tabPage13->UseVisualStyleBackColor = true;
    //
    // button11
    //
    this->button11->Location = System::Drawing::Point(305, 12);
    this->button11->Name = L"button11";
    this->button11->Size = System::Drawing::Size(124, 23);
    this->button11->TabIndex = 140;
    this->button11->Text = L"Windows 8.1 and 10";
    this->button11->UseVisualStyleBackColor = true;
    this->button11->Click += gcnew System::EventHandler(this, &Form1::button11_Click);
    //
    // label34
    //
    this->label34->AutoSize = true;
    this->label34->Location = System::Drawing::Point(496, 70);
    this->label34->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label34->Name = L"label34";
    this->label34->Size = System::Drawing::Size(29, 13);
    this->label34->TabIndex = 139;
    this->label34->Text = L"Year";
    //
    // label35
    //
    this->label35->AutoSize = true;
    this->label35->Location = System::Drawing::Point(468, 70);
    this->label35->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label35->Name = L"label35";
    this->label35->Size = System::Drawing::Size(26, 13);
    this->label35->TabIndex = 138;
    this->label35->Text = L"Day";
    //
    // label36
    //
    this->label36->AutoSize = true;
    this->label36->Location = System::Drawing::Point(426, 70);
    this->label36->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label36->Name = L"label36";
    this->label36->Size = System::Drawing::Size(37, 13);
    this->label36->TabIndex = 137;
    this->label36->Text = L"Month";
    //
    // textBox18
    //
    this->textBox18->Location = System::Drawing::Point(428, 85);
    this->textBox18->Margin = System::Windows::Forms::Padding(2);
    this->textBox18->Name = L"textBox18";
    this->textBox18->Size = System::Drawing::Size(37, 20);
    this->textBox18->TabIndex = 136;
    //
    // textBox19
    //
    this->textBox19->Location = System::Drawing::Point(471, 85);
    this->textBox19->Margin = System::Windows::Forms::Padding(2);
    this->textBox19->Name = L"textBox19";
    this->textBox19->Size = System::Drawing::Size(26, 20);
    this->textBox19->TabIndex = 135;
    //
    // textBox20
    //
    this->textBox20->Location = System::Drawing::Point(500, 85);
    this->textBox20->Margin = System::Windows::Forms::Padding(2);
    this->textBox20->Name = L"textBox20";
    this->textBox20->Size = System::Drawing::Size(43, 20);
    this->textBox20->TabIndex = 134;
    //
    // label37
    //
    this->label37->AutoSize = true;
    this->label37->BackColor = System::Drawing::SystemColors::Window;
    this->label37->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label37->Location = System::Drawing::Point(222, 88);
    this->label37->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label37->Name = L"label37";
    this->label37->Size = System::Drawing::Size(174, 13);
    this->label37->TabIndex = 133;
    this->label37->Text = L"Windows XP Old Driver After:";
    //
    // label38
    //
    this->label38->AutoSize = true;
    this->label38->Location = System::Drawing::Point(496, 110);
    this->label38->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label38->Name = L"label38";
    this->label38->Size = System::Drawing::Size(29, 13);
    this->label38->TabIndex = 132;
    this->label38->Text = L"Year";
    //
    // label39
    //
    this->label39->AutoSize = true;
    this->label39->Location = System::Drawing::Point(468, 110);
    this->label39->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label39->Name = L"label39";
    this->label39->Size = System::Drawing::Size(26, 13);
    this->label39->TabIndex = 131;
    this->label39->Text = L"Day";
    //
    // label40
    //
    this->label40->AutoSize = true;
    this->label40->Location = System::Drawing::Point(426, 110);
    this->label40->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label40->Name = L"label40";
    this->label40->Size = System::Drawing::Size(37, 13);
    this->label40->TabIndex = 130;
    this->label40->Text = L"Month";
    //
    // textBox21
    //
    this->textBox21->Location = System::Drawing::Point(428, 125);
    this->textBox21->Margin = System::Windows::Forms::Padding(2);
    this->textBox21->Name = L"textBox21";
    this->textBox21->Size = System::Drawing::Size(37, 20);
    this->textBox21->TabIndex = 129;
    //
    // textBox22
    //
    this->textBox22->Location = System::Drawing::Point(471, 125);
    this->textBox22->Margin = System::Windows::Forms::Padding(2);
    this->textBox22->Name = L"textBox22";
    this->textBox22->Size = System::Drawing::Size(26, 20);
    this->textBox22->TabIndex = 128;
    //
    // textBox23
    //
    this->textBox23->Location = System::Drawing::Point(500, 125);
    this->textBox23->Margin = System::Windows::Forms::Padding(2);
    this->textBox23->Name = L"textBox23";
    this->textBox23->Size = System::Drawing::Size(43, 20);
    this->textBox23->TabIndex = 127;
    //
    // label41
    //
    this->label41->AutoSize = true;
    this->label41->BackColor = System::Drawing::SystemColors::Window;
    this->label41->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label41->Location = System::Drawing::Point(222, 127);
    this->label41->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label41->Name = L"label41";
    this->label41->Size = System::Drawing::Size(186, 13);
    this->label41->TabIndex = 126;
    this->label41->Text = L"Windows Vista Old Driver After:";
    //
    // label30
    //
    this->label30->AutoSize = true;
    this->label30->Location = System::Drawing::Point(497, 192);
    this->label30->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label30->Name = L"label30";
    this->label30->Size = System::Drawing::Size(29, 13);
    this->label30->TabIndex = 125;
    this->label30->Text = L"Year";
    //
    // label31
    //
    this->label31->AutoSize = true;
    this->label31->Location = System::Drawing::Point(468, 192);
    this->label31->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label31->Name = L"label31";
    this->label31->Size = System::Drawing::Size(26, 13);
    this->label31->TabIndex = 124;
    this->label31->Text = L"Day";
    //
    // label32
    //
    this->label32->AutoSize = true;
    this->label32->Location = System::Drawing::Point(427, 192);
    this->label32->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label32->Name = L"label32";
    this->label32->Size = System::Drawing::Size(37, 13);
    this->label32->TabIndex = 123;
    this->label32->Text = L"Month";
    //
    // textBox15
    //
    this->textBox15->Location = System::Drawing::Point(428, 209);
    this->textBox15->Margin = System::Windows::Forms::Padding(2);
    this->textBox15->Name = L"textBox15";
    this->textBox15->Size = System::Drawing::Size(37, 20);
    this->textBox15->TabIndex = 122;
    //
    // textBox16
    //
    this->textBox16->Location = System::Drawing::Point(471, 209);
    this->textBox16->Margin = System::Windows::Forms::Padding(2);
    this->textBox16->Name = L"textBox16";
    this->textBox16->Size = System::Drawing::Size(26, 20);
    this->textBox16->TabIndex = 121;
    //
    // textBox17
    //
    this->textBox17->Location = System::Drawing::Point(500, 209);
    this->textBox17->Margin = System::Windows::Forms::Padding(2);
    this->textBox17->Name = L"textBox17";
    this->textBox17->Size = System::Drawing::Size(43, 20);
    this->textBox17->TabIndex = 120;
    //
    // label33
    //
    this->label33->AutoSize = true;
    this->label33->BackColor = System::Drawing::SystemColors::Window;
    this->label33->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label33->Location = System::Drawing::Point(222, 211);
    this->label33->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label33->Name = L"label33";
    this->label33->Size = System::Drawing::Size(165, 13);
    this->label33->TabIndex = 119;
    this->label33->Text = L"Windows 8 Old Driver After:";
    //
    // menuStrip2
    //
    this->menuStrip2->Dock = System::Windows::Forms::DockStyle::None;
    this->menuStrip2->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
        this->xPDatesToolStripMenuItem,
            this->vistaDatesToolStripMenuItem, this->datesToolStripMenuItem, this->datesToolStripMenuItem1
    });
    this->menuStrip2->Location = System::Drawing::Point(234, 38);
    this->menuStrip2->Name = L"menuStrip2";
    this->menuStrip2->Padding = System::Windows::Forms::Padding(4, 2, 0, 2);
    this->menuStrip2->Size = System::Drawing::Size(261, 24);
    this->menuStrip2->TabIndex = 102;
    this->menuStrip2->Text = L"menuStrip2";
    //
    // xPDatesToolStripMenuItem
    //
    this->xPDatesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {
        this->rTMAugToolStripMenuItem,
            this->sP1Sep92002ToolStripMenuItem, this->sP2Aug252004ToolStripMenuItem, this->sP2bAug2006ToolStripMenuItem, this->sP2cAug102007ToolStripMenuItem,
            this->sP3Apr212008RTMToolStripMenuItem
    });
    this->xPDatesToolStripMenuItem->Name = L"xPDatesToolStripMenuItem";
    this->xPDatesToolStripMenuItem->Size = System::Drawing::Size(65, 20);
    this->xPDatesToolStripMenuItem->Text = L"XP Dates";
    //
    // rTMAugToolStripMenuItem
    //
    this->rTMAugToolStripMenuItem->Name = L"rTMAugToolStripMenuItem";
    this->rTMAugToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->rTMAugToolStripMenuItem->Text = L"RTM: Aug. 24, 2001";
    this->rTMAugToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::XPRTM);
    //
    // sP1Sep92002ToolStripMenuItem
    //
    this->sP1Sep92002ToolStripMenuItem->Name = L"sP1Sep92002ToolStripMenuItem";
    this->sP1Sep92002ToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->sP1Sep92002ToolStripMenuItem->Text = L"SP1: Sep. 9, 2002";
    this->sP1Sep92002ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::XPSP1);
    //
    // sP2Aug252004ToolStripMenuItem
    //
    this->sP2Aug252004ToolStripMenuItem->Name = L"sP2Aug252004ToolStripMenuItem";
    this->sP2Aug252004ToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->sP2Aug252004ToolStripMenuItem->Text = L"SP2: Aug. 25, 2004";
    this->sP2Aug252004ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::XPSP2);
    //
    // sP2bAug2006ToolStripMenuItem
    //
    this->sP2bAug2006ToolStripMenuItem->Name = L"sP2bAug2006ToolStripMenuItem";
    this->sP2bAug2006ToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->sP2bAug2006ToolStripMenuItem->Text = L"SP2b: Aug. 2006";
    this->sP2bAug2006ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::XPSP2b);
    //
    // sP2cAug102007ToolStripMenuItem
    //
    this->sP2cAug102007ToolStripMenuItem->Name = L"sP2cAug102007ToolStripMenuItem";
    this->sP2cAug102007ToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->sP2cAug102007ToolStripMenuItem->Text = L"SP2c: Aug. 10, 2007";
    this->sP2cAug102007ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::XPSP2c);
    //
    // sP3Apr212008RTMToolStripMenuItem
    //
    this->sP3Apr212008RTMToolStripMenuItem->Name = L"sP3Apr212008RTMToolStripMenuItem";
    this->sP3Apr212008RTMToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->sP3Apr212008RTMToolStripMenuItem->Text = L"SP3: Apr. 21, 2008 (RTM)";
    this->sP3Apr212008RTMToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::XPSP3);
    //
    // vistaDatesToolStripMenuItem
    //
    this->vistaDatesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
        this->rTMNov82007ToolStripMenuItem,
            this->feb42008ToolStripMenuItem, this->sP2Apr282009RTMToolStripMenuItem
    });
    this->vistaDatesToolStripMenuItem->Name = L"vistaDatesToolStripMenuItem";
    this->vistaDatesToolStripMenuItem->Size = System::Drawing::Size(76, 20);
    this->vistaDatesToolStripMenuItem->Text = L"Vista Dates";
    //
    // rTMNov82007ToolStripMenuItem
    //
    this->rTMNov82007ToolStripMenuItem->Name = L"rTMNov82007ToolStripMenuItem";
    this->rTMNov82007ToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->rTMNov82007ToolStripMenuItem->Text = L"RTM: Nov. 8, 2006 \?";
    this->rTMNov82007ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::VistaRTM);
    //
    // feb42008ToolStripMenuItem
    //
    this->feb42008ToolStripMenuItem->Name = L"feb42008ToolStripMenuItem";
    this->feb42008ToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->feb42008ToolStripMenuItem->Text = L"SP1: Feb. 4, 2008";
    this->feb42008ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::VistaSP1);
    //
    // sP2Apr282009RTMToolStripMenuItem
    //
    this->sP2Apr282009RTMToolStripMenuItem->Name = L"sP2Apr282009RTMToolStripMenuItem";
    this->sP2Apr282009RTMToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->sP2Apr282009RTMToolStripMenuItem->Text = L"SP2: Apr. 28, 2009 (RTM)";
    this->sP2Apr282009RTMToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::VistaSP2);
    //
    // datesToolStripMenuItem
    //
    this->datesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
        this->rTMJul222009ToolStripMenuItem,
            this->sP1Feb112011ToolStripMenuItem
    });
    this->datesToolStripMenuItem->Name = L"datesToolStripMenuItem";
    this->datesToolStripMenuItem->Size = System::Drawing::Size(57, 20);
    this->datesToolStripMenuItem->Text = L"7 Dates";
    //
    // rTMJul222009ToolStripMenuItem
    //
    this->rTMJul222009ToolStripMenuItem->Name = L"rTMJul222009ToolStripMenuItem";
    this->rTMJul222009ToolStripMenuItem->Size = System::Drawing::Size(166, 22);
    this->rTMJul222009ToolStripMenuItem->Text = L"RTM: Jul. 22, 2009";
    this->rTMJul222009ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::SevenRTM);
    //
    // sP1Feb112011ToolStripMenuItem
    //
    this->sP1Feb112011ToolStripMenuItem->Name = L"sP1Feb112011ToolStripMenuItem";
    this->sP1Feb112011ToolStripMenuItem->Size = System::Drawing::Size(166, 22);
    this->sP1Feb112011ToolStripMenuItem->Text = L"SP1: Feb. 9, 2011";
    this->sP1Feb112011ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::SevenSP1);
    //
    // datesToolStripMenuItem1
    //
    this->datesToolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->rTMAugust12012ToolStripMenuItem });
    this->datesToolStripMenuItem1->Name = L"datesToolStripMenuItem1";
    this->datesToolStripMenuItem1->Size = System::Drawing::Size(57, 20);
    this->datesToolStripMenuItem1->Text = L"8 Dates";
    //
    // rTMAugust12012ToolStripMenuItem
    //
    this->rTMAugust12012ToolStripMenuItem->Name = L"rTMAugust12012ToolStripMenuItem";
    this->rTMAugust12012ToolStripMenuItem->Size = System::Drawing::Size(168, 22);
    this->rTMAugust12012ToolStripMenuItem->Text = L"RTM: Aug. 1, 2012";
    this->rTMAugust12012ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::EightRTM);
    //
    // label21
    //
    this->label21->AutoSize = true;
    this->label21->Location = System::Drawing::Point(497, 153);
    this->label21->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label21->Name = L"label21";
    this->label21->Size = System::Drawing::Size(29, 13);
    this->label21->TabIndex = 118;
    this->label21->Text = L"Year";
    //
    // label20
    //
    this->label20->AutoSize = true;
    this->label20->Location = System::Drawing::Point(468, 153);
    this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label20->Name = L"label20";
    this->label20->Size = System::Drawing::Size(26, 13);
    this->label20->TabIndex = 117;
    this->label20->Text = L"Day";
    //
    // label19
    //
    this->label19->AutoSize = true;
    this->label19->Location = System::Drawing::Point(427, 153);
    this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label19->Name = L"label19";
    this->label19->Size = System::Drawing::Size(37, 13);
    this->label19->TabIndex = 116;
    this->label19->Text = L"Month";
    //
    // textBox9
    //
    this->textBox9->Location = System::Drawing::Point(428, 168);
    this->textBox9->Margin = System::Windows::Forms::Padding(2);
    this->textBox9->Name = L"textBox9";
    this->textBox9->Size = System::Drawing::Size(37, 20);
    this->textBox9->TabIndex = 115;
    //
    // textBox8
    //
    this->textBox8->Location = System::Drawing::Point(471, 168);
    this->textBox8->Margin = System::Windows::Forms::Padding(2);
    this->textBox8->Name = L"textBox8";
    this->textBox8->Size = System::Drawing::Size(26, 20);
    this->textBox8->TabIndex = 114;
    //
    // textBox14
    //
    this->textBox14->Location = System::Drawing::Point(500, 168);
    this->textBox14->Margin = System::Windows::Forms::Padding(2);
    this->textBox14->Name = L"textBox14";
    this->textBox14->Size = System::Drawing::Size(43, 20);
    this->textBox14->TabIndex = 113;
    //
    // label8
    //
    this->label8->AutoSize = true;
    this->label8->BackColor = System::Drawing::SystemColors::Window;
    this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label8->Location = System::Drawing::Point(222, 172);
    this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label8->Name = L"label8";
    this->label8->Size = System::Drawing::Size(165, 13);
    this->label8->TabIndex = 112;
    this->label8->Text = L"Windows 7 Old Driver After:";
    //
    // textBox5
    //
    this->textBox5->Location = System::Drawing::Point(336, 268);
    this->textBox5->Margin = System::Windows::Forms::Padding(2);
    this->textBox5->Name = L"textBox5";
    this->textBox5->Size = System::Drawing::Size(206, 20);
    this->textBox5->TabIndex = 111;
    //
    // label10
    //
    this->label10->AutoSize = true;
    this->label10->BackColor = System::Drawing::SystemColors::Window;
    this->label10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label10->Location = System::Drawing::Point(222, 272);
    this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label10->Name = L"label10";
    this->label10->Size = System::Drawing::Size(115, 13);
    this->label10->TabIndex = 110;
    this->label10->Text = L"Link Seen by User:";
    //
    // textBox6
    //
    this->textBox6->Location = System::Drawing::Point(336, 246);
    this->textBox6->Margin = System::Windows::Forms::Padding(2);
    this->textBox6->Name = L"textBox6";
    this->textBox6->Size = System::Drawing::Size(206, 20);
    this->textBox6->TabIndex = 109;
    //
    // label11
    //
    this->label11->AutoSize = true;
    this->label11->BackColor = System::Drawing::SystemColors::Window;
    this->label11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label11->Location = System::Drawing::Point(222, 250);
    this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label11->Name = L"label11";
    this->label11->Size = System::Drawing::Size(66, 13);
    this->label11->TabIndex = 108;
    this->label11->Text = L"URL Path:";
    //
    // tabPage10
    //
    this->tabPage10->Controls->Add(this->button12);
    this->tabPage10->Controls->Add(this->checkBox21);
    this->tabPage10->Controls->Add(this->button5);
    this->tabPage10->Controls->Add(this->checkBox16);
    this->tabPage10->Controls->Add(this->checkBox15);
    this->tabPage10->Controls->Add(this->checkBox14);
    this->tabPage10->Controls->Add(this->checkBox13);
    this->tabPage10->Controls->Add(this->textBox25);
    this->tabPage10->Controls->Add(this->numericUpDown1);
    this->tabPage10->Controls->Add(this->checkBox12);
    this->tabPage10->Controls->Add(this->checkBox11);
    this->tabPage10->Controls->Add(this->textBox24);
    this->tabPage10->Controls->Add(this->label45);
    this->tabPage10->Controls->Add(this->richTextBox13);
    this->tabPage10->Controls->Add(this->label44);
    this->tabPage10->Controls->Add(this->richTextBox12);
    this->tabPage10->Controls->Add(this->label43);
    this->tabPage10->Controls->Add(this->richTextBox11);
    this->tabPage10->Controls->Add(this->label42);
    this->tabPage10->Controls->Add(this->richTextBox10);
    this->tabPage10->Controls->Add(this->label24);
    this->tabPage10->Controls->Add(this->label23);
    this->tabPage10->Controls->Add(this->richTextBox7);
    this->tabPage10->Controls->Add(this->richTextBox6);
    this->tabPage10->Location = System::Drawing::Point(4, 22);
    this->tabPage10->Margin = System::Windows::Forms::Padding(2);
    this->tabPage10->Name = L"tabPage10";
    this->tabPage10->Padding = System::Windows::Forms::Padding(2);
    this->tabPage10->Size = System::Drawing::Size(1016, 425);
    this->tabPage10->TabIndex = 7;
    this->tabPage10->Text = L"Excluded Drivers";
    this->tabPage10->UseVisualStyleBackColor = true;
    //
    // button12
    //
    this->button12->Location = System::Drawing::Point(622, 9);
    this->button12->Name = L"button12";
    this->button12->Size = System::Drawing::Size(124, 23);
    this->button12->TabIndex = 141;
    this->button12->Text = L"Windows 8.1 and 10";
    this->button12->UseVisualStyleBackColor = true;
    this->button12->Click += gcnew System::EventHandler(this, &Form1::button12_Click);
    //
    // checkBox21
    //
    this->checkBox21->AutoSize = true;
    this->checkBox21->Location = System::Drawing::Point(104, 15);
    this->checkBox21->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox21->Name = L"checkBox21";
    this->checkBox21->Size = System::Drawing::Size(57, 17);
    this->checkBox21->TabIndex = 80;
    this->checkBox21->Text = L"Delete";
    this->checkBox21->UseVisualStyleBackColor = true;
    this->checkBox21->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox21_CheckedChanged);
    //
    // button5
    //
    this->button5->Location = System::Drawing::Point(306, 13);
    this->button5->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->button5->Name = L"button5";
    this->button5->Size = System::Drawing::Size(75, 22);
    this->button5->TabIndex = 79;
    this->button5->Text = L"Proceed";
    this->button5->UseVisualStyleBackColor = true;
    this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
    //
    // checkBox16
    //
    this->checkBox16->AutoSize = true;
    this->checkBox16->Location = System::Drawing::Point(797, 57);
    this->checkBox16->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox16->Name = L"checkBox16";
    this->checkBox16->Size = System::Drawing::Size(15, 14);
    this->checkBox16->TabIndex = 22;
    this->checkBox16->UseVisualStyleBackColor = true;
    //
    // checkBox15
    //
    this->checkBox15->AutoSize = true;
    this->checkBox15->Location = System::Drawing::Point(717, 57);
    this->checkBox15->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox15->Name = L"checkBox15";
    this->checkBox15->Size = System::Drawing::Size(15, 14);
    this->checkBox15->TabIndex = 21;
    this->checkBox15->UseVisualStyleBackColor = true;
    //
    // checkBox14
    //
    this->checkBox14->AutoSize = true;
    this->checkBox14->Location = System::Drawing::Point(639, 57);
    this->checkBox14->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox14->Name = L"checkBox14";
    this->checkBox14->Size = System::Drawing::Size(15, 14);
    this->checkBox14->TabIndex = 20;
    this->checkBox14->UseVisualStyleBackColor = true;
    //
    // checkBox13
    //
    this->checkBox13->AutoSize = true;
    this->checkBox13->Location = System::Drawing::Point(553, 57);
    this->checkBox13->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox13->Name = L"checkBox13";
    this->checkBox13->Size = System::Drawing::Size(15, 14);
    this->checkBox13->TabIndex = 19;
    this->checkBox13->UseVisualStyleBackColor = true;
    //
    // textBox25
    //
    this->textBox25->Location = System::Drawing::Point(374, 54);
    this->textBox25->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->textBox25->Name = L"textBox25";
    this->textBox25->Size = System::Drawing::Size(148, 20);
    this->textBox25->TabIndex = 18;
    //
    // numericUpDown1
    //
    this->numericUpDown1->Enabled = false;
    this->numericUpDown1->Location = System::Drawing::Point(176, 13);
    this->numericUpDown1->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->numericUpDown1->Name = L"numericUpDown1";
    this->numericUpDown1->Size = System::Drawing::Size(120, 20);
    this->numericUpDown1->TabIndex = 17;
    this->numericUpDown1->ValueChanged += gcnew System::EventHandler(this, &Form1::checkBox12_CheckedChanged);
    //
    // checkBox12
    //
    this->checkBox12->AutoSize = true;
    this->checkBox12->Location = System::Drawing::Point(56, 15);
    this->checkBox12->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox12->Name = L"checkBox12";
    this->checkBox12->Size = System::Drawing::Size(44, 17);
    this->checkBox12->TabIndex = 16;
    this->checkBox12->Text = L"Edit";
    this->checkBox12->UseVisualStyleBackColor = true;
    this->checkBox12->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox12_CheckedChanged);
    //
    // checkBox11
    //
    this->checkBox11->AutoSize = true;
    this->checkBox11->Location = System::Drawing::Point(8, 15);
    this->checkBox11->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox11->Name = L"checkBox11";
    this->checkBox11->Size = System::Drawing::Size(45, 17);
    this->checkBox11->TabIndex = 15;
    this->checkBox11->Text = L"Add";
    this->checkBox11->UseVisualStyleBackColor = true;
    this->checkBox11->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox11_CheckedChanged);
    //
    // textBox24
    //
    this->textBox24->Location = System::Drawing::Point(8, 54);
    this->textBox24->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->textBox24->Name = L"textBox24";
    this->textBox24->Size = System::Drawing::Size(361, 20);
    this->textBox24->TabIndex = 14;
    //
    // label45
    //
    this->label45->AutoSize = true;
    this->label45->Location = System::Drawing::Point(796, 37);
    this->label45->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label45->Name = L"label45";
    this->label45->Size = System::Drawing::Size(13, 13);
    this->label45->TabIndex = 13;
    this->label45->Text = L"8";
    //
    // richTextBox13
    //
    this->richTextBox13->Location = System::Drawing::Point(765, 80);
    this->richTextBox13->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox13->Name = L"richTextBox13";
    this->richTextBox13->ReadOnly = true;
    this->richTextBox13->Size = System::Drawing::Size(78, 431);
    this->richTextBox13->TabIndex = 12;
    this->richTextBox13->Text = L"";
    this->richTextBox13->WordWrap = false;
    //
    // label44
    //
    this->label44->AutoSize = true;
    this->label44->Location = System::Drawing::Point(716, 37);
    this->label44->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label44->Name = L"label44";
    this->label44->Size = System::Drawing::Size(13, 13);
    this->label44->TabIndex = 11;
    this->label44->Text = L"7";
    //
    // richTextBox12
    //
    this->richTextBox12->Location = System::Drawing::Point(685, 80);
    this->richTextBox12->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox12->Name = L"richTextBox12";
    this->richTextBox12->ReadOnly = true;
    this->richTextBox12->Size = System::Drawing::Size(78, 431);
    this->richTextBox12->TabIndex = 10;
    this->richTextBox12->Text = L"";
    this->richTextBox12->WordWrap = false;
    //
    // label43
    //
    this->label43->AutoSize = true;
    this->label43->Location = System::Drawing::Point(630, 37);
    this->label43->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label43->Name = L"label43";
    this->label43->Size = System::Drawing::Size(30, 13);
    this->label43->TabIndex = 9;
    this->label43->Text = L"Vista";
    //
    // richTextBox11
    //
    this->richTextBox11->Location = System::Drawing::Point(605, 80);
    this->richTextBox11->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox11->Name = L"richTextBox11";
    this->richTextBox11->ReadOnly = true;
    this->richTextBox11->Size = System::Drawing::Size(78, 431);
    this->richTextBox11->TabIndex = 8;
    this->richTextBox11->Text = L"";
    this->richTextBox11->WordWrap = false;
    //
    // label42
    //
    this->label42->AutoSize = true;
    this->label42->Location = System::Drawing::Point(550, 41);
    this->label42->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label42->Name = L"label42";
    this->label42->Size = System::Drawing::Size(21, 13);
    this->label42->TabIndex = 7;
    this->label42->Text = L"XP";
    //
    // richTextBox10
    //
    this->richTextBox10->Location = System::Drawing::Point(525, 80);
    this->richTextBox10->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox10->Name = L"richTextBox10";
    this->richTextBox10->ReadOnly = true;
    this->richTextBox10->Size = System::Drawing::Size(78, 431);
    this->richTextBox10->TabIndex = 6;
    this->richTextBox10->Text = L"";
    this->richTextBox10->WordWrap = false;
    //
    // label24
    //
    this->label24->AutoSize = true;
    this->label24->Location = System::Drawing::Point(413, 41);
    this->label24->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label24->Name = L"label24";
    this->label24->Size = System::Drawing::Size(61, 13);
    this->label24->TabIndex = 5;
    this->label24->Text = L"Driver Date";
    //
    // label23
    //
    this->label23->AutoSize = true;
    this->label23->Location = System::Drawing::Point(134, 37);
    this->label23->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label23->Name = L"label23";
    this->label23->Size = System::Drawing::Size(66, 13);
    this->label23->TabIndex = 4;
    this->label23->Text = L"Driver Name";
    //
    // richTextBox7
    //
    this->richTextBox7->Location = System::Drawing::Point(374, 80);
    this->richTextBox7->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox7->Name = L"richTextBox7";
    this->richTextBox7->ReadOnly = true;
    this->richTextBox7->Size = System::Drawing::Size(148, 431);
    this->richTextBox7->TabIndex = 1;
    this->richTextBox7->Text = L"May 13 2009\nMay 18 2009\nJuly 13 2009";
    this->richTextBox7->WordWrap = false;
    //
    // richTextBox6
    //
    this->richTextBox6->Location = System::Drawing::Point(8, 80);
    this->richTextBox6->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox6->Name = L"richTextBox6";
    this->richTextBox6->ReadOnly = true;
    this->richTextBox6->Size = System::Drawing::Size(361, 431);
    this->richTextBox6->TabIndex = 0;
    this->richTextBox6->Text = L"ASACPI.sys\nGEARAspiWDM.sys\nintelppm.sys";
    this->richTextBox6->WordWrap = false;
    //
    // tabPage12
    //
    this->tabPage12->Controls->Add(this->checkBox22);
    this->tabPage12->Controls->Add(this->button6);
    this->tabPage12->Controls->Add(this->numericUpDown2);
    this->tabPage12->Controls->Add(this->checkBox17);
    this->tabPage12->Controls->Add(this->checkBox18);
    this->tabPage12->Controls->Add(this->textBox26);
    this->tabPage12->Controls->Add(this->checkBox9);
    this->tabPage12->Controls->Add(this->checkBox8);
    this->tabPage12->Controls->Add(this->label26);
    this->tabPage12->Controls->Add(this->richTextBox9);
    this->tabPage12->Location = System::Drawing::Point(4, 22);
    this->tabPage12->Margin = System::Windows::Forms::Padding(2);
    this->tabPage12->Name = L"tabPage12";
    this->tabPage12->Padding = System::Windows::Forms::Padding(2);
    this->tabPage12->Size = System::Drawing::Size(1016, 425);
    this->tabPage12->TabIndex = 9;
    this->tabPage12->Text = L"User kd Commands";
    this->tabPage12->UseVisualStyleBackColor = true;
    //
    // checkBox22
    //
    this->checkBox22->AutoSize = true;
    this->checkBox22->Location = System::Drawing::Point(290, 17);
    this->checkBox22->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox22->Name = L"checkBox22";
    this->checkBox22->Size = System::Drawing::Size(57, 17);
    this->checkBox22->TabIndex = 85;
    this->checkBox22->Text = L"Delete";
    this->checkBox22->UseVisualStyleBackColor = true;
    this->checkBox22->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox22_CheckedChanged);
    //
    // button6
    //
    this->button6->Location = System::Drawing::Point(486, 17);
    this->button6->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->button6->Name = L"button6";
    this->button6->Size = System::Drawing::Size(75, 22);
    this->button6->TabIndex = 84;
    this->button6->Text = L"Proceed";
    this->button6->UseVisualStyleBackColor = true;
    this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
    //
    // numericUpDown2
    //
    this->numericUpDown2->Enabled = false;
    this->numericUpDown2->Location = System::Drawing::Point(360, 17);
    this->numericUpDown2->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->numericUpDown2->Name = L"numericUpDown2";
    this->numericUpDown2->Size = System::Drawing::Size(120, 20);
    this->numericUpDown2->TabIndex = 83;
    this->numericUpDown2->ValueChanged += gcnew System::EventHandler(this, &Form1::checkBox17_CheckedChanged);
    //
    // checkBox17
    //
    this->checkBox17->AutoSize = true;
    this->checkBox17->Location = System::Drawing::Point(242, 17);
    this->checkBox17->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox17->Name = L"checkBox17";
    this->checkBox17->Size = System::Drawing::Size(44, 17);
    this->checkBox17->TabIndex = 82;
    this->checkBox17->Text = L"Edit";
    this->checkBox17->UseVisualStyleBackColor = true;
    this->checkBox17->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox17_CheckedChanged);
    //
    // checkBox18
    //
    this->checkBox18->AutoSize = true;
    this->checkBox18->Location = System::Drawing::Point(194, 17);
    this->checkBox18->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox18->Name = L"checkBox18";
    this->checkBox18->Size = System::Drawing::Size(45, 17);
    this->checkBox18->TabIndex = 81;
    this->checkBox18->Text = L"Add";
    this->checkBox18->UseVisualStyleBackColor = true;
    this->checkBox18->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox18_CheckedChanged);
    //
    // textBox26
    //
    this->textBox26->Location = System::Drawing::Point(194, 86);
    this->textBox26->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->textBox26->Name = L"textBox26";
    this->textBox26->Size = System::Drawing::Size(361, 20);
    this->textBox26->TabIndex = 80;
    //
    // checkBox9
    //
    this->checkBox9->AutoSize = true;
    this->checkBox9->Location = System::Drawing::Point(558, 195);
    this->checkBox9->Margin = System::Windows::Forms::Padding(2);
    this->checkBox9->Name = L"checkBox9";
    this->checkBox9->Size = System::Drawing::Size(163, 17);
    this->checkBox9->TabIndex = 9;
    this->checkBox9->Text = L"Run User kd Commands First";
    this->checkBox9->UseVisualStyleBackColor = true;
    this->checkBox9->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox9_CheckedChanged);
    //
    // checkBox8
    //
    this->checkBox8->AutoSize = true;
    this->checkBox8->Location = System::Drawing::Point(558, 217);
    this->checkBox8->Margin = System::Windows::Forms::Padding(2);
    this->checkBox8->Name = L"checkBox8";
    this->checkBox8->Size = System::Drawing::Size(164, 17);
    this->checkBox8->TabIndex = 8;
    this->checkBox8->Text = L"Only Use User kd Commands";
    this->checkBox8->UseVisualStyleBackColor = true;
    this->checkBox8->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox8_CheckedChanged);
    //
    // label26
    //
    this->label26->AutoSize = true;
    this->label26->Location = System::Drawing::Point(322, 70);
    this->label26->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label26->Name = L"label26";
    this->label26->Size = System::Drawing::Size(99, 13);
    this->label26->TabIndex = 7;
    this->label26->Text = L"User kd Commands";
    //
    // richTextBox9
    //
    this->richTextBox9->Location = System::Drawing::Point(194, 109);
    this->richTextBox9->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox9->Name = L"richTextBox9";
    this->richTextBox9->ReadOnly = true;
    this->richTextBox9->Size = System::Drawing::Size(361, 346);
    this->richTextBox9->TabIndex = 6;
    this->richTextBox9->Text = L"";
    this->richTextBox9->WordWrap = false;
    //
    // tabPage11
    //
    this->tabPage11->Controls->Add(this->checkBox23);
    this->tabPage11->Controls->Add(this->button7);
    this->tabPage11->Controls->Add(this->numericUpDown3);
    this->tabPage11->Controls->Add(this->checkBox19);
    this->tabPage11->Controls->Add(this->checkBox20);
    this->tabPage11->Controls->Add(this->textBox27);
    this->tabPage11->Controls->Add(this->button1);
    this->tabPage11->Controls->Add(this->label25);
    this->tabPage11->Controls->Add(this->richTextBox8);
    this->tabPage11->Location = System::Drawing::Point(4, 22);
    this->tabPage11->Margin = System::Windows::Forms::Padding(2);
    this->tabPage11->Name = L"tabPage11";
    this->tabPage11->Padding = System::Windows::Forms::Padding(2);
    this->tabPage11->Size = System::Drawing::Size(1016, 425);
    this->tabPage11->TabIndex = 8;
    this->tabPage11->Text = L"Problematic Drivers Stats";
    this->tabPage11->UseVisualStyleBackColor = true;
    //
    // checkBox23
    //
    this->checkBox23->AutoSize = true;
    this->checkBox23->Location = System::Drawing::Point(290, 17);
    this->checkBox23->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox23->Name = L"checkBox23";
    this->checkBox23->Size = System::Drawing::Size(57, 17);
    this->checkBox23->TabIndex = 86;
    this->checkBox23->Text = L"Delete";
    this->checkBox23->UseVisualStyleBackColor = true;
    this->checkBox23->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox23_CheckedChanged);
    //
    // button7
    //
    this->button7->Location = System::Drawing::Point(486, 17);
    this->button7->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->button7->Name = L"button7";
    this->button7->Size = System::Drawing::Size(75, 22);
    this->button7->TabIndex = 84;
    this->button7->Text = L"Proceed";
    this->button7->UseVisualStyleBackColor = true;
    this->button7->Click += gcnew System::EventHandler(this, &Form1::button7_Click);
    //
    // numericUpDown3
    //
    this->numericUpDown3->Enabled = false;
    this->numericUpDown3->Location = System::Drawing::Point(360, 17);
    this->numericUpDown3->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->numericUpDown3->Name = L"numericUpDown3";
    this->numericUpDown3->Size = System::Drawing::Size(120, 20);
    this->numericUpDown3->TabIndex = 83;
    this->numericUpDown3->ValueChanged += gcnew System::EventHandler(this, &Form1::checkBox19_CheckedChanged);
    //
    // checkBox19
    //
    this->checkBox19->AutoSize = true;
    this->checkBox19->Location = System::Drawing::Point(242, 17);
    this->checkBox19->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox19->Name = L"checkBox19";
    this->checkBox19->Size = System::Drawing::Size(44, 17);
    this->checkBox19->TabIndex = 82;
    this->checkBox19->Text = L"Edit";
    this->checkBox19->UseVisualStyleBackColor = true;
    this->checkBox19->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox19_CheckedChanged);
    //
    // checkBox20
    //
    this->checkBox20->AutoSize = true;
    this->checkBox20->Location = System::Drawing::Point(194, 17);
    this->checkBox20->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->checkBox20->Name = L"checkBox20";
    this->checkBox20->Size = System::Drawing::Size(45, 17);
    this->checkBox20->TabIndex = 81;
    this->checkBox20->Text = L"Add";
    this->checkBox20->UseVisualStyleBackColor = true;
    this->checkBox20->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox20_CheckedChanged);
    //
    // textBox27
    //
    this->textBox27->Location = System::Drawing::Point(194, 86);
    this->textBox27->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
    this->textBox27->Name = L"textBox27";
    this->textBox27->Size = System::Drawing::Size(361, 20);
    this->textBox27->TabIndex = 80;
    //
    // button1
    //
    this->button1->Location = System::Drawing::Point(568, 214);
    this->button1->Margin = System::Windows::Forms::Padding(2);
    this->button1->Name = L"button1";
    this->button1->Size = System::Drawing::Size(172, 19);
    this->button1->TabIndex = 6;
    this->button1->Text = L"Clear Driver Statistics";
    this->button1->UseVisualStyleBackColor = true;
    this->button1->Click += gcnew System::EventHandler(this, &Form1::clearDriverStatistics);
    //
    // label25
    //
    this->label25->AutoSize = true;
    this->label25->Location = System::Drawing::Point(338, 70);
    this->label25->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label25->Name = L"label25";
    this->label25->Size = System::Drawing::Size(66, 13);
    this->label25->TabIndex = 5;
    this->label25->Text = L"Driver Name";
    //
    // richTextBox8
    //
    this->richTextBox8->Location = System::Drawing::Point(194, 109);
    this->richTextBox8->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox8->Name = L"richTextBox8";
    this->richTextBox8->ReadOnly = true;
    this->richTextBox8->Size = System::Drawing::Size(361, 338);
    this->richTextBox8->TabIndex = 1;
    this->richTextBox8->Text = L"";
    this->richTextBox8->WordWrap = false;
    //
    // tabPage2
    //
    this->tabPage2->Controls->Add(this->richTextBox1);
    this->tabPage2->Controls->Add(this->label13);
    this->tabPage2->Location = System::Drawing::Point(4, 22);
    this->tabPage2->Margin = System::Windows::Forms::Padding(2);
    this->tabPage2->Name = L"tabPage2";
    this->tabPage2->Padding = System::Windows::Forms::Padding(2);
    this->tabPage2->Size = System::Drawing::Size(1016, 425);
    this->tabPage2->TabIndex = 1;
    this->tabPage2->Text = L"Header";
    this->tabPage2->UseVisualStyleBackColor = true;
    //
    // richTextBox1
    //
    this->richTextBox1->Location = System::Drawing::Point(2, 24);
    this->richTextBox1->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox1->Name = L"richTextBox1";
    this->richTextBox1->Size = System::Drawing::Size(744, 435);
    this->richTextBox1->TabIndex = 28;
    this->richTextBox1->Text = L"";
    //
    // label13
    //
    this->label13->AutoSize = true;
    this->label13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label13->Location = System::Drawing::Point(352, 2);
    this->label13->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label13->Name = L"label13";
    this->label13->Size = System::Drawing::Size(62, 20);
    this->label13->TabIndex = 16;
    this->label13->Text = L"Header";
    //
    // tabPage5
    //
    this->tabPage5->Controls->Add(this->richTextBox2);
    this->tabPage5->Controls->Add(this->label14);
    this->tabPage5->Location = System::Drawing::Point(4, 22);
    this->tabPage5->Margin = System::Windows::Forms::Padding(2);
    this->tabPage5->Name = L"tabPage5";
    this->tabPage5->Padding = System::Windows::Forms::Padding(2);
    this->tabPage5->Size = System::Drawing::Size(1016, 425);
    this->tabPage5->TabIndex = 2;
    this->tabPage5->Text = L"Footer";
    this->tabPage5->UseVisualStyleBackColor = true;
    //
    // richTextBox2
    //
    this->richTextBox2->Location = System::Drawing::Point(2, 24);
    this->richTextBox2->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox2->Name = L"richTextBox2";
    this->richTextBox2->Size = System::Drawing::Size(744, 435);
    this->richTextBox2->TabIndex = 31;
    this->richTextBox2->Text = L"";
    //
    // label14
    //
    this->label14->AutoSize = true;
    this->label14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label14->Location = System::Drawing::Point(294, 2);
    this->label14->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label14->Name = L"label14";
    this->label14->Size = System::Drawing::Size(56, 20);
    this->label14->TabIndex = 30;
    this->label14->Text = L"Footer";
    //
    // tabPage6
    //
    this->tabPage6->Controls->Add(this->richTextBox3);
    this->tabPage6->Controls->Add(this->label15);
    this->tabPage6->Location = System::Drawing::Point(4, 22);
    this->tabPage6->Margin = System::Windows::Forms::Padding(2);
    this->tabPage6->Name = L"tabPage6";
    this->tabPage6->Padding = System::Windows::Forms::Padding(2);
    this->tabPage6->Size = System::Drawing::Size(1016, 425);
    this->tabPage6->TabIndex = 3;
    this->tabPage6->Text = L"Signature";
    this->tabPage6->UseVisualStyleBackColor = true;
    //
    // richTextBox3
    //
    this->richTextBox3->Location = System::Drawing::Point(2, 24);
    this->richTextBox3->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox3->Name = L"richTextBox3";
    this->richTextBox3->Size = System::Drawing::Size(744, 435);
    this->richTextBox3->TabIndex = 32;
    this->richTextBox3->Text = L"";
    //
    // label15
    //
    this->label15->AutoSize = true;
    this->label15->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label15->Location = System::Drawing::Point(282, 2);
    this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label15->Name = L"label15";
    this->label15->Size = System::Drawing::Size(78, 20);
    this->label15->TabIndex = 31;
    this->label15->Text = L"Signature";
    //
    // tabPage7
    //
    this->tabPage7->Controls->Add(this->richTextBox4);
    this->tabPage7->Controls->Add(this->label16);
    this->tabPage7->Location = System::Drawing::Point(4, 22);
    this->tabPage7->Margin = System::Windows::Forms::Padding(2);
    this->tabPage7->Name = L"tabPage7";
    this->tabPage7->Padding = System::Windows::Forms::Padding(2);
    this->tabPage7->Size = System::Drawing::Size(1016, 425);
    this->tabPage7->TabIndex = 4;
    this->tabPage7->Text = L"Driver Update Header";
    this->tabPage7->UseVisualStyleBackColor = true;
    //
    // richTextBox4
    //
    this->richTextBox4->Location = System::Drawing::Point(2, 24);
    this->richTextBox4->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox4->Name = L"richTextBox4";
    this->richTextBox4->Size = System::Drawing::Size(744, 435);
    this->richTextBox4->TabIndex = 33;
    this->richTextBox4->Text = L"";
    //
    // label16
    //
    this->label16->AutoSize = true;
    this->label16->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label16->Location = System::Drawing::Point(244, 2);
    this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label16->Name = L"label16";
    this->label16->Size = System::Drawing::Size(164, 20);
    this->label16->TabIndex = 32;
    this->label16->Text = L"Driver Update Header";
    //
    // tabPage8
    //
    this->tabPage8->Controls->Add(this->textBox13);
    this->tabPage8->Controls->Add(this->textBox12);
    this->tabPage8->Controls->Add(this->label18);
    this->tabPage8->Controls->Add(this->label17);
    this->tabPage8->Location = System::Drawing::Point(4, 22);
    this->tabPage8->Margin = System::Windows::Forms::Padding(2);
    this->tabPage8->Name = L"tabPage8";
    this->tabPage8->Padding = System::Windows::Forms::Padding(2);
    this->tabPage8->Size = System::Drawing::Size(1016, 425);
    this->tabPage8->TabIndex = 5;
    this->tabPage8->Text = L"Code Box";
    this->tabPage8->UseVisualStyleBackColor = true;
    //
    // textBox13
    //
    this->textBox13->Location = System::Drawing::Point(274, 173);
    this->textBox13->Margin = System::Windows::Forms::Padding(2);
    this->textBox13->Name = L"textBox13";
    this->textBox13->Size = System::Drawing::Size(194, 20);
    this->textBox13->TabIndex = 31;
    //
    // textBox12
    //
    this->textBox12->Location = System::Drawing::Point(274, 145);
    this->textBox12->Margin = System::Windows::Forms::Padding(2);
    this->textBox12->Name = L"textBox12";
    this->textBox12->Size = System::Drawing::Size(194, 20);
    this->textBox12->TabIndex = 30;
    //
    // label18
    //
    this->label18->AutoSize = true;
    this->label18->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label18->Location = System::Drawing::Point(174, 176);
    this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label18->Name = L"label18";
    this->label18->Size = System::Drawing::Size(91, 13);
    this->label18->TabIndex = 29;
    this->label18->Text = L"Code Box End:";
    //
    // label17
    //
    this->label17->AutoSize = true;
    this->label17->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label17->Location = System::Drawing::Point(174, 148);
    this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label17->Name = L"label17";
    this->label17->Size = System::Drawing::Size(101, 13);
    this->label17->TabIndex = 28;
    this->label17->Text = L"Code Box Begin:";
    //
    // tabPage9
    //
    this->tabPage9->Controls->Add(this->label46);
    this->tabPage9->Controls->Add(this->richTextBox5);
    this->tabPage9->Location = System::Drawing::Point(4, 22);
    this->tabPage9->Margin = System::Windows::Forms::Padding(2);
    this->tabPage9->Name = L"tabPage9";
    this->tabPage9->Padding = System::Windows::Forms::Padding(2);
    this->tabPage9->Size = System::Drawing::Size(1016, 425);
    this->tabPage9->TabIndex = 6;
    this->tabPage9->Text = L"Template";
    this->tabPage9->UseVisualStyleBackColor = true;
    //
    // label46
    //
    this->label46->AutoSize = true;
    this->label46->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label46->Location = System::Drawing::Point(280, 0);
    this->label46->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label46->Name = L"label46";
    this->label46->Size = System::Drawing::Size(75, 20);
    this->label46->TabIndex = 35;
    this->label46->Text = L"Template";
    //
    // richTextBox5
    //
    this->richTextBox5->Location = System::Drawing::Point(2, 24);
    this->richTextBox5->Margin = System::Windows::Forms::Padding(2);
    this->richTextBox5->Name = L"richTextBox5";
    this->richTextBox5->Size = System::Drawing::Size(744, 435);
    this->richTextBox5->TabIndex = 34;
    this->richTextBox5->Text = L"";
    //
    // button9
    //
    this->button9->Location = System::Drawing::Point(483, 0);
    this->button9->Name = L"button9";
    this->button9->Size = System::Drawing::Size(143, 21);
    this->button9->TabIndex = 113;
    this->button9->Text = L"Save Forum .zdn";
    this->button9->UseVisualStyleBackColor = true;
    this->button9->Click += gcnew System::EventHandler(this, &Form1::button9_Click);
    //
    // menuStrip1
    //
    this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
        this->pressF1ForHelpToolStripMenuItem,
            this->optionsToolStripMenuItem, this->saveAndRunToolStripMenuItem1, this->helpToolStripMenuItem
    });
    this->menuStrip1->Location = System::Drawing::Point(0, 0);
    this->menuStrip1->Name = L"menuStrip1";
    this->menuStrip1->Padding = System::Windows::Forms::Padding(4, 2, 0, 2);
    this->menuStrip1->Size = System::Drawing::Size(1276, 24);
    this->menuStrip1->TabIndex = 27;
    this->menuStrip1->Text = L"menuStrip1";
    //
    // pressF1ForHelpToolStripMenuItem
    //
    this->pressF1ForHelpToolStripMenuItem->Checked = true;
    this->pressF1ForHelpToolStripMenuItem->CheckState = System::Windows::Forms::CheckState::Checked;
    this->pressF1ForHelpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {
        this->saveToolStripMenuItem,
            this->loadToolStripMenuItem, this->saveAsToolStripMenuItem, this->loadPreviousToolStripMenuItem, this->quickSaveToolStripMenuItem,
            this->toolStripSeparator1, this->exitToolStripMenuItem
    });
    this->pressF1ForHelpToolStripMenuItem->Name = L"pressF1ForHelpToolStripMenuItem";
    this->pressF1ForHelpToolStripMenuItem->Size = System::Drawing::Size(37, 20);
    this->pressF1ForHelpToolStripMenuItem->Text = L"File";
    //
    // saveToolStripMenuItem
    //
    this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
    this->saveToolStripMenuItem->Size = System::Drawing::Size(206, 22);
    this->saveToolStripMenuItem->Text = L"Open";
    this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::Open);
    //
    // loadToolStripMenuItem
    //
    this->loadToolStripMenuItem->Name = L"loadToolStripMenuItem";
    this->loadToolStripMenuItem->Size = System::Drawing::Size(206, 22);
    this->loadToolStripMenuItem->Text = L"Save";
    this->loadToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveOnly);
    //
    // saveAsToolStripMenuItem
    //
    this->saveAsToolStripMenuItem->Name = L"saveAsToolStripMenuItem";
    this->saveAsToolStripMenuItem->Size = System::Drawing::Size(206, 22);
    this->saveAsToolStripMenuItem->Text = L"Save As";
    this->saveAsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveAs);
    //
    // loadPreviousToolStripMenuItem
    //
    this->loadPreviousToolStripMenuItem->Name = L"loadPreviousToolStripMenuItem";
    this->loadPreviousToolStripMenuItem->Size = System::Drawing::Size(206, 22);
    this->loadPreviousToolStripMenuItem->Text = L"Revert to Last Quick Save";
    this->loadPreviousToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::loadPreviousSettings2);
    //
    // quickSaveToolStripMenuItem
    //
    this->quickSaveToolStripMenuItem->Name = L"quickSaveToolStripMenuItem";
    this->quickSaveToolStripMenuItem->Size = System::Drawing::Size(206, 22);
    this->quickSaveToolStripMenuItem->Text = L"Quick Save";
    this->quickSaveToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveAndRunTheApps);
    //
    // toolStripSeparator1
    //
    this->toolStripSeparator1->Name = L"toolStripSeparator1";
    this->toolStripSeparator1->Size = System::Drawing::Size(203, 6);
    //
    // exitToolStripMenuItem
    //
    this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
    this->exitToolStripMenuItem->Size = System::Drawing::Size(206, 22);
    this->exitToolStripMenuItem->Text = L"Exit";
    this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::Exit);
    //
    // optionsToolStripMenuItem
    //
    this->optionsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {
        this->alwaysOnTopToolStripMenuItem,
            this->useParallelThreadingToolStripMenuItem, this->sysnativeResultsDIrectoryToolStripMenuItem, this->loggingToolStripMenuItem,
            this->closeAppsWhenFinishedToolStripMenuItem, this->windowsToolStripMenuItem
    });
    this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
    this->optionsToolStripMenuItem->Size = System::Drawing::Size(61, 20);
    this->optionsToolStripMenuItem->Text = L"Options";
    //
    // alwaysOnTopToolStripMenuItem
    //
    this->alwaysOnTopToolStripMenuItem->Name = L"alwaysOnTopToolStripMenuItem";
    this->alwaysOnTopToolStripMenuItem->Size = System::Drawing::Size(252, 22);
    this->alwaysOnTopToolStripMenuItem->Text = L"Always On Top";
    this->alwaysOnTopToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::onChecked);
    //
    // useParallelThreadingToolStripMenuItem
    //
    this->useParallelThreadingToolStripMenuItem->CheckOnClick = true;
    this->useParallelThreadingToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->changeThreadsToolStripMenuItem });
    this->useParallelThreadingToolStripMenuItem->Name = L"useParallelThreadingToolStripMenuItem";
    this->useParallelThreadingToolStripMenuItem->Size = System::Drawing::Size(252, 22);
    this->useParallelThreadingToolStripMenuItem->Text = L"Use Parallel Threading";
    this->useParallelThreadingToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::useParallelThreadingToolStripMenuItem_Click);
    //
    // changeThreadsToolStripMenuItem
    //
    this->changeThreadsToolStripMenuItem->Name = L"changeThreadsToolStripMenuItem";
    this->changeThreadsToolStripMenuItem->Size = System::Drawing::Size(201, 22);
    this->changeThreadsToolStripMenuItem->Text = L"ChangeNumberThreads";
    this->changeThreadsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::useParallelThreadingToolStripMenuItem_Click);
    //
    // sysnativeResultsDIrectoryToolStripMenuItem
    //
    this->sysnativeResultsDIrectoryToolStripMenuItem->CheckOnClick = true;
    this->sysnativeResultsDIrectoryToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->setDirectoryToolStripMenuItem });
    this->sysnativeResultsDIrectoryToolStripMenuItem->Name = L"sysnativeResultsDIrectoryToolStripMenuItem";
    this->sysnativeResultsDIrectoryToolStripMenuItem->Size = System::Drawing::Size(252, 22);
    this->sysnativeResultsDIrectoryToolStripMenuItem->Text = L"Output SysnativeResults Directory";
    this->sysnativeResultsDIrectoryToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::sysnativeResultsDIrectoryToolStripMenuItem_Click);
    //
    // setDirectoryToolStripMenuItem
    //
    this->setDirectoryToolStripMenuItem->Name = L"setDirectoryToolStripMenuItem";
    this->setDirectoryToolStripMenuItem->Size = System::Drawing::Size(141, 22);
    this->setDirectoryToolStripMenuItem->Text = L"Set Directory";
    this->setDirectoryToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::setDirectoryToolStripMenuItem_Click);
    //
    // loggingToolStripMenuItem
    //
    this->loggingToolStripMenuItem->CheckOnClick = true;
    this->loggingToolStripMenuItem->Name = L"loggingToolStripMenuItem";
    this->loggingToolStripMenuItem->Size = System::Drawing::Size(252, 22);
    this->loggingToolStripMenuItem->Text = L"Logging";
    this->loggingToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::loggingToolStripMenuItem_Click);
    //
    // closeAppsWhenFinishedToolStripMenuItem
    //
    this->closeAppsWhenFinishedToolStripMenuItem->Name = L"closeAppsWhenFinishedToolStripMenuItem";
    this->closeAppsWhenFinishedToolStripMenuItem->Size = System::Drawing::Size(252, 22);
    this->closeAppsWhenFinishedToolStripMenuItem->Text = L"Close Apps When Finished";
    this->closeAppsWhenFinishedToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::onChecked2);
    //
    // windowsToolStripMenuItem
    //
    this->windowsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->kdexePathsToolStripMenuItem });
    this->windowsToolStripMenuItem->Name = L"windowsToolStripMenuItem";
    this->windowsToolStripMenuItem->Size = System::Drawing::Size(252, 22);
    this->windowsToolStripMenuItem->Text = L"Windows";
    //
    // kdexePathsToolStripMenuItem
    //
    this->kdexePathsToolStripMenuItem->Name = L"kdexePathsToolStripMenuItem";
    this->kdexePathsToolStripMenuItem->Size = System::Drawing::Size(147, 22);
    this->kdexePathsToolStripMenuItem->Text = L"kd.exe Path(s)";
    this->kdexePathsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::kdexePathsToolStripMenuItem_Click);
    //
    // saveAndRunToolStripMenuItem1
    //
    this->saveAndRunToolStripMenuItem1->Name = L"saveAndRunToolStripMenuItem1";
    this->saveAndRunToolStripMenuItem1->Size = System::Drawing::Size(90, 20);
    this->saveAndRunToolStripMenuItem1->Text = L"Save and Run";
    this->saveAndRunToolStripMenuItem1->Click += gcnew System::EventHandler(this, &Form1::runTheApps2);
    //
    // helpToolStripMenuItem
    //
    this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
    this->helpToolStripMenuItem->Size = System::Drawing::Size(44, 20);
    this->helpToolStripMenuItem->Text = L"Help";
    this->helpToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::Help);
    //
    // textBox4
    //
    this->textBox4->Location = System::Drawing::Point(1176, 89);
    this->textBox4->Margin = System::Windows::Forms::Padding(2);
    this->textBox4->Name = L"textBox4";
    this->textBox4->Size = System::Drawing::Size(182, 20);
    this->textBox4->TabIndex = 68;
    //
    // label27
    //
    this->label27->AutoSize = true;
    this->label27->BackColor = System::Drawing::SystemColors::Window;
    this->label27->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label27->Location = System::Drawing::Point(1049, 95);
    this->label27->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label27->Name = L"label27";
    this->label27->Size = System::Drawing::Size(101, 13);
    this->label27->TabIndex = 67;
    this->label27->Text = L"Originating Post:";
    //
    // textBox11
    //
    this->textBox11->Location = System::Drawing::Point(1176, 67);
    this->textBox11->Margin = System::Windows::Forms::Padding(2);
    this->textBox11->Name = L"textBox11";
    this->textBox11->Size = System::Drawing::Size(182, 20);
    this->textBox11->TabIndex = 66;
    //
    // label28
    //
    this->label28->AutoSize = true;
    this->label28->BackColor = System::Drawing::SystemColors::Window;
    this->label28->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label28->Location = System::Drawing::Point(1049, 70);
    this->label28->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label28->Name = L"label28";
    this->label28->Size = System::Drawing::Size(67, 13);
    this->label28->TabIndex = 65;
    this->label28->Text = L"Username:";
    //
    // label29
    //
    this->label29->AutoSize = true;
    this->label29->BackColor = System::Drawing::SystemColors::Window;
    this->label29->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label29->ForeColor = System::Drawing::SystemColors::Desktop;
    this->label29->Location = System::Drawing::Point(1084, 45);
    this->label29->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label29->Name = L"label29";
    this->label29->Size = System::Drawing::Size(267, 17);
    this->label29->TabIndex = 64;
    this->label29->Text = L"Driver Reference Table Update Info";
    //
    // button2
    //
    this->button2->Location = System::Drawing::Point(1209, 212);
    this->button2->Margin = System::Windows::Forms::Padding(2);
    this->button2->Name = L"button2";
    this->button2->Size = System::Drawing::Size(112, 19);
    this->button2->TabIndex = 63;
    this->button2->TabStop = false;
    this->button2->Text = L"Process BSODs";
    this->button2->UseVisualStyleBackColor = true;
    this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
    //
    // button3
    //
    this->button3->Location = System::Drawing::Point(1092, 212);
    this->button3->Margin = System::Windows::Forms::Padding(2);
    this->button3->Name = L"button3";
    this->button3->Size = System::Drawing::Size(112, 19);
    this->button3->TabIndex = 62;
    this->button3->TabStop = false;
    this->button3->Text = L"Change Settings";
    this->button3->UseVisualStyleBackColor = true;
    this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
    //
    // checkBox10
    //
    this->checkBox10->AutoSize = true;
    this->checkBox10->Location = System::Drawing::Point(240, 4);
    this->checkBox10->Margin = System::Windows::Forms::Padding(2);
    this->checkBox10->Name = L"checkBox10";
    this->checkBox10->Size = System::Drawing::Size(102, 17);
    this->checkBox10->TabIndex = 77;
    this->checkBox10->TabStop = false;
    this->checkBox10->Text = L"Full GUI Version";
    this->checkBox10->UseVisualStyleBackColor = true;
    //
    // checkBox24
    //
    this->checkBox24->AutoSize = true;
    this->checkBox24->Location = System::Drawing::Point(360, 4);
    this->checkBox24->Margin = System::Windows::Forms::Padding(2);
    this->checkBox24->Name = L"checkBox24";
    this->checkBox24->Size = System::Drawing::Size(103, 17);
    this->checkBox24->TabIndex = 77;
    this->checkBox24->TabStop = false;
    this->checkBox24->Text = L"New .dmps Only";
    this->checkBox24->UseVisualStyleBackColor = true;
    //
    // button4
    //
    this->button4->Location = System::Drawing::Point(1149, 235);
    this->button4->Margin = System::Windows::Forms::Padding(2);
    this->button4->Name = L"button4";
    this->button4->Size = System::Drawing::Size(137, 19);
    this->button4->TabIndex = 78;
    this->button4->TabStop = false;
    this->button4->Text = L"View Previous Output";
    this->button4->UseVisualStyleBackColor = true;
    this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
    //
    // label12
    //
    this->label12->AutoSize = true;
    this->label12->Location = System::Drawing::Point(3, 483);
    this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label12->Name = L"label12";
    this->label12->Size = System::Drawing::Size(107, 13);
    this->label12->TabIndex = 80;
    this->label12->Text = L" � 2024 Mike Pryor";
    //
    // textBox28
    //
    this->textBox28->Location = System::Drawing::Point(1179, 172);
    this->textBox28->Margin = System::Windows::Forms::Padding(2);
    this->textBox28->Name = L"textBox28";
    this->textBox28->Size = System::Drawing::Size(182, 20);
    this->textBox28->TabIndex = 84;
    //
    // label47
    //
    this->label47->AutoSize = true;
    this->label47->BackColor = System::Drawing::SystemColors::Window;
    this->label47->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label47->Location = System::Drawing::Point(1049, 178);
    this->label47->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label47->Name = L"label47";
    this->label47->Size = System::Drawing::Size(120, 13);
    this->label47->TabIndex = 83;
    this->label47->Text = L"Output Subdirectory";
    //
    // textBox29
    //
    this->textBox29->Location = System::Drawing::Point(1179, 150);
    this->textBox29->Margin = System::Windows::Forms::Padding(2);
    this->textBox29->Name = L"textBox29";
    this->textBox29->Size = System::Drawing::Size(182, 20);
    this->textBox29->TabIndex = 82;
    //
    // label48
    //
    this->label48->AutoSize = true;
    this->label48->BackColor = System::Drawing::SystemColors::Window;
    this->label48->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label48->Location = System::Drawing::Point(1049, 153);
    this->label48->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label48->Name = L"label48";
    this->label48->Size = System::Drawing::Size(100, 13);
    this->label48->TabIndex = 81;
    this->label48->Text = L"Output Directory";
    //
    // label49
    //
    this->label49->AutoSize = true;
    this->label49->BackColor = System::Drawing::SystemColors::Window;
    this->label49->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
        static_cast<System::Byte>(0)));
    this->label49->ForeColor = System::Drawing::SystemColors::Desktop;
    this->label49->Location = System::Drawing::Point(1117, 132);
    this->label49->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
    this->label49->Name = L"label49";
    this->label49->Size = System::Drawing::Size(200, 17);
    this->label49->TabIndex = 85;
    this->label49->Text = L"Output Directory Structure";
    //
    // numericUpDown4
    //
    this->numericUpDown4->Location = System::Drawing::Point(1086, 261);
    this->numericUpDown4->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 3600, 0, 0, 0 });
    this->numericUpDown4->Name = L"numericUpDown4";
    this->numericUpDown4->Size = System::Drawing::Size(57, 20);
    this->numericUpDown4->TabIndex = 113;
    //
    // label50
    //
    this->label50->AutoSize = true;
    this->label50->Location = System::Drawing::Point(1155, 264);
    this->label50->Name = L"label50";
    this->label50->Size = System::Drawing::Size(142, 13);
    this->label50->TabIndex = 114;
    this->label50->Text = L"DmpList timeout (in seconds)";
    //
    // backgroundWorker1
    //
    this->backgroundWorker1->WorkerReportsProgress = true;
    this->backgroundWorker1->WorkerSupportsCancellation = true;
    this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Form1::backgroundWorker1_DoWork);
    //
    // Form1
    //
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->ClientSize = System::Drawing::Size(1276, 505);
    this->Controls->Add(this->button9);
    this->Controls->Add(this->numericUpDown4);
    this->Controls->Add(this->label50);
    this->Controls->Add(this->label49);
    this->Controls->Add(this->textBox28);
    this->Controls->Add(this->label12);
    this->Controls->Add(this->button4);
    this->Controls->Add(this->label47);
    this->Controls->Add(this->checkBox10);
    this->Controls->Add(this->checkBox24);
    this->Controls->Add(this->textBox4);
    this->Controls->Add(this->textBox29);
    this->Controls->Add(this->label27);
    this->Controls->Add(this->textBox11);
    this->Controls->Add(this->label48);
    this->Controls->Add(this->label28);
    this->Controls->Add(this->label29);
    this->Controls->Add(this->button2);
    this->Controls->Add(this->button3);
    this->Controls->Add(this->menuStrip1);
    this->Controls->Add(this->tabControl1);
    this->DoubleBuffered = true;
    this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
    this->Margin = System::Windows::Forms::Padding(2);
    this->Name = L"Form1";
    this->Text = L"Sysnative BSOD Apps 3.2.0";
    this->Load += gcnew System::EventHandler(this, &Form1::loadForm1);
    this->ResizeEnd += gcnew System::EventHandler(this, &Form1::resize);
    this->Resize += gcnew System::EventHandler(this, &Form1::resize);
    this->tabControl1->ResumeLayout(false);
    this->tabPage1->ResumeLayout(false);
    this->tabPage1->PerformLayout();
    //this->tabControl2->ResumeLayout(false);
    this->tabPage4->ResumeLayout(false);
    this->tabPage13->ResumeLayout(false);
    this->tabPage13->PerformLayout();
    this->menuStrip2->ResumeLayout(false);
    this->menuStrip2->PerformLayout();
    this->tabPage10->ResumeLayout(false);
    this->tabPage10->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->EndInit();
    this->tabPage12->ResumeLayout(false);
    this->tabPage12->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->EndInit();
    this->tabPage11->ResumeLayout(false);
    this->tabPage11->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown3))->EndInit();
    this->tabPage2->ResumeLayout(false);
    this->tabPage2->PerformLayout();
    this->tabPage5->ResumeLayout(false);
    this->tabPage5->PerformLayout();
    this->tabPage6->ResumeLayout(false);
    this->tabPage6->PerformLayout();
    this->tabPage7->ResumeLayout(false);
    this->tabPage7->PerformLayout();
    this->tabPage8->ResumeLayout(false);
    this->tabPage8->PerformLayout();
    this->tabPage9->ResumeLayout(false);
    this->tabPage9->PerformLayout();
    this->menuStrip1->ResumeLayout(false);
    this->menuStrip1->PerformLayout();
    (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown4))->EndInit();
    this->ResumeLayout(false);
    this->PerformLayout();
}

System::Void Form1::centerForm1()
{
  loggingToolStripMenuItem->Checked = false;
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::string dmpListTemp = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP")) + "\\SysnativeBSODApps\\";
  std::string DODmpOptions = dmpListTemp;

  std::string pathDmpListTimeout = DODmpOptions + "dmpListTimeout.txt";
  std::ifstream inDmpListTimeout(pathDmpListTimeout);
  int dmpListTimeout;
  if(inDmpListTimeout.good())
  {
    inDmpListTimeout >> dmpListTimeout;
  }
  else{
    dmpListTimeout = 30;
  }
  inDmpListTimeout.close();

  // Using CDPI example class
  CDPI g_metrics;
  int x, y;
  int dpiX = g_metrics.GetDPIX();

  label29->Location =     System::Drawing::Point(  51,   34);
  label28->Location =     System::Drawing::Point(   4,  39 + label29->Height);
  label27->Location =     System::Drawing::Point(   4,  49 + label29->Height + label28->Height);
  label49->Location =     System::Drawing::Point(  95,   30 + textBox11->Location.Y + textBox11->Height);
  label48->Location =     System::Drawing::Point(   4,  10 + label49->Location.Y + label49->Height);
  label47->Location =     System::Drawing::Point(   4,  10 + label48->Location.Y + label48->Height);
  textBox11->Location =   System::Drawing::Point( label47->Width + 20,  34 + label29->Height);
  textBox4->Location =    System::Drawing::Point( label47->Width + 20,  44 + label29->Height + label28->Height);
  textBox29->Location =   System::Drawing::Point( label47->Width + 20,  5 + label49->Location.Y + label49->Height);
  textBox28->Location =    System::Drawing::Point( label47->Width + 20,  5 + label48->Location.Y + label48->Height);

  button3->Location =     System::Drawing::Point(  int(double(textBox11->Location.X + textBox11->Width - button3->Width - button2->Width)/2.0), 10 + label47->Location.Y + label47->Height);
  x = button3->Location.X + button3->Width + 10;
  y = 10 + label47->Location.Y + label47->Height;
  button2->Location =     System::Drawing::Point( x, y);
  x = button3->Location.X + int(double(button3->Width + button2->Width + 10 - button4->Width) / double(2.0));
  y = 10 + label47->Location.Y + label47->Height + button3->Height + 10;
  button4->Location =     System::Drawing::Point(  x, y);
  x = button3->Location.X;
  y = 10 + label47->Location.Y + label47->Height + button3->Height + button4->Height + 20;
  checkBox10->Location =  System::Drawing::Point( x, y);
  x = button2->Location.X;
  y = 10 + label47->Location.Y + label47->Height + button3->Height + button4->Height + 20;
  checkBox24->Location =  System::Drawing::Point( x, y);
  x = button3->Location.X;
  y = 10 + label47->Location.Y + label47->Height + button3->Height + button4->Height + checkBox10->Height + 30;
  numericUpDown4->Location = System::Drawing::Point( x, y);
  x = x + numericUpDown4->Width + 10;
  y = 10 + label47->Location.Y + label47->Height + button3->Height + button4->Height + checkBox10->Height + 33;

  numericUpDown4->Value = dmpListTimeout;

  label50->Location = System::Drawing::Point( x, y);
  x = textBox11->Location.X + textBox11->Width + 30;
  y = y + label50->Height + label12->Height + int(double(60) * double(dpiX)/double(96));
  this->Width = x;
  this->Height = y;
  CenterWindow myWindow;
  myWindow.MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), x, y);
  x = int(0.5*double(this->Width - checkBox24->Width));
  y = 10 + label47->Location.Y + label47->Height + button3->Height + button4->Height + 20;
  checkBox24->Location =  System::Drawing::Point( x, y);
  pressF1ForHelpToolStripMenuItem->Enabled = false;
  saveAndRunToolStripMenuItem1->Enabled = false;
  pressF1ForHelpToolStripMenuItem->Visible = false;
  saveAndRunToolStripMenuItem1->Visible = false;
}

// load first screen settings
System::Void Form1::loadForm1(System::Object^  sender, System::EventArgs^  e)
{
  backgroundWorker1->RunWorkerAsync();
}

// Loading tabControl1
System::Void Form1::loadTabControl1(System::Object^  sender, System::EventArgs^  e){
  // Using CDPI example class
  CDPI g_metrics;
  int x, y;
  int dpiX = g_metrics.GetDPIX();

  x = checkBox3->Location.X + checkBox3->Width + 150;
  y = checkBox4->Height + checkBox4->Location.Y + int(double(80) * double(dpiX)/double(96)) + label12->Height + int(double(60) * double(dpiX)/double(96));
  this->Width = x;
  if(y < 768){
    this->Height = y;
    CenterWindow myWindow;
    myWindow.MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), x, y);
  }
  else{
    // this->Text = y.ToString();
    WindowState = System::Windows::Forms::FormWindowState::Maximized;
  }

  menuStrip1->Location =  System::Drawing::Point(   0,   0);
  tabControl1->Location = System::Drawing::Point(   2,  29 + menuStrip1->Height);
  checkBox10->Location =  System::Drawing::Point( checkBox7->Location.X + 10,   5);
  checkBox24->Location =  System::Drawing::Point( checkBox7->Location.X + checkBox10->Width + 20,   5);
  button9->Location =  System::Drawing::Point( checkBox7->Location.X + checkBox10->Width + checkBox24->Width + 30,   5);
  this->checkBox1->Location = System::Drawing::Point(textBox3->Location.X + textBox3->Width + 10, checkBox1->Location.Y);
  this->checkBox2->Location = System::Drawing::Point(textBox10->Location.X + textBox10->Width + 10, checkBox2->Location.Y);
  this->checkBox3->Location = System::Drawing::Point(label9->Location.X + label9->Width + 10, checkBox3->Location.Y);
  this->checkBox4->Location = System::Drawing::Point(label9->Location.X + label9->Width + 10, checkBox4->Location.Y);

  label29->Visible = false;
  label28->Visible = false;
  textBox11->Visible = false;
  label27->Visible = false;
  textBox4->Visible = false;
  label49->Visible = false;
  label48->Visible = false;
  textBox29->Visible = false;
  label47->Visible = false;
  textBox28->Visible = false;
  button3->Visible = false;
  button2->Visible = false;
  button4->Visible = false;
  label50->Visible = false;
  numericUpDown4->Visible = false;
  checkBox10->Visible = false;

  tabControl1->Visible = true;
  button9->Visible = true;
  menuStrip1->Visible = true;
  button9->Visible = true;

  std::string userProfile = ConfigurationUtil::getUserProfilePath();

  loadPreviousSettings(sender, e);

  if(checkBox8->Checked){
    checkBox9->Checked = false;
  }
  if(checkBox9->Checked){
    checkBox8->Checked = false;
  }

  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::string tempPath = tempDir + "\\percentDone.txt";
  std::ofstream outPercentDone;
  outPercentDone.open(tempPath);
  outPercentDone << 100;
  outPercentDone.close();
  Show();
  Hide();
  Show();
}

//Change Settings button
System::Void Form1::button3_Click(System::Object^  sender, System::EventArgs^  e){
  Hide();
  mForumName = textBox4->Text;
  std::string inChar = SystemStandardConversionUtil::convertSystemStringToStdString(mForumName);
  std::stringstream ssforumString;
  // Remove html path characters from forum name
  for(int i77 = 0; i77 < int(inChar.length()); i77++){
    if(inChar.c_str()[i77] != '/' && inChar.c_str()[i77] != '.' && inChar.c_str()[i77] != ':' && inChar.c_str()[i77] != ' '){
      ssforumString << inChar.c_str()[i77];
    }
  }
  mForumName = gcnew System::String(ssforumString.str().c_str());
  pressF1ForHelpToolStripMenuItem->Enabled = true;
  saveAndRunToolStripMenuItem1->Enabled = true;
  pressF1ForHelpToolStripMenuItem->Visible = true;
  saveAndRunToolStripMenuItem1->Visible = true;
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::ofstream outPercentDone(tempDir + "\\percentDone.txt");
  outPercentDone << 452312;
  outPercentDone.close();
  std::ofstream outProg;
  std::string tempPath;
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving First Screen Settings, Please Wait...";
  outProg.close();

  Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
  newThread->Start();

  std::ofstream outfile;
  System::String^ s = textBox4->Text;
  const char* chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string dmpListTemp = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP")) + "\\SysnativeBSODApps\\";

  std::string DOParms = userProfile + "\\SysnativeBSODApps\\parms\\";
  std::string DODmpOptions = dmpListTemp;

  std::string pathDmpListTimeout = DODmpOptions + "dmpListTimeout.txt";

  DODmpOptions = userProfile + "\\SysnativeBSODApps\\dmpOptions\\";

  std::ofstream outDmpListTimeout(pathDmpListTimeout);
  outDmpListTimeout << int(numericUpDown4->Value);
  outDmpListTimeout.close();

  outfile.open(tempDir + "\\originatingPost.txt");
  outfile << os;
  outfile.close();

  // write the $un95 file
  DOParms = tempDir;
  std::string inPath = DOParms + "\\un95";
  outfile.open(inPath);

  s = textBox11->Text;
  chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile << os;
  outfile.close();

  (inPath,userProfile + "\\SysnativeBSODApps\\parms\\un95");

  outfile.open(tempDir + "\\fullGUI.txt");
  outfile << checkBox10->Checked;
  outfile.close();

  s = textBox29->Text;
  chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  // \ / : * ? " < > | cannot be used for directory names
  std::stringstream sstemp;
  int badSymbol = 0;
  for(int i=0; i<int(os.length()); i++){
    if(os[i] == '\\' || os[i] == '/' || os[i] == ':' || os[i] == '*' || os[i] == '?' || os[i] == '\"' || os[i] == '<' || os[i] == '>' || os[i] == '|'){
      sstemp << '_';
      badSymbol++;
    }
    else{
      sstemp << os[i];
    }
  }
  if(badSymbol){
    System::Windows::Forms::MessageBox::Show(L"Bad Symbol in Directory Name\r\n Replaced bad symbol with _",L"Bad Symbol in Directory Name", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
  }
  os = sstemp.str();

  outfile.open(tempDir + "\\outputDmpsDir.txt");
  outfile << os;
  outfile.close();

  s = textBox28->Text;
  chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  // \ / : * ? " < > | cannot be used for directory names
  std::stringstream sstemp2;
  badSymbol = 0;
  for(int i=0; i<int(os.length()); i++){
    if(os[i] == '\\' || os[i] == '/' || os[i] == ':' || os[i] == '*' || os[i] == '?' || os[i] == '\"' || os[i] == '<' || os[i] == '>' || os[i] == '|'){
      sstemp2 << '_';
      badSymbol++;
    }
    else{
      sstemp2 << os[i];
    }
  }
  if(badSymbol){
	  System::Windows::Forms::MessageBox::Show(L"Bad Symbol in Subdirectory Name\r\n Replaced bad symbol with _",L"Bad Symbol in Directory Name", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
  }
  os = sstemp2.str();

  outfile.open(tempDir + "\\outputDmpsSubDir.txt");
  outfile << os;
  outfile.close();

  loadTabControl1(sender, e);
}

System::Void Form1::runTheApps(System::Object^  sender, System::EventArgs^  e) {
  Hide();
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  if(tabControl1->Enabled){
    saveAndRunTheApps2(sender, e);
	std::ofstream outGUI(tempDir + "\\outGUI.txt");
    outGUI << "0";
    outGUI.close();
  }
  else{
	std::ofstream outGUI(tempDir + "\\outGUI.txt");
    outGUI << "0";
    outGUI.close();
  }

  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string path = userProfile + "\\SysnativeBSODApps\\backup\\tempFileName.txt";
  remove(path.c_str());

  if(CheckDebugFile())
  {
    Form1::Close();
  }
  else
  {
	std::string tempDir = ConfigurationUtil::getTempDirectory();
	std::string tempPath = tempDir + "\\percentDone.txt";
	std::ofstream outPercentDone;
    outPercentDone.open(tempPath);
    outPercentDone << 339155;
    outPercentDone.close();    
    System::String^ str2 = L"No valid kd.exe path found ...\n\n" + 
      "Please use the Auto-Finding Tool or kd.exe Path(s) (Options/Windows) to add a valid kd.exe path ...";
    if(System::Windows::Forms::MessageBox::Show(str2,L"Close Auto-Finding Tool?", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error,
		System::Windows::Forms::MessageBoxDefaultButton::Button1) == System::Windows::Forms::DialogResult::OK)
    {
    }
    Form1::Close();
  }
}

System::Void Form1::runTheApps2(System::Object^  sender, System::EventArgs^  e) {
  std::string inChar = SystemStandardConversionUtil::convertSystemStringToStdString(textBox2->Text);
  textBox4->Text = gcnew System::String(inChar.c_str());
  std::stringstream ssforumString;
  // Remove html path characters from forum name
  for(int i77 = 0; i77 < int(inChar.length()); i77++){
    if(inChar.c_str()[i77] != '/' && inChar.c_str()[i77] != '.' && inChar.c_str()[i77] != ':' && inChar.c_str()[i77] != ' '){
      ssforumString << inChar.c_str()[i77];
    }
  }
  textBox2->Text = gcnew System::String(ssforumString.str().c_str());
  if(textBox2->Text == mForumName && mForumName != "")
  {
    button9_Click(sender,  e);
  }
  else
  {
    textBox2->Text = "wrongName";
    System::Windows::Forms::MessageBox::Show(L"Forum settings will be saved to %SysnativeBSODApps%\\forumSettings\\wrongName.zdn.\n Please make sure to enter the forum name in the originating post for the first screen.",L"Forum name changed", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
    button9_Click(sender,  e);
  }
  std::string tempDir = ConfigurationUtil::getTempDirectory();

  std::string tempPath = tempDir + "\\openingFile.txt";
  remove(tempPath.c_str());
  tempPath = tempDir + "\\openingFile2.txt";
  remove(tempPath.c_str());
  Hide();
  std::ofstream outPercentDone;
  outPercentDone.open(tempDir + "\\percentDone.txt");
  outPercentDone << 452312;
  outPercentDone.close();
  std::ofstream outProg;
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving first Screen Settings, Please Wait...";
  outProg.close();
  if(checkBox10->Checked)
  {
    Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
    newThread->Start();
  }
  runTheApps(sender,e);
}

// Process BSODs button
System::Void Form1::button2_Click(System::Object^  sender, System::EventArgs^  e){
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  Hide();
  std::ofstream outPercentDone;
  outPercentDone.open(tempDir + "\\percentDone.txt");
  outPercentDone << 452312;
  outPercentDone.close();
  std::ofstream outProg;
  std::string tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving first Screen Settings, Please Wait...";
  outProg.close();
  if(checkBox10->Checked){
    Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
    newThread->Start();
  }
  Hide();
  std::ofstream outfile;
  System::String^ s = textBox4->Text;
  const char* chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile.open(tempDir + "\\originatingPost.txt");
  outfile << os;
  outfile.close();

  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string dmpListTemp = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP")) + "\\SysnativeBSODApps\\";

  std::string DOParms = userProfile + "\\SysnativeBSODApps\\parms\\";
  std::string DODmpOptions = dmpListTemp;

  std::string pathDmpListTimeout = DODmpOptions + "dmpListTimeout.txt";

  DODmpOptions = userProfile + "\\SysnativeBSODApps\\dmpOptions\\";

  std::ofstream outDmpListTimeout(pathDmpListTimeout);
  outDmpListTimeout << int(numericUpDown4->Value);
  outDmpListTimeout.close();

  std::string inChar = os;
  std::ifstream openingFile(tempDir + "\\openingFile.txt");
  //If the user is not opening a file
  if(!openingFile.good()){
    //Get the forum the post came from to generate forum settings
    std::stringstream ssforumString;
    for(int i77 = 0; i77 < int(inChar.length()); i77++){
      if(inChar.c_str()[i77] != '/' && inChar.c_str()[i77] != '.' && inChar.c_str()[i77] != ':'){
        ssforumString << inChar.c_str()[i77];
      }
    }
	std::string forumString = ssforumString.str();
    if(forumString.find("answersmicrosoft") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\answersmicrosoft.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("socialmicrosoftcomForums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\socialmicrosoftcomForums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("technetmicrosoft") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\technetmicrosoft.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("sysnative") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\sysnative.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("forumstechguy") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumstechguy.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("geekstogo") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\geekstogo.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("howtogeek") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\howtogeek.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("majorgeeks") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\majorgeeks.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windowsforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowsforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("forumswindowsforum") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumswindowsforum.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("forummintywhite") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forummintywhite.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("winsource") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\winsource.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windowssecretscomforums") != std::string::npos){
	  std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowssecretscomforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windowsitpro") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowsitpro.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windows7forums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows7forums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("w7forums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\w7forums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("sevenforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\sevenforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("eightforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\eightforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windows8forums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows8forums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("win8forums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\win8forums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("techsupportforum") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\techsupportforum.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("bleepingcomputer") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\bleepingcomputer.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("tenforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\tenforums.zdn";
      openForumFile(forumFileName);
    }
  }
  openingFile.close();

  // write the $un95 file
  DOParms = tempDir + "\\";
  std::string inPath = DOParms + "un95";
  outfile.open(inPath);

  s = textBox11->Text;
  chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile << os;
  outfile.close();

  FileSystemUtil::copyFile(tempDir + "\\un95",userProfile + "\\SysnativeBSODApps\\parms\\un95", true);

  outfile.open(tempDir + "\\fullGUI.txt");
  outfile << checkBox10->Checked;
  outfile.close();

  s = textBox29->Text;
  chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  // \ / : * ? " < > | cannot be used for directory names
  std::stringstream sstemp;
  int badSymbol = 0;
  for(int i=0; i<int(os.length()); i++){
    if(os[i] == '\\' || os[i] == '/' || os[i] == ':' || os[i] == '*' || os[i] == '?' || os[i] == '\"' || os[i] == '<' || os[i] == '>' || os[i] == '|'){
      sstemp << '_';
      badSymbol++;
    }
    else{
      sstemp << os[i];
    }
  }
  if(badSymbol){
	  System::Windows::Forms::MessageBox::Show(L"Bad Symbol in Directory Name\r\n Replaced bad symbol with _",L"Bad Symbol in Directory Name", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
  }
  os = sstemp.str();

  outfile.open(tempDir + "\\outputDmpsDir.txt");
  outfile << os;
  outfile.close();

  s = textBox28->Text;
  chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  // \ / : * ? " < > | cannot be used for directory names
  std::stringstream sstemp2;
  badSymbol = 0;
  for(int i=0; i<int(os.length()); i++){
    if(os[i] == '\\' || os[i] == '/' || os[i] == ':' || os[i] == '*' || os[i] == '?' || os[i] == '\"' || os[i] == '<' || os[i] == '>' || os[i] == '|'){
      sstemp2 << '_';
      badSymbol++;
    }
    else{
      sstemp2 << os[i];
    }
  }
  if(badSymbol){
	  System::Windows::Forms::MessageBox::Show(L"Bad Symbol in Subdirectory Name\r\n Replaced bad symbol with _",L"Bad Symbol in Directory Name", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
  }
  os = sstemp2.str();

  outfile.open(tempDir + "\\outputDmpsSubDir.txt");
  outfile << os;
  outfile.close();

  tempPath = tempDir + "\\newDmpsOnly.txt";
  remove(tempPath.c_str());

  std::string temp = tempPath;
  outfile.open(temp);
  if(checkBox24->Checked){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  runTheApps(sender, e);
}

// Only use user kd commands
System::Void Form1::checkBox8_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
  if(checkBox8->Checked){
    // No need to run user kd commands first since only user kd commands are going to run
    checkBox9->Checked = false;
  }
}

// View Previous Output button
System::Void Form1::button4_Click(System::Object^  sender, System::EventArgs^  e){
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::ofstream outfile(tempDir + "\\viewPreviousHTML.txt");
  outfile << "1";
  outfile.close();
  std::ifstream infile(tempDir + "\\viewPreviousHTML.txt");
  while(!infile.good()){
    infile.close();
    outfile.open(tempDir + "\\viewPreviousHTML.txt");
    outfile << "1";
    outfile.close();
    infile.open(tempDir + "\\viewPreviousHTML.txt");
  }
  Close();
}

// Run user kd commands first
System::Void Form1::checkBox9_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox9->Checked){
    // implies that default kd commands should also be run
    checkBox8->Checked = false;
  }
}

// Load forumSettings file and save settings files separately
System::Void Form1::openForumFile(std::string os)
{
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::ifstream getSettings(os);
  std::string thisLine;
  while(getSettings.good() && !getSettings.eof() && os.length() > 0){
    if(thisLine.find("## ###") != std::string::npos){
      std::string thisFileName;
      thisFileName = userProfile + "\\SysnativeBSODApps\\" + thisLine.substr(6,thisLine.length()-6);
	  std::ofstream changeFile(thisFileName.c_str());
      while(!getSettings.eof() && getSettings.good()){
        std::string previousLine = thisLine;
        getline(getSettings,thisLine);
        int badFile = 0;
		std::string thisLine2;
        if(thisLine.find("## ###") != std::string::npos)
        {
          badFile = 1;
          thisLine2 = thisLine.substr(6,thisLine.length()-6);
        }
        while(!getSettings.eof() && badFile){
          if(thisLine2 == "parms\\$_code_box_begin" ||
            thisLine2 == "parms\\$_code_box_end" ||
            thisLine2 == "parms\\$_driver_update_header1" ||
            thisLine2 == "parms\\$_footer1" ||
            thisLine2 == "parms\\$_header1" ||
            thisLine2 == "parms\\$_sig1" ||
            thisLine2 == "parms\\un95" ||
            thisLine2 == "dmpOptions\\7oldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\8oldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\81oldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\10oldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\addSpaces.txt" ||
            thisLine2 == "dmpOptions\\allFilesDmps.txt" ||
            thisLine2 == "dmpOptions\\driverInOut.txt" ||
            thisLine2 == "dmpOptions\\EightED.txt" ||
            thisLine2 == "dmpOptions\\EightOneED.txt" ||
            thisLine2 == "dmpOptions\\TenED.txt" ||
            thisLine2 == "dmpOptions\\excludedDrivers.txt" ||
            thisLine2 == "dmpOptions\\fullGUI.txt" ||
            thisLine2 == "dmpOptions\\htmlBBCode.txt" ||
            thisLine2 == "dmpOptions\\inHtml.txt" ||
            thisLine2 == "dmpOptions\\kdCommands.txt" ||
            thisLine2 == "dmpOptions\\noBBCode.txt" ||
            thisLine2 == "dmpOptions\\noBBCodeCodeBoxes.txt" ||
            thisLine2 == "dmpOptions\\oldDriversRed.txt" ||
            thisLine2 == "dmpOptions\\onlyUser.txt" ||
            thisLine2 == "dmpOptions\\openOnExit.txt" ||
            thisLine2 == "dmpOptions\\outputOptions.txt" ||
            thisLine2 == "dmpOptions\\previous.txt" ||
            thisLine2 == "dmpOptions\\problemDrivers.txt" ||
            thisLine2 == "dmpOptions\\SevenED.txt" ||
            thisLine2 == "dmpOptions\\symbols.txt" ||
            thisLine2 == "dmpOptions\\template.txt" ||
            thisLine2 == "dmpOptions\\turnOffBBCode.txt" ||
            thisLine2 == "dmpOptions\\userFirst.txt" ||
            thisLine2 == "dmpOptions\\VistaED.txt" ||
            thisLine2 == "dmpOptions\\VistaoldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\XPED.txt" ||
            thisLine2 == "dmpOptions\\XPoldDriverAfter.txt"){
              badFile = 0;
              break;
          }
          getline(getSettings,thisLine);
          if(thisLine.find("## ###") != std::string::npos)
          {
            thisLine2 = thisLine.substr(6,thisLine.length()-6);
          }
        }
        if(!(previousLine.find("## ###") != std::string::npos) &&
          !(thisLine.find("## ###") != std::string::npos)/* &&
          !getSettings.eof()*/){
            changeFile << std::endl;
        }
        if(thisLine.find("## ###") != std::string::npos){
          break;
        }
        changeFile << thisLine;
      }
      if(thisLine.find("## ###") != std::string::npos){
        changeFile.close();
      }
    }
    else{
      getline(getSettings,thisLine);
    }
  }
  getSettings.close();
}

// Open quick save blank.zdn file
System::Void Form1::open(std::string os)
{
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::ifstream getSettings(os);
  std::string thisLine;
  while(getSettings.good() && !getSettings.eof() && os.length() > 0){
    if(thisLine.find("## ###") != std::string::npos){
      std::string thisFileName;
      thisFileName = userProfile + "\\SysnativeBSODApps\\" + thisLine.substr(6,thisLine.length()-6);
	  std::ofstream changeFile(thisFileName.c_str());
      while(!getSettings.eof() && getSettings.good()){
        std::string previousLine = thisLine;
        getline(getSettings,thisLine);
        int badFile = 0;
		std::string thisLine2;
        if(thisLine.find("## ###") != std::string::npos)
        {
          badFile = 1;
          thisLine2 = thisLine.substr(6,thisLine.length()-6);
        }
        while(!getSettings.eof() && badFile){
          if(thisLine2 == "parms\\$_code_box_begin" ||
            thisLine2 == "parms\\$_code_box_end" ||
            //thisLine2 == "parms\\$_dbugdir" ||
            thisLine2 == "parms\\$_driver_update_header1" ||
            thisLine2 == "parms\\$_footer1" ||
            thisLine2 == "parms\\$_header1" ||
            thisLine2 == "parms\\$_sig1" ||
            thisLine2 == "parms\\un95" ||
            thisLine2 == "dmpOptions\\7oldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\8oldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\81oldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\10oldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\addSpaces.txt" ||
            thisLine2 == "dmpOptions\\allFilesDmps.txt" ||
            thisLine2 == "dmpOptions\\driverInOut.txt" ||
            thisLine2 == "dmpOptions\\EightED.txt" ||
            thisLine2 == "dmpOptions\\EightOneED.txt" ||
            thisLine2 == "dmpOptions\\TenED.txt" ||
            thisLine2 == "dmpOptions\\excludedDrivers.txt" ||
            thisLine2 == "dmpOptions\\fullGUI.txt" ||
            thisLine2 == "dmpOptions\\htmlBBCode.txt" ||
            thisLine2 == "dmpOptions\\inHtml.txt" ||
            thisLine2 == "dmpOptions\\kdCommands.txt" ||
            //thisLine2 == "dmpOptions\\kdPaths.txt" ||
            thisLine2 == "dmpOptions\\noBBCode.txt" ||
            thisLine2 == "dmpOptions\\noBBCodeCodeBoxes.txt" ||
            thisLine2 == "dmpOptions\\oldDriversRed.txt" ||
            thisLine2 == "dmpOptions\\onlyUser.txt" ||
            thisLine2 == "dmpOptions\\openOnExit.txt" ||
            thisLine2 == "dmpOptions\\outputOptions.txt" ||
            thisLine2 == "dmpOptions\\previous.txt" ||
            thisLine2 == "dmpOptions\\problemDrivers.txt" ||
            thisLine2 == "dmpOptions\\SevenED.txt" ||
            thisLine2 == "dmpOptions\\symbols.txt" ||
            thisLine2 == "dmpOptions\\template.txt" ||
            thisLine2 == "dmpOptions\\turnOffBBCode.txt" ||
            thisLine2 == "dmpOptions\\userFirst.txt" ||
            thisLine2 == "dmpOptions\\VistaED.txt" ||
            thisLine2 == "dmpOptions\\VistaoldDriverAfter.txt" ||
            thisLine2 == "dmpOptions\\XPED.txt" ||
            thisLine2 == "dmpOptions\\XPoldDriverAfter.txt"){
              badFile = 0;
              break;
          }
          getline(getSettings,thisLine);
          if(thisLine.find("## ###") != std::string::npos)
          {
            thisLine2 = thisLine.substr(6,thisLine.length()-6);
          }
        }
        if(!(previousLine.find("## ###") != std::string::npos) &&
          !(thisLine.find("## ###") != std::string::npos)/* &&
          !getSettings.eof()*/){
            changeFile << std::endl;
        }
        if(thisLine.find("## ###") != std::string::npos){
          break;
        }
        changeFile << thisLine;
      }
      if(thisLine.find("## ###") != std::string::npos){
        changeFile.close();
      }
    }
    else{
      getline(getSettings,thisLine);
    }
  }
  getSettings.close();
}

//Open the help .chm file
System::Void Form1::Help(System::Object^  sender, System::EventArgs^  e)
{
  std::string path = ConfigurationUtil::getUserProfilePath() + "\\SysnativeBSODApps\\SysnativeBSODApps.chm";
  System::String^ str2 = gcnew System::String(path.c_str());
  System::Windows::Forms::Help::ShowHelp(this, str2);
}

// Compare two std::strings regardless of case
bool Form1::unSignedCompare(std::string str1, std::string str2)
{
  if (str1.size() != str2.size()) {
    return false;
  }
  for (std::string::const_iterator c1 = str1.begin(), c2 = str2.begin(); c1 != str1.end(); ++c1, ++c2) {
    // Convert both cases to lowercase prior to comparing
    if (tolower(*c1) != tolower(*c2)) {
      return false;
    }
  }
  return true;
}

// Revert to Last Quick Save
System::Void Form1::loadPreviousSettings2(System::Object^  sender, System::EventArgs^  e)
{
  Hide();
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::ofstream openingFile(tempDir + "\\openingFile.txt");
  openingFile << 0;
  openingFile.close();
  std::ofstream openingFile2(tempDir + "\\openingFile2.txt");
  openingFile2 << 0;
  openingFile2.close();
  std::ofstream outPercentDone(tempDir + "\\percentDone.txt");
  outPercentDone << 452312;
  outPercentDone.close();
  std::ofstream outProg;
  std::string tempPath;
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving first Screen Settings, Please Wait...";
  outProg.close();
  if(checkBox10->Checked){
    Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
    newThread->Start();
  }
  loadPreviousSettings(sender,e);

  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  outPercentDone << 100;
  outPercentDone.close();
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  remove(std::string(userProfile + "\\SysnativeBSODApps\\backup\\tempFileName.txt").c_str());
  Show();
}

// load all settings from file
System::Void Form1::loadPreviousSettings(System::Object^  sender, System::EventArgs^  e)
{
  // Using CDPI example class
  CDPI g_metrics;
  int x, y;
  int dpiX = g_metrics.GetDPIX();

  tabControl1->Height = this->Height - 110 - int(double(100)*double(dpiX-96)/double(64));
  tabControl1->Width = this->Width - int(double(20)*double(dpiX)/double(96));
  richTextBox1->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox1->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox2->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox2->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox3->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox3->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox4->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox4->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox5->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox5->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox6->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox7->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox8->Height = tabControl1->Height - 200 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox9->Height = tabControl1->Height - 200 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox10->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox11->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox12->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox13->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));

  x = 0;
  y = this->Height - label12->Height - int(double(50) * double(dpiX)/double(96));
  this->label12->Location = System::Drawing::Point(x, y);

  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::ofstream outProg;

  std::string DODmpOptions = userProfile + "\\SysnativeBSODApps\\dmpOptions";
  std::string DOParms = userProfile + "\\SysnativeBSODApps\\parms\\";

  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::string tempPath;
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Checking forum settings from Originating Post input, Please Wait...";
  std::cout << "Checking forum settings from Originating Post input, Please Wait..." << std::endl;
  outProg.close();

  // read in the originatingPost.txt file
  std::string inChar;
  System::String^ str2;
  std::string inPath = tempDir + "\\originatingPost.txt";
  std::ifstream infile;
  infile.open(inPath);
  if(infile.good()){
    getline(infile,inChar);
    infile.close();
	std::string originatingPost = inChar;
    str2 = gcnew System::String(originatingPost.c_str());
	std::stringstream ssforumString;

	std::ifstream openingFile(tempDir + "\\openingFile.txt");
	std::ifstream openingFile2(tempDir + "\\openingFile2.txt");

    //If the user is not opening a file
    if(!openingFile.good()){
    //Get the forum the post came from to generate forum settings
      for(int i77 = 0; i77 < int(inChar.length()); i77++){
        if(inChar.c_str()[i77] != '/' && inChar.c_str()[i77] != '.' && inChar.c_str()[i77] != ':'){
          ssforumString << inChar.c_str()[i77];
        }
      }
	  std::string forumString = ssforumString.str();
      if(forumString.find("answersmicrosoft") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\answersmicrosoft.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("socialmicrosoftcomForums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\socialmicrosoftcomForums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("technetmicrosoft") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\technetmicrosoft.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("sysnative") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\sysnative.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("forumstechguy") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumstechguy.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("geekstogo") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\geekstogo.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("howtogeek") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\howtogeek.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("majorgeeks") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\majorgeeks.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("forumscnetcom") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumscnetcom.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("windowsforums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowsforums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("forumswindowsforum") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumswindowsforum.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("forummintywhite") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forummintywhite.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("winsource") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\winsource.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("windowssecretscomforums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowssecretscomforums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("windowsitpro") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowsitpro.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("windows7forums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows7forums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("w7forums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\w7forums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("sevenforums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\sevenforums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("eightforums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\eightforums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("windows8forums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows8forums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("win8forums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows8forums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("techsupportforum") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\techsupportforum.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("bleepingcomputer") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\bleepingcomputer.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.find("tenforums") != std::string::npos){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\tenforums.zdn";
        openForumFile(forumFileName);
      }
      if(forumString.length() == 0){
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\blankSaveAndRun.zdn";
        openForumFile(forumFileName);
      }
    }
    if(openingFile2.good()){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\blank.zdn";
      open(forumFileName);
    }
    openingFile.close();
    openingFile2.close();
    tempPath = tempDir + "\\openingFile.txt";
    remove(tempPath.c_str());
    tempPath = tempDir + "\\openingFile2.txt";
    remove(tempPath.c_str());
  }
  else{
    str2 = "";
  }
  textBox2->Text = str2;

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading Files to Save and Output Options settings, Please Wait...";
  std::cout << "Loading Files to Save and Output Options settings, Please Wait..." << std::endl;
  outProg.close();

  std::string temp = DODmpOptions + "\\outputOptions.txt";
  infile.open(temp);
  // Determine whether the outputOptions.txt file exists
  if(infile.good()){
    checkedListBox2->SetItemChecked(0, false);
    checkedListBox2->SetItemChecked(1, false);
    checkedListBox2->SetItemChecked(2, false);
    checkedListBox2->SetItemChecked(3, false);
    checkedListBox2->SetItemChecked(4, false);
    checkedListBox2->SetItemChecked(5, false);
    checkedListBox2->SetItemChecked(6, false);
    checkedListBox2->SetItemChecked(7, false);
    checkedListBox2->SetItemChecked(8, false);
    checkedListBox2->SetItemChecked(9, false);
    checkedListBox2->SetItemChecked(10, false);
    checkedListBox2->SetItemChecked(11, false);
    checkedListBox2->SetItemChecked(12, false);
    checkedListBox2->SetItemChecked(13, false);
    checkedListBox2->SetItemChecked(14, false);
    checkedListBox2->SetItemChecked(15, false);
    checkedListBox2->SetItemChecked(16, false);
    checkedListBox2->SetItemChecked(17, false);
    checkedListBox2->SetItemChecked(18, false);
    checkedListBox2->SetItemChecked(19, false);
    checkedListBox2->SetItemChecked(20, false);
    checkedListBox2->SetItemChecked(21, false);
    checkedListBox2->SetItemChecked(22, false);
    checkedListBox2->SetItemChecked(23, false);
    checkedListBox2->SetItemChecked(24, false);
    checkedListBox2->SetItemChecked(25, false);
    checkedListBox2->SetItemChecked(26, false);
    checkedListBox2->SetItemChecked(27, false);
    checkedListBox1->SetItemChecked(0, false);
    checkedListBox1->SetItemChecked(1, false);
    checkedListBox1->SetItemChecked(2, false);
    checkedListBox1->SetItemChecked(3, false);
    checkedListBox1->SetItemChecked(4, false);
    checkedListBox1->SetItemChecked(5, false);
    checkedListBox1->SetItemChecked(6, false);
    checkedListBox1->SetItemChecked(7, false);
    checkedListBox1->SetItemChecked(8, false);
    checkedListBox1->SetItemChecked(9, false);
    checkedListBox1->SetItemChecked(10, false);
    checkedListBox1->SetItemChecked(11, false);
    checkedListBox1->SetItemChecked(12, false);
    checkedListBox1->SetItemChecked(13, false);
    checkedListBox1->SetItemChecked(14, false);
    // get the output options for the .dmp file analysis information
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        size_t found;
        found = inChar.find("outputDmps");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(0, true);
        }
        found = inChar.find("outputFullDrivers");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(1, true);
        }
        found = inChar.find("outputThirdPartyDriversName");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(2, true);;
        }
        found = inChar.find("outputThirdPartyDriversDate");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(3, true);
        }
        found = inChar.find("outputImportantInfo");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(4, true);
        }
        found = inChar.find("outputStack");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(5, true);
        }
        found = inChar.find("outputMissingFromDRT");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(6, true);
        }
        found = inChar.find("outputUnloadedDrivers");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(7, true);
        }
        found = inChar.find("outputSMBIOS");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(8, true);
        }
        found = inChar.find("outputTemplate");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(9, true);
        }
        found = inChar.find("outputNinetyNine");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(10, true);
        }
        found = inChar.find("outputNinetyEight");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(11, true);
        }
        found = inChar.find("outputNinetySeven");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(12, true);
        }
        found = inChar.find("outputNinetyFive");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(13, true);
        }
        found = inChar.find("outputEightyEight");
        if(found != std::string::npos){
          checkedListBox1->SetItemChecked(14, true);
        }
        found = inChar.find("loadingDumpFile");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(0, true);
        }
        found = inChar.find("kernelVersion");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(1, true);
        }
        found = inChar.find("missingServicePack");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(2, true);
        }
        found = inChar.find("builtBy");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(3, true);
        }
        found = inChar.find("debugSessionTime");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(4, true);
        }
        found = inChar.find("systemUptime");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(5, true);
        }
        found = inChar.find("bugCheck");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(6, true);
        }
        found = inChar.find("unableToVerify");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(7, true);
        }
        found = inChar.find("symbolsNotLoaded");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(8, true);
        }
        found = inChar.find("probablyCausedBy");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(9, true);
        }
        found = inChar.find("defaultBucketID");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(10, true);
        }
        found = inChar.find("verifier");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(11, true);
        }
        found = inChar.find("bugcheckStr");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(12, true);
        }
        found = inChar.find("bugCheckAnalysis");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(13, true);
        }
        found = inChar.find("processName");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(14, true);
        }
        found = inChar.find("failureBucketID");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(15, true);
        }
        found = inChar.find("bugcheckCode");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(16, true);
        }
        found = inChar.find("errorCode");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(17, true);
        }
        found = inChar.find("diskHardwareError");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(18, true);
        }
        found = inChar.find("arguments");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(19, true);
        }
        found = inChar.find("CPUID");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(20, true);
        }
        found = inChar.find("maxSpeed");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(21, true);
        }
        found = inChar.find("currentSpeed");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(22, true);
        }
        found = inChar.find("overclock");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(27, true);
        }
        found = inChar.find("BIOSVersion");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(23, true);
        }
        found = inChar.find("BIOSReleaseDate");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(24, true);
        }
        found = inChar.find("systemManufacturer");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(25, true);
        }
        found = inChar.find("systemProductName");
        if(found != std::string::npos){
          checkedListBox2->SetItemChecked(26, true);
        }
      }
    }
  }
  else{
    // the default for all options is one if the file does not exist
    checkedListBox1->SetItemChecked(0, false);
    checkedListBox1->SetItemChecked(1, false);
    checkedListBox1->SetItemChecked(2, false);
    checkedListBox1->SetItemChecked(3, false);
    checkedListBox1->SetItemChecked(4, false);
    checkedListBox1->SetItemChecked(5, false);
    checkedListBox1->SetItemChecked(6, false);
    checkedListBox1->SetItemChecked(7, false);
    checkedListBox1->SetItemChecked(8, false);
    checkedListBox1->SetItemChecked(9, false);
    checkedListBox1->SetItemChecked(10, true);
    checkedListBox1->SetItemChecked(11, true);
    checkedListBox1->SetItemChecked(12, true);
    checkedListBox1->SetItemChecked(13, true);
    checkedListBox1->SetItemChecked(14, true);
    checkedListBox2->SetItemChecked(0, true);
    checkedListBox2->SetItemChecked(1, true);
    checkedListBox2->SetItemChecked(2, true);
    checkedListBox2->SetItemChecked(3, true);
    checkedListBox2->SetItemChecked(4, true);
    checkedListBox2->SetItemChecked(5, true);
    checkedListBox2->SetItemChecked(6, true);
    checkedListBox2->SetItemChecked(7, true);
    checkedListBox2->SetItemChecked(8, true);
    checkedListBox2->SetItemChecked(9, true);
    checkedListBox2->SetItemChecked(10, true);
    checkedListBox2->SetItemChecked(11, true);
    checkedListBox2->SetItemChecked(12, true);
    checkedListBox2->SetItemChecked(13, true);
    checkedListBox2->SetItemChecked(14, true);
    checkedListBox2->SetItemChecked(15, true);
    checkedListBox2->SetItemChecked(16, true);
    checkedListBox2->SetItemChecked(17, true);
    checkedListBox2->SetItemChecked(18, true);
    checkedListBox2->SetItemChecked(19, true);
    checkedListBox2->SetItemChecked(20, true);
    checkedListBox2->SetItemChecked(21, true);
    checkedListBox2->SetItemChecked(22, true);
    checkedListBox2->SetItemChecked(23, true);
    checkedListBox2->SetItemChecked(24, true);
    checkedListBox2->SetItemChecked(25, true);
    checkedListBox2->SetItemChecked(26, true);
  }
  infile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading Formatting and Miscellaneous Options settings, Please Wait...";
  std::cout << "Loading Formatting and Miscellaneous Options settings, Please Wait..." << std::endl;
  outProg.close();

  int fullGUI = 1;

  if(fullGUI){
    checkBox10->Checked = true;
  }
  else{
    checkBox10->Checked = true;
  }

  temp = DODmpOptions + "\\addSpaces.txt";
  infile.open(temp);
  int addSpaces = 0;
  // Determine whether the addSpaces.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> addSpaces;
      }
    }
  }
  else{
    // if the file does not exist, add spaces as default action
    addSpaces = 1;
  }
  if(addSpaces){
    checkedListBox3->SetItemChecked(0, true);
  }
  else{
    checkedListBox3->SetItemChecked(0, false);
  }
  infile.close();

  temp = DODmpOptions + "\\noBBCode.txt";
  infile.open(temp);
  int noBBCode = 0;
  // Determine whether the noBBCode.txt file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> noBBCode;
      }
    }
  }
  else{
    // if the file does not exist, turn off BBCode by default
    noBBCode = 1;
  }
  if(noBBCode){
    checkedListBox3->SetItemChecked(1, true);
  }
  else{
    checkedListBox3->SetItemChecked(1, false);
  }
  infile.close();

  temp = DODmpOptions + "\\htmlBBCode.txt";
  infile.open(temp);
  int noBBCodeHtml = 0;
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> noBBCodeHtml;
      }
    }
  }
  else{
    // if the file does not exist, output only .txt files as default action
    noBBCodeHtml = 0;
  }
  if(noBBCodeHtml){
    checkedListBox3->SetItemChecked(2, true);
  }
  else{
    checkedListBox3->SetItemChecked(2, false);
  }
  infile.close();

  temp = DODmpOptions + "\\turnOffBBCode.txt";
  infile.open(temp);
  int turnOffBBCode = 0;
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> turnOffBBCode;
      }
    }
  }
  else{
    // if the file does not exist, turn on BBCode as default action
    turnOffBBCode = 0;
  }
  if(turnOffBBCode){
    checkedListBox3->SetItemChecked(3, true);
  }
  else{
    checkedListBox3->SetItemChecked(3, false);
  }
  infile.close();

  temp = DODmpOptions + "\\noBBCodeCodeBoxes.txt";
  infile.open(temp);
  int noBBCodeCodeBoxes = 0;
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> noBBCodeCodeBoxes;
      }
    }
  }
  else{
    // if the file does not exist, turn on BBCode as default action
    noBBCodeCodeBoxes = 0;
  }
  if(noBBCodeCodeBoxes){
    checkedListBox3->SetItemChecked(4, true);
  }
  else{
    checkedListBox3->SetItemChecked(4, false);
  }
  infile.close();

  temp = DODmpOptions + "\\oldDriversRed.txt";
  infile.open(temp);
  int oldDriversRed = 0;
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> oldDriversRed;
      }
    }
  }
  else{
    // if the file does not exist, turn on red as default action
    oldDriversRed = 1;
  }
  if(oldDriversRed){
    checkedListBox3->SetItemChecked(5, true);
  }
  else{
    checkedListBox3->SetItemChecked(5, false);
  }
  infile.close();

  temp = DODmpOptions + "\\allFilesDmps.txt";
  infile.open(temp);
  int allFilesDmps = 0;
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> allFilesDmps;
      }
    }
  }
  else{
    // if the file does not exist, check all files as default action
    allFilesDmps = 1;
  }
  if(allFilesDmps){
    checkedListBox3->SetItemChecked(6, true);
  }
  else{
    checkedListBox3->SetItemChecked(6, false);
  }
  infile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading User kd Commands settings, Please Wait...";
  std::cout << "Loading User kd Commands settings, Please Wait..." << std::endl;
  outProg.close();

  temp = DODmpOptions + "\\onlyUser.txt";
  infile.open(temp);
  int onlyUser = 0;
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> onlyUser;
      }
    }
  }
  else{
    // if the file does not exist, use default kd commands
    onlyUser = 0;
  }
  if(onlyUser){
    checkBox8->Checked = true;
  }
  else{
    checkBox8->Checked = false;
  }
  infile.close();

  temp = DODmpOptions + "\\userFirst.txt";
  infile.open(temp);
  int userFirst = 0;
  // Determine whether the file exists
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> userFirst;
      }
    }
  }
  else{
    // if the file does not exist, use default kd commands first
    userFirst = 0;
  }
  if(userFirst){
    checkBox9->Checked = true;
  }
  else{
    checkBox9->Checked = false;
  }
  infile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading User symbols paths, Please Wait...";
  std::cout << "Loading User symbols paths, Please Wait..." << std::endl;
  outProg.close();

  std::string options = DODmpOptions + "\\symbols.txt";
  std::ifstream infileOptions(options);
  std::string inputCh;
  std::string symbolsString = "";
  std::string symbolsString2 = "";
  int symbolsInt;
  // Determine whether the symbols.txt file exists
  if(infileOptions.good()){
    // If it exists, obtain the symbols path
    size_t foundPound;
    // Get only the symbols path and ignore any commented lines
    int countOptions = 0;
    while(!infileOptions.eof()){
      getline(infileOptions,inputCh);
      foundPound = inputCh.find("#");
      while(foundPound != std::string::npos){
        getline(infileOptions,inputCh);
        foundPound = inputCh.find("#");
      }
      countOptions++;
      if (countOptions == 1){
        symbolsString = inputCh;
      }
      if (countOptions == 2){
        if(inputCh == "1" || inputCh == "2"){
          std::stringstream sstemp;
          sstemp << inputCh;
          sstemp >> symbolsInt;
        }
        else{
          symbolsString2 = inputCh;
        }
      }
      if (countOptions == 3){
        if(inputCh == "1" || inputCh == "2"){
          std::stringstream sstemp;
          sstemp << inputCh;
          sstemp >> symbolsInt;
        }
      }
      if (countOptions == 4){
        if(inputCh == "1" || inputCh == "0"){
          if(inputCh == "1"){
            checkBox1->Checked = true;
            checkBox2->Checked = false;
          }
          else{
            checkBox1->Checked = false;
            checkBox2->Checked = true;
            checkBox3->Checked = false;
            checkBox4->Checked = false;
          }
        }
      }
    }
    infileOptions.close();
  }
  else{
    // if it does not exist, use online symbols and create symbols.txt
    symbolsString = "c:\\symbols";
    symbolsString2 = "srv*c:\\symbols*https://msdl.microsoft.com/download/symbols";
    checkBox1->Checked = true;
    checkBox2->Checked = false;
    checkBox3->Checked = true;
    checkBox4->Checked = false;
    infileOptions.close();
	std::ofstream outfile(options);
    outfile << symbolsString << std::endl;
    outfile << symbolsString2 << std::endl;
    outfile << "2" << std::endl;
    outfile << "1";
    outfile.close();
  }
  size_t found;
  found = symbolsString.find("http");
  // if the path is already to the internet, disregard the integer 2 option (it was likely user error)
  if(found != std::string::npos && symbolsString2 == ""){
    symbolsString2 = symbolsString;
    int asterisks = 0;
	std::stringstream symbolsStringTemp;
    for(int i = 0; i < int(symbolsString2.length()); i++){
      if(asterisks == 1 && symbolsString2.c_str()[i] != '*'){
        symbolsStringTemp << symbolsString2.c_str()[i];
      }
      if(symbolsString2.c_str()[i] == '*'){
        asterisks++;
      }
    }
    symbolsString = symbolsStringTemp.str();
    checkBox1->Checked = false;
    checkBox2->Checked = true;
    checkBox3->Checked = false;
    checkBox4->Checked = false;
  }
  else if(symbolsString2 == ""){
    symbolsString2 = "SRV*" + symbolsString + "*https://msdl.microsoft.com/download/symbols";
    checkBox1->Checked = true;
    checkBox2->Checked = false;
  }
  int asterisks = 0;
  for(int i = 0; i < int(symbolsString2.length()); i++){
    if(symbolsString2.c_str()[i] == '*'){
      asterisks++;
      if(asterisks > 2){
        std::string errorString;
        errorString = "You have too many *s in your symbols paths.";
        errorString = errorString + "\r\n\r\n";
        errorString = errorString + "Check your paths.";
        // convert the command std::string to wstring format
        System::String^ str2 = gcnew System::String(errorString.c_str());
		System::Windows::Forms::MessageBox::Show(str2,L"Too many *s", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
        break;
      }
    }
  }
  asterisks = 0;
  for(int i = 0; i < int(symbolsString.length()); i++){
    if(symbolsString.c_str()[i] == '*'){
      asterisks++;
      if(asterisks > 0){
        std::string errorString;
        errorString = "You have too many *s in your symbols paths.";
        errorString = errorString + "\r\n\r\n";
        errorString = errorString + "Check your paths.";
        // convert the command std::string to wstring format
        System::String^ str2 = gcnew System::String(errorString.c_str());
		System::Windows::Forms::MessageBox::Show(str2,L"Too many *s", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
        break;
      }
    }
  }
  str2 = gcnew System::String(symbolsString.c_str());
  textBox3->Text = str2;
  str2 = gcnew System::String(symbolsString2.c_str());
  textBox10->Text = str2;
  if(checkBox1->Checked && symbolsInt == 2){
    checkBox3->Checked = true;
    checkBox4->Checked = false;
  }
  else if(checkBox1->Checked){
    checkBox3->Checked = false;
    checkBox4->Checked = true;
  }

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading Driver Reference Table user input, Please Wait...";
  std::cout << "Loading Driver Reference Table user input, Please Wait..." << std::endl;
  outProg.close();

  // read in the $un95 file
  inPath = DOParms + "un95";
  infile.open(inPath);
  if(infile.good()){
    getline(infile,inChar);
    infile.close();
	std::string un95 = inChar;
    str2 = gcnew System::String(un95.c_str());
  }
  else{
    str2 = "";
  }
  textBox1->Text = str2;

  // Read lines from the dmpOptions driverInOut.txt file
  std::string driverInSite;
  std::string driverOutSite;
  options = DODmpOptions + "\\driverInOut.txt";
  infileOptions.open(options);
  // Determine whether the driverInOut.txt file exists
  if(infileOptions.good()){
    // If it exists, obtain the formatting of the driver lists
    size_t foundPound;
    // Get only the formatting information and ignore commented lines
    int countOptions = 0;
    while(!infileOptions.eof()){
      getline(infileOptions,inputCh);
      foundPound = inputCh.find("#");
      while(foundPound != std::string::npos){
        getline(infileOptions,inputCh);
        foundPound = inputCh.find("#");
      }
      countOptions++;
      if (countOptions < 2){
        driverInSite = inputCh;
      }
      else if(countOptions < 3){
        driverOutSite = inputCh;
      }
    }
    infileOptions.close();
  }
  else{
    // if driverInOut.txt does not exist, create the formatting for the drivers by default
    driverInSite = "https://www.sysnative.com/drivers/driver.php?id=";
    driverOutSite = "https://www.sysnative.com/drivers/driver.php?id=[B][COLOR=BLUE]<driver Name>[/COLOR][/B]";
    infileOptions.close();
	std::ofstream outfile(options);
    outfile << driverInSite << std::endl;
    outfile << driverOutSite;
    outfile.close();
  }
  str2 = gcnew System::String(driverInSite.c_str());
  textBox6->Text = str2;
  str2 = gcnew System::String(driverOutSite.c_str());
  textBox5->Text = str2;

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading formatting std::strings for _88-debug.txt and _98-debug.txt, Please Wait...";
  std::cout << "Loading formatting std::strings for _88-debug.txt and _98-debug.txt, Please Wait..." << std::endl;
  outProg.close();

  // read in the $_header1 file
  inPath = DOParms + "$_header1";
  infile.open(inPath);
  std::string header1;
  richTextBox1->Clear();
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      header1 = inCh;
      if(!infile.eof()){
        header1 = header1 + "\n";
      }
      str2 = gcnew System::String(header1.c_str());
      richTextBox1->AppendText(str2);
    }
  }
  else{
    richTextBox1->AppendText("");
  }
  infile.close();

  // read in the $_footer file
  inPath = DOParms + "$_footer1";
  infile.open(inPath);
  std::string footer1;
  richTextBox2->Clear();
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      footer1 = inCh;
      if(!infile.eof()){
        footer1 = footer1 + "\n";
      }
      str2 = gcnew System::String(footer1.c_str());
      richTextBox2->AppendText(str2);
    }
  }
  else{
    richTextBox2->AppendText("");
  }
  infile.close();

  // read in the $_sig1 file
  inPath = DOParms + "$_sig1";
  infile.open(inPath);
  std::string sig1;
  richTextBox3->Clear();
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      sig1 = inCh;
      if(!infile.eof()){
        sig1 = sig1 + "\n";
      }
      str2 = gcnew System::String(sig1.c_str());
      richTextBox3->AppendText(str2);
    }
  }
  else{
    richTextBox3->AppendText("");
  }
  infile.close();

  // read in the $_driver_update_header1 file
  inPath = DOParms + "$_driver_update_header1";
  infile.open(inPath);
  std::string driverUpdateHeader1;
  richTextBox4->Clear();
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      driverUpdateHeader1 = inCh;
      if(!infile.eof()){
        driverUpdateHeader1 = driverUpdateHeader1 + "\n";
      }
      str2 = gcnew System::String(driverUpdateHeader1.c_str());
      richTextBox4->AppendText(str2);
    }
  }
  else{
    richTextBox4->AppendText("");
  }
  infile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading template.txt input, Please Wait...";
  std::cout << "Loading template.txt input, Please Wait..." << std::endl;
  outProg.close();

  // read in the $_driver_update_header1 file
  inPath = DODmpOptions + "\\template.txt";
  infile.open(inPath);
  std::string template1;
  richTextBox5->Clear();
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      template1 = inCh;
      size_t found;
      // check for 3rd party drivers section
      found = template1.find("<3rd Party Drivers>");
      size_t found2;
      found2 = template1.find("<3rd Party Drivers");
      if(found != std::string::npos){
      }
      else if(found2 != std::string::npos){
        // Use the triangle brackets to find where the driver list starts
        int thirdParty = 0;
        int triangle = 0;
		std::stringstream template2;
        for(int j = 0; j < int(template1.length()); j++){
          if(template1.c_str()[j] == '<'){
            // driver format starts with the first triangle bracket
            thirdParty = 1;
            triangle = 1;
          }
          // determine whether the user wants to list drivers by name or date
          found = template1.find("date");
          found2 = template1.find("name");
          if(found != std::string::npos){
            if(thirdParty){
              found = template1.find("descriptions");
              if(found != std::string::npos){
                template2 << "<3rd Party Drivers><date><descriptions>";
                thirdParty = 0;
              }
              else{
                template2 << "<3rd Party Drivers><date>";
                thirdParty = 0;
              }
            }
            else if(!triangle){
              template2 << template1.c_str()[j];
            }
          }
          else if(found2 != std::string::npos){
            if(thirdParty){
              found = template1.find("descriptions");
              if(found != std::string::npos){
                template2 << "<3rd Party Drivers><name><descriptions>";
                thirdParty = 0;
              }
              else{
                template2 << "<3rd Party Drivers><name>";
                thirdParty = 0;
              }
            }
            else if(!triangle){
              template2 << template1.c_str()[j];
            }
          }
          else{
            if(thirdParty){
              found = template1.find("descriptions");
              if(found != std::string::npos){
                template2 << "<3rd Party Drivers><date><descriptions>";
                thirdParty = 0;
              }
              else{
                template2 << "<3rd Party Drivers><date>";
                thirdParty = 0;
              }
            }
            else if(!triangle){
              template2 << template1.c_str()[j];
            }
          }
          if(template1.c_str()[j] == '>'){
            // driver format ends with the second triangle bracket
            thirdParty = 0;
            triangle = 0;
          }
        }
        template1 = template2.str();
      }
      if(!infile.eof()){
        template1 = template1 + "\n";
      }
      str2 = gcnew System::String(template1.c_str());
      richTextBox5->AppendText(str2);
    }
  }
  else{
    richTextBox5->AppendText("");
  }
  infile.close();

  using namespace System::Runtime::InteropServices;

  richTextBox6->Clear();
  richTextBox7->Clear();

  System::String ^ s = richTextBox6->Text;
  const char* chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading Excluded Drivers list, Please Wait...";
  std::cout << "Loading Excluded Drivers list, Please Wait..." << std::endl;
  outProg.close();

  std::ofstream tempExcluded("tempExcludedDrivers.txt");

  tempExcluded << os;
  tempExcluded.close();

  s = richTextBox7->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  tempExcluded.open("tempDates.txt");

  tempExcluded << os;
  tempExcluded.close();

  std::vector<std::string> excludedNames;
  std::vector<std::string> excludedDates;

  // read in the Excluded Driver Names
  inPath = "tempExcludedDrivers.txt";
  infile.open(inPath);
  int num = 0;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
	  std::string newString = " ";
      getline(infile,inCh);
      num++;
	  std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
	  std::stringstream ssnum2;
	  std::stringstream ssErase;
      for(int i = 0; i < int(inCh.length()); i++){
        if(inCh.c_str()[i] == ' '){
          if(i > 0 && inCh.c_str()[i-1] != ' '){
            ssErase << inCh.c_str()[i];
          }
        }
        else{
          ssErase << inCh.c_str()[i];
        }
      }
      inCh = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inCh.length()); i++){
        if(intFound && !brokenList2 && (inCh.c_str()[i] == ' '  || inCh.c_str()[i] == ')'  || inCh.c_str()[i] == '.'
          || inCh.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inCh.c_str()[i] == '0' || inCh.c_str()[i] == '1' || inCh.c_str()[i] == '2' ||
          inCh.c_str()[i] == '3' || inCh.c_str()[i] == '4' || inCh.c_str()[i] == '5' ||
          inCh.c_str()[i] == '6' || inCh.c_str()[i] == '7' || inCh.c_str()[i] == '8' ||
          inCh.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inCh.c_str()[i];
        }
        else if(intFound && !brokenList && inCh.c_str()[i] != ' '  && inCh.c_str()[i] != ')'  && inCh.c_str()[i] != '.'
          && inCh.c_str()[i] != '}'){
            find2 = i;
            brokenList = 1;
            brokenList2 = 1;
        }
      }
      if(find2 == 0 && find < 2){
        find2 = inCh.length();
      }
      if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
        newString = inCh.substr(find2,inCh.length() - find2);
      }
      else{
        newString = inCh;
      }
      if(int(newString.length()) > 0){
        excludedNames.push_back(newString);
      }
    }
  }
  else{
  }
  infile.close();
  remove("tempExcludedDrivers.txt");

  // read in the Excluded Driver Dates
  inPath = "tempDates.txt";
  infile.open(inPath);
  std::string excludedDrivers;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
	  std::string newString;
      getline(infile,inCh);
      num++;
	  std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
	  std::stringstream ssnum2;
	  std::stringstream ssErase;
      for(int i = 0; i < int(inCh.length()); i++){
        if(inCh.c_str()[i] == ' '){
          if(i > 0 && inCh.c_str()[i-1] != ' '){
            ssErase << inCh.c_str()[i];
          }
        }
        else{
          ssErase << inCh.c_str()[i];
        }
      }
      inCh = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inCh.length()); i++){
        if(intFound && !brokenList2 && (inCh.c_str()[i] == ' '  || inCh.c_str()[i] == ')'  || inCh.c_str()[i] == '.'
          || inCh.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inCh.c_str()[i] == '0' || inCh.c_str()[i] == '1' || inCh.c_str()[i] == '2' ||
          inCh.c_str()[i] == '3' || inCh.c_str()[i] == '4' || inCh.c_str()[i] == '5' ||
          inCh.c_str()[i] == '6' || inCh.c_str()[i] == '7' || inCh.c_str()[i] == '8' ||
          inCh.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inCh.c_str()[i];
        }
        else if(intFound && !brokenList && inCh.c_str()[i] != ' '  && inCh.c_str()[i] != ')'  && inCh.c_str()[i] != '.'
          && inCh.c_str()[i] != '}'){
            find2 = i;
            brokenList = 1;
            brokenList2 = 1;
        }
      }
      if(find2 == 0 && find < 2){
        find2 = inCh.length();
      }
      if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
        newString = inCh.substr(find2,inCh.length() - find2);
      }
      else{
        newString = inCh;
      }
      if(int(newString.length()) > 0){
        excludedDates.push_back(newString);
      }
    }
  }
  else{
  }
  infile.close();
  remove("tempDates.txt");

  // read in the excludedDrivers.txt file
  inPath = DODmpOptions + "\\excludedDrivers.txt";
  infile.open(inPath);
  richTextBox6->Clear();
  richTextBox7->Clear();
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
	  std::stringstream sstemp;
      sstemp << inCh;
	  std::string excludedName;
	  std::string excludedMonth;
	  std::string excludedDay;
	  std::string excludedYear;
	  std::string excludedDate;
      sstemp >> excludedName;
      sstemp >> excludedMonth;
      sstemp >> excludedDay;
      sstemp >> excludedYear;
      int foundAlready = 0;
      while(excludedDates.size() < excludedNames.size()){
        excludedDates.push_back("2008");
      }
      for(int i = 0; i < int(excludedNames.size()); i++){
        if(unSignedCompare(excludedName,excludedNames[i])){
          //foundAlready++;
          //excludedDates[i] = excludedMonth + " " + excludedDay + " " + excludedYear;
        }
      }
      if(!foundAlready){
        excludedNames.push_back(excludedName);
        excludedDate = excludedMonth + " " + excludedDay + " " + excludedYear;
        excludedDates.push_back(excludedDate);
      }
    }
  }
  infile.close();
  while(excludedDates.size() < excludedNames.size()){
    excludedDates.push_back("2008");
  }
  for(int i = 0; i < int(excludedNames.size()); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << excludedNames[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << excludedNames[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << excludedNames[i];
    }
    excludedNames[i] = sstemp.str();
    if(i < int(excludedNames.size()) - 1){
      excludedNames[i] = excludedNames[i] + "\n";
    }
    str2 = gcnew System::String(excludedNames[i].c_str());
    richTextBox6->AppendText(str2);
	std::stringstream sstemp2;
    if(i < 9){
      sstemp2 << "{" << i + 1 << "}    " << excludedDates[i];
    }
    else if(i >= 9 && i < 99){
      sstemp2 << "{" << i + 1 << "}   " << excludedDates[i];
    }
    else if(i >= 99 && i < 999){
      sstemp2 << "{" << i + 1 << "}  " << excludedDates[i];
    }
    excludedDates[i] = sstemp2.str();
    if(i < int(excludedNames.size()) - 1){
      excludedDates[i] = excludedDates[i] + "\n";
    }
    str2 = gcnew System::String(excludedDates[i].c_str());
    richTextBox7->AppendText(str2);
  }

  // read in the XPED.txt file
  inPath = DODmpOptions + "\\XPED.txt";
  infile.open(inPath);
  richTextBox10->Clear();
  std::vector<std::string> XPED;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      XPED.push_back(inCh);
    }
  }
  infile.close();
  while(XPED.size() < excludedNames.size()){
    XPED.push_back("");
  }
  for(int i = 0; i < int(excludedNames.size()); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << XPED[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << XPED[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << XPED[i];
    }
    XPED[i] = sstemp.str();
    if(i < int(excludedNames.size()) - 1){
      XPED[i] = XPED[i] + "\n";
    }
    str2 = gcnew System::String(XPED[i].c_str());
    richTextBox10->AppendText(str2);
  }

  // read in the VistaED.txt file
  inPath = DODmpOptions + "\\VistaED.txt";
  infile.open(inPath);
  richTextBox11->Clear();
  std::vector<std::string> VistaED;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      VistaED.push_back(inCh);
    }
  }
  infile.close();
  while(VistaED.size() < excludedNames.size()){
    VistaED.push_back("");
  }
  for(int i = 0; i < int(excludedNames.size()); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << VistaED[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << VistaED[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << VistaED[i];
    }
    VistaED[i] = sstemp.str();
    if(i < int(excludedNames.size()) - 1){
      VistaED[i] = VistaED[i] + "\n";
    }
    str2 = gcnew System::String(VistaED[i].c_str());
    richTextBox11->AppendText(str2);
  }

  // read in the SevenED.txt file
  inPath = DODmpOptions + "\\SevenED.txt";
  infile.open(inPath);
  richTextBox12->Clear();
  std::vector<std::string> SevenED;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      SevenED.push_back(inCh);
    }
  }
  infile.close();
  while(SevenED.size() < excludedNames.size()){
    SevenED.push_back("");
  }
  for(int i = 0; i < int(excludedNames.size()); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << SevenED[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << SevenED[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << SevenED[i];
    }
    SevenED[i] = sstemp.str();
    if(i < int(excludedNames.size()) - 1){
      SevenED[i] = SevenED[i] + "\n";
    }
    str2 = gcnew System::String(SevenED[i].c_str());
    richTextBox12->AppendText(str2);
  }

  // read in the EightED.txt file
  inPath = DODmpOptions + "\\EightED.txt";
  infile.open(inPath);
  richTextBox13->Clear();
  std::vector<std::string> EightED;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      EightED.push_back(inCh);
    }
  }
  infile.close();
  while(EightED.size() < excludedNames.size()){
    EightED.push_back("");
  }
  for(int i = 0; i < int(excludedNames.size()); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << EightED[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << EightED[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << EightED[i];
    }
    EightED[i] = sstemp.str();
    if(i < int(excludedNames.size()) - 1){
      EightED[i] = EightED[i] + "\n";
    }
    str2 = gcnew System::String(EightED[i].c_str());
    richTextBox13->AppendText(str2);
  }

  s = richTextBox8->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading Problem Drivers list, Please Wait...";
  std::cout << "Loading Problem Drivers list, Please Wait..." << std::endl;
  outProg.close();

  // read in the problemDrivers.txt file
  inPath = DODmpOptions + "\\problemDrivers.txt";
  infile.open(inPath);
  richTextBox8->Clear();
  std::vector<std::string> problemNames;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
	  std::stringstream sstemp;
      sstemp << inCh;
	  std::string excludedName;
	  std::string excludedMonth;
	  std::string excludedDay;
	  std::string excludedYear;
	  std::string excludedDate;
      sstemp >> excludedName;
      sstemp >> excludedMonth;
      sstemp >> excludedDay;
      sstemp >> excludedYear;
      problemNames.push_back(excludedName);
    }
  }
  infile.close();
  for(int i = 0; i < int(problemNames.size()); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << problemNames[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << problemNames[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << problemNames[i];
    }
    problemNames[i] = sstemp.str();
    if(i < int(problemNames.size()) - 1){
      problemNames[i] = problemNames[i] + "\n";
    }
    str2 = gcnew System::String(problemNames[i].c_str());
    richTextBox8->AppendText(str2);
  }

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading User kd Commands list, Please Wait...";
  std::cout << "Loading User kd Commands list, Please Wait..." << std::endl;
  outProg.close();

  // read in the kdCommands.txt file
  inPath = DODmpOptions + "\\kdCommands.txt";
  infile.open(inPath);
  richTextBox9->Clear();
  std::vector<std::string> kdCommands;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      kdCommands.push_back(inCh);
    }
  }
  infile.close();
  for(int i = 0; i < int(kdCommands.size()); i++){
    std::stringstream sstemp;
    if(i < 9){
      sstemp << "{" << i + 1 << "}    " << kdCommands[i];
    }
    else if(i >= 9 && i < 99){
      sstemp << "{" << i + 1 << "}   " << kdCommands[i];
    }
    else if(i >= 99 && i < 999){
      sstemp << "{" << i + 1 << "}  " << kdCommands[i];
    }
    kdCommands[i] = sstemp.str();
    if(i < int(kdCommands.size()) - 1){
      kdCommands[i] = kdCommands[i] + "\n";
    }
    str2 = gcnew System::String(kdCommands[i].c_str());
    richTextBox9->AppendText(str2);
  }

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading code box formatting settings, Please Wait...";
  std::cout << "Loading code box formatting settings, Please Wait..." << std::endl;
  outProg.close();

  // read in the $_code_box_begin file
  inPath = DOParms + "$_code_box_begin";
  infile.open(inPath);
  if(infile.good()){
    getline(infile,inChar);
    infile.close();
	std::string codeBoxBegin = inChar;
    str2 = gcnew System::String(codeBoxBegin.c_str());
  }
  else{
    infile.close();
	std::string codeBoxBegin = "[CODE]";
    str2 = gcnew System::String(codeBoxBegin.c_str());
  }
  textBox12->Text = str2;

  // read in the $_code_box_begin file
  inPath = DOParms + "$_code_box_end";
  infile.open(inPath);
  if(infile.good()){
    getline(infile,inChar);
    infile.close();
	std::string codeBoxEnd = inChar;
    str2 = gcnew System::String(codeBoxEnd.c_str());
  }
  else{
    infile.close();
	std::string codeBoxEnd = "[/CODE]";
    str2 = gcnew System::String(codeBoxEnd.c_str());
  }
  textBox13->Text = str2;

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Loading OS Old Driver After dates, Please Wait...";
  std::cout << "Loading OS Old Driver After dates, Please Wait..." << std::endl;
  outProg.close();

  // read in the XPoldDriverAfter file
  inPath = DODmpOptions + "\\XPoldDriverAfter.txt";
  infile.open(inPath);
  System::String^ str3;
  System::String^ str4;
  int oldDriverAfterCount = 0;
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
		std::string oldDriverAfter = inChar;
        str4 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str3 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str2 = gcnew System::String(oldDriverAfter.c_str());
        oldDriverAfterCount++;
      }
    }
    infile.close();
  }
  else{
    std::string oldDriverAfter = "2004";
    str2 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "25";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Aug";
    str4 = gcnew System::String(oldDriverAfter.c_str());
    infile.close();
  }
  if(str2 == str3 && str2 == str4){
    std::string oldDriverAfter = "31";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Dec";
    str4 = gcnew System::String(oldDriverAfter.c_str());
  }
  if(str2 == str3){
    std::string oldDriverAfter = "1";
    str3 = gcnew System::String(oldDriverAfter.c_str());
  }
  textBox18->Text = str4;
  textBox19->Text = str3;
  textBox20->Text = str2;

  // read in the VistaoldDriverAfter file
  inPath = DODmpOptions + "\\VistaoldDriverAfter.txt";
  infile.open(inPath);
  oldDriverAfterCount = 0;
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
        std::string oldDriverAfter = inChar;
        str4 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str3 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str2 = gcnew System::String(oldDriverAfter.c_str());
        oldDriverAfterCount++;
      }
    }
    infile.close();
  }
  else{
    std::string oldDriverAfter = "2009";
    str2 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "28";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Apr";
    str4 = gcnew System::String(oldDriverAfter.c_str());
    infile.close();
  }
  if(str2 == str3 && str2 == str4){
    std::string oldDriverAfter = "31";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Dec";
    str4 = gcnew System::String(oldDriverAfter.c_str());
  }
  if(str2 == str3){
    std::string oldDriverAfter = "1";
    str3 = gcnew System::String(oldDriverAfter.c_str());
  }
  textBox21->Text = str4;
  textBox22->Text = str3;
  textBox23->Text = str2;

  // read in the oldDriverAfter file
  inPath = DODmpOptions + "\\7oldDriverAfter.txt";
  infile.open(inPath);
  oldDriverAfterCount = 0;
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
		std::string oldDriverAfter = inChar;
        str4 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str3 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str2 = gcnew System::String(oldDriverAfter.c_str());
        oldDriverAfterCount++;
      }
    }
    infile.close();
  }
  else{
    infile.close();
    inPath = DODmpOptions + "\\oldDriverAfter.txt";
    infile.open(inPath);
    oldDriverAfterCount = 0;
    if(infile.good()){
      while(!infile.eof()){
        getline(infile,inChar);
        size_t found;
        found = inChar.find("#");
        if(found != std::string::npos){
        }
        else{
          std::stringstream sstemp;
          sstemp << inChar;
          sstemp >> inChar;
          std::string oldDriverAfter = inChar;
          str4 = gcnew System::String(oldDriverAfter.c_str());
          sstemp >> inChar;
          oldDriverAfter = inChar;
          str3 = gcnew System::String(oldDriverAfter.c_str());
          sstemp >> inChar;
          oldDriverAfter = inChar;
          str2 = gcnew System::String(oldDriverAfter.c_str());
          oldDriverAfterCount++;
        }
      }
      infile.close();
    }
    else{
      std::string oldDriverAfter = "2009";
      str2 = gcnew System::String(oldDriverAfter.c_str());
      oldDriverAfter = "12";
      str3 = gcnew System::String(oldDriverAfter.c_str());
      oldDriverAfter = "Jul";
      str4 = gcnew System::String(oldDriverAfter.c_str());
      infile.close();
    }
  }
  if(str2 == str3 && str2 == str4){
    std::string oldDriverAfter = "31";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Dec";
    str4 = gcnew System::String(oldDriverAfter.c_str());
  }
  if(str2 == str3){
    std::string oldDriverAfter = "1";
    str3 = gcnew System::String(oldDriverAfter.c_str());
  }
  textBox9->Text = str4;
  textBox8->Text = str3;
  textBox14->Text = str2;

  // read in the 8oldDriverAfter file
  inPath = DODmpOptions + "\\8oldDriverAfter.txt";
  infile.open(inPath);
  oldDriverAfterCount = 0;
  if(infile.good()){
    while(!infile.eof()){
      getline(infile,inChar);
      size_t found;
      found = inChar.find("#");
      if(found != std::string::npos){
      }
      else{
        std::stringstream sstemp;
        sstemp << inChar;
        sstemp >> inChar;
        std::string oldDriverAfter = inChar;
        str4 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str3 = gcnew System::String(oldDriverAfter.c_str());
        sstemp >> inChar;
        oldDriverAfter = inChar;
        str2 = gcnew System::String(oldDriverAfter.c_str());
        oldDriverAfterCount++;
      }
    }
    infile.close();
  }
  else{
    std::string oldDriverAfter = "2012";
    str2 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "1";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Jan";
    str4 = gcnew System::String(oldDriverAfter.c_str());
    infile.close();
  }
  if(str2 == str3 && str2 == str4){
    std::string oldDriverAfter = "31";
    str3 = gcnew System::String(oldDriverAfter.c_str());
    oldDriverAfter = "Dec";
    str4 = gcnew System::String(oldDriverAfter.c_str());
  }
  if(str2 == str3){
    std::string oldDriverAfter = "1";
    str3 = gcnew System::String(oldDriverAfter.c_str());
  }
  textBox15->Text = str4;
  textBox16->Text = str3;
  textBox17->Text = str2;

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving backup of current settings, Please Wait...";
  std::cout << "Saving backup of current settings, Please Wait..." << std::endl;
  outProg.close();

  std::ofstream openingFile(tempDir + "\\openingFile.txt");
  openingFile << 0;
  openingFile.close();
  saveAndRunTheApps2(sender, e);
  std::string mkDirBackup;

  System::String^ userProfilePath = gcnew System::String(userProfile.c_str());
  FileSystemUtil::createDirectoryWithBackup(userProfilePath + "\\SysnativeBSODApps", "backup");
  std::vector<std::string> fileListing;
  std::string backupDir = userProfile + "\\SysnativeBSODApps\\backup";
  fileListing = FileSystemUtil::getFiles(backupDir);
  int i45 = 0;
  int maxi45 = 0;
  std::ofstream openSaveAs(tempDir + "\\saveAs.txt");
  openSaveAs << 0;
  openSaveAs.close();
  size_t i1 = 0;
  while (i1 < fileListing.size()){
    fileListing[i1] = FileSystemUtil::getFileName(fileListing[i1]);
    i1++;
  }
  i1 = 0;

  while(i1 < fileListing.size() + 1){
    std::string thisLine;
    if(i1 < fileListing.size())
    {
      thisLine = fileListing[i1];
    }
    i1++;
    for(int i46 = 0; i46 < 100; i46++){
      std::stringstream ssBackup;
      ssBackup << i46 << ".zdn";
      if(thisLine == ssBackup.str()){
        i45 = i46;
        break;
      }
    }
    std::stringstream ssBackup;
    ssBackup << i45 << ".zdn";
    if(thisLine == ssBackup.str()){
      i45++;
      if(i45 > maxi45){
        maxi45 = i45;
      }
      else{
        i45 = maxi45;
      }
    }
    else{
      thisLine = backupDir + "\\" + ssBackup.str();
      saveBackup(thisLine);
      break;
    }
    if(i45 == 100){
      for(int j = 0; j < 90; j++){
        std::stringstream ssDelete;
        ssDelete << j;
        thisLine = backupDir + "\\" + ssDelete.str() + ".zdn";
        remove(thisLine.c_str());
      }
      thisLine = backupDir + "\\" + "10" + ".zdn";
      saveBackup(thisLine);
      for(int j = 0; j < 10; j++){
        std::string copy1;
        std::string copy2;
		std::stringstream ssDelete, ssDelete2;
        ssDelete << j;
        ssDelete2 << j + 90;
        copy1 = backupDir + "\\" + ssDelete2.str() + ".zdn";
        copy2 = backupDir + "\\" + ssDelete.str() + ".zdn";
        System::IO::File::Move(gcnew System::String(copy1.c_str()), gcnew System::String(copy2.c_str()));
      }
      break;
    }
  }
  tabControl1->Enabled = true;
  button9->Visible = true;
  tabControl1->Visible = true;
  std::ofstream outGUI(tempDir + "\\outGUI.txt");
  outGUI << "1";
  outGUI.close();
  checkList1Change(sender, e);
  checkList2Change(sender, e);
  checkList3Change(sender, e);
}

// Quick Save
System::Void Form1::saveAndRunTheApps(System::Object^ sender, System::EventArgs^ e)
{
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  Hide();
  std::ofstream openSaveAs(tempDir + "\\saveAs2.txt");
  openSaveAs << 0;
  openSaveAs.close();
  std::ofstream openingFile(tempDir + "\\openingFile.txt");
  openingFile << 0;
  openingFile.close();
  std::ofstream openingFile2(tempDir + "\\openingFile2.txt");
  openingFile2 << 0;
  openingFile2.close();
  std::ofstream outPercentDone;
  outPercentDone.open(tempDir + "\\percentDone.txt");
  outPercentDone << 452312;
  outPercentDone.close();

  Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
  newThread->Start();

  saveAndRunTheApps2(sender,e);
  std::string tempPath;
  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  outPercentDone << 100;
  outPercentDone.close();
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  remove(std::string(userProfile + "\\SysnativeBSODApps\\backup\\tempFileName.txt").c_str());
  Show();
}

//Saves settings to file
System::Void Form1::saveAndRunTheApps2(System::Object^  sender, System::EventArgs^  e)
{
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::ofstream outPercentDone(tempDir + "\\percentDone.txt");
  outPercentDone << 452312;
  outPercentDone.close();
  std::ofstream outProg;

  if(!tabControl1->Enabled){
    return;
  }

  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string DODmpOptions = userProfile + "\\SysnativeBSODApps\\dmpOptions";
  std::string DOParms = userProfile + "\\SysnativeBSODApps\\parms\\";
  std::string temp = DODmpOptions + "\\outputOptions.txt";
  std::ofstream outfile;
  outfile.open(temp);

  std::string tempPath;
  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving Output Options, Please Wait...";
  std::cout << "Saving Output Options, Please Wait..." << std::endl;
  outProg.close();
  if(checkedListBox1->GetItemChecked(0)){
    outfile << "outputDmps" << std::endl;
  }
  else{
    outfile << "# outputDmps" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(1)){
    outfile << "outputFullDrivers" << std::endl;
  }
  else{
    outfile << "# outputFullDrivers" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(2)){
    outfile << "outputThirdPartyDriversName" << std::endl;
  }
  else{
    outfile << "# outputThirdPartyDriversName" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(3)){
    outfile << "outputThirdPartyDriversDate" << std::endl;
  }
  else{
    outfile << "# outputThirdPartyDriversDate" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(4)){
    outfile << "outputImportantInfo" << std::endl;
  }
  else{
    outfile << "# outputImportantInfo" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(5)){
    outfile << "outputStack" << std::endl;
  }
  else{
    outfile << "# outputStack" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(6)){
    outfile << "outputMissingFromDRT" << std::endl;
  }
  else{
    outfile << "# outputMissingFromDRT" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(7)){
    outfile << "outputUnloadedDrivers" << std::endl;
  }
  else{
    outfile << "# outputUnloadedDrivers" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(8)){
    outfile << "outputSMBIOS" << std::endl;
  }
  else{
    outfile << "# outputSMBIOS" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(9)){
    outfile << "outputTemplate" << std::endl;
  }
  else{
    outfile << "# outputTemplate" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(10)){
    outfile << "outputNinetyNine" << std::endl;
  }
  else{
    outfile << "# outputNinetyNine" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(11)){
    outfile << "outputNinetyEight" << std::endl;
  }
  else{
    outfile << "# outputNinetyEight" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(12)){
    outfile << "outputNinetySeven" << std::endl;
  }
  else{
    outfile << "# outputNinetySeven" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(13)){
    outfile << "outputNinetyFive" << std::endl;
  }
  else{
    outfile << "# outputNinetyFive" << std::endl;
  }
  if(checkedListBox1->GetItemChecked(14)){
    outfile << "outputEightyEight" << std::endl;
  }
  else{
    outfile << "# outputEightyEight" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(0)){
    outfile << "loadingDumpFile" << std::endl;
  }
  else{
    outfile << "# loadingDumpFile" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(1)){
    outfile << "kernelVersion" << std::endl;
  }
  else{
    outfile << "# kernelVersion" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(2)){
    outfile << "missingServicePack" << std::endl;
  }
  else{
    outfile << "# missingServicePack" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(3)){
    outfile << "builtBy" << std::endl;
  }
  else{
    outfile << "# builtBy" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(4)){
    outfile << "debugSessionTime" << std::endl;
  }
  else{
    outfile << "# debugSessionTime" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(5)){
    outfile << "systemUptime" << std::endl;
  }
  else{
    outfile << "# systemUptime" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(6)){
    outfile << "bugCheck" << std::endl;
  }
  else{
    outfile << "# bugCheck" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(7)){
    outfile << "unableToVerify" << std::endl;
  }
  else{
    outfile << "# unableToVerify" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(8)){
    outfile << "symbolsNotLoaded" << std::endl;
  }
  else{
    outfile << "# symbolsNotLoaded" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(9)){
    outfile << "probablyCausedBy" << std::endl;
  }
  else{
    outfile << "# probablyCausedBy" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(10)){
    outfile << "defaultBucketID" << std::endl;
  }
  else{
    outfile << "# defaultBucketID" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(11)){
    outfile << "verifier" << std::endl;
  }
  else{
    outfile << "# verifier" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(12)){
    outfile << "bugcheckStr" << std::endl;
  }
  else{
    outfile << "# bugcheckStr" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(13)){
    outfile << "bugCheckAnalysis" << std::endl;
  }
  else{
    outfile << "# bugCheckAnalysis" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(14)){
    outfile << "processName" << std::endl;
  }
  else{
    outfile << "# processName" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(15)){
    outfile << "failureBucketID" << std::endl;
  }
  else{
    outfile << "# failureBucketID" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(16)){
    outfile << "bugcheckCode" << std::endl;
  }
  else{
    outfile << "# bugcheckCode" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(17)){
    outfile << "errorCode" << std::endl;
  }
  else{
    outfile << "# errorCode" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(18)){
    outfile << "diskHardwareError" << std::endl;
  }
  else{
    outfile << "# diskHardwareError" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(19)){
    outfile << "arguments" << std::endl;
  }
  else{
    outfile << "# arguments" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(20)){
    outfile << "CPUID" << std::endl;
  }
  else{
    outfile << "# CPUID" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(21)){
    outfile << "maxSpeed" << std::endl;
  }
  else{
    outfile << "# maxSpeed" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(22)){
    outfile << "currentSpeed" << std::endl;
  }
  else{
    outfile << "# currentSpeed" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(23)){
    outfile << "BIOSVersion" << std::endl;
  }
  else{
    outfile << "# BIOSVersion" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(24)){
    outfile << "BIOSReleaseDate" << std::endl;
  }
  else{
    outfile << "# BIOSReleaseDate" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(25)){
    outfile << "systemManufacturer" << std::endl;
  }
  else{
    outfile << "# systemManufacturer" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(26)){
    outfile << "systemProductName" << std::endl;
  }
  else{
    outfile << "# systemProductName" << std::endl;
  }
  if(checkedListBox2->GetItemChecked(27)){
    outfile << "overclock" << std::endl;
  }
  else{
    outfile << "# overclock" << std::endl;
  }
  outfile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving Formatting and Miscellaneous Options, Please Wait...";
  std::cout << "Saving Formatting and Miscellaneous Options, Please Wait..." << std::endl;
  outProg.close();

  temp = DODmpOptions + "\\fullGUI.txt";
  outfile.open(temp);
  if(checkBox10->Checked){
    outfile << "1";
  }
  else{
    outfile << "1";
  }
  outfile.close();

  outfile.open(tempDir + "\\fullGUI.txt");
  outfile << checkBox10->Checked;
  outfile.close();

  remove(std::string(tempDir + "\\newDmpsOnly.txt").c_str());

  temp = tempDir + "\\newDmpsOnly.txt";
  outfile.open(temp);
  if(checkBox24->Checked){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  temp = DODmpOptions + "\\addSpaces.txt";
  outfile.open(temp);
  if(checkedListBox3->GetItemChecked(0)){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  temp = DODmpOptions + "\\noBBCode.txt";
  outfile.open(temp);
  if(checkedListBox3->GetItemChecked(1)){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  temp = DODmpOptions + "\\htmlBBCode.txt";
  outfile.open(temp);
  if(checkedListBox3->GetItemChecked(2)){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  temp = DODmpOptions + "\\turnOffBBCode.txt";
  outfile.open(temp);
  if(checkedListBox3->GetItemChecked(3)){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  temp = DODmpOptions + "\\noBBCodeCodeBoxes.txt";
  outfile.open(temp);
  if(checkedListBox3->GetItemChecked(4)){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  temp = DODmpOptions + "\\oldDriversRed.txt";
  outfile.open(temp);
  if(checkedListBox3->GetItemChecked(5)){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  temp = DODmpOptions + "\\allFilesDmps.txt";
  outfile.open(temp);
  if(checkedListBox3->GetItemChecked(6)){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();


  using namespace System::Runtime::InteropServices;

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving symbols paths, Please Wait...";
  std::cout << "Saving symbols paths, Please Wait..." << std::endl;
  outProg.close();

  std::string options = DODmpOptions + "\\symbols.txt";
  std::ifstream infileOptions(options);
  std::string inputCh;
  std::string symbolsStringStore = "";
  std::string symbolsString2Store = "";
  int symbolsIntStore;
  // Determine whether the symbols.txt file exists
  if(infileOptions.good()){
    // If it exists, obtain the symbols path
    size_t foundPound;
    // Get only the symbols path and ignore any commented lines
    int countOptions = 0;
    while(!infileOptions.eof()){
      getline(infileOptions,inputCh);
      foundPound = inputCh.find("#");
      while(foundPound != std::string::npos){
        getline(infileOptions,inputCh);
        foundPound = inputCh.find("#");
      }
      countOptions++;
      if (countOptions == 1){
        symbolsStringStore = inputCh;
      }
      if (countOptions == 2){
        if(inputCh == "1" || inputCh == "2"){
          std::stringstream sstemp;
          sstemp << inputCh;
          sstemp >> symbolsIntStore;
        }
        else{
          symbolsString2Store = inputCh;
        }
      }
      if (countOptions == 3){
        if(inputCh == "1" || inputCh == "2"){
          std::stringstream sstemp;
          sstemp << inputCh;
          sstemp >> symbolsIntStore;
        }
      }
    }
    infileOptions.close();
  }

  temp = DODmpOptions + "\\symbols.txt";
  outfile.open(temp);
  int symbolsInt;

  System::String ^ s = textBox3->Text;
  const char* chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile << os << std::endl;

  s = textBox10->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile << os << std::endl;

  if(checkBox3->Checked){
    symbolsInt = 2;
  }
  else if(checkBox4->Checked){
    symbolsInt = 1;
  }
  else{
    symbolsInt = symbolsIntStore;
  }
  outfile << symbolsInt << std::endl;
  if(checkBox1->Checked){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving User kd Commands settings, Please Wait...";
  std::cout << "Saving User kd Commands settings, Please Wait..." << std::endl;
  outProg.close();

  temp = DODmpOptions + "\\onlyUser.txt";
  outfile.open(temp);
  if(checkBox8->Checked){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  temp = DODmpOptions + "\\userFirst.txt";
  outfile.open(temp);
  if(checkBox9->Checked){
    outfile << "1";
  }
  else{
    outfile << "0";
  }
  outfile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving Driver Reference Table user input, Please Wait...";
  std::cout << "Saving Driver Reference Table user input, Please Wait..." << std::endl;
  outProg.close();

  // write the $un95 file
  std::string inPath = DOParms + "un95";
  outfile.open(inPath);

  s = textBox1->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile << os;
  outfile.close();

  // write the $un95 file
  inPath = tempDir + "\\un95";
  outfile.open(inPath);

  s = textBox1->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile << os;
  outfile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving _88-debug.txt and _98-debug.txt std::strings, Please Wait...";
  std::cout << "Saving _88-debug.txt and _98-debug.txt std::strings, Please Wait..." << std::endl;
  outProg.close();

  // Read lines from the dmpOptions driverInOut.txt file
  std::string driverInSite;
  std::string driverOutSite;
  options = DODmpOptions + "\\driverInOut.txt";
  outfile.open(options);

  s = textBox6->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  driverInSite = os;

  s = textBox5->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  driverOutSite = os;
  outfile << driverInSite << std::endl << driverOutSite;
  outfile.close();

  richTextBox1->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\$_header1", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  std::string copy1, copy2;
  copy1 = tempDir + "\\$_header1";
  copy2 = DOParms + "$_header1";
  FileSystemUtil::copyFile(copy1, copy2, true);

  richTextBox2->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\$_footer1", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  copy1 = tempDir + "\\$_footer1";
  copy2 = DOParms + "$_footer1";
  FileSystemUtil::copyFile(copy1, copy2, true);

  richTextBox3->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\$_sig1", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  copy1 = tempDir + "\\$_sig1";
  copy2 = DOParms + "$_sig1";
  FileSystemUtil::copyFile(copy1, copy2, true);

  richTextBox4->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\$_driver_update_header1", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  copy1 = tempDir + "\\$_driver_update_header1";
  copy2 = DOParms + "$_driver_update_header1";
  FileSystemUtil::copyFile(copy1, copy2, true);

  richTextBox5->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\template.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  copy1 = tempDir + "\\template.txt";
  copy2 = DODmpOptions + "\\template.txt";
  FileSystemUtil::copyFile(copy1, copy2, true);

  richTextBox6->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\excludedNames.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);
  richTextBox7->SaveFile(gcnew System::String(tempDir.c_str()) + L"\\excludedDates.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  std::ifstream excludedNamesFile(tempDir + "\\excludedNames.txt");
  std::ifstream excludedDatesFile(tempDir + "\\excludedDates.txt");

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving Excluded Drivers list, Please Wait...";
  std::cout << "Saving Excluded Drivers list, Please Wait..." << std::endl;
  outProg.close();

  std::string excludedNamesFilePath;
  excludedNamesFilePath = DODmpOptions + "\\excludedDrivers.txt";
  outfile.open(excludedNamesFilePath);

  std::vector<std::string> excludedNames;
  std::vector<std::string> excludedDates;
  int num = 0;
  while(!excludedNamesFile.eof() && excludedNamesFile.good()){
    std::string inString;
    std::string newString;
    getline(excludedNamesFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) > 0){
      excludedNames.push_back(newString);
    }
    getline(excludedDatesFile,inString);
    find = 0;
    find2 = 0;
    find3 = 0;
    intFound = 0;
    brokenList = 0;
    std::stringstream ssnum3;
    std::stringstream ssErase2;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase2 << inString.c_str()[i];
        }
      }
      else{
        ssErase2 << inString.c_str()[i];
      }
    }
    inString = ssErase2.str();
    brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum3 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum3.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) > 0){
      excludedDates.push_back(newString);
    }
  }

  while(excludedDates.size() < excludedNames.size()){
    excludedDates.push_back("2008");
  }
  for(int i = 0; i < int(excludedNames.size()); i++){
    outfile << excludedNames[i] << " " << excludedDates[i];
    if(i < int(excludedNames.size()) - 1){
      outfile << std::endl;
    }
  }
  outfile.close();

  richTextBox8->SaveFile(gcnew System::String(tempDir.c_str()) + "\\problemNames.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  std::ifstream problemNamesFile(tempDir + "\\problemNames.txt");

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving Problem Drivers list, Please Wait...";
  std::cout << "Saving Problem Drivers list, Please Wait..." << std::endl;
  outProg.close();

  std::string problemNamesFilePath;
  problemNamesFilePath = DODmpOptions + "\\problemDrivers.txt";
  outfile.open(problemNamesFilePath);

  std::vector<std::string> problemNames;
  num = 0;
  while(!problemNamesFile.eof() && problemNamesFile.good()){
    std::string inString;
    std::string newString;
    getline(problemNamesFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) > 0){
      problemNames.push_back(newString);
    }
  }

  for(int i = 0; i < int(problemNames.size()); i++){
    outfile << problemNames[i];
    if(i < int(problemNames.size()) - 1){
      outfile << std::endl;
    }
  }
  outfile.close();

  richTextBox9->SaveFile(gcnew System::String(tempDir.c_str()) + "\\kdCommands.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  std::ifstream kdCommandsFile(tempDir + "\\kdCommands.txt");

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "User kd Commands list, Please Wait...";
  std::cout << "User kd Commands list, Please Wait..." << std::endl;
  outProg.close();

  std::string kdCommandsFilePath;
  kdCommandsFilePath = DODmpOptions + "\\kdCommands.txt";
  outfile.open(kdCommandsFilePath);

  std::vector<std::string> kdCommands;
  num = 0;
  while(!kdCommandsFile.eof() && kdCommandsFile.good()){
    std::string inString;
    std::string newString;
    getline(kdCommandsFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) > 0){
      kdCommands.push_back(newString);
    }
  }

  for(int i = 0; i < int(kdCommands.size()); i++){
    outfile << kdCommands[i];
    if(i < int(kdCommands.size()) - 1){
      outfile << std::endl;
    }
  }
  outfile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving OS Excluded Drivers list, Please Wait...";
  std::cout << "Saving OS Excluded Drivers list, Please Wait..." << std::endl;
  outProg.close();

  richTextBox10->SaveFile(gcnew System::String(tempDir.c_str()) + "\\XPED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  std::ifstream XPEDFile(tempDir + "\\XPED.txt");

  std::string XPEDFilePath;
  XPEDFilePath = DODmpOptions + "\\XPED.txt";
  outfile.open(XPEDFilePath);

  std::vector<std::string> XPED;
  num = 0;
  while(!XPEDFile.eof() && XPEDFile.good()){
    std::string inString;
    std::string newString;
    getline(XPEDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      XPED.push_back(newString);
    }
  }

  for(int i = 0; i < int(XPED.size()); i++){
    outfile << XPED[i];
    if(i < int(XPED.size()) - 1){
      outfile << std::endl;
    }
  }
  outfile.close();
  XPEDFile.close();

  richTextBox11->SaveFile(gcnew System::String(tempDir.c_str()) + "\\VistaED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  std::ifstream VistaEDFile(tempDir + "\\VistaED.txt");

  std::string VistaEDFilePath;
  VistaEDFilePath = DODmpOptions + "\\VistaED.txt";
  outfile.open(VistaEDFilePath);

  std::vector<std::string> VistaED;
  num = 0;
  while(!VistaEDFile.eof() && VistaEDFile.good()){
    std::string inString;
    std::string newString;
    getline(VistaEDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      VistaED.push_back(newString);
    }
  }

  for(int i = 0; i < int(VistaED.size()); i++){
    outfile << VistaED[i];
    if(i < int(VistaED.size()) - 1){
      outfile << std::endl;
    }
  }
  outfile.close();
  VistaEDFile.close();

  richTextBox12->SaveFile(gcnew System::String(tempDir.c_str()) + "\\SevenED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  std::ifstream SevenEDFile(tempDir + "\\SevenED.txt");

  std::string SevenEDFilePath;
  SevenEDFilePath = DODmpOptions + "\\SevenED.txt";
  outfile.open(SevenEDFilePath);

  std::vector<std::string> SevenED;
  num = 0;
  while(!SevenEDFile.eof() && SevenEDFile.good()){
    std::string inString;
    std::string newString;
    getline(SevenEDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      SevenED.push_back(newString);
    }
  }

  for(int i = 0; i < int(SevenED.size()); i++){
    outfile << SevenED[i];
    if(i < int(SevenED.size()) - 1){
      outfile << std::endl;
    }
  }
  outfile.close();
  SevenEDFile.close();

  richTextBox13->SaveFile(gcnew System::String(tempDir.c_str()) + "\\EightED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  std::ifstream EightEDFile(tempDir + "\\EightED.txt");

  std::string EightEDFilePath;
  EightEDFilePath = DODmpOptions + "\\EightED.txt";
  outfile.open(EightEDFilePath);

  std::vector<std::string> EightED;
  num = 0;
  while(!EightEDFile.eof() && EightEDFile.good()){
    std::string inString;
    std::string newString;
    getline(EightEDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      EightED.push_back(newString);
    }
  }

  for(int i = 0; i < int(EightED.size()); i++){
    outfile << EightED[i];
    if(i < int(EightED.size()) - 1){
      outfile << std::endl;
    }
  }
  outfile.close();
  EightEDFile.close();

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving code box formatting, Please Wait...";
  std::cout << "Saving code box formatting, Please Wait..." << std::endl;
  outProg.close();

  s = textBox12->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile.open(tempDir + "\\$_code_box_begin");

  outfile << os;
  outfile.close();
  copy1 = tempDir + "\\$_code_box_begin";
  copy2 = DOParms + "$_code_box_begin";
  FileSystemUtil::copyFile(copy1, copy2, true);

  s = textBox13->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile.open(tempDir + "\\$_code_box_end");

  outfile << os;
  outfile.close();
  copy1 = tempDir + "\\$_code_box_end";
  copy2 = DOParms + "$_code_box_end";
  FileSystemUtil::copyFile(copy1, copy2, true);
  /*Form2 ^form2 = gcnew Form2();
  form2->ShowDialog();*/

  s = textBox20->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  s = textBox19->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os2 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  s = textBox18->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os3 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  tempPath = tempDir + "\\analyzedDmpOf.txt";
  outProg.open(tempPath);
  outProg << "Saving OS Old Driver After info, Please Wait...";
  std::cout << "Saving OS Old Driver After info, Please Wait..." << std::endl;
  outProg.close();

  options = DODmpOptions + "\\XPoldDriverAfter.txt";
  outfile.open(options);
  outfile << os3 << " " << os2 << " " << os;
  outfile.close();

  s = textBox23->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  s = textBox22->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os2 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  s = textBox21->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os3 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  options = DODmpOptions + "\\VistaoldDriverAfter.txt";
  outfile.open(options);
  outfile << os3 << " " << os2 << " " << os;
  outfile.close();

  s = textBox14->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  s = textBox8->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os2 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  s = textBox9->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os3 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  options = DODmpOptions + "\\7oldDriverAfter.txt";
  outfile.open(options);
  outfile << os3 << " " << os2 << " " << os;
  outfile.close();

  s = textBox17->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  s = textBox16->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os2 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  s = textBox15->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os3 = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  options = DODmpOptions + "\\8oldDriverAfter.txt";
  outfile.open(options);
  outfile << os3 << " " << os2 << " " << os;
  outfile.close();

  s = textBox4->Text;
  chars =
  (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
  os = chars;
  Marshal::FreeHGlobal(System::IntPtr((void*)chars));

  outfile.open(tempDir + "\\originatingPost.txt");

  outfile << os;
  outfile.close();


  std::stringstream ssforumString;
  std::string inChar = os;
  std::ifstream openingFile(tempDir + "\\openingFile.txt");
  std::ifstream openingFile2(tempDir + "\\openingFile2.txt");

  //If the user is not opening a file
  if(!openingFile.good()){

    tempPath = tempDir + "\\analyzedDmpOf.txt";
    outProg.open(tempPath);
    outProg << "Loading forum settings from Originating Post input, Please Wait...";
    std::cout << "Loading forum settings from Originating Post input, Please Wait..." << std::endl;
    outProg.close();
    //Get the forum the post came from to generate forum settings
    for(int i77 = 0; i77 < int(inChar.length()); i77++){
      if(inChar.c_str()[i77] != '/' && inChar.c_str()[i77] != '.' && inChar.c_str()[i77] != ':'){
        ssforumString << inChar.c_str()[i77];
      }
    }
    std::string forumString = ssforumString.str();
    std::cout << forumString << std::endl;
    if(forumString.find("answersmicrosoft") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\answersmicrosoft.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("socialmicrosoftcomForums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\socialmicrosoftcomForums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("technetmicrosoft") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\technetmicrosoft.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("sysnative") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\sysnative.zdn";
      std::cout << forumFileName << std::endl;
      openForumFile(forumFileName);
    }
    if(forumString.find("forumstechguy") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumstechguy.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("geekstogo") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\geekstogo.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("howtogeek") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\howtogeek.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("majorgeeks") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\majorgeeks.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("forumscnetcom") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumscnetcom.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windowsforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowsforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("forumscnetcom") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumscnetcom.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("forumswindowsforum") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumswindowsforum.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("forummintywhite") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forummintywhite.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("winsource") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\winsource.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windowssecretscomforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowssecretscomforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windowsitpro") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowsitpro.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windows7forums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows7forums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("w7forums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\w7forums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("sevenforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\sevenforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("eightforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\eightforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("windows8forums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows8forums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("win8forums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\win8forums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("techsupportforum") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\techsupportforum.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("bleepingcomputer") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\bleepingcomputer.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.find("tenforums") != std::string::npos){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\tenforums.zdn";
      openForumFile(forumFileName);
    }
    if(forumString.length() == 0){
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\blankSaveAndRun.zdn";
      saveBackup(forumFileName);
    }
  }

  if(openingFile2.good()){
    std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\blank.zdn";
    saveBackup(forumFileName);
  }
  openingFile.close();
  openingFile2.close();
  remove(std::string(tempDir + "\\openingFile.txt").c_str());
  remove(std::string(tempDir + "\\openingFile2.txt").c_str());
  tempDir = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP")) + "\\SysnativeBSODApps";
  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  outPercentDone << 95;
  outPercentDone.close();
  Sleep(1395);
}

// Local symbols option
System::Void Form1::checkBox1Change(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox1->Checked){
    checkBox2->Checked = false;
  }

  std::string userProfile = ConfigurationUtil::getUserProfilePath();

  std::string DODmpOptions = userProfile + "\\SysnativeBSODApps\\dmpOptions\\";
  std::string options = DODmpOptions + "\\symbols.txt";
  std::ifstream infileOptions(options);
  std::string inputCh;
  std::string symbolsString = "";
  std::string symbolsString2 = "";
  int symbolsInt;
  // Determine whether the symbols.txt file exists
  if(infileOptions.good()){
    // If it exists, obtain the symbols path
    size_t foundPound;
    // Get only the symbols path and ignore any commented lines
    int countOptions = 0;
    while(!infileOptions.eof()){
      getline(infileOptions,inputCh);
      foundPound = inputCh.find("#");
      while(foundPound != std::string::npos){
        getline(infileOptions,inputCh);
        foundPound = inputCh.find("#");
      }
      countOptions++;
      if (countOptions == 1){
        symbolsString = inputCh;
      }
      if (countOptions == 2){
        if(inputCh == "1" || inputCh == "2"){
          std::stringstream sstemp;
          sstemp << inputCh;
          sstemp >> symbolsInt;
        }
        else{
          symbolsString2 = inputCh;
        }
      }
      if (countOptions == 3){
        if(inputCh == "1" || inputCh == "2"){
          std::stringstream sstemp;
          sstemp << inputCh;
          sstemp >> symbolsInt;
        }
      }
    }
    infileOptions.close();
  }
  else{
    // if it does not exist, use online symbols and create symbols.txt
    symbolsString = "c:\\symbols";
    symbolsString2 = "srv*c:\\symbols*https://msdl.microsoft.com/download/symbols";
    symbolsInt = 2;
    infileOptions.close();
  }
  if(symbolsInt == 2){
    checkBox3->Checked = true;
    checkBox4->Checked = false;
  }
  else{
    checkBox3->Checked = false;
    checkBox4->Checked = true;
  }
}

// Online symbols options
System::Void Form1::checkBox2Change(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox2->Checked){
    checkBox1->Checked = false;
    checkBox3->Checked = false;
    checkBox4->Checked = false;
  }
}

// Local symbols option for module warnings
System::Void Form1::checkBox3Change(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox2->Checked){
    checkBox3->Checked = false;
    checkBox4->Checked = false;
  }
  if(checkBox3->Checked){
    checkBox4->Checked = false;
  }
}

// Local symbols option for WRONG symbols
System::Void Form1::checkBox4Change(System::Object^  sender, System::EventArgs^  e) {
       if(checkBox2->Checked){
         checkBox3->Checked = false;
         checkBox4->Checked = false;
       }
       if(checkBox4->Checked){
         checkBox3->Checked = false;
       }
     }

//open save as file
System::Void Form1::Open(System::Object^  sender, System::EventArgs^  e)
{
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::ofstream openingFile(tempDir + "\\openingFile.txt");
  openingFile << 0;
  openingFile.close();
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  System::IO::Stream^ myStream;
  System::Windows::Forms::OpenFileDialog^ openFileDialog1 = gcnew System::Windows::Forms::OpenFileDialog;
  System::String^ userProfileStr;
  std::string userProfile2 = userProfile + "\\Documents\\";
  userProfileStr = gcnew System::String(userProfile2.c_str());
  openFileDialog1->InitialDirectory = userProfileStr;
  openFileDialog1->Filter = "All files (*.*)|*.*|zdn files (*.zdn)|*.zdn";
  openFileDialog1->FilterIndex = 2;
  openFileDialog1->RestoreDirectory = true;
  if ( openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK )
  {
    if ( (myStream = openFileDialog1->OpenFile()) != nullptr )
    {
      using namespace System::Runtime::InteropServices;
      System::String ^ s = openFileDialog1->FileName;
      const char* chars =
      (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
      std::string os = chars;
      Marshal::FreeHGlobal(System::IntPtr((void*)chars));
      myStream->Close();
      std::ifstream getSettings(os);
      std::string thisLine;
      while(!getSettings.eof() && os.length() > 0 && getSettings.good()){
        if(thisLine.find("## ###") != std::string::npos){
          std::string thisFileName;
          thisFileName = userProfile + "\\SysnativeBSODApps\\" + thisLine.substr(6,thisLine.length()-6);
          std::ofstream changeFile(thisFileName.c_str());
          while(!getSettings.eof() && getSettings.good()){
            std::string previousLine = thisLine;
            getline(getSettings,thisLine);
            if(thisLine.find("### ## ###") != std::string::npos){
            }
            else if(thisLine.find("## ###") != std::string::npos){
              break;
            }
            int badFile = 0;
            std::string thisLine2;
            if(thisLine.find("### ## ###") != std::string::npos)
            {
              badFile = 1;
              thisLine2 = thisLine.substr(10,thisLine.length()-10);
            }
            while(!getSettings.eof() && badFile){
              if(thisLine2 == "parms\\$_code_box_begin" ||
                  thisLine2 == "parms\\$_code_box_end" ||
                  thisLine2 == "parms\\$_driver_update_header1" ||
                  thisLine2 == "parms\\$_footer1" ||
                  thisLine2 == "parms\\$_header1" ||
                  thisLine2 == "parms\\$_sig1" ||
                  thisLine2 == "parms\\un95" ||
                  thisLine2 == "dmpOptions\\7oldDriverAfter.txt" ||
                  thisLine2 == "dmpOptions\\8oldDriverAfter.txt" ||
                  thisLine2 == "dmpOptions\\addSpaces.txt" ||
                  thisLine2 == "dmpOptions\\allFilesDmps.txt" ||
                  thisLine2 == "dmpOptions\\driverInOut.txt" ||
                  thisLine2 == "dmpOptions\\EightED.txt" ||
                  thisLine2 == "dmpOptions\\EightOneED.txt" ||
                  thisLine2 == "dmpOptions\\TenED.txt" ||
                  thisLine2 == "dmpOptions\\excludedDrivers.txt" ||
                  thisLine2 == "dmpOptions\\fullGUI.txt" ||
                  thisLine2 == "dmpOptions\\htmlBBCode.txt" ||
                  thisLine2 == "dmpOptions\\inHtml.txt" ||
                  thisLine2 == "dmpOptions\\kdCommands.txt" ||
                  thisLine2 == "dmpOptions\\noBBCode.txt" ||
                  thisLine2 == "dmpOptions\\noBBCodeCodeBoxes.txt" ||
                  thisLine2 == "dmpOptions\\oldDriversRed.txt" ||
                  thisLine2 == "dmpOptions\\onlyUser.txt" ||
                  thisLine2 == "dmpOptions\\openOnExit.txt" ||
                  thisLine2 == "dmpOptions\\outputOptions.txt" ||
                  thisLine2 == "dmpOptions\\previous.txt" ||
                  thisLine2 == "dmpOptions\\problemDrivers.txt" ||
                  thisLine2 == "dmpOptions\\SevenED.txt" ||
                  thisLine2 == "dmpOptions\\symbols.txt" ||
                  thisLine2 == "dmpOptions\\template.txt" ||
                  thisLine2 == "dmpOptions\\turnOffBBCode.txt" ||
                  thisLine2 == "dmpOptions\\userFirst.txt" ||
                  thisLine2 == "dmpOptions\\VistaED.txt" ||
                  thisLine2 == "dmpOptions\\VistaoldDriverAfter.txt" ||
                  thisLine2 == "dmpOptions\\XPED.txt" ||
                  thisLine2 == "dmpOptions\\XPoldDriverAfter.txt"){
                    badFile = 0;
                    break;
              }
              getline(getSettings,thisLine);
              if(thisLine.find("### ## ###") != std::string::npos)
              {
                thisLine2 = thisLine.substr(10,thisLine.length()-10);
              }
            }
            if(previousLine.find("### ## ###") != std::string::npos){
                changeFile << std::endl;
            }
            else if(thisLine.find("### ## ###") != std::string::npos &&
              !getSettings.eof()){
                changeFile << std::endl;
            }
            else if(!(previousLine.find("## ###") != std::string::npos) &&
              !(thisLine.find("## ###") != std::string::npos)/* &&
              !getSettings.eof()*/){
                changeFile << std::endl;
            }
            if(thisLine.find("### ## ###") != std::string::npos){
              previousLine = thisLine;
              thisLine = thisLine.substr(4,thisLine.length()-4);
            }
            changeFile << thisLine;
            if(previousLine.find("### ## ###") != std::string::npos){
              thisLine = previousLine;
            }
          }
          if(thisLine.find("## ###") != std::string::npos){
            changeFile.close();
          }
        }
        else{
          getline(getSettings,thisLine);
        }
      }
      getSettings.close();
      if(os.length() > 0){
        //button10_Click(sender,e);
        std::string backupFile = userProfile + "\\SysnativeBSODApps\\backup\\tempFileName.txt>backupDir.txt";
        std::ofstream outFileNameFile(backupFile);
        outFileNameFile << os;
        outFileNameFile.close();
        FileSystemUtil::copyFile(userProfile + "\\SysnativeBSODApps\\dmpOptions\\fullGUI.txt", tempDir + "\\fullGUI.txt>null.txt", true);
        FileSystemUtil::copyFile(userProfile + "\\SysnativeBSODApps\\parms\\un95", tempDir + "\\un95", true);
        Hide();
        std::ofstream openingFile(tempDir + "\\openingFile.txt");
        openingFile << 0;
        openingFile.close();
        std::ofstream outPercentDone;
        outPercentDone.open(tempDir + "\\percentDone.txt");
        outPercentDone << 452312;
        outPercentDone.close();

        Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
        newThread->Start();

        loadPreviousSettings(sender, e);
        std::string tempDir = ConfigurationUtil::getTempDirectory();
        std::string tempPath;
        tempPath = tempDir + "\\percentDone.txt";
        outPercentDone.open(tempPath);
        outPercentDone << 100;
        outPercentDone.close();
        Show();
      }
    }
  }
}

//save as
System::Void Form1::saveAs(System::Object^  sender, System::EventArgs^  e)
{
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  if(tabControl1->Enabled){
    remove(std::string(userProfile + "\\SysnativeBSODApps\\backup\\tempFileName.txt").c_str());
    std::ofstream openingFile(tempDir + "\\openingFile.txt");
    openingFile << 0;
    openingFile.close();
    Hide();

    Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
    newThread->Start();

    saveAndRunTheApps2(sender, e);
    std::ofstream(outPercentDone);
    std::string tempPath;
    tempPath = tempDir + "\\percentDone.txt";
    outPercentDone.open(tempPath);
    outPercentDone << 100;
    outPercentDone.close();
    Show();
    System::IO::Stream^ myStream;
    System::Windows::Forms::SaveFileDialog^ saveFileDialog1 = gcnew System::Windows::Forms::SaveFileDialog;
    System::String^ userProfileStr;
    std::string userProfile2 = userProfile + "\\Documents\\";
    userProfileStr = gcnew System::String(userProfile2.c_str());
    saveFileDialog1->InitialDirectory = userProfileStr;
    saveFileDialog1->Filter = "All files (*.*)|*.*|zdn files (*.zdn)|*.zdn";
    saveFileDialog1->FilterIndex = 2;
    saveFileDialog1->RestoreDirectory = true;
    if ( saveFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK )
    {
      if ( (myStream = saveFileDialog1->OpenFile()) != nullptr )
      {
        using namespace System::Runtime::InteropServices;
        System::String ^ s = saveFileDialog1->FileName;
        const char* chars =
        (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
        std::string os = chars;
        Marshal::FreeHGlobal(System::IntPtr((void*)chars));
        myStream->Close();
        std::ofstream openSaveAs(tempDir + "\\saveAs.txt");
        openSaveAs << 0;
        openSaveAs.close();
        saveBackup(os);
        if(os.length() > 0){
          std::string backupFile = userProfile + "\\SysnativeBSODApps\\backup\\tempFileName.txt";
          std::ofstream outFileNameFile(backupFile);
          outFileNameFile << os;
          outFileNameFile.close();
        }
      }
    }
  }
}

//save backup and other .zdn based files
System::Void Form1::saveBackup(std::string os)
{
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string deleteFile;
  remove(os.c_str());

  std::vector<std::string> myDir = FileSystemUtil::getFiles(userProfile + "\\SysnativeBSODApps\\parms");
  std::vector<std::string> myDir2 = FileSystemUtil::getFiles(userProfile + "\\SysnativeBSODApps\\dmpOptions");
  std::vector<std::string> myDir3 = FileSystemUtil::getFiles(userProfile + "\\SysnativeBSODApps\\forumSettings");
  std::string sysnativeDir = userProfile + "\\SysnativeBSODApps\\";
  std::ofstream outFileName;
  outFileName.open(os, std::ios::app);
  std::string directoryName = "## ###directory parms";
  outFileName.close();
  std::string thisLine;
  int fl = 0;
  while(fl < int(myDir.size()))
  {
    size_t find1 = myDir[fl].find("\\");
    int j1 = 0;
    while(find1 != std::string::npos)
    {
      j1 = int(find1);
      find1 = myDir[fl].find("\\",j1+1,1);
    }
    myDir[fl] = myDir[fl].substr(j1+1,myDir[fl].length()-j1-1);
    fl++;
  }
  fl = 0;
  while(fl < int(myDir2.size()))
  {
    size_t find1 = myDir2[fl].find("\\");
    int j1 = 0;
    while(find1 != std::string::npos)
    {
      j1 = int(find1);
      find1 = myDir2[fl].find("\\",j1+1,1);
    }
    myDir2[fl] = myDir2[fl].substr(j1+1,myDir2[fl].length()-j1-1);
    fl++;
  }
  fl = 0;
  while(fl < int(myDir3.size()))
  {
    size_t find1 = myDir3[fl].find("\\");
    int j1 = 0;
    while(find1 != std::string::npos)
    {
      j1 = int(find1);
      find1 = myDir3[fl].find("\\",j1+1,1);
    }
    myDir3[fl] = myDir3[fl].substr(j1+1,myDir3[fl].length()-j1-1);
    fl++;
  }
  fl = 0;
  while(fl < int(myDir.size())){
    thisLine = myDir[fl];
    fl++;
    if(thisLine == "$_code_box_begin" ||
        thisLine == "$_code_box_end" ||
                thisLine == "$_driver_update_header1" ||
                thisLine == "$_footer1" ||
                thisLine == "$_header1" ||
                thisLine == "$_sig1" ||
                thisLine == "un95"){
        outFileName.open(os, std::ios::app);
        thisLine = directoryName.substr(16,directoryName.length()-16) + "\\" + thisLine;
        outFileName << std::endl << "## ###" << thisLine << std::endl;
        thisLine = sysnativeDir + "\\" + thisLine;
        std::ifstream inInput(thisLine);
        while(!inInput.eof() && inInput.good()){
          getline(inInput,thisLine);
          outFileName << thisLine;
          if(!inInput.eof()){
            outFileName << std::endl;
          }
        }
        inInput.close();
        outFileName.close();
    }
  }
  std::ifstream openSaveAs(tempDir + "\\saveAs.txt");
  std::ifstream openSaveAs2(tempDir + "\\saveAs2.txt");
  fl = 0;
  while((openSaveAs.good() || openSaveAs2.good()) && fl < int(myDir.size())){
    thisLine = myDir[fl];
    fl++;
  }
  outFileName.open(os, std::ios::app);
  directoryName = "## ###directory dmpOptions";
  outFileName.close();
  fl = 0;
  while(fl < int(myDir2.size())){
    thisLine = myDir2[fl];
    fl++;
    if(thisLine == "7oldDriverAfter.txt" ||
                thisLine == "8oldDriverAfter.txt" ||
                thisLine == "addSpaces.txt" ||
                thisLine == "allFilesDmps.txt" ||
                thisLine == "driverInOut.txt" ||
                thisLine == "EightED.txt" ||
                thisLine == "EightOneED.txt" ||
                thisLine == "TenED.txt" ||
                thisLine == "excludedDrivers.txt" ||
                thisLine == "fullGUI.txt" ||
                thisLine == "htmlBBCode.txt" ||
                thisLine == "inHtml.txt" ||
                thisLine == "kdCommands.txt" ||
                thisLine == "noBBCode.txt" ||
                thisLine == "noBBCodeCodeBoxes.txt" ||
                thisLine == "oldDriversRed.txt" ||
                thisLine == "onlyUser.txt" ||
                thisLine == "openOnExit.txt" ||
                thisLine == "outputOptions.txt" ||
                thisLine == "previous.txt" ||
                thisLine == "problemDrivers.txt" ||
                thisLine == "SevenED.txt" ||
                thisLine == "symbols.txt" ||
                thisLine == "template.txt" ||
                thisLine == "turnOffBBCode.txt" ||
                thisLine == "userFirst.txt" ||
                thisLine == "VistaED.txt" ||
                thisLine == "VistaoldDriverAfter.txt" ||
                thisLine == "XPED.txt" ||
                thisLine == "XPoldDriverAfter.txt"){
        outFileName.open(os, std::ios::app);
        thisLine = directoryName.substr(16,directoryName.length()-16) + "\\" + thisLine;
        outFileName << std::endl << "## ###" << thisLine << std::endl;
        thisLine = sysnativeDir + "\\" + thisLine;
        std::ifstream inInput(thisLine);
        while(!inInput.eof() && inInput.good()){
          getline(inInput,thisLine);
          outFileName << thisLine;
          if(!inInput.eof()){
            outFileName << std::endl;
          }
        }
        inInput.close();
        outFileName.close();
    }
  }
  fl = 0;
  while((openSaveAs.good() || openSaveAs2.good()) && fl < int(myDir2.size())){
    thisLine = myDir2[fl];
    fl++;
  }
  outFileName.open(os, std::ios::app);
  directoryName = "## ###directory forumSettings";
  outFileName.close();
  fl = 0;
  while(openSaveAs.good() && fl < int(myDir3.size())){
    thisLine = myDir3[fl];
    fl++;
    if(thisLine.length() > 0){
      outFileName.open(os, std::ios::app);
      thisLine = directoryName.substr(16,directoryName.length()-16) + "\\" + thisLine;
      outFileName << std::endl << "## ###" << thisLine << std::endl;
      thisLine = sysnativeDir + "\\" + thisLine;
      std::ifstream inInput(thisLine);
      while(!inInput.eof() && inInput.good()){
        getline(inInput,thisLine);
        if(thisLine.find("## ###") != std::string::npos){
          thisLine = "### " + thisLine;
        }
        outFileName << thisLine;
        if(!inInput.eof()){
          outFileName << std::endl;
        }
      }
      inInput.close();
      outFileName.close();
    }
  }
  openSaveAs.close();
  std::string tempPath = tempDir + "\\saveAs.txt";
  remove(tempPath.c_str());
  tempPath = tempDir + "\\saveAs2.txt";
  remove(tempPath.c_str());
}

// save option
System::Void Form1::saveOnly(System::Object^  sender, System::EventArgs^  e)
{
  if(tabControl1->Enabled){
    std::string userProfile = ConfigurationUtil::getUserProfilePath();
    std::string tempDir = ConfigurationUtil::getTempDirectory();
    std::string inFileName = userProfile + "\\SysnativeBSODApps\\backup\\tempFileName.txt";
    std::ifstream inFileNameFile;
    inFileNameFile.open(inFileName);
    if(inFileNameFile.good()){
      std::ofstream openingFile(tempDir + "\\openingFile.txt");
      openingFile << 0;
      openingFile.close();
      getline(inFileNameFile,inFileName);
      std::ofstream outPercentDone;
      outPercentDone.open(tempDir + "\\percentDone.txt");
      outPercentDone << 452312;
      outPercentDone.close();
      Hide();

      Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
      newThread->Start();

      saveAndRunTheApps2(sender,e);

      std::ofstream openSaveAs(tempDir + "\\saveAs.txt");
      openSaveAs << 0;
      openSaveAs.close();
      saveBackup(inFileName);
      std::string tempDir = ConfigurationUtil::getTempDirectory();
      std::string tempPath;
      tempPath = tempDir + "\\percentDone.txt";
      outPercentDone.open(tempPath);
      outPercentDone << 100;
      outPercentDone.close();
      Show();
    }
    else{
      saveAs(sender, e);
    }
  }
}

// select all Files to Save
System::Void Form1::checkBox5_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox5->Checked){
    for(int i = 0; i < checkedListBox1->Items->Count; i++){
      checkedListBox1->SetItemChecked(i, true);
    }
  }
  else{
    for(int i = 0; i < checkedListBox1->Items->Count; i++){
      checkedListBox1->SetItemChecked(i, false);
    }
  }
}

//select all Output Options
System::Void Form1::checkBox6_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox6->Checked){
    for(int i = 0; i < checkedListBox2->Items->Count; i++){
      checkedListBox2->SetItemChecked(i, true);
    }
  }
  else{
    for(int i = 0; i < checkedListBox2->Items->Count; i++){
      checkedListBox2->SetItemChecked(i, false);
    }
  }
}

//select all Formatting and Miscellaneous Options
System::Void Form1::checkBox7_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox7->Checked){
    for(int i = 0; i < checkedListBox3->Items->Count; i++){
      checkedListBox3->SetItemChecked(i, true);
    }
  }
  else{
    for(int i = 0; i < checkedListBox3->Items->Count; i++){
      checkedListBox3->SetItemChecked(i, false);
    }
  }
}

// Determine whether a Files to Save item is checked
System::Void Form1::checkList1Change(System::Object^  sender, System::EventArgs^  e)
{
  for(int i = 0; i < checkedListBox1->Items->Count; i++){
    if(checkedListBox1->GetItemChecked(i)){
      checkBox5->Checked = true;
      break;
    }
    else{
      checkBox5->Checked = false;
    }
  }
}

// Determine whether an Output Options item is checked
System::Void Form1::checkList2Change(System::Object^  sender, System::EventArgs^  e)
{
  for(int i = 0; i < checkedListBox2->Items->Count; i++){
    if(checkedListBox2->GetItemChecked(i)){
      checkBox6->Checked = true;
      break;
    }
    else{
      checkBox6->Checked = false;
    }
  }
}

// Determine whether a Formatting and Miscellaneous Options item is checked
System::Void Form1::checkList3Change(System::Object^  sender, System::EventArgs^  e)
{
  for(int i = 0; i < checkedListBox3->Items->Count; i++){
    if(checkedListBox3->GetItemChecked(i)){
      checkBox7->Checked = true;
      break;
    }
    else{
      checkBox7->Checked = false;
    }
  }
}

// Move objects for resize of the main Form1 window
System::Void Form1::resize(System::Object^  sender, System::EventArgs^  e)
{
  // Using CDPI example class
  CDPI g_metrics;
  int dpiX = g_metrics.GetDPIX();
  int x,y;

  tabControl1->Height = this->Height - 110 - int(double(100)*double(dpiX-96)/double(64));
  tabControl1->Width = this->Width - int(double(20)*double(dpiX)/double(96));
  richTextBox1->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox1->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox2->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox2->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox3->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox3->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox4->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox4->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox5->Height = tabControl1->Height - 60 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox5->Width = this->Width - int(double(34)*double(dpiX)/double(96));
  richTextBox6->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox7->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox8->Height = tabControl1->Height - 200 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox9->Height = tabControl1->Height - 200 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox10->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox11->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox12->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));
  richTextBox13->Height = tabControl1->Height - 160 - int(double(40)*double(dpiX-96)/double(64));

  x = 0;
  y = this->Height - label12->Height - int(double(50) * double(dpiX)/double(96));
  this->label12->Location = System::Drawing::Point(x, y);
}

// RTM of XP
System::Void Form1::XPRTM(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Aug";
  textBox19->Text = L"24";
  textBox20->Text = L"2001";
}

// SP1 of XP
System::Void Form1::XPSP1(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Sep";
  textBox19->Text = L"9";
  textBox20->Text = L"2002";
}

// SP2 of XP
System::Void Form1::XPSP2(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Aug";
  textBox19->Text = L"25";
  textBox20->Text = L"2004";
}

// SP2b of XP
System::Void Form1::XPSP2b(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Aug";
  textBox19->Text = L"";
  textBox20->Text = L"2006";
}

// SP2c of XP
System::Void Form1::XPSP2c(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Aug";
  textBox19->Text = L"10";
  textBox20->Text = L"2007";
}

// SP3 of XP
System::Void Form1::XPSP3(System::Object^  sender, System::EventArgs^  e)
{
  textBox18->Text = L"Apr";
  textBox19->Text = L"21";
  textBox20->Text = L"2008";
}

// RTM of Vista
System::Void Form1::VistaRTM(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Nov";
  textBox22->Text = L"8";
  textBox23->Text = L"2006";
}

// SP1 of Vista
System::Void Form1::VistaSP1(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Feb";
  textBox22->Text = L"4";
  textBox23->Text = L"2008";
}

// SP2 of Vista
System::Void Form1::VistaSP2(System::Object^  sender, System::EventArgs^  e)
{
  textBox21->Text = L"Apr";
  textBox22->Text = L"28";
  textBox23->Text = L"2009";
}

// RTM of Seven
System::Void Form1::SevenRTM(System::Object^  sender, System::EventArgs^  e) {
  textBox9->Text = L"Jul";
  textBox8->Text = L"22";
  textBox14->Text = L"2009";
}

// SP1 of Seven
System::Void Form1::SevenSP1(System::Object^  sender, System::EventArgs^  e)
{
  textBox9->Text = L"Feb";
  textBox8->Text = L"9";
  textBox14->Text = L"2011";
}

// RTM of Eight
System::Void Form1::EightRTM(System::Object^  sender, System::EventArgs^  e)
{
  textBox15->Text = L"Aug";
  textBox16->Text = L"1";
  textBox17->Text = L"2012";
}

// Always on top option
System::Void Form1::onChecked(System::Object^  sender, System::EventArgs^  e) {
  // Read user profile and save it to the userprofile std::string variable
  std::string userProfile = ConfigurationUtil::getUserProfilePath();

  int inTopi = 1;
  std::ofstream outTop(userProfile + "\\SysnativeBSODApps\\dmpOptions\\alwaysOnTop.txt");
  if(alwaysOnTopToolStripMenuItem->Checked){
    alwaysOnTopToolStripMenuItem->Checked = false;
    this->TopMost = false;
    outTop << 0;
  }
  else if(!alwaysOnTopToolStripMenuItem->Checked){
    alwaysOnTopToolStripMenuItem->Checked = true;
    this->TopMost = true;
    outTop << 1;
  }
  outTop.close();
}

// Close apps when finished option
System::Void Form1::onChecked2(System::Object^  sender, System::EventArgs^  e) {
  // Read user profile and save it to the userprofile std::string variable
  std::string userProfile = ConfigurationUtil::getUserProfilePath();

  int inTopi = 1;
  std::ofstream outFinished(userProfile + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
  if(closeAppsWhenFinishedToolStripMenuItem->Checked)
  {
    closeAppsWhenFinishedToolStripMenuItem->Checked = false;
        outFinished << 0;
  }
  else if(!closeAppsWhenFinishedToolStripMenuItem->Checked)
  {
    closeAppsWhenFinishedToolStripMenuItem->Checked = true;
        outFinished << 1;
  }
  outFinished.close();
}
//Exit in File menu
System::Void Form1::Exit(System::Object^  sender, System::EventArgs^  e)
{
  Form1::Close();
}

//Button to clear driver statistics
System::Void Form1::clearDriverStatistics(System::Object^  sender, System::EventArgs^  e)
{
  std::string errorString;
  errorString = "Are you sure you want to clear all statistics?";
  errorString = errorString + "\r\n\r\n";
  errorString = errorString + "This cannot be undone, and all current statistics will be reset to 0.";
  errorString = errorString + "\r\n";
  errorString = errorString + "All csv files will be deleted from the SysnativeBSODApps\\driverStatistics directory.";
  // convert the command std::string to wstring format
  System::String^ str2 = gcnew System::String(errorString.c_str());
  if(System::Windows::Forms::MessageBox::Show(str2,L"Clear Driver Statistics", System::Windows::Forms::MessageBoxButtons::OKCancel, System::Windows::Forms::MessageBoxIcon::Question,
	  System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly) == System::Windows::Forms::DialogResult::OK)
  {
    System::String^ userProfile = System::Environment::GetEnvironmentVariable("USERPROFILE");
    System::IO::Directory::Delete(userProfile + "\\SysnativeBSODApps\\driverStatistics", true);
  }
  else{
    return;
  }
}

void Form1::deleteNewOSes(int num)
{
    std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string tempDir = userProfile + "\\SysnativeBSODApps\\dmpOptions";

  // read in the EightOneED.txt file
  std::string inPath = tempDir + "\\EightOneED.txt";
  std::ifstream infile;
  infile.open(inPath);
  std::vector<std::string> EightOneED;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      EightOneED.push_back(inCh);
    }
  }
  infile.close();

  std::ofstream outfile;
  outfile.open(inPath);
  int nWritten = 0;
  for(size_t i=0; i < EightOneED.size(); i++)
  {
    if(i != num)
    {
      if(nWritten)
      {
        outfile << std::endl;
      }
      nWritten++;
      outfile << EightOneED[i];
    }
  }
  outfile.close();

  // read in the TenED.txt file
  inPath = tempDir + "\\TenED.txt";
  infile.open(inPath);
  std::vector<std::string> TenED;
  if(infile.good()){
    while(!infile.eof()){
      std::string inCh;
      getline(infile,inCh);
      TenED.push_back(inCh);
    }
  }
  infile.close();

  outfile.open(inPath);
  nWritten = 0;
  for(size_t i=0; i < TenED.size(); i++)
  {
    if(i != num)
    {
      if(nWritten)
      {
        outfile << std::endl;
      }
      nWritten++;
      outfile << TenED[i];
    }
  }
  outfile.close();
}

// Add/Edit/Delete items from excluded drivers list
System::Void Form1::button5_Click(System::Object^  sender, System::EventArgs^  e)
{
  int num = 0;
  System::String^ str2;
  std::vector<std::string> ED1;
  std::vector<std::string> ED2;
  std::vector<std::string> ED3;
  std::vector<std::string> ED4;
  std::vector<std::string> ED5;
  std::vector<std::string> ED6;
  std::ifstream EDFile;
  int boxClear = 0;
  int osClear = 0;

  if(checkBox21->Checked){
    checkBox12->Checked = true;
    checkBox11->Checked = false;
    numericUpDown1->Visible = true;
    textBox24->Clear();
    textBox25->Clear();
    checkBox13->Checked = false;
    checkBox14->Checked = false;
    checkBox15->Checked = false;
    checkBox16->Checked = false;
  }
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  richTextBox6->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if((int(newString.length()) > 0) || (checkBox12->Checked && int(numericUpDown1->Value) == num && int(newString.length()) >= 0)){
      System::String ^ s = textBox24->Text;
      const char* chars =
      (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
      std::string os = chars;
      System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(os.length() > 3){
          ED1.push_back(os);
        }
        else{
          boxClear++;
        }

      }
      else{
        ED1.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    System::String ^ s = textBox24->Text;
    const char* chars =
    (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
    std::string os = chars;
    System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    if(os.length() > 3){
      ED1.push_back(os);
    }
    else{
      boxClear++;
    }
  }
  EDFile.close();

  richTextBox7->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  num = 0;
  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if((int(newString.length()) > 0) || (checkBox12->Checked && int(numericUpDown1->Value) == num && int(newString.length()) >= 0)){
      System::String ^ s = textBox25->Text;
      const char* chars =
      (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
      std::string os = chars;
      System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(os.length() > 3){
          ED2.push_back(os);
        }
        else{
          boxClear++;
        }

      }
      else{
        ED2.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    System::String ^ s = textBox25->Text;
    const char* chars =
    (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
    std::string os = chars;
    System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    if(os.length() > 3){
      ED2.push_back(os);
    }
    else{
      boxClear++;
    }
  }
  EDFile.close();

  richTextBox10->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  num = 0;
  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(checkBox13->Checked){
          ED3.push_back("x");
          osClear++;
        }
        else if(!boxClear){
          ED3.push_back("");
        }

      }
      else{
        ED3.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    if(checkBox13->Checked){
      ED3.push_back("x");
      osClear++;
    }
    else{
      ED3.push_back("");
    }
  }
  EDFile.close();

  richTextBox11->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  num = 0;
  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(checkBox14->Checked){
          ED4.push_back("x");
          osClear++;
        }
        else if(!boxClear){
          ED4.push_back("");
        }
      }
      else{
        ED4.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    if(checkBox14->Checked){
      ED4.push_back("x");
      osClear++;
    }
    else{
      ED4.push_back("");
    }
  }
  EDFile.close();

  richTextBox12->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  num = 0;
  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(checkBox15->Checked){
          ED5.push_back("x");
          osClear++;
        }
        else if(!boxClear){
          ED5.push_back("");
        }
      }
      else{
        ED5.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    if(checkBox15->Checked){
      ED5.push_back("x");
      osClear++;
    }
    else{
      ED5.push_back("");
    }
  }
  EDFile.close();

  richTextBox13->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  num = 0;
  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      if(checkBox12->Checked && int(numericUpDown1->Value) == num){
        if(checkBox16->Checked){
          ED6.push_back("x");
          osClear++;
        }
        else if(!boxClear){
          ED6.push_back("");
        }
      }
      else{
        ED6.push_back(newString);
      }
    }
  }
  if(checkBox11->Checked){
    if(checkBox16->Checked){
      ED6.push_back("x");
      osClear++;
    }
    else{
      ED6.push_back("");
    }
  }
  EDFile.close();


  if(boxClear == 2 && !osClear && checkBox12->Checked){
    TopMost = true;
    if(System::Windows::Forms::MessageBox::Show("Are you sure you want to delete the line chosen?\nThis cannot be undone!",L"Clear Excluded Driver", System::Windows::Forms::MessageBoxButtons::YesNo, System::Windows::Forms::MessageBoxIcon::Question,
		System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly) == System::Windows::Forms::DialogResult::Yes){
        boxClear = 0;
        osClear = 1;
        deleteNewOSes(int(numericUpDown1->Value) - 1);
    }
    else
    {
      boxClear = 23;
    }
    TopMost = false;
  }


  if(!boxClear && osClear){
    if(checkBox11->Checked)
    {
      std::ofstream outfile;
      std::string inPath = userProfile + "\\SysnativeBSODApps\\dmpOptions\\EightOneED.txt";
      outfile.open(inPath, std::ios::app);
      outfile << std::endl;
      outfile.close();
      inPath = userProfile + "\\SysnativeBSODApps\\dmpOptions\\TenED.txt";
      outfile.open(inPath, std::ios::app);
      outfile << std::endl;
      outfile.close();
    }
    richTextBox6->Clear();
    for(int i = 0; i < int(ED1.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED1[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED1[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED1[i];
      }
      ED1[i] = sstemp.str();
      if(i < int(ED1.size()) - 1){
        ED1[i] = ED1[i] + "\n";
      }
      str2 = gcnew System::String(ED1[i].c_str());
      richTextBox6->AppendText(str2);
    }
    richTextBox7->Clear();
    for(int i = 0; i < int(ED2.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED2[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED2[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED2[i];
      }
      ED2[i] = sstemp.str();
      if(i < int(ED2.size()) - 1){
        ED2[i] = ED2[i] + "\n";
      }
      str2 = gcnew System::String(ED2[i].c_str());
      richTextBox7->AppendText(str2);
    }
    richTextBox10->Clear();
    for(int i = 0; i < int(ED3.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED3[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED3[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED3[i];
      }
      ED3[i] = sstemp.str();
      if(i < int(ED3.size()) - 1){
        ED3[i] = ED3[i] + "\n";
      }
      str2 = gcnew System::String(ED3[i].c_str());
      richTextBox10->AppendText(str2);
    }
    richTextBox11->Clear();
    for(int i = 0; i < int(ED4.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED4[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED4[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED4[i];
      }
      ED4[i] = sstemp.str();
      if(i < int(ED4.size()) - 1){
        ED4[i] = ED4[i] + "\n";
      }
      str2 = gcnew System::String(ED4[i].c_str());
      richTextBox11->AppendText(str2);
    }
    richTextBox12->Clear();
    for(int i = 0; i < int(ED5.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED5[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED5[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED5[i];
      }
      ED5[i] = sstemp.str();
      if(i < int(ED5.size()) - 1){
        ED5[i] = ED5[i] + "\n";
      }
      str2 = gcnew System::String(ED5[i].c_str());
      richTextBox12->AppendText(str2);
    }
    richTextBox13->Clear();
    for(int i = 0; i < int(ED6.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED6[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED6[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED6[i];
      }
      ED6[i] = sstemp.str();
      if(i < int(ED6.size()) - 1){
        ED6[i] = ED6[i] + "\n";
      }
      str2 = gcnew System::String(ED6[i].c_str());
      richTextBox13->AppendText(str2);
    }
  }
  else if(boxClear && boxClear != 23){
    TopMost = true;
	System::Windows::Forms::MessageBox::Show(L"A key input was left blank.\nPlease fill in all text boxes",L"Blank Input", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
    TopMost = false;
  }
  else if(!osClear && boxClear != 23){
    TopMost = true;
	System::Windows::Forms::MessageBox::Show(L"No Windows Version was selected for this driver.\nPlease select a Windows OS version",L"Missing OS Version", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
    TopMost = false;
  }
  checkBox21_CheckedChanged(sender, e);
}

// Add items to excluded drivers
System::Void Form1::checkBox11_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox11->Checked){
    checkBox12->Checked = false;
    numericUpDown1->Enabled = false;
    checkBox21->Checked = false;
    textBox24->ReadOnly = false;
    textBox25->ReadOnly = false;
    checkBox13->Enabled = true;
    checkBox14->Enabled = true;
    checkBox15->Enabled = true;
    checkBox16->Enabled = true;
  }
}

// Edit excluded drivers list
System::Void Form1::checkBox12_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox12->Checked){
    textBox24->ReadOnly = false;
    textBox25->ReadOnly = false;
    checkBox13->Enabled = true;
    checkBox14->Enabled = true;
    checkBox15->Enabled = true;
    checkBox16->Enabled = true;
    checkBox11->Checked = false;
    numericUpDown1->Enabled = true;
    int num = 0;
    System::String^ str2;
    std::vector<std::string> ED1;
    std::ifstream EDFile;

    std::string tempDir = ConfigurationUtil::getTempDirectory();
    richTextBox6->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown1->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox24->Text = str2;
      }
    }
  }
  EDFile.close();

    richTextBox7->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    ED1.clear();
    num = 0;
    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown1->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox25->Text = str2;
      }
    }
  }
  EDFile.close();

    richTextBox10->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    ED1.clear();
    num = 0;
    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(newString.length() > 0 && num == int(numericUpDown1->Value)){
        checkBox13->Checked = true;
      }
      else if(num == int(numericUpDown1->Value)){
        checkBox13->Checked = false;
      }
    }
  }
  EDFile.close();

    richTextBox11->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    ED1.clear();
    num = 0;
    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(newString.length() > 0 && num == int(numericUpDown1->Value)){
        checkBox14->Checked = true;
      }
      else if(num == int(numericUpDown1->Value)){
        checkBox14->Checked = false;
      }
    }
  }
  EDFile.close();

    richTextBox12->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    ED1.clear();
    num = 0;
    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(newString.length() > 0 && num == int(numericUpDown1->Value)){
        checkBox15->Checked = true;
      }
      else if(num == int(numericUpDown1->Value)){
        checkBox15->Checked = false;
      }
    }
  }
  EDFile.close();

    richTextBox13->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    ED1.clear();
    num = 0;
    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(newString.length() > 0 && num == int(numericUpDown1->Value)){
        checkBox16->Checked = true;
      }
      else if(num == int(numericUpDown1->Value)){
        checkBox16->Checked = false;
      }
    }
  }
  EDFile.close();

  numericUpDown1->Maximum = int(ED1.size());
  numericUpDown1->Minimum = 1;
  }
  else if(!checkBox12->Checked){
    numericUpDown1->Enabled = false;
    checkBox21->Checked = false;
  }
  if(checkBox21->Checked){
    checkBox12->Checked = true;
    checkBox11->Checked = false;
    numericUpDown1->Visible = true;
    textBox24->ReadOnly = true;
    textBox25->ReadOnly = true;
    checkBox13->Enabled = false;
    checkBox14->Enabled = false;
    checkBox15->Enabled = false;
    checkBox16->Enabled = false;
    checkBox13->Checked = false;
    checkBox14->Checked = false;
    checkBox15->Checked = false;
    checkBox16->Checked = false;
  }
}

//Delete excluded driver from list
System::Void Form1::checkBox21_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
  if(checkBox21->Checked){
    checkBox12->Checked = true;
    checkBox21->Checked = true;
    int num = 0;
    System::String^ str2;
    std::vector<std::string> ED1;
    std::ifstream EDFile;

    std::string tempDir = ConfigurationUtil::getTempDirectory();
    richTextBox6->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown1->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox24->Text = str2;
      }
    }
  }
  EDFile.close();
    checkBox11->Checked = false;
    numericUpDown1->Visible = true;
    textBox24->ReadOnly = true;
    textBox25->ReadOnly = true;
    checkBox13->Enabled = false;
    checkBox14->Enabled = false;
    checkBox15->Enabled = false;
    checkBox16->Enabled = false;
    checkBox13->Checked = false;
    checkBox14->Checked = false;
    checkBox15->Checked = false;
    checkBox16->Checked = false;
  }
  else if(!checkBox21->Checked){
    textBox24->ReadOnly = false;
    textBox25->ReadOnly = false;
    checkBox12_CheckedChanged(sender, e);
    checkBox13->Enabled = true;
    checkBox14->Enabled = true;
    checkBox15->Enabled = true;
    checkBox16->Enabled = true;
  }
}

// Proceed to add/edit/delete user kd command
System::Void Form1::button6_Click(System::Object^  sender, System::EventArgs^  e)
{
  int num = 0;
  System::String^ str2;
  std::vector<std::string> ED1;
  std::ifstream EDFile;
  int boxClear = 0;

  if(checkBox22->Checked){
    checkBox17->Checked = true;
    checkBox18->Checked = false;
    numericUpDown2->Visible = true;
    textBox26->Clear();
  }

  std::string tempDir = ConfigurationUtil::getTempDirectory();
  richTextBox9->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if((int(newString.length()) > 0) || (checkBox17->Checked && int(numericUpDown2->Value) == num && int(newString.length()) >= 0)){
      System::String ^ s = textBox26->Text;
      const char* chars =
      (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
      std::string os = chars;
      System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
      if(checkBox17->Checked && int(numericUpDown2->Value) == num){
        if(os.length() > 0){
          ED1.push_back(os);
        }
        else{
          boxClear++;
        }

      }
      else{
        ED1.push_back(newString);
      }
    }
  }
  if(checkBox18->Checked){
    System::String ^ s = textBox26->Text;
    const char* chars =
    (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
    std::string os = chars;
    System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    if(os.length() > 0){
      ED1.push_back(os);
    }
    else{
      boxClear++;
    }
  }
  EDFile.close();


  if(boxClear && checkBox17->Checked){
    TopMost = true;
    if(System::Windows::Forms::MessageBox::Show("Are you sure you want to delete the line chosen?\nThis cannot be undone!",L"Clear kd Command", System::Windows::Forms::MessageBoxButtons::YesNo, System::Windows::Forms::MessageBoxIcon::Question,
		System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly) == System::Windows::Forms::DialogResult::Yes){
        boxClear = 0;
    }
    TopMost = false;
  }


  if(!boxClear){
    richTextBox9->Clear();
    for(int i = 0; i < int(ED1.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED1[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED1[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED1[i];
      }
      ED1[i] = sstemp.str();
      if(i < int(ED1.size()) - 1){
        ED1[i] = ED1[i] + "\n";
      }
      str2 = gcnew System::String(ED1[i].c_str());
      richTextBox9->AppendText(str2);
    }
  }
  else if(boxClear){
    System::Windows::Forms::MessageBox::Show(L"The input was left blank.\nPlease fill in the text box",L"Blank Input", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
  }
  checkBox22_CheckedChanged(sender, e);
}

// Add checkbox for kd user commands
System::Void Form1::checkBox18_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox18->Checked){
    checkBox17->Checked = false;
    numericUpDown2->Enabled = false;
    checkBox22->Checked = false;
    textBox26->ReadOnly = false;
  }
}

// Edit checkbox for kd user commands
System::Void Form1::checkBox17_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox17->Checked){
    checkBox18->Checked = false;
    numericUpDown2->Enabled = true;
    textBox26->ReadOnly = false;
    int num = 0;
    System::String^ str2;
    std::vector<std::string> ED1;
    std::ifstream EDFile;

    std::string tempDir = ConfigurationUtil::getTempDirectory();
    richTextBox9->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown2->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox26->Text = str2;
      }
    }
  }
  EDFile.close();

  numericUpDown2->Maximum = int(ED1.size());
  numericUpDown2->Minimum = 1;
  }
  else if(!checkBox17->Checked){
    numericUpDown2->Enabled = false;
    checkBox22->Checked = false;
  }
}

// Delete checkbox for user kd commands
System::Void Form1::checkBox22_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
{
  if(checkBox22->Checked){
    checkBox17->Checked = true;
    checkBox22->Checked = true;
    int num = 0;
    System::String^ str2;
    std::vector<std::string> ED1;
    std::ifstream EDFile;

    std::string tempDir = ConfigurationUtil::getTempDirectory();
    richTextBox9->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown2->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox26->Text = str2;
      }
    }
  }
  EDFile.close();

  numericUpDown2->Maximum = int(ED1.size());
  numericUpDown2->Minimum = 1;
  checkBox18->Checked = false;
  textBox26->ReadOnly = true;
  numericUpDown2->Visible = true;
  textBox26->Clear();
  }
  else if(!checkBox22->Checked){
    checkBox17_CheckedChanged(sender, e);
    textBox26->ReadOnly = false;
  }
}

// Proceed button for adding to driver statistics
System::Void Form1::button7_Click(System::Object^  sender, System::EventArgs^  e)
{
  int num = 0;
  System::String^ str2;
  std::vector<std::string> ED1;
  std::ifstream EDFile;
  int boxClear = 0;

  if(checkBox23->Checked){
    checkBox19->Checked = true;
    checkBox20->Checked = false;
    numericUpDown3->Visible = true;
    textBox27->Clear();
  }

  std::string tempDir = ConfigurationUtil::getTempDirectory();
  richTextBox8->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

  EDFile.open(tempDir + "\\ED.txt");

  while(!EDFile.eof() && EDFile.good()){
    std::string inString;
    std::string newString;
    getline(EDFile,inString);
    num++;
    std::stringstream ssnum;
    ssnum << num;
    int find = 0;
    int find2 = 0;
    int find3 = 0;
    int intFound = 0;
    int brokenList = 0;
    std::stringstream ssnum2;
    std::stringstream ssErase;
    for(int i = 0; i < int(inString.length()); i++){
      if(inString.c_str()[i] == ' '){
        if(i > 0 && inString.c_str()[i-1] != ' '){
          ssErase << inString.c_str()[i];
        }
      }
      else{
        ssErase << inString.c_str()[i];
      }
    }
    inString = ssErase.str();
    int brokenList2 = 0;
    for(int i = 0; i < int(inString.length()); i++){
      if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
        || inString.c_str()[i] == '}')){
          brokenList2 = 1;
      }
      if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
        inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
        inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
        inString.c_str()[i] == '9') && !brokenList && !brokenList2){
          if(!intFound){
            find = i;
          }
          intFound++;
          ssnum2 << inString.c_str()[i];
      }
      else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if((int(newString.length()) > 0) || (checkBox19->Checked && int(numericUpDown3->Value) == num && int(newString.length()) >= 0)){
      System::String ^ s = textBox27->Text;
      const char* chars =
      (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
      std::string os = chars;
      System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
      if(checkBox19->Checked && int(numericUpDown3->Value) == num){
        if(os.length() > 3){
          ED1.push_back(os);
        }
        else{
          boxClear++;
        }
      }
      else{
        ED1.push_back(newString);
      }
    }
  }
  if(checkBox20->Checked){
    System::String ^ s = textBox27->Text;
    const char* chars =
    (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
    std::string os = chars;
    System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
    if(os.length() > 3){
      ED1.push_back(os);
    }
    else{
      boxClear++;
    }
  }
  EDFile.close();


  if(boxClear && checkBox19->Checked){
    TopMost = true;
    if(System::Windows::Forms::MessageBox::Show("Are you sure you want to delete the line chosen?\nThis cannot be undone!",L"Clear Driver From Statistics", System::Windows::Forms::MessageBoxButtons::YesNo, System::Windows::Forms::MessageBoxIcon::Question,
		System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly) == System::Windows::Forms::DialogResult::Yes){
        boxClear = 0;
    }
    TopMost = false;
  }


  if(!boxClear){
    richTextBox8->Clear();
    for(int i = 0; i < int(ED1.size()); i++){
      std::stringstream sstemp;
      if(i < 9){
        sstemp << "{" << i + 1 << "}    " << ED1[i];
      }
      else if(i >= 9 && i < 99){
        sstemp << "{" << i + 1 << "}   " << ED1[i];
      }
      else if(i >= 99 && i < 999){
        sstemp << "{" << i + 1 << "}  " << ED1[i];
      }
      ED1[i] = sstemp.str();
      if(i < int(ED1.size()) - 1){
        ED1[i] = ED1[i] + "\n";
      }
      str2 = gcnew System::String(ED1[i].c_str());
      richTextBox8->AppendText(str2);
    }
  }
  else if(boxClear){
	  System::Windows::Forms::MessageBox::Show(L"The input was left blank.\nPlease fill in the text box",L"Blank Input", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
  }
  checkBox23_CheckedChanged(sender, e);
}

// Add checkbox for adding to driver statistics
System::Void Form1::checkBox20_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox20->Checked){
    checkBox19->Checked = false;
    numericUpDown3->Enabled = false;
    checkBox23->Checked = false;
  }
}

// Edit checkbox for editing drivers in driver statistics list
System::Void Form1::checkBox19_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox19->Checked){
    checkBox20->Checked = false;
    numericUpDown3->Enabled = true;
    int num = 0;
    System::String^ str2;
    std::vector<std::string> ED1;
    std::ifstream EDFile;

    std::string tempDir = ConfigurationUtil::getTempDirectory();
    richTextBox8->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown3->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox27->Text = str2;
      }
    }
  }
  EDFile.close();

  numericUpDown3->Maximum = int(ED1.size());
  numericUpDown3->Minimum = 1;
  }
  else if(!checkBox19->Checked){
    numericUpDown3->Enabled = false;
    checkBox23->Checked = false;
  }
}

// Delete checkbox for removing items from driver statistics
System::Void Form1::checkBox23_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
  if(checkBox23->Checked){
    checkBox19->Checked = true;
    checkBox23->Checked = true;
    int num = 0;
    System::String^ str2;
    std::vector<std::string> ED1;
    std::ifstream EDFile;

    std::string tempDir = ConfigurationUtil::getTempDirectory();
    richTextBox8->SaveFile(gcnew System::String(tempDir.c_str()) + "\\ED.txt", System::Windows::Forms::RichTextBoxStreamType::PlainText);

    EDFile.open(tempDir + "\\ED.txt");

    while(!EDFile.eof() && EDFile.good()){
      std::string inString;
      std::string newString;
      getline(EDFile,inString);
      num++;
      std::stringstream ssnum;
      ssnum << num;
      int find = 0;
      int find2 = 0;
      int find3 = 0;
      int intFound = 0;
      int brokenList = 0;
      std::stringstream ssnum2;
      std::stringstream ssErase;
      for(int i = 0; i < int(inString.length()); i++){
        if(inString.c_str()[i] == ' '){
          if(i > 0 && inString.c_str()[i-1] != ' '){
            ssErase << inString.c_str()[i];
          }
        }
        else{
          ssErase << inString.c_str()[i];
        }
      }
      inString = ssErase.str();
      int brokenList2 = 0;
      for(int i = 0; i < int(inString.length()); i++){
        if(intFound && !brokenList2 && (inString.c_str()[i] == ' '
          || inString.c_str()[i] == '}')){
            brokenList2 = 1;
        }
        if((inString.c_str()[i] == '0' || inString.c_str()[i] == '1' || inString.c_str()[i] == '2' ||
          inString.c_str()[i] == '3' || inString.c_str()[i] == '4' || inString.c_str()[i] == '5' ||
          inString.c_str()[i] == '6' || inString.c_str()[i] == '7' || inString.c_str()[i] == '8' ||
          inString.c_str()[i] == '9') && !brokenList && !brokenList2){
            if(!intFound){
              find = i;
            }
            intFound++;
            ssnum2 << inString.c_str()[i];
        }
        else if(intFound && !brokenList && inString.c_str()[i] != ' '
        && inString.c_str()[i] != '}'){
          find2 = i;
          brokenList = 1;
          brokenList2 = 1;
      }
    }
    if(find2 == 0 && find < 2){
      find2 = inString.length();
    }
    if(ssnum.str() == ssnum2.str() && find < 2 && find2 > find){
      newString = inString.substr(find2,inString.length() - find2);
    }
    else{
      newString = inString;
    }
    if(int(newString.length()) >= 0){
      ED1.push_back(newString);
      if(num == int(numericUpDown3->Value)){
        str2 = gcnew System::String(newString.c_str());
        textBox27->Text = str2;
      }
    }
  }
  EDFile.close();

  numericUpDown3->Maximum = int(ED1.size());
  numericUpDown3->Minimum = 1;
  checkBox20->Checked = false;
  numericUpDown3->Visible = true;
  textBox27->ReadOnly = true;
  textBox27->Clear();
  }
  else if(!checkBox23->Checked)
  {
    checkBox19_CheckedChanged(sender, e);
    textBox27->ReadOnly = false;
  }
}

// Save Forum .zdn file
System::Void Form1::button9_Click(System::Object^  sender, System::EventArgs^  e)
{
    std::string tempDir = ConfigurationUtil::getTempDirectory();
    Hide();
  std::ofstream openingFile(tempDir + "\\openingFile.txt");
  openingFile << 0;
  openingFile.close();
  std::ofstream outPercentDone(tempDir + "\\percentDone.txt");
  outPercentDone << 452312;
  outPercentDone.close();

  Thread^ newThread = gcnew Thread(gcnew System::Threading::ThreadStart(mProgressBarDispatcher, &ManagedProgressBarDispatcherWrapper::openProgressBar));
  newThread->Start();

  System::String^ s = textBox2->Text;
  const char* chars =
  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
  std::string os = chars;
  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

    std::string userProfile = ConfigurationUtil::getUserProfilePath();
    std::string dmpListTemp = SystemStandardConversionUtil::convertSystemStringToStdString(System::Environment::GetEnvironmentVariable("TEMP")) + "\\SysnativeBSODApps\\";
  std::ifstream infile;
  std::string inChar;

  std::string DOParms = userProfile + "\\SysnativeBSODApps\\parms\\";
  std::string DODmpOptions = dmpListTemp;

  std::string pathDmpListTimeout = DODmpOptions + "dmpListTimeout.txt";

  DODmpOptions = userProfile + "\\SysnativeBSODApps\\dmpOptions\\";

  std::ofstream outDmpListTimeout(pathDmpListTimeout);
  outDmpListTimeout << int(numericUpDown4->Value);
  outDmpListTimeout.close();


  std::stringstream ssforumString;
  inChar = os;
  //If the user is not opening a file
  if(1){
    //Get the forum the post came from to generate forum settings
    for(int i77 = 0; i77 < int(inChar.length()); i77++){
      if(inChar.c_str()[i77] != '/' && inChar.c_str()[i77] != '.' && inChar.c_str()[i77] != ':'){
        ssforumString << inChar.c_str()[i77];
      }
    }
    std::string forumString = ssforumString.str();
    if(forumString.find("answersmicrosoft") != std::string::npos){
      saveAndRunTheApps2(sender, e);
    std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\answersmicrosoft.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("socialmicrosoftcomForums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\socialmicrosoftcomForums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("technetmicrosoft") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\technetmicrosoft.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("sysnative") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\sysnative.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("forumstechguy") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumstechguy.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("geekstogo") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\geekstogo.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("howtogeek") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\howtogeek.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("majorgeeks") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\majorgeeks.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("forumscnetcom") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumscnetcom.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("windowsforums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowsforums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("forumswindowsforum") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forumswindowsforum.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("forummintywhite") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\forummintywhite.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("winsource") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\winsource.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("windowssecretscomforums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowssecretscomforums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("windowsitpro") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windowsitpro.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("windows7forums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows7forums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("win8forums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\win8forums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("w7forums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\w7forums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("sevenforums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\sevenforums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("eightforums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\eightforums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("windows8forums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\windows8forums.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("techsupportforum") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\techsupportforum.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("bleepingcomputer") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\bleepingcomputer.zdn";
      saveBackup(forumFileName);
    }
    else if(forumString.find("tenforums") != std::string::npos){
      saveAndRunTheApps2(sender, e);
      std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\tenforums.zdn";
      saveBackup(forumFileName);
    }
    else
        {
            std::string tempPath;
        tempPath = tempDir + "\\percentDone.txt";
        outPercentDone.open(tempPath);
        outPercentDone << 339154;
        outPercentDone.close();
		System::Windows::Forms::MessageBox::Show(L"No file name exists for that forum.\nCheck your link in the Originating Post input,\nor ask that the forum be added to support",
                              L"No forum by that name", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1);
        outPercentDone.open(tempPath);
        outPercentDone << 0;
        outPercentDone.close();
        saveAndRunTheApps2(sender, e);
        std::string forumFileName = userProfile + "\\SysnativeBSODApps\\forumSettings\\wrongName.zdn";
        saveBackup(forumFileName);
    }
  }

    std::string tempPath;
  tempPath = tempDir + "\\percentDone.txt";
  outPercentDone.open(tempPath);
  outPercentDone << 100;
  outPercentDone.close();
  Sleep(495);
  Show();
}

System::Void Form1::useParallelThreadingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  mDefaultThreads = mDispatcher->defaultThreads;
  mUserThreads = mDispatcher->userThreads;
  if(useParallelThreadingToolStripMenuItem->Checked)
  {
    mDispatcher->defaultThreads = mDefaultThreads;
    mDispatcher->userThreads = mUserThreads;
    bool bTop = this->TopMost;
    if(bTop)
    {
      onChecked(sender, e);
    }
    mDispatcher->ShowDialog();
    if(bTop)
    {
      onChecked(sender, e);
    }
    mParallelChecked = true;
    changeThreadsToolStripMenuItem->Visible = true;
  }
  else
  {
    mDefaultThreads = 1;
    mUserThreads = 1;
    mDispatcher->defaultThreads = 1;
    mDispatcher->userThreads = 1;
    mParallelChecked = false;
    changeThreadsToolStripMenuItem->Visible = false;
  }
}

void Form1::UpdateFirstScreenUI()
{
    // Get the default output files
    checkedListBox1->SetItemChecked(0, false);
    checkedListBox1->SetItemChecked(1, false);
    checkedListBox1->SetItemChecked(2, false);
    checkedListBox1->SetItemChecked(3, false);
    checkedListBox1->SetItemChecked(4, false);
    checkedListBox1->SetItemChecked(5, false);
    checkedListBox1->SetItemChecked(6, false);
    checkedListBox1->SetItemChecked(7, false);
    checkedListBox1->SetItemChecked(8, false);
    checkedListBox1->SetItemChecked(9, false);
    checkedListBox1->SetItemChecked(10, true);
    checkedListBox1->SetItemChecked(11, true);
    checkedListBox1->SetItemChecked(12, true);
    checkedListBox1->SetItemChecked(13, true);
    checkedListBox1->SetItemChecked(14, true);
    if (mParallelChecked)
    {
        changeThreadsToolStripMenuItem->Visible = true;
    }
    else
    {
        changeThreadsToolStripMenuItem->Visible = false;
    }

    if (mSysnativeResultsChecked)
    {
        sysnativeResultsDIrectoryToolStripMenuItem->Checked = true;
        setDirectoryToolStripMenuItem->Visible = true;
    }
    else
    {
        sysnativeResultsDIrectoryToolStripMenuItem->Checked = false;
        setDirectoryToolStripMenuItem->Visible = false;
    }

    checkBox11->Checked = true;
    checkBox12->Checked = false;
    checkBox18->Checked = true;
    checkBox17->Checked = false;
    checkBox20->Checked = true;
    checkBox19->Checked = false;

    label29->Visible = true;
    label28->Visible = true;
    textBox11->Visible = true;
    label27->Visible = true;
    textBox4->Visible = true;
    label49->Visible = true;
    label48->Visible = true;
    textBox29->Visible = true;
    label47->Visible = true;
    textBox28->Visible = true;
    button3->Visible = true;
    button2->Visible = true;
    button4->Visible = true;
    checkBox10->Visible = false;

    tabControl1->Visible = false;
    menuStrip1->Visible = true;
    button9->Visible = false;

    textBox11->Text = mUsername;
    textBox29->Text = mOutputDir;
    textBox28->Text = mTimeDir;

    if (mFullGui) {
        checkBox10->Checked = true;
    }
    else {
        checkBox10->Checked = true;
    }
}

void Form1::setupFirstScreen()
{
  // create LoadSaveDZDN object
  LoadSaveDZDN* lsd;
  lsd = new LoadSaveDZDN();
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  // Read user profile and save it to the userprofile std::string variable
  std::string userProfile = ConfigurationUtil::getUserProfilePath();

  // Load standard settings
  std::vector<std::string> standardSettings = lsd->LoadDefaultDZDN();
  delete lsd;
  // Default parallel threads
  mParallelChecked = false;
  mDefaultThreads = 20;
  mUserThreads = 20;
  mDispatcher->defaultThreads = 20;
  mDispatcher->userThreads = 20;
  int ptnu = 0;
  // Default Sysnative Results
  mSysnativeResultsChecked = 1;
  mSysnativeResults = gcnew System::String(std::string(userProfile + "\\SysnativeResults").c_str());
  mSysnativeResultsBuilder->sysnativeResultsS = mSysnativeResults;
  // Default output order
  System::Array::Resize(mFileList, 7);
  mFileList[0] = "stack";
  mFileList[1] = "_97-debug";
  mFileList[2] = "_98-debug";
  mFileList[3] = "template";
  mFileList[4] = "_88-debug";
  mFileList[5] = "_99-debug";
  mFileList[6] = "_95-debug";
  for(size_t i = 0; i < standardSettings.size(); ++i)
  {
    std::string inString;
    inString = standardSettings[i];
    if(inString.find("## ###parallelThreading_nu") != std::string::npos)
    {
      int cb[3];
      int j = 0;
      while(j < 3)
      {
        i++;
        inString = standardSettings[i];
        std::stringstream sstemp;
        sstemp << inString;
        sstemp >> cb[j];
        j++;
      }
      mParallelChecked = static_cast<bool>(cb[0]);
      mDispatcher->defaultThreads = cb[1];
      mDispatcher->userThreads = cb[2];
    }
    if(inString.find("## ###sysnativeResults_cb") != std::string::npos)
    {
      int cb;
      i++;
      inString = standardSettings[i];
      std::stringstream sstemp;
      sstemp << inString;
      sstemp >> cb;
      mSysnativeResultsChecked = static_cast<bool>(cb);
    }
    if(inString.find("## ###sysnativeResults_s") != std::string::npos)
    {
      i++;
      inString = standardSettings[i];
      mSysnativeResults = gcnew System::String(inString.c_str());
      mSysnativeResultsBuilder->sysnativeResultsS = mSysnativeResults;
    }
    if(inString.find("## ###outputOrder_list") != std::string::npos)
    {
      ++i;
      inString = standardSettings[i];
      int oos = 0;
      while(i < standardSettings.size())
      {
        if(standardSettings[i].find("## ###") != std::string::npos)
        {
          break;
        }
        inString = standardSettings[i];
        if(inString.length() > 0)
        {
          System::Array::Resize(mFileList, oos+1);
          mFileList[oos] = gcnew System::String(inString.c_str());
          oos++;
        }
        ++i;
      }
      --i;
    }
    if(inString.find("## ###kdPaths") != std::string::npos)
    {
      ++i;
      inString = standardSettings[i];
      int kds = 0;
      while(i < standardSettings.size())
      {
        if(standardSettings[i].find("## ###") != std::string::npos)
        {
          break;
        }
        inString = standardSettings[i];
        if(inString.length() > 0)
        {
          System::Array::Resize(mSavedKdPaths,kds+1);
          mSavedKdPaths[kds] = gcnew System::String(inString.c_str());
          ++kds;
        }
        ++i;
      }
      --i;
    }
    if (inString.find("## ###kdActive") != std::string::npos)
    {
        ++i;
        inString = standardSettings[i];
        int kds = 0;
        while (i < standardSettings.size())
        {
            if (standardSettings[i].find("## ###") != std::string::npos)
            {
                break;
            }
            inString = standardSettings[i];
            if (inString.length() > 0)
            {
                System::Array::Resize(mActiveKdPaths, kds + 1);
                mActiveKdPaths[kds] = std::stoi(inString);
                ++kds;
            }
            ++i;
        }
        --i;
    }
  }
  std::string tempPath = tempDir + "\\openingFile.txt";
  remove(tempPath.c_str());
  tempPath = tempDir + "\\openingFile2.txt";
  remove(tempPath.c_str());
  tempPath = userProfile + "\\SysnativeBSODApps\\backup\\tempFileName.txt";
  remove(tempPath.c_str());
  std::string inputString0;
  std::string inputCh0;

  std::string inChar;
  System::String^ str2;

  std::string DOParms = userProfile + "\\SysnativeBSODApps\\parms\\";
  std::string DODmpOptions = tempDir;

  DODmpOptions = userProfile + "\\SysnativeBSODApps\\dmpOptions\\";
  // this->Text = x.ToString();

  // read in the $un95 file
  std::string inPath = DOParms + "un95";
  std::ifstream infile;
  infile.open(inPath);
  if(infile.good())
  {
    getline(infile,inChar);
    infile.close();
    std::string un95 = inChar;
    str2 = gcnew System::String(un95.c_str());
  }
  else{
    str2 = "";
  }
  mUsername = str2;

  mOutputDir = L"outputDmps";

  // Create the output directory structure with the current
  //    date and time in its name
  std::string timeNow = ConfigurationUtil::currentDateTime();
  replace_if(timeNow.begin(),timeNow.end(),::ispunct,'_');
  std::string timeDir = timeNow.substr(0,timeNow.end()-timeNow.begin()-5);

  str2 = gcnew System::String(timeDir.c_str());
  mTimeDir = str2;

  mFullGui = 1;
  this->SafeInvoke(this, gcnew System::Action(this, &Form1::UpdateFirstScreenUI));
}

bool Form1::CheckDebugFile()
{
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  // Read user profile and save it to the userprofile std::string variable
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string dbugDir;
  std::string path3;
  if(mSavedKdPaths == nullptr || mSavedKdPaths->Length < 1)
  {// Check for kd paths
    std::string path = userProfile + "\\SysnativeBSODApps\\dmpOptions\\kdPaths.txt";
    std::ifstream infile(path);
    mSavedKdPaths = gcnew cli::array<System::String^>(0);
    mActiveKdPaths = gcnew cli::array<int>(0);
    if(infile.good())
    {
      int lineNum = 0;
      while(!infile.eof())
      {
        getline(infile,dbugDir);
        std::stringstream ssDbugDir;
        for(size_t i = 0; i < dbugDir.length(); ++i)
        {
          if(dbugDir[i] != '\"')
          {
            ssDbugDir << dbugDir[i];
          }
        }
        System::Array::Resize(mSavedKdPaths, mSavedKdPaths->Length + 1);
        System::Array::Resize(mActiveKdPaths, mActiveKdPaths->Length + 1);
        mSavedKdPaths[lineNum] = gcnew System::String(ssDbugDir.str().c_str());
        mActiveKdPaths[lineNum] = 0;
        ++lineNum;
      }
    }
    path3 = userProfile + "\\SysnativeBSODApps\\parms\\$_dbugdir";
    std::ifstream infile3(path3);
    if(infile3.good())
    {
      getline(infile3,dbugDir);
    }
    infile3.close();
    std::stringstream sstemp2;
    for(int i = 0; i < int(dbugDir.length()); i++){
      if(dbugDir.c_str()[i] != '\"'){
        sstemp2 << dbugDir.c_str()[i];
      }
    }
    dbugDir = sstemp2.str();
    for(int i = 0; i < mSavedKdPaths->Length; ++i)
    {
      if(dbugDir == SystemStandardConversionUtil::convertSystemStringToStdString(mSavedKdPaths[i]))
      {
        mActiveKdPaths[i] = 1;
      }
    }
  }
  else if (mActiveKdPaths != nullptr)
  {
    for(int i = 0; i < mActiveKdPaths->Length; ++i)
    {
      if(mActiveKdPaths[i] == 1)
      {
        if(mSavedKdPaths[i]->Length > 2)
        {
          dbugDir = SystemStandardConversionUtil::convertSystemStringToStdString(mSavedKdPaths[i]);
        }
      }
    }
  }
  std::ifstream inDbugDir(dbugDir);
  if(inDbugDir.good())
  {
    return true;
  }
  return false;
}

System::Void Form1::hideControlsForKdSearch()
{
    // Using CDPI example class
    CDPI g_metrics;
    int dpiX = g_metrics.GetDPIX();

    label29->Text = L"Waiting for User to Select kd.exe Path:\r\n Options->Windows->kd.exe Path(s)";
    label28->Visible = false;
    label27->Visible = false;
    label49->Visible = false;
    label48->Visible = false;
    label47->Visible = false;
    textBox11->Visible = false;
    textBox4->Visible = false;
    textBox29->Visible = false;
    textBox28->Visible = false;
    button2->Visible = false;
    button3->Visible = false;
    button4->Visible = false;
    label50->Visible = false;
    numericUpDown4->Visible = false;
    checkBox24->Visible = false;
    this->Width = label29->Location.X + label29->Width + 30;
    if (tabControl1->Enabled)
    {
        this->Height = checkBox4->Height + checkBox4->Location.Y + int(double(80) * double(dpiX) / double(96)) +
            label12->Height + int(double(60) * double(dpiX) / double(96));
    }
}

System::Void Form1::showControlsAfterKdSearch()
{
    int x = checkBox3->Location.X + checkBox3->Width + 150;
    if (!tabControl1->Enabled)
    {
        label28->Visible = true;
        label27->Visible = true;
        label49->Visible = true;
        label48->Visible = true;
        label47->Visible = true;
        textBox11->Visible = true;
        textBox4->Visible = true;
        textBox29->Visible = true;
        textBox28->Visible = true;
        button2->Visible = true;
        button3->Visible = true;
        button4->Visible = true;
        label50->Visible = true;
        checkBox24->Visible = true;
        numericUpDown4->Visible = true;
        label29->Text = L"Driver Reference Table Update Info";
        x = textBox11->Location.X + textBox11->Width + 30;
    }
    this->Width = x;
}

System::Void Form1::SafeInvoke(Control^ control, System::Action^ action)
{
    if (control != nullptr && !control->IsDisposed && control->InvokeRequired)
    {
        try
        {
            control->Invoke(action);
        }
        catch (System::ObjectDisposedException ^ excep)
        {
            LogUtil::logErrorAsync(excep->Message);
        }
    }
    else if (control != nullptr && !control->IsDisposed)
    {
        action();
    }
}

void Form1::checkForKD(System::ComponentModel::BackgroundWorker^ worker)
{
  std::string tempDir = ConfigurationUtil::getTempDirectory();
  // Read user profile and save it to the userprofile std::string variable
  std::string userProfile = ConfigurationUtil::getUserProfilePath();

  int foundPaths = 0;
  while(!foundPaths)
  {
    if ( worker->CancellationPending )
    {
      break;
    }
    if (CheckDebugFile())
    {
        break;
    }
    this->SafeInvoke(this, gcnew System::Action(this, &Form1::hideControlsForKdSearch));
    
    Sleep(105);  
  }
  this->SafeInvoke(this, gcnew System::Action(this, &Form1::showControlsAfterKdSearch));
  
}

System::Void Form1::UpdateWorker1UI()
{
    alwaysOnTopToolStripMenuItem->Checked = mTopMost;
    TopMost = mTopMost;
    closeAppsWhenFinishedToolStripMenuItem->Checked = mCloseWhenFinished;

    //Check if logging should be enabled
    loggingToolStripMenuItem->Checked = mLoggingChecked;
}

System::Void Form1::backgroundWorker1_DoWork(System::Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
{
  // Get the BackgroundWorker that raised this event.
  System::ComponentModel::BackgroundWorker^ worker = dynamic_cast<System::ComponentModel::BackgroundWorker^>(sender);

  std::string tempDir = ConfigurationUtil::getTempDirectory();
  // Read user profile and save it to the userprofile std::string variable
  System::String^ userProfile = SystemStandardConversionUtil::convertStdStringToSystemString(ConfigurationUtil::getUserProfilePath());

  FileSystemUtil::removeDirectoryAndContents(userProfile + "\\SysnativeBSODApps\\Logs");

  setupFirstScreen();

  int inTopi = 1;
  std::ifstream inTop(SystemStandardConversionUtil::convertSystemStringToStdString(userProfile) + "\\SysnativeBSODApps\\dmpOptions\\alwaysOnTop.txt");
  if(inTop.good())
  {
    inTop >> inTopi;
  }
  inTop.close();
  if(inTopi)
  {
    mTopMost = true;
  }
  else
  {
    mTopMost = false;
  }
  if(inTopi)
  {
    mFindPaths->onTop = 1;
    mDispatcher->onTop = 1;
  }

  int inCloseFinished = 0;
  std::ifstream inFinished(SystemStandardConversionUtil::convertSystemStringToStdString(userProfile) + "\\SysnativeBSODApps\\dmpOptions\\closeWhenFinished.txt");
  if(inFinished.good())
  {
    inFinished >> inCloseFinished;
  }
  inFinished.close();
  mCloseWhenFinished = static_cast<bool>(inCloseFinished);

  int notFound = 0;
  mFindPaths = gcnew FindPaths();
  mDispatcher = gcnew ParallelThreading();
  mSysnativeResultsBuilder = gcnew SetDirectorySysnativeResults();
  std::string dbugDir = "";
  
  if(mSavedKdPaths == nullptr || mSavedKdPaths->Length < 1)
  {// Check for kd path
    std::string path3 = SystemStandardConversionUtil::convertSystemStringToStdString(userProfile) + "\\SysnativeBSODApps\\parms\\$_dbugdir";
    std::ifstream infile3(path3);
    if(infile3.good())
    {
      getline(infile3,dbugDir);
    }
    infile3.close();
    std::stringstream sstemp2;
    for(int i = 0; i < int(dbugDir.length()); i++){
      if(dbugDir.c_str()[i] != '\"'){
        sstemp2 << dbugDir.c_str()[i];
      }
    }
    dbugDir = sstemp2.str();
  }
  else if (mActiveKdPaths != nullptr)
  {
    for(int i = 0; i < mActiveKdPaths->Length; ++i)
    {
      if(mActiveKdPaths[i] == 1)
      {
        if(mSavedKdPaths[i]->Length > 2)
        {
          dbugDir = SystemStandardConversionUtil::convertSystemStringToStdString(mSavedKdPaths[i]);
        }
      }
    }
  }
  std::ifstream inDbugDir(dbugDir);
  if(!inDbugDir.good())
  {
    notFound = 1;
  }
  if(notFound)
  {
    bool bTop = this->TopMost;
    if(bTop)
    {
      onChecked(sender, e);
    }
    mFindPaths->ShowDialog();
    if(bTop)
    {
      onChecked(sender, e);
    }
    if(mFindPaths->m_SAKdPaths != nullptr && mFindPaths->m_NAKdPaths != nullptr)
    {
      mSavedKdPaths = gcnew cli::array<System::String^>(mFindPaths->m_SAKdPaths->Length);
      mActiveKdPaths = gcnew cli::array<int>(mFindPaths->m_NAKdPaths->Length);
      mFindPaths->m_SAKdPaths->CopyTo(mSavedKdPaths, 0);
      mFindPaths->m_NAKdPaths->CopyTo(mActiveKdPaths, 0);
    }
    else
    {
      mFindPaths->m_SAKdPaths = nullptr;
      mFindPaths->m_NAKdPaths = nullptr;
    }
  }
  this->SafeInvoke(this, gcnew System::Action(this, &Form1::UpdateWorker1UI));

  checkForKD(worker);
}

// Determine whether to output the SysnativeResults directory and choose the path if it is output
System::Void Form1::sysnativeResultsDIrectoryToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  if(sysnativeResultsDIrectoryToolStripMenuItem->Checked)
  {
    mSysnativeResults = mSysnativeResultsBuilder->sysnativeResultsS;
    mSysnativeResultsBuilder = gcnew SetDirectorySysnativeResults();
    mSysnativeResultsChecked = true;
    setDirectoryToolStripMenuItem->Visible = true;
    mSysnativeResultsBuilder->currentPath(mSysnativeResults);
    bool bTop = this->TopMost;
    if(bTop)
    {
      onChecked(sender, e);
    }
    mSysnativeResultsBuilder->ShowDialog();
    if(bTop)
    {
      onChecked(sender, e);
    }
    mSysnativeResults = mSysnativeResultsBuilder->sysnativeResultsS;
  }
  else
  {
    mSysnativeResultsChecked = false;
    setDirectoryToolStripMenuItem->Visible = false;
  }
}

// Set up directory path for SysnativeResults
System::Void Form1::setDirectoryToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  sysnativeResultsDIrectoryToolStripMenuItem_Click(sender, e);
}

// Determine whether to enable logging
System::Void Form1::loggingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
    mLoggingChecked = loggingToolStripMenuItem->Checked;
}

// Update excluded drivers after Windows 8.l and Windows 10 excluded drivers are saved
System::Void Form1::refillExcludedDrivers()
{
  richTextBox6->Clear();
  for(int i = 0; i < mDriverNames->Length; i++)
  {
    if(i == mDriverNames->Length - 1)
    {
      if(i < 9)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}    " + mDriverNames[i];
        richTextBox6->AppendText(thisLine);
      }
      if(i >= 9 && i < 99)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}   " + mDriverNames[i];
        richTextBox6->AppendText(thisLine);
      }
      if(i >= 99 && i < 999)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}  " + mDriverNames[i];
        richTextBox6->AppendText(thisLine);
      }
    }
    else
    {
      if(i < 9)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}    " + mDriverNames[i] + "\n";
        richTextBox6->AppendText(thisLine);
      }
      if(i >= 9 && i < 99)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}   " + mDriverNames[i] + "\n";
        richTextBox6->AppendText(thisLine);
      }
      if(i >= 99 && i < 999)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}  " + mDriverNames[i] + "\n";
        richTextBox6->AppendText(thisLine);
      }
    }
  }
  richTextBox7->Clear();
  for(int i = 0; i < mDriverDates->Length; i++)
  {
    if(i == mDriverNames->Length - 1)
    {
      if(i < 9)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}    " + mDriverDates[i];
        richTextBox7->AppendText(thisLine);
      }
      if(i >= 9 && i < 99)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}   " + mDriverDates[i];
        richTextBox7->AppendText(thisLine);
      }
      if(i >= 99 && i < 999)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}  " + mDriverDates[i];
        richTextBox7->AppendText(thisLine);
      }
    }
    else
    {
      if(i < 9)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}    " + mDriverDates[i] + "\n";
        richTextBox7->AppendText(thisLine);
      }
      if(i >= 9 && i < 99)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}   " + mDriverDates[i] + "\n";
        richTextBox7->AppendText(thisLine);
      }
      if(i >= 99 && i < 999)
      {
        System::String^ thisLine = "{" + (i+1).ToString() + "}  " + mDriverDates[i] + "\n";
        richTextBox7->AppendText(thisLine);
      }
    }
  }
  for(int i = richTextBox10->Lines->Length; i < mDriverNames->Length; i++)
  {
    if(i < 9)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}    ";
      richTextBox10->AppendText(thisLine);
    }
    if(i >= 9 && i < 99)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}   ";
      richTextBox10->AppendText(thisLine);
    }
    if(i >= 99 && i < 999)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}  ";
      richTextBox10->AppendText(thisLine);
    }
  }
  for(int i = richTextBox11->Lines->Length; i < mDriverNames->Length; i++)
  {
    if(i < 9)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}    ";
      richTextBox11->AppendText(thisLine);
    }
    if(i >= 9 && i < 99)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}   ";
      richTextBox11->AppendText(thisLine);
    }
    if(i >= 99 && i < 999)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}  ";
      richTextBox11->AppendText(thisLine);
    }
  }
  for(int i = richTextBox12->Lines->Length; i < mDriverNames->Length; i++)
  {
    if(i < 9)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}    ";
      richTextBox12->AppendText(thisLine);
    }
    if(i >= 9 && i < 99)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}   ";
      richTextBox12->AppendText(thisLine);
    }
    if(i >= 99 && i < 999)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}  ";
      richTextBox12->AppendText(thisLine);
    }
  }
  for(int i = richTextBox13->Lines->Length; i < mDriverNames->Length; i++)
  {
    if(i < 9)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}    ";
      richTextBox13->AppendText(thisLine);
    }
    if(i >= 9 && i < 99)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}   ";
      richTextBox13->AppendText(thisLine);
    }
    if(i >= 99 && i < 999)
    {
      System::String^ thisLine = "\n" + "{" + (i+1).ToString() + "}  ";
      richTextBox13->AppendText(thisLine);
    }
  }
}

// Open Windows 8.1 & 10 window

System::Void Form1::button11_Click(System::Object^  sender, System::EventArgs^  e)
{
  std::string userProfile = ConfigurationUtil::getUserProfilePath();
  std::string tempDir = userProfile + "\\SysnativeBSODApps\\dmpOptions";

  m_nSavedED = 0;
  SysnativeBSODApps::Windows_8_1_and_10^ recentWindows;
  recentWindows = gcnew SysnativeBSODApps::Windows_8_1_and_10();
  recentWindows->tempStringS = gcnew System::String(tempDir.c_str());
  recentWindows->m_SADriverNames = richTextBox6->Lines;
  recentWindows->m_SADriverDates = richTextBox7->Lines;
  recentWindows->m_SAXPED = richTextBox10->Lines;
  recentWindows->m_SAVistaED = richTextBox11->Lines;
  recentWindows->m_SASevenED = richTextBox12->Lines;
  recentWindows->m_SAEightED = richTextBox13->Lines;
  recentWindows->ShowDialog();
  mDriverNames = recentWindows->m_SADriverNames;
  mDriverDates = recentWindows->m_SADriverDates;
  m_nSavedED = recentWindows->m_nSavedED;

  if(mDriverNames && mDriverDates && m_nSavedED)
  {
    richTextBox10->Lines = recentWindows->m_SAXPED;
    richTextBox11->Lines = recentWindows->m_SAVistaED;
    richTextBox12->Lines = recentWindows->m_SASevenED;
    richTextBox13->Lines = recentWindows->m_SAEightED;
    refillExcludedDrivers();
  }
}

System::Void Form1::button12_Click(System::Object^  sender, System::EventArgs^  e)
{
  button11_Click(sender,e);
}

System::Void Form1::kdexePathsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
  bool bTop = this->TopMost;
  if(bTop)
  {
    onChecked(sender, e);
  }
  mKdPaths = gcnew KDPaths();
  if(mLoggingChecked)
  {
    mKdPaths->EnableLogging();
  }
  if(mActiveKdPaths != nullptr && mSavedKdPaths != nullptr)
  {
	cli::array<int>^ nAKdPaths = gcnew cli::array<int>(mActiveKdPaths->Length);
	cli::array<System::String^>^ sAKdPaths = gcnew cli::array<System::String^>(mSavedKdPaths->Length);
    mSavedKdPaths->CopyTo(sAKdPaths, 0);
    mActiveKdPaths->CopyTo(nAKdPaths, 0);
    mKdPaths->SetKdPaths(nAKdPaths, sAKdPaths);
  }
  else
  {
    mKdPaths->SetKdPaths(nullptr, nullptr);
  }
  mKdPaths->ShowDialog();
  if(mKdPaths->GetKdPaths() != nullptr && mKdPaths->GetKdActive() != nullptr)
  {
    mSavedKdPaths = gcnew cli::array<System::String^>(mKdPaths->GetKdPaths()->Length);
    mActiveKdPaths = gcnew cli::array<int>(mKdPaths->GetKdActive()->Length);
    mKdPaths->GetKdPaths()->CopyTo(mSavedKdPaths, 0);
    mKdPaths->GetKdActive()->CopyTo(mActiveKdPaths, 0);
  }
  if(bTop)
  {
    onChecked(sender, e);
  }
}
}// end namespace SysnativeBSODApps