#pragma once

#include <string>

class ConfigurationUtil
{
    public:
		static std::string getParmsDirectory();
        static std::string getUserProfilePath();
        static std::string currentDateTime();
        static std::string getTimeDirectory();
        static std::string getLogDirectory();
        static std::string getCurrentDirectory();

        static void setTempDirectory(const std::string& tempDir);
        static std::string getTempDirectory();

        static void setOutputDirectory(const std::string& outputDir);
        static std::string getOutputDirectory();

    private:
		static std::string parmsDirectory;
		static std::string outputDirectory;
        static std::string tempDirectory;
		static std::string userProfilePath;
        static std::string timeDirectory;
};