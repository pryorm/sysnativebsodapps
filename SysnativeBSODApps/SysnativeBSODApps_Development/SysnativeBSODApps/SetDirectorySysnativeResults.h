

#include "debug.h"

//#include <vld.h>

#pragma once

namespace SysnativeBSODApps {

  /// <summary>
  /// Summary for SetDirectorySysnativeResults
  /// </summary>
  public ref class SetDirectorySysnativeResults : public System::Windows::Forms::Form
  {
  public:
    static System::String^ sysnativeResultsS;
    SetDirectorySysnativeResults(void)
    {
      InitializeComponent();
      //
      //TODO: Add the constructor code here
      //
    }

  protected:
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    ~SetDirectorySysnativeResults()
    {
      if (components)
      {
        delete components;
      }
    }
  private: System::Windows::Forms::Button^  button1;
  protected: 
  private: System::Windows::Forms::TextBox^  textBox1;
  private: System::Windows::Forms::Button^  button2;
  private: System::Windows::Forms::Button^  button3;
  private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog1;

  private:
    /// <summary>
    /// Required designer variable.
    /// </summary>
    System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    void InitializeComponent(void)
    {
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(SetDirectorySysnativeResults::typeid));
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->textBox1 = (gcnew System::Windows::Forms::TextBox());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->button3 = (gcnew System::Windows::Forms::Button());
      this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
      this->SuspendLayout();
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(327, 21);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(75, 23);
      this->button1->TabIndex = 0;
      this->button1->Text = L"Browse";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &SetDirectorySysnativeResults::button1_Click);
      // 
      // textBox1
      // 
      this->textBox1->Location = System::Drawing::Point(12, 23);
      this->textBox1->Name = L"textBox1";
      this->textBox1->Size = System::Drawing::Size(309, 20);
      this->textBox1->TabIndex = 1;
      // 
      // button2
      // 
      this->button2->Location = System::Drawing::Point(246, 76);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(75, 23);
      this->button2->TabIndex = 2;
      this->button2->Text = L"OK";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &SetDirectorySysnativeResults::button2_Click);
      // 
      // button3
      // 
      this->button3->Location = System::Drawing::Point(327, 76);
      this->button3->Name = L"button3";
      this->button3->Size = System::Drawing::Size(75, 23);
      this->button3->TabIndex = 3;
      this->button3->Text = L"Cancel";
      this->button3->UseVisualStyleBackColor = true;
      this->button3->Click += gcnew System::EventHandler(this, &SetDirectorySysnativeResults::button3_Click);
      // 
      // SetDirectorySysnativeResults
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(410, 111);
      this->Controls->Add(this->button3);
      this->Controls->Add(this->button2);
      this->Controls->Add(this->textBox1);
      this->Controls->Add(this->button1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Name = L"SetDirectorySysnativeResults";
      this->Text = L"Set Directory SysnativeResults";
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
  public: void currentPath(System::String^ curPath);
  private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
  private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e);
};
}
