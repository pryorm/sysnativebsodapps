#pragma once

namespace SysnativeBSODApps {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ChangeSettings
	/// </summary>
	public ref class ChangeSettings : public System::Windows::Forms::Form
	{
	public:
    String^ tempStringS;
		ChangeSettings(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ChangeSettings()
		{
			if (components)
			{
				delete components;
			}
		}
  private: System::Windows::Forms::Button^  button9;
  protected: 
  private: System::Windows::Forms::Label^  label12;

  private: System::Windows::Forms::MenuStrip^  menuStrip1;
  private: System::Windows::Forms::ToolStripMenuItem^  pressF1ForHelpToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  loadToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  saveAsToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  loadPreviousToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  quickSaveToolStripMenuItem;
  private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
  private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  alwaysOnTopToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  useParallelThreadingToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  changeThreadsToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sysnativeResultsDIrectoryToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  setDirectoryToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  loggingToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  saveAndRunToolStripMenuItem1;
  private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
  private: System::Windows::Forms::TabControl^  tabControl1;
  private: System::Windows::Forms::TabPage^  tabPage1;
  private: System::Windows::Forms::TextBox^  textBox2;
  private: System::Windows::Forms::Label^  label6;
  private: System::Windows::Forms::TextBox^  textBox1;
  private: System::Windows::Forms::Label^  label5;
  private: System::Windows::Forms::Label^  label4;
  private: System::Windows::Forms::CheckBox^  checkBox7;
  private: System::Windows::Forms::CheckBox^  checkBox6;
  private: System::Windows::Forms::CheckBox^  checkBox5;
  private: System::Windows::Forms::CheckBox^  checkBox4;
  private: System::Windows::Forms::CheckBox^  checkBox3;
  private: System::Windows::Forms::CheckBox^  checkBox2;
  private: System::Windows::Forms::CheckBox^  checkBox1;
  private: System::Windows::Forms::TextBox^  textBox10;
  private: System::Windows::Forms::Label^  label22;
  private: System::Windows::Forms::TabControl^  tabControl2;
  private: System::Windows::Forms::TabPage^  tabPage3;
  private: System::Windows::Forms::TabPage^  tabPage4;
  private: System::Windows::Forms::Button^  button10;
  private: System::Windows::Forms::ComboBox^  comboBox1;
  private: System::Windows::Forms::Button^  button8;
  private: System::Windows::Forms::Label^  label9;
  private: System::Windows::Forms::TextBox^  textBox3;
  private: System::Windows::Forms::Label^  label7;
  private: System::Windows::Forms::Label^  label3;
  private: System::Windows::Forms::CheckedListBox^  checkedListBox3;
  private: System::Windows::Forms::Label^  label2;
  private: System::Windows::Forms::Label^  label1;
  private: System::Windows::Forms::CheckedListBox^  checkedListBox2;
  private: System::Windows::Forms::CheckedListBox^  checkedListBox1;
  private: System::Windows::Forms::TabPage^  tabPage13;
  private: System::Windows::Forms::Label^  label34;
  private: System::Windows::Forms::Label^  label35;
  private: System::Windows::Forms::Label^  label36;
  private: System::Windows::Forms::TextBox^  textBox18;
  private: System::Windows::Forms::TextBox^  textBox19;
  private: System::Windows::Forms::TextBox^  textBox20;
  private: System::Windows::Forms::Label^  label37;
  private: System::Windows::Forms::Label^  label38;
  private: System::Windows::Forms::Label^  label39;
  private: System::Windows::Forms::Label^  label40;
  private: System::Windows::Forms::TextBox^  textBox21;
  private: System::Windows::Forms::TextBox^  textBox22;
  private: System::Windows::Forms::TextBox^  textBox23;
  private: System::Windows::Forms::Label^  label41;
  private: System::Windows::Forms::Label^  label30;
  private: System::Windows::Forms::Label^  label31;
  private: System::Windows::Forms::Label^  label32;
  private: System::Windows::Forms::TextBox^  textBox15;
  private: System::Windows::Forms::TextBox^  textBox16;
  private: System::Windows::Forms::TextBox^  textBox17;
  private: System::Windows::Forms::Label^  label33;
  private: System::Windows::Forms::MenuStrip^  menuStrip2;
  private: System::Windows::Forms::ToolStripMenuItem^  xPDatesToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMAugToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP1Sep92002ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP2Aug252004ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP2bAug2006ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP2cAug102007ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP3Apr212008RTMToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  vistaDatesToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMNov82007ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  feb42008ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP2Apr282009RTMToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  datesToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMJul222009ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  sP1Feb112011ToolStripMenuItem;
  private: System::Windows::Forms::ToolStripMenuItem^  datesToolStripMenuItem1;
  private: System::Windows::Forms::ToolStripMenuItem^  rTMAugust12012ToolStripMenuItem;
  private: System::Windows::Forms::Label^  label21;
  private: System::Windows::Forms::Label^  label20;
  private: System::Windows::Forms::Label^  label19;
  private: System::Windows::Forms::TextBox^  textBox9;
  private: System::Windows::Forms::TextBox^  textBox8;
  private: System::Windows::Forms::TextBox^  textBox14;
  private: System::Windows::Forms::Label^  label8;
  private: System::Windows::Forms::TextBox^  textBox5;
  private: System::Windows::Forms::Label^  label10;
  private: System::Windows::Forms::TextBox^  textBox6;
  private: System::Windows::Forms::Label^  label11;
  private: System::Windows::Forms::TabPage^  tabPage10;
  private: System::Windows::Forms::CheckBox^  checkBox21;
  private: System::Windows::Forms::Button^  button5;
  private: System::Windows::Forms::CheckBox^  checkBox16;
  private: System::Windows::Forms::CheckBox^  checkBox15;
  private: System::Windows::Forms::CheckBox^  checkBox14;
  private: System::Windows::Forms::CheckBox^  checkBox13;
  private: System::Windows::Forms::TextBox^  textBox25;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
  private: System::Windows::Forms::CheckBox^  checkBox12;
  private: System::Windows::Forms::CheckBox^  checkBox11;
  private: System::Windows::Forms::TextBox^  textBox24;
  private: System::Windows::Forms::Label^  label45;
  private: System::Windows::Forms::RichTextBox^  richTextBox13;
  private: System::Windows::Forms::Label^  label44;
  private: System::Windows::Forms::RichTextBox^  richTextBox12;
  private: System::Windows::Forms::Label^  label43;
  private: System::Windows::Forms::RichTextBox^  richTextBox11;
  private: System::Windows::Forms::Label^  label42;
  private: System::Windows::Forms::RichTextBox^  richTextBox10;
  private: System::Windows::Forms::Label^  label24;
  private: System::Windows::Forms::Label^  label23;
  private: System::Windows::Forms::RichTextBox^  richTextBox7;
  private: System::Windows::Forms::RichTextBox^  richTextBox6;
  private: System::Windows::Forms::TabPage^  tabPage12;
  private: System::Windows::Forms::CheckBox^  checkBox22;
  private: System::Windows::Forms::Button^  button6;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown2;
  private: System::Windows::Forms::CheckBox^  checkBox17;
  private: System::Windows::Forms::CheckBox^  checkBox18;
  private: System::Windows::Forms::TextBox^  textBox26;
  private: System::Windows::Forms::CheckBox^  checkBox9;
  private: System::Windows::Forms::CheckBox^  checkBox8;
  private: System::Windows::Forms::Label^  label26;
  private: System::Windows::Forms::RichTextBox^  richTextBox9;
  private: System::Windows::Forms::TabPage^  tabPage11;
  private: System::Windows::Forms::CheckBox^  checkBox23;
  private: System::Windows::Forms::Button^  button7;
  private: System::Windows::Forms::NumericUpDown^  numericUpDown3;
  private: System::Windows::Forms::CheckBox^  checkBox19;
  private: System::Windows::Forms::CheckBox^  checkBox20;
  private: System::Windows::Forms::TextBox^  textBox27;
  private: System::Windows::Forms::Button^  button1;
  private: System::Windows::Forms::Label^  label25;
  private: System::Windows::Forms::RichTextBox^  richTextBox8;
  private: System::Windows::Forms::TabPage^  tabPage2;
  private: System::Windows::Forms::RichTextBox^  richTextBox1;
  private: System::Windows::Forms::Label^  label13;
  private: System::Windows::Forms::TabPage^  tabPage5;
  private: System::Windows::Forms::RichTextBox^  richTextBox2;
  private: System::Windows::Forms::Label^  label14;
  private: System::Windows::Forms::TabPage^  tabPage6;
  private: System::Windows::Forms::RichTextBox^  richTextBox3;
  private: System::Windows::Forms::Label^  label15;
  private: System::Windows::Forms::TabPage^  tabPage7;
  private: System::Windows::Forms::RichTextBox^  richTextBox4;
  private: System::Windows::Forms::Label^  label16;
  private: System::Windows::Forms::TabPage^  tabPage8;
  private: System::Windows::Forms::TextBox^  textBox13;
  private: System::Windows::Forms::TextBox^  textBox12;
  private: System::Windows::Forms::Label^  label18;
  private: System::Windows::Forms::Label^  label17;
  private: System::Windows::Forms::TabPage^  tabPage9;
  private: System::Windows::Forms::Label^  label46;
  private: System::Windows::Forms::RichTextBox^  richTextBox5;
private: System::Windows::Forms::CheckBox^  checkBox24;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ChangeSettings::typeid));
      this->button9 = (gcnew System::Windows::Forms::Button());
      this->label12 = (gcnew System::Windows::Forms::Label());
      this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
      this->pressF1ForHelpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->loadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->saveAsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->loadPreviousToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->quickSaveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
      this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->alwaysOnTopToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->useParallelThreadingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->changeThreadsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sysnativeResultsDIrectoryToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->setDirectoryToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->loggingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->saveAndRunToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
      this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
      this->textBox2 = (gcnew System::Windows::Forms::TextBox());
      this->label6 = (gcnew System::Windows::Forms::Label());
      this->textBox1 = (gcnew System::Windows::Forms::TextBox());
      this->label5 = (gcnew System::Windows::Forms::Label());
      this->label4 = (gcnew System::Windows::Forms::Label());
      this->checkBox7 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox6 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox5 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox4 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
      this->textBox10 = (gcnew System::Windows::Forms::TextBox());
      this->label22 = (gcnew System::Windows::Forms::Label());
      this->tabControl2 = (gcnew System::Windows::Forms::TabControl());
      this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
      this->tabPage4 = (gcnew System::Windows::Forms::TabPage());
      this->button10 = (gcnew System::Windows::Forms::Button());
      this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
      this->button8 = (gcnew System::Windows::Forms::Button());
      this->label9 = (gcnew System::Windows::Forms::Label());
      this->textBox3 = (gcnew System::Windows::Forms::TextBox());
      this->label7 = (gcnew System::Windows::Forms::Label());
      this->label3 = (gcnew System::Windows::Forms::Label());
      this->checkedListBox3 = (gcnew System::Windows::Forms::CheckedListBox());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->checkedListBox2 = (gcnew System::Windows::Forms::CheckedListBox());
      this->checkedListBox1 = (gcnew System::Windows::Forms::CheckedListBox());
      this->tabPage13 = (gcnew System::Windows::Forms::TabPage());
      this->label34 = (gcnew System::Windows::Forms::Label());
      this->label35 = (gcnew System::Windows::Forms::Label());
      this->label36 = (gcnew System::Windows::Forms::Label());
      this->textBox18 = (gcnew System::Windows::Forms::TextBox());
      this->textBox19 = (gcnew System::Windows::Forms::TextBox());
      this->textBox20 = (gcnew System::Windows::Forms::TextBox());
      this->label37 = (gcnew System::Windows::Forms::Label());
      this->label38 = (gcnew System::Windows::Forms::Label());
      this->label39 = (gcnew System::Windows::Forms::Label());
      this->label40 = (gcnew System::Windows::Forms::Label());
      this->textBox21 = (gcnew System::Windows::Forms::TextBox());
      this->textBox22 = (gcnew System::Windows::Forms::TextBox());
      this->textBox23 = (gcnew System::Windows::Forms::TextBox());
      this->label41 = (gcnew System::Windows::Forms::Label());
      this->label30 = (gcnew System::Windows::Forms::Label());
      this->label31 = (gcnew System::Windows::Forms::Label());
      this->label32 = (gcnew System::Windows::Forms::Label());
      this->textBox15 = (gcnew System::Windows::Forms::TextBox());
      this->textBox16 = (gcnew System::Windows::Forms::TextBox());
      this->textBox17 = (gcnew System::Windows::Forms::TextBox());
      this->label33 = (gcnew System::Windows::Forms::Label());
      this->menuStrip2 = (gcnew System::Windows::Forms::MenuStrip());
      this->xPDatesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->rTMAugToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sP1Sep92002ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sP2Aug252004ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sP2bAug2006ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sP2cAug102007ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sP3Apr212008RTMToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->vistaDatesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->rTMNov82007ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->feb42008ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sP2Apr282009RTMToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->datesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->rTMJul222009ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->sP1Feb112011ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->datesToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->rTMAugust12012ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
      this->label21 = (gcnew System::Windows::Forms::Label());
      this->label20 = (gcnew System::Windows::Forms::Label());
      this->label19 = (gcnew System::Windows::Forms::Label());
      this->textBox9 = (gcnew System::Windows::Forms::TextBox());
      this->textBox8 = (gcnew System::Windows::Forms::TextBox());
      this->textBox14 = (gcnew System::Windows::Forms::TextBox());
      this->label8 = (gcnew System::Windows::Forms::Label());
      this->textBox5 = (gcnew System::Windows::Forms::TextBox());
      this->label10 = (gcnew System::Windows::Forms::Label());
      this->textBox6 = (gcnew System::Windows::Forms::TextBox());
      this->label11 = (gcnew System::Windows::Forms::Label());
      this->tabPage10 = (gcnew System::Windows::Forms::TabPage());
      this->checkBox21 = (gcnew System::Windows::Forms::CheckBox());
      this->button5 = (gcnew System::Windows::Forms::Button());
      this->checkBox16 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox15 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox14 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox13 = (gcnew System::Windows::Forms::CheckBox());
      this->textBox25 = (gcnew System::Windows::Forms::TextBox());
      this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
      this->checkBox12 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox11 = (gcnew System::Windows::Forms::CheckBox());
      this->textBox24 = (gcnew System::Windows::Forms::TextBox());
      this->label45 = (gcnew System::Windows::Forms::Label());
      this->richTextBox13 = (gcnew System::Windows::Forms::RichTextBox());
      this->label44 = (gcnew System::Windows::Forms::Label());
      this->richTextBox12 = (gcnew System::Windows::Forms::RichTextBox());
      this->label43 = (gcnew System::Windows::Forms::Label());
      this->richTextBox11 = (gcnew System::Windows::Forms::RichTextBox());
      this->label42 = (gcnew System::Windows::Forms::Label());
      this->richTextBox10 = (gcnew System::Windows::Forms::RichTextBox());
      this->label24 = (gcnew System::Windows::Forms::Label());
      this->label23 = (gcnew System::Windows::Forms::Label());
      this->richTextBox7 = (gcnew System::Windows::Forms::RichTextBox());
      this->richTextBox6 = (gcnew System::Windows::Forms::RichTextBox());
      this->tabPage12 = (gcnew System::Windows::Forms::TabPage());
      this->checkBox22 = (gcnew System::Windows::Forms::CheckBox());
      this->button6 = (gcnew System::Windows::Forms::Button());
      this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
      this->checkBox17 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox18 = (gcnew System::Windows::Forms::CheckBox());
      this->textBox26 = (gcnew System::Windows::Forms::TextBox());
      this->checkBox9 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox8 = (gcnew System::Windows::Forms::CheckBox());
      this->label26 = (gcnew System::Windows::Forms::Label());
      this->richTextBox9 = (gcnew System::Windows::Forms::RichTextBox());
      this->tabPage11 = (gcnew System::Windows::Forms::TabPage());
      this->checkBox23 = (gcnew System::Windows::Forms::CheckBox());
      this->button7 = (gcnew System::Windows::Forms::Button());
      this->numericUpDown3 = (gcnew System::Windows::Forms::NumericUpDown());
      this->checkBox19 = (gcnew System::Windows::Forms::CheckBox());
      this->checkBox20 = (gcnew System::Windows::Forms::CheckBox());
      this->textBox27 = (gcnew System::Windows::Forms::TextBox());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->label25 = (gcnew System::Windows::Forms::Label());
      this->richTextBox8 = (gcnew System::Windows::Forms::RichTextBox());
      this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
      this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
      this->label13 = (gcnew System::Windows::Forms::Label());
      this->tabPage5 = (gcnew System::Windows::Forms::TabPage());
      this->richTextBox2 = (gcnew System::Windows::Forms::RichTextBox());
      this->label14 = (gcnew System::Windows::Forms::Label());
      this->tabPage6 = (gcnew System::Windows::Forms::TabPage());
      this->richTextBox3 = (gcnew System::Windows::Forms::RichTextBox());
      this->label15 = (gcnew System::Windows::Forms::Label());
      this->tabPage7 = (gcnew System::Windows::Forms::TabPage());
      this->richTextBox4 = (gcnew System::Windows::Forms::RichTextBox());
      this->label16 = (gcnew System::Windows::Forms::Label());
      this->tabPage8 = (gcnew System::Windows::Forms::TabPage());
      this->textBox13 = (gcnew System::Windows::Forms::TextBox());
      this->textBox12 = (gcnew System::Windows::Forms::TextBox());
      this->label18 = (gcnew System::Windows::Forms::Label());
      this->label17 = (gcnew System::Windows::Forms::Label());
      this->tabPage9 = (gcnew System::Windows::Forms::TabPage());
      this->label46 = (gcnew System::Windows::Forms::Label());
      this->richTextBox5 = (gcnew System::Windows::Forms::RichTextBox());
      this->checkBox24 = (gcnew System::Windows::Forms::CheckBox());
      this->menuStrip1->SuspendLayout();
      this->tabControl1->SuspendLayout();
      this->tabPage1->SuspendLayout();
      this->tabControl2->SuspendLayout();
      this->tabPage4->SuspendLayout();
      this->tabPage13->SuspendLayout();
      this->menuStrip2->SuspendLayout();
      this->tabPage10->SuspendLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->BeginInit();
      this->tabPage12->SuspendLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown2))->BeginInit();
      this->tabPage11->SuspendLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown3))->BeginInit();
      this->tabPage2->SuspendLayout();
      this->tabPage5->SuspendLayout();
      this->tabPage6->SuspendLayout();
      this->tabPage7->SuspendLayout();
      this->tabPage8->SuspendLayout();
      this->tabPage9->SuspendLayout();
      this->SuspendLayout();
      // 
      // button9
      // 
      this->button9->Location = System::Drawing::Point(344, 1);
      this->button9->Name = L"button9";
      this->button9->Size = System::Drawing::Size(143, 21);
      this->button9->TabIndex = 118;
      this->button9->Text = L"Save Forum .zdn";
      this->button9->UseVisualStyleBackColor = true;
      // 
      // label12
      // 
      this->label12->AutoSize = true;
      this->label12->Location = System::Drawing::Point(8, 485);
      this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label12->Name = L"label12";
      this->label12->Size = System::Drawing::Size(107, 13);
      this->label12->TabIndex = 117;
      this->label12->Text = L" � 2013 Mikael Pryor";
      // 
      // menuStrip1
      // 
      this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->pressF1ForHelpToolStripMenuItem, 
        this->optionsToolStripMenuItem, this->saveAndRunToolStripMenuItem1, this->helpToolStripMenuItem});
      this->menuStrip1->Location = System::Drawing::Point(0, 0);
      this->menuStrip1->Name = L"menuStrip1";
      this->menuStrip1->Padding = System::Windows::Forms::Padding(4, 2, 0, 2);
      this->menuStrip1->Size = System::Drawing::Size(1040, 24);
      this->menuStrip1->TabIndex = 115;
      this->menuStrip1->Text = L"menuStrip1";
      // 
      // pressF1ForHelpToolStripMenuItem
      // 
      this->pressF1ForHelpToolStripMenuItem->Checked = true;
      this->pressF1ForHelpToolStripMenuItem->CheckState = System::Windows::Forms::CheckState::Checked;
      this->pressF1ForHelpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {this->saveToolStripMenuItem, 
        this->loadToolStripMenuItem, this->saveAsToolStripMenuItem, this->loadPreviousToolStripMenuItem, this->quickSaveToolStripMenuItem, 
        this->toolStripSeparator1, this->exitToolStripMenuItem});
      this->pressF1ForHelpToolStripMenuItem->Name = L"pressF1ForHelpToolStripMenuItem";
      this->pressF1ForHelpToolStripMenuItem->Size = System::Drawing::Size(37, 20);
      this->pressF1ForHelpToolStripMenuItem->Text = L"File";
      // 
      // saveToolStripMenuItem
      // 
      this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
      this->saveToolStripMenuItem->Size = System::Drawing::Size(206, 22);
      this->saveToolStripMenuItem->Text = L"Open";
      // 
      // loadToolStripMenuItem
      // 
      this->loadToolStripMenuItem->Name = L"loadToolStripMenuItem";
      this->loadToolStripMenuItem->Size = System::Drawing::Size(206, 22);
      this->loadToolStripMenuItem->Text = L"Save";
      // 
      // saveAsToolStripMenuItem
      // 
      this->saveAsToolStripMenuItem->Name = L"saveAsToolStripMenuItem";
      this->saveAsToolStripMenuItem->Size = System::Drawing::Size(206, 22);
      this->saveAsToolStripMenuItem->Text = L"Save As";
      // 
      // loadPreviousToolStripMenuItem
      // 
      this->loadPreviousToolStripMenuItem->Name = L"loadPreviousToolStripMenuItem";
      this->loadPreviousToolStripMenuItem->Size = System::Drawing::Size(206, 22);
      this->loadPreviousToolStripMenuItem->Text = L"Revert to Last Quick Save";
      // 
      // quickSaveToolStripMenuItem
      // 
      this->quickSaveToolStripMenuItem->Name = L"quickSaveToolStripMenuItem";
      this->quickSaveToolStripMenuItem->Size = System::Drawing::Size(206, 22);
      this->quickSaveToolStripMenuItem->Text = L"Quick Save";
      // 
      // toolStripSeparator1
      // 
      this->toolStripSeparator1->Name = L"toolStripSeparator1";
      this->toolStripSeparator1->Size = System::Drawing::Size(203, 6);
      // 
      // exitToolStripMenuItem
      // 
      this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
      this->exitToolStripMenuItem->Size = System::Drawing::Size(206, 22);
      this->exitToolStripMenuItem->Text = L"Exit";
      // 
      // optionsToolStripMenuItem
      // 
      this->optionsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->alwaysOnTopToolStripMenuItem, 
        this->useParallelThreadingToolStripMenuItem, this->sysnativeResultsDIrectoryToolStripMenuItem, this->loggingToolStripMenuItem});
      this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
      this->optionsToolStripMenuItem->Size = System::Drawing::Size(61, 20);
      this->optionsToolStripMenuItem->Text = L"Options";
      // 
      // alwaysOnTopToolStripMenuItem
      // 
      this->alwaysOnTopToolStripMenuItem->Name = L"alwaysOnTopToolStripMenuItem";
      this->alwaysOnTopToolStripMenuItem->Size = System::Drawing::Size(252, 22);
      this->alwaysOnTopToolStripMenuItem->Text = L"Always On Top";
      // 
      // useParallelThreadingToolStripMenuItem
      // 
      this->useParallelThreadingToolStripMenuItem->CheckOnClick = true;
      this->useParallelThreadingToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->changeThreadsToolStripMenuItem});
      this->useParallelThreadingToolStripMenuItem->Name = L"useParallelThreadingToolStripMenuItem";
      this->useParallelThreadingToolStripMenuItem->Size = System::Drawing::Size(252, 22);
      this->useParallelThreadingToolStripMenuItem->Text = L"Use Parallel Threading";
      // 
      // changeThreadsToolStripMenuItem
      // 
      this->changeThreadsToolStripMenuItem->Name = L"changeThreadsToolStripMenuItem";
      this->changeThreadsToolStripMenuItem->Size = System::Drawing::Size(201, 22);
      this->changeThreadsToolStripMenuItem->Text = L"ChangeNumberThreads";
      // 
      // sysnativeResultsDIrectoryToolStripMenuItem
      // 
      this->sysnativeResultsDIrectoryToolStripMenuItem->CheckOnClick = true;
      this->sysnativeResultsDIrectoryToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->setDirectoryToolStripMenuItem});
      this->sysnativeResultsDIrectoryToolStripMenuItem->Name = L"sysnativeResultsDIrectoryToolStripMenuItem";
      this->sysnativeResultsDIrectoryToolStripMenuItem->Size = System::Drawing::Size(252, 22);
      this->sysnativeResultsDIrectoryToolStripMenuItem->Text = L"Output SysnativeResults Directory";
      // 
      // setDirectoryToolStripMenuItem
      // 
      this->setDirectoryToolStripMenuItem->Name = L"setDirectoryToolStripMenuItem";
      this->setDirectoryToolStripMenuItem->Size = System::Drawing::Size(141, 22);
      this->setDirectoryToolStripMenuItem->Text = L"Set Directory";
      // 
      // loggingToolStripMenuItem
      // 
      this->loggingToolStripMenuItem->CheckOnClick = true;
      this->loggingToolStripMenuItem->Name = L"loggingToolStripMenuItem";
      this->loggingToolStripMenuItem->Size = System::Drawing::Size(252, 22);
      this->loggingToolStripMenuItem->Text = L"Logging";
      // 
      // saveAndRunToolStripMenuItem1
      // 
      this->saveAndRunToolStripMenuItem1->Name = L"saveAndRunToolStripMenuItem1";
      this->saveAndRunToolStripMenuItem1->Size = System::Drawing::Size(90, 20);
      this->saveAndRunToolStripMenuItem1->Text = L"Save and Run";
      // 
      // helpToolStripMenuItem
      // 
      this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
      this->helpToolStripMenuItem->Size = System::Drawing::Size(44, 20);
      this->helpToolStripMenuItem->Text = L"Help";
      // 
      // tabControl1
      // 
      this->tabControl1->Controls->Add(this->tabPage1);
      this->tabControl1->Controls->Add(this->tabPage13);
      this->tabControl1->Controls->Add(this->tabPage10);
      this->tabControl1->Controls->Add(this->tabPage12);
      this->tabControl1->Controls->Add(this->tabPage11);
      this->tabControl1->Controls->Add(this->tabPage2);
      this->tabControl1->Controls->Add(this->tabPage5);
      this->tabControl1->Controls->Add(this->tabPage6);
      this->tabControl1->Controls->Add(this->tabPage7);
      this->tabControl1->Controls->Add(this->tabPage8);
      this->tabControl1->Controls->Add(this->tabPage9);
      this->tabControl1->Enabled = false;
      this->tabControl1->Location = System::Drawing::Point(7, 26);
      this->tabControl1->Margin = System::Windows::Forms::Padding(2);
      this->tabControl1->Name = L"tabControl1";
      this->tabControl1->SelectedIndex = 0;
      this->tabControl1->Size = System::Drawing::Size(1024, 451);
      this->tabControl1->TabIndex = 114;
      this->tabControl1->Visible = false;
      // 
      // tabPage1
      // 
      this->tabPage1->AutoScroll = true;
      this->tabPage1->Controls->Add(this->textBox2);
      this->tabPage1->Controls->Add(this->label6);
      this->tabPage1->Controls->Add(this->textBox1);
      this->tabPage1->Controls->Add(this->label5);
      this->tabPage1->Controls->Add(this->label4);
      this->tabPage1->Controls->Add(this->checkBox7);
      this->tabPage1->Controls->Add(this->checkBox6);
      this->tabPage1->Controls->Add(this->checkBox5);
      this->tabPage1->Controls->Add(this->checkBox4);
      this->tabPage1->Controls->Add(this->checkBox3);
      this->tabPage1->Controls->Add(this->checkBox2);
      this->tabPage1->Controls->Add(this->checkBox1);
      this->tabPage1->Controls->Add(this->textBox10);
      this->tabPage1->Controls->Add(this->label22);
      this->tabPage1->Controls->Add(this->tabControl2);
      this->tabPage1->Controls->Add(this->label9);
      this->tabPage1->Controls->Add(this->textBox3);
      this->tabPage1->Controls->Add(this->label7);
      this->tabPage1->Controls->Add(this->label3);
      this->tabPage1->Controls->Add(this->checkedListBox3);
      this->tabPage1->Controls->Add(this->label2);
      this->tabPage1->Controls->Add(this->label1);
      this->tabPage1->Controls->Add(this->checkedListBox2);
      this->tabPage1->Controls->Add(this->checkedListBox1);
      this->tabPage1->Location = System::Drawing::Point(4, 22);
      this->tabPage1->Margin = System::Windows::Forms::Padding(2);
      this->tabPage1->Name = L"tabPage1";
      this->tabPage1->Padding = System::Windows::Forms::Padding(2);
      this->tabPage1->Size = System::Drawing::Size(1016, 425);
      this->tabPage1->TabIndex = 0;
      this->tabPage1->Text = L"Analysis Options";
      this->tabPage1->UseVisualStyleBackColor = true;
      // 
      // textBox2
      // 
      this->textBox2->Location = System::Drawing::Point(127, 399);
      this->textBox2->Margin = System::Windows::Forms::Padding(2);
      this->textBox2->Name = L"textBox2";
      this->textBox2->Size = System::Drawing::Size(182, 20);
      this->textBox2->TabIndex = 112;
      // 
      // label6
      // 
      this->label6->AutoSize = true;
      this->label6->BackColor = System::Drawing::SystemColors::Window;
      this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label6->Location = System::Drawing::Point(12, 404);
      this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label6->Name = L"label6";
      this->label6->Size = System::Drawing::Size(101, 13);
      this->label6->TabIndex = 111;
      this->label6->Text = L"Originating Post:";
      // 
      // textBox1
      // 
      this->textBox1->Location = System::Drawing::Point(127, 375);
      this->textBox1->Margin = System::Windows::Forms::Padding(2);
      this->textBox1->Name = L"textBox1";
      this->textBox1->Size = System::Drawing::Size(182, 20);
      this->textBox1->TabIndex = 110;
      // 
      // label5
      // 
      this->label5->AutoSize = true;
      this->label5->BackColor = System::Drawing::SystemColors::Window;
      this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label5->Location = System::Drawing::Point(12, 378);
      this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label5->Name = L"label5";
      this->label5->Size = System::Drawing::Size(67, 13);
      this->label5->TabIndex = 109;
      this->label5->Text = L"Username:";
      // 
      // label4
      // 
      this->label4->AutoSize = true;
      this->label4->BackColor = System::Drawing::SystemColors::Window;
      this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label4->ForeColor = System::Drawing::SystemColors::Desktop;
      this->label4->Location = System::Drawing::Point(41, 352);
      this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label4->Name = L"label4";
      this->label4->Size = System::Drawing::Size(267, 17);
      this->label4->TabIndex = 108;
      this->label4->Text = L"Driver Reference Table Update Info";
      // 
      // checkBox7
      // 
      this->checkBox7->AutoSize = true;
      this->checkBox7->Location = System::Drawing::Point(512, 9);
      this->checkBox7->Margin = System::Windows::Forms::Padding(2);
      this->checkBox7->Name = L"checkBox7";
      this->checkBox7->Size = System::Drawing::Size(15, 14);
      this->checkBox7->TabIndex = 80;
      this->checkBox7->UseVisualStyleBackColor = true;
      // 
      // checkBox6
      // 
      this->checkBox6->AutoSize = true;
      this->checkBox6->Location = System::Drawing::Point(176, 8);
      this->checkBox6->Margin = System::Windows::Forms::Padding(2);
      this->checkBox6->Name = L"checkBox6";
      this->checkBox6->Size = System::Drawing::Size(15, 14);
      this->checkBox6->TabIndex = 79;
      this->checkBox6->UseVisualStyleBackColor = true;
      // 
      // checkBox5
      // 
      this->checkBox5->AutoSize = true;
      this->checkBox5->Location = System::Drawing::Point(15, 9);
      this->checkBox5->Margin = System::Windows::Forms::Padding(2);
      this->checkBox5->Name = L"checkBox5";
      this->checkBox5->Size = System::Drawing::Size(15, 14);
      this->checkBox5->TabIndex = 29;
      this->checkBox5->UseVisualStyleBackColor = true;
      // 
      // checkBox4
      // 
      this->checkBox4->AutoSize = true;
      this->checkBox4->Location = System::Drawing::Point(658, 403);
      this->checkBox4->Margin = System::Windows::Forms::Padding(2);
      this->checkBox4->Name = L"checkBox4";
      this->checkBox4->Size = System::Drawing::Size(225, 17);
      this->checkBox4->TabIndex = 78;
      this->checkBox4->Text = L"Online only if symbols are WRONG (faster)";
      this->checkBox4->UseVisualStyleBackColor = true;
      // 
      // checkBox3
      // 
      this->checkBox3->AutoSize = true;
      this->checkBox3->Location = System::Drawing::Point(658, 383);
      this->checkBox3->Margin = System::Windows::Forms::Padding(2);
      this->checkBox3->Name = L"checkBox3";
      this->checkBox3->Size = System::Drawing::Size(231, 17);
      this->checkBox3->TabIndex = 77;
      this->checkBox3->Text = L"Online for module warnings (robust symbols)";
      this->checkBox3->UseVisualStyleBackColor = true;
      // 
      // checkBox2
      // 
      this->checkBox2->AutoSize = true;
      this->checkBox2->Location = System::Drawing::Point(777, 316);
      this->checkBox2->Margin = System::Windows::Forms::Padding(2);
      this->checkBox2->Name = L"checkBox2";
      this->checkBox2->Size = System::Drawing::Size(15, 14);
      this->checkBox2->TabIndex = 76;
      this->checkBox2->UseVisualStyleBackColor = true;
      // 
      // checkBox1
      // 
      this->checkBox1->AutoSize = true;
      this->checkBox1->Location = System::Drawing::Point(777, 291);
      this->checkBox1->Margin = System::Windows::Forms::Padding(2);
      this->checkBox1->Name = L"checkBox1";
      this->checkBox1->Size = System::Drawing::Size(15, 14);
      this->checkBox1->TabIndex = 75;
      this->checkBox1->UseVisualStyleBackColor = true;
      // 
      // textBox10
      // 
      this->textBox10->Location = System::Drawing::Point(621, 313);
      this->textBox10->Margin = System::Windows::Forms::Padding(2);
      this->textBox10->Name = L"textBox10";
      this->textBox10->Size = System::Drawing::Size(143, 20);
      this->textBox10->TabIndex = 74;
      // 
      // label22
      // 
      this->label22->AutoSize = true;
      this->label22->BackColor = System::Drawing::SystemColors::Window;
      this->label22->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label22->Location = System::Drawing::Point(507, 317);
      this->label22->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label22->Name = L"label22";
      this->label22->Size = System::Drawing::Size(97, 13);
      this->label22->TabIndex = 73;
      this->label22->Text = L"Online Symbols:";
      // 
      // tabControl2
      // 
      this->tabControl2->Controls->Add(this->tabPage3);
      this->tabControl2->Controls->Add(this->tabPage4);
      this->tabControl2->Location = System::Drawing::Point(14, 282);
      this->tabControl2->Margin = System::Windows::Forms::Padding(2);
      this->tabControl2->Name = L"tabControl2";
      this->tabControl2->SelectedIndex = 0;
      this->tabControl2->Size = System::Drawing::Size(459, 68);
      this->tabControl2->TabIndex = 65;
      // 
      // tabPage3
      // 
      this->tabPage3->Location = System::Drawing::Point(4, 22);
      this->tabPage3->Margin = System::Windows::Forms::Padding(2);
      this->tabPage3->Name = L"tabPage3";
      this->tabPage3->Padding = System::Windows::Forms::Padding(2);
      this->tabPage3->Size = System::Drawing::Size(451, 42);
      this->tabPage3->TabIndex = 0;
      this->tabPage3->Text = L" ";
      this->tabPage3->UseVisualStyleBackColor = true;
      // 
      // tabPage4
      // 
      this->tabPage4->Controls->Add(this->button10);
      this->tabPage4->Controls->Add(this->comboBox1);
      this->tabPage4->Controls->Add(this->button8);
      this->tabPage4->Location = System::Drawing::Point(4, 22);
      this->tabPage4->Margin = System::Windows::Forms::Padding(2);
      this->tabPage4->Name = L"tabPage4";
      this->tabPage4->Padding = System::Windows::Forms::Padding(2);
      this->tabPage4->Size = System::Drawing::Size(451, 42);
      this->tabPage4->TabIndex = 1;
      this->tabPage4->Text = L"kd.exe Path";
      this->tabPage4->UseVisualStyleBackColor = true;
      // 
      // button10
      // 
      this->button10->Location = System::Drawing::Point(355, 12);
      this->button10->Name = L"button10";
      this->button10->Size = System::Drawing::Size(91, 23);
      this->button10->TabIndex = 3;
      this->button10->Text = L"Clear List";
      this->button10->UseVisualStyleBackColor = true;
      // 
      // comboBox1
      // 
      this->comboBox1->DropDownWidth = 500;
      this->comboBox1->FormattingEnabled = true;
      this->comboBox1->Location = System::Drawing::Point(7, 12);
      this->comboBox1->Name = L"comboBox1";
      this->comboBox1->Size = System::Drawing::Size(292, 21);
      this->comboBox1->TabIndex = 2;
      // 
      // button8
      // 
      this->button8->Location = System::Drawing::Point(304, 12);
      this->button8->Name = L"button8";
      this->button8->Size = System::Drawing::Size(40, 23);
      this->button8->TabIndex = 1;
      this->button8->Text = L"...";
      this->button8->UseVisualStyleBackColor = true;
      // 
      // label9
      // 
      this->label9->AutoSize = true;
      this->label9->BackColor = System::Drawing::SystemColors::Window;
      this->label9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label9->Location = System::Drawing::Point(507, 394);
      this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label9->Name = L"label9";
      this->label9->Size = System::Drawing::Size(139, 13);
      this->label9->TabIndex = 59;
      this->label9->Text = L"Local Symbols Options:";
      // 
      // textBox3
      // 
      this->textBox3->Location = System::Drawing::Point(621, 290);
      this->textBox3->Margin = System::Windows::Forms::Padding(2);
      this->textBox3->Name = L"textBox3";
      this->textBox3->Size = System::Drawing::Size(143, 20);
      this->textBox3->TabIndex = 58;
      // 
      // label7
      // 
      this->label7->AutoSize = true;
      this->label7->BackColor = System::Drawing::SystemColors::Window;
      this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label7->Location = System::Drawing::Point(507, 294);
      this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label7->Name = L"label7";
      this->label7->Size = System::Drawing::Size(92, 13);
      this->label7->TabIndex = 57;
      this->label7->Text = L"Local Symbols:";
      // 
      // label3
      // 
      this->label3->AutoSize = true;
      this->label3->BackColor = System::Drawing::SystemColors::Window;
      this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label3->ForeColor = System::Drawing::SystemColors::Desktop;
      this->label3->Location = System::Drawing::Point(529, 8);
      this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label3->Name = L"label3";
      this->label3->Size = System::Drawing::Size(285, 17);
      this->label3->TabIndex = 51;
      this->label3->Text = L"Formatting and Miscellaneous Options";
      // 
      // checkedListBox3
      // 
      this->checkedListBox3->CheckOnClick = true;
      this->checkedListBox3->FormattingEnabled = true;
      this->checkedListBox3->Items->AddRange(gcnew cli::array< System::Object^  >(7) {L"Add spaces between lines in importantInfo.txt", 
        L"No BBCode in importantInfo.txt", L"Turn Off BBCode Parsing in HTML Viewers", L"Turn off all BBCode", L"Turn off BBCode in Code Boxes", 
        L"Old drivers shown in red", L"Treat all files as possible .dmps (more thorough, but slower)"});
      this->checkedListBox3->Location = System::Drawing::Point(510, 24);
      this->checkedListBox3->Margin = System::Windows::Forms::Padding(2);
      this->checkedListBox3->Name = L"checkedListBox3";
      this->checkedListBox3->Size = System::Drawing::Size(342, 214);
      this->checkedListBox3->TabIndex = 50;
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->BackColor = System::Drawing::SystemColors::Window;
      this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label2->ForeColor = System::Drawing::SystemColors::Desktop;
      this->label2->Location = System::Drawing::Point(199, 8);
      this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(118, 17);
      this->label2->TabIndex = 49;
      this->label2->Text = L"Output Options";
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->BackColor = System::Drawing::SystemColors::Window;
      this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label1->ForeColor = System::Drawing::SystemColors::Desktop;
      this->label1->Location = System::Drawing::Point(44, 8);
      this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(102, 17);
      this->label1->TabIndex = 48;
      this->label1->Text = L"Files to Save";
      // 
      // checkedListBox2
      // 
      this->checkedListBox2->CheckOnClick = true;
      this->checkedListBox2->FormattingEnabled = true;
      this->checkedListBox2->Items->AddRange(gcnew cli::array< System::Object^  >(28) {L"Loading Dump File", L"Kernel Version", 
        L"Missing Service Pack", L"Built by:", L"Debug session time:", L"System Uptime:", L"BugCheck ", L"*** WARNING:", L"*** ERROR:  ", 
        L"Probably caused by", L"DEFAULT_BUCKET_ID:", L"VERIFIER", L"BUGCHECK_STR:", L"Bugcheck Info", L"PROCESS_NAME:", L"FAILURE_BUCKET_ID:", 
        L"Bugcheck code ", L"Error Code", L"DISK_HARDWARE_ERROR", L"Arguments ", L"CPUID", L"MaxSpeed:", L"CurrentSpeed:", L"BiosVersion =", 
        L"BiosReleaseDate =", L"SystemManufacturer = ", L"SystemProductName = ", L"Overclock Ratio:"});
      this->checkedListBox2->Location = System::Drawing::Point(174, 24);
      this->checkedListBox2->Margin = System::Windows::Forms::Padding(2);
      this->checkedListBox2->Name = L"checkedListBox2";
      this->checkedListBox2->Size = System::Drawing::Size(156, 214);
      this->checkedListBox2->TabIndex = 47;
      // 
      // checkedListBox1
      // 
      this->checkedListBox1->CheckOnClick = true;
      this->checkedListBox1->FormattingEnabled = true;
      this->checkedListBox1->Items->AddRange(gcnew cli::array< System::Object^  >(15) {L"dmps.txt", L"drivers.txt", L"3rdPartyDriversName.txt", 
        L"3rdPartyDriversDate.txt", L"importantInfo.txt", L"stack.txt", L"missingFromDRT.txt", L"unloadedDrivers.txt", L"SMBIOS.txt", 
        L"template.txt", L"_99-debug.txt", L"_98-debug.txt", L"_97-debug.txt", L"_95-debug.txt", L"_88-debug.txt"});
      this->checkedListBox1->Location = System::Drawing::Point(14, 24);
      this->checkedListBox1->Margin = System::Windows::Forms::Padding(2);
      this->checkedListBox1->Name = L"checkedListBox1";
      this->checkedListBox1->Size = System::Drawing::Size(155, 214);
      this->checkedListBox1->TabIndex = 46;
      // 
      // tabPage13
      // 
      this->tabPage13->AutoScroll = true;
      this->tabPage13->Controls->Add(this->label34);
      this->tabPage13->Controls->Add(this->label35);
      this->tabPage13->Controls->Add(this->label36);
      this->tabPage13->Controls->Add(this->textBox18);
      this->tabPage13->Controls->Add(this->textBox19);
      this->tabPage13->Controls->Add(this->textBox20);
      this->tabPage13->Controls->Add(this->label37);
      this->tabPage13->Controls->Add(this->label38);
      this->tabPage13->Controls->Add(this->label39);
      this->tabPage13->Controls->Add(this->label40);
      this->tabPage13->Controls->Add(this->textBox21);
      this->tabPage13->Controls->Add(this->textBox22);
      this->tabPage13->Controls->Add(this->textBox23);
      this->tabPage13->Controls->Add(this->label41);
      this->tabPage13->Controls->Add(this->label30);
      this->tabPage13->Controls->Add(this->label31);
      this->tabPage13->Controls->Add(this->label32);
      this->tabPage13->Controls->Add(this->textBox15);
      this->tabPage13->Controls->Add(this->textBox16);
      this->tabPage13->Controls->Add(this->textBox17);
      this->tabPage13->Controls->Add(this->label33);
      this->tabPage13->Controls->Add(this->menuStrip2);
      this->tabPage13->Controls->Add(this->label21);
      this->tabPage13->Controls->Add(this->label20);
      this->tabPage13->Controls->Add(this->label19);
      this->tabPage13->Controls->Add(this->textBox9);
      this->tabPage13->Controls->Add(this->textBox8);
      this->tabPage13->Controls->Add(this->textBox14);
      this->tabPage13->Controls->Add(this->label8);
      this->tabPage13->Controls->Add(this->textBox5);
      this->tabPage13->Controls->Add(this->label10);
      this->tabPage13->Controls->Add(this->textBox6);
      this->tabPage13->Controls->Add(this->label11);
      this->tabPage13->Location = System::Drawing::Point(4, 22);
      this->tabPage13->Name = L"tabPage13";
      this->tabPage13->Padding = System::Windows::Forms::Padding(3);
      this->tabPage13->Size = System::Drawing::Size(1016, 425);
      this->tabPage13->TabIndex = 10;
      this->tabPage13->Text = L"Old Driver After";
      this->tabPage13->UseVisualStyleBackColor = true;
      // 
      // label34
      // 
      this->label34->AutoSize = true;
      this->label34->Location = System::Drawing::Point(496, 72);
      this->label34->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label34->Name = L"label34";
      this->label34->Size = System::Drawing::Size(29, 13);
      this->label34->TabIndex = 139;
      this->label34->Text = L"Year";
      // 
      // label35
      // 
      this->label35->AutoSize = true;
      this->label35->Location = System::Drawing::Point(468, 72);
      this->label35->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label35->Name = L"label35";
      this->label35->Size = System::Drawing::Size(26, 13);
      this->label35->TabIndex = 138;
      this->label35->Text = L"Day";
      // 
      // label36
      // 
      this->label36->AutoSize = true;
      this->label36->Location = System::Drawing::Point(426, 72);
      this->label36->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label36->Name = L"label36";
      this->label36->Size = System::Drawing::Size(37, 13);
      this->label36->TabIndex = 137;
      this->label36->Text = L"Month";
      // 
      // textBox18
      // 
      this->textBox18->Location = System::Drawing::Point(428, 87);
      this->textBox18->Margin = System::Windows::Forms::Padding(2);
      this->textBox18->Name = L"textBox18";
      this->textBox18->Size = System::Drawing::Size(37, 20);
      this->textBox18->TabIndex = 136;
      // 
      // textBox19
      // 
      this->textBox19->Location = System::Drawing::Point(471, 87);
      this->textBox19->Margin = System::Windows::Forms::Padding(2);
      this->textBox19->Name = L"textBox19";
      this->textBox19->Size = System::Drawing::Size(26, 20);
      this->textBox19->TabIndex = 135;
      // 
      // textBox20
      // 
      this->textBox20->Location = System::Drawing::Point(500, 87);
      this->textBox20->Margin = System::Windows::Forms::Padding(2);
      this->textBox20->Name = L"textBox20";
      this->textBox20->Size = System::Drawing::Size(43, 20);
      this->textBox20->TabIndex = 134;
      // 
      // label37
      // 
      this->label37->AutoSize = true;
      this->label37->BackColor = System::Drawing::SystemColors::Window;
      this->label37->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label37->Location = System::Drawing::Point(222, 90);
      this->label37->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label37->Name = L"label37";
      this->label37->Size = System::Drawing::Size(174, 13);
      this->label37->TabIndex = 133;
      this->label37->Text = L"Windows XP Old Driver After:";
      // 
      // label38
      // 
      this->label38->AutoSize = true;
      this->label38->Location = System::Drawing::Point(496, 112);
      this->label38->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label38->Name = L"label38";
      this->label38->Size = System::Drawing::Size(29, 13);
      this->label38->TabIndex = 132;
      this->label38->Text = L"Year";
      // 
      // label39
      // 
      this->label39->AutoSize = true;
      this->label39->Location = System::Drawing::Point(468, 112);
      this->label39->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label39->Name = L"label39";
      this->label39->Size = System::Drawing::Size(26, 13);
      this->label39->TabIndex = 131;
      this->label39->Text = L"Day";
      // 
      // label40
      // 
      this->label40->AutoSize = true;
      this->label40->Location = System::Drawing::Point(426, 112);
      this->label40->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label40->Name = L"label40";
      this->label40->Size = System::Drawing::Size(37, 13);
      this->label40->TabIndex = 130;
      this->label40->Text = L"Month";
      // 
      // textBox21
      // 
      this->textBox21->Location = System::Drawing::Point(428, 127);
      this->textBox21->Margin = System::Windows::Forms::Padding(2);
      this->textBox21->Name = L"textBox21";
      this->textBox21->Size = System::Drawing::Size(37, 20);
      this->textBox21->TabIndex = 129;
      // 
      // textBox22
      // 
      this->textBox22->Location = System::Drawing::Point(471, 127);
      this->textBox22->Margin = System::Windows::Forms::Padding(2);
      this->textBox22->Name = L"textBox22";
      this->textBox22->Size = System::Drawing::Size(26, 20);
      this->textBox22->TabIndex = 128;
      // 
      // textBox23
      // 
      this->textBox23->Location = System::Drawing::Point(500, 127);
      this->textBox23->Margin = System::Windows::Forms::Padding(2);
      this->textBox23->Name = L"textBox23";
      this->textBox23->Size = System::Drawing::Size(43, 20);
      this->textBox23->TabIndex = 127;
      // 
      // label41
      // 
      this->label41->AutoSize = true;
      this->label41->BackColor = System::Drawing::SystemColors::Window;
      this->label41->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label41->Location = System::Drawing::Point(222, 129);
      this->label41->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label41->Name = L"label41";
      this->label41->Size = System::Drawing::Size(186, 13);
      this->label41->TabIndex = 126;
      this->label41->Text = L"Windows Vista Old Driver After:";
      // 
      // label30
      // 
      this->label30->AutoSize = true;
      this->label30->Location = System::Drawing::Point(497, 194);
      this->label30->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label30->Name = L"label30";
      this->label30->Size = System::Drawing::Size(29, 13);
      this->label30->TabIndex = 125;
      this->label30->Text = L"Year";
      // 
      // label31
      // 
      this->label31->AutoSize = true;
      this->label31->Location = System::Drawing::Point(468, 194);
      this->label31->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label31->Name = L"label31";
      this->label31->Size = System::Drawing::Size(26, 13);
      this->label31->TabIndex = 124;
      this->label31->Text = L"Day";
      // 
      // label32
      // 
      this->label32->AutoSize = true;
      this->label32->Location = System::Drawing::Point(427, 194);
      this->label32->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label32->Name = L"label32";
      this->label32->Size = System::Drawing::Size(37, 13);
      this->label32->TabIndex = 123;
      this->label32->Text = L"Month";
      // 
      // textBox15
      // 
      this->textBox15->Location = System::Drawing::Point(428, 211);
      this->textBox15->Margin = System::Windows::Forms::Padding(2);
      this->textBox15->Name = L"textBox15";
      this->textBox15->Size = System::Drawing::Size(37, 20);
      this->textBox15->TabIndex = 122;
      // 
      // textBox16
      // 
      this->textBox16->Location = System::Drawing::Point(471, 211);
      this->textBox16->Margin = System::Windows::Forms::Padding(2);
      this->textBox16->Name = L"textBox16";
      this->textBox16->Size = System::Drawing::Size(26, 20);
      this->textBox16->TabIndex = 121;
      // 
      // textBox17
      // 
      this->textBox17->Location = System::Drawing::Point(500, 211);
      this->textBox17->Margin = System::Windows::Forms::Padding(2);
      this->textBox17->Name = L"textBox17";
      this->textBox17->Size = System::Drawing::Size(43, 20);
      this->textBox17->TabIndex = 120;
      // 
      // label33
      // 
      this->label33->AutoSize = true;
      this->label33->BackColor = System::Drawing::SystemColors::Window;
      this->label33->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label33->Location = System::Drawing::Point(222, 213);
      this->label33->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label33->Name = L"label33";
      this->label33->Size = System::Drawing::Size(165, 13);
      this->label33->TabIndex = 119;
      this->label33->Text = L"Windows 8 Old Driver After:";
      // 
      // menuStrip2
      // 
      this->menuStrip2->Dock = System::Windows::Forms::DockStyle::None;
      this->menuStrip2->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->xPDatesToolStripMenuItem, 
        this->vistaDatesToolStripMenuItem, this->datesToolStripMenuItem, this->datesToolStripMenuItem1});
      this->menuStrip2->Location = System::Drawing::Point(234, 40);
      this->menuStrip2->Name = L"menuStrip2";
      this->menuStrip2->Padding = System::Windows::Forms::Padding(4, 2, 0, 2);
      this->menuStrip2->Size = System::Drawing::Size(261, 24);
      this->menuStrip2->TabIndex = 102;
      this->menuStrip2->Text = L"menuStrip2";
      // 
      // xPDatesToolStripMenuItem
      // 
      this->xPDatesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {this->rTMAugToolStripMenuItem, 
        this->sP1Sep92002ToolStripMenuItem, this->sP2Aug252004ToolStripMenuItem, this->sP2bAug2006ToolStripMenuItem, this->sP2cAug102007ToolStripMenuItem, 
        this->sP3Apr212008RTMToolStripMenuItem});
      this->xPDatesToolStripMenuItem->Name = L"xPDatesToolStripMenuItem";
      this->xPDatesToolStripMenuItem->Size = System::Drawing::Size(65, 20);
      this->xPDatesToolStripMenuItem->Text = L"XP Dates";
      // 
      // rTMAugToolStripMenuItem
      // 
      this->rTMAugToolStripMenuItem->Name = L"rTMAugToolStripMenuItem";
      this->rTMAugToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->rTMAugToolStripMenuItem->Text = L"RTM: Aug. 24, 2001";
      // 
      // sP1Sep92002ToolStripMenuItem
      // 
      this->sP1Sep92002ToolStripMenuItem->Name = L"sP1Sep92002ToolStripMenuItem";
      this->sP1Sep92002ToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->sP1Sep92002ToolStripMenuItem->Text = L"SP1: Sep. 9, 2002";
      // 
      // sP2Aug252004ToolStripMenuItem
      // 
      this->sP2Aug252004ToolStripMenuItem->Name = L"sP2Aug252004ToolStripMenuItem";
      this->sP2Aug252004ToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->sP2Aug252004ToolStripMenuItem->Text = L"SP2: Aug. 25, 2004";
      // 
      // sP2bAug2006ToolStripMenuItem
      // 
      this->sP2bAug2006ToolStripMenuItem->Name = L"sP2bAug2006ToolStripMenuItem";
      this->sP2bAug2006ToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->sP2bAug2006ToolStripMenuItem->Text = L"SP2b: Aug. 2006";
      // 
      // sP2cAug102007ToolStripMenuItem
      // 
      this->sP2cAug102007ToolStripMenuItem->Name = L"sP2cAug102007ToolStripMenuItem";
      this->sP2cAug102007ToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->sP2cAug102007ToolStripMenuItem->Text = L"SP2c: Aug. 10, 2007";
      // 
      // sP3Apr212008RTMToolStripMenuItem
      // 
      this->sP3Apr212008RTMToolStripMenuItem->Name = L"sP3Apr212008RTMToolStripMenuItem";
      this->sP3Apr212008RTMToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->sP3Apr212008RTMToolStripMenuItem->Text = L"SP3: Apr. 21, 2008 (RTM)";
      // 
      // vistaDatesToolStripMenuItem
      // 
      this->vistaDatesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->rTMNov82007ToolStripMenuItem, 
        this->feb42008ToolStripMenuItem, this->sP2Apr282009RTMToolStripMenuItem});
      this->vistaDatesToolStripMenuItem->Name = L"vistaDatesToolStripMenuItem";
      this->vistaDatesToolStripMenuItem->Size = System::Drawing::Size(76, 20);
      this->vistaDatesToolStripMenuItem->Text = L"Vista Dates";
      // 
      // rTMNov82007ToolStripMenuItem
      // 
      this->rTMNov82007ToolStripMenuItem->Name = L"rTMNov82007ToolStripMenuItem";
      this->rTMNov82007ToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->rTMNov82007ToolStripMenuItem->Text = L"RTM: Nov. 8, 2006 \?";
      // 
      // feb42008ToolStripMenuItem
      // 
      this->feb42008ToolStripMenuItem->Name = L"feb42008ToolStripMenuItem";
      this->feb42008ToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->feb42008ToolStripMenuItem->Text = L"SP1: Feb. 4, 2008";
      // 
      // sP2Apr282009RTMToolStripMenuItem
      // 
      this->sP2Apr282009RTMToolStripMenuItem->Name = L"sP2Apr282009RTMToolStripMenuItem";
      this->sP2Apr282009RTMToolStripMenuItem->Size = System::Drawing::Size(202, 22);
      this->sP2Apr282009RTMToolStripMenuItem->Text = L"SP2: Apr. 28, 2009 (RTM)";
      // 
      // datesToolStripMenuItem
      // 
      this->datesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->rTMJul222009ToolStripMenuItem, 
        this->sP1Feb112011ToolStripMenuItem});
      this->datesToolStripMenuItem->Name = L"datesToolStripMenuItem";
      this->datesToolStripMenuItem->Size = System::Drawing::Size(57, 20);
      this->datesToolStripMenuItem->Text = L"7 Dates";
      // 
      // rTMJul222009ToolStripMenuItem
      // 
      this->rTMJul222009ToolStripMenuItem->Name = L"rTMJul222009ToolStripMenuItem";
      this->rTMJul222009ToolStripMenuItem->Size = System::Drawing::Size(167, 22);
      this->rTMJul222009ToolStripMenuItem->Text = L"RTM: Jul. 22, 2009";
      // 
      // sP1Feb112011ToolStripMenuItem
      // 
      this->sP1Feb112011ToolStripMenuItem->Name = L"sP1Feb112011ToolStripMenuItem";
      this->sP1Feb112011ToolStripMenuItem->Size = System::Drawing::Size(167, 22);
      this->sP1Feb112011ToolStripMenuItem->Text = L"SP1: Feb. 9, 2011";
      // 
      // datesToolStripMenuItem1
      // 
      this->datesToolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->rTMAugust12012ToolStripMenuItem});
      this->datesToolStripMenuItem1->Name = L"datesToolStripMenuItem1";
      this->datesToolStripMenuItem1->Size = System::Drawing::Size(57, 20);
      this->datesToolStripMenuItem1->Text = L"8 Dates";
      // 
      // rTMAugust12012ToolStripMenuItem
      // 
      this->rTMAugust12012ToolStripMenuItem->Name = L"rTMAugust12012ToolStripMenuItem";
      this->rTMAugust12012ToolStripMenuItem->Size = System::Drawing::Size(169, 22);
      this->rTMAugust12012ToolStripMenuItem->Text = L"RTM: Aug. 1, 2012";
      // 
      // label21
      // 
      this->label21->AutoSize = true;
      this->label21->Location = System::Drawing::Point(497, 155);
      this->label21->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label21->Name = L"label21";
      this->label21->Size = System::Drawing::Size(29, 13);
      this->label21->TabIndex = 118;
      this->label21->Text = L"Year";
      // 
      // label20
      // 
      this->label20->AutoSize = true;
      this->label20->Location = System::Drawing::Point(468, 155);
      this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label20->Name = L"label20";
      this->label20->Size = System::Drawing::Size(26, 13);
      this->label20->TabIndex = 117;
      this->label20->Text = L"Day";
      // 
      // label19
      // 
      this->label19->AutoSize = true;
      this->label19->Location = System::Drawing::Point(427, 155);
      this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label19->Name = L"label19";
      this->label19->Size = System::Drawing::Size(37, 13);
      this->label19->TabIndex = 116;
      this->label19->Text = L"Month";
      // 
      // textBox9
      // 
      this->textBox9->Location = System::Drawing::Point(428, 170);
      this->textBox9->Margin = System::Windows::Forms::Padding(2);
      this->textBox9->Name = L"textBox9";
      this->textBox9->Size = System::Drawing::Size(37, 20);
      this->textBox9->TabIndex = 115;
      // 
      // textBox8
      // 
      this->textBox8->Location = System::Drawing::Point(471, 170);
      this->textBox8->Margin = System::Windows::Forms::Padding(2);
      this->textBox8->Name = L"textBox8";
      this->textBox8->Size = System::Drawing::Size(26, 20);
      this->textBox8->TabIndex = 114;
      // 
      // textBox14
      // 
      this->textBox14->Location = System::Drawing::Point(500, 170);
      this->textBox14->Margin = System::Windows::Forms::Padding(2);
      this->textBox14->Name = L"textBox14";
      this->textBox14->Size = System::Drawing::Size(43, 20);
      this->textBox14->TabIndex = 113;
      // 
      // label8
      // 
      this->label8->AutoSize = true;
      this->label8->BackColor = System::Drawing::SystemColors::Window;
      this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label8->Location = System::Drawing::Point(222, 174);
      this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label8->Name = L"label8";
      this->label8->Size = System::Drawing::Size(165, 13);
      this->label8->TabIndex = 112;
      this->label8->Text = L"Windows 7 Old Driver After:";
      // 
      // textBox5
      // 
      this->textBox5->Location = System::Drawing::Point(336, 270);
      this->textBox5->Margin = System::Windows::Forms::Padding(2);
      this->textBox5->Name = L"textBox5";
      this->textBox5->Size = System::Drawing::Size(206, 20);
      this->textBox5->TabIndex = 111;
      // 
      // label10
      // 
      this->label10->AutoSize = true;
      this->label10->BackColor = System::Drawing::SystemColors::Window;
      this->label10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label10->Location = System::Drawing::Point(222, 274);
      this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label10->Name = L"label10";
      this->label10->Size = System::Drawing::Size(115, 13);
      this->label10->TabIndex = 110;
      this->label10->Text = L"Link Seen by User:";
      // 
      // textBox6
      // 
      this->textBox6->Location = System::Drawing::Point(336, 248);
      this->textBox6->Margin = System::Windows::Forms::Padding(2);
      this->textBox6->Name = L"textBox6";
      this->textBox6->Size = System::Drawing::Size(206, 20);
      this->textBox6->TabIndex = 109;
      // 
      // label11
      // 
      this->label11->AutoSize = true;
      this->label11->BackColor = System::Drawing::SystemColors::Window;
      this->label11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label11->Location = System::Drawing::Point(222, 252);
      this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label11->Name = L"label11";
      this->label11->Size = System::Drawing::Size(66, 13);
      this->label11->TabIndex = 108;
      this->label11->Text = L"URL Path:";
      // 
      // tabPage10
      // 
      this->tabPage10->AutoScroll = true;
      this->tabPage10->Controls->Add(this->checkBox21);
      this->tabPage10->Controls->Add(this->button5);
      this->tabPage10->Controls->Add(this->checkBox16);
      this->tabPage10->Controls->Add(this->checkBox15);
      this->tabPage10->Controls->Add(this->checkBox14);
      this->tabPage10->Controls->Add(this->checkBox13);
      this->tabPage10->Controls->Add(this->textBox25);
      this->tabPage10->Controls->Add(this->numericUpDown1);
      this->tabPage10->Controls->Add(this->checkBox12);
      this->tabPage10->Controls->Add(this->checkBox11);
      this->tabPage10->Controls->Add(this->textBox24);
      this->tabPage10->Controls->Add(this->label45);
      this->tabPage10->Controls->Add(this->richTextBox13);
      this->tabPage10->Controls->Add(this->label44);
      this->tabPage10->Controls->Add(this->richTextBox12);
      this->tabPage10->Controls->Add(this->label43);
      this->tabPage10->Controls->Add(this->richTextBox11);
      this->tabPage10->Controls->Add(this->label42);
      this->tabPage10->Controls->Add(this->richTextBox10);
      this->tabPage10->Controls->Add(this->label24);
      this->tabPage10->Controls->Add(this->label23);
      this->tabPage10->Controls->Add(this->richTextBox7);
      this->tabPage10->Controls->Add(this->richTextBox6);
      this->tabPage10->Location = System::Drawing::Point(4, 22);
      this->tabPage10->Margin = System::Windows::Forms::Padding(2);
      this->tabPage10->Name = L"tabPage10";
      this->tabPage10->Padding = System::Windows::Forms::Padding(2);
      this->tabPage10->Size = System::Drawing::Size(1016, 425);
      this->tabPage10->TabIndex = 7;
      this->tabPage10->Text = L"Excluded Drivers";
      this->tabPage10->UseVisualStyleBackColor = true;
      // 
      // checkBox21
      // 
      this->checkBox21->AutoSize = true;
      this->checkBox21->Location = System::Drawing::Point(104, 17);
      this->checkBox21->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox21->Name = L"checkBox21";
      this->checkBox21->Size = System::Drawing::Size(57, 17);
      this->checkBox21->TabIndex = 80;
      this->checkBox21->Text = L"Delete";
      this->checkBox21->UseVisualStyleBackColor = true;
      // 
      // button5
      // 
      this->button5->Location = System::Drawing::Point(306, 15);
      this->button5->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->button5->Name = L"button5";
      this->button5->Size = System::Drawing::Size(75, 22);
      this->button5->TabIndex = 79;
      this->button5->Text = L"Proceed";
      this->button5->UseVisualStyleBackColor = true;
      // 
      // checkBox16
      // 
      this->checkBox16->AutoSize = true;
      this->checkBox16->Location = System::Drawing::Point(797, 59);
      this->checkBox16->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox16->Name = L"checkBox16";
      this->checkBox16->Size = System::Drawing::Size(15, 14);
      this->checkBox16->TabIndex = 22;
      this->checkBox16->UseVisualStyleBackColor = true;
      // 
      // checkBox15
      // 
      this->checkBox15->AutoSize = true;
      this->checkBox15->Location = System::Drawing::Point(717, 59);
      this->checkBox15->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox15->Name = L"checkBox15";
      this->checkBox15->Size = System::Drawing::Size(15, 14);
      this->checkBox15->TabIndex = 21;
      this->checkBox15->UseVisualStyleBackColor = true;
      // 
      // checkBox14
      // 
      this->checkBox14->AutoSize = true;
      this->checkBox14->Location = System::Drawing::Point(639, 59);
      this->checkBox14->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox14->Name = L"checkBox14";
      this->checkBox14->Size = System::Drawing::Size(15, 14);
      this->checkBox14->TabIndex = 20;
      this->checkBox14->UseVisualStyleBackColor = true;
      // 
      // checkBox13
      // 
      this->checkBox13->AutoSize = true;
      this->checkBox13->Location = System::Drawing::Point(553, 59);
      this->checkBox13->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox13->Name = L"checkBox13";
      this->checkBox13->Size = System::Drawing::Size(15, 14);
      this->checkBox13->TabIndex = 19;
      this->checkBox13->UseVisualStyleBackColor = true;
      // 
      // textBox25
      // 
      this->textBox25->Location = System::Drawing::Point(374, 56);
      this->textBox25->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->textBox25->Name = L"textBox25";
      this->textBox25->Size = System::Drawing::Size(148, 20);
      this->textBox25->TabIndex = 18;
      // 
      // numericUpDown1
      // 
      this->numericUpDown1->Enabled = false;
      this->numericUpDown1->Location = System::Drawing::Point(176, 15);
      this->numericUpDown1->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->numericUpDown1->Name = L"numericUpDown1";
      this->numericUpDown1->Size = System::Drawing::Size(120, 20);
      this->numericUpDown1->TabIndex = 17;
      // 
      // checkBox12
      // 
      this->checkBox12->AutoSize = true;
      this->checkBox12->Location = System::Drawing::Point(56, 17);
      this->checkBox12->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox12->Name = L"checkBox12";
      this->checkBox12->Size = System::Drawing::Size(44, 17);
      this->checkBox12->TabIndex = 16;
      this->checkBox12->Text = L"Edit";
      this->checkBox12->UseVisualStyleBackColor = true;
      // 
      // checkBox11
      // 
      this->checkBox11->AutoSize = true;
      this->checkBox11->Location = System::Drawing::Point(8, 17);
      this->checkBox11->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox11->Name = L"checkBox11";
      this->checkBox11->Size = System::Drawing::Size(45, 17);
      this->checkBox11->TabIndex = 15;
      this->checkBox11->Text = L"Add";
      this->checkBox11->UseVisualStyleBackColor = true;
      // 
      // textBox24
      // 
      this->textBox24->Location = System::Drawing::Point(8, 56);
      this->textBox24->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->textBox24->Name = L"textBox24";
      this->textBox24->Size = System::Drawing::Size(361, 20);
      this->textBox24->TabIndex = 14;
      // 
      // label45
      // 
      this->label45->AutoSize = true;
      this->label45->Location = System::Drawing::Point(796, 39);
      this->label45->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label45->Name = L"label45";
      this->label45->Size = System::Drawing::Size(13, 13);
      this->label45->TabIndex = 13;
      this->label45->Text = L"8";
      // 
      // richTextBox13
      // 
      this->richTextBox13->Location = System::Drawing::Point(765, 82);
      this->richTextBox13->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox13->Name = L"richTextBox13";
      this->richTextBox13->ReadOnly = true;
      this->richTextBox13->Size = System::Drawing::Size(78, 431);
      this->richTextBox13->TabIndex = 12;
      this->richTextBox13->Text = L"";
      this->richTextBox13->WordWrap = false;
      // 
      // label44
      // 
      this->label44->AutoSize = true;
      this->label44->Location = System::Drawing::Point(716, 39);
      this->label44->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label44->Name = L"label44";
      this->label44->Size = System::Drawing::Size(13, 13);
      this->label44->TabIndex = 11;
      this->label44->Text = L"7";
      // 
      // richTextBox12
      // 
      this->richTextBox12->Location = System::Drawing::Point(685, 82);
      this->richTextBox12->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox12->Name = L"richTextBox12";
      this->richTextBox12->ReadOnly = true;
      this->richTextBox12->Size = System::Drawing::Size(78, 431);
      this->richTextBox12->TabIndex = 10;
      this->richTextBox12->Text = L"";
      this->richTextBox12->WordWrap = false;
      // 
      // label43
      // 
      this->label43->AutoSize = true;
      this->label43->Location = System::Drawing::Point(630, 39);
      this->label43->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label43->Name = L"label43";
      this->label43->Size = System::Drawing::Size(30, 13);
      this->label43->TabIndex = 9;
      this->label43->Text = L"Vista";
      // 
      // richTextBox11
      // 
      this->richTextBox11->Location = System::Drawing::Point(605, 82);
      this->richTextBox11->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox11->Name = L"richTextBox11";
      this->richTextBox11->ReadOnly = true;
      this->richTextBox11->Size = System::Drawing::Size(78, 431);
      this->richTextBox11->TabIndex = 8;
      this->richTextBox11->Text = L"";
      this->richTextBox11->WordWrap = false;
      // 
      // label42
      // 
      this->label42->AutoSize = true;
      this->label42->Location = System::Drawing::Point(550, 43);
      this->label42->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label42->Name = L"label42";
      this->label42->Size = System::Drawing::Size(21, 13);
      this->label42->TabIndex = 7;
      this->label42->Text = L"XP";
      // 
      // richTextBox10
      // 
      this->richTextBox10->Location = System::Drawing::Point(525, 82);
      this->richTextBox10->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox10->Name = L"richTextBox10";
      this->richTextBox10->ReadOnly = true;
      this->richTextBox10->Size = System::Drawing::Size(78, 431);
      this->richTextBox10->TabIndex = 6;
      this->richTextBox10->Text = L"";
      this->richTextBox10->WordWrap = false;
      // 
      // label24
      // 
      this->label24->AutoSize = true;
      this->label24->Location = System::Drawing::Point(413, 43);
      this->label24->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label24->Name = L"label24";
      this->label24->Size = System::Drawing::Size(61, 13);
      this->label24->TabIndex = 5;
      this->label24->Text = L"Driver Date";
      // 
      // label23
      // 
      this->label23->AutoSize = true;
      this->label23->Location = System::Drawing::Point(134, 39);
      this->label23->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label23->Name = L"label23";
      this->label23->Size = System::Drawing::Size(66, 13);
      this->label23->TabIndex = 4;
      this->label23->Text = L"Driver Name";
      // 
      // richTextBox7
      // 
      this->richTextBox7->Location = System::Drawing::Point(374, 82);
      this->richTextBox7->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox7->Name = L"richTextBox7";
      this->richTextBox7->ReadOnly = true;
      this->richTextBox7->Size = System::Drawing::Size(148, 431);
      this->richTextBox7->TabIndex = 1;
      this->richTextBox7->Text = L"May 13 2009\nMay 18 2009\nJuly 13 2009";
      this->richTextBox7->WordWrap = false;
      // 
      // richTextBox6
      // 
      this->richTextBox6->Location = System::Drawing::Point(8, 82);
      this->richTextBox6->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox6->Name = L"richTextBox6";
      this->richTextBox6->ReadOnly = true;
      this->richTextBox6->Size = System::Drawing::Size(361, 431);
      this->richTextBox6->TabIndex = 0;
      this->richTextBox6->Text = L"ASACPI.sys\nGEARAspiWDM.sys\nintelppm.sys";
      this->richTextBox6->WordWrap = false;
      // 
      // tabPage12
      // 
      this->tabPage12->AutoScroll = true;
      this->tabPage12->Controls->Add(this->checkBox22);
      this->tabPage12->Controls->Add(this->button6);
      this->tabPage12->Controls->Add(this->numericUpDown2);
      this->tabPage12->Controls->Add(this->checkBox17);
      this->tabPage12->Controls->Add(this->checkBox18);
      this->tabPage12->Controls->Add(this->textBox26);
      this->tabPage12->Controls->Add(this->checkBox9);
      this->tabPage12->Controls->Add(this->checkBox8);
      this->tabPage12->Controls->Add(this->label26);
      this->tabPage12->Controls->Add(this->richTextBox9);
      this->tabPage12->Location = System::Drawing::Point(4, 22);
      this->tabPage12->Margin = System::Windows::Forms::Padding(2);
      this->tabPage12->Name = L"tabPage12";
      this->tabPage12->Padding = System::Windows::Forms::Padding(2);
      this->tabPage12->Size = System::Drawing::Size(1016, 425);
      this->tabPage12->TabIndex = 9;
      this->tabPage12->Text = L"User kd Commands";
      this->tabPage12->UseVisualStyleBackColor = true;
      // 
      // checkBox22
      // 
      this->checkBox22->AutoSize = true;
      this->checkBox22->Location = System::Drawing::Point(290, 19);
      this->checkBox22->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox22->Name = L"checkBox22";
      this->checkBox22->Size = System::Drawing::Size(57, 17);
      this->checkBox22->TabIndex = 85;
      this->checkBox22->Text = L"Delete";
      this->checkBox22->UseVisualStyleBackColor = true;
      // 
      // button6
      // 
      this->button6->Location = System::Drawing::Point(486, 19);
      this->button6->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->button6->Name = L"button6";
      this->button6->Size = System::Drawing::Size(75, 22);
      this->button6->TabIndex = 84;
      this->button6->Text = L"Proceed";
      this->button6->UseVisualStyleBackColor = true;
      // 
      // numericUpDown2
      // 
      this->numericUpDown2->Enabled = false;
      this->numericUpDown2->Location = System::Drawing::Point(360, 19);
      this->numericUpDown2->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->numericUpDown2->Name = L"numericUpDown2";
      this->numericUpDown2->Size = System::Drawing::Size(120, 20);
      this->numericUpDown2->TabIndex = 83;
      // 
      // checkBox17
      // 
      this->checkBox17->AutoSize = true;
      this->checkBox17->Location = System::Drawing::Point(242, 19);
      this->checkBox17->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox17->Name = L"checkBox17";
      this->checkBox17->Size = System::Drawing::Size(44, 17);
      this->checkBox17->TabIndex = 82;
      this->checkBox17->Text = L"Edit";
      this->checkBox17->UseVisualStyleBackColor = true;
      // 
      // checkBox18
      // 
      this->checkBox18->AutoSize = true;
      this->checkBox18->Location = System::Drawing::Point(194, 19);
      this->checkBox18->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox18->Name = L"checkBox18";
      this->checkBox18->Size = System::Drawing::Size(45, 17);
      this->checkBox18->TabIndex = 81;
      this->checkBox18->Text = L"Add";
      this->checkBox18->UseVisualStyleBackColor = true;
      // 
      // textBox26
      // 
      this->textBox26->Location = System::Drawing::Point(194, 88);
      this->textBox26->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->textBox26->Name = L"textBox26";
      this->textBox26->Size = System::Drawing::Size(361, 20);
      this->textBox26->TabIndex = 80;
      // 
      // checkBox9
      // 
      this->checkBox9->AutoSize = true;
      this->checkBox9->Location = System::Drawing::Point(558, 197);
      this->checkBox9->Margin = System::Windows::Forms::Padding(2);
      this->checkBox9->Name = L"checkBox9";
      this->checkBox9->Size = System::Drawing::Size(163, 17);
      this->checkBox9->TabIndex = 9;
      this->checkBox9->Text = L"Run User kd Commands First";
      this->checkBox9->UseVisualStyleBackColor = true;
      // 
      // checkBox8
      // 
      this->checkBox8->AutoSize = true;
      this->checkBox8->Location = System::Drawing::Point(558, 219);
      this->checkBox8->Margin = System::Windows::Forms::Padding(2);
      this->checkBox8->Name = L"checkBox8";
      this->checkBox8->Size = System::Drawing::Size(164, 17);
      this->checkBox8->TabIndex = 8;
      this->checkBox8->Text = L"Only Use User kd Commands";
      this->checkBox8->UseVisualStyleBackColor = true;
      // 
      // label26
      // 
      this->label26->AutoSize = true;
      this->label26->Location = System::Drawing::Point(322, 72);
      this->label26->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label26->Name = L"label26";
      this->label26->Size = System::Drawing::Size(99, 13);
      this->label26->TabIndex = 7;
      this->label26->Text = L"User kd Commands";
      // 
      // richTextBox9
      // 
      this->richTextBox9->Location = System::Drawing::Point(194, 111);
      this->richTextBox9->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox9->Name = L"richTextBox9";
      this->richTextBox9->ReadOnly = true;
      this->richTextBox9->Size = System::Drawing::Size(361, 346);
      this->richTextBox9->TabIndex = 6;
      this->richTextBox9->Text = L"";
      this->richTextBox9->WordWrap = false;
      // 
      // tabPage11
      // 
      this->tabPage11->AutoScroll = true;
      this->tabPage11->Controls->Add(this->checkBox23);
      this->tabPage11->Controls->Add(this->button7);
      this->tabPage11->Controls->Add(this->numericUpDown3);
      this->tabPage11->Controls->Add(this->checkBox19);
      this->tabPage11->Controls->Add(this->checkBox20);
      this->tabPage11->Controls->Add(this->textBox27);
      this->tabPage11->Controls->Add(this->button1);
      this->tabPage11->Controls->Add(this->label25);
      this->tabPage11->Controls->Add(this->richTextBox8);
      this->tabPage11->Location = System::Drawing::Point(4, 22);
      this->tabPage11->Margin = System::Windows::Forms::Padding(2);
      this->tabPage11->Name = L"tabPage11";
      this->tabPage11->Padding = System::Windows::Forms::Padding(2);
      this->tabPage11->Size = System::Drawing::Size(1016, 425);
      this->tabPage11->TabIndex = 8;
      this->tabPage11->Text = L"Problematic Drivers Stats";
      this->tabPage11->UseVisualStyleBackColor = true;
      // 
      // checkBox23
      // 
      this->checkBox23->AutoSize = true;
      this->checkBox23->Location = System::Drawing::Point(290, 19);
      this->checkBox23->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox23->Name = L"checkBox23";
      this->checkBox23->Size = System::Drawing::Size(57, 17);
      this->checkBox23->TabIndex = 86;
      this->checkBox23->Text = L"Delete";
      this->checkBox23->UseVisualStyleBackColor = true;
      // 
      // button7
      // 
      this->button7->Location = System::Drawing::Point(486, 19);
      this->button7->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->button7->Name = L"button7";
      this->button7->Size = System::Drawing::Size(75, 22);
      this->button7->TabIndex = 84;
      this->button7->Text = L"Proceed";
      this->button7->UseVisualStyleBackColor = true;
      // 
      // numericUpDown3
      // 
      this->numericUpDown3->Enabled = false;
      this->numericUpDown3->Location = System::Drawing::Point(360, 19);
      this->numericUpDown3->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->numericUpDown3->Name = L"numericUpDown3";
      this->numericUpDown3->Size = System::Drawing::Size(120, 20);
      this->numericUpDown3->TabIndex = 83;
      // 
      // checkBox19
      // 
      this->checkBox19->AutoSize = true;
      this->checkBox19->Location = System::Drawing::Point(242, 19);
      this->checkBox19->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox19->Name = L"checkBox19";
      this->checkBox19->Size = System::Drawing::Size(44, 17);
      this->checkBox19->TabIndex = 82;
      this->checkBox19->Text = L"Edit";
      this->checkBox19->UseVisualStyleBackColor = true;
      // 
      // checkBox20
      // 
      this->checkBox20->AutoSize = true;
      this->checkBox20->Location = System::Drawing::Point(194, 19);
      this->checkBox20->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->checkBox20->Name = L"checkBox20";
      this->checkBox20->Size = System::Drawing::Size(45, 17);
      this->checkBox20->TabIndex = 81;
      this->checkBox20->Text = L"Add";
      this->checkBox20->UseVisualStyleBackColor = true;
      // 
      // textBox27
      // 
      this->textBox27->Location = System::Drawing::Point(194, 88);
      this->textBox27->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
      this->textBox27->Name = L"textBox27";
      this->textBox27->Size = System::Drawing::Size(361, 20);
      this->textBox27->TabIndex = 80;
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(568, 216);
      this->button1->Margin = System::Windows::Forms::Padding(2);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(172, 19);
      this->button1->TabIndex = 6;
      this->button1->Text = L"Clear Driver Statistics";
      this->button1->UseVisualStyleBackColor = true;
      // 
      // label25
      // 
      this->label25->AutoSize = true;
      this->label25->Location = System::Drawing::Point(338, 72);
      this->label25->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label25->Name = L"label25";
      this->label25->Size = System::Drawing::Size(66, 13);
      this->label25->TabIndex = 5;
      this->label25->Text = L"Driver Name";
      // 
      // richTextBox8
      // 
      this->richTextBox8->Location = System::Drawing::Point(194, 111);
      this->richTextBox8->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox8->Name = L"richTextBox8";
      this->richTextBox8->ReadOnly = true;
      this->richTextBox8->Size = System::Drawing::Size(361, 338);
      this->richTextBox8->TabIndex = 1;
      this->richTextBox8->Text = L"";
      this->richTextBox8->WordWrap = false;
      // 
      // tabPage2
      // 
      this->tabPage2->AutoScroll = true;
      this->tabPage2->Controls->Add(this->richTextBox1);
      this->tabPage2->Controls->Add(this->label13);
      this->tabPage2->Location = System::Drawing::Point(4, 22);
      this->tabPage2->Margin = System::Windows::Forms::Padding(2);
      this->tabPage2->Name = L"tabPage2";
      this->tabPage2->Padding = System::Windows::Forms::Padding(2);
      this->tabPage2->Size = System::Drawing::Size(1016, 425);
      this->tabPage2->TabIndex = 1;
      this->tabPage2->Text = L"Header";
      this->tabPage2->UseVisualStyleBackColor = true;
      // 
      // richTextBox1
      // 
      this->richTextBox1->Location = System::Drawing::Point(2, 26);
      this->richTextBox1->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox1->Name = L"richTextBox1";
      this->richTextBox1->Size = System::Drawing::Size(744, 435);
      this->richTextBox1->TabIndex = 28;
      this->richTextBox1->Text = L"";
      // 
      // label13
      // 
      this->label13->AutoSize = true;
      this->label13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label13->Location = System::Drawing::Point(352, 4);
      this->label13->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label13->Name = L"label13";
      this->label13->Size = System::Drawing::Size(62, 20);
      this->label13->TabIndex = 16;
      this->label13->Text = L"Header";
      // 
      // tabPage5
      // 
      this->tabPage5->AutoScroll = true;
      this->tabPage5->Controls->Add(this->richTextBox2);
      this->tabPage5->Controls->Add(this->label14);
      this->tabPage5->Location = System::Drawing::Point(4, 22);
      this->tabPage5->Margin = System::Windows::Forms::Padding(2);
      this->tabPage5->Name = L"tabPage5";
      this->tabPage5->Padding = System::Windows::Forms::Padding(2);
      this->tabPage5->Size = System::Drawing::Size(1016, 425);
      this->tabPage5->TabIndex = 2;
      this->tabPage5->Text = L"Footer";
      this->tabPage5->UseVisualStyleBackColor = true;
      // 
      // richTextBox2
      // 
      this->richTextBox2->Location = System::Drawing::Point(2, 26);
      this->richTextBox2->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox2->Name = L"richTextBox2";
      this->richTextBox2->Size = System::Drawing::Size(744, 435);
      this->richTextBox2->TabIndex = 31;
      this->richTextBox2->Text = L"";
      // 
      // label14
      // 
      this->label14->AutoSize = true;
      this->label14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label14->Location = System::Drawing::Point(294, 4);
      this->label14->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label14->Name = L"label14";
      this->label14->Size = System::Drawing::Size(56, 20);
      this->label14->TabIndex = 30;
      this->label14->Text = L"Footer";
      // 
      // tabPage6
      // 
      this->tabPage6->AutoScroll = true;
      this->tabPage6->Controls->Add(this->richTextBox3);
      this->tabPage6->Controls->Add(this->label15);
      this->tabPage6->Location = System::Drawing::Point(4, 22);
      this->tabPage6->Margin = System::Windows::Forms::Padding(2);
      this->tabPage6->Name = L"tabPage6";
      this->tabPage6->Padding = System::Windows::Forms::Padding(2);
      this->tabPage6->Size = System::Drawing::Size(1016, 425);
      this->tabPage6->TabIndex = 3;
      this->tabPage6->Text = L"Signature";
      this->tabPage6->UseVisualStyleBackColor = true;
      // 
      // richTextBox3
      // 
      this->richTextBox3->Location = System::Drawing::Point(2, 26);
      this->richTextBox3->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox3->Name = L"richTextBox3";
      this->richTextBox3->Size = System::Drawing::Size(744, 435);
      this->richTextBox3->TabIndex = 32;
      this->richTextBox3->Text = L"";
      // 
      // label15
      // 
      this->label15->AutoSize = true;
      this->label15->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label15->Location = System::Drawing::Point(282, 4);
      this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label15->Name = L"label15";
      this->label15->Size = System::Drawing::Size(78, 20);
      this->label15->TabIndex = 31;
      this->label15->Text = L"Signature";
      // 
      // tabPage7
      // 
      this->tabPage7->AutoScroll = true;
      this->tabPage7->Controls->Add(this->richTextBox4);
      this->tabPage7->Controls->Add(this->label16);
      this->tabPage7->Location = System::Drawing::Point(4, 22);
      this->tabPage7->Margin = System::Windows::Forms::Padding(2);
      this->tabPage7->Name = L"tabPage7";
      this->tabPage7->Padding = System::Windows::Forms::Padding(2);
      this->tabPage7->Size = System::Drawing::Size(1016, 425);
      this->tabPage7->TabIndex = 4;
      this->tabPage7->Text = L"Driver Update Header";
      this->tabPage7->UseVisualStyleBackColor = true;
      // 
      // richTextBox4
      // 
      this->richTextBox4->Location = System::Drawing::Point(2, 26);
      this->richTextBox4->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox4->Name = L"richTextBox4";
      this->richTextBox4->Size = System::Drawing::Size(744, 435);
      this->richTextBox4->TabIndex = 33;
      this->richTextBox4->Text = L"";
      // 
      // label16
      // 
      this->label16->AutoSize = true;
      this->label16->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label16->Location = System::Drawing::Point(244, 4);
      this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label16->Name = L"label16";
      this->label16->Size = System::Drawing::Size(164, 20);
      this->label16->TabIndex = 32;
      this->label16->Text = L"Driver Update Header";
      // 
      // tabPage8
      // 
      this->tabPage8->AutoScroll = true;
      this->tabPage8->Controls->Add(this->textBox13);
      this->tabPage8->Controls->Add(this->textBox12);
      this->tabPage8->Controls->Add(this->label18);
      this->tabPage8->Controls->Add(this->label17);
      this->tabPage8->Location = System::Drawing::Point(4, 22);
      this->tabPage8->Margin = System::Windows::Forms::Padding(2);
      this->tabPage8->Name = L"tabPage8";
      this->tabPage8->Padding = System::Windows::Forms::Padding(2);
      this->tabPage8->Size = System::Drawing::Size(1016, 425);
      this->tabPage8->TabIndex = 5;
      this->tabPage8->Text = L"Code Box";
      this->tabPage8->UseVisualStyleBackColor = true;
      // 
      // textBox13
      // 
      this->textBox13->Location = System::Drawing::Point(274, 175);
      this->textBox13->Margin = System::Windows::Forms::Padding(2);
      this->textBox13->Name = L"textBox13";
      this->textBox13->Size = System::Drawing::Size(194, 20);
      this->textBox13->TabIndex = 31;
      // 
      // textBox12
      // 
      this->textBox12->Location = System::Drawing::Point(274, 147);
      this->textBox12->Margin = System::Windows::Forms::Padding(2);
      this->textBox12->Name = L"textBox12";
      this->textBox12->Size = System::Drawing::Size(194, 20);
      this->textBox12->TabIndex = 30;
      // 
      // label18
      // 
      this->label18->AutoSize = true;
      this->label18->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label18->Location = System::Drawing::Point(174, 178);
      this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label18->Name = L"label18";
      this->label18->Size = System::Drawing::Size(91, 13);
      this->label18->TabIndex = 29;
      this->label18->Text = L"Code Box End:";
      // 
      // label17
      // 
      this->label17->AutoSize = true;
      this->label17->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label17->Location = System::Drawing::Point(174, 150);
      this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label17->Name = L"label17";
      this->label17->Size = System::Drawing::Size(101, 13);
      this->label17->TabIndex = 28;
      this->label17->Text = L"Code Box Begin:";
      // 
      // tabPage9
      // 
      this->tabPage9->AutoScroll = true;
      this->tabPage9->Controls->Add(this->label46);
      this->tabPage9->Controls->Add(this->richTextBox5);
      this->tabPage9->Location = System::Drawing::Point(4, 22);
      this->tabPage9->Margin = System::Windows::Forms::Padding(2);
      this->tabPage9->Name = L"tabPage9";
      this->tabPage9->Padding = System::Windows::Forms::Padding(2);
      this->tabPage9->Size = System::Drawing::Size(1016, 425);
      this->tabPage9->TabIndex = 6;
      this->tabPage9->Text = L"Template";
      this->tabPage9->UseVisualStyleBackColor = true;
      // 
      // label46
      // 
      this->label46->AutoSize = true;
      this->label46->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label46->Location = System::Drawing::Point(280, 2);
      this->label46->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label46->Name = L"label46";
      this->label46->Size = System::Drawing::Size(75, 20);
      this->label46->TabIndex = 35;
      this->label46->Text = L"Template";
      // 
      // richTextBox5
      // 
      this->richTextBox5->Location = System::Drawing::Point(2, 26);
      this->richTextBox5->Margin = System::Windows::Forms::Padding(2);
      this->richTextBox5->Name = L"richTextBox5";
      this->richTextBox5->Size = System::Drawing::Size(744, 435);
      this->richTextBox5->TabIndex = 34;
      this->richTextBox5->Text = L"";
      // 
      // checkBox24
      // 
      this->checkBox24->AutoSize = true;
      this->checkBox24->Location = System::Drawing::Point(237, 3);
      this->checkBox24->Margin = System::Windows::Forms::Padding(2);
      this->checkBox24->Name = L"checkBox24";
      this->checkBox24->Size = System::Drawing::Size(103, 17);
      this->checkBox24->TabIndex = 119;
      this->checkBox24->TabStop = false;
      this->checkBox24->Text = L"New .dmps Only";
      this->checkBox24->UseVisualStyleBackColor = true;
      // 
      // ChangeSettings
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(1040, 505);
      this->Controls->Add(this->checkBox24);
      this->Controls->Add(this->button9);
      this->Controls->Add(this->label12);
      this->Controls->Add(this->menuStrip1);
      this->Controls->Add(this->tabControl1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Name = L"ChangeSettings";
      this->Text = L"Change Settings";
      this->Load += gcnew System::EventHandler(this, &ChangeSettings::ChangeSettings_Load);
      this->menuStrip1->ResumeLayout(false);
      this->menuStrip1->PerformLayout();
      this->tabControl1->ResumeLayout(false);
      this->tabPage1->ResumeLayout(false);
      this->tabPage1->PerformLayout();
      this->tabControl2->ResumeLayout(false);
      this->tabPage4->ResumeLayout(false);
      this->tabPage13->ResumeLayout(false);
      this->tabPage13->PerformLayout();
      this->menuStrip2->ResumeLayout(false);
      this->menuStrip2->PerformLayout();
      this->tabPage10->ResumeLayout(false);
      this->tabPage10->PerformLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->EndInit();
      this->tabPage12->ResumeLayout(false);
      this->tabPage12->PerformLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown2))->EndInit();
      this->tabPage11->ResumeLayout(false);
      this->tabPage11->PerformLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown3))->EndInit();
      this->tabPage2->ResumeLayout(false);
      this->tabPage2->PerformLayout();
      this->tabPage5->ResumeLayout(false);
      this->tabPage5->PerformLayout();
      this->tabPage6->ResumeLayout(false);
      this->tabPage6->PerformLayout();
      this->tabPage7->ResumeLayout(false);
      this->tabPage7->PerformLayout();
      this->tabPage8->ResumeLayout(false);
      this->tabPage8->PerformLayout();
      this->tabPage9->ResumeLayout(false);
      this->tabPage9->PerformLayout();
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
private: System::Void ChangeSettings_Load(System::Object^  sender, System::EventArgs^  e);
};
}
