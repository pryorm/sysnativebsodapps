mkdir Release
mkdir Setup\Release
cd Release
mkdir SysnativeBSODApps
mkdir SysnativeBSODApps\driverStatistics
copy ..\..\Help\SysnativeBSODApps.chm SysnativeBSODApps\SysnativeBSODApps.chm
copy ..\..\Data\driverStatistics SysnativeBSODApps\driverStatistics
copy ..\..\SysnativeBSODApps_Development\Release\SysnativeBSODApps.exe SysnativeBSODApps\SysnativeBSODApps.exe
copy ..\Setup.exe.compatibility.manifest
copy ..\Setup.exe.compatibility.manifest ..\Setup\Release\Setup.exe.compatibility.manifest