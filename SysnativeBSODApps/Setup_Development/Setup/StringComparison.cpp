#include "stdafx.h"
#include "stringComparison.hpp"
#include <string>
#include <time.h>
#include <vector>
#include <fstream>

// Empty Constructor and Destructor
StringComparison::StringComparison() {
}
StringComparison::~StringComparison() {
}

// Compare two strings regardless of case
bool StringComparison::unSignedCompare(std::string str1, std::string str2){
	if (str1.size() != str2.size()) {
		return false;
	}
	for (std::string::const_iterator c1 = str1.begin(), c2 = str2.begin(); c1 != str1.end(); ++c1, ++c2) {
		// Convert both cases to lowercase prior to comparing
		if (tolower(*c1) != tolower(*c2)) {
			return false;
		}
	}
	return true;
}

// Check if the driver has already been discovered
int StringComparison::driverNewCheck(std::string test1, std::string test2, std::vector<std::string> driverData, std::vector<std::string> timestampData, int countDmps, int strSize){
	if(countDmps == 0){
		return 1;
	}
	int count = 0;
	int count2 = 0;
	for (int i=0; i < strSize; i++){
		if (unSignedCompare(test1, driverData[i]) && unSignedCompare(test2, timestampData[i])){
			count++;
		}
		if (unSignedCompare(test1, driverData[i])){
			count2++;
		}
	}
	if (count == 0){
		if(count2 > 0){
			return 2;
		}
		return 1;
	}
	return 0;
}

// Check for drivers that are not Windows driver by checking through the 
// database and the database of System drivers
std::vector<int> StringComparison::thirdPartyCheck(std::string test, std::string userprofile, std::vector<std::string> database, std::vector<int> databaseSystem, int strSize){
	std::vector<int> count;
	count.push_back(0);
	count.push_back(0);
	std::ofstream outfile;
	outfile.open("compare.txt", std::ios::app);
	for (int i=0; i < strSize; i++){
		/*outfile << test << " " << database[i] << " " << databaseSystem[i] << endl;
		outfile << i << " of " << strSize << endl;*/
		if (unSignedCompare(test, database[i]) && databaseSystem[i] == 1){
			count[0]++;
		}
		if (unSignedCompare(test, database[i])){
			count[1]++;
		}
	}
	if(count[0] > 0){
		count[0] = 0;
	}
	else{
		count[0] = 1;
	}
	if(count[1] > 0){
		count[1] = 0;
	}
	else{
		count[1] = 1;
	}
	return count;
}

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::string StringComparison::currentDateTime() {

    time_t     timeNow = time(0);
	
	struct tm ts;
	char szBuffer[80] = "DD_MM_YY_HH_MM_SS";

    errno_t err = localtime_s(&ts, &timeNow);
	
    if (err)
    {
    }
    else
    {
        // Format the time
        strftime(szBuffer, sizeof(szBuffer), "%Y_%b_%d_%X%p", &ts);
    }
    return szBuffer;
}