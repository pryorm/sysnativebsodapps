#include "stdafx.h"

//
// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly:System::Reflection::AssemblyTitleAttribute("Setup")]; // 'AssemblyTitleAttribute': attribute not found
[assembly:System::Reflection::AssemblyDescriptionAttribute("")];
[assembly:System::Reflection::AssemblyConfigurationAttribute("")];
[assembly:System::Reflection::AssemblyCompanyAttribute("")];
[assembly:System::Reflection::AssemblyProductAttribute("Setup")];
[assembly:System::Reflection::AssemblyCopyrightAttribute("Copyright (c)  2012")];
[assembly:System::Reflection::AssemblyTrademarkAttribute("")];
[assembly:System::Reflection::AssemblyCultureAttribute("")];

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the value or you can default the Revision and Build Numbers
// by using the '*' as shown below:

[assembly:System::Reflection::AssemblyVersionAttribute("1.0.*")];

[assembly:System::Runtime::InteropServices::ComVisible(false)];

[assembly:System::CLSCompliantAttribute(true)];

[System::Security::SuppressUnmanagedCodeSecurity]
public ref class SecurityManager
{
	// Placeholder to require administrative permissions
};