// Setup.cpp : main project file.

#include "stdafx.h"
#include "Form1.h"
#include "MyCheckDir.h"
#include "StringToString.h"

[System::STAThreadAttribute]
int main(array<System::String ^> ^args)
{
  MyCheckDir* mcd;
  StringToString* sts;
  sts = new StringToString();
  mcd = new MyCheckDir();
  std::string myPath = sts->MyToString(System::IO::Directory::GetCurrentDirectory());
  std::vector<std::string> fileList;
  fileList = mcd->GetFileListing(myPath + "\\SysnativeBSODApps");
  std::string inString;
  int appCount = 0;
  int helpCount = 0;
  int fl = 0;
	while(fl < int(fileList.size())){
		inString = fileList[fl];
		if(inString.find("SysnativeBSODApps.exe") != std::string::npos){
			appCount++;
		}
		if(inString.find("SysnativeBSODApps.chm") != std::string::npos){
			helpCount++;
		}
    fl++;
	}
	HANDLE hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);
	ShowWindow( GetConsoleWindow(), SW_HIDE );
	// Enabling Windows XP visual effects before any controls are created
	System::Windows::Forms::Application::EnableVisualStyles();
	System::Windows::Forms::Application::SetCompatibleTextRenderingDefault(false);
	if(appCount && helpCount){
		std::cout << appCount << " " << helpCount << std::endl;
	}
	else{
		System::Windows::Forms::MessageBox::Show(L"Setup is unable to install from this location,\n ****EXITING****",L"Install Error", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Error, System::Windows::Forms::MessageBoxDefaultButton::Button1, System::Windows::Forms::MessageBoxOptions::DefaultDesktopOnly);
		return 0;
	}
  
  StringToString* myString;
  myString = new StringToString();
  std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
  std::string userProfile = myString->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
	// Create the main window and run it
  System::Windows::Forms::Application::Run(gcnew Setup::Form1());
  // remove(string(tempDir + "\\hiddenbat.vbs").c_str());
  // remove(string(tempDir + "\\hidden.bat").c_str());
	remove("temp.txt");
	remove("SysnativeBSODApps\\license.txt");
  delete myString;
	return 0;
}
