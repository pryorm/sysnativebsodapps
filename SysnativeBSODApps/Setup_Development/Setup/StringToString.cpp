#include "stdafx.h"
#include "StringToString.h"
#include <time.h>
#include <algorithm>	// for replace_if

StringToString::StringToString()
{
}

StringToString::~StringToString()
{
}

// converts System String to std::string
std::string StringToString::MyToString(System::String^ myString)
{
	System::String^ s;
	s = myString;
	const char* chars = 
		(const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
	std::string os = chars;
	return os;
}

// obtains the %userprofile% directory
std::string StringToString::getUserProfileString()
{
	char *userProfileTmp;			
	char userProfile[250];
	size_t number_of_elements;

	_dupenv_s(&userProfileTmp, &number_of_elements, "userprofile");

	int i;
	for (i = 0; i < (int) number_of_elements && i < 250; i++)
	{
		userProfile[i] = userProfileTmp[i];
	}
	return std::string(userProfile);
}

// gets the current date and time in a usable format
std::string StringToString::currentDateTime()
{

    time_t     timeNow = time(0);
	
	struct tm ts;
	char szBuffer[80] = "DD_MM_YY_HH_MM_SS";

    errno_t err = localtime_s(&ts, &timeNow);
	
    if (err)
    {
    }
    else
    {
        // Format the time
        strftime(szBuffer, sizeof(szBuffer), "%Y_%b_%d_%X%p", &ts);
    }
    return szBuffer;
}

// gets the time directory string for subdirectory naming
std::string StringToString::timeDirectoryString()
{
	std::string timeNow = currentDateTime();
	replace_if(timeNow.begin(),timeNow.end(),::ispunct,'_');
	std::string timeDir = timeNow.substr(0,timeNow.end()-timeNow.begin()-5);
	return timeDir;
}