#include "CenterWindow.h"
#include "StringToString.h"
#include "MyCheckDir.h"
#include "ConsoleInfo.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include "DirectoryStructure.hpp"
#include <time.h>
#pragma once

namespace Setup {

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
  private:
    int percentDone_i;
    MyCheckDir* mcd;
    bool m_bInstalling;
	private: System::Windows::Forms::TextBox^  textBox1;
	protected: 
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  textBox3;

	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Button^  button8;
	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Button^  button10;
	private: System::Windows::Forms::Button^  button11;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::ProgressBar^  progressBar1;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;

	private: System::Windows::Forms::CheckBox^  checkBox1;
  private: System::Windows::Forms::TextBox^  textBox5;
  private: System::Windows::Forms::Button^  button12;
  private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog1;
  private: System::ComponentModel::BackgroundWorker^  backgroundWorker2;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
      this->textBox1 = (gcnew System::Windows::Forms::TextBox());
      this->textBox2 = (gcnew System::Windows::Forms::TextBox());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->button1 = (gcnew System::Windows::Forms::Button());
      this->button2 = (gcnew System::Windows::Forms::Button());
      this->button3 = (gcnew System::Windows::Forms::Button());
      this->button4 = (gcnew System::Windows::Forms::Button());
      this->button5 = (gcnew System::Windows::Forms::Button());
      this->label3 = (gcnew System::Windows::Forms::Label());
      this->button6 = (gcnew System::Windows::Forms::Button());
      this->button7 = (gcnew System::Windows::Forms::Button());
      this->label4 = (gcnew System::Windows::Forms::Label());
      this->textBox3 = (gcnew System::Windows::Forms::TextBox());
      this->label6 = (gcnew System::Windows::Forms::Label());
      this->label7 = (gcnew System::Windows::Forms::Label());
      this->button8 = (gcnew System::Windows::Forms::Button());
      this->button9 = (gcnew System::Windows::Forms::Button());
      this->label8 = (gcnew System::Windows::Forms::Label());
      this->button10 = (gcnew System::Windows::Forms::Button());
      this->button11 = (gcnew System::Windows::Forms::Button());
      this->label11 = (gcnew System::Windows::Forms::Label());
      this->textBox4 = (gcnew System::Windows::Forms::TextBox());
      this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
      this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
      this->label9 = (gcnew System::Windows::Forms::Label());
      this->label10 = (gcnew System::Windows::Forms::Label());
      this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
      this->textBox5 = (gcnew System::Windows::Forms::TextBox());
      this->button12 = (gcnew System::Windows::Forms::Button());
      this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
      this->backgroundWorker2 = (gcnew System::ComponentModel::BackgroundWorker());
      this->SuspendLayout();
      // 
      // textBox1
      // 
      this->textBox1->Location = System::Drawing::Point(16, 79);
      this->textBox1->Margin = System::Windows::Forms::Padding(2);
      this->textBox1->Multiline = true;
      this->textBox1->Name = L"textBox1";
      this->textBox1->ReadOnly = true;
      this->textBox1->ScrollBars = System::Windows::Forms::ScrollBars::Both;
      this->textBox1->Size = System::Drawing::Size(492, 175);
      this->textBox1->TabIndex = 0;
      this->textBox1->TabStop = false;
      this->textBox1->Text = resources->GetString(L"textBox1.Text");
      // 
      // textBox2
      // 
      this->textBox2->Location = System::Drawing::Point(561, 79);
      this->textBox2->Margin = System::Windows::Forms::Padding(2);
      this->textBox2->Multiline = true;
      this->textBox2->Name = L"textBox2";
      this->textBox2->ReadOnly = true;
      this->textBox2->ScrollBars = System::Windows::Forms::ScrollBars::Both;
      this->textBox2->Size = System::Drawing::Size(492, 175);
      this->textBox2->TabIndex = 1;
      this->textBox2->TabStop = false;
      this->textBox2->Text = resources->GetString(L"textBox2.Text");
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label1->Location = System::Drawing::Point(14, 7);
      this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(378, 26);
      this->label1->TabIndex = 2;
      this->label1->Text = L"Welcome to the SysnativeBSODApps Setup Program\r\n    This program will install Sys" 
        L"nativeBSODApps on your computer";
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(559, 268);
      this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(496, 26);
      this->label2->TabIndex = 3;
      this->label2->Text = L"If you accept all the terms of the agreement, click \'I Agree\' to continue. You mu" 
        L"st accept the agreement \r\nto install SysnativeBSODApps.";
      // 
      // button1
      // 
      this->button1->Location = System::Drawing::Point(314, 319);
      this->button1->Margin = System::Windows::Forms::Padding(2);
      this->button1->Name = L"button1";
      this->button1->Size = System::Drawing::Size(77, 26);
      this->button1->TabIndex = 4;
      this->button1->Text = L"Next >";
      this->button1->UseVisualStyleBackColor = true;
      this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
      // 
      // button2
      // 
      this->button2->Location = System::Drawing::Point(430, 319);
      this->button2->Margin = System::Windows::Forms::Padding(2);
      this->button2->Name = L"button2";
      this->button2->Size = System::Drawing::Size(77, 26);
      this->button2->TabIndex = 5;
      this->button2->Text = L"Cancel";
      this->button2->UseVisualStyleBackColor = true;
      this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
      // 
      // button3
      // 
      this->button3->Location = System::Drawing::Point(776, 319);
      this->button3->Margin = System::Windows::Forms::Padding(2);
      this->button3->Name = L"button3";
      this->button3->Size = System::Drawing::Size(77, 26);
      this->button3->TabIndex = 7;
      this->button3->Text = L"< Back";
      this->button3->UseVisualStyleBackColor = true;
      this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
      // 
      // button4
      // 
      this->button4->Location = System::Drawing::Point(858, 319);
      this->button4->Margin = System::Windows::Forms::Padding(2);
      this->button4->Name = L"button4";
      this->button4->Size = System::Drawing::Size(77, 26);
      this->button4->TabIndex = 6;
      this->button4->Text = L"I Agree";
      this->button4->UseVisualStyleBackColor = true;
      this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
      // 
      // button5
      // 
      this->button5->Location = System::Drawing::Point(975, 319);
      this->button5->Margin = System::Windows::Forms::Padding(2);
      this->button5->Name = L"button5";
      this->button5->Size = System::Drawing::Size(77, 26);
      this->button5->TabIndex = 8;
      this->button5->Text = L"Cancel";
      this->button5->UseVisualStyleBackColor = true;
      this->button5->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
      // 
      // label3
      // 
      this->label3->AutoSize = true;
      this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label3->Location = System::Drawing::Point(559, 7);
      this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label3->Name = L"label3";
      this->label3->Size = System::Drawing::Size(361, 39);
      this->label3->TabIndex = 9;
      this->label3->Text = L"License Agreement\r\n    Please review the licensing agreement before installing th" 
        L"e \r\n    Sysnative BSOD Processing Apps";
      // 
      // button6
      // 
      this->button6->Location = System::Drawing::Point(425, 616);
      this->button6->Margin = System::Windows::Forms::Padding(2);
      this->button6->Name = L"button6";
      this->button6->Size = System::Drawing::Size(77, 26);
      this->button6->TabIndex = 13;
      this->button6->Text = L"Cancel";
      this->button6->UseVisualStyleBackColor = true;
      this->button6->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
      // 
      // button7
      // 
      this->button7->Location = System::Drawing::Point(309, 616);
      this->button7->Margin = System::Windows::Forms::Padding(2);
      this->button7->Name = L"button7";
      this->button7->Size = System::Drawing::Size(77, 26);
      this->button7->TabIndex = 12;
      this->button7->Text = L"Next >";
      this->button7->UseVisualStyleBackColor = true;
      this->button7->Click += gcnew System::EventHandler(this, &Form1::button7_Click);
      // 
      // label4
      // 
      this->label4->AutoSize = true;
      this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label4->Location = System::Drawing::Point(9, 356);
      this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label4->Name = L"label4";
      this->label4->Size = System::Drawing::Size(124, 13);
      this->label4->TabIndex = 11;
      this->label4->Text = L"Destination Location";
      // 
      // textBox3
      // 
      this->textBox3->Location = System::Drawing::Point(11, 401);
      this->textBox3->Margin = System::Windows::Forms::Padding(2);
      this->textBox3->Multiline = true;
      this->textBox3->Name = L"textBox3";
      this->textBox3->ReadOnly = true;
      this->textBox3->ScrollBars = System::Windows::Forms::ScrollBars::Both;
      this->textBox3->Size = System::Drawing::Size(492, 36);
      this->textBox3->TabIndex = 10;
      this->textBox3->TabStop = false;
      this->textBox3->Text = L"Setup will install the Sysnative BSOD Processing Apps in the following location(s" 
        L").";
      // 
      // label6
      // 
      this->label6->AutoSize = true;
      this->label6->Location = System::Drawing::Point(9, 461);
      this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label6->Name = L"label6";
      this->label6->Size = System::Drawing::Size(92, 13);
      this->label6->TabIndex = 15;
      this->label6->Text = L"Destination Folder";
      // 
      // label7
      // 
      this->label7->AutoSize = true;
      this->label7->Location = System::Drawing::Point(9, 561);
      this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label7->Name = L"label7";
      this->label7->Size = System::Drawing::Size(238, 13);
      this->label7->TabIndex = 16;
      this->label7->Text = L"Click \'Next\' to continue or \'Cancel\' to exit Setup...";
      // 
      // button8
      // 
      this->button8->Location = System::Drawing::Point(227, 616);
      this->button8->Margin = System::Windows::Forms::Padding(2);
      this->button8->Name = L"button8";
      this->button8->Size = System::Drawing::Size(77, 26);
      this->button8->TabIndex = 17;
      this->button8->Text = L"< Back";
      this->button8->UseVisualStyleBackColor = true;
      this->button8->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
      // 
      // button9
      // 
      this->button9->Location = System::Drawing::Point(776, 616);
      this->button9->Margin = System::Windows::Forms::Padding(2);
      this->button9->Name = L"button9";
      this->button9->Size = System::Drawing::Size(77, 26);
      this->button9->TabIndex = 25;
      this->button9->Text = L"< Back";
      this->button9->UseVisualStyleBackColor = true;
      this->button9->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
      // 
      // label8
      // 
      this->label8->AutoSize = true;
      this->label8->Location = System::Drawing::Point(558, 534);
      this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label8->Name = L"label8";
      this->label8->Size = System::Drawing::Size(289, 13);
      this->label8->TabIndex = 24;
      this->label8->Text = L"Click \'Install\' to install the software or \'Cancel\' to exit Setup...";
      // 
      // button10
      // 
      this->button10->Location = System::Drawing::Point(974, 616);
      this->button10->Margin = System::Windows::Forms::Padding(2);
      this->button10->Name = L"button10";
      this->button10->Size = System::Drawing::Size(77, 26);
      this->button10->TabIndex = 21;
      this->button10->Text = L"Cancel";
      this->button10->UseVisualStyleBackColor = true;
      this->button10->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
      // 
      // button11
      // 
      this->button11->Location = System::Drawing::Point(858, 616);
      this->button11->Margin = System::Windows::Forms::Padding(2);
      this->button11->Name = L"button11";
      this->button11->Size = System::Drawing::Size(77, 26);
      this->button11->TabIndex = 20;
      this->button11->Text = L"Install";
      this->button11->UseVisualStyleBackColor = true;
      this->button11->Click += gcnew System::EventHandler(this, &Form1::button11_Click);
      // 
      // label11
      // 
      this->label11->AutoSize = true;
      this->label11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
        static_cast<System::Byte>(0)));
      this->label11->Location = System::Drawing::Point(558, 356);
      this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label11->Name = L"label11";
      this->label11->Size = System::Drawing::Size(41, 13);
      this->label11->TabIndex = 19;
      this->label11->Text = L"Install";
      // 
      // textBox4
      // 
      this->textBox4->Location = System::Drawing::Point(560, 401);
      this->textBox4->Margin = System::Windows::Forms::Padding(2);
      this->textBox4->Multiline = true;
      this->textBox4->Name = L"textBox4";
      this->textBox4->ReadOnly = true;
      this->textBox4->ScrollBars = System::Windows::Forms::ScrollBars::Both;
      this->textBox4->Size = System::Drawing::Size(492, 36);
      this->textBox4->TabIndex = 18;
      this->textBox4->TabStop = false;
      this->textBox4->Text = L"The Sysnative BSOD Processing Apps are now ready to be installed";
      // 
      // progressBar1
      // 
      this->progressBar1->Location = System::Drawing::Point(562, 487);
      this->progressBar1->Name = L"progressBar1";
      this->progressBar1->Size = System::Drawing::Size(491, 23);
      this->progressBar1->TabIndex = 26;
      // 
      // backgroundWorker1
      // 
      this->backgroundWorker1->WorkerReportsProgress = true;
      this->backgroundWorker1->WorkerSupportsCancellation = true;
      this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Form1::backgroundWorker1_DoWork);
      this->backgroundWorker1->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &Form1::backgroundWorker1_ProgressChanged);
      this->backgroundWorker1->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &Form1::backgroundWorker1_RunWorkerCompleted);
      // 
      // label9
      // 
      this->label9->AutoSize = true;
      this->label9->Location = System::Drawing::Point(558, 461);
      this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label9->Name = L"label9";
      this->label9->Size = System::Drawing::Size(193, 13);
      this->label9->TabIndex = 27;
      this->label9->Text = L"Status Message: Shoulld Not See Me...";
      // 
      // label10
      // 
      this->label10->AutoSize = true;
      this->label10->Location = System::Drawing::Point(14, 268);
      this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
      this->label10->Name = L"label10";
      this->label10->Size = System::Drawing::Size(238, 13);
      this->label10->TabIndex = 28;
      this->label10->Text = L"Click \'Next\' to continue or \'Cancel\' to exit Setup...";
      // 
      // checkBox1
      // 
      this->checkBox1->AutoSize = true;
      this->checkBox1->Location = System::Drawing::Point(14, 492);
      this->checkBox1->Name = L"checkBox1";
      this->checkBox1->Size = System::Drawing::Size(67, 17);
      this->checkBox1->TabIndex = 30;
      this->checkBox1->Text = L"All Users";
      this->checkBox1->UseVisualStyleBackColor = true;
      this->checkBox1->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox1_CheckedChanged);
      // 
      // textBox5
      // 
      this->textBox5->Location = System::Drawing::Point(11, 515);
      this->textBox5->Name = L"textBox5";
      this->textBox5->Size = System::Drawing::Size(412, 20);
      this->textBox5->TabIndex = 31;
      // 
      // button12
      // 
      this->button12->Location = System::Drawing::Point(428, 513);
      this->button12->Name = L"button12";
      this->button12->Size = System::Drawing::Size(75, 23);
      this->button12->TabIndex = 32;
      this->button12->Text = L"Browse";
      this->button12->UseVisualStyleBackColor = true;
      this->button12->Click += gcnew System::EventHandler(this, &Form1::button12_Click);
      // 
      // backgroundWorker2
      // 
      this->backgroundWorker2->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Form1::backgroundWorker2_DoWork);
      this->backgroundWorker2->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &Form1::backgroundWorker2_RunWorkerCompleted);
      // 
      // Form1
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(1164, 661);
      this->Controls->Add(this->button12);
      this->Controls->Add(this->textBox5);
      this->Controls->Add(this->checkBox1);
      this->Controls->Add(this->label10);
      this->Controls->Add(this->label9);
      this->Controls->Add(this->progressBar1);
      this->Controls->Add(this->button9);
      this->Controls->Add(this->label8);
      this->Controls->Add(this->button10);
      this->Controls->Add(this->button11);
      this->Controls->Add(this->label11);
      this->Controls->Add(this->textBox4);
      this->Controls->Add(this->button8);
      this->Controls->Add(this->label7);
      this->Controls->Add(this->label6);
      this->Controls->Add(this->button6);
      this->Controls->Add(this->button7);
      this->Controls->Add(this->label4);
      this->Controls->Add(this->textBox3);
      this->Controls->Add(this->label3);
      this->Controls->Add(this->button5);
      this->Controls->Add(this->button3);
      this->Controls->Add(this->button4);
      this->Controls->Add(this->button2);
      this->Controls->Add(this->button1);
      this->Controls->Add(this->label2);
      this->Controls->Add(this->label1);
      this->Controls->Add(this->textBox2);
      this->Controls->Add(this->textBox1);
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->Margin = System::Windows::Forms::Padding(2);
      this->Name = L"Form1";
      this->Text = L"Sysnative BSOD Processing Apps";
      this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion

private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
  m_bInstalling = false;
  mcd = nullptr;
	CDPI g_metrics;
	int dpiX = g_metrics.GetDPIX();
	label1->Visible = true;
	textBox1->Visible = true;
	label10->Visible = true;
	button1->Visible = true;
	button2->Visible = true;
	label1->Location = System::Drawing::Point( 14,   7);
	int xPos, yPos;
	label1->Width = textBox1->Width;
	yPos = label1->Location.Y + label1->Height + 14;
	textBox1->Location = System::Drawing::Point( 16,  yPos);		
	yPos = textBox1->Location.Y + textBox1->Height + 14;
	label10->Location = System::Drawing::Point( 14, yPos);
		
	label3->Width = textBox2->Width;
	label3->Location = System::Drawing::Point( 14,   7);
	yPos = label3->Location.Y + label3->Height + 14;
	textBox2->Location = System::Drawing::Point( 16,  yPos);
	yPos = textBox2->Location.Y + textBox2->Height + 14;
	label2->Location = System::Drawing::Point( 14, yPos);	
	xPos = textBox1->Location.X + textBox1->Width - button5->Width;	
	yPos = label2->Location.Y + label2->Height + 14;
	button2->Location = System::Drawing::Point(xPos, yPos);
	xPos = button2->Location.X - 14 - button1->Width;
	button1->Location = System::Drawing::Point(xPos, yPos);	
	this->Height = button2->Location.Y + button2->Height + int(double(60) * double(dpiX)/double(96));;
	this->Width = button2->Location.X + button2->Width + 30;
	CenterWindow *myWindow;
	myWindow = new CenterWindow;
	myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
	delete myWindow;

	label3->Visible = false;
	textBox2->Visible = false;
	label2->Visible = false;
	button3->Visible = false;
	button4->Visible = false;
	button5->Visible = false;

	label4->Visible = false;
	textBox3->Visible = false;
	label6->Visible = false;
	checkBox1->Visible = false;
  textBox5->Visible = false;
  button12->Visible = false;
	label7->Visible = false;
	button8->Visible = false;
	button7->Visible = false;
	button6->Visible = false;

	label11->Visible = false;
	textBox4->Visible = false;
	label9->Visible = false;
	progressBar1->Visible = false;
	label8->Visible = false;
	button9->Visible = false;
	button11->Visible = false;
	button10->Visible = false;
  StringToString* myString;
  myString = new StringToString();
  std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
  // create a junction to the flash drive settings using a batch file
  mcd = new MyCheckDir();
  mcd->RemoveDir(tempDir + "\\SysnativeBSODApps_zdnbak");
  delete mcd;
  mcd = nullptr;
  delete myString;
}
       
     
      
      
/*
Second Screen:
label3       (559,   7) ( 14,   7)
textBox2     (561,  79) ( 16,  79)
label2       (559, 268) ( 14, 268)
button3      (776, 319) (231, 319)
button4      (858, 319) (314, 319)
button5      (975, 319) (430, 319)

Third Screen:
label4       (  9, 356) ( 14,   7)
textBox3     ( 11, 401) ( 16,  79)
label6       (  9, 461) ( 14, 139)
label5       ( 26, 483) ( 31, 161)
label7       (  9, 508) ( 14, 186)
button8      (227, 616) (231, 319)
button7      (309, 616) (314, 319)
button6      (425, 616) (430, 319)

Fourth Screen:
label11      (558, 356) ( 14,   7)
textBox4     (560, 401) ( 16,  79)
Label9       (558, 461) ( 14, 139)
progressBar1 (562, 487) ( 18, 165)
label8       (558, 534) ( 14, 186)
button9      (776, 616) (231, 319)
button11     (858, 616) (314, 319)
button10     (974, 616) (430, 319)
*/
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {			    
			 // Cancel the asynchronous operation. 
			 this->backgroundWorker1->CancelAsync();
			 Close();
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
			 Form1_Load(sender,e);
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		CDPI g_metrics;
		int dpiX = g_metrics.GetDPIX();
		label1->Visible = false;
		textBox1->Visible = false;
		label10->Visible = false;
		button1->Visible = false;
		button2->Visible = false;

		label3->Visible = true;
		textBox2->Visible = true;
		label2->Visible = true;
		button3->Visible = true;
		button4->Visible = true;
		button5->Visible = true;

		int xPos, yPos;
		label3->Width = textBox2->Width;
		label3->Location = System::Drawing::Point( 14,   7);
		yPos = label3->Location.Y + label3->Height + 14;
		textBox2->Location = System::Drawing::Point( 16,  yPos);
		yPos = textBox2->Location.Y + textBox2->Height + 14;
		label2->Location = System::Drawing::Point( 14, yPos);	
		xPos = textBox2->Location.X + textBox2->Width - button5->Width;	
		yPos = label2->Location.Y + label2->Height + 14;
		button5->Location = System::Drawing::Point(xPos, yPos);
		xPos = button5->Location.X - 14 - button4->Width;
		button4->Location = System::Drawing::Point(xPos, yPos);
		xPos = button4->Location.X - 14 - button3->Width;
		button3->Location = System::Drawing::Point(xPos, yPos);
		this->Height = button5->Location.Y + button5->Height + int(double(60) * double(dpiX)/double(96));;
		this->Width = button5->Location.X + button5->Width + 30;
		CenterWindow *myWindow;
		myWindow = new CenterWindow;
		myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
		delete myWindow;

		label4->Visible = false;
		textBox3->Visible = false;
		label6->Visible = false;
		checkBox1->Visible = false;
		textBox5->Visible = false;
        button12->Visible = false;
		label7->Visible = false;
		button8->Visible = false;
		button7->Visible = false;
		button6->Visible = false;

		label11->Visible = false;
		textBox4->Visible = false;
		label9->Visible = false;
		progressBar1->Visible = false;
		label8->Visible = false;
		button9->Visible = false;
		button11->Visible = false;
		button10->Visible = false;
		 }
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e)
  {
    StringToString* myString;
    myString = new StringToString();
	std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
	std::string userProfile = myString->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
	std::ifstream inFile(userProfile + "\\SysnativeBSODApps\\dmpOptions\\installDir.txt");
    if(inFile.good())
    {
      std::string inString;
      getline(inFile,inString);
      textBox5->Text = gcnew System::String(inString.c_str());
    }
    else
    {
      textBox5->Text = gcnew System::String(userProfile.c_str());
    }
    inFile.close();
		std::ofstream outFile("SysnativeBSODApps\\license.txt");
		outFile << "028374615983726";
		outFile.close();
		CDPI g_metrics;
		int dpiX = g_metrics.GetDPIX();
		label1->Visible = false;
		textBox1->Visible = false;
		label10->Visible = false;
		button1->Visible = false;
		button2->Visible = false;

		label3->Visible = false;
		textBox2->Visible = false;
		label2->Visible = false;
		button3->Visible = false;
		button4->Visible = false;
		button5->Visible = false;

		label4->Visible = true;
		textBox3->Visible = true;
		label6->Visible = true;
		checkBox1->Visible = true;
		textBox5->Visible = true;
        button12->Visible = true;
		label7->Visible = true;
		button8->Visible = true;
		button7->Visible = true;
		button6->Visible = true;
		
		checkBox1->Checked = true;
		checkBox1_CheckedChanged(sender, e);

		int xPos, yPos;
		label4->Location = System::Drawing::Point( 14,   7);		
		yPos = label4->Location.Y + label4->Height + 14;
		textBox3->Location = System::Drawing::Point( 16,  yPos);		
		yPos = textBox3->Location.Y + textBox3->Height + 14;
		label6->Location = System::Drawing::Point( 14, yPos);		
		yPos = label6->Location.Y + label6->Height + 14;
		checkBox1->Location = System::Drawing::Point( 33, yPos);	
		yPos = checkBox1->Location.Y + checkBox1->Height;
		textBox5->Location = System::Drawing::Point( 30, yPos);
		textBox5->Width = textBox3->Location.X + textBox3->Width - button12->Width - 30;
		xPos = textBox5->Location.X + textBox5->Width + 10;
		button12->Location = System::Drawing::Point( xPos, yPos - 2);

		label3->Width = textBox2->Width;
		label3->Location = System::Drawing::Point( 14,   7);
		yPos = label3->Location.Y + label3->Height + 14;
		textBox2->Location = System::Drawing::Point( 16,  yPos);
		yPos = textBox2->Location.Y + textBox2->Height + 14;
		label2->Location = System::Drawing::Point( 14, yPos);	
		xPos = textBox3->Location.X + textBox3->Width - button6->Width;	
		yPos = label2->Location.Y + label2->Height + 14;
		button6->Location = System::Drawing::Point(xPos, yPos);
		xPos = button6->Location.X - 14 - button7->Width;
		button7->Location = System::Drawing::Point(xPos, yPos);
		xPos = button7->Location.X - 14 - button8->Width;
		button8->Location = System::Drawing::Point(xPos, yPos);

		this->Height = button6->Location.Y + button6->Height + int(double(60) * double(dpiX)/double(96));
		int myHeight = button6->Location.Y + button6->Height + int(double(60) * double(dpiX)/double(96));

		yPos = textBox5->Location.Y + textBox5->Height + 14;
		label7->Location = System::Drawing::Point( 14, yPos);

		this->Width = button6->Location.X + button6->Width + 30;
		CenterWindow *myWindow;
		myWindow = new CenterWindow;
		myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
		delete myWindow;

		label11->Visible = false;
		textBox4->Visible = false;
		label9->Visible = false;
		progressBar1->Visible = false;
		label8->Visible = false;
		button9->Visible = false;
		button11->Visible = false;
		button10->Visible = false;
    delete myString;
		 }
private: System::Void makeLink(std::string userProfile, std::string path)
{
  OSVERSIONINFOEX osvi;
  ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
  osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
  BOOL bResult = VerifyVersionInfo(&osvi, VER_MAJORVERSION, NULL); // VERSETUPFILE_VERSION is undefined
  if (bResult) {
	  std::cout << "Version verified successfully." << std::endl;
  }
  else {
	  std::cout << "Version verification failed." << std::endl;
  }
  bool IsWindowsVistaOrGreater = bool(osvi.dwMajorVersion != 5);
  std::ofstream outPublicDebug;
  outPublicDebug.open("C:\\Users\\Public\\Documents\\debug.txt", std::ios::app);
  outPublicDebug << int(IsWindowsVistaOrGreater) << " " << osvi.dwMajorVersion << std::endl;
  outPublicDebug.close();
  StringToString* myString;
  myString = new StringToString();
  std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
  // create a junction to the flash drive settings using a batch file
  mcd = new MyCheckDir();
  //mcd->RemoveDir(tempDir + "\\SysnativeBSODApps_zdnbak");
  mcd->MakeDir(tempDir + "\\SysnativeBSODApps_zdnbak");
  mcd->MyCopyDir(userProfile + "\\SysnativeBSODApps",tempDir + "\\SysnativeBSODApps_zdnbak");
  label1->Text = L"Backing up old files. Please wait...";
  Refresh();
  std::ofstream outBatchRunning(tempDir + "\\batchRunning.txt");
  outBatchRunning << 1;
  outBatchRunning.close();
  std::vector<std::string> dirListInit;
  dirListInit = mcd->GetFileListing(tempDir);
  std::string batch = "";
  batch = batch + "attrib -h -s \"" + userProfile + "\\SysnativeBSODApps\"";
  batch = batch + "\nattrib /l -h -s \"" + userProfile + "\\SysnativeBSODApps\"";
  if(!IsWindowsVistaOrGreater)
  {
    batch = batch + "\nattrib -h -s \"" + userProfile + "\\SysnativeBSODApps\"";
  }
  while(path[path.length()-1] == '\\')
  {
    path = path.substr(0,path.length()-1);
  }
  std::ifstream inInstallPath(userProfile + "\\SysnativeBSODApps\\dmpOptions\\installDir.txt");
  std::string oldPath;
  getline(inInstallPath,oldPath);
  inInstallPath.close();
  if(oldPath.length() < 2)
  {
	  inInstallPath.open(path + "\\SysnativeBSODApps\\dmpOptions\\installDir.txt");
	  getline(inInstallPath,oldPath);
	  inInstallPath.close();
  }
  outPublicDebug.open("C:\\Users\\Public\\Documents\\debug2.txt", std::ios::app);
  outPublicDebug << "oldPath = path: " << oldPath << " = " << path << std::endl;
  outPublicDebug << "userProfile = path: " << userProfile << " = " << path << std::endl;
  outPublicDebug << "userProfile\\ = path: " << userProfile + "\\" << " = " << path << std::endl;
  if(oldPath != path && oldPath + "\\" != path && oldPath != path + "\\")
  {
	  if(!IsWindowsVistaOrGreater)
	  {
		  batch = batch + "\nmkdir \"" + userProfile + "\\SysnativeBSODApps2\"";
		  batch = batch + "\nxcopy /s /e /h \"" + oldPath + "\\SysnativeBSODApps\" \"" + tempDir + "\\SysnativeBSODApps_zdnbak\"";
		  batch = batch + "\nrmdir /s /q  \"" + oldPath + "\\SysnativeBSODApps\"";
		  batch = batch + "\nrmdir \"" + path + "\\SysnativeBSODApps\"";
		  batch = batch + "\nmkdir \"" + path + "\\SysnativeBSODApps\"";
	  }
	  else
	  {
		  mcd->RemoveDir(oldPath + "\\SysnativeBSODApps");
		  mcd->RemoveJunc(path + "\\SysnativeBSODApps");
	  }
  }
  if(userProfile != path && userProfile + "\\" != path && userProfile != path + "\\")
  {
	  if(!IsWindowsVistaOrGreater)
	  {
		  batch = batch + "\nrmdir \"" + userProfile + "\\SysnativeBSODApps\"";
		  batch = batch + "\nlinkd \"" + userProfile + "\\SysnativeBSODApps\" \"" + path + "\\SysnativeBSODApps\"";
	  }
	  else
	  {
		  mcd->RemoveJunc(userProfile + "\\SysnativeBSODApps");
		  mcd->MakeJunc(userProfile + "\\SysnativeBSODApps", path + "\\SysnativeBSODApps");
      mcd->CheckDir(path, "SysnativeBSODApps");
	  }
	/*
    batch = batch + "\nrmdir /s /q \"" + userProfile + "\\SysnativeBSODApps\"";
    batch = batch + "\nlinkd \"" + userProfile + "\\SysnativeBSODApps\" \"" + path + "\\SysnativeBSODApps\"";
    batch = batch + "\nmklink /j \"" + userProfile + "\\SysnativeBSODApps\" \"" + path + "\\SysnativeBSODApps\"";
    batch = batch + "\nattrib /l +h +s \"" + userProfile + "\\SysnativeBSODApps\"";
    batch = batch + "\nicacls \"" + path + "\\SysnativeBSODApps\" /t /c /grant Users:F";
    batch = batch + "\ncacls \"" + path + "\\SysnativeBSODApps\" /e /t /c /g Users:F";
	*/
    batch = batch + "\nattrib /l +h +s \"" + userProfile + "\\SysnativeBSODApps\"";
	  if(!IsWindowsVistaOrGreater)
	  {
      batch = batch + "\nattrib +h +s \"" + userProfile + "\\SysnativeBSODApps\"";
	  }
    batch = batch + "\nicacls \"" + path + "\\SysnativeBSODApps\" /t /c /grant Users:F";
    batch = batch + "\ncacls \"" + path + "\\SysnativeBSODApps\" /e /t /c /g Users:F";
  }
  if(oldPath != path && oldPath + "\\" != path && oldPath != path + "\\")
  {
	  if(!IsWindowsVistaOrGreater)
	  {
		  batch = batch + "\nxcopy /s /e /h \"" + tempDir + "\\SysnativeBSODApps_zdnbak\" \"" + userProfile + "\\SysnativeBSODApps\"";
	  }
	  else
	  {
		  mcd->MyCopyDir(tempDir + "\\SysnativeBSODApps_zdnbak",userProfile + "\\SysnativeBSODApps");
	  }
  }
  batch = batch + "\ndel \"" + tempDir + "\\batchRunning.txt\"";
  std::ofstream outfile(tempDir + "\\hidden.bat");
  outfile << batch;
  outfile.close();
  outPublicDebug << std::endl << "batch = " << std::endl << batch << std::endl << std::endl;
  // run the batch file behind the scenes
  std::string vbs = "Set WshShell = CreateObject(\"WScript.Shell\")";
  vbs = vbs + "\nWshShell.Run chr(34) & \"" + tempDir + "\\hidden.bat\" & Chr(34), 0";
  vbs = vbs + "\nSet WshShell = Nothing";
  outfile.open(tempDir + "\\hiddenbat.vbs");
  outfile << vbs;
  outfile.close();
  std::string myCommand = "cmd.exe /c " + tempDir + "\\hiddenbat.vbs";
  std::string test;
  // run the batch file with the console command option
  ConsoleInfo *console;
  console = new ConsoleInfo();
  // create a wstring for the input to the console object
  std::wstring temp;
  // convert the command string to wstring format
  temp.assign(myCommand.begin(),myCommand.end());
  // run the command and save the output to a string in memory
  test = console->getConsoleInfo(const_cast<LPWSTR>(temp.c_str()));
  // delete the console object now that it is not needed (cleans up the memory)
  delete console;
  std::vector<std::string> dirList;
  dirList = mcd->GetFileListing(tempDir);
  int foundRunning = 1;
  int foundRunning2 = 1;
  while(foundRunning == foundRunning2)
  {
	  
    int dl = 0;
	foundRunning = 0;
	while(dl < int(dirList.size())){      
      size_t find1 = dirList[dl].find("batchRunning.txt");
      if(find1 != std::string::npos)
      {
        foundRunning++;
      }
	  dl++;
	}
    dl = 0;
	foundRunning2 = 0;
	while(dl < int(dirListInit.size())){      
      size_t find1 = dirListInit[dl].find("batchRunning.txt");
      if(find1 != std::string::npos)
      {
        foundRunning2++;
      }
	  dl++;
	}
	Sleep(200);
    dirList = mcd->GetFileListing(tempDir);
	outPublicDebug << foundRunning2 << " == " << foundRunning << std::endl;
  }
  outPublicDebug.close();
  //mcd->MyCopyDir(tempDir + "\\SysnativeBSODApps_zdnbak", userProfile + "\\SysnativeBSODApps");
  //mcd->RemoveDir(tempDir + "\\SysnativeBSODApps_zdnbak");
  delete mcd;
  mcd = nullptr;
  delete myString;
}
private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {
    StringToString* myString;
    myString = new StringToString();
	std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
	std::string userProfile = myString->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
    delete myString;
		CDPI g_metrics;
		int dpiX = g_metrics.GetDPIX();
		label1->Visible = false;
		textBox1->Visible = false;
		label10->Visible = false;
		button1->Visible = false;
		button2->Visible = false;

		label3->Visible = false;
		textBox2->Visible = false;
		label2->Visible = false;
		button3->Visible = false;
		button4->Visible = false;
		button5->Visible = false;

		label4->Visible = false;
		textBox3->Visible = false;
		label6->Visible = false;
		checkBox1->Visible = false;
		textBox5->Visible = false;
        button12->Visible = false;
		label7->Visible = false;
		button8->Visible = false;
		button7->Visible = false;
		button6->Visible = false;

		label11->Visible = true;
		textBox4->Visible = true;
		label9->Visible = false;
		progressBar1->Visible = false;
		label8->Visible = true;
		button9->Visible = true;
		button11->Visible = true;
		button10->Visible = true;
		button9->Enabled = true;
		button11->Enabled = true;

    if(!backgroundWorker2->IsBusy)
    {
      backgroundWorker2->RunWorkerAsync();
    }
		
		int xPos, yPos;
		label11->Location = System::Drawing::Point( 14,   7);		
		yPos = label11->Location.Y + label11->Height + 14;
		textBox4->Location = System::Drawing::Point( 16,  yPos);		
		yPos = textBox4->Location.Y + textBox4->Height + 14;
		label8->Location = System::Drawing::Point( 14, yPos);
		
		label3->Width = textBox2->Width;
		label3->Location = System::Drawing::Point( 14,   7);
		yPos = label3->Location.Y + label3->Height + 14;
		textBox2->Location = System::Drawing::Point( 16,  yPos);
		yPos = textBox2->Location.Y + textBox2->Height + 14;
		label2->Location = System::Drawing::Point( 14, yPos);	
		xPos = textBox4->Location.X + textBox4->Width - button10->Width;	
		yPos = label2->Location.Y + label2->Height + 14;
		button10->Location = System::Drawing::Point(xPos, yPos);
		xPos = button10->Location.X - 14 - button11->Width;
		button11->Location = System::Drawing::Point(xPos, yPos);
		xPos = button11->Location.X - 14 - button9->Width;
		button9->Location = System::Drawing::Point(xPos, yPos);
		this->Height = button10->Location.Y + button10->Height + int(double(60) * double(dpiX)/double(96));;
		this->Width = button10->Location.X + button10->Width + 30;
		CenterWindow *myWindow;
		myWindow = new CenterWindow;
		myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
		delete myWindow;
		 }
private: System::Void button11_Click(System::Object^  sender, System::EventArgs^  e) {
		StringToString* myString;
    myString = new StringToString();
	std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
	std::string userProfile = myString->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
    delete myString;
           
    CDPI g_metrics;
		int dpiX = g_metrics.GetDPIX();
		label1->Visible = false;
		textBox1->Visible = false;
		label10->Visible = false;
		button1->Visible = false;
		button2->Visible = false;

		label3->Visible = false;
		textBox2->Visible = false;
		label2->Visible = false;
		button3->Visible = false;
		button4->Visible = false;
		button5->Visible = false;

		label4->Visible = false;
		textBox3->Visible = false;
		label6->Visible = false;
		checkBox1->Visible = false;
		textBox5->Visible = false;
        button12->Visible = false;
		label7->Visible = false;
		button8->Visible = false;
		button7->Visible = false;
		button6->Visible = false;

		label11->Visible = true;
		textBox4->Visible = true;
		label8->Visible = true;
		button9->Visible = true;
		button11->Visible = true;
		button10->Visible = true;
		button9->Enabled = false;
		button11->Enabled = false;
		
		int xPos, yPos;
		label11->Location = System::Drawing::Point( 14,   7);		
		yPos = label11->Location.Y + label11->Height + 14;
		textBox4->Location = System::Drawing::Point( 16,  yPos);		
		yPos = textBox4->Location.Y + textBox4->Height + 14;
		label8->Location = System::Drawing::Point( 14, yPos);
		
		label3->Width = textBox2->Width;
		label3->Location = System::Drawing::Point( 14,   7);
		yPos = label3->Location.Y + label3->Height + 14;
		textBox2->Location = System::Drawing::Point( 16,  yPos);
		yPos = textBox2->Location.Y + textBox2->Height + 14;
		label2->Location = System::Drawing::Point( 14, yPos);	
		xPos = textBox4->Location.X + textBox4->Width - button10->Width;	
		yPos = label2->Location.Y + label2->Height + 14;
		button10->Location = System::Drawing::Point(xPos, yPos);
		xPos = button10->Location.X - 14 - button11->Width;
		button11->Location = System::Drawing::Point(xPos, yPos);
		xPos = button11->Location.X - 14 - button9->Width;
		button9->Location = System::Drawing::Point(xPos, yPos);
		this->Height = button10->Location.Y + button10->Height + int(double(60) * double(dpiX)/double(96));;
		this->Width = button10->Location.X + button10->Width + 30;
		CenterWindow *myWindow;
		myWindow = new CenterWindow;
		myWindow->MyCenterWindow(static_cast<HWND>(this->Handle.ToPointer()), this->Width, this->Height);
		delete myWindow;

		label9->Location = System::Drawing::Point( 14, 139);
		progressBar1->Location = System::Drawing::Point( 18, 165);   

		if(!backgroundWorker1->IsBusy)
    {
      backgroundWorker1->RunWorkerAsync();
    }

		percentDone_i = 231245;
    m_bInstalling = true;
}
private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
    //Sleep(5000);
    StringToString* myString;
    myString = new StringToString();
	std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
	std::string userProfile = myString->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
    delete myString;

		label9->Visible = true;
		progressBar1->Visible = true;
		progressBar1->Maximum = 100;
		std::string inString;
		std::string inString2;
		std::string status = "Installing Sysnative BSOD Apps ...";
    if(mcd != nullptr)
    {
      status = mcd->GetCurrentAction();
    }
		System::String^ str;
		str = gcnew System::String(status.c_str());
		label9->Text = str;
		int percentDone = 231245;
    time_t t1,t2;
    t1 = clock();
    t2 = clock();
    float diff01 = ((float)t2 - (float)t1) / 1000.0F;
    float maxTime = 600.0F;
		while(progressBar1->Value < progressBar1->Maximum && diff01 < maxTime){
      t2 = clock();
      diff01 = ((float)t2 - (float)t1) / 1000.0F;
			Sleep(100);
			Refresh();
      status = "Installing Sysnative BSOD Apps ...";
      if(mcd != nullptr)
      {
        status = mcd->GetCurrentAction();
      }
			System::String^ str;
			str = gcnew System::String(status.c_str());
			label9->Text = str;
			std::ifstream inFile;
            inFile.open(inString2);
			if(inFile.good()){
				inFile >> percentDone;
			}
			inFile.close();
			int percentDone2 = 0;
      diff01 = ((float)t2 - (float)t1) / 1000.0F;
			while(percentDone == 231245 && diff01 < maxTime){
        t2 = clock();
        diff01 = ((float)t2 - (float)t1) / 1000.0F;
				Sleep(100);
				Refresh();
				percentDone = percentDone_i;
        status = "Installing BSOD Apps ...";
        if(mcd != nullptr)
        {
          status = mcd->GetCurrentAction();
        }
				System::String^ str;
				str = gcnew System::String(status.c_str());
				label9->Text = str;

				percentDone2 = int(diff01/maxTime * 100.0F);
				if(percentDone2 > 95){
					percentDone2 = 0;
				}
				backgroundWorker1->ReportProgress(percentDone2);
				progressBar1->Value = percentDone2;
			}
      percentDone = progressBar1->Maximum;
			backgroundWorker1->ReportProgress(percentDone);
			progressBar1->Value = percentDone;
			Refresh();
		 }
		 backgroundWorker1->ReportProgress(progressBar1->Maximum - 1);
		 progressBar1->Value = progressBar1->Maximum - 1;
		 }
private: System::Void backgroundWorker1_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e) {
		// Change the value of the ProgressBar to the BackgroundWorker progress.
		 progressBar1->Value = e->ProgressPercentage;
		 }
private: System::Void backgroundWorker1_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) {
			 Refresh();
			 Sleep(1000);
			 Close();
		 }
private: System::Void checkBox1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
// browse for install location
private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e) {
  StringToString* myString;
  myString = new StringToString();
  std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
  std::string userProfile = myString->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
  delete myString;
  folderBrowserDialog1->ShowDialog();
	textBox5->Text = folderBrowserDialog1->SelectedPath;
         }
private: System::Void backgroundWorker2_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) 
{
          
    while(!m_bInstalling)
    {
      Sleep(100);
    }
		StringToString* myString;
    myString = new StringToString();
	std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
	std::string userProfile = myString->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
    delete myString;

    if(checkBox1->Checked)
    {
		std::string inString;

		  inString = userProfile;
		  std::stringstream sstemp;
		  int countSlash = 0;
		  int countSlash2 = 0;
		  std::string upPath;
		  for(int i=0; i<int(inString.length()); i++){
			  if(inString[i] == '\\'){
				  countSlash++;
			  }
		  }
		  for(int i=0; i<int(inString.length()); i++){
			  sstemp << inString[i];
			  if(inString[i] == '\\'){
				  countSlash2++;
			  }
			  if(countSlash2 == countSlash){
				  break;
			  }
		  }
		  upPath = sstemp.str().substr(0,sstemp.str().length()-1);
		  std::vector<std::string> dirList;
		  
	    std::ofstream outPublicDebug("C:\\Users\\Public\\Documents\\debug.txt");
	    outPublicDebug << "Getting Directory Listing" << std::endl;
	    outPublicDebug.close();
	    outPublicDebug.open("C:\\Users\\Public\\Documents\\debug2.txt");
	    outPublicDebug << "Getting Directory Listing" << std::endl;
	    outPublicDebug.close();
      mcd = new MyCheckDir();
      dirList = mcd->GetDirectoryListing(upPath);

      int dl = 0;
	    while(dl < int(dirList.size())){
        size_t find1 = dirList[dl].find("\\");
        int j1 = 0;
        while(find1 != std::string::npos)
        {
          j1 = int(find1);
          find1 = dirList[dl].find("\\",j1+1,1);
        }
        inString = dirList[dl].substr(j1+1,dirList[dl].length()-j1-1);
			  if(inString != "Public" && inString != "UpdatusUser" && inString != "All Users" && inString != "Default"
				   && inString != "Default User" && inString != "Default.migrated" && inString.length() > 0){
				  inString = upPath + "\\" + inString;
				  System::String^ str = gcnew System::String(inString.c_str());
          StringToString* sts;
          sts = new StringToString();
		  std::string sysnativePath = sts->MyToString(textBox5->Text);

		  std::string myPath = myString->MyToString(System::IO::Directory::GetCurrentDirectory());

		      outPublicDebug.open("C:\\Users\\Public\\Documents\\debug.txt", std::ios::app);
		      outPublicDebug << "copying " << myPath + "\\SysnativeBSODApps to " << inString + "\\SysnativeBSODApps" << std::endl;
		      outPublicDebug.close();

				  mcd->MyCopyDir(myPath + "\\SysnativeBSODApps", inString + "\\SysnativeBSODApps");

		      outPublicDebug.open("C:\\Users\\Public\\Documents\\debug.txt", std::ios::app);
		      outPublicDebug << "Making links." << std::endl;
		      outPublicDebug.close();
          delete mcd;
          mcd = nullptr;
          makeLink(inString,sysnativePath);
          mcd = new MyCheckDir();
          mcd->CheckDir(inString,"SysnativeBSODApps");
          mcd->CheckDir(inString + "\\SysnativeBSODApps","backup");
          mcd->CheckDir(inString + "\\SysnativeBSODApps","dmpOptions");
          mcd->CheckDir(inString + "\\SysnativeBSODApps","download");
          mcd->CheckDir(inString + "\\SysnativeBSODApps","driverStatistics");
          mcd->CheckDir(inString + "\\SysnativeBSODApps","forumSettings");
          mcd->CheckDir(inString + "\\SysnativeBSODApps","parms");
          mcd->MyCopyDir(myPath + "\\SysnativeBSODApps", inString + "\\SysnativeBSODApps");
          delete sts;
          
          remove(std::string(userProfile + "\\SysnativeBSODApps\\dmpOptions\\installDir.txt").c_str());
          Sleep(500);
		  std::ofstream outFile(sysnativePath + "\\SysnativeBSODApps\\dmpOptions\\installDir.txt");
          outFile << sysnativePath;
          outFile.close();
			  }
        dl++;
		  }
    }
    else
    {
      std::string inString;

      inString = userProfile;
      StringToString* sts;
      sts = new StringToString();
	  std::string sysnativePath = sts->MyToString(textBox5->Text);
      delete sts;

      myString = new StringToString();
	  std::string myPath = myString->MyToString(System::IO::Directory::GetCurrentDirectory());

			mcd->MyCopyDir(myPath + "\\SysnativeBSODApps", inString + "\\SysnativeBSODApps");
      delete mcd;
      mcd = nullptr;
		  makeLink(inString,sysnativePath);
      mcd = new MyCheckDir();
      mcd->CheckDir(inString,"SysnativeBSODApps");
      mcd->CheckDir(inString + "\\SysnativeBSODApps","backup");
      mcd->CheckDir(inString + "\\SysnativeBSODApps","dmpOptions");
      mcd->CheckDir(inString + "\\SysnativeBSODApps","download");
      mcd->CheckDir(inString + "\\SysnativeBSODApps","driverStatistics");
      mcd->CheckDir(inString + "\\SysnativeBSODApps","forumSettings");
      mcd->CheckDir(inString + "\\SysnativeBSODApps","parms");
      mcd->MyCopyDir(tempDir + "\\SysnativeBSODApps_zdnbak", userProfile + "\\SysnativeBSODApps");
      mcd->MyCopyDir(myPath + "\\SysnativeBSODApps", inString + "\\SysnativeBSODApps");
      delete myString;
      
      remove(std::string(userProfile + "\\SysnativeBSODApps\\dmpOptions\\installDir.txt").c_str());
      Sleep(500);
	  std::ofstream outFile(inString + "\\SysnativeBSODApps\\dmpOptions\\installDir.txt");
      outFile << sysnativePath;
      outFile.close();
      
		  percentDone_i = 95;
      mcd->SetCurrentAction("Finished!!!");
      
      Sleep(1000);

      
      mcd = new MyCheckDir();
      mcd->SetCurrentAction("Creating directories");

		  DirectoryStructure structure;

		  for(int i=0; i<1; i++){
			  if(1){
				  std::string userString;
				  System::String ^ s = textBox5->Text;
				  const char* chars = 
				  (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
				  userString = chars;
				  std::cout << "userString = " << userString << std::endl;
				  System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));

				  std::cout << "%USERPROFILE% = " << structure.userProfile << std::endl;

				  // First check the current directory structure
				  structure.getCurrentSubdirectories(0);
				  // Create any missing directories needed by this app
				  structure.setUpDirectoryStructure(0);

				  structure.userProfile = userString;
        
				  mcd->SetCurrentAction("Removing extra directories");

				  mcd->SetCurrentAction("Copying help file and apps");

                  myString = new StringToString();
		          std::string myPath = myString->MyToString(System::IO::Directory::GetCurrentDirectory());

				  mcd->MyCopyDir(myPath + "\\SysnativeBSODApps", structure.userProfile + "\\SysnativeBSODApps");

                  delete myString;
			  }
		  }
	  }

	  /*HANDLE hConsole;
	  hConsole = GetStdHandle (STD_OUTPUT_HANDLE);
	  ShowWindow( GetConsoleWindow(), SW_SHOW );*/

	  mcd->SetCurrentAction("Compiling Apps With Native Image Generator");

	  System::Diagnostics::Process^ ngenProcess = gcnew System::Diagnostics::Process();
	  System::String^ runtimeStr = System::Runtime::InteropServices::RuntimeEnvironment::GetRuntimeDirectory();
	  System::String^ ngenStr;
      ngenStr = System::IO::Path::Combine(runtimeStr, "ngen.exe");
      ngenProcess->StartInfo->UseShellExecute = false;
      ngenProcess->StartInfo->FileName = ngenStr;
      ngenProcess->StartInfo->Arguments = 
      System::String::Format("install \"{0}\"", "SysnativeBSODApps\\SysnativeBSODApps.exe");
      ngenProcess->StartInfo->CreateNoWindow = true;
      ngenProcess->Start();
      ngenProcess->WaitForExit();

	  mcd->SetCurrentAction("Finished Compiling Apps With Native Image Generator");
		
	  TopMost = true;
	  Hide();
	  UpdateZOrder();
	  BringToFront();
	  SetForegroundWindow(static_cast<HWND>(this->Handle.ToPointer()));
	  Show();
	  TopMost = false;
    Sleep(1000);

    delete mcd;
    mcd = nullptr;
}
private: System::Void backgroundWorker2_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) {           
  StringToString* myString;
  myString = new StringToString();
  std::string tempDir = myString->MyToString(System::Environment::GetEnvironmentVariable("TEMP"));
  delete myString;

	percentDone_i = 100;
		
	if(mcd != nullptr)
  {
    mcd->SetCurrentAction("Finished!!!");
  }
}
};
}

