#include "stdafx.h"
#include "DirectoryStructure.hpp"
#include <stdlib.h>		// for system
#include <fstream>		// for ifstream
#include <string>		// for string
#include <sstream>		// for stringstream
#include <vector>		// for vector
#include <iostream>		// for cout
#include <algorithm>	// for replace_if
#include "StringComparison.hpp"

// Constructor creates comparison object for comparing strings
//    and driver names/timestamps
DirectoryStructure::DirectoryStructure(){
  comparison = new StringComparison();
  sts = new StringToString();
  mcd = new MyCheckDir();
  tempDir = sts->MyToString(System::Environment::GetEnvironmentVariable("TEMP")) + "\\SysnativeBSODApps";
  userProfile = sts->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
}

// Destructor deletes the comparison object
DirectoryStructure::~DirectoryStructure(){
  delete comparison;
  delete sts;
  delete mcd;
}

void DirectoryStructure::setUpDirectoryStructure(int form){
	// If the outputDmps directory does not exist,
	//    create the directory
  
  System::String^ myPath = System::IO::Directory::GetCurrentDirectory();
  std::string currentDir = sts->MyToString(myPath);
	// mcd->CheckDir(currentDir,gcnew System::String(DSOutputDmpsDir.c_str()));

	std::ifstream inOutputDmpsSubDir(tempDir + "\\outputDmpsSubDir.txt");
	getline(inOutputDmpsSubDir, timeDir);
	timeDir = DSOutputDmpsDir + "\\" + timeDir;
	inOutputDmpsSubDir.close();

	// Create an output subdirectory based on the date/time
	if(!form){
		mcd->MakeDir(timeDir);
	}

  mcd->CheckDir(userProfile,"SysnativeBSODApps");
  mcd->CheckDir(userProfile + "\\SysnativeBSODApps","backup");
  mcd->CheckDir(userProfile + "\\SysnativeBSODApps","dmpOptions");
  mcd->CheckDir(userProfile + "\\SysnativeBSODApps","download");
  mcd->CheckDir(userProfile + "\\SysnativeBSODApps","forumSettings");
  mcd->CheckDir(userProfile + "\\SysnativeBSODApps","parms");
  mcd->CheckDir(userProfile + "\\SysnativeBSODApps","driverStatistics");
  parms = userProfile + "\\SysnativeBSODApps\\parms\\";

	
}

void DirectoryStructure::getCurrentSubdirectories(int form){
  userProfile = sts->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
  std::ifstream inOutputDmpsDir(tempDir + "\\outputDmpsDir.txt");
	getline(inOutputDmpsDir, DSOutputDmpsDir);
	inOutputDmpsDir.close();
}

// Get the user profile directory name
void DirectoryStructure::getUserProfile(){
  userProfile = sts->MyToString(System::Environment::GetEnvironmentVariable("USERPROFILE"));
}

void DirectoryStructure::deleteTemporaryDirectory(){	
}

void DirectoryStructure::deleteDmpOptionsDirectory(){
	mcd->RemoveDir(timeDir);
}