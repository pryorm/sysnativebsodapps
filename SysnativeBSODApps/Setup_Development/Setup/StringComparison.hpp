#ifndef STRINGCOMPARISON_HPP
#define STRINGCOMPARISON_HPP
//-----------------------------------------------------------------------------
//  File:         StringComparison c++ class
//  Description:  Compare driver list names with those in DRT
//  Project:      dmpAnalysis
//  Author:       Mikael Pryor
//  Date:         July  28, 2012
//  Changes:      July  28, 2012
//  Changes Made: Added functions for comparing driver lists and getting the
//                current date and time.
//-----------------------------------------------------------------------------

#include <string>
#include <vector>

class StringComparison {
public:
	StringComparison();
	virtual ~StringComparison();
	bool unSignedCompare(std::string str1, std::string str2);
	int driverNewCheck(std::string test1, std::string test2, std::vector<std::string> driverData, std::vector<std::string> timestampData, int countDmps, int strSize);
	std::vector<int> thirdPartyCheck(std::string test, std::string userprofile, std::vector<std::string> database, std::vector<int> databaseSystem, int strSize);
	const std::string currentDateTime();
protected:

private:
  
};

#endif