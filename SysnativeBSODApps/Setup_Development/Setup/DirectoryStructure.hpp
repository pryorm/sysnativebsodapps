#ifndef DIRECTORYSTRUCTURE_HPP
#define DIRECTORYSTRUCTURE_HPP
//-----------------------------------------------------------------------------
//  File:         DirectoryStructure c++ class
//  Description:  Generates the directory structure for the output files
//  Project:      dmpAnalysis
//  Author:       Mikael Pryor
//  Date:         July  28, 2012
//  Changes:      July  29, 2012
//  Changes Made: Added functions for obtaining the current driver structure
//                and for generating the directories needed for output files.
//                Also obtained the user profile directory.
//    07/29/2012: Added SysnativeBSODApps directory listings
//                Added the deleteTempDirectory() function.
//-----------------------------------------------------------------------------
#include "StringComparison.hpp"
#include "MyCheckDir.h"
#include "StringToString.h"

class DirectoryStructure {
public:
	// Constructor, Destructor, and Functions
	DirectoryStructure();
	virtual ~DirectoryStructure();
	void setUpDirectoryStructure(int form);
	void getCurrentSubdirectories(int form);
	void getUserProfile();
	void deleteTemporaryDirectory();
	void deleteDmpOptionsDirectory();
	
	// Variables
	std::string timeDir;
	std::string userProfile;
	std::string parms;
	std::string parms2;
	int parmsExist;					// For determining if the _jcgriff2_ parms directory exists
	int dmpsExist;
protected:

private:
	int DSoutputDmps;				// For determining whether outputDmps directory exists
	int DStmp;						// For determining whether tmp directory exists
	int DSSysnativeBSODApps;		// For determining whether SysnativeBSODApps exists
	int DSSysnativeBSODAppsSub1;	// For determining whether subdirectories exist
	int DSSysnativeBSODAppsSub2;	// For determining whether subdirectories exist
	int DSSysnativeBSODAppsSub3;	// For determining whether subdirectories exist
	int DSSysnativeBSODAppsSub4;	// For determining whether subdirectories exist
	int DSSysnativeBSODAppsSub5;	// For determining whether subdirectories exist
	std::string DSOutputDmpsDir;
	StringComparison *comparison;	// For creating the time-based directory name
  
  MyCheckDir* mcd;
  StringToString* sts;
  std::string tempDir;
};

#endif