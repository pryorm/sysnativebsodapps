#ifndef MYCHECKDIR_H
#define MYCHECKDIR_H
#include <vector>
#include <string>

class MyCheckDir{
public:
  MyCheckDir();
  virtual ~MyCheckDir();
  std::vector<int> DirSearch(System::String^ sDir, System::String^ search);
  void CheckDir(std::string subPath, System::String^ name);
  std::vector<std::string> GetFileListing(std::string dir);
  std::vector<std::string> GetDirectoryListing(std::string dir);
  std::vector<std::string> GetPathsRecursive(std::string dir,std::string search);
  void MyCopy(const std::string &from, const std::string &to);
  void MyMove(const std::string &from, const std::string &to);
  void MyCopyDir(const std::string &from, const std::string &to);
  void MakeDir(std::string pathS);
  void RemoveFilesRecursive(std::string pathS);
  void MakeJunc(std::string pathS, std::string pathS2);
  void RemoveJunc(std::string pathS);
  void RemoveDir(std::string pathS);
  void OpenMyFile(std::string str);
  std::vector<std::string> MyCheckDir::DmpCheck();
  std::string GetCurrentAction();
  void SetCurrentAction(std::string curAction);
private:
    std::string m_sCurAction;
};
#endif