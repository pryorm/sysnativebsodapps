#include "stdafx.h"
#include "MyCheckDir.h"
#include "StringToString.h"
#include <tchar.h>
#include <fstream>
#include <Windows.h>

MyCheckDir::MyCheckDir()
{
  m_sCurAction = "Checking Directory Structure ... ";
}

MyCheckDir::~MyCheckDir()
{
}

std::string MyCheckDir::GetCurrentAction()
{
  return m_sCurAction;
}

void MyCheckDir::SetCurrentAction(std::string curAction)
{
  m_sCurAction = curAction;
}

std::vector<int> MyCheckDir::DirSearch(System::String^ sDir, System::String^ search)
{
  std::vector<int> foundThem;
  foundThem.push_back(0);
  foundThem.push_back(0);
  try
  {
    StringToString* sts;
    sts = new StringToString();
    // Find the subfolders in the folder that is passed in.
    cli::array<System::String^>^ d = System::IO::Directory::GetDirectories(sDir,search, System::IO::SearchOption::TopDirectoryOnly);
    int numDirs = d->Length;
    // Find all the files in the subfolder.
	cli::array<System::String^>^ f = System::IO::Directory::GetFiles(sDir,search, System::IO::SearchOption::TopDirectoryOnly);
    int numFiles = f->Length;

    for (int j=0; j < numFiles; j++)
    {
      foundThem[0] = 1;
      m_sCurAction = "Found file : " + sts->MyToString(f[j]);
    }

    for (int i=0; i < numDirs; i++)
    {
      foundThem[1] = 1;
      m_sCurAction = "Found directory : " + sts->MyToString(d[i]);
    }
    delete sts;
  }
  catch (System::Exception^ e)
  {
    System::Console::WriteLine(e->Message);
  }
  return foundThem;
}

void MyCheckDir::MakeDir(std::string pathS)
{
    m_sCurAction = "Making " + pathS;
	std::wstring temp;
    // convert the path string to wstring format
    temp.assign(pathS.begin(),pathS.end());
    CreateDirectory(const_cast<LPWSTR>(temp.c_str()),NULL);
}

void MyCheckDir::RemoveFilesRecursive(std::string pathS)
{
  System::String^ sDir = gcnew System::String(pathS.c_str());
  System::String^ sName = "";
  try
  {
    // Find the subfolders in the folder that is passed in.
    array<System::String^>^ d = System::IO::Directory::GetDirectories(sDir);
    int numDirs = d->Length;
    // Find the subfolders in the folder that is passed in.
    array<System::String^>^ f = System::IO::Directory::GetFiles(sDir);
    int numFiles = f->Length;

    for (int i=0; i < numFiles; i++)
    {
      StringToString* myString;
      myString = new StringToString();
      remove(myString->MyToString(f[i]).c_str());
      m_sCurAction = "Removing file : " + myString->MyToString(f[i]);
      delete myString;
    }
		for (int i=0; i < numDirs; i++)
		{
          std::vector<std::string> tempPath;
          StringToString myString;
          RemoveDir(myString.MyToString(d[i]));
          m_sCurAction = "Removing directory : " + myString.MyToString(d[i]);
		}
  }
  catch (System::Exception^ e)
  {
    System::Console::WriteLine(e->Message);
  }
}

void MyCheckDir::MakeJunc(std::string pathS, std::string pathS2)
{
  m_sCurAction = "Making junction from " + pathS + " to " + pathS2;
  std::wstring temp;
  std::wstring temp2;
  // convert the path string to wstring format
  temp.assign(pathS.begin(),pathS.end());
  temp2.assign(pathS2.begin(),pathS2.end());
  std::ofstream outPublicDebug;
  BOOLEAN bSymLink = CreateSymbolicLink(const_cast<LPWSTR>(temp.c_str()),const_cast<LPWSTR>(temp2.c_str()),1);
}

void MyCheckDir::RemoveJunc(std::string pathS)
{
  m_sCurAction = "Removing junction at : " + pathS;
  std::wstring temp;
  // convert the path string to wstring format
  temp.assign(pathS.begin(),pathS.end());
  RemoveDirectory(const_cast<LPWSTR>(temp.c_str()));
	DeleteFile(const_cast<LPWSTR>(temp.c_str()));
}


void MyCheckDir::RemoveDir(std::string pathS)
{
  m_sCurAction = "Removing directory : " + pathS;
  RemoveFilesRecursive(pathS);
  std::wstring temp;
  // convert the path string to wstring format
  temp.assign(pathS.begin(),pathS.end());
  RemoveDirectory(const_cast<LPWSTR>(temp.c_str()));
}

void MyCheckDir::CheckDir(std::string subPath, System::String^ name)
{
  StringToString* myString;
  myString = new StringToString();
  m_sCurAction = "Searching " + subPath + " for " + myString->MyToString(name);
  std::vector<int> foundThem;
  foundThem.push_back(0);
  foundThem.push_back(0);
  std::string path = subPath;
  System::String^ search = name;
  System::String^ sDir = gcnew System::String(path.c_str());
  foundThem = DirSearch(sDir,search);
  if(foundThem[0])
  {
    std::string pathS = myString->MyToString(sDir) + "\\" + myString->MyToString(search);
    std::string pathS2 = myString->MyToString(sDir) + "\\" + myString->MyToString(search) + "_zdnbak";
	std::wstring temp;
    // convert the path string to wstring format
    temp.assign(pathS.begin(),pathS.end());
	std::wstring temp2;
    // convert the path string to wstring format
    temp2.assign(pathS2.begin(),pathS2.end());
    MoveFile(const_cast<LPWSTR>(temp.c_str()),const_cast<LPWSTR>(temp2.c_str()));
  }
  if(!foundThem[1])
  {
    std::string pathS = myString->MyToString(sDir) + "\\" + myString->MyToString(search);
    MakeDir(pathS);
  }
  delete myString;
}

std::vector<std::string> MyCheckDir::GetFileListing(std::string dir)
{
  System::String^ sDir = gcnew System::String(dir.c_str());
  std::vector<std::string> foundThem;
  try
  {
    // Find the subfolders in the folder that is passed in.
    array<System::String^>^ f = System::IO::Directory::GetFiles(sDir);
    int numFiles = f->Length;

    for (int i=0; i < numFiles; i++)
    {
      StringToString* myString;
      myString = new StringToString();
      foundThem.push_back(myString->MyToString(f[i]));
      m_sCurAction = "This directory includes file : " + myString->MyToString(f[i]);
      delete myString;
    }
  }
  catch (System::Exception^ e)
  {
    System::Console::WriteLine(e->Message);
  }
  return foundThem;
}

std::vector<std::string> MyCheckDir::GetPathsRecursive(std::string dir, std::string search)
{
  System::String^ sDir = gcnew System::String(dir.c_str());
  System::String^ sName = gcnew System::String(search.c_str());
  std::vector<std::string> foundThem;
  try
  {
		// Find the subfolders in the folder that is passed in.
		array<System::String^>^ d = System::IO::Directory::GetDirectories(sDir);
		int numDirs = d->Length;
    // Find the files in the folder that is passed in.
    array<System::String^>^ f = System::IO::Directory::GetFiles(sDir,sName, System::IO::SearchOption::TopDirectoryOnly);
    int numFiles = f->Length;

    for (int i=0; i < numFiles; i++)
    {
      StringToString* myString;
      myString = new StringToString();
      foundThem.push_back(myString->MyToString(f[i]));
      m_sCurAction = "This directory includes file : " + myString->MyToString(f[i]);
      delete myString;
    }
		for (int i=0; i < numDirs; i++)
		{
			std::vector<std::string> tempPath;
      StringToString* myString;
      myString = new StringToString();
      tempPath = GetPathsRecursive(myString->MyToString(d[i]),search);
      m_sCurAction = "This directory includes directory : " + myString->MyToString(d[i]);
      delete myString;
      for(int j=0; j < int(tempPath.size()); j++)
      {
        foundThem.push_back(tempPath[j]);
      }
		}
  }
  catch (System::Exception^ e)
  {
    System::Console::WriteLine(e->Message);
  }
  return foundThem;
}

std::vector<std::string> MyCheckDir::GetDirectoryListing(std::string dir)
{
	System::String^ sDir = gcnew System::String(dir.c_str());
  std::vector<std::string> foundThem;
  try
  {
    // Find the subfolders in the folder that is passed in.
    array<System::String^>^ d = System::IO::Directory::GetDirectories(sDir);
    int numDirs = d->Length;

    for (int i=0; i < numDirs; i++)
    {
      StringToString* myString;
      myString = new StringToString();
      foundThem.push_back(myString->MyToString(d[i]));
      m_sCurAction = "This directory includes directory : " + myString->MyToString(d[i]);
      delete myString;
    }
  }
  catch (System::Exception^ e)
  {
    System::Console::WriteLine(e->Message);
  }
  return foundThem;
}

void MyCheckDir::MyCopy(const std::string &from, const std::string &to)
{   
  remove(to.c_str());
  std::wstring temp;
  // convert the path string to wstring format
  temp.assign(from.begin(),from.end());
  std::wstring temp2;
  // convert the path string to wstring format
  temp2.assign(to.begin(),to.end());
  m_sCurAction = "Copying " + from + " to " + to;
  CopyFile(const_cast<LPWSTR>(temp.c_str()),const_cast<LPWSTR>(temp2.c_str()),true);
}

void MyCheckDir::MyMove(const std::string &from, const std::string &to)
{
    remove(to.c_str());
	std::wstring temp;
    // convert the path string to wstring format
    temp.assign(from.begin(),from.end());
	std::wstring temp2;
    // convert the path string to wstring format
    temp2.assign(to.begin(),to.end());
    m_sCurAction = "Moving " + from + " to " + to;
    MoveFile(const_cast<LPWSTR>(temp.c_str()),const_cast<LPWSTR>(temp2.c_str()));
}

void MyCheckDir::MyCopyDir(const std::string &from, const std::string &to)
{
  m_sCurAction = "Copying " + from + " to " + to;
  System::String^ sDir = gcnew System::String(from.c_str());
  std::wstring temp;
  // convert the path string to wstring format
  temp.assign(to.begin(),to.end());
  CreateDirectory(const_cast<LPWSTR>(temp.c_str()),NULL);
  try
  {
		// Find the subfolders in the folder that is passed in.
		array<System::String^>^ d = System::IO::Directory::GetDirectories(sDir);
		int numDirs = d->Length;
    // Find the subfolders in the folder that is passed in.
    array<System::String^>^ f = System::IO::Directory::GetFiles(sDir);
    int numFiles = f->Length;

    for (int i=0; i < numFiles; i++)
    {
      StringToString* myString;
      myString = new StringToString();
	  std::string fileName;
      size_t find1 = myString->MyToString(f[i]).find("\\");
      int j1 = 0;
      while(find1 != std::string::npos)
      {
        j1 = int(find1);
        find1 = myString->MyToString(f[i]).find("\\",j1+1,1);
      }
      fileName = myString->MyToString(f[i]).substr(j1+1,myString->MyToString(f[i]).length()-j1-1);
      MyCopy(myString->MyToString(f[i]),to + "\\" + fileName);
      delete myString;
    }
    
    for (int i=0; i < numDirs; i++)
    {
      StringToString* myString;
      myString = new StringToString();
	  std::string dirName;
      size_t find1 = myString->MyToString(d[i]).find("\\");
      int j1 = 0;
      while(find1 != std::string::npos)
      {
        j1 = int(find1);
        find1 = myString->MyToString(d[i]).find("\\",j1+1,1);
      }
      dirName = myString->MyToString(d[i]).substr(j1+1,myString->MyToString(d[i]).length()-j1-1);
      MyCopyDir(myString->MyToString(d[i]), to + "\\" + dirName);
      delete myString;
    }
  }
  catch (System::Exception^ e)
  {
    System::Console::WriteLine(e->Message);
  }
}

void MyCheckDir::OpenMyFile(std::string str)
{
  std::string tempPath = "C:\\WINDOWS\\system32\\notepad.exe " + str;
  TCHAR *Lparam=new TCHAR[tempPath.size()+1];
  Lparam[tempPath.size()]=0;
  //As much as we'd love to, we can't use memcpy() because
  //sizeof(TCHAR)==sizeof(char) may not be true:
  copy(tempPath.begin(),tempPath.end(),Lparam);

  STARTUPINFO startup_info = {0};
  startup_info.cb = sizeof startup_info;
  PROCESS_INFORMATION pi = {0};

  if(!CreateProcess( _T("C:\\WINDOWS\\system32\\notepad.exe"),
    _T(param),
    NULL,
    NULL,
    FALSE,
    0,
    NULL,
    NULL,
    &startup_info,
    &pi) )
  {
    printf( "CreateProcess failed (%d).\n", GetLastError() );
  }
}

std::vector<std::string> MyCheckDir::DmpCheck()
{
  System::String^ myPath = System::IO::Directory::GetCurrentDirectory();
  array<System::String^>^ f = System::IO::Directory::GetFiles(myPath);
  int numFiles = f->Length;
  int i = 0;
  std::vector<std::string> foundDmps;
  for (int j=0; j < numFiles; j++)
  {
    StringToString* myString;
    myString = new StringToString;
	std::string path = myString->MyToString(f[j]);
	std::string path2 = path;
		for(int i1=0; i1 < int(path.size()); i1++)
    {
			path2[i1] = tolower(path[i1]);
		}
    if((path2.find("dmp") != std::string::npos) || (path2.find("txt") != std::string::npos))
    {
      using namespace System::IO;
      using namespace System::Text;
	  System::String^ ft = System::IO::File::ReadAllText(f[j]);
      double size;
	  std::ifstream ifile(path);
      ifile.seekg(0, std::ios_base::end);//seek to end
      //now get current position as length of file
      size = double(ifile.tellg()) / 1024.0;
      size_t find1 = myString->MyToString(f[j]).find("\\");
      int i1;
      while (find1 != std::string::npos)
      {
        i1 = int(find1);
        find1 = myString->MyToString(f[j]).find("\\",i1+1,1);
      }
      if((myString->MyToString(ft).find("PAGEDU") != std::string::npos) || (myString->MyToString(ft).find("MDMP") != std::string::npos))
      {
        std::string dmpNames;
        dmpNames = myString->MyToString(f[j]).substr(i1+1,myString->MyToString(f[j]).length()-i1-1);
        foundDmps.push_back(dmpNames);
      }
    }
  }
  return foundDmps;
}