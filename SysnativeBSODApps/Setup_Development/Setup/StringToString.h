#ifndef STRINGTOSTRING_H
#define STRINGTOSTRING_H
#include <string>

class StringToString{
public:
	StringToString();
	virtual ~StringToString();

	// converts System String to std::string
	std::string MyToString(System::String^ myString);

	// obtains the %userprofile% directory
	std::string getUserProfileString();

	// gets the current date and time in a usable format
	std::string currentDateTime(); 

	// gets the time directory string for subdirectory naming
	std::string timeDirectoryString();
private:
};
#endif